<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/ESOP-gratuity-list.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Gratuity Lists</h1>
			<button class="btn-normal f-right margin-top-20 modal-trigger" modal-target="add-gratuity">Add Gratuity</button>
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<div>
					<div class="select add-radius display-inline-mid">
						<p class="white-color margin-bottom-5">ESOP Name:</p>
						<select>
							<option value="ALL ESOP">All ESOP</option>
							<option value="ESOP 1">ESOP 1</option>
							<option value="ESOP 2">ESOP 2</option>
							<option value="E-ESOP 1">E-ESOP 1</option>
							<option value="ESOP 3">ESOP 3</option>
						</select>
					</div>
					<div class="select add-radius display-inline-mid">
						<p class="white-color margin-bottom-5">Status:</p>
						<select>
							<option value="All status">All Status</option>
							<option value="Pending Approval">Pending Approval</option>
							<option value="Gratuity Sent">Gratuity Sent</option>
							<option value="Approved">Approved</option>
							<option value="Rejected">Rejected</option>
						</select>
					</div>
					<div class="display-inline-bot search-me">
						<button class="btn-normal display-inline-mid margin-left-10">Filter List</button>
					</div>

					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
				</div>
			</div>


			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content">

		

		<div class="tbl-rounded">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Date Added</th>
						<th>ESOP Name</th>
						<th>Gratuity per Share</th>
						<th>Total Value of Gratuity Given</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>October 10, 2015</td>
						<td>ESOP 1</td>
						<td>Php 0.60 / Share</td>
						<td>Php 1,300,215.15</td>
						<td>Pending Approval</td>
						<td><span class="blue-color modal-trigger cursor-default" modal-target="approve-gratuity">Approve</span><span> | </span><span class="light-red-color modal-trigger cursor-default" modal-target="reject-gratuity">Reject</span></td>
					</tr>
					<tr>
						<td>September 30, 2015</td>
						<td>ESOP 2</td>
						<td>Php 0.60 / Share</td>
						<td>Php 1,300,215.15</td>
						<td><span class="green-color">Approve</span></td>
						<td></td>
					</tr>
					<tr>
						<td>September 30, 2015</td>
						<td>E-ESOP 1</td>
						<td>Php 0.60 / Share</td>
						<td>Php 1,300,215.15</td>
						<td><span class="light-red-color">Rejected</span></td>
						<td><span class="blue-color modal-trigger cursor-default" modal-target="view-rejection-reason">View Rejection Reason</span></td>
					</tr>
					<tr>
						<td>September 20, 2015</td>
						<td>ESOP 3</td>
						<td>Php 0.60 / Share</td>
						<td>Php 1,300,215.15</td>
						<td><span class="green-color">Gratuity Sent</span></td>
						<td></td>
					</tr>
				</tbody>
			</table>
	
		

	</div>
</section>

<!-- add Gratuity -->
<div class="modal-container" modal-id="add-gratuity">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Add Gratuity</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<select >
									<option value="dept1">ESOP 1</option>
									<option value="dept2">ESOP 2</option>
									<option value="dept3">E-ESOP 1</option>
									<option value="dept4">ESOP 3</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Price per Share</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-150px margin-left-20">
							<div class="select add-radius width-100px">
								<select>
									<option value="price1">PHP</option>
									<option value="price2">USD</option>
									<option value="price3">CAD</option>
									<option value="price4">HKD</option>
									<option value="price5">INR</option>
								</select>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Add Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- Reject Graduity -->
<div class="modal-container" modal-id="reject-gratuity">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Reject Gratuity</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<p><strong>Please indicate reason for rejection:</strong></p>
			<textarea rows="4" cols="30">
			</textarea>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Reject Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- Approve Gratuity -->
<div class="modal-container" modal-id="approve-gratuity">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Approve Gratuity</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<p><strong>Are you sure you want to approve this gratuity application?</strong></p>
			<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>ESOP 1</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Dividend Price per Share:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>Php 0.60 / Share</strong></p>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Approve Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- View Rejection Reason -->
<div class="modal-container" modal-id="view-rejection-reason">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">View Rejection Reason</h4>
			<div class="modal-close close-me"></div>
		</div>
		<!-- content -->
		<div class="modal-content">		
			<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>ESOP 1</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Dividend Price per Share:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>Php 0.60 / Share</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Date Applied:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>September 30, 2015</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Reason:</p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</strong></p>
			
		</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>