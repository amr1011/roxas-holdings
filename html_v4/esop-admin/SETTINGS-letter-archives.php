<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left ">Letter Archives</h1>
			<h2 class="f-left hidden">ESOP View</h2>
			<a href="SETTINGS-offer-letter.php">
				<button class="btn-normal f-right">Back to Active List</button>
			</a>
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10 add-radius margin-top-5">
							<select>
								<option value="op1">Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid add-border-radius-5px margin-top-5" />
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-5">Search</button>
					</td>
				</tr>
			<tbody>
		</table>		
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="text-right-line margin-bottom-50">
			<div class="line"></div>
		</div>
		
		<div class="clear"></div>
	

		<div class="panel-group text-left margin-top-30">

			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color  active">							
							Offer Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>						
					</a>
					<p class="f-right white-color font-16">04 Letters</p>		
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">		

					<div class="panel-body padding-15px">
						
						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

					</div>		
				</div>
			</div>


			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Acceptance Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>	
					<p class="white-color font-16 f-right">10 Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
					<div class="panel-body padding-15px">
						
						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-archive.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

					</div>	
				</div>
			</div>
		</div>
	<div>

</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>