<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP List</h1>
			<div class="f-right">
				<button class="btn-normal margin-top-20 margin-right-10">Download Template for ESOP Distribution</button>
				<a href="SETTINGS-add-user.php">
					<button class="btn-normal margin-top-20">Add User</button>
				</a>
			</div>
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="username">Username</option>
							<option value="name">Name</option>
							<option value="company">Company</option>
							<option value="user-role">User Role</option>
						</select>
					</div>
					<div class="display-inline-mid username">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
					<div class="display-inline-mid name">
						<input type="text" class="search width-200px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
				</div>
			</div>

			<div class="display-inline-mid margin-left-10 company">
				<p class="white-color margin-bottom-5 ">Company</p>
				<div>
					<div class="select add-radius margin-right-10">
						<select>
							<option value="roxol">ROXOL</option>
							<option value="capdi">CAPDI</option>
							<option value="caci">CACI</option>
						</select>
					</div>
					<div class="select add-radius margin-left-10">
						<select>
							<option value="all">All</option>
							<option value="esop-admin">ESOP Admin</option>
							<option value="hr">HR Department</option>
						</select>
					</div>
					
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid user-role margin-left-10">
				<p class="white-color margin-bottom-5 ">User Role
				</p>				
				<div class="select add-radius">
					<select>
						<option value="role-admin">ESOP Admin</option>
						<option value="role-head">HR Head</option>
						<option value="role-officer">HR Officer</option>
						<option value="role-emp">Employee</option>
					</select>
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Company</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Username</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Employee No.</a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">

		<div class="grid-content">

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee No.</th>
						<th>Employee Name</th>
						<th>Username</th>
						<th>User Role</th>
						<th>Company</th>
						<th>Department</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
				</tbody>
			</table>



		</div>


	<div>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>