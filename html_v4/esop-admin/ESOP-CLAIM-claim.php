<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/ESOP-gratuity-list.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP Claims</h1>
			
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
							
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
					
				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<p class="padding-left-20 margin-bottom-5 white-color">Price per Share</p>
				
				
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>

			<button class="btn-normal f-right margin-top-20 modal-trigger" modal-target="claim-for-an-employee">Claim for an Employee</button>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">ESOP Name</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>				
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Status <i class="fa fa-chevron-down"></i></a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">

		<div class="grid-content">

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-pending font-15 font-bold padding-right-20">PENDING</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-final font-15 font-bold padding-right-20">FOR FINALIZATION</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim-finalization.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-complete font-15 font-bold padding-right-20">COMPLETED</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-complete font-15 font-bold padding-right-20">COMPLETE</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-reject font-15 font-bold padding-right-20">REJECTED</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-reject font-15 font-bold padding-right-20">REJECTED</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>
		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee No.</th>
						<th>Employee Name</th>
						<th>ESOP Name</th>
						<th>Vesting Year</th>
						<th>No. of Shares</th>
						<th>Price per Share</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-pending">Pending</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-final">For Finalization</p></td>
						<td><a href="ESOP-CLAIM-view-claim-finalization.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-complete">Approved</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><a href="color-complete">Approved</a></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-reject">Rejected</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
</section>

<!-- claim for an employee -->
<div class="modal-container" modal-id="claim-for-an-employee">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Claim for an Employee</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="success">Claim created successfully. <br /> Redirecting you to the Claim Document...</div>
			<table>
				<tbody>
					<tr>
						<td><strong>Select Employee:</strong></td>
					</tr>
					<tr>
						<td>
							<p>Department:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<select >
									<option value="dept1">Select Department</option>
									<option value="dept2">IICT</option>
									<option value="dept3">HR Dept</option>
									<option value="dept4">Cane Supply Dept</option>
									<option value="dept5">Medical and Dental Dept</option>
									<option value="dept6">Trading Options Dept</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Rank:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<select>
									<option value="rank1">Select Rank</option>
									<option value="rank2">Executive</option>
									<option value="rank3">Senior Manager</option>
									<option value="rank4">Manager</option>
									<option value="rank5">AM</option>
								</select>							
							</div>
						</td>
					</tr>
				</tbody>
				<tbody>
					<tr>
						<td>
							<p>Employee:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<select>
									<option value="emo1">Select Employee</option>
									<option value="emp2">Aaron Paul Labing-Isa</option>
									<option value="emp3">Eric Nilo</option>
									<option value="emp4">Luzviminda Uy</option>
									<option value="emp5">Paul Aaron Yoingco</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>Select ESOP to claim:</strong></td>
					</tr>
					<tr>
						<td>
							<p>ESOP Name:</p>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<select>
									<option value="esp1">ESOP 1</option>
									<option value="esp2">ESOP 2</option>
									<option value="esp3">ESOP 3</option>
									<option value="esp4">E-ESOP 1</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Select Vesting Year (Multiselect)</p>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<select>
									<option value="svy1">Select Vesting Year (Multiselect)</option>
									<option value="svy2">Year 1</option>
									<option value="svy3">Year 2</option>
									<option value="svy4">Year 3</option>
								</select>
							</div>
						</td>
					</tr>
					
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Claim Vesting Year</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>