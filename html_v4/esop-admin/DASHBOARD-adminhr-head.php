<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="content-panel">  

  <div class="content tect-center margin-top-40 text-center">      
    <h2 class="font-400 font-styling">Welcome to RHI Employee Stock Options Plan (ESOP) System</h2>

    <h2 class="font-400 margin-top-40 text-center font-styling">Currently, you have no ESOP in the system. <br/> To add an ESOP, please click the button below.</h2>
    <button class="btn-normal display-inline-mid modal-trigger font-20 margin-top-25 padding-top-10 padding-bottom-10"  modal-target="add-esop">Add an ESOP</button>	
    <h2 class="font-400 margin-top-40 text-center font-styling">Once you have an ESOP ofered to you, you can:</h2>
  <div>
  <div class="content">
    <div class="display-inline-mid width-300px margin-20px">
      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/handshake.png" alt=""  class="icons-dashboard"></div>
      <p class="font-18 white-color margin-top-20">Distribute ESOP Shares</p>
    </div>
    <div class="display-inline-mid width-300px margin-20px">
      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/group-3.png" alt=""  class="icons-dashboard"></div>
      <p class="font-18 white-color margin-top-20">Manage Employee ESOP Claims</p>
    </div>
    <div class="display-inline-mid width-300px margin-20px">
      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/money.png" alt=""  class="icons-dashboard"></div>
      <p class="font-18 white-color margin-top-20">Manage Gratuities and <br/> ESOP Payments</p>
    </div>
  </div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">ADD ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">Total Share Quantity is invalid. <br /> Please type numbers only in the textbox</div>
			<table class="width-100percent">
				<tbody>
					<tr>
						<td class="width-200px">ESOP Name:</td>
						<td><input type="text" class="small width-250px add-border-radius-5px"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid add-radius ">
								<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-bottom-40">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal add-border-radius-5px width-250px"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
							<label class="black-color font-15 txt-normal margin-top-10 share-lbl">
								<input type="checkbox" class="zoom1-2" />
									Share Allotment
							</label>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							
							<div class="add-esop-dash ">
								<table>
									<thead>
										<tr>
											<th>ESOP Name</th>
											<th>Remaining Share QTY</th>
											<th>Shares to be Taken</th>						
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>ESOP 1</td>
											<td>826,985.08</td>
											<td><input type="text" class="small add-border-radius-5px" value="200000" /></td>						
										</tr>
										<tr>
											<td>ESOP 2</td>
											<td>25,500,000.00</td>
											<td><input type="text" class="small add-border-radius-5px" /></td>
										</tr>					
									</tbody>
								</table>
							</div>

						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal add-border-radius-5px width-250px"/>
							<div class="select xsmall display-inline-mid margin-left-10 add-radius">
								<select>
									<option value="PHP">PHP</option>
									<option value="USD">USD</option>
									<option value="CAD">CAD</option>
									<option value="HKD">HKD</option>
									<option value="INR">INR</option>
								</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal add-border-radius-5px width-250px"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			
			<button type="button" class="display-inline-mid btn-dark">Add ESOP</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>