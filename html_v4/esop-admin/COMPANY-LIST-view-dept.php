<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/ESOP-gratuity-list.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">View Department Information</h1>
			<div class="f-right">
				<button class="btn-normal display-inline-mid margin-right-10 modal-trigger" modal-target="add-rank">Add Rank</button>
				<a href="COMPANY-LIST-edit-rank.php"><button class="btn-normal display-inline-mid margin-right-10 " >Edit Rank</button></a>
				<button class="btn-normal display-inline-mid modal-trigger"  modal-target="edit-rank">Edit Department</button>				
			</div>
			<div class="clear"></div>
		</div>
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="margin-top-20 white-color">
			<table class="width-100percent">
				<tbody>
					<tr>
						<td>Department Code:</td>
						<td>Department Name:</td>
					</tr>
					<tr>
						<td class="padding-top-10">101001</td>
						<td class="padding-top-10">Office of the President</td>
					</tr>
				</tbody>
			</table>
			
			<p class="margin-top-30">Description:</p>
			<p class="margin-top-10">Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​,
			 morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra
			  adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The 
			  voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. 
			  Nescio an Undead zombies. Sicut malus movie horror.</p>
		</div>
		<div class="text-right-line margin-top-5">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Rank <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Rank No.</a></p>					
			</div>
		</div>
	
		<h2 class="margin-top-40">Rank List</h2>
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display comp-info ">
				<thead>
					<tr>
						<th class="width-200px">No</th>
						<th>Rank</th>
						<th class="width-150px">No. of Employee</th>					
					</tr>
				</thead>		
				<tbody>
					<tr>
						<td>10100101</td>
						<td>Chairman</td>
						<td>10</td>
					</tr>
					<tr>
						<td>10100102</td>
						<td>President &amp; CEO</td>
						<td>05</td>
					</tr>
				</tbody>
			</table>
		</div>
	

		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Audit Logs
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
							<div class="panel-body ">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Date of Activiy</th>
											<th>User</th>
											<th>Shares Offered</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>September 10, 2015</td>
											<td>SALAZAR, JOSELITO</td>
											<td>Created Department <span class="font-bold">"HR Department"</span></td>
										</tr>
										<tr>
											<td>September 10, 2015</td>
											<td>SALAZAR, JOSELITO</td>
											<td>Created Rank <span class="font-bold">"Chairman"</span></td>
										</tr>
									
									</tbody>
								</table>

							</div>			
						</div>
					</div>
		</div>
	<div>
</section>

<!-- edit RANK -->
<div class="modal-container" modal-id="edit-rank">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT DEPARTMENT INFORMATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<table class="">
				<tbody>
					<tr>
						<td>Department Code:</td>
						<td><input type="text" class="normal margin-left-10 add-border-radius-5px width-300px" value="101001 " /></td>
					</tr>
					<tr>
						<td>Department Name:</td>
						<td><input type="text" class="normal margin-left-10 add-border-radius-5px width-300px" value="Office of the President " /></td>
					</tr>
					<tr>
						<td class="v-top">Description:</td>
						<td>
							<textarea class="margin-left-10 black-color width-300px add-border-radius-5px" placeholder="Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead."></textarea>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal">Edit Department</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add RANK -->
<div class="modal-container" modal-id="add-rank">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD RANK</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<table class="width-100ppercent center-margin">
				<tbody>
					<tr>
						<td class="padding-top-10">No. :</td>
						<td><input type="text" class="normal margin-left-20 width-300px add-border-radius-5px"/></td>
					</tr>
					<tr>
						<td class="v-top padding-top-15">Rank Name:</td>
						<td>
							<div class="select margin-left-20 width-300px add-radius">
								<select>
									<option value="op1">Value 1</option>
									<option value="op2">Value 2</option>
									<option value="op3">Value 3</option>
								</select>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal">Add Rank</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>