<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="ESOP-esop.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="ESOP-view-esop.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>Distribute Shares</a>
			</div>			
			<div class="clear"></div>
		</div>
		<div class="f-right">
			<a href="ESOP-view-esop.php">
				<button class="btn-cancel margin-right-10 color-cancel">Cancel</button>
			</a>
			<a href="ESOP-view-esop-2.php">
				<button class="btn-normal">Finalise Distribution</button>
			</a>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		
		<div class="error-out margin-top-30">Share's of CACI Employees have exceeded the Total Alloted Shares. 
			Please re-distribute the shares accordingly
		</div>
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content print-statement">

		<div>
			<h2 class="f-left margin-top-0 margin-bottom-0">CACI</h2>			
			<div class="f-right">
				<input type="text" class="large add-border-radius-5px display-inline-mid" />
				<p class="display-inline-mid font-20 white-color margin-left-10">Allotted Shares</p>
			</div>
			<div class="clear"></div>
		</div>
		
		<div class="long-panel border-10px margin-top-30 bg-other-color">
			<div class="f-left">
				<p class="first-text margin-right-30">Offer Letter</p>
				<div class="select add-radius width-200px">
					<select>
						<option value="letter1">Offer Letter 1</option>
						<option value="letter2">Offer Letter 2</option>
						<option value="letter3">Offer Letter 3</option>
						<option value="letter4">Offer Letter 4</option>
						<option value="letter5">Offer Letter 5</option>
					</select>
				</div>
			</div>
			<div class="f-right">
				<p class="second-text f-left margin-right-50">Acceptance Letter:</p>
				<div class="select add-radius f-left drop-change-color width-200px">
					<select>
						<option value="acc1">Acceptance Letter 1</option>
						<option value="acc2">Acceptance Letter 2</option>
						<option value="acc3">Acceptance Letter 3</option>
						<option value="acc4">Acceptance Letter 4</option>
						<option value="acc5">Acceptance Letter 5</option>
					</select>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>


		<div class="width-100per display-block margin-top-30 bg-last-content border-10px">
			<p class="font-20 white-color padding-top-10 padding-left-30 padding-bottom-10">Employee</p>
		</div>

		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Company Code</th>
						<th>Employee Code</th>
						<th>Department Name</th>
						<th>Employee Name</th>
						<th>Rank / Level</th>
						<th>Alloted Shares</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>0001</td>
						<td>0001</td>
						<td>Office of the President</td>
						<td>Juan Dela Cruz</td>
						<td>Executive</td>
						<td><input type="text" class="small add-border-radius-5px " /></td>
					</tr>
					<tr>
						<td>0002</td>
						<td>01214</td>
						<td>Corporate Accounting</td>
						<td>Marcelino Cabingao</td>
						<td>Executive</td>
						<td><input type="text" class="small add-border-radius-5px " /></td>
					</tr>					
				</tbody>
			</table>
		</div>

		<div class="text-right-line margin-top-30">
			<div class="line"></div>
		</div>


		<div class="margin-top-70">
			<h2 class="margin-top-0 margin-bottom-0 f-left">CAPDI.HO</h2>
			
			<div class="f-right">
				<input type="text" class="large add-border-radius-5px display-inline-mid" />
				<p class="display-inline-mid font-20 white-color margin-left-10">Allotted Shares</p>
			</div>
			<div class="clear"></div>
		</div>
		
		<div class="long-panel border-10px margin-top-30 bg-other-color">
			<div class="f-left">
				<p class="first-text margin-right-30">Offer Letter</p>
				<div class="select add-radius width-200px">
					<select>
						<option value="letter1">Offer Letter 1</option>
						<option value="letter2">Offer Letter 2</option>
						<option value="letter3">Offer Letter 3</option>
						<option value="letter4">Offer Letter 4</option>
						<option value="letter5">Offer Letter 5</option>
					</select>
				</div>
			</div>
			<div class="f-right">
				<p class="second-text f-left margin-right-50">Acceptance Letter:</p>
				<div class="select add-radius f-left drop-change-color width-200px">
					<select>
						<option value="acc1">Acceptance Letter 1</option>
						<option value="acc2">Acceptance Letter 2</option>
						<option value="acc3">Acceptance Letter 3</option>
						<option value="acc4">Acceptance Letter 4</option>
						<option value="acc5">Acceptance Letter 5</option>
					</select>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>


		<div class="width-100per display-block margin-top-30 bg-last-content border-10px">
			<p class="font-20 white-color padding-top-10 padding-left-30 padding-bottom-10">Employee</p>
		</div>

		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Company Code</th>
						<th>Employee Code</th>
						<th>Department Name</th>
						<th>Employee Name</th>
						<th>Rank / Level</th>
						<th>Alloted Shares</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>0001</td>
						<td>0001</td>
						<td>Office of the President</td>
						<td>Juan Dela Cruz</td>
						<td>Executive</td>
						<td><input type="text" class="small add-border-radius-5px " /></td>
					</tr>
					<tr>
						<td>0002</td>
						<td>01214</td>
						<td>Corporate Accounting</td>
						<td>Marcelino Cabingao</td>
						<td>Executive</td>
						<td><input type="text" class="small add-border-radius-5px " /></td>
					</tr>					
				</tbody>
			</table>
		</div>

	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>