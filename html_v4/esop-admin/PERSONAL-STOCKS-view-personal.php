<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="PERSONAL-STOCK.php">Personal Stock</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>				
			</div>			
			<div class="clear"></div>
		</div>
		<h1 class="f-left"><abbr title="Employee Stock Option Plan">ESOP</abbr> 1 Statement of Account</h1>
		<div class="f-right">
			<button class="btn-normal margin-right-10 modal-trigger" modal-target="claim-document">View Claim Documents</button>
			<button class="btn-normal modal-trigger" modal-target="vesting-rights">Claim Vesting Right</button>
		</div>
		<div class="clear"></div>

		<div class="user-profile margin-top-30">
			<div class="user-image">
				<img src="../assets/images/profile/profile.jpg" alt="user-picture" />
			</div>
			<div class="user-name">
				<p class="name display-block">Joselito Salazar</p>
				<p class="position">Employee No: <span>132</span></p>
			</div>
			<div class="user-info">
				<table>
					<tbody>
						<tr>
							<td>Company</td>
							<td>ROXOL</td>
						</tr>
						<tr>
							<td>Department</td>
							<td>ISD</td>
						</tr>
						<tr>
							<td>Rank:</td>
							<td>Officer I</td>
						</tr>
						<tr>
							<td>Separation Status: </td>
							<td>N/A</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content">

		<div class="option-box">
			<p class="title">Grant Started</p>
			<p class="description">August 31, 2013</p>
		</div>

		<div class="option-box">
			<p class="title">Subscription Price</p>
			<p class="description">Php 6.00 </p>
		</div>

		<div class="option-box">
			<p class="title">No. of Shares Availed</p>
			<p class="description">130,144</p>
		</div>

		<div class="option-box">
			<p class="title">Value of Shares Availed</p>
			<p class="description">Php 324,058.56 </p>
		</div>

		<h2 class="margin-top-35">Vesting Rights</h2>

		<div class="tbl-rounded">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>No.</th>
						<th>Year</th>
						<th>Percentage</th>
						<th>No. of Shares</th>
						<th>Value of Shares</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content">					
						<td colspan="2"></td>
						<td class="combine"><span class="total-text">Total:</span> 100%</td>
						<td>130,144</td>
						<td>Php 324,058.56 </td>
					</tr>
				</tbody>
			</table>
		

		<h2 class="margin-top-35">Summary</h2>

		<div class="long-panel border-10px">
			<p class="first-text">Amount Shares Taken 130,144 | @ 2.40</p>
			<p class="second-text margin-right-80">Php 324,058.56</p>
			<div class="clear"></div>
		</div>
	
		<div class="tbl-runded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>No.</th>
						<th>Payment Details</th>
						<th>Shares</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>06</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>-</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="4" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>

		<h2 class="margin-top-35">Payment Application</h2>

		<div class="long-panel border-10px">
			<p class="first-text font-bold">Year 2</p>
			<p class="first-text margin-left-30";>July 28, 2015</p>
			<p class="second-text margin-right-65">With Vesting Rights</p>
			<div class="clear"></div>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10">130,144 | @ 2.40</span></p>
			<p class="second-text margin-right-80">Php 324,058.56</p>
			<div class="clear"></div>
		</div>

		<div class="tbl-runded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>No.</th>
						<th>Payment Details</th>
						<th>Shares</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>06</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>-</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="4" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text font-bold">Year 3</p>
			<p class="first-text margin-left-30">July 28, 2015</p>
			<p class="second-text margin-right-65">With Vesting Rights</p>
			<div class="clear"></div>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text"><strong>Amount Shares Taken</strong>  <span class="margin-left-10">130,144 | @ 2.40</span></p>
			<p class="second-text margin-right-80">Php 324,058.56</p>
			<div class="clear"></div>
		</div>

		<div class="tbl-runded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>No.</th>
						<th>Payment Details</th>
						<th>Shares</th>
						<th>Page</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>06</td>
						<td>3 of 5</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>12</td>
						<td>3 of 5</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>-</td>
						<td>3 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>12</td>
						<td>3 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>12</td>
						<td>2 of 4</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="5" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text font-bold">Year 4</p>
			<p class="first-text margin-left-30">July 28, 2015</p>
			<p class="second-text margin-right-65">With Vesting Rights</p>
			<div class="clear"></div>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10">130,144 | @ 2.40</span></p>
			<p class="second-text margin-right-80">Php 324,058.56</p>
			<div class="clear"></div>
		</div>

		<div class="tbl-runded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>No.</th>
						<th>Payment Details</th>
						<th>Shares</th>
						<th>Page</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>06</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>12</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>-</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>12</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>12</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="5" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text font-bold">Year 5</p>
			<p class="first-text margin-left-30">July 28, 2015</p>
			<p class="second-text margin-right-65">With Vesting Rights</p>
			<div class="clear"></div>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10">130,144 | @ 2.40</span></p>
			<p class="second-text margin-right-80">Php 324,058.56</p>
			<div class="clear"></div>
		</div>

		<div class="tbl-runded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>No.</th>
						<th>Payment Details</th>
						<th>Shares</th>
						<th>Page</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>06</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>12</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>-</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>12</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>12</td>
						<td>4 of 5</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="5" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>


		<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Activity Description</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Claimed <span class="font-bold">Year 1</span>Vesting Rights</td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>VALENCIA, RENATO CRUZ</td>
									<td>View Claim Form from <span class="font-bold">Year 1</span>Vesting Rights</td>
								</tr>
							
							</tbody>
						</table>

					</div>			
				</div>
			</div>
			
		<div>
	</div>
</section>

<!-- claim documents  -->
<div class="modal-container" modal-id="claim-document">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">VIEW CLAIM DOCUMENTS</h4>
			<div class="modal-close close-me margin-top-5"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<table class="width-100percent text-center margin-top-10">
				<thead>
					<tr>
						<th></th>
						<th class="text-center">Years Claimed</th>
						<th class="text-center">Date of Vesting Rights</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="radio" name="claim-document" id="year1"></td>
						<td><label for="year1" class="black-color normal font-normal">Year 1 &amp; Year 2</label></td>
						<td>July 01, 2013</td>
					</tr>
					<tr>
						<td><input type="radio" name="claim-document" id="year3"></td>
						<td><label for="year3" class="black-color normal font-normal">Year 3</label></td>
						<td>July 01, 2013</td>
					</tr>
				</tbody>
			</table>
			<div class="margin-top-10 text-center">
				<p class="display-inline-mid margin-right-30 ">Document Type</p>
				<div class="select add-radius display-inline-mid width-250px personal-stock-dd">
					<select>
						<option value="op1">Statement of Account</option>
						<option value="op2">Claim Form</option>
					</select>	
				</div>
			</div>
		</div>	
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal personal-stock-dd-btn">Confirm</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- vesting righhts  -->
<div class="modal-container " modal-id="vesting-rights">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CLAIM VESTING RIGHTS</h4>
			<div class="modal-close close-me margin-top-5"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please select allowed vesting year/s to claim:</p>
			<table class="width-100percent text-center margin-top-10">
				<thead>
					<tr>
						<th></th>
						<th class="text-center">Allowed Year</th>
						<th class="text-center">Date of Vesting Rights</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="width-50px"><input type="checkbox"></td>
						<td>Year 1</td>
						<td>September 01, 2013</td>
					</tr>
					<tr>
						<td><input type="checkbox">
						</td>
						<td>Year 1</td>
						<td>September 01, 2013</td>
					</tr>
				</tbody>
			</table>
					
		</div>	
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>					
			<button type="button" class="display-inline-mid btn-normal">Claim Vesting Rights</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>