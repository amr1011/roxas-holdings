<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../esop-admin/REPORTS-donwloadable-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		<div class="breadcrumbs margin-bottom-20 border-10px">
			<a href="ESOP-esop.php">Reports</a>
			<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
			<a href="ESOP-view-esop.php">Downloadable Reports</a>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
	
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">

		<div class="grid-content">

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>Share Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="share-summary">Generate Report</button>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>Subscription Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="subscription-summary">Generate Report</button>
				</div>
			</div>
			
			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="esop-summary">Generate Report</button>
				</div>
			</div>
			
			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>Claim Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="claim-summary">Generate Report</button>
				</div>
			</div>

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price per Share</th>
						<th>Vesting Years</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
</section>

<!-- Share Summary -->
<div class="modal-container" modal-id="share-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Share Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class=""></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 1</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 2</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 3</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 4</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Filter by Company:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="">Show All Companies</p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ROXOL</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CACI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CADPI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">RHI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Generate Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- Suscription Summary -->
<div class="modal-container" modal-id="subscription-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Subscription Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class=""></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 1</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 2</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 3</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ESOP 4</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Filter by Company:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="">Show All Companies</p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ROXOL</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CACI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CADPI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">RHI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Generate Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- ESOP Summary -->
<div class="modal-container" modal-id="esop-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">ESOP Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-100percent margin-left-10">
								<select >
									<option value="dept1">ESOP 1</option>
									<option value="dept2">ESOP 2</option>
									<option value="dept3">E-ESOP 1</option>
									<option value="dept4">ESOP 3</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Company:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class=""></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0 ">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CACI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CADPI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ROXOL</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">RHI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Department:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="">All Departments</p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">IICT</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">HR Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Cane Supply Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Medical and Dental Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Trading Options Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Rank:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="">All Ranks</p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Executive</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Senior Manager</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Manager</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">AM</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Generate Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- Claim Summary -->
<div class="modal-container" modal-id="claim-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Claim Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-100percent margin-left-10">
								<select >
									<option value="dept1">ESOP 1</option>
									<option value="dept2">ESOP 2</option>
									<option value="dept3">E-ESOP 1</option>
									<option value="dept4">ESOP 3</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Company:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class=""></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0 ">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CACI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">CADPI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">ROXOL</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">RHI</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Department:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="">All Departments</p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">IICT</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">HR Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Cane Supply Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Medical and Dental Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Trading Options Dept</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Rank:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="">All Ranks</p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div">
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Executive</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Senior Manager</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">Manager</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										<div class="thumb_list_view small text-size-0">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10">AM</p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Generate Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>