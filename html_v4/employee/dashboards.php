<?php include "../construct/header.php"; ?>

<!-- for navigation  -->
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>						
						<li><a href="../employee/personal-stocks.php">Personal Stocks</a></li>
						<li><a href="../employee/stock-offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT	
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="profile-page.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">

	
</section>

<section section-style="content-panel">	

	<div class="content margin-top-40 text-center">			
		<h2 class="font-400 font-styling">Welcome to RHI Employee Stock Options Plan (ESOP) System</h2>

		<h2 class="font-400 margin-top-40 text-center font-styling">Currently, You have no ESOP offered to you.<br>Please wait until your HR department has offered you a plan.
		<h2 class="font-400 margin-top-40 text-center font-styling">Once you have an ESOP offered to you, you can:</h2>
	<div>
	<div class="content">
		<div class="width-300px margin-20px display-inline-top">
			<div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/handshake.png" alt=""  class="icons-dashboard"></div>
			<p class="font-18 white-color font-styling margin-top-20">Accept ESOP Offers</p>
		</div>
		<div class="width-300px margin-20px display-inline-top">
			<div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/Group 3.png" alt=""  class="icons-dashboard"></div>
			<p class="font-18 white-color font-styling margin-top-20">Claim Vesting Rights from the Stock Option Plan</p>
		</div>
		<div class="width-300px margin-20px display-inline-top">
			<div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/1464598092_12.png" alt=""  class="icons-dashboard"></div>
			<p class="font-18 white-color font-styling margin-top-20">View ESOP Statement of Accounts and other documents</p>
		</div>
	</div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>