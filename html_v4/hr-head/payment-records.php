<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">ESOP Payments</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">ESOP Gratuity</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/ESOP-gratuity-list.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../hr-head/REPORTS-downloadable-report.php">Downloadable Reports</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Payment Records</h1>
			
		<div class="f-right">
			<button class="btn-normal margin-right-10">Download Payment Template</button>
			<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Payment Record</button>
			<button class="btn-normal modal-trigger" modal-target="add-payment-record">Add Payment Record</button>				
		</div>
			<div class="clear"></div>
		</div>

		<div class="header-effect">
			<div class="display-inline-mid default">
				<div class="inline-block">
					<div class="select add-radius display-inline-mid margin-right-10">
						<p class="white-color margin-bottom-5">ESOP Name</p>
						<select>
							<option value="ESOP Name">ALL ESOP</option>
							<option value="Vesting Years">ESOP 1</option>
							<option value="Grant Date">ESOP 2</option>
							<option value="Share QTY">E-ESOP 1</option>
							<option value="Share QTY">ESOP 3</option>
							
						</select>
					</div>
					<div class="select add-radius display-inline-mid margin-right-10">
						<p class="white-color margin-bottom-5">Payment Method</p>
						<select>
							<option value="ESOP Name">All</option>
							<option value="Vesting Years">Cash</option>
							<option value="Grant Date">Credit Card</option>
							<option value="Share QTY">Check</option>
							
						</select>
					</div>
					<div class="select add-radius display-inline-mid margin-right-10">
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-25">Search</button>
					</div>
				</div>	
			</div>
		</div>

	</div>
</section>
<section section-style="content-panel">

  <div class="content padding-top-30">
	    <div class="clear"></div>
	    <div class="tbl-rounded margin-top-20 table-content display_on">
	      <table class="table-roxas tbl-display">
	        <thead>
	          <tr>
	            <th>Date Added</th>
	            <th>Employee Name</th>
	            <th>ESOP Name</th>
	            <th>Payment Method</th>
	            <th>Amount</th>
	          </tr>
	        </thead>
	        <tbody>
	          <tr>
	            <td>Sept 10, 2015</td>
	            <td>Jomar Tanael</td>
	            <td>ESOP 1</td>
	            <td>Profit Share</td>
	            <td>Php 500,000.00</td>
	          </tr>
	          <tr>
	            <td>Sept 10, 2015</td>
	            <td>Joselito Salazar</td>
	            <td>ESOP 1</td>
	            <td>Credit Card</td>
	            <td>Php 500,000.00</td>
	          </tr>
	          <tr>
	            <td>Sept 10, 2015</td>
	            <td>Joselito Salazar</td>
	            <td>ESOP 1</td>
	            <td>Cash</td>
	            <td>Php 500,000.00</td>
	          </tr>
	          <tr>
	            <td>Sept 10, 2015</td>
	            <td>Joselito Salazar</td>
	            <td>ESOP 1</td>
	            <td>Cash</td>
	            <td>Php 500,000.00</td>
	          </tr>
	          <tr>
	            <td>Sept 10, 2015</td>
	            <td>Joselito Salazar</td>
	            <td>ESOP 1</td>
	            <td>Check</td>
	            <td>Php 500,000.00</td>
	          </tr>
	        </tbody>
	      </table>
	    </div>
	</div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>			
			<div class=" margin-top-20">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			<a href="payment-preview-of-template.php">			
				<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
			</a>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add RANK -->
<div class="modal-container" modal-id="add-payment-record">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD PAYMENT RECORD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>
			<table class="width-100ppercent center-margin">
				<tbody>
					<tr>
						<td class="padding-top-10">Employee Name :</td>
						<td>
							<div class="modal-content text-center">	
					<div class="display-inline-mid member-selection">
						<div class="">
							<div class="member-container display-inline-mid">
								<p class="">1 Member Selected</p>
								<div class="angle-down">
									<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
								</div>
							</div>
						</div>
						<div class="popup_person_list">
							<div class="display-inline-mid width-100percent text-left font-0">
								<input type="text" class="width-90percent display-inline-mid">
								<div class="sSearch display-inline-mid width-10percent">
									<i class="fa fa-search font-18" aria-hidden="true"></i>
								</div>
							</div>
							<div class="popup_person_list_div">
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Dwayne Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Girl Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Hello Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Hi Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Kapoy Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Katulugon Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Diputa Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Hayup Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
							</div>										
						</div>
					</div>
				</div>
			</div>
						</td>
					</tr>
					<tr>
						<td class="v-top padding-top-15">ESOP Name:</td>
						<td>
							<div class="select margin-left-20 width-300px add-radius">
								<select>
									<option value="op1">ESOP 1</option>
									<option value="op2">ESOP 2</option>
									<option value="op3">ESOP 3</option>
									<option value="op3">E-ESOP 1</option>
									<option value="op3">E-ESOP 1</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="v-top padding-top-15">Payment Method:</td>
						<td>
							<div class="select margin-left-20 width-300px add-radius">
								<select>
									<option value="op1">Cash</option>
									<option value="op2">Credit Card</option>
									<option value="op3">Check</option>
									<option value="op3">Profit Share</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Employee Name :</td>
						<td><input type="text" class="normal margin-left-20 width-150px add-border-radius-5px display-inline-mid"/>
						<div class="select add-radius width-150px display-inline-mid">
							<select>
								<option value="op1">Cash</option>
								<option value="op2">Credit Card</option>
								<option value="op3">Check</option>
								<option value="op3">Profit Share</option>
							</select>
						</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-5">Cancel</button>			
			<a href="ESOP-view-employee.php"><button type="button" class="display-inline-mid btn-normal close-me">Add Payment</button></a>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>