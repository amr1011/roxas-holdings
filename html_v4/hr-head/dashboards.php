<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<section section-style="content-panel">
		<div  class="parent-dashboard">
			<div class="child-dashboard font-0 width-100percent">	
				<h1 class="font-30">Welcome to RHI Employee Stock Options Plan (ESOP) System</h1>
				<h1  class="font-30 padding-top-30">Currently, you have no ESOP offered to you. <br/> Please wait until your HR department has ofered you a plan.</h1>
				<h1  class="font-30 padding-top-30">Once you have an ESOP ofered to you, you can:</h1>
				<div class="images-dashboard font-0">
					<div class="width-100per">
						<div class="width-100per-over-3 display-inline-top">
							<div class="circle">
								<img class="dashboard-icon" src="../assets/images/handshake.png">
							</div>
							<p class="padding-top-20">Accept ESOP Offers</p>

						</div>
						<div class="width-100per-over-3 display-inline-top">
							<div class="circle">
								<img class="dashboard-icon" src="../assets/images/group-3.png">
							</div>
							<p class="padding-top-20">Claim Vesting Rights from <br/> the Stock Option Plan</p>
						</div>
						<div class="width-100per-over-3 display-inline-top">
							<div class="circle">
								<img class="file-copy" src="../assets/images/file-copy.png">
							</div>
							<p class="padding-top-20">View ESOP Statement of Account <br/> and other documents</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>