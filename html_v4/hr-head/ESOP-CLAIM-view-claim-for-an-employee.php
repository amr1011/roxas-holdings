<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP Claims</h1>
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
							<option value="Status">Status</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>

			<div class="display-inline-mid margin-left-10 status">
				<label class="margin-bottom-5 white-color">Status</label>
				<br />
				<div class="select add-radius">
					<select>
						<option value="status1">All</option>
						<option value="status2">Pending</option>
						<option value="status3">Completed</option>
						<option value="status4">Rejected</option>
						<option value="status5">For Finalization</option>						
					</select>
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>
			</div>
			
		</div>
		<div class="text-right-line margin-top-30">
			
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-10">
		<div class="grid-content">
			<h2 class="">CACI</h2>
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price Per Share</th>
						<th>Vesting Year</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
			</tbody></table>




			

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee No.</th>
						<th>Employee Name</th>
						<th>ESOP Name</th>
						<th>Vesting Year</th>
						<th>No. of Shares</th>
						<th>Price per Share</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-pending">Pending</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-final">For Finalization</p></td>
						<td><a href="ESOP-CLAIM-view-claim-finalization.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-complete">Approved</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><a href="color-complete">Approved</a></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-reject">Rejected</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
	<div class="content padding-top-10">
		<div class="grid-content">
			<h2 class="">CAPDI</h2>
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price Per Share</th>
						<th>Vesting Year</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
			</tbody></table>




			

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee No.</th>
						<th>Employee Name</th>
						<th>ESOP Name</th>
						<th>Vesting Year</th>
						<th>No. of Shares</th>
						<th>Price per Share</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-pending">Pending</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-final">For Finalization</p></td>
						<td><a href="ESOP-CLAIM-view-claim-finalization.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-complete">Approved</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><a href="color-complete">Approved</a></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-reject">Rejected</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
	<div class="content padding-top-10">

		<div class="grid-content">
			<h2 class="">ROXOL</h2>
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price Per Share</th>
						<th>Vesting Year</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><>View Offer</td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
					<tr>
						<td>EPSON 1</td>
						<td>Sept. 01, 2013</td>
						<td>500,00 Shares</td>
						<td>Php 6.00</td>
						<td>5 Year</td>
						<td><a href="ESOP-CLAIM-view-for-claiming.php">View Offer</a></td>
					</tr>
			</tbody></table>




			

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee No.</th>
						<th>Employee Name</th>
						<th>ESOP Name</th>
						<th>Vesting Year</th>
						<th>No. of Shares</th>
						<th>Price per Share</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-pending">Pending</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-final">For Finalization</p></td>
						<td><a href="ESOP-CLAIM-view-claim-finalization.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-complete">Approved</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><a href="color-complete">Approved</a></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-reject">Rejected</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>


</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>