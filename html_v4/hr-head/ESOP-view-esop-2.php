<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/ESOP-gratuity-list.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../hr-head/REPORTS-downloadable-report.php">Downloadable Reports</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="ESOP-esop.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>ESOP 1</a>
			</div>
			<div class="f-right">
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="offer-expiration">Set Offer Expiration</button>
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Share Distribution Template</button>
				<a href="ESOP-view-esop-distribute.php">
					<button class="btn-normal margin-right-10 ">Edit Distribute Shares</button>				
				</a>
				<button class="btn-normal modal-trigger" modal-target="edit-esop">Edit ESOP</button>		
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">


		<h2 class="f-left">ESOP 1</h2>
	
		<div class="clear"></div>

		<div class="option-box trio">
			<p class="title">Total Alloted Shares</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">Php 2.49</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">2 Years</p>
		</div>

		<div class="option-box trio">
			<p class="title">No. of Employee Accepted</p>
			<p class="description">56 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Availed</p>
			<p class="description">250,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Unavailed</p>
			<p class="description">250,000 Shares</p>
		</div>

		<div class="text-right-line margin-top-25">
			<div class="line"></div>
		</div>

		<h2 class="f-left margin-top-30">CAPDI.HO</h2>		
		<div class="clear"></div>

		<div class="option-box trio">
			<p class="title">Total Alloted Shares</p>
			<p class="description">125,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">Php 2.49</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">2 Years</p>
		</div>

		<div class="option-box trio">
			<p class="title">No. of Employee Accepted</p>
			<p class="description">2 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Availed</p>
			<p class="description">125,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Unavailed</p>
			<p class="description">125,000 Shares</p>
		</div>

		
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-10 padding-top-30">

			<div class="accordion_custom ">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Employee
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Company Code</th>
									<th>Employee Code</th>
									<th>Department Name</th>
									<th>Employee Name</th>
									<th>Rank / Level</th>
									<th>Alloted Shares</th>
									<th>Total Shares Availed</th>
									<th>Total Amount Availed</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
				
			<div class="text-right-line margin-top-25 ">				
				<div class="line"></div>								
			</div>
			
			<h2 class="f-left margin-top-30">CAPDI.HO</h2>		
			<div class="clear"></div>

			<div class="option-box trio">
				<p class="title">Total Alloted Shares</p>
				<p class="description">250,000 Shares</p>
			</div>
			<div class="option-box trio">
				<p class="title">Price per Share</p>
				<p class="description">Php 2.49</p>
			</div>
			<div class="option-box trio">
				<p class="title">Vesting Years</p>
				<p class="description">1 Years</p>
			</div>

			<div class="option-box trio">
				<p class="title">No. of Employee Accepted</p>
				<p class="description">22 Employees</p>
			</div>
			<div class="option-box trio">
				<p class="title">Total Shared Availed</p>
				<p class="description">125,000 Shares</p>
			</div>
			<div class="option-box trio">
				<p class="title">Total Shared Unavailed</p>
				<p class="description">125,000 Shares</p>
			</div>

			
			<div class="accordion_custom margin-top-30">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Employee
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Company Code</th>
									<th>Employee Code</th>
									<th>Department Name</th>
									<th>Employee Name</th>
									<th>Rank / Level</th>
									<th>Alloted Shares</th>
									<th>Total Shares Availed</th>
									<th>Total Amount Availed</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
							</tbody>
						</table>

					</div>			
				</div>
			</div>	

			<div class="text-right-line margin-bottom-60">
				<div class="line"></div>
			</div>

			<div class="accordion_custom margin-top-30">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Action Description</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name <strong>"ESOP 1"</strong></td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>VALENCIA, RENATO CRUZ</td>
									<td>Edited ESOP 1 Price per Share from <strong>"1.00"</strong> to <strong>"6.00"</strong></td>
								</tr>
							
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	
			<div class="margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>	
			<a href="ESOP-view-esop-2-preview-temp.php">		
				<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
			</a>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- edit esop  -->
<div class="modal-container " modal-id="edit-esop">
	<div class="modal-body small width-600px">
		<div class="modal-head">
			<h4 class="text-left">EDIT ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">	
			<div class="error">Total Share Quantity is Invalid. <br>Please type numbers only in the textbox.</div>	
	
			<table class="send-claim margin-top-10 ">
				<tbody>
					<tr>
						<td class="width-200px">
							<p>ESOP NAME</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-300px" />
						</td>
					</tr>
					<tr>
						<td>
							<p>Grant Date: </p>
						</td>
						<td>
							<div class="date-picker display-inline-mid add-radius ">
								<input type="text" data-date-format="MM/DD/YYYY" class="width-250px">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Total Share Quantity</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-300px display-inline-mid" value="500,000">
							<p class="display-inline-mid">Shares</p>
						</td>
					</tr>	
					<tr>
						<td><p>Price per Share:</p></td>
						<td>
							<input type="text" class="small add-border-radius-5px">
							<div class="select width-100px add-radius margin-left-10">
								<select>
									<option value="Php">PHP</option>
									<option value="usd">USD</option>
									<option value="cad">CAD</option>
									<option value="hkd">HKD</option>
									<option value="inr">INR</option>
								</select>
							</div> 
						</td>
					</tr>			
					<tr>
						<td>
							<p>Vesting Years:</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px display-inline-mid width-300px" />
							<p class="display-inline-mid">Years</p>
						</td>
					</tr>					
				</tbody>
			</table>		

			<div class="margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- offer expiration  -->
<div class="modal-container" modal-id="offer-expiration">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SET OFFER EXPIRATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<div class="modal-content">
			<div class="error">Date below has already passed. Please choose another date.</div>
			<div class="margin-top-20">
				<p class="display-inline-mid margin-left-15">Expiry Date:</p>
				<div class="date-picker display-inline-mid add-radius margin-left-20">
					<input type="text" data-date-format="MM/DD/YYYY" class="width-250px">
					<span class="fa fa-calendar text-center"></span>
				</div>
			</div>
		</div>

		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			<button type="button" class="display-inline-mid btn-dark">Set Offer Expiration</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>