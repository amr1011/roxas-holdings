<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/ESOP-gratuity-list.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../hr-head/REPORTS-downloadable-report.php">Downloadable Reports</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
				
			<div class="clear"></div>
		</div>
		
		<div class="f-right">			
			<button class="btn-normal display-inline-block margin-right-10 ">Archive this Letter</button>			
			<button class="btn-normal display-inline-block modal-trigger " modal-target="edit-letter" id="edit-letter-click">Edit Letter</button>
		</div>
		<div class="clear"></div>

	</div>

</section>

<section section-style="content-panel">

	<div class="content">

		<p class="font-20 white-color">Offer Letter</p>
		<h2 class="margin-bottom-30">Offer Letter v.1 for ROXOL</h2>

			
		<div class="letter-head border-tl-10px border-tr-10px"> 
			<p class="f-left">Date Created: October 30, 2015</p>
			<p class="f-right">Created By: Joselito Salazar</p>
			<div class="clear"></div>
		</div>

		<div class="letter-content border-bl-10px border-br-10px">
			<p class="f-right font-bold">&#60;Date of Letter></p>
			<div class="f-left margin-top-20">
				<p class="font-bold"> &#60;Sender Name></p>
				<p class="font-bold"> &#60;Sender Rank></p>
				<p class="font-bold"> &#60;Sender Department></p>
			</div>
			<div class="clear"></div>

			<p class="margin-top-20">Dear <span class="font-bold">&#60;Recepient's Name>,</span></p>
			<p class="margin-top-10">In accordance with the terms of the Employee Stock Option Plan (<abbr title="
			Employee Stock Option Plan">ESOP</abbr>) of Roxas Holdings,
			Inc. (<abbr title="Roxas Holdings Inc.">RHI</abbr>), which was approved by the Securities and Exchange Commission 
			(<abbr title="Securities and Exchange Commission">SEC</abbr>) on April 30, 2014, 
			the company offers you an option to subscribe and purchange <span class="font-bold"> &#60;Offer Shares> </span>
			shares of RHI which has been allocated to you over the exercise period of <span class="font-bold">
			&#60;Vesting Years> </span> years commencing from the date of this letter offer under the following conditions:
			</p>

			<ol class="margin-top-20">
				<li class="font-bold">Subscription Price &amp; Terms.</li>
				<p>This subscription price is based on the average price of the RHI shares in the 
				Philippine Stock Exchange for thirty(30) trading days prior to the date of this 
				subscription price is <strong>&#60;Price per Share></strong>.</p>
				<li class="font-bold margin-top-20">Vesting of Option.</li>
				<p>The Option will vest as follows:</p>
			</ol>

			<table class="table-offer">
				<thead>
					<tr>
						<th>Vesting Date</th>
						<th>Vesting Option for</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>At the end of one (1) year from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of two (2) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of three (3) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
				</tbody>
			</table>


		</div>

		<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom margin-top-30">

				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>		

				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Shares Offered</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>VALENCIA, RENATO CRUZ</td>
									<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
								</tr>
							
							</tbody>
						</table>

					</div>			
				</div>
			</div>
		</div>

	<div>
</section>

<!-- edit letter -->
<div class="modal-container edit-letter" modal-id="edit-letter">
	<div class="modal-body max-width-1200 width-1200px ">
		<div class="modal-head ">
			<h4 class="text-left">EDIT LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content ">	

			<div class="head">
				<div class="display-inline-mid width-80percent">
					<p class="margin-bottom-5">Title</p>
					<input type="text" class="normal width-100percent add-border-radius-5px" value="Offer Letter v.1 for ROXOL" />
				</div>
				<div class="display-inline-mid margin-left-20">
					<p class="margin-bottom-5">Letter Type:</p>
					<div class="select add-radius">
						<select>
							<option value="op1">Offer Letter A</option>
							<option value="op2">Offer Letter B</option>
							<option value="op3">Offer Letter B</option>
						</select>
					</div>
				</div>
			</div>

			<div class="big-header">
				<p class="f-left margin-left-10 white-color">Date Created: October 30, 2015</p>
				<p class="f-right margin-right-20 white-color">Created By: Joselito Salazar</p>
				<div class="clear"></div>
			</div>

			<div class="big-body">

				<div class="function">
					<!-- left - side -->
					<div class="format f-left  ">			
						<div class="text-files">
							<textarea name="editor1" id="editor1" rows="10" cols="80"></textarea>
						</div>
					</div>							
				</div>

				<div class="letter-nav ">
					<!-- right side  -->

					<div class="tag  ">
						<p class="text-center">TAGS</p>
					</div>					
					
					<div class="menu hover-letter">
						<ul>
							<li>ESOP Name</li>
							<li>Offer Shares</li>
							<li>Price / Share</li>
							<li>Total Value of Share</li>
							<li>Vesting Years</li>
							<li>Date of Letter</li>
							<li>Sender Name</li>
							<li>Sender Rank</li>
							<li>Sender Department</li>
							<li>Recipient Name</li>
							<li>Recipient Rank</li>
							<li>Recipient Department</li>								
							<li></li>								
						</ul>
					</div>

					<div class="clear"></div>

				</div>

			</div>
				
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			
			<button type="button" class="display-inline-mid btn-normal alert-btn">Submit</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>