<?php include "../construct/header.php"; ?>

<header custom-style="header">
  <img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
  <nav>
    <ul>
      <li>
        <a><img src="../assets/images/ui/esop-btn.svg"></a>
        <div class="sub-nav">
          <p>ESOP</p>
          <ul>
            <li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
            <li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
            <li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
          </ul>
        </div>
      </li>
      <li>  
        <a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
        <div class="sub-nav">
          <p>Claims</p>
        </div>
      </li>
      <li>
        <a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
        <div class="sub-nav">
          <p>Companies</p>
        </div>
      </li>
      <li>
        <a ><img src="../assets/images/ui/reports-btn.svg"></a>
        <div class="sub-nav">
          <p>Reports</p>
          <ul>
            <li><a href="../hr-head/ESOP-gratuity-list.php">Gratuity</a></li>
            <li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
            <li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
            <li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
            <li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
            <li><a href="../hr-head/REPORTS-downloadable-report.php">Downloadable Reports</a></li>
          </ul>
        </div>
      </li>
      <li>
        <a><img src="../assets/images/ui/settings-btn.svg"></a>
        <div class="sub-nav">
          <p>Settings</p>
          <ul>
            <li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
            <li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
            <li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </nav>
  <a href="#" class="log">
    LOG OUT
  </a>
  <a href="#" class="bell">
    <i class="fa fa-bell"></i>
  </a>  
  <a href="../esop-admin/PROFILE-PAGE.php" class="profile">
    <img src="../assets/images/profile/profile.jpg" class="img-circle"/>
    <p>Maria Cruz</p>   
    <i class="fa fa-caret-down white-color fa-2x"></i>
  </a>

  
  <div class="clear"></div>
</header>



<section section-style="top-panel">
  <div class="content">
    <div class="header-effect">
      <div class="breadcrumbs margin-bottom-20 border-10px">
        <a href="groupwide_stock_offer.php">Groupwide Stock Offer</a>
        <span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
        <a href="ESOP-view-esop.php">ESOP 1</a>
      </div>
      <div class="display-inline-mid default display-block">
        <p class="white-color margin-bottom-5">Status:</p>
        <div>
          <div class="select add-radius display-inline-mid">
            <select class="select display-inline-block">
              <option value="ESOP Name">ALL</option>
              <option value="Vesting Years">Vesting Years</option>
              <option value="Grant Date">Grant Date</option>
              <option value="Share QTY">Price per Share</option>
            </select>
          </div>
          <div class="display-inline-mid search-me">
            <button class="btn-normal display-inline-mid margin-left-10">Filter List</button>
          </div>
          <div class="f-right display-inline-mid modal-trigger" modal-target="modal_update_offer_list">
            <button class="btn-normal display-inline-mid margin-left-10">Update Offer List</button>
          </div>
          <div class="f-right display-inline-mid modal-trigger" modal-target="modal_update_offer_list">
            <button class="btn-normal display-inline-mid margin-left-10">Download Selected Offer Letter</button>
          </div>
          <div class="f-right display-inline-mid modal-trigger" modal-target="modal_update_offer_list">
            <button class="btn-normal display-inline-mid margin-left-10">Download Selected Acceptance Letter</button>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<section section-style="content-panel">

  <div class="content">
    <h2 class="f-left margin-top-30">CACI</h2>    
    <div class="clear"></div>
    <div class="tbl-rounded margin-top-20 table-content display_on">
      <table class="table-roxas tbl-display">
        <thead>
          <tr>
            <th><input type="checkbox" name="" id=""> </th>
            <th>Name</th>
            <th>Alloted Shares</th>
            <th>Accepted Shares</th>
            <th>Offer Letter</th>
            <th>Acceptance Letter</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><input type="checkbox" name="" id=""> </td>
            <td>Jose Lito Salazar</td>
            <td>500,000 Shares</td>
            <td><label class="black-color font-15">500,000 Shares</label> <input class="display-none" type="text" name="" id="" value="500,000"> </td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="gray-color">Waiting</label></td>
            <td><div class="inline-block"><span class="blue-color display-none">Edit<span></div> <div class="display-inline-block"><span class="red-color display-inlineblock">Cancel</span>|<span class="blue-color display-inlineblock">Save</span></div></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="" id=""> </td>
            <td>Jose Lito Salazar</td>
            <td>500,000 Shares</td>
            <td><label class="black-color font-15">500,000 Shares</label> <input class="display-none" type="text" name="" id="" value="500,000"> </td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="green-color">Accepted</label></td>
            <td><div class="inline-block"><span class="blue-color display-none">Edit<span></div> <div class="display-inline-block"><span class="red-color display-inlineblock">Cancel</span>|<span class="blue-color display-inlineblock">Save</span></div></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="" id=""> </td>
            <td>Jose Lito Salazar</td>
            <td>500,000 Shares</td>
            <td><label class="black-color font-15">500,000 Shares</label> <input class="display-none" type="text" name="" id="" value="500,000"> </td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="green-color">Accepted</label></td>
            <td><div class="inline-block"><span class="blue-color display-none">Edit<span></div> <div class="display-inline-block"><span class="red-color display-inlineblock">Cancel</span>|<span class="blue-color display-inlineblock">Save</span></div></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="" id="" class="display-none"> </td>
            <td>Jose Lito Salazar</td>
            <td>500,000 Shares</td>
            <td><label class="black-color font-15">500,000 Shares</label> <input class="display-none" type="text" name="" id="" value="500,000"> </td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="green-color">Accepted</label></td>
            <td><div class="inline-block"><span class="blue-color">Edit<span></div> <div class="display-none"><span class="red-color display-inlineblock">Cancel</span>|<span class="blue-color display-inlineblock">Save</span></div></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="" id="" class="display-none"> </td>
            <td>Jose Lito Salazar</td>
            <td>500,000 Shares</td>
            <td><label class="black-color font-15">500,000 Shares</label> <input class="display-none" type="text" name="" id="" value="500,000"> </td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="blue-color">Download</label></td>
            <td><label class="green-color">Accepted</label></td>
            <td><div class="inline-block"><span class="blue-color">Edit<span></div> <div class="display-none"><span class="red-color display-inlineblock">Cancel</span>|<span class="blue-color display-inlineblock">Save</span></div></td>
          </tr>
        </tbody>
      </table>
    </div>

    <!-- paganation -->
    <div class="paganation-container">
      <div class="paganation-content">
        <div class="btn-page">Previous</div>
        <div class="btn-page">1</div>
        <div class="btn-page">2</div>
        <div class="btn-page">3</div>
        <div class="btn-page">4</div>
        <div class="btn-page">5</div>
        <div class="btn-page">6</div>
        <div class="btn-page">7</div>
        <div class="btn-page">8</div>
        <div class="btn-page">9</div>
        <div class="btn-page">10</div>
        <div class="btn-page">...</div>
        <div class="btn-page">50</div>
        <div class="btn-page">Next</div>
      </div>
    </div>

</section>

<!-- update offer list -->
<div class="modal-container" modal-id="modal_update_offer_list">
      <div class="modal-body width-600px">
        <div class="modal-head">
          <h4 class="text-left">Update Offer List</h4>
          <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">   
          <div class="success">Offer List Updated</div>

          <br />

          <p>Are you sure you want to update the offer acceptance list of <strong>ESOP 1 for CACI?</strong></p>
          
        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
          <button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>      
          <button type="button" class="display-inline-mid btn-dark close-me">Update List</button>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>