<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">ESOP Payments</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">ESOP Gratuity</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Subscription</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Claiming Report</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Downloadable Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP List</h1>
			<button class="btn-normal f-right margin-top-20 modal-trigger" modal-target="add-esop">Add ESOP</button>
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">ESOP Name</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Grant Date</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Price per Share</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Vesting Year <i class="fa fa-chevron-down"></i></a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">

		<div class="grid-content">

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 2</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 3</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>E-ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ROXAS ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>PESO ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 15</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="ESOP-view-esop.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price per Share</th>
						<th>Vesting Years</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>