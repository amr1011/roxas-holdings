<?php include "../construct/header.php"; ?>
	<section>
		<div class="modal-container showed">
			<div class="modal-body small bg-white">
				<!-- content -->
				<div class="modal-content text-center">	
					<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					<p class="font-25 font-bold">ESOP Management System</p>
					<div class="error margin-top-20 margin-bottom-20">Username and Password do not match. Please try again.</div>

					<div class="margin-bottom-20 margin-top-20">
						<p class="display-inline-mid padding-right-10">Username: </p>
						<input type="text" class="small add-border-radius-5px width-300px display-inline-mid">
					</div>
					<div class="margin-bottom-20 margin-top-20">
						<p class="display-inline-mid padding-right-10">Password: </p>
						<input type="password" class="small add-border-radius-5px width-300px display-inline-mid">
					</div>
					<div class="margin-bottom-20 margin-top-20 text-left padding-left-100 margin-left-15">
						<a href="forgot-password.php" class="">Can't Remember Password?</a>
					</div>
					<div class="margin-bottom-10">
						<button class="btn-normal display-inline-mid margin-left-10">Login</button>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include "../construct/footer.php"; ?>