<?php include "../construct/header.php"; ?>
	<!--reset password-->
		<div class="modal-container showed" modal-id="admin-reset-password">
			<div class="modal-body small bg-white">
				<!-- content -->
				<div class="modal-content text-center">	
					<div class="text-center">
						<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					</div>
					<p class="font-25 font-bold">ESOP Management System</p>
					<div class="success margin-bottom-20 margin-top-20">You admin has reset your password. Please change it now to prevent anyone in accessing your account.</div>
					<div class="margin-bottom-20 margin-top-20 text-left margin-left-20">
						<p class="display-inline-mid padding-right-34">New Password: </p>
						<input type="text" class="small add-border-radius-5px width-250px display-inline-mid">
					</div>
					<div class="margin-bottom-20 margin-top-20 text-left margin-left-20">
						<p class="display-inline-mid padding-right-10">Confirm Password: </p>
						<input type="text" class="small add-border-radius-5px width-250px display-inline-mid">
					</div>
					<div class="margin-bottom-10 text-center">
						<button class="btn-normal display-inline-mid margin-left-10 modal-trigger width-150px close-me" modal-target="admin-email-sent">Submit</button>
					</div>
				</div>
			</div>
		</div>
		<!--reset password-->

		<!--Email Sent-->
		<div class="modal-container" modal-id="admin-email-sent">
			<div class="modal-body small bg-white">
				<!-- content -->
				<div class="modal-content width-90percent margin-auto text-center">	
					<div class="text-center">
						<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					</div>
					<p class="font-25 font-bold">ESOP Management System</p>
					<div class="success margin-bottom-20 margin-top-20">You password has been reset. Click the button below to proceed.</div>
					<div class="margin-bottom-10 text-center">
						<button class="btn-normal display-inline-mid margin-left-10 modal-trigger width-150px close-me" modal-target="">OK</button>
					</div>
				</div>
			</div>
		</div>
		<!--Email Sent-->

<?php include "../construct/footer.php"; ?>