<?php include "../construct/header.php"; ?>
	<section>
		<!--cant remember password-->
		<div class="modal-container showed">
			<div class="modal-body small bg-white ">
				<!-- content -->
				<div class="modal-content width-90percent margin-auto text-center">	
					<div class="text-center">
						<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					</div>
					<p class="font-25 font-bold">ESOP Management System</p>
					<p class="margin-top-20 margin-bottom-20 text-left margin-left-20">Please enter your email for Password Reset:</p>
					<div class="margin-bottom-20 margin-top-20 text-left margin-left-20">
						<p class="display-inline-mid padding-right-10">Email: </p>
						<input type="text" class="small add-border-radius-5px width-300px display-inline-mid">
					</div>
					<div class="margin-bottom-10 text-center">
						<button class="btn-normal display-inline-mid margin-left-10 modal-trigger close-me" modal-target="email-sent">Reset Password</button>
					</div>
				</div>
			</div>
		</div>
		<!--cant remember password-->

		<!--Email Sent-->
		<div class="modal-container" modal-id="email-sent">
			<div class="modal-body small bg-white">
				<!-- content -->
				<div class="modal-content width-90percent margin-auto text-center">	
					<div class="text-center">
						<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					</div>
					<p class="font-25 font-bold">ESOP Management System</p>
					<div class="success margin-bottom-20 margin-top-20">An email has been sent to your email for password reset.</div>
					<div class="margin-bottom-10 text-center">
						<button class="btn-normal display-inline-mid margin-left-10 modal-trigger width-150px close-me" modal-target="email-template">OK</button>
					</div>
				</div>
			</div>
		</div>
		<!--Email Sent-->



		<!--Email Template-->
		<div class="modal-container" modal-id="email-template">
			<div class="modal-body medium bg-white">
				<!-- content -->
				<div class="modal-content text-left">	
					<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					<div class="margin-bottom-20 margin-top-20">
						<p class="font-20 font-bold margin-bottom-20">Hello Jose, </p>
						<p>We've received a request from you to reset your password. If you didn't make the request, just ignore this email. Otherwise, you can reset your password by clicking this link:</p>
					</div>
					<div class="margin-bottom-20 margin-top-20 text-center">
						<button class="btn-normal display-inline-mid width-300px modal-trigger close-me" modal-target="reset-password">Reset Password</button>
					</div>
					<div class="margin-bottom-10">
						<p class="margin-bottom-20">If you have anymore concerns, kindly contact your HR Officer.</p>
						<p class="">Thank You,<br> RHI ESOP Team</p>
					</div>
				</div>
			</div>
		</div>
		<!--Email Template-->

		<!--reset password-->
		<div class="modal-container" modal-id="reset-password">
			<div class="modal-body small bg-white">
				<!-- content -->
				<div class="modal-content text-center">	
					<div class="text-center">
						<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					</div>
					<p class="font-25 font-bold">ESOP Management System</p>
					<div class="success margin-bottom-20 margin-top-20">You have successfully reset your password. Please indicate a new password to prevent anyone in accessing your account.</div>
					<div class="margin-bottom-20 margin-top-20 text-left margin-left-20">
						<p class="display-inline-mid padding-right-34">New Password: </p>
						<input type="text" class="small add-border-radius-5px width-250px display-inline-mid">
					</div>
					<div class="margin-bottom-20 margin-top-20 text-left margin-left-20">
						<p class="display-inline-mid padding-right-10">Confirm Password: </p>
						<input type="text" class="small add-border-radius-5px width-250px display-inline-mid">
					</div>
					<div class="margin-bottom-10 text-center">
						<button class="btn-normal display-inline-mid margin-left-10 modal-trigger width-150px close-me" modal-target="password-complete">Submit</button>
					</div>
				</div>
			</div>
		</div>
		<!--reset password-->

		<!--password complete-->
		<div class="modal-container" modal-id="password-complete">
			<div class="modal-body small bg-white">
				<!-- content -->
				<div class="modal-content width-90percent margin-auto text-center">	
					<div class="text-center">
						<img class="w-logo" src="../assets/images/roxas-holdings-logo.png" class="">
					</div>
					<p class="font-25 font-bold">ESOP Management System</p>
					<div class="success margin-bottom-20 margin-top-20">You password has been reset. Click the button below to proceed.</div>
					<div class="margin-bottom-10 text-center">
						<button class="btn-normal display-inline-mid margin-left-10 modal-trigger width-150px close-me" modal-target="">OK</button>
					</div>
				</div>
			</div>
		</div>
		<!--password complete-->
	</section>
<?php include "../construct/footer.php"; ?>