<?php include "../construct/header.php"; ?>
<section>
		<div class="modal-container showed">
			<div class="modal-body small bg-white">
				<!-- content -->
				<div class="modal-content text-center">	
					<div class="display-inline-mid member-selection">
						<div class="for-selection">
							<div class="member-container display-inline-mid">
								<p class="">1 Member Selected</p>
								<div class="angle-down">
									<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
								</div>
							</div>
						</div>
						<div class="popup_person_list">
							<div class="display-inline-mid width-100percent text-left font-0">	
								<input type="text" class="width-90percent display-inline-mid font-15 padding-left-10">
								<div class="sSearch display-inline-mid width-10percent">
									<i class="fa fa-search font-18" aria-hidden="true"></i>
								</div>
							</div>
							<div class="popup_person_list_div">
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Dwayne Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Girl Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Hello Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Hi Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Kapoy Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Katulugon Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Diputa Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
								<div class="thumb_list_view small text-size-0">
									<div class="thumb_list_inner display-inline-mid width-90percent">
										<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
										<div class="profile display-inline-mid">
											<p class="profile_name font-15 padding-left-10">Hayup Garcia</p>														
										</div>
									</div>
									<div class="check width-10percent">
										<i class="fa fa-check font-18" aria-hidden="true"></i>
									</div>

								</div>
							</div>										
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php include "../construct/footer.php"; ?>