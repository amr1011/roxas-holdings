<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Employee Payment Report</h1>			
			<div class="clear"></div>

		</div>
		<div>
			<p class="white-color margin-bottom-5 margin-top-20 ">Please Indicate Date Range</p>
			<div>
				<label class="display-inline-mid ">From</label>
				<div class="date-picker add-radius display-inline-mid margin-left-10">
					<input type="text" data-date-format="MM/DD/YYYY">
					<span class="fa fa-calendar text-center"></span>
				</div>
				<label class="display-inline-mid margin-left-10">To</label>
				<div class="date-picker add-radius display-inline-mid margin-left-10">
					<input type="text" data-date-format="MM/DD/YYYY">
					<span class="fa fa-calendar text-center"></span>
				</div>
				<a href="REPORTS-ep-generated-report.php">
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</a>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
	
	
		<div class="calendar-icon">
			<i class="fa fa-calendar"></i>
			<h2 class="margin-top-50">Please Supply the Date Range to Generate the Report</h2>
		</div>	
		
	<div>
</section>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>