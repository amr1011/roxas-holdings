<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>
<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP Claims</h1>
			<a href="ESOP-CLAIM-for-employee.php"><button class="btn-normal display-inline-mid margin-left-10 f-right margin-top-15">Claim for an Employee</button></a>
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
							<option value="Status">Status</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>

			<div class="display-inline-mid margin-left-10 status">
				<label class="margin-bottom-5 white-color">Status</label>
				<br />
				<div class="select add-radius">
					<select>
						<option value="status1">All</option>
						<option value="status2">Pending</option>
						<option value="status3">Completed</option>
						<option value="status4">Rejected</option>
						<option value="status5">For Finalization</option>						
					</select>
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>
			</div>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Grant Date</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Price per Share</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">ESOP Name</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Vesting Years <i class="fa fa-chevron-down"></i></a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content padding-top-30">
		<div class="grid-content">
			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-pending font-15 font-bold padding-right-20">PENDING</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-final font-15 font-bold padding-right-20">FOR FINALIZATION</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim-finalization.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-complete font-15 font-bold padding-right-20">COMPLETED</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-complete font-15 font-bold padding-right-20">COMPLETE</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-reject font-15 font-bold padding-right-20">REJECTED</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="data-box data-two divide-by-2">
				<table class="width-100per">
					<tbody>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
							<td colspan="2" class="text-right color-reject font-15 font-bold padding-right-20">REJECTED</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
							<td>ESOP 1</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">Year 1</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
							<td>25,000 Shares</td>
							<td class="text-right">Price per Share:</td>
							<td class="text-right">Php 6.00 </td>
						</tr>
					</tbody>
				</table>
				
				<div class="data-hover text-center">
					<a href="ESOP-CLAIM-view-claim.php">
						<button class="btn-normal">View Claim</button>
					</a>
				</div>
			</div>

			<div class="paganation-container">
				<div class="paganation-content">
					<div class="btn-page">Previous</div>
					<div class="btn-page">1</div>
					<div class="btn-page">2</div>
					<div class="btn-page">3</div>
					<div class="btn-page">4</div>
					<div class="btn-page">5</div>
					<div class="btn-page">6</div>
					<div class="btn-page">7</div>
					<div class="btn-page">8</div>
					<div class="btn-page">9</div>
					<div class="btn-page">10</div>
					<div class="btn-page">...</div>
					<div class="btn-page">50</div>
					<div class="btn-page">Next</div>
				</div>
			</div>
		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee No.</th>
						<th>Employee Name</th>
						<th>ESOP Name</th>
						<th>Vesting Year</th>
						<th>No. of Shares</th>
						<th>Price per Share</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-pending">Pending</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-final">For Finalization</p></td>
						<td><a href="ESOP-CLAIM-view-claim-finalization.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-complete">Approved</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><a href="color-complete">Approved</a></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>25,000 Shares</td>
						<td>Php 6.00</td>
						<td><p class="color-reject">Rejected</p></td>
						<td><a href="ESOP-CLAIM-view-claim.php">View claim</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>