<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
</section>

<section section-style="content-panel">	
	

	<div class="content ">
			
		<div class="margin-bottom-20 margin-top-20">
			<h2>Welcome, <span>Maria Cruz</span></h2>
			<p class="font-20 white-color">CAPDI. HO</p>
			
			<div class="f-left width-100per">
				<table class="width-100per">
					<tbody>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Price Per Share</p>
									<p class="description">Php 2.49</p>
								</div>
							</td>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Alloted Shares</p>
									<p class="description">15,000.00 Shares</p>
								</div>
							</td>
							<td rowspan="2" class="width-400px">
								<div class="with-txt">
									<div class="svg-cont chart-container">
										<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

										
										<div class="data-container">
											<span data-value="70%">Total Shares Availed</span>										
											<span data-value="30%">Total Shares Unavailed</span>										
										</div>		
										<div class="graph-txt">
											<p class="lower">30 %</p>
											<p class="higher">70 %</p>
										</div>													
									</div>
													
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shared Availed</p>
									<p class="description">12,500,000.00 Shares</p>
								</div>
							</td>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shares Unavailed</p>
									<p class="description">50,000,000 Shares</p>
								</div>
							</td>
						</tr>
					</tbody>


				</table>
				
			</div>
			
			<div class="clear"></div>

			<div class="text-right-line margin-top-30 margin-bottom-70">
				<div class="line"></div>
			</div>
			
		
				

		</div>

		<div class="tab-panel">
			
			<input type="radio" name="tabs" id="toggle-tab1" checked="checked" />
			<label for="toggle-tab1">ESOP 1</label>

			<input type="radio" name="tabs" id="toggle-tab2" />
			<label for="toggle-tab2">ESOP 2</label>

			<input type="radio" name="tabs" id="toggle-tab3" />
			<label for="toggle-tab3">ESOP 3</label>


			<div id="tab1" class="tab">

				<h2 class="margin-top-35 black-color">Total of ESOP 1</h2>				
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Price per Share</th>
								<th>Total Alloted Shares</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Unavailed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Php 2.49</td>
								<td>30,000,000 Shares</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tbl-rounded margin-top-30">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Employee Code</th>
								<th>Employee Name</th>
								<th>Rank / Level</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Availed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0001</td>
								<td>Joselito Salazar</td>
								<td>Chairman</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0002</td>
								<td>Aaron Paul Labing-Lima</td>
								<td>President</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0003</td>
								<td>Eric Nilo</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0004</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0005</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							
							
						</tbody>
					</table>
				</div>

			</div>

			<div id="tab2" class="tab">
			
				<h2 class="margin-top-35 black-color">Total of ESOP 1</h2>				
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Price per Share</th>
								<th>Total Alloted Shares</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Unavailed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Php 2.49</td>
								<td>30,000,000 Shares</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tbl-rounded margin-top-30">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Employee Code</th>
								<th>Employee Name</th>
								<th>Rank / Level</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Availed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0001</td>
								<td>Joselito Salazar</td>
								<td>Chairman</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0002</td>
								<td>Aaron Paul Labing-Lima</td>
								<td>President</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0003</td>
								<td>Eric Nilo</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0004</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0005</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							
							
						</tbody>
					</table>
				</div>

		
			</div>

			<div id="tab3" class="tab">

				<h2 class="margin-top-35 black-color">Total of ESOP 1</h2>				
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Price per Share</th>
								<th>Total Alloted Shares</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Unavailed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Php 2.49</td>
								<td>30,000,000 Shares</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tbl-rounded margin-top-30">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Employee Code</th>
								<th>Employee Name</th>
								<th>Rank / Level</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Availed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0001</td>
								<td>Joselito Salazar</td>
								<td>Chairman</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0002</td>
								<td>Aaron Paul Labing-Lima</td>
								<td>President</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0003</td>
								<td>Eric Nilo</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0004</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0005</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							
							
						</tbody>
					</table>
				</div>

			</div>

		</div>

	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>