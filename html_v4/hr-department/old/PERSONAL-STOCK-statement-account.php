<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="PERSONAL-STOCK.php">Personal Stocks</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="PERSONAL-STOCK-view.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>Statement of Account</a>
			</div>			
			<div class="clear"></div>
		</div>
		<button class="btn-normal f-right">Print Statement of Account</button>
		<div class="clear"></div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content print-statement">

		<div class="user-profile margin-bottom-30">
			<div class="margin-bottom-20 margin-top-20">
				<h2><abbr title="Employee Stock Option Plan">ESOP</abbr> 1 Statement of Account</h2>
			</div>
			<div class="user-name">
				<p class="name display-block">Joselito Salazar</p>
				<p class="position">Employee No: <span>132</span></p>
			</div>
			<div class="user-info">
				<table>
					<tbody>
						<tr>
							<td>Company</td>
							<td>ROXOL</td>
						</tr>
						<tr>
							<td>Department</td>
							<td>ISD</td>
						</tr>
						<tr>
							<td>Rank:</td>
							<td>Officer I</td>
						</tr>
						<tr>
							<td>Separation Status: </td>
							<td>N/A</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="option-box">
			<p class="title">Grant Started</p>
			<p class="description">August 31, 2013</p>
		</div>
		<div class="option-box">
			<p class="title">Subscription Price</p>
			<p class="description">PHP 6.00 </p>
		</div>
		<div class="option-box">
			<p class="title">No. of Shares Availed</p>
			<p class="description">130,144</p>
		</div>
		<div class="option-box">
			<p class="title">Value of Shares Availed</p>
			<p class="description">Php 324,058.56 </p>
		</div>

		<h2 class="margin-top-35">Vesting Rights</h2>


		<div class="tbl-rounded">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>No.</th>
						<th>Year</th>
						<th>Percentage</th>
						<th>No. of Shares</th>
						<th>Value of Shares</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content">					
						<td colspan="2"></td>
						<td class="combine"><span class="total-text">Total:</span> 100%</td>
						<td>130,144</td>
						<td>Php 324,058.56 </td>
					</tr>
				</tbody>
			</table>
		</div>
		<h2 class="margin-top-35">Summary</h2>

		<div class="long-panel border-10px">
			<p class="first-text"><strong>Amount Shares Taken:</strong> <span class="margin-left-10">130,144 | @ 2.40</span></p>
			<p class="second-text margin-right-50">Php 324, 058.56 </p>
			<div class="clear"></div>
		</div>
		
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th class="width-250px">No.</th>
						<th>Payment Details</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="3" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>

		<h2 class="margin-top-35">Payment Application</h2>
		
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display remove-bottom">
				<thead>
					<tr>
						<th colspan="5">
							<p class="f-left margin-left-10 font-20">YEAR 1 - SEPTEMBER 01, 2013</p>
							
							<div class="clear"></div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5">
							<p class="f-left margin-left-10 font-15">Amount Shares Taken <span class="margin-left-30">26,028</span></p>
							<p class="f-right margin-right-50 font-18">64,809.72 Php</p>
							<div class="clear"></div>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<p class="text-left font-bold margin-left-10">Less Payment</p>
						</td>
					</tr>
					<tr>
						<td class="width-150px">					
							<p>No</p>
						</td>
						<td class="width-150px">Payment Date</td>
						<td class="">Payment Mode</td>
						<td class="">Amount</td>
						<td class=" width-400px">Payment Details</td>
					</tr>

					<tr>
						<td>1</td>
						<td>October 2, 2013</td>
						<td>Gratuity</td>
						<td>7,808.64 Php</td>
						<td>Gratuity @.6 / Share</td>
					</tr>

					<tr>
						<td colspan="4"></td>
						<td >
							<p class="display-inline-mid ">Total Payments:</p>
							<p class="display-inline-mid margin-left-10">7,808.64 Php</p>
						</td>
					</tr>			
				</tbody>
			</table>
		</div>


		<div class="bg-last-content padding-10px text-right add-border-bottom">
			<div class=" white-color text-left margin-right-100">
				<p class=" f-left font-bold margin-left-20">Outstanding Balance: (For Year 1) </p>
				<p class="f-right white-color margin-right-40 ">Php 18,399.36 </p>	
				<div class="clear"></div>
			</div>
			
		</div>

		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display remove-bottom">
				<thead>
					<tr>
						<th colspan="5">
							<p class="f-left margin-left-10 font-20">YEAR 2 - SEPTEMBER 01, 2013</p>
							
							<div class="clear"></div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5">
							<p class="f-left margin-left-10 font-15">Amount Shares Taken <span class="margin-left-35">26,028</span></p>
							<p class="f-right margin-right-50 font-18">64,809.72 Php</p>
							<div class="clear"></div>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<p class="text-left font-bold margin-left-10">Less Payment</p>
						</td>
					</tr>
					<tr>
						<td class="width-150px">					
							<p>No</p>
						</td>
						<td class="width-150px">Payment Date</td>
						<td class="">Payment Mode</td>
						<td class="">Amount</td>
						<td class=" width-400px">Payment Details</td>
					</tr>

					<tr>
						<td>1</td>
						<td>October 2, 2013</td>
						<td>Gratuity</td>
						<td>7,808.64 Php</td>
						<td>Gratuity @.6 / Share</td>
					</tr>

					<tr>
						<td colspan="4"></td>
						<td >
							<p class="display-inline-mid ">Total Payments:</p>
							<p class="display-inline-mid margin-left-10">7,808.64 Php</p>
						</td>
					</tr>			
				</tbody>
			</table>
		</div>

		
		<div class="bg-last-content padding-10px text-right add-border-bottom">
			<div class="white-color text-left margin-right-100">
				<p class=" f-left font-bold margin-left-20">Outstanding Balance: (For Year 2) </p>
				<p class="f-right white-color margin-right-40 ">Php 18,399.36 </p>	
				<div class="clear"></div>
				
			</div>
			
		</div>

	<div>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>