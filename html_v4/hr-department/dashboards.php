<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>
<section>
	<section section-style="content-panel">  
	  <div class="content tect-center margin-top-40 text-center">      
	    <h2 class="font-400 font-styling">Welcome to RHI Employee Stock Options Plan (ESOP) System</h2>

	    <h2 class="font-400 font-styling margin-top-40 text-center">Currently, you have no ESOP offered to you. <br/> Please wait until your HR department has ofered you a plan. </h2>
	    <h2 class="font-400 margin-top-40 text-center font-styling">Once you have an ESOP ofered to you, you can:</h2>
	  <div>
	  <div class="content">
	    <div class="display-inline-mid width-300px margin-20px">
	      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/handshake.png" alt=""  class="icons-dashboard"></div>
	      <p class="font-18 white-color margin-top-20">Distribute ESOP Shares</p>
	    </div>
	    <div class="display-inline-mid width-300px margin-20px">
	      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/group-3.png" alt=""  class="icons-dashboard"></div>
	      <p class="font-18 white-color margin-top-20">Manage Employee ESOP Claims</p>
	    </div>
	    <div class="display-inline-mid width-300px margin-20px">
	      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/file.png" alt=""  class="icons-dashboard"></div>
	      <p class="font-18 white-color margin-top-20">View ESOP Statement of Accounts and other documents of Employees</p>
	    </div>
	  </div>
</section>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>