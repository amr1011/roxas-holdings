<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>
<section section-style="top-panel">
	<div class="content">
		<div>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="ESOP-CLAIM-hr.php">ESOP Claims</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="ESOP-CLAIM-for-employee.php">Claim for an Employee</a>				
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>Claim for an Employee</a>
			</div>

			<h1 class="f-left">Claim for an Employee</h1>
			<button class="btn-normal display-inline-mid margin-left-10 f-right margin-top-15 modal-trigger" modal-target="claim-selected-vesting-right">Claim Selected Vesting Rights</button>
			<div class="clear"></div>
		</div>

		<div class="header-effect">
			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
							<option value="Status">Status</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>

			<div class="display-inline-mid margin-left-10 status">
				<label class="margin-bottom-5 white-color">Status</label>
				<br />
				<div class="select add-radius">
					<select>
						<option value="status1">All</option>
						<option value="status2">Pending</option>
						<option value="status3">Completed</option>
						<option value="status4">Rejected</option>
						<option value="status5">For Finalization</option>						
					</select>
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>
			</div>
			
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content padding-top-30">
		<table class="table-roxas tbl-display">
			<thead>
				<tr>
					<th><input type="checkbox"/></th>
					<th>Name</th>
					<th>Department</th>
					<th>Rank</th>
					<th>Accepted Shares</th>
					<th>Years with Vesting Rights</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><input type="checkbox"/></td>
					<td>Juan De la Cruz</td>
					<td>Mills Dept</td>
					<td>Executive</td>
					<td>100,000 Shares</td>
					<td>
						<div class="select add-radius display-inline-mid">
							<select>
								<option value="year 1">Year 1</option>
								<option value="year 2">Year 2</option>
								<option value="year 3">Year 3</option>
								<option value="year 4">Year 4</option>
								<option value="year 5">Year 5</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="checkbox"/></td>
					<td>Juan De la Cruz</td>
					<td>Mills Dept</td>
					<td>Executive</td>
					<td>100,000 Shares</td>
					<td>
						<div class="select add-radius display-inline-mid">
							<select>
								<option value="year 1">Year 1</option>
								<option value="year 2">Year 2</option>
								<option value="year 3">Year 3</option>
								<option value="year 4">Year 4</option>
								<option value="year 5">Year 5</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="checkbox"/></td>
					<td>Juan De la Cruz</td>
					<td>Mills Dept</td>
					<td>Executive</td>
					<td>100,000 Shares</td>
					<td>
						<div class="select add-radius display-inline-mid">
							<select>
								<option value="year 1">Year 1</option>
								<option value="year 2">Year 2</option>
								<option value="year 3">Year 3</option>
								<option value="year 4">Year 4</option>
								<option value="year 5">Year 5</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="checkbox"/></td>
					<td>Juan De la Cruz</td>
					<td>Mills Dept</td>
					<td>Executive</td>
					<td>100,000 Shares</td>
					<td>
						<div class="select add-radius display-inline-mid">
							<select>
								<option value="year 1">Year 1</option>
								<option value="year 2">Year 2</option>
								<option value="year 3">Year 3</option>
								<option value="year 4">Year 4</option>
								<option value="year 5">Year 5</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="checkbox"/></td>
					<td>Juan De la Cruz</td>
					<td>Mills Dept</td>
					<td>Executive</td>
					<td>100,000 Shares</td>
					<td>
						<div class="select add-radius display-inline-mid">
							<select>
								<option value="year 1">Year 1</option>
								<option value="year 2">Year 2</option>
								<option value="year 3">Year 3</option>
								<option value="year 4">Year 4</option>
								<option value="year 5">Year 5</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="checkbox"/></td>
					<td>Juan De la Cruz</td>
					<td>Mills Dept</td>
					<td>Executive</td>
					<td>100,000 Shares</td>
					<td>
						<div class="select add-radius display-inline-mid">
							<select>
								<option value="year 1">Year 1</option>
								<option value="year 2">Year 2</option>
								<option value="year 3">Year 3</option>
								<option value="year 4">Year 4</option>
								<option value="year 5">Year 5</option>
							</select>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	<div>
</section>

<div class="modal-container" modal-id="claim-selected-vesting-right">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Claim Vesiting Rights</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">	
			<div class="success">Vesting Rights has been claimed successfully.</div>
			<p class=" font-bold">Are you sure you want to claim the selected vestign rights of <strong>ESOP 1</strong>?</p>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal">Claim Vesting Right</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>