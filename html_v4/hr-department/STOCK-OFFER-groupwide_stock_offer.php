<?php include "../construct/header.php"; ?>

<header custom-style="header">
  <img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
  <nav>
    <ul>
      <li>
        <a><img src="../assets/images/ui/esop-btn.svg"></a>
        <div class="sub-nav">
          <p>ESOP</p>
          <ul>
            <li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
            <li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
            <li><a href="../esop-admin/STOCK-OFFER-groupwide_stock_offer.php">Stock Offers</a></li>
          </ul>
        </div>
      </li>
      <li>  
        <a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
        <div class="sub-nav">
          <p>Claims</p>
        </div>
      </li>
      <li>
        <a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
        <div class="sub-nav">
          <p>Companies</p>
        </div>
      </li>
      <li>
        <a ><img src="../assets/images/ui/reports-btn.svg"></a>
        <div class="sub-nav">
          <p>Reports</p>
          <ul>
            <li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
            <li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
            <li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
            <li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
            <li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
          </ul>
        </div>
      </li>
      <li>
        <a><img src="../assets/images/ui/settings-btn.svg"></a>
        <div class="sub-nav">
          <p>Settings</p>
          <ul>
            <li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
            <li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
            <li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </nav>
  <a href="#" class="log">
    LOG OUT
  </a>
  <a href="#" class="bell">
    <i class="fa fa-bell"></i>
  </a>  
  <a href="../esop-admin/PROFILE-PAGE.php" class="profile">
    <img src="../assets/images/profile/profile.jpg" class="img-circle"/>
    <p>Maria Cruz</p>   
    <i class="fa fa-caret-down white-color fa-2x"></i>
  </a>

  
  <div class="clear"></div>
</header>



<section section-style="top-panel">
  <div class="content">
    <div>
      <h1 class="f-left">Groupwide Stock Offers</h1>
      <div class="clear"></div>
    </div>

    <div class="header-effect">

      <div class="display-inline-mid default">
        <p class="white-color margin-bottom-5">Search</p>
        <div>
          <div class="select add-radius display-inline-mid">
            <select>
              <option value="ESOP Name">ESOP Name</option>
              <option value="Vesting Years">Vesting Years</option>
              <option value="Grant Date">Grant Date</option>
              <option value="Share QTY">Price per Share</option>
            </select>
          </div>
          <div class="display-inline-mid search-me">
            <input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
            <button class="btn-normal display-inline-mid margin-left-10">Search</button>
          </div>
          <div class="display-inline-mid vesting-years">
            <input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
            <button class="btn-normal display-inline-mid margin-left-10">Search</button>
          </div>
        </div>
      </div>

      <!-- <div class="text-right-line margin-top-25 margin-bottom-30"> -->
      </div>

  </div>
</section>
<section section-style="content-panel">

  <div class="content padding-top-30">
    <h2 class="f-left margin-top-30">CACI</h2>    
    <div class="clear"></div>
    <div class="tbl-rounded margin-top-20 table-content display_on">
      <table class="table-roxas tbl-display">
        <thead>
          <tr>
            <th>Name</th>
            <th>Date Granted</th>
            <th>Total Share Quantity</th>
            <th>Price per Share</th>
            <th>Vesting Years</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="text-right-line margin-top-25 margin-bottom-30">
      <div class="line"></div>
    </div>
    <h2 class="f-left margin-top-30">CAPDI</h2>    
    <div class="clear"></div>
    <div class="tbl-rounded margin-top-20 table-content display_on">
      <table class="table-roxas tbl-display">
        <thead>
          <tr>
            <th>Name</th>
            <th>Date Granted</th>
            <th>Total Share Quantity</th>
            <th>Price per Share</th>
            <th>Vesting Years</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="text-right-line margin-top-25 margin-bottom-30">
      <div class="line"></div>
    </div>
    

    <h2 class="f-left margin-top-30">ROXOL</h2>    
    <div class="clear"></div>
    <div class="tbl-rounded margin-top-20 table-content display_on">
      <table class="table-roxas tbl-display">
        <thead>
          <tr>
            <th>Name</th>
            <th>Date Granted</th>
            <th>Total Share Quantity</th>
            <th>Price per Share</th>
            <th>Vesting Years</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
          <tr>
            <td>ESOP 1</td>
            <td>Sept 10, 2015</td>
            <td>500,000 Shares</td>
            <td>Php 6.00</td>
            <td>5 Years</td>
            <td><a href="STOCK-OFFER-view_groupwide_stock_offer.php">View Offer</a></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="text-right-line margin-top-25 margin-bottom-30">
      <div class="line"></div>
    </div>

  </div>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>