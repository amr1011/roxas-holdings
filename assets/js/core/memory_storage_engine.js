/*
 * "Giraffe Tools" - cr8vwebsolutions's JS framework
 * memory_storage_engine.js - an extension that allows storing data as an array in a hash table
 *
 * Version: 0.0.1
 * Date Started: November 12, 2012
 * Last Update: November 12, 2012
 *
 * Copyright (c) 2012 Chuck Cerrillo (chuck@cr8vwebsolutions.com)
 * JSLint Valid (http://www.jslint.com/)
 *
 */
/*jslint plusplus: true, evil: true */
/*global jQuery:true */

(function () {
	"use strict";
	//  AJAX
	// This is the primitive ajax function that will be used by the other functions
	var data = {};
	function check_uiobject_data (oFilter, oSettings) {
		var bResult = false,
			x = 0;
		if (oSettings && oFilter) {
			if (oSettings.data && oSettings.data[0]) {
				bResult = true;
				for (x in oFilter) {
					if (oFilter.hasOwnProperty(x)) {
						if (oFilter[x] !== oSettings.data[0][x]) {
							bResult = false;
							break;
						}
					}
				}
			} else if (oSettings.data) {
				bResult = true;
				for (x in oFilter) {
					if (oFilter.hasOwnProperty(x)) {
						if (oFilter[x] !== oSettings.data[x]) {
							bResult = false;
							break;
						}
					}
				}
			}
		}
		return bResult;
	}
	CPlatform.prototype.db = {
		dump: function () {
			return data;
		},
		show_tables: function () {
			var x,
				arResult = [];
			for (x in data) {
				if (data.hasOwnProperty(x)) {
					arResult.push(x);
				}
			}
			return arResult;
		},
		count: function (oObjects) {
			var iCount = 0,
				x = 0;
			for (x in oObjects) {
				if (oObjects.hasOwnProperty(x)) {
					iCount++;
				}
			}
			return iCount;
		},
		add_all: function (oValues, sTable) {
			if (typeof sTable == 'undefined') {
				sTable = '';
			}
			if (sTable.length > 0) {
				if (!data[sTable] || data[sTable] === 'undefined') {
					data[sTable] = [];
				}
				
				// data[sTable][this.count(data[sTable])] = oValues;
				data[sTable] = oValues;
				/*console.log(data[sTable]);*/
				// console.log(sTable+'=='+JSON.stringify(data[sTable]) +'--'+ JSON.stringify(oValues));

				return true;
			}
			return false;
		},
		add: function (oValues, sTable) {
			if (typeof sTable == 'undefined') {
				sTable = '';
			}
			if (sTable.length > 0) {
				if (!data[sTable] || data[sTable] === 'undefined') {
					data[sTable] = [];
				}
				
				data[sTable][this.count(data[sTable])] = oValues;

				/*console.log(data[sTable]);*/
				return true;
			}
			return false;
		},
		update: function (oWhere, oValues, sTable, iTimestamp) {
			var x = 0,
				y = 0,
				bMatch = false,
				iCount = 0,
				arData = [];
			if (sTable.length > 0) {
				for (x in data[sTable]) {
					if (data[sTable].hasOwnProperty(x)) {
						bMatch = false;
						for (y in oWhere) {
							if (oWhere.hasOwnProperty(y)) {
								if (oWhere[y] === data[sTable][x][y]) {
									bMatch = true;
									break;
								}
							}
						}
						if (bMatch === true) {
							for (y in oValues) {
								if (oValues.hasOwnProperty(y)) {
									data[sTable][x][y] = oValues[y];
								}
							}
							if (data[sTable] && data[sTable] !== 'undefined' && data[sTable][x] && data[sTable][x] !== 'undefined') {
								data[sTable][x].timestamp = iTimestamp;
							}
						}
					}
				}
				for (x in data[sTable]) {
					if (data[sTable].hasOwnProperty(x) && arData[x] !== 'null') {
						arData[iCount] = data[sTable][x];
						iCount++;
					}
				}
				return true;
			}
			return false;
		},
		remove: function (oObject, sTable) {
			if (sTable.length > 0) {
				var oData = data[sTable],
					oFinalData = {},
					x = 0,
					y = 0,
					iCount = 0,
					arData = [],
					bMatch = true;
				for (x in oData) {
					if (oData.hasOwnProperty(x)) {
						bMatch = true;
						for (y in oObject) {
							if (oObject.hasOwnProperty(y)) {
								if (oObject[y] !== oData[x][y]) {
									bMatch = false;
									break;
								}
							}
						}
						if (bMatch === false) {
							oFinalData[x] = oData[x];
						}
					}
				}
				data[sTable] = oFinalData;
				for (x in oData) {
					if (oData.hasOwnProperty(x)) {
						if (arData[x] !== 'null') {
							arData[iCount] = oData[x];
							iCount++;
						}
					}
				}
				data[sTable] = arData;
				return true;
			}
			return false;
		},
		clear: function (sTable) {
			if (sTable && sTable.length > 0) {
				data[sTable] = {};
				return true;
			}
			return false;
		},
		//select: function (sType, oFields, oWhere, iLimit, oOrder, oDataFilter) {
		select: function (oSettings) {
			var arData = [],
				iCount = 0,
				x = 0,
				y = 0,
				oRow,
				bMatch = true,
				bDataFilter = true,
				sField = '',
				oWhere = {},
				oFields = {},
				iLimit = 0,
				oData,
				sTable;
			if (oSettings && oSettings !== 'undefined') {
				if (oSettings.table && oSettings.table !== 'undefined' && oSettings.table.length > 0) {
					sTable = oSettings.table;
				}
				if (oSettings.fields && oSettings.fields !== 'undefined') {
					oFields = oSettings.fields;
				}
				if (oSettings.where && oSettings.where !== 'undefined') {
					oWhere = oSettings.where;
				}
				if (oSettings.limit && oSettings.limit !== 'undefined') {
					iLimit = oSettings.limit;
				}
			}
			if (sTable.length > 0) {
				oData = data[sTable];
				results:
				for (x in oData) {
					if (oData.hasOwnProperty(x)) {
						if (arData[x] !== 'null') {
							bMatch = true;
							if(oWhere){
								for (y in oWhere) {
									if (oWhere.hasOwnProperty(y)) {
										if (oWhere[y] !== oData[x][y]) {
											bMatch = false;
											break;
										}
									}
								}
							}
							if (bMatch === true) {
								bDataFilter = true;
								if (typeof(oDataFilter) !== 'undefined') {
									if (oData[x].settings && typeof(oData[x].settings[0]) === 'object') {
										bDataFilter = check_uiobject_data(oDataFilter, validate_json(oData[x].settings[0]));
									} else if (oData[x].settings) {
										bDataFilter = check_uiobject_data(oDataFilter, validate_json(oData[x].settings));
									} else {
										bDataFilter = check_uiobject_data(oDataFilter, oData[x]);
									}
								}
								if (bDataFilter && oFields) {
									if (this.count(oFields) > 0) {

										oRow = {};
										for (y in oFields) {
											if (oFields.hasOwnProperty(y)) {
												sField = oFields[y];
												if (oData[x] && oData[x][sField]) {
													oRow[sField] = oData[x][sField];
												}
											}
										}

										arData.push(oRow);
									} else {
										arData.push(oData[x]);
									}
									iCount++;
									if (iLimit && iLimit > 0 && iCount >= iLimit) {
										break results;
									}
								}
							}
						}
					}
				}
			}
			return arData;
		}
	};
}());