/**
 *  Employee Payment Report Class
 *
 *  @author Jechonias Alejandro
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var
        uiDefaultContainer,
        $emplpaymentDateFrom,
        $emplpaymentDateTo,
        $emplpaymentDateAddedFrom,
        $emplpaymentDateAddedTo,
        $emplpaymentGenerateButton,
        $emplpaymentDateAddedButton,
        $emplpaymentContainer,
        $emplpaymentTemplate,
        $emplpaymentBody,
        $emplpaymentEmpty,
        $emplpaymentHeader,
        uiPaymentDropdown,
        oCurrencyName = {}
        ;


    CPlatform.prototype.employee_payment_report = {

        initialize : function() {
            /*declare variables*/
                uiPaymentDropdown =$('#payment_method_dropdown');
                $emplpaymentGenerateButton = $('button.generate-emplpayment');
                $emplpaymentDateAddedButton = $('button.emplpayment_added_search');
                $emplpaymentContainer = $('tbody[data-container="emplpayment"]');
                $emplpaymentTemplate = $('tr.template.emplpayment');
                $emplpaymentBody = $('div.emplpayment.tbl-rounded');
                $emplpaymentEmpty = $('div.empty.calendar-icon');
                $emplpaymentHeader = $('#emplpayment_search_params');

           /*bind on load data*/
            roxas.employee_payment_report.bind_events_on_load();
            roxas.employee_payment_report.get_payment_method();

           /*initialize events*/


           var uiDisable = $(".emplselect").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt");
            uiDisable.attr("readonly", true);

            $("#employee_name").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".employee_name_search").click();
                    }
                });

            $("#esop_name").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".esop_name_search").click();
                    }
                });

            $("#vesting_years").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".vesting_years_search").click();
                    }
                });

            $("#payment_type").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".payment_type_search").click();
                    }
                });

            $("#payment_value").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".payment_value_search").click();
                    }
                });
           


           $emplpaymentHeader.on( "click", '.employee_name_search',function() {

                $('tbody.emplpayment').find('tr.no_results_found').remove();

                var rows = $('tbody.emplpayment').find('tr:not(".template")');
                var uiSearchFilter = $emplpaymentHeader.find('input[name="employee_name"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('custom-attr-name').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_payment_report.check_results();
            });

           $emplpaymentHeader.on( "click", '.esop_name_search',function() {

                $('tbody.emplpayment').find('tr.no_results_found').remove();

                var rows = $('tbody.emplpayment').find('tr:not(".template")');
                var uiSearchFilter = $emplpaymentHeader.find('input[name="esop_name"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('custom-attr-esop_name').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_payment_report.check_results();
            });

            $emplpaymentHeader.on( "click", '.vesting_years_search',function() {

                $('tbody.emplpayment').find('tr.no_results_found').remove();

                var rows = $('tbody.emplpayment').find('tr:not(".template")');
                var uiSearchFilter = $emplpaymentHeader.find('input[name="vesting_years"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('custom-attr-vesting_years').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_payment_report.check_results();
            });

            $emplpaymentHeader.on( "click", '.payment_type_search',function() {

                $('tbody.emplpayment').find('tr.no_results_found').remove();

                var rows = $('tbody.emplpayment').find('tr:not(".template")');
                var uiSearchFilter = $emplpaymentHeader.find('input[name="payment_type"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('custom-attr-payment_type').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_payment_report.check_results();
            });

            $emplpaymentHeader.on( "click", '.payment_value_search',function() {

                 $('tbody.emplpayment').find('tr.no_results_found').remove();

                var rows = $('tbody.emplpayment').find('tr:not(".template")');
                var uiSearchFilter = $emplpaymentHeader.find('input[name="payment_value"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                    val = parseFloat(cr8v.remove_commas(val)).toFixed(2);
                rows.show().filter(function() {
                    var text = cr8v.remove_commas($(this).attr('custom-attr-payment_value')).replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();
                
                roxas.employee_payment_report.check_results();
            });

            $('.btn_export').on('click', function(){
                 var $clonedTable = $(".emplpayment").clone();
                 $clonedTable.find('[style*="display: none"]').remove();

                 $($clonedTable).table2excel({
                     exclude: ".template",
                     name: "Employee Payment Report",
                     filename: "Employee_Payment_Summary_Report"

                });
            });

           if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                $emplpaymentDateAddedFrom = $('#empl_from');
                $emplpaymentDateAddedTo = $('#empl_to');

                $emplpaymentDateAddedFrom.datetimepicker(dateTimePickerOptions);
                $emplpaymentDateAddedTo.datetimepicker(dateTimePickerOptions);


                $emplpaymentDateAddedFrom.on("dp.change", function (e) {
                    $emplpaymentDateAddedTo.data('DateTimePicker').setMinDate(e.date);
                });

                $emplpaymentDateAddedTo.on("dp.change", function (e) {
                    $emplpaymentDateAddedFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            $emplpaymentHeader.on( "click", '.emplpayment_added_search',function() {

                var uiSearchFrom = $emplpaymentHeader.find('input[name="empl_from"]');
                var uiSearchTo = $emplpaymentHeader.find('input[name="empl_to"]');

                 $('tbody.emplpayment').find('tr.no_results_found').remove();

                if(uiSearchFrom.val() != '' && uiSearchTo.val() != '')
                {
                     var oParams = {
                         'date_from' : uiSearchFrom.val(),
                         'date_to' : uiSearchTo.val()
                     };

                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_employee_payment",
                        "beforeSend": function () {
                            if (cr8v.var_check($emplpaymentDateAddedButton)) {
                                cr8v.show_spinner($emplpaymentDateAddedButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($emplpaymentDateAddedButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $emplpaymentEmpty.addClass('hidden');
                                $emplpaymentBody.removeClass('hidden');
                                $emplpaymentContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $emplpaymentTemplate, $emplpaymentContainer );
                                }
                                roxas.employee_payment_report.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                        },
                        "error" : function() {
                            cr8v.show_spinner($emplpaymentDateAddedButton, false);
                        }
                    }

                    roxas.employee_payment_report.ajax(oAjaxConfig)
                }

           

            });

           if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                $emplpaymentDateFrom = $('#emplpayment_from');
                $emplpaymentDateTo = $('#emplpayment_to');

                $emplpaymentDateFrom.datetimepicker(dateTimePickerOptions);
                $emplpaymentDateTo.datetimepicker(dateTimePickerOptions);


                $emplpaymentDateFrom.on("dp.change", function (e) {
                    $emplpaymentDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                $emplpaymentDateTo.on("dp.change", function (e) {
                    $emplpaymentDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }
       
           $emplpaymentGenerateButton.on('click' , function() {

                 $('tbody.emplpayment').find('tr.no_results_found').remove();

                if($emplpaymentDateFrom.val() != '' && $emplpaymentDateTo.val() != '')
                {
                     var oParams = {
                         'date_from' : $emplpaymentDateFrom.val(),
                         'date_to' : $emplpaymentDateTo.val(),
                         'group_by' : 'upm.id'
                         // 'where' : [
                         //    {
                         //        'field' : 'esop',
                         //        'value' : '1'
                         //    }
                         // ]
                     };

                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_employee_payment",
                        "beforeSend": function () {
                            if (cr8v.var_check($emplpaymentGenerateButton)) {
                                cr8v.show_spinner($emplpaymentGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($emplpaymentGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $emplpaymentEmpty.addClass('hidden');
                                $emplpaymentBody.removeClass('hidden');
                                $emplpaymentContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $emplpaymentTemplate, $emplpaymentContainer );
                                }
                                roxas.employee_payment_report.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();


                            $('#emplpayment_search_params').removeClass('hidden');
                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($emplpaymentGenerateButton, false);
                        }
                    }

                    roxas.employee_payment_report.ajax(oAjaxConfig)
                }

            });

        },
        get_payment_method : function(){
          
            var oGetEsopParams = {
                    "limit" : 999999,
                    "where" : [{
                        "field" : "e.parent_id",
                        "value" : 0
                    }]
            };
            
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oGetEsopParams,
                "url"    : roxas.config('url.server.base') + "reports/get_payment_method",
                "beforeSend": function () {

                },
                "success": function (oData) {
                    
                    
                   
                    if(oData.status == true)
                            {
                                uiPaymentDropdown.find('div.option').remove();
                                
                                for(var x in oData.data)
                                {
                                    var oPaymentMethod = oData.data[x];
                                    var uiTemplate = uiPaymentDropdown;
                                    cr8v.append_dropdown(oPaymentMethod, uiTemplate, false, "esop");
                                }
                               
                            }
                   
                },
                "complete": function () {

                },
            };

            roxas.employee_payment_report.ajax(oAjaxConfig);
            
        },

        /*created custom functions*/
        
        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'check_results' : function() {

            if($('tr.emplpayment:visible').length==0){

                var uiNoresults =
                        "<tr class='no_results_found'>"+
                            "<td colspan='6'>No Results found</td>"+
                          "</tr>";

                $emplpaymentContainer.append(uiNoresults);
            }

        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('index') > -1)
            {
                localStorage.removeItem('claim_id');
                roxas.employee_payment_report.get_currency();
                var oGetParams = {
                    "limit" : 999999
                };
                roxas.employee_payment_report.get(oGetParams, undefined);
                roxas.employee_payment_report.bind_search_event();
            }
            else if(window.location.href.indexOf('view') > -1)
            {
                var iClaimID = localStorage.getItem('claim_id');
                if(cr8v.var_check(iClaimID) == false)
                {
                   window.location =  roxas.config('url.server.base') + "employee_payment_report/index";     
                }
                var oGetParams = {
                "search_field" : 'id',
                "keyword" : iClaimID,
                "limit" : "1"
                };
                roxas.employee_payment_report.get_currency();

                roxas.employee_payment_report.view_get(oGetParams, undefined);
            }

        },

        /**
         * get
         * @description This function is for getting and rendering employee_payment_report on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get : function(oParams, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "employee_payment_report/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        
                        
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.employee_payment_report.ajax(oAjaxConfig);
            }
        },

        /**
         * get_currency
         * @description This function is for getting and rendering currency on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_currency : function(){
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : {'data' : []},
                "url"    : roxas.config('url.server.base') + "esop/get_currency",
                "beforeSend": function () {

                },
                "success": function (oData) {
                    console.log(oData)
                    if(cr8v.var_check(oData))
                    {
                        if(oData.status == true)
                        {
                            oCurrencyName = oData.data;
                        }
                    }
                },
                "complete": function () {

                },
            };

            roxas.employee_payment_report.ajax(oAjaxConfig);
        },
        
        /**
         * get_currency_name
         * @description This function is for getting currency name based on the object returned of get_currency
         * @dependencies N/A
         * @param {int} iCurrencyID
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_currency_name : function(iCurrencyID)
        {
            var sReturn = '';
            if(cr8v.var_check(iCurrencyID))
            {
                for(var i = 0, max = oCurrencyName.length ; i < max; i++)
                {
                    var sCurrencyData = oCurrencyName[i];
                    if(sCurrencyData.id == iCurrencyID)
                    {
                        sReturn = sCurrencyData.name;
                    }
                }
            }
            return sReturn;
        },

        

       
    }

}());

$(window).load(function () {
    roxas.employee_payment_report.initialize();
});
