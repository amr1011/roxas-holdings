/**
 *  Ranks Class
 *
 *
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var uiAddRankModalBtn,
        uiAddRankBtn,
        uiAddRankModal,
        uiAddRankForm,

        uiEditRankModalBtn,
        uiEditRankBtn,
        uiEditRankModal,
        uiEditRankForm,

        uiRankListContainer,

        uiEditDepartmentModal,

        uiNoResultsFound,

        uiSortRankContainer
        ;


    CPlatform.prototype.ranks = {

        initialize : function() {

            /*declare variables*/
            uiAddRankModalBtn = $('[modal-target="add-rank"]');
            uiAddRankBtn = $('#add_rank_btn');
            uiAddRankModal = $('[modal-id="add-rank"]');
            uiAddRankForm = $('form#add_rank_form');

            uiEditRankModalBtn = $('[modal-target="edit-rank"]');
            uiEditRankBtn = $('#edit_rank_btn');
            uiEditRankModal = $('[modal-id="edit-rank"]');
            uiEditRankForm = $('form#edit_rank_form');

            uiRankListContainer = $('[data-container="rank_list_container"]');

            uiEditDepartmentModal = $('[modal-id="edit-department"]');

            uiNoResultsFound = $('.no_results_found.template');

            uiSortRankContainer = $('#sort_rank');

            /*bind on load data*/
            roxas.ranks.bind_events_on_load();
            roxas.ranks.bind_sorting_events();

            /*initialize events*/
            cr8v.input_numeric($('[name="rank_number"]'));
            
            /*for edit rank*/
            uiRankListContainer.on('click', '.edit_rank', function(){
                var uiThis = $(this);
                var uiParent = uiThis.parents('.rank:first');

                roxas.ranks.populate_edit_rank_data_on_modal(uiParent);
            });

            uiEditRankModal.on('click', '.close-me', function(){
                $("body").css({'overflow-y':'initial'});
                uiEditRankModal.removeClass('showed');
                roxas.ranks.clear_edit_rank_modal();
            });

            /*for back to department list*/
            $('.back_to_previous_company').on('click', function(){
                window.location =  roxas.config('url.server.base') + "departments/department_list/";
            });

        },
        /**
         * bind_sorting_events
         * @description This function is for binding events for sorting
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_sorting_events : function(){
            uiSortRankContainer.on('click', '.sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');
                var uiTemplates = uiRankListContainer.find('.rank:not(.template)');

                cr8v.clear_sorting_classes(uiSortRankContainer.find('.sort_field'));
                cr8v.ui_sorting(uiThis, sSortBy, uiTemplates.length, uiTemplates, uiRankListContainer)
            });
            
            cr8v.clear_sorting_classes(uiSortRankContainer.find('.sort_field'));
        },

        /**
         * populate_edit_rank_data_on_modal
         * @description This function is for putting data on the edit rank modal
         * @dependencies 
         * @param {jQuery} uiParent
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        populate_edit_rank_data_on_modal : function(uiParent){
            if(cr8v.var_check(uiParent))
            {   
                uiEditRankModal.addClass('showed');

                var iRankID = uiParent.attr('data-rank-id');
                var sRankName = uiParent.attr('data-rank-name');
                var sRankNumber = uiParent.attr('data-rank-number');

                uiEditRankModal.find('input[name="rank_number"]').val(sRankNumber);
                uiEditRankModal.find('input[name="rank_name"]').val(sRankName);
                uiEditRankModal.attr('data-rank-id', iRankID);
            }
        },

        /**
         * clear_edit_rank_modal
         * @description This function is for clearing data in edit rank modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_edit_rank_modal : function(){
            uiEditRankModal.find('input').removeClass('input-error');
            uiEditRankModal.find('input[name="rank_number"]').val('');
            uiEditRankModal.find('input[name="rank_name"]').val('');
            uiEditRankModal.removeAttr('data-rank-id');
            uiEditRankModal.find('.edit_rank_error_message').addClass('hidden');
            uiEditRankModal.find('.edit_rank_success_message').addClass('hidden');
        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('rank') > -1 && localStorage.getItem('department_id') != null)
            {
                // console.log('rank')
                var oGetRankParams = {
                    "search_field" : 'department_id',
                    "keyword" : localStorage.getItem('department_id'),
                    "limit" : 999999
                };
                roxas.ranks.get_ranks(oGetRankParams);

                /*for add of rank events*/
                roxas.ranks.bind_add_rank_events();
                /*for edit of rank events*/
                roxas.ranks.bind_edit_rank_events();
            }
            else
            {
                // if(localStorage.getItem('company_id') != null)
                // {
                //     // console.log('rank')
                //     var oGetDepartmentParams = {
                //         "params" : {
                //             "keyword" : 'd.company_id',
                //             "search_field" : localStorage.getItem('company_id'),
                //         }
                //     };
                //     roxas.departments.get_departments(oGetDepartmentParams);

                //     /*for add of department events*/
                //     roxas.departments.bind_add_department_events();
                // }
            }
        },

        /**
         * clear_add_rank_modal
         * @description This function is for clearing data in add rank modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_add_rank_modal : function(){
            /*clear data*/
            uiAddRankModal.find('input').removeClass('input-error');
            uiAddRankModal.find('input[name="rank_name"]').val('');
            uiAddRankModal.find('input[name="rank_number"]').val('');
            uiAddRankModal.find('.add_rank_success_message').addClass('hidden');
            uiAddRankModal.find('.add_rank_error_message').addClass('hidden');
        },

        /**
         * bind_add_rank_events
         * @description This function is for binding events for add rank info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_add_rank_events : function(){

            uiAddRankModalBtn.on('click', function(){
                /*clear data*/
                roxas.ranks.clear_add_rank_modal();
                roxas.ranks.generate_new_code({table : 'rank', column : 'number'});
            });

            /*save new rank*/
            uiAddRankBtn.on('click', function(){
                uiAddRankForm.submit();
            });

            /*prevent on submit of form*/
            uiAddRankModal.on('submit', uiAddRankForm, function (e) {
                e.preventDefault();
            });

            /*add a validation to adding a new rank*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiAddRankBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiAddRankModal.find('.add_rank_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiAddRankModal.find('.add_rank_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiAddRankBtn, true);
                    
                    roxas.ranks.assemble_add_rank_information(uiAddRankModal, uiAddRankBtn);
                }
            };

            /*bind the validation to add new rank form*/
            cr8v.validate_form(uiAddRankForm, oValidationConfig);
        },


        /**
         * bind_edit_rank_events
         * @description This function is for binding events for add rank info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_edit_rank_events : function(){

            /*save edit rank*/
            uiEditRankBtn.on('click', function(){
                uiEditRankForm.submit();
            });

            /*prevent on submit of form*/
            uiEditRankModal.on('submit', uiEditRankForm, function (e) {
                e.preventDefault();
            });

            /*add a validation to editing a rank*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiEditRankBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiEditRankModal.find('.edit_rank_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiEditRankModal.find('.edit_rank_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiEditRankBtn, true);
                    
                    roxas.ranks.assemble_edit_rank_information(uiEditRankModal, uiEditRankBtn);
                }
            };

            /*bind the validation to edit rank form*/
            cr8v.validate_form(uiEditRankForm, oValidationConfig);
        },

        /**
         * assemble_add_rank_information
         * @description This function will assemble add rank information
         * @dependencies N/A
         * @param {jQuery} uiAddRankModal
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_add_rank_information : function(uiAddRankModal, uiAddRankBtn){
            var oParams = {
                'name' : uiAddRankModal.find('input[name="rank_name"]').val(),
                'number' : uiAddRankModal.find('input[name="rank_number"]').val(),
                'department_id' : uiEditDepartmentModal.attr('data-department-id'),
            };
            roxas.ranks.add_rank(oParams, uiAddRankBtn);
        },

        /**
         * assemble_edit_rank_information
         * @description This function will assemble add rank information
         * @dependencies N/A
         * @param {jQuery} uiAddRankModal
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_edit_rank_information : function(uiEditRankModal, uiEditRankBtn){
            var oParams = {
                'id' : uiEditRankModal.attr('data-rank-id'),
                'data': [
                    {
                        'name' : uiEditRankModal.find('input[name="rank_name"]').val(),
                        'number' : uiEditRankModal.find('input[name="rank_number"]').val(),
                    }
                ]
            };
            roxas.ranks.edit_rank(oParams, uiEditRankBtn);
        },

        /**
         * add_rank
         * @description This function will add rank number and name
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_rank : function(oParams, uiAddRankBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "ranks/add",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiAddRankBtn)) {
                            cr8v.show_spinner(uiAddRankBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddRankModal.find('.add_rank_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiAddRankModal.hasClass('showed'))
                                    {
                                        roxas.ranks.clear_add_rank_modal();
                                        uiAddRankModal.find('.close-me').trigger('click');
                                    }
                                }, 1000)
                                
                                var oRankData = oData.data;
                                var uiTemplate = $('.rank.template').clone().removeClass('template hidden').show();
                                var uiManipulatedTemplate = roxas.ranks.manipulate_template(oRankData, uiTemplate);
                                uiRankListContainer.prepend(uiManipulatedTemplate);

                                uiNoResultsFound.addClass('template hidden');
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiAddRankModal.find('.add_rank_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiAddRankBtn)) {
                            cr8v.show_spinner(uiAddRankBtn, false);
                        }
                    },
                };

                roxas.ranks.ajax(oAjaxConfig);
            }
        },

        /**
         * add_department
         * @description This function will add department code, name and description
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        edit_rank : function(oParams, uiEditRankBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "ranks/edit",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiEditRankBtn)) {
                            cr8v.show_spinner(uiEditRankBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiEditRankModal.find('.edit_rank_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiEditRankModal.hasClass('showed'))
                                    {
                                        roxas.ranks.clear_edit_rank_modal();
                                        uiEditRankModal.find('.close-me').trigger('click');
                                    }
                                }, 1000)

                                var oRankData = oData.data;
                                var uiTemplate = $('[data-rank-id="'+oRankData.id+'"]');
                                var uiManipulatedTemplate = roxas.ranks.manipulate_template(oRankData, uiTemplate);
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiEditRankModal.find('.edit_rank_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiEditRankBtn)) {
                            cr8v.show_spinner(uiEditRankBtn, false);
                        }
                    },
                };

                roxas.departments.ajax(oAjaxConfig);
            }
        },

        /**
         * manipulate_template
         * @description This function will put the data of the rank needed on the template
         * @dependencies N/A
         * @param {object} oRankData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template : function(oRankData, uiTemplate){
            if(cr8v.var_check(oRankData) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.attr('data-rank-id', oRankData.id);
                uiTemplate.attr('data-rank-name', oRankData.name);
                uiTemplate.attr('data-rank-number', oRankData.number);
                uiTemplate.attr('data-rank-employee-count', oRankData.employee_count);

                uiTemplate.find('[data-label="rank_name"]').text(oRankData.name);
                uiTemplate.find('[data-label="rank_number"]').text(oRankData.number);
                uiTemplate.find('[data-label="rank_description"]').text(oRankData.description);
                uiTemplate.find('[data-label="rank_employee_count"]').text(oRankData.employee_count);

                return uiTemplate;
            }
        },

        /**
         * get_ranks
         * @description This function is for getting and rendering ranks on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_ranks : function(oParams, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "ranks/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiRankListContainer.find('.rank:not(.template)').remove();
                                uiNoResultsFound.addClass('template hidden');

                                var arrLogRankIDS = [];

                                if(oData.data.length > 0)
                                {
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oRankData = oData.data[i];
                                        var uiTemplate = $('.rank.template').clone().removeClass('template hidden').show();
                                        var uiManipulatedTemplate = roxas.ranks.manipulate_template(oRankData, uiTemplate);
                                        uiRankListContainer.prepend(uiManipulatedTemplate);
                                        
                                        // arrLogIDS.push(oRankData.id);
                                        arrLogRankIDS.push(oRankData.id);
                                    }
                                }
                                else
                                {
                                    uiNoResultsFound.removeClass('template hidden');
                                }

                                var oGetLogsParams = {
                                    "where" : [
                                        {
                                            "field" : "ref_id",
                                            "operator" : "IN",
                                            "value" : "("+arrLogIDS.join()+")"
                                        },
                                        {
                                            "field" : "type",
                                            "operator" : "IN",
                                            "value" : "('department')"
                                        }
                                    ]
                                };
                                cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
                                
                                if(cr8v.var_check(arrLogRankIDS) && count(arrLogRankIDS) > 0)
                                {
                                    var oGetLogsParams = {
                                        "where" : [
                                            {
                                                "field" : "ref_id",
                                                "operator" : "IN",
                                                "value" : "("+arrLogRankIDS.join()+")"
                                            },
                                            {
                                                "field" : "type",
                                                "operator" : "IN",
                                                "value" : "('rank')"
                                            }
                                        ]
                                    };
                                    cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.ranks.ajax(oAjaxConfig);
            }
        },

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        generate_new_code : function(oParams) {
            var oData = {
                table : oParams.table,
                column : oParams.column
            },
            oAjaxConfig = {
                "type"   : "POST",
                "data"   : oData,
                "url"    : roxas.config('url.server.base') + "esop/generate_new_code",
                "success": function (oResult) {
                    // console.log(oResult);
                    if(oResult.status) {
                        // sGeneratedCode = oResult.data;    
                        // console.log(oResult.data);
                        $('.code-generated').val(oResult.data);
                    }
                }
            };

            roxas.ranks.ajax(oAjaxConfig);
        }
    }

}());

$(window).load(function () {
    roxas.ranks.initialize();
});
