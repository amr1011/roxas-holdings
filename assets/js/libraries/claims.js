/**
 *  Claims Class
 *
 *  @author Jechonias Alejandro
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var
        uiListContainer,
        uiGridContainer,
        uiNoResultsFound,
        oCurrencyName = {},
        uiSearchBtn,
        uiSearchDropdown,
        uiSearchnameInput,
        uiSearchVestingYearsInput,
        uiSearchGrantFromInput,
        uiSearchGrantToInput,
        uiSearchPricePerShareinput,
        uiSortContainer,
        uiIndexPage,
        uiViewPage,
        uiSendClaimFormModal,
        uiRejectClaimFormModal,
        uiApproveClaimFormModal,
        uiMarkClaimAsCompleted,
        uiCurrencyDropdown,
        uiClassCurrencyDropdown,
        uiPaymentDropdown,
        uiClassPaymentDropdown,
        oPaymentName = {},
        uitest
        ;


    CPlatform.prototype.claims = {

        initialize : function() {
            /*declare variables*/
            uitest = $( ".btn-add-claim" )
            uiGridContainer = $('[data-container="view_by_grid"]');
            uiListContainer = $('[data-container="view_by_list"]');
            uiNoResultsFound = $('.no_results_found.template');

            uiSearchBtn = $('.search_btn');
            uiSearchDropdown = $('#search_dropdown');
            uiSearchnameInput = $('.search_name_input');
            uiSearchVestingYearsInput = $('.search_vesting_years_input');
            uiSearchGrantFromInput = $('.search_grant_date_from_input');
            uiSearchGrantToInput = $('.search_grant_date_to_input');
            uiSearchPricePerShareinput = $('.search_price_per_share_input');

            uiSortContainer = $('#sort');
            
            uiIndexPage = $('section.claim_index');
            uiViewPage = $('section.claim_view');
            uiSendClaimFormModal = $('[modal-id="send-claim"]');
            uiRejectClaimFormModal = $('[modal-id="reject-claim"]');
            uiApproveClaimFormModal = $('[modal-id="approve-claim"]');
            uiMarkClaimAsCompleted = $('[modal-id="mark-claim-as-completed"]');

            uiCurrencyDropdown = $('#currency_dropdown');
            uiClassCurrencyDropdown = $('.currency_dropdown');

            uiPaymentDropdown = $('#payment_method_dropdown');
            uiClassPaymentDropdown = $('.payment_method_dropdown');

           /*bind on load data*/
            roxas.claims.bind_events_on_load();
            roxas.claims.bind_sorting_events();
           
           /*initialize events*/
           uiGridContainer.off("click",'.grid_view_data').on("click",'.grid_view_data', function() {       
                var uiThis = $(this);
                var iClaimID = uiThis.parents('.tmp_grid').attr('data-id');


                if(localStorage.getItem('soa_from_personal') != null)
                {
                    localStorage.removeItem('soa_from_personal');
                    localStorage.setItem('soa_from_personal', false);
                }

                roxas.claims.view_data(iClaimID);    
           });

           uiListContainer.off("click",'.list_view_data').on("click",'.list_view_data', function() {       
                var uiThis = $(this);
                var iClaimID = uiThis.parents('.tmp_list').attr('data-id');
                roxas.claims.view_data(iClaimID);    
           });

           uiViewPage.off("click",'.back_to_index').on("click",'.back_to_index', function() {       
                var uiThis = $(this);
                localStorage.removeItem('claim_id');
                window.location =  roxas.config('url.server.base') + "claims/index";
           });

          uiViewPage.on("click",'.send_claim', function() {       
                var uiThis = $(this);
                var iCVRTotal = uiViewPage.data('data-cvr-total');
                    uiSendClaimFormModal.find('input[name="amount_paid"]').val(iCVRTotal); 
           });

         uiSendClaimFormModal.change("click",'.amountpaid', function() { 
            alert("test");

        });


        cr8v.input_numeric($('input[name="amount_paid"]'));
        cr8v.input_numeric($('input[name="document_name"]'));

          uiSendClaimFormModal.on("click",'.btn-add-claim', function() {       
                var uiBtn = $(this);
                var iClaimID = uiViewPage.data('data-claim-id');
                var iUserID = uiViewPage.data('data-user-id');
                var iEsopID = uiViewPage.data('data-esop-id');
                var iCVRTotal = uiViewPage.data('data-cvr-total');
                var sDocumentName = uiSendClaimFormModal.find('input[name="document_name"]').val();
                var sCurrency = uiSendClaimFormModal.find('div#currency_dropdown').attr('currency_id');
                var sAmountPaid = uiSendClaimFormModal.find('input[name="amount_paid"]').val();
                var sDateOfOR = uiSendClaimFormModal.find('input[name="date_of_or"]').val();
                var sClaimAttachment = uiSendClaimFormModal.find('input[name="claim_attachment"]').val();
                var sClaimAttachmentUpload = uiSendClaimFormModal.find('input[name="claim_attachment"]');
                var sEsopName = uiViewPage.data('data-esop-name');
                var sUserName = uiViewPage.data('data-user-name');


                if(sDateOfOR != "" && sDateOfOR != "0")
                {
                   sDateOfOR = moment(sDateOfOR).format('YYYY-MM-DD');
                }else{

                }
                
                var files = sClaimAttachmentUpload[0]["files"];
                var data = new FormData();


  
                $.each(files, function(key, value){
                     data.append("claim_attachment", value);
                     data.append("csrf_ironman_token", $.cookie('csrf_ironman_cookie'));
                });

      

                if(roxas.claims.validate_send_claim_esop_to_admin() == true)
                {

                        if(files.length > 0){
                               var oAjaxConfig = {
                                    "type"   : "POST",
                                    "data"   : data,
                                    "processData" : false,
                                    "contentType" : false,
                                    "url"    : roxas.config('url.server.base') + "claims/ajax_upload_attachment",
                                    "beforeSend": function () {
                                        if (cr8v.var_check(uiBtn)) {
                                            cr8v.show_spinner(uiBtn, true);
                                        }
                                    },
                                    "success": function (oData) {

                                        if(cr8v.var_check(oData))
                                        {
                                            if(oData.status == true)
                                            {
                                                uiSendClaimFormModal.find('.add_error_message').addClass('hidden').html('');
                                                uiSendClaimFormModal.find('.add_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                                                var sPath = oData.data.file_path;
                                                var sDocumentType = oData.data.document_type;

                                                      var oParams = {
                                                        'claim_id'                       : iClaimID,
                                                        'document_name'                  : sDocumentName,
                                                        'amount_paid'                    : sAmountPaid,
                                                        'currency'                       : sCurrency,
                                                        'status'                         : 1,
                                                        'user_id'                        : iUserID,
                                                        'path'                           : sPath,
                                                        'document_type'                  : sDocumentType,
                                                        'date_of_or'                     : sDateOfOR,
                                                        'esop_name'                     : sEsopName,
                                                        'user_name'                     : sUserName                                                        
                                                        }

                                                /*for(var i in oParams){
                                                     data.append(i, oParams[i]);  
                                                }*/
                                                //return
                                                roxas.claims.send_claim_esop_to_admin(oParams, uiBtn); 


                                            }
                                            else
                                            {
                                                uiSendClaimFormModal.find('.add_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                                            }
                                        }
                                    },
                                    "complete": function () {
                                        if (cr8v.var_check(uiBtn)) {
                                            cr8v.show_spinner(uiBtn, false);
                                        }
                                    },
                                };

                                roxas.claims.ajax(oAjaxConfig);  
                        }else{
                               var  oParams = {
                                'claim_id'                       : iClaimID,
                                'document_name'                  : sDocumentName,
                                'amount_paid'                    : sAmountPaid,
                                'currency'                       : sCurrency,
                                'status'                         : 1,
                                'user_id'                        : iUserID,
                                'path'                           : "",
                                'document_type'                  : "",
                                'date_of_or'                     : sDateOfOR,
                                'esop_name'                     : sEsopName,
                                'user_name'                     : sUserName   
                                }

                                roxas.claims.send_claim_esop_to_admin(oParams, uiBtn); 
                        }

                }                                

                  
           });

           uiSendClaimFormModal.on("click",'.lbl_upload_file', function() {       
                uiSendClaimFormModal.find('input[name="claim_attachment"]').trigger("click");     
           });
           
           uiSendClaimFormModal.on("change",'input[name="claim_attachment"]', function() {       
               var uiThis = $(this);
               uiSendClaimFormModal.find('[data-container="attachement_holder"]').removeClass('hidden');  
               uiSendClaimFormModal.find('[data-container="attachement_name"]').text(uiThis.val());  

               var file_inserted= $(this).val();
               var extention = file_inserted.substring(file_inserted.lastIndexOf('.') + 1);
                //alert(extention);
               
               var arrAllowedExtentions = ["", "pdf", "PDF", "jpg", "JPG", "png", "PNG", "gif", "GIF"];
               if(arrAllowedExtentions.indexOf(extention) > 0){
                  uiSendClaimFormModal.find('.invalid_file').addClass('hidden');
               }
               else
               {
                //alert("ERROR: Invalid File Upload!");
                uiSendClaimFormModal.find('.remove_attachment').trigger('click');
                uiSendClaimFormModal.find('.invalid_file').removeClass('hidden');
               }  
           });

           uiSendClaimFormModal.on("click",'.remove_attachment', function() {       
               var uiThis = $(this);
               uiSendClaimFormModal.find('[data-container="attachement_holder"]').addClass('hidden');    
               uiSendClaimFormModal.find('input[name="claim_attachment"]').val('');
           });

           uiRejectClaimFormModal.on("click",'.btn-reject-claim', function() {       
                var uiBtn = $(this);
                var iClaimID = uiViewPage.data('data-claim-id');
                var sRejectReason = uiRejectClaimFormModal.find('textarea[name="reject_reason"]').val();
                if(sRejectReason !== '')
                {
                     uiRejectClaimFormModal.find('.error_message').addClass('hidden').html('');
                     uiRejectClaimFormModal.find('textarea[name="reject_reason"]').removeClass('input-error');     
                       var oParams = {
                        'claim_id'                       : iClaimID,
                        'status'                         : 3,
                        'reject_reason'                  : sRejectReason
                        };

                        roxas.claims.reject_esop_claim(oParams, uiBtn); 
                }       
                else
                {      uiRejectClaimFormModal.find('.error_message').removeClass('hidden').html('Reject reason is required.');
                       uiRejectClaimFormModal.find('textarea[name="reject_reason"]').addClass('input-error');     
                }
           });

           uiApproveClaimFormModal.on("click",'.btn-approve-claim', function() {       
                var uiBtn = $(this);
                var iClaimID = uiViewPage.data('data-claim-id');
                var sEsopName = uiViewPage.data('data-esop-name');
                var sUserName = uiViewPage.data('data-user-name');
                var iUserID = uiViewPage.data('data-user-id');

                var oParams = {
                        'claim_id'                       : iClaimID,
                        'status'                         : 1,
                        'esop_name'                      : sEsopName,
                        'user_name'                      : sUserName,
                        'user_id'                        : iUserID,
                        };
                roxas.claims.approve_esop_claim(oParams, uiBtn);  
           });

           uiMarkClaimAsCompleted.on("click",'.btn-mark-claim-as-completed', function() {       
                var uiBtn = $(this);
                var iClaimID = uiViewPage.data('data-claim-id');
                var iClaimDate = uiMarkClaimAsCompleted.find('input[name="date_completed"]').val();
                var sEsopName = uiViewPage.data('data-esop-name');
                var sUserName = uiViewPage.data('data-user-name');
                var iUserID = uiViewPage.data('data-user-id');

                var sVestingRightsIDs = uiViewPage.data('data-vesting-rights-ids');
                var oParams = {
                        'vesting_rights_ids' : sVestingRightsIDs
                };
                
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "claims/update_vesting_rights",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                var oParams = {
                                'claim_id'                       : iClaimID,
                                'status'                         : 2,
                                'date_completed'                 : iClaimDate,
                                'esop_name'                      : sEsopName,
                                'user_name'                      : sUserName,
                                'user_id'                        : iUserID,
                                };

                                roxas.claims.mark_claim_as_completed(oParams, uiBtn);

                            }
                            else
                            {
                                uiMarkClaimAsCompleted.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
                 
           });

            $('button.print_claim').on('click' , function() {

                $('#divcontents').find('*').each(function() {
                    var style = cr8v.css($(this));
                    $(this).css(style)
                });

                var content = document.getElementById("divcontents");
                var pri = document.getElementById("ifmcontentstoprint").contentWindow;
                pri.document.open();
                pri.document.write(content.innerHTML);
                pri.document.close();
                pri.focus();
                pri.print();

            })
           

            $('button.print_soa').on('click' , function() {
                // cr8v.show_spinner($('button.print_soa'), true);
//                 $('[section-style="content-panel"]').find('*').each(function() {
//                     var style = cr8v.css($(this));
//                     $(this).css(style)
//                 });

                var content = $('[section-style="content-panel"]');
                var pri = document.getElementById("ifmcontentstoprint").contentWindow;
//                 pri.document.open();
//                 pri.document.write(content.html());
//                 pri.document.close();
//                 pri.focus();
//                 pri.print();

                var oParams = {'html' : content.html()};

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/download_soa",
                    "beforeSend": function () {
                        cr8v.show_spinner($('button.print_soa'), true);
                    },
                    "success" : function(oData) {
                        console.log(oData)
                        if(oData.status == true)
                        {
                            if(cr8v.var_check(oData.url) && cr8v.var_check(oData.file_name))
                            {
                                window.location = oData.url;

                                var sFilePathForDelete = './assets/templates/' + oData.file_name;

                                var oParams = {
                                    "file_path" : sFilePathForDelete
                                };

                                setTimeout(function(){
                                    cr8v.delete_file(oParams, function(oData){
                                    
                                    });
                                }, 2000);
                            }
                        }
                    },
                    "complete" : function() {
                        cr8v.show_spinner($('button.print_soa'), false);
                    }
                }
                roxas.claims.ajax(oAjaxConfig)
            })
            




        },

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('index') > -1)
            {
                localStorage.removeItem('claim_id');
                roxas.claims.get_currency();
                var oGetParams = {
                    "limit" : 999999
                };
                roxas.claims.get(oGetParams, undefined);
               // roxas.claims.get_employee(oGetParams, undefined);
                roxas.claims.bind_search_event();
            }
            else if(window.location.href.indexOf('view') > -1)
            {
                var iClaimID = localStorage.getItem('claim_id');
                if(cr8v.var_check(iClaimID) == false)
                {
                   window.location =  roxas.config('url.server.base') + "claims/index";     
                }
                var oGetParams = {
                "search_field" : 'id',
                "keyword" : iClaimID,
                "limit" : "1"
                };
                roxas.claims.get_currency();
                roxas.claims.view_get(oGetParams, undefined);
                roxas.claims.bind_dropdown_events();
            }
            else if(window.location.href.indexOf('employee_claims') > -1)
            {
                localStorage.removeItem('claim_id');
                roxas.claims.get_currency();
                var oGetParams = {
                    "limit" : 999999,
                    "where" : [{
                        "field" : 'uc.user_id',
                        "value" : iUserID
                    }]
                };
                roxas.claims.get_employee(oGetParams, undefined);
            }

        },
        /**
         * bind_dropdown_events
         * @description This function will bind the events for the dropdowns
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_dropdown_events : function(){
            uiClassCurrencyDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiCurrencyDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiClassPaymentDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiPaymentDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            
            uiCurrencyDropdown.on('click', '.option.currency', function(){
                var uiThis = $(this);
                var iCurrencyID = uiThis.attr('data-value');

                uiCurrencyDropdown.attr('currency_id', iCurrencyID);
            });

            uiPaymentDropdown.on('click', '.option.payment_type', function(){
                var uiThis = $(this);
                var iValue = uiThis.attr('data-value');

                uiPaymentDropdown.attr('payment_type_id', iValue);
            });

            uiClassCurrencyDropdown.on('click', '.option.currency', function(){
                var uiThis = $(this);
                var iCurrencyID = uiThis.attr('data-value');

                uiClassCurrencyDropdown.attr('currency_id', iCurrencyID);
            });
        },

        /**
         * get
         * @description This function is for getting and rendering claims on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get : function(oParams, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "claims/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        //console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                                 uiGridContainer.find('.tmp_grid:not(.template)').remove();
                                 uiListContainer.find('.tmp_list:not(.template)').remove();
                                 uiNoResultsFound.addClass('template hidden');

                            if(oData.status == true)
                            {                             
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i <= max; i++)
                                        {
                                            var oEsopData = oData.data[i];

                                            var uiTemplateList = $('.tmp_list.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateList = roxas.claims.manipulate_template_list(oEsopData, uiTemplateList);

                                            var uiTemplateGrid = $('.tmp_grid.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateGrid = roxas.claims.manipulate_template_grid(oEsopData, uiTemplateGrid);
                                            
                                                uiListContainer.prepend(uiManipulatedTemplateList);
                                                uiGridContainer.prepend(uiManipulatedTemplateGrid);       
                                            
                                        }
                                    }
                                    else
                                    {
                                        uiNoResultsFound.removeClass('template hidden');
                                    }
                            }
                            else
                            {
                                uiNoResultsFound.removeClass('template hidden');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
            }
        },

        get_employee : function(oParams, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "claims/get_employee_claims",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        //console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                                 uiGridContainer.find('.tmp_grid:not(.template)').remove();
                                 uiListContainer.find('.tmp_list:not(.template)').remove();
                                 uiNoResultsFound.addClass('template hidden');

                            if(oData.status == true)
                            {                             
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i <= max; i++)
                                        {
                                            var oEsopData = oData.data[i];

                                            var uiTemplateList = $('.tmp_list.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateList = roxas.claims.manipulate_template_list(oEsopData, uiTemplateList);

                                            var uiTemplateGrid = $('.tmp_grid.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateGrid = roxas.claims.manipulate_template_grid(oEsopData, uiTemplateGrid);
                                            
                                                uiListContainer.prepend(uiManipulatedTemplateList);
                                                uiGridContainer.prepend(uiManipulatedTemplateGrid);       
                                            
                                        }
                                    }
                                    else
                                    {
                                        uiNoResultsFound.removeClass('template hidden');
                                    }
                            }
                            else
                            {
                                uiNoResultsFound.removeClass('template hidden');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
            }
        },

        /**
         * view_get
         * @description This function is for getting and rendering view claim on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
         view_get : function(oParams, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "claims/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData.data)
                        if(cr8v.var_check(oData))
                        {

                            if(oData.status == true)
                            {                         
                                    if(oData.data.length > 0)
                                    {
                                        var oData = oData.data[0];
                                        roxas.claims.supply_template_view(oData);
                                        uiViewPage.removeClass('hidden');
                                    }
                            }
                            else
                            {
                               uiViewPage.addClass('hidden');     
                            }

                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
            }
        },

        /**
         * manipulate_template_grid
         * @description This function will put the data of the esop list view
         * @dependencies N/A
         * @param {object} oData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        manipulate_template_grid : function(oData, uiTemplate){
            //console.log(oData);
            if(cr8v.var_check(oData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oData.currency_name) ? oData.currency_name : roxas.claims.get_currency_name(oData.currency) ;
                var sPricePerShare = sCurrencyName +' '+ oData.price_per_share;
                var sGrantDate = moment(oData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('data-id', oData.id);
                uiTemplate.attr('data-name', oData.esop_name);
                uiTemplate.attr('data-price-per-share', oData.price_per_share);
                uiTemplate.attr('data-year-no', oData.year_no);
                uiTemplate.data('data-save-value', oData);

                if(oData.claim_status == 0)
                {
                   if(count(oData.data_claim_attachments) > 0)
                   {
                      uiTemplate.find('[data-label="claim_status"]').addClass('color-pending').text('PENDING APPROVAL');     
                   }
                   else
                   {
                      uiTemplate.find('[data-label="claim_status"]').addClass('color-pending').text('PENDING');     
                   }
                   
                }
                else if (oData.claim_status == 1)
                {
                   uiTemplate.find('[data-label="claim_status"]').addClass('color-final').text('FOR FINALIZATION');
                }
                else if (oData.claim_status == 2)
                {
                   var sDateCompleted = oData.date_completed;
                       sDateCompleted = moment(sDateCompleted, 'YYYY-MM-DD').format('MMMM DD, YYYY');
                   uiTemplate.find('[data-label="claim_status"]').addClass('color-complete').text('CLAIMED AS OF '+sDateCompleted);
                }
                else if (oData.claim_status == 3)
                {
                   uiTemplate.find('[data-label="claim_status"]').addClass('color-reject').text('REJECTED');          
                }

                uiTemplate.find('[data-label="employee_name"]').text(oData.first_name+' '+oData.last_name);
                uiTemplate.find('[data-label="employee_code"]').text(oData.employee_code);
                uiTemplate.find('[data-label="esop_name"]').text(oData.esop_name);
                uiTemplate.find('[data-label="outstanding_balance"]').text(oData.value_of_shares);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oData.total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                //uiTemplate.find('[data-label="year_no"]').text('Year '+oData.year_no);
                uiTemplate.find('[data-label="value_of_shares"]').text(number_format(oData.value_of_shares, 2) +' Shares');
                
                if(oData.data_claim_vesting_rights.length > 0)
                    {
                        var sVestingYears = '';
                        for(var i = 0, max = oData.data_claim_vesting_rights.length -1; i <= max; i++)
                        {
                            var oCVRData = oData.data_claim_vesting_rights[i];

                            
                            sVestingYears += ' Year '+ oCVRData.year_no + ', ';
                        }
                        sVestingYears = sVestingYears.substring(0, sVestingYears.length-2);
                        uiTemplate.find('[data-label="vesting_years"]').text(sVestingYears);
                        
                    }
                    
                if(iUserRole == 4)//admin filter with claim attachment
                    {
                        if(oData.data_claim_attachments.length > 0)
                        {
                                return uiTemplate;
                        }
                    }
                else//hr show all
                {
                      return uiTemplate;   
                }

            }
        },

        /**
         * manipulate_template_list
         * @description This function will put the data of the esop list view
         * @dependencies N/A
         * @param {object} oData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        manipulate_template_list : function(oData, uiTemplate){
            if(cr8v.var_check(oData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oData.currency_name) ? oData.currency_name : roxas.claims.get_currency_name(oData.currency) ;
                var sPricePerShare = sCurrencyName +' '+ oData.price_per_share;
                var sGrantDate = moment(oData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('data-id', oData.id);
                uiTemplate.attr('data-name', oData.esop_name);
                uiTemplate.attr('data-price-per-share', oData.price_per_share);
                uiTemplate.attr('data-year-no', oData.year_no);

                if(oData.claim_status == 0)
                {
                   uiTemplate.find('[data-label="claim_status"]').addClass('color-pending').text('PENDING');
                }
                else if (oData.claim_status == 1)
                {
                   uiTemplate.find('[data-label="claim_status"]').addClass('color-final').text('FOR FINALIZATION');
                }
                else if (oData.claim_status == 2)
                {
                   uiTemplate.find('[data-label="claim_status"]').addClass('color-complete').text('COMPLETED');
                }
                else if (oData.claim_status == 3)
                {
                   uiTemplate.find('[data-label="claim_status"]').addClass('color-reject').text('REJECTED');          
                }

                uiTemplate.find('[data-label="employee_name"]').text(oData.first_name+' '+oData.last_name);
                uiTemplate.find('[data-label="employee_code"]').text(oData.employee_code);
                uiTemplate.find('[data-label="esop_name"]').text(oData.esop_name);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oData.total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiTemplate.find('[data-label="year_no"]').text('Year '+oData.year_no);
                uiTemplate.find('[data-label="value_of_shares"]').text(number_format(oData.value_of_shares, 2) +' Shares');
                
                if(oData.data_claim_vesting_rights.length > 0)
                    {
                        var sVestingYears = '';
                        for(var i = 0, max = oData.data_claim_vesting_rights.length -1; i <= max; i++)
                        {
                            var oCVRData = oData.data_claim_vesting_rights[i];

                            
                            sVestingYears += ' Year '+ oCVRData.year_no + ', ';
                        }
                        sVestingYears = sVestingYears.substring(0, sVestingYears.length-2);
                        uiTemplate.find('[data-label="vesting_years"]').text(sVestingYears);
                        
                    }
                    
                if(iUserRole == 4)//admin filter with claim attachment
                    {
                        if(oData.data_claim_attachments.length > 0)
                        {
                                return uiTemplate;
                        }
                    }
                else//hr show all
                {
                      return uiTemplate;   
                }
                                 
               
            }
        },

        /**
         * get_currency
         * @description This function is for getting and rendering currency on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_currency : function(){
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : {'data' : []},
                "url"    : roxas.config('url.server.base') + "esop/get_currency",
                "beforeSend": function () {

                },
                "success": function (oData) {
                   // console.log(oData)
                    if(cr8v.var_check(oData))
                    {
                        if(oData.status == true)
                        {
                            oCurrencyName = oData.data;
                            uiCurrencyDropdown.find('div.option').remove();
                            uiClassCurrencyDropdown.find('div.option').remove();
                            for(var i = 0, max = oData.data.length ; i < max; i++)
                            {
                                var oCurrencyData = oData.data[i];

                                var uiTemplate = uiCurrencyDropdown;
                                cr8v.append_dropdown(oCurrencyData, uiTemplate, false, 'currency');
                                cr8v.append_dropdown(oCurrencyData, uiClassCurrencyDropdown, false, 'currency');

                            }
                            uiCurrencyDropdown.find('.option:first').trigger('click');
                            uiClassCurrencyDropdown.find('.option:first').trigger('click');
                        }
                    }
                },
                "complete": function () {

                },
            };

            roxas.claims.ajax(oAjaxConfig);
        },
        
        /**
         * get_currency_name
         * @description This function is for getting currency name based on the object returned of get_currency
         * @dependencies N/A
         * @param {int} iCurrencyID
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_currency_name : function(iCurrencyID)
        {
            var sReturn = '';
            if(cr8v.var_check(iCurrencyID))
            {
                for(var i = 0, max = oCurrencyName.length ; i < max; i++)
                {
                    var sCurrencyData = oCurrencyName[i];
                    if(sCurrencyData.id == iCurrencyID)
                    {
                        sReturn = sCurrencyData.name;
                    }
                }
            }
            return sReturn;
        },

        /**
         * bind_search_event
         * @description This function is for binding search event
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        bind_search_event : function(){
            
            uiSearchDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);

            uiSearchBtn.on('click', function(){
                var sSearchField = uiSearchDropdown.find('.frm-custom-dropdown-txt').find('input').val();
                if(sSearchField == 'ESOP Name')
                {
                    if(uiSearchnameInput.val().length >= 0)
                    {   
                        roxas.claims.assemble_search_params(uiSearchBtn, sSearchField, uiSearchnameInput.val());
                    }
                }
                else if(sSearchField == 'Vesting Years')
                {
                    if(uiSearchVestingYearsInput.val().length > 0)
                    {
                        roxas.claims.assemble_search_params(uiSearchBtn, sSearchField, uiSearchVestingYearsInput.val());
                    }
                }
                else if(sSearchField == 'Grant Date')
                {
                    var sDateFrom = uiSearchGrantFromInput.val();
                    var bValidGrantDateFrom = cr8v.validate_date(sDateFrom, 'MM/DD/YYYY');

                    var sDateTo = uiSearchGrantToInput.val();
                    var bValidGrantDateTo = cr8v.validate_date(sDateTo, 'MM/DD/YYYY');

                    if(bValidGrantDateFrom && bValidGrantDateTo)
                    {
                        roxas.claims.assemble_search_params(uiSearchBtn, sSearchField);
                    }
                }
                else if(sSearchField == 'Price per Share')
                {
                    if(uiSearchPricePerShareinput.val().length > 0)
                    {
                        roxas.claims.assemble_search_params(uiSearchBtn, sSearchField, uiSearchPricePerShareinput.val());
                    }
                }
                else if(sSearchField == 'Status')
                {
                    var uiClaimStatus = $('input[name="claim_status"]').val();
                    var sClaimStatus = 0;
                    if(uiClaimStatus != "All")
                    {
                        
                        if(uiClaimStatus == 'Pending')
                        {
                           sClaimStatus = 0;
                        }
                        else if(uiClaimStatus == 'Completed')
                        {
                           sClaimStatus = 2;
                        }
                        else if(uiClaimStatus == 'Rejected')
                        {
                           sClaimStatus = 3;
                        }
                        else if(uiClaimStatus == 'For Finalization')
                        {
                           sClaimStatus = 1;
                        }
                        roxas.claims.assemble_search_params(uiSearchBtn, sSearchField, sClaimStatus);
                    }
                    else if(uiClaimStatus == "All")
                    {
                        sClaimStatus = ""; 
                        roxas.claims.assemble_search_params(uiSearchBtn, sSearchField, sClaimStatus);
                    }
                }
            });

            uiSearchnameInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchnameInput.siblings('.search_btn').trigger('click');
                }
            });

            uiSearchVestingYearsInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchVestingYearsInput.siblings('.search_btn').trigger('click');
                }
            });

            uiSearchGrantFromInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchGrantFromInput.siblings('.search_btn').trigger('click');
                }
            });

            uiSearchGrantToInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchGrantToInput.siblings('.search_btn').trigger('click');
                }
            });

            uiSearchPricePerShareinput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchPricePerShareinput.siblings('.search_btn').trigger('click');
                }
            });

        },

        /**
         * assemble_search_params
         * @description This function is for assembling params for search
         * @dependencies 
         * @param {jQuery} uiButton
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        assemble_search_params : function(uiButton, sSearchField , sSearchValue, sSorting, sSortField){
            /*sample params*/
            /*{
                "keyword" : "string", 
                "search_field" : "string", 
                "sorting" : "string", 
                "sort_field" : "string", 
                "offset" : "int", 
                "limit" : "int"
            }*/

            var oGetParams = {
                'where' : []
            };

            if(sSearchField == 'ESOP Name')
            {
                oGetParams.search_field = 'name';
                oGetParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Vesting Years')
            {
                oGetParams.search_field = 'vesting_years';
                oGetParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Grant Date')
            {
                var sDateFrom = moment(uiSearchGrantFromInput.val(), 'MM/DD/YYYY').format('YYYY-MM-DD');
                var sDateTo = moment(uiSearchGrantToInput.val(), 'MM/DD/YYYY').format('YYYY-MM-DD');

                sSearchValue = "'" + sDateFrom + "' AND '" + sDateTo + "'";

                oGetParams.search_field = 'grant_date';
                oGetParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Price per Share')
            {
                oGetParams.search_field = 'price_per_share';
                oGetParams.keyword = sSearchValue;
            }

            else if(sSearchField == 'Status')
            {
                oGetParams.search_field = 'claim_status';
                oGetParams.keyword = sSearchValue;
            }

            if(cr8v.var_check(sSorting))
            {
                oGetParams.sorting = sSorting;
            }

            if(cr8v.var_check(sSortField))
            {
                oGetParams.sort_field = sSortField;
            }

            roxas.claims.get(oGetParams, uiButton);
        },

        /**
         * bind_sorting_events
         * @description This function is for binding events for sorting
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        bind_sorting_events : function(){
            uiSortContainer.on('click', '.sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');

                cr8v.clear_sorting_classes(uiSortContainer.find('.sort_field'));

                if(uiGridContainer.is(':visible'))
                {
                    var uiTemplatesGrid = uiGridContainer.find('.tmp_grid:not(.template)');                    
                    cr8v.ui_sorting(uiThis, sSortBy, uiTemplatesGrid.length, uiTemplatesGrid, uiGridContainer);
                }
                else
                {
                    var uiTemplatesList = uiListContainer.find('.tmp_list:not(.template)');
                    cr8v.ui_sorting(uiThis, sSortBy, uiTemplatesList.length, uiTemplatesList, uiListContainer);
                }

            });
            
            cr8v.clear_sorting_classes(uiSortContainer.find('.sort_field'));

            $(".view-by .grid").off("click").on("click", function() {       
                cr8v.clear_sorting_classes(uiSortContainer.find('.sort_field'));
                
                $(".grid-content").fadeIn();
                $(".table-content").fadeOut();


                $(".dash-table").fadeOut();
                $(".dash-grid").fadeIn();

                $(this).css({
                    'color':'#BDC3C7',
                    'font-size':'20px'
                });
                $(this).next(".list").css({
                    'font-size':'15px',
                    'color':'#fff'
                });
            });

            $(".view-by .list").off("click").on("click", function() {
                cr8v.clear_sorting_classes(uiSortContainer.find('.sort_field'));

                $(".table-content").fadeIn();
                $(".grid-content").fadeOut();   

                $(".dash-grid").fadeOut();
                $(".dash-table").fadeIn();  

                $(this).css({
                    'color':'#BDC3C7',
                    'font-size':'22px'
                });
                $(this).prev(".grid").css({
                    'font-size':'15px',
                    'color': '#fff'
                });
            });

        },

        /**
         * view_data
         * @description This function is for populating view data
         * @dependencies 
         * @param oData
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        view_data : function(iClaimID){
            if(cr8v.var_check(iClaimID))
            {
                localStorage.removeItem("claim_id");
                localStorage.setItem('claim_id', iClaimID);
                window.location =  roxas.config('url.server.base') + "claims/view";
            }
        },

        /**
         * supply_template_view
         * @description This function will put the data of the claim list view
         * @dependencies N/A
         * @param {object} oData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        supply_template_view : function(oData){
            var uiTemplate = uiViewPage;
            if(cr8v.var_check(oData) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.data('data-esop-id',oData.esop_id);
                uiTemplate.data('data-claim-id',oData.id);
                uiTemplate.data('data-esop-name',oData.esop_name);
                uiTemplate.data('data-user-id',oData.user_id);
                uiTemplate.data('data-user-name',oData.first_name+' '+oData.last_name);

                var sCurrencyName = cr8v.var_check(oData.currency_name) ? oData.currency_name : roxas.claims.get_currency_name(oData.currency) ;
                var sPricePerShare = sCurrencyName +' '+ oData.price_per_share;
                var sGrantDate = moment(oData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.find('[data-label="employee_name"]').text(oData.first_name+' '+oData.last_name);
                uiTemplate.find('[data-label="employee_code"]').text(oData.employee_code);
                uiTemplate.find('[data-label="esop_name"]').text(oData.esop_name);
                uiTemplate.find('[data-label="outstanding_balance"]').text(oData.value_of_shares);
                uiTemplate.find('[data-label="company_name"]').text(oData.company_name);
                uiTemplate.find('[data-label="department_name"]').text(oData.department_name);
                uiTemplate.find('[data-label="rank_name"]').text(oData.rank_name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="no_of_shares"]').text(oData.no_of_shares);

                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oData.total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiTemplate.find('[data-label="year_no"]').text('Year '+oData.year_no);

                var iValueOfShares = parseFloat(oData.value_of_shares) * parseFloat(oData.vesting_years);
                uiTemplate.find('[data-label="value_of_shares"]').text(number_format(iValueOfShares, 2) +' Shares');
                
                /*populate vesting rights table*/
                var uiVRListContainer = $('[data-container="vr_list_view"]');
                if(oData.data_vesting_rights.length > 0)
                    {
                        for(var i = 0, max = oData.data_vesting_rights.length -1; i <= max; i++)
                        {
                            var oVRData = oData.data_vesting_rights[i];

                            var uiTemplateList = $('.tmp_vr_list.template').clone().removeClass('template hidden');
                            var uiManipulatedTemplateList = roxas.claims.manipulate_vesting_rights_list(oVRData, uiTemplateList);

                            uiVRListContainer.prepend(uiManipulatedTemplateList);

                        }
                        var iTotalNumberOfShares = parseFloat(oData.no_of_shares) * parseFloat(oData.vesting_years);
                        uiTemplate.find('[data-label="total_no_of_shares"]').text(number_format(iTotalNumberOfShares, 2));
                
                    }
                
                /*populate user payments table summary*/
                var uiUPListContainer = $('[data-container="up_list_view"]');
                if(oData.data_user_payments_summary.length > 0)
                    { 
                        var iTotalPayment = 0;
                        for(var i = 0, max = oData.data_user_payments_summary.length -1; i <= max; i++)
                        {
                            var oUPData = oData.data_user_payments_summary[i];

                            var uiTemplateList = $('.tmp_up_list.template').clone().removeClass('template hidden');
                            var uiManipulatedTemplateList = roxas.claims.manipulate_user_payments_list(oUPData, uiTemplateList);

                            uiUPListContainer.prepend(uiManipulatedTemplateList);
                            
                            iTotalPayment += parseFloat(oUPData.amount_paid);
                        }
                        var iOBTotal = oData.value_of_shares - iTotalPayment;

                        //MALEEEEEEEEEEEE bakit biglang template josko
                        //uiTemplate.find('[data-label="total_payment"]').text(number_format(iTotalPayment, 2));

                        uiUPListContainer.find('[data-label="total_payment"]').text(number_format(iTotalPayment, 2));
                        uiUPListContainer.find('[data-label="ob_as_of"]').text(moment().format('MMMM DD, YYYY'));
                        uiUPListContainer.find('[data-label="ob_as_of_total"]').text(number_format(iOBTotal, 2))
                    }
                    else
                    {
                            uiUPListContainer.find('tr.no_record_found').removeClass('hidden');
                    }

                 /*populate payment application*/
                 var uiPAListContainer = $('[data-container="payment_application_content"]');
                 if(oData.data_claim_vesting_rights.length > 0)
                    {
                        $('.payment_application_header').removeClass('hidden');
                        var sVestingYears = '';
                        var iCVRTotal = 0;
                        var sVestingRightsID = '';
                        for(var i = 0, max = oData.data_claim_vesting_rights.length -1; i <= max; i++)
                        {
                            var oPAData = oData.data_claim_vesting_rights[i];

                            var uiTemplate = $('.tmp_payment_application_container.template').clone().removeClass('template hidden');
                            var uiManipulatedTemplateList = roxas.claims.manipulate_user_payment_applications_container(oPAData, uiTemplate);

                            uiPAListContainer.prepend(uiManipulatedTemplateList);
                            
                            iTotalPayment += parseFloat(oPAData.amount_paid);
                            sVestingYears += ' Year '+ oPAData.year_no + ', ';
                            iCVRTotal += parseFloat(oPAData.value_of_shares);
                            sVestingRightsID += oPAData.vesting_rights_id+',';
                        }
                        
                        sVestingYears = sVestingYears.substring(0, sVestingYears.length-2);
                        uiViewPage.find('[data-label="vesting_years"]').text(sVestingYears);
                        uiViewPage.data('data-cvr-total',iCVRTotal);
                        uiViewPage.data('data-vesting-rights-ids',sVestingRightsID);
                        //console.log(sVestingRightsID);
                    }
                
                if(oData.data_claim_attachments.length > 0)
                    {
                        /*unsetting send_claim_esop_to_admin if there are already data*/
                        uiViewPage.find('[data-container="send_claim_esop_container"]').remove();

                        /*populate claim attachment*/
                        var oClaimAttachmentData = oData.data_claim_attachments[0];
                        var uiClaimAttachmentContainer = uiViewPage.find('[data-container="claim_attachment_container"]').removeClass('hidden');
                        var sDateOfOR = oClaimAttachmentData.date_of_or;
                        var sDateCreated = moment(oClaimAttachmentData.date_created, 'YYYY-MM-DD').format('MMMM DD, YYYY');
                        var sDLink = roxas.config('url.server.base')+oClaimAttachmentData.path;
                        var sCurrencyName = cr8v.var_check(oClaimAttachmentData.currency_name) ? oClaimAttachmentData.currency_name : roxas.claims.get_currency_name(oClaimAttachmentData.currency) ;
                        var sAmountPaid = number_format(oClaimAttachmentData.amount_paid, 2) +' '+ sCurrencyName;
                        var sNewDateOR = "";
                                

                         console.log(JSON.stringify(oClaimAttachmentData));

                        //  console.log(JSON.stringify(oClaimAttachmentData));

                
                         if(sDateOfOR != "0000-00-00")
                         {
                                sNewDateOR = moment(oClaimAttachmentData.date_of_or, 'YYYY-MM-DD').format('MMMM DD, YYYY');
                         }else{
                                sNewDateOR = "";
                         }

                       
                        uiClaimAttachmentContainer.find('[data-label="date_of_or"]').text(sNewDateOR);
                        uiClaimAttachmentContainer.find('[data-label="date_created"]').text(sDateCreated);
                        uiClaimAttachmentContainer.find('[data-label="amount_paid"]').text(sAmountPaid);

                        if(oClaimAttachmentData.document_type != "" && oClaimAttachmentData.path != "")
                        {
                            if(oClaimAttachmentData.document_name != "")
                            {
                                uiClaimAttachmentContainer.find('[data-label="document_name"]').text(oClaimAttachmentData.document_name);
                                uiClaimAttachmentContainer.find('[data-label="document_name_type"]').text(oClaimAttachmentData.document_name +'.'+ oClaimAttachmentData.document_type);
                            }else{

                                uiClaimAttachmentContainer.find('[data-label="document_name_type"]').text("OR File" +'.'+ oClaimAttachmentData.document_type);

                                uiClaimAttachmentContainer.find('[data-label="document_name"]').text("");
                            }

                            uiClaimAttachmentContainer.find("#display-or-attachment").show();


                            uiClaimAttachmentContainer.find('[data-label="download_attachment"]').attr('href',sDLink).attr('target','_blank');
                            
                           
                        }else{

                                 uiClaimAttachmentContainer.find('[data-label="document_name"]').text("");
                                 uiClaimAttachmentContainer.find("#display-or-attachment").hide();
                        }
                       
                        
                    }
                    else
                    {
                          /*admin hide buttons if already approved*/
                          uiViewPage.find('[modal-target="reject-claim"]').remove();
                          uiViewPage.find('[modal-id="reject-claim"]').remove();
                          uiViewPage.find('[modal-target="approve-claim"]').remove();
                          uiViewPage.find('[modal-id="approve-claim"]').remove();
                    }

                   /*buttons*/

                     if(oData.claim_status == 1)//for finalization
                    {  
                          /*admin hide buttons if already approved*/
                          uiViewPage.find('[modal-target="reject-claim"]').remove();
                          uiViewPage.find('[modal-id="reject-claim"]').remove();
                          uiViewPage.find('[modal-target="approve-claim"]').remove();
                          uiViewPage.find('[modal-id="approve-claim"]').remove();  

                         /*hr add buttons if already approved*/
                         uiViewPage.find('[data-container="mark_claim_as_completed"]').removeClass('hidden');

                         var iDateCompleted = moment().format('MM/DD/YYYY');
                         uiMarkClaimAsCompleted.find('input[name=date_completed]').val(iDateCompleted);
                    }
                    else if(oData.claim_status == 2)//for finalization
                    {
                         /*admin hide buttons if already approved*/
                          uiViewPage.find('[modal-target="reject-claim"]').remove();
                          uiViewPage.find('[modal-id="reject-claim"]').remove();
                          uiViewPage.find('[modal-target="approve-claim"]').remove();
                          uiViewPage.find('[modal-id="approve-claim"]').remove();   
                    }


                if(localStorage.getItem('soa_from_personal') != null)
                {
                    if(localStorage.getItem('soa_from_personal') == 'true')
                    {
                        $('#divcontents').parents('.emp-claim-cont:first').remove();
                        $('[data-container="soa_buttons_container"]').removeClass('hidden');
                        $('[data-container="send_claim_esop_container"]').remove();
                        $('[data-container="mark_claim_as_completed"]').remove();
                        $('[data-container="esop_admin_buttons_conter"]').remove();
                    }
                }

                roxas.claims.populate_claim_form_employee(oData);
            }
        },

        populate_claim_form_employee : function(oData) {
            var iTotalPayment = 0,
                iTotalShares = 0;

            for (var i = 0 ; i < oData.data_claim_vesting_rights.length ; i++)
            {
                 iTotalPayment += parseFloat(oData.data_claim_vesting_rights[i].no_of_shares);
                 iTotalShares += parseFloat(oData.data_claim_vesting_rights[i].value);   
            }

            var $claim_employee_form = $('div.emp-form');

            $claim_employee_form.find('[data-label="claim_no_of_shares"]').html(cr8v.var_check(iTotalPayment) ? number_format(iTotalPayment, 2) : '');
            $claim_employee_form.find('[data-label="claim_employee_name"]').html(cr8v.var_check(oData.first_name) ? oData.first_name + ' ' + oData.middle_name + ' ' + oData.last_name : '');
            $claim_employee_form.find('[data-label="claim_date_claimed"]').html(moment().format('MMMM DD, YYYY'));
            $claim_employee_form.find('[data-label="claim_esop_name"]').html(cr8v.var_check(oData.esop_name) ? oData.esop_name : '');
            
        },

        /**
         * manipulate_vesting_rights_list
         * @description This function will put the data of the vr list view
         * @dependencies N/A
         * @param {object} oData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        manipulate_vesting_rights_list : function(oData, uiTemplate){
            if(cr8v.var_check(oData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oData.currency_name) ? oData.currency_name : roxas.claims.get_currency_name(oData.currency) ;
                var sPricePerShare = sCurrencyName +' '+ oData.price_per_share;
                var sYear = moment(oData.year, 'YYYY-MM-DD').format('MMMM DD, YYYY'); 
                
                uiTemplate.find('[data-label="year_no"]').text(oData.year_no);
                uiTemplate.find('[data-label="year"]').text(sYear);
                uiTemplate.find('[data-label="percentage"]').text(oData.percentage);
                uiTemplate.find('[data-label="no_of_shares"]').text(number_format(oData.no_of_shares, 2));
                uiTemplate.find('[data-label="value_of_shares"]').text(number_format(oData.value_of_shares, 2));
                
                return uiTemplate;
            }
        },

        /**
         * manipulate_user_payments_list
         * @description This function will put the data of the up list view
         * @dependencies N/A
         * @param {object} oData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        manipulate_user_payments_list : function(oData, uiTemplate){
            if(cr8v.var_check(oData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oData.currency_name) ? oData.currency_name : roxas.claims.get_currency_name(oData.currency) ;
                var sAmountPaid = sCurrencyName +' '+ oData.amount_paid;
                var sDate = moment(oData.date_of_or, 'YYYY-MM-DD').format('MMMM DD, YYYY'); 
                
                uiTemplate.find('[data-label="id"]').text(oData.id);
                uiTemplate.find('[data-label="payment_name"]').text(oData.payment_name);
                uiTemplate.find('[data-label="date_of_or"]').text(sDate);
                uiTemplate.find('[data-label="amount_paid"]').text(number_format(sAmountPaid, 2) + ' ' +sCurrencyName);

                return uiTemplate;
            }
        },

        /**
         * manipulate_user_payment_applications_container
         * @description This function will put the data of the up payment applications
         * @dependencies N/A
         * @param {object} oData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        manipulate_user_payment_applications_container : function(oData, uiTemplate){
            if(cr8v.var_check(oData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oData.currency_name) ? oData.currency_name : roxas.claims.get_currency_name(oData.currency) ;
                var sAmountPaid = sCurrencyName +' '+ oData.amount_paid;
                var sYear = moment(oData.year, 'YYYY-MM-DD').format('MMMM DD, YYYY'); 
                
                uiTemplate.attr('data-vesting-rights-id',oData.vesting_rights_id);
                uiTemplate.find('[data-label="id"]').text(oData.id);
                uiTemplate.find('[data-label="year"]').text(sYear);
                uiTemplate.find('[data-label="value_of_shares"]').text(number_format(oData.value_of_shares, 2) + ' ' +sCurrencyName);
                uiTemplate.find('[data-label="year_no"]').text(oData.year_no);
                uiTemplate.find('[data-label="no_of_shares"]').text(number_format(oData.no_of_shares, 2));
                
                /*populate user payments table*/
                var uiUPListContainer = $('[data-container="pa_list_view"]');
                if(oData.data_user_payments.length > 0)
                    {
                        var iTotalPayment = 0;
                        for(var i = 0, max = oData.data_user_payments.length -1; i <= max; i++)
                        {
                            var oUPData = oData.data_user_payments[i];
                            //DALAWA YUNG CNCLONE MO BANOOOOOOOOOOOOOOBS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            //var uiTemplateList = $('.tmp_pa_list.template').clone().removeClass('template hidden');
                            var uiTemplateList = $('.tmp_pa_list.template:first').clone().removeClass('template hidden');
                            var uiManipulatedTemplateList = roxas.claims.manipulate_user_payments_list(oUPData, uiTemplateList);

                            uiTemplate.find('[data-container="pa_list_view"]').prepend(uiManipulatedTemplateList);
                            
                            iTotalPayment += parseFloat(oUPData.amount_paid);
                        }

                        var iOBTotal = oData.value_of_shares - iTotalPayment;

                        uiTemplate.find('[data-label="total_payment"]').text(number_format(iTotalPayment, 2));
                        uiTemplate.find('[data-label="ob_as_of"]').text(moment().format('MMMM DD, YYYY'));
                        uiTemplate.find('[data-label="ob_as_of_total"]').text(number_format(iOBTotal, 2))

                        // ANO TO BAKIT Sa PNKA CONTAINER YUNG MGA MNAMANIPULATE MO???? DB DPAT UNG NSA TEMPLATE LANG?
                        //uiUPListContainer.find('[data-label="total_payment"]').text(number_format(iTotalPayment, 2));
                        //uiUPListContainer.find('[data-label="ob_as_of"]').text(moment().format('MMMM DD, YYYY'));
                        //uiUPListContainer.find('[data-label="ob_as_of_total"]').text(number_format(iOBTotal, 2))

                    }
                    else
                    {
                            // ANO TO BAKIT Sa PNKA CONTAINER YUNG MGA SHNSHOW MO???? DB DPAT UNG NSA TEMPLATE LANG?
                            //uiUPListContainer.find('tr.no_record_found').removeClass('hidden');

                        uiTemplate.find('tr.no_record_found').removeClass('hidden');
                    }

                return uiTemplate;
            }
        },

        /**
         * validate_send_claim_esop_to_admin
         * @description This function will validate_send_claim_esop_to_admin
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        validate_send_claim_esop_to_admin : function(){
            var iError = 0;
            var sErrorMsg = '';
            var iCVRTotal = uiViewPage.data('data-cvr-total');
            console.log(iCVRTotal);
            var uiDocumentName = uiSendClaimFormModal.find('input[name="document_name"]');
            var uiAmountPaid = uiSendClaimFormModal.find('input[name="amount_paid"]');
            var uiCurrency = uiSendClaimFormModal.find('div#currency_dropdown').find('input.dd-txt');
            var uiDateOfOR = uiSendClaimFormModal.find('input[name="date_of_or"]');
            var uiClaimAttachment = uiSendClaimFormModal.find('input[name="claim_attachment"]'); 
            
                //             if(uiDocumentName.val() == '')
                //             {
                //                sErrorMsg += 'OR number is required.<br/>';
                //                iError++;
                //             }

             if(uiDocumentName.val() != '' && uiDateOfOR.val() == '')
            {
               sErrorMsg += 'Date of OR is required.<br/>';
               iError++;
            }

            if(uiAmountPaid.val() == '')
            {
               sErrorMsg += 'Amount paid is required.<br/>';
               iError++;
            }
            else if(uiAmountPaid.val() > iCVRTotal)
            {
               sErrorMsg += 'Amount paid must be greater than Outstanding Balance.<br/>';
               iError++;
            }
            else if(uiAmountPaid.val() < iCVRTotal)
            {
               sErrorMsg += 'Amount paid must be greater than Outstanding Balance.<br/>';
               iError++;
            }

            if(uiCurrency.val() == '')
            {
               sErrorMsg += 'Currency is required.<br/>';
               iError++;
            }

//             if(uiDateOfOR.val() == '')
//             {
//                sErrorMsg += 'Date of OR is required.<br/>';
//                iError++;
//             }
        
            // if(uiClaimAttachment.val() == '')
                // {
                // sErrorMsg += 'Claim Attachment is required.<br/>';
                //  iError++;
                // }

            if(iError > 0)
            {
               uiSendClaimFormModal.find('.add_error_message').removeClass('hidden');
               uiSendClaimFormModal.find('.add_error_message').html(sErrorMsg);
               return false;     
            }
            else
            {
               uiSendClaimFormModal.find('.add_error_message').addClass('hidden');
               uiSendClaimFormModal.find('.add_error_message').html('');
               return true;
            }
    
        },

        /**
         * send_claim_esop_to_admin
         * @description This function will send_claim_esop_to_admin
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        send_claim_esop_to_admin : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "claims/send_claim_esop_to_admin",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        
                        if(cr8v.var_check(oData))
                        {console.log(oData);
                            if(oData.status == true)
                            {
                                uiSendClaimFormModal.find('.add_error_message').addClass('hidden').html('');
                                uiSendClaimFormModal.find('.add_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiSendClaimFormModal.hasClass('showed'))
                                    {
                                        uiSendClaimFormModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'claims/index';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiSendClaimFormModal.find('.add_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
            }
        },

        /**
         * reject_esop_claim
         * @description This function will reject_esop_claim
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        reject_esop_claim : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "claims/reject_esop_claim",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiRejectClaimFormModal.find('.error_message').addClass('hidden').html('');
                                uiRejectClaimFormModal.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiRejectClaimFormModal.hasClass('showed'))
                                    {
                                        uiRejectClaimFormModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'claims/index';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiRejectClaimFormModal.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
            }
        },

        /**
         * approve_esop_claim
         * @description This function will approve_esop_claim
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        approve_esop_claim : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "claims/approve_esop_claim",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiApproveClaimFormModal.find('.error_message').addClass('hidden').html('');
                                uiApproveClaimFormModal.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiApproveClaimFormModal.hasClass('showed'))
                                    {
                                        uiApproveClaimFormModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'claims/index';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiApproveClaimFormModal.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
            }
        },

        /**
         * mark_claim_as_completed
         * @description This function will mark_claim_as_completed
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        mark_claim_as_completed : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "claims/mark_claim_as_completed",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiMarkClaimAsCompleted.find('.error_message').addClass('hidden').html('');
                                uiMarkClaimAsCompleted.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiMarkClaimAsCompleted.hasClass('showed'))
                                    {
                                        uiMarkClaimAsCompleted.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'claims/index';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiMarkClaimAsCompleted.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.claims.ajax(oAjaxConfig);
            }
        },

        

       
    }

}());

$(window).load(function () {
    roxas.claims.initialize();
});
