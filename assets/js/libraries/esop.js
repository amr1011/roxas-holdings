/**
 *  Esop Class
 *
 *
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var
        uiCurrencyDropdown,
        uiClassCurrencyDropdown,

        uiAddEsopForm,
        uiAddEsopModal,
        uiAddEsopBtn,

        uiEsopListContainer,
        uiEsopGridContainer,

        uiNoResultsFound,

        uiEditEsopForm,
        uiEditEsopModal,
        uiEditEsopBtn,

        oCurrencyName = {},

        uiSearchEsopBtn,
        uiEsopSearchDropdown,
        uiSearchEsopnameInput,
        uiSearchEsopVestingYearsInput,
        uiSearchEsopGrantFromInput,
        uiSearchEsopGrantToInput,
        uiSearchEsopPricePerShareinput,

        uiSortEsopContainer,

        uiOfferLetterDropdown,
        uiAcceptanceLetterDropdown,

        uiEsopCompaniesContainer,
        uiEsopEmployeesContainer,

        uiFinalizeAddDistributionSharesBtn,

        uiFinalizeEditDistributionSharesBtn,

        uiSetOfferExpBtn,
        uiSetOfferExpModal,
        uiSetOfferExpModalBtn,
        uiSetOfferExpForm,

        uiSortEsopEmployeesContainer,

        uiSearchStockOfferBtn,
        uiStockOfferSearchDropdown,
        uiSearchStockOffernameInput,
        uiSearchStockOfferVestingYearsInput,
        uiSearchStockOfferGrantFromInput,
        uiSearchStockOfferGrantToInput,
        uiSearchStockOfferPricePerShareinput,
        uiFilterByCompany,

        uiViewAcceptanceLetterBtn,

        uiAcceptOfferModal,

        uiAcceptanceLetterContainer,
        uiOfferLetterContainer,

        uiBackToOfferLetterContainerBtn,
        uiSendLetterBtn,

        uiOfferLetterHTMLContainer,
        uiAcceptanceLetterHTMLContainer,

        uiPrintOfferLetterBtn,
        uiPrintAcceptanceLetterBtn,

        $vestingRightClaimContainer,
        $vestingRightTemplate,
        $vestingRightClaimModal,
        $vestingRightClaimButton,

        $paymentSummaryContainer,

        $claimDocumentsModal,
        $claimDocumentsTemplate,
        $claimDocumentsContainer,
        $claimDocumentButton,

        $claimFormPaymentTrContainer,
        $claimFormPaymentContainer,
        $claimFormPaymentTrTemplate,
        $claimFormPaymentTemplate,

        filler,

        uiTriggerUploadShareDistribution,
        uiUploadShareDistributionModal,
        uiUploadShareDistributionModalBtn,
        uiUploadShareDistributionBtn,
        uiUploadShareDistributionContainer,
        uiUploadShareDistributionErrorContainer,
        uiConfirmUploadBtn,
        uiNoErrorsFound,
        uiCancelUploadBtn,
        sShareDistributionFilePath,
        sShareDistributionFileName,

        uiEsopSendLetterModal,
        uiEsopSendLetterModalBtn,
        uiEsopSendLetterBtn,

        uiEsopBatchContainer,
        uiEsopBatchTabContainer,

        uiAddPaymentRecordModal,

        uiPaymentDropdown,
        uiEsopListDropdown,
        uiClassPaymentDropdown,
        oPaymentName = {},


        uiCreateEsopBatchModal,
        uiCreateEsopBatchModalBtn,
        uiCreateEsopBatchBtn,

        uiAddGratuityModal,

        uiGratuityListContainer = $(''),


        uiEditEsopBatchForm, 
        uiEditEsopBatchModal,
        uiEditEsopBatchBtn,
        uiEditEsopBatchModalBtn,

        uiBatchAddDistributeSharesBtn,
        uiBatchEditDistributeSharesBtn,

        uiSetOfferExpBatchBtn,
        uiSetOfferExpBatchModal,
        uiSetOfferExpBatchModalBtn,
        uiSetOfferExpBatchForm,

        uiEsopSendLetterBatchModal,
        uiEsopSendLetterBatchModalBtn,
        uiEsopSendLetterBatchBtn,

        uiApproveGratuityModal,
        uiRejectGratuityModal,
        uiSendGratuityToEmployeesModal,
        uiViewGratuityRejectReasonModal,
        
        uiSendGratuityModal,
        uiViewGratuityRejectReasonModal,

        iCreateEsopBatchID = 0,

        uiTriggerBatchUploadShareDistribution,
        uiUploadBatchShareDistributionModal,
        uiUploadBatchShareDistributionModalBtn,
        uiUploadBatchShareDistributionBtn,
        uiUploadBatchShareDistributionContainer,
        uiUploadBatchShareDistributionErrorContainer,
        uiConfirmBatchUploadBtn,
        uiBatchNoErrorsFound,
        uiCancelBatchUploadBtn,
        sBatchShareDistributionFilePath,
        sBatchShareDistributionFileName,
        sGeneratedCode,

        uiAddNewEmployeeModal,
        uiAddNewEmployeeModalBtn,
        uiAddNewEmployeeForm,
        uiAddNewEmployeeBtn,
        uiClassCompanyDropdown,
        uiClassEmployeeDropDown,
        uiClassEsopDropDown,


        $shareAllotmentContainer,
        $shareAllotmentTemplate,

        uiPersonalStocksContainer,
        uiPersonalStocksTabContainer,

        uiHRDeptEsopContainer,
        uiHRDeptEsopTabContainer,
        
        uiEsopAdminGridTabContainer,
        uiEsopAdminListTabContainer,
        uiClassCompanyFilterDashboard,

        uiTriggerUploadPaymentFile,
        uiUploadPaymentModalBtn,
        uiUploadPaymentModal,
        uiCancelUploadPaymentBtn,
        uiConfirmUploadPaymentBtn,
        uiOpenPaymentFileBtn,
        sUploadPaymentFilePath = '',
        sUploadPaymentFileName = '',
        uiUploadPaymentContainer,
        uiPaymentNoErrorsFound,
        uiUploadPaymentErrorContainer,
        uiPaymentUploadEsopDropdown,
        uiTableRoxas,
        uiPaymentMethodDropdown,
        uiGroupWideSearchInput,
        iExpiredEsop = 0,
        filler,

        uiCompanyDropdown,
        uiUsersDropdown,
        uiAddPaymentPerUser,
        uiPaymentTable,
        uiGratuityTable;


    CPlatform.prototype.esop = {

        initialize : function(){
            /*declare variables*/

            uiTableRoxas = $('table.table-roxas');
            uiPaymentMethodDropdown = $('#payment_method_dropdown');
            uiCurrencyDropdown = $('#currency_dropdown');

            uiEsopListDropdown = $('.esop_dropdown');
            uiClassCurrencyDropdown = $('.currency_dropdown');

            uiPaymentDropdown = $('#payment_method_dropdown');

            uiClassPaymentDropdown = $('.payment_method_dropdown');

            uiAddEsopForm = $('#add_esop_form');
            uiAddEsopModal = $('[modal-id="add-esop"]');
            uiAddEsopBtn = $('#add_esop_btn');

            uiEsopListContainer = $('[data-container="esop_view_by_list"]');
            uiEsopGridContainer = $('[data-container="esop_view_by_grid"]');

            uiNoResultsFound = $('.no_results_found.template');

            uiEditEsopForm = $('#edit_esop_form');
            uiEditEsopModal = $('[modal-id="edit-esop"]');
            uiEditEsopBtn = $('#edit_esop_btn');

            uiSearchEsopBtn = $('.search_esop_btn');
            uiEsopSearchDropdown = $('#search_esop_dropdown');
            uiSearchEsopnameInput = $('.search_esop_name_input');
            uiSearchEsopVestingYearsInput = $('.search_vesting_years_input');
            uiSearchEsopGrantFromInput = $('.search_grant_date_from_input');
            uiSearchEsopGrantToInput = $('.search_grant_date_to_input');
            uiSearchEsopPricePerShareinput = $('.search_price_per_share_input');

            uiSortEsopContainer = $('#sort_esop');

            uiOfferLetterDropdown = $('.offer_letter_dropdown');
            uiAcceptanceLetterDropdown = $('.acceptance_letter_dropdown');

            uiEsopCompaniesContainer = $('[data-container="esop_companies_container"]');
            uiEsopEmployeesContainer = $('[data-container="esop_employees_container"]');

            uiFinalizeAddDistributionSharesBtn = $('#finalize_add_distribute_shares_btn');

            uiFinalizeEditDistributionSharesBtn = $('#finalize_edit_distribute_shares_btn');

            uiSetOfferExpBtn = $('#set_expiration_button');
            uiSetOfferExpModal = $('[modal-id="offer-expiration"]');
            uiSetOfferExpModalBtn = $('[modal-target="offer-expiration"]');
            uiSetOfferExpForm = $('#set_expiration_form');

            uiSortEsopEmployeesContainer = $('.sort_esop_employees');

            uiSearchStockOfferBtn = $('.search_stock_offer_btn');
            uiStockOfferSearchDropdown = $('#search_stock_offer_dropdown');
            uiSearchStockOffernameInput = $('.search_esop_name_stock_offer_input');
            uiSearchStockOfferVestingYearsInput = $('.search_vesting_years_stock_offer_input');
            uiSearchStockOfferGrantFromInput = $('.search_grant_date_from_stock_offer_input');
            uiSearchStockOfferGrantToInput = $('.search_grant_date_to_stock_offer_input');
            uiSearchStockOfferPricePerShareinput = $('.search_price_per_share_stock_offer_input');
            uiFilterByCompany = $('.filter-by-company');

            uiViewAcceptanceLetterBtn = $('#view_acceptance_offer_btn');

            uiAcceptOfferModal = $('[modal-id="accept-offer"]');

            uiOfferLetterContainer = $('[data-container="offer_letter"]');
            uiAcceptanceLetterContainer = $('[data-container="acceptance_letter"]');

            uiBackToOfferLetterContainerBtn = $('#back_to_offer_letter_btn');
            uiSendLetterBtn = $('#send_letter_btn');

            uiOfferLetterHTMLContainer = $('[data-container="offer_letter_html_container"]');
            uiAcceptanceLetterHTMLContainer = $('[data-container="acceptance_letter_html_container"]');

            uiPrintOfferLetterBtn = $('#print_offer_letter');
            uiPrintAcceptanceLetterBtn = $('#print_acceptance_letter');

            $vestingRightClaimModal = $('[modal-id="vesting-rights"]');
            $vestingRightClaimButton = $vestingRightClaimModal.find('button.submit_vesting_rights');    
            $vestingRightClaimContainer = $('tbody[data-container="vesting_rights_claim"]');
            $vestingRightTemplate = $('tr.vesting_rights_claim.template');

            $paymentSummaryContainer = $('tbody[data-container="summary_vesting"]');

            $claimDocumentsModal = $('[modal-id="claim-document"]');
            $claimDocumentsTemplate = $('.template.claim_documents');
            $claimDocumentsContainer = $('tbody[data-container="claim_documents"]');
            $claimDocumentButton = $claimDocumentsModal.find('button.view_claim_document');

            $claimFormPaymentContainer = $('data[data-container="payments_claim_form"]');
            $claimFormPaymentTemplate = $('data.claim_form.payments.template');

            uiTriggerUploadShareDistribution = $('#trigger_upload_file');
            uiUploadShareDistributionModal = $('[modal-id="share-distribution-template"]');
            uiUploadShareDistributionModalBtn = $('[modal-target="share-distribution-template"]');
            uiUploadShareDistributionBtn = $('#upload_shares');
            uiUploadShareDistributionContainer = $('[data-container="upload_share_distribution_container"]');
            uiUploadShareDistributionErrorContainer = $('[data-container="upload_share_distribution_error_container"]');
            uiConfirmUploadBtn = $('#confirm_upload_btn');
            uiNoErrorsFound = $('.no_upload_share_distribution_error');
            uiCancelUploadBtn = $('#cancel_upload_btn');

            uiEsopSendLetterModal = $('[modal-id="send-letter"]');
            uiEsopSendLetterModalBtn = $('[modal-target="send-letter"]');
            uiEsopSendLetterBtn = $('#esop_send_letter_btn');

            uiEsopBatchContainer = $('[data-container="esop_batch_container"]');
            uiEsopBatchTabContainer = $('[data-container="esop_batch_tab_container"]');

            uiAddPaymentRecordModal = $('div[modal-id="payment-record"]');

            uiCreateEsopBatchModal = $('[modal-id="esop-batch"]');
            uiCreateEsopBatchModalBtn = $('[modal-target="esop-batch"]');
            uiCreateEsopBatchBtn = $('#create_esop_batch_btn');

            uiAddGratuityModal = $('div[modal-id="add-gratuity"]');

            uiGratuityListContainer = $('[data-container="gratuity_list_container"]');

            uiEditEsopBatchForm = $('#edit_esop_batch_form');
            uiEditEsopBatchModal = $('[modal-id="edit-esop-batch"]');
            uiEditEsopBatchModalBtn = $('[modal-target="edit-esop-batch"]');
            uiEditEsopBatchBtn = $('#edit_esop_batch_btn');
        
            uiBatchAddDistributeSharesBtn = $('.batch_add_distribute_shares');
            uiBatchEditDistributeSharesBtn = $('.batch_edit_distribute_shares');

            uiSetOfferExpBatchBtn = $('#set_expiration_batch_button');
            uiSetOfferExpBatchModal = $('[modal-id="offer-expiration-batch"]');
            uiSetOfferExpBatchModalBtn = $('[modal-target="offer-expiration-batch"]');
            uiSetOfferExpBatchForm = $('#set_expiration_batch_form');

            uiEsopSendLetterBatchModal = $('[modal-id="send-letter-batch"]');
            uiEsopSendLetterBatchModalBtn = $('[modal-target="send-letter-batch"]');
            uiEsopSendLetterBatchBtn = $('#esop_send_letter_batch_btn');

            uiApproveGratuityModal = $('[modal-id="approve-gratuity"]');
            uiRejectGratuityModal = $('[modal-id="reject-grant"]');
            uiViewGratuityRejectReasonModal = $('[modal-id="view-reject-reason"]');
            uiSendGratuityToEmployeesModal = $('[modal-id="send-gratuity-to-employees"]');
            
            uiTriggerBatchUploadShareDistribution = $('#trigger_batch_upload_file');
            uiUploadBatchShareDistributionModal = $('[modal-id="batch-share-distribution-template"]');
            uiUploadBatchShareDistributionModalBtn = $('[modal-target="batch-share-distribution-template"]');
            uiUploadBatchShareDistributionBtn = $('#upload_batch_shares');
            uiUploadBatchShareDistributionContainer = $('[data-container="upload_batch_share_distribution_container"]');
            uiUploadBatchShareDistributionErrorContainer = $('[data-container="upload_batch_share_distribution_error_container"]');
            uiConfirmBatchUploadBtn = $('#confirm_batch_upload_btn');
            uiBatchNoErrorsFound = $('.no_upload_batch_share_distribution_error');
            uiCancelBatchUploadBtn = $('#cancel_batch_upload_btn');

            uiAddNewEmployeeModal = $('[modal-id="add-new-employee"]');
            uiAddNewEmployeeModalBtn = $('[modal-target="add-new-employee"]');
            uiAddNewEmployeeForm = $('#add_new_employee_form');
            uiAddNewEmployeeBtn = $('#add_new_employee_btn');
            uiClassCompanyDropdown = $('.company_dropdown');
            uiClassEmployeeDropDown = $('.employee_dropdown');
            uiClassEsopDropDown = $('.esop_dropdown');

            $shareAllotmentContainer = $('tbody[data-container="share_allotment"]');
            $shareAllotmentTemplate = $('tr[data-template="share_allotment"]');

            uiPersonalStocksContainer = $('[data-container="personal_stocks_container"]');
            uiPersonalStocksTabContainer = $('[data-container="personal_stocks_tab_container"]');

            uiHRDeptEsopContainer = $('[data-container="hr_department_esop_container"]');
            uiHRDeptEsopTabContainer = $('[data-container="hr_department_esop_tab_container"]');

            uiEsopAdminGridTabContainer = $('[data-container="hr_head_and_admin_esop_grid_tab_container"]');
            uiEsopAdminListTabContainer = $('[data-container="hr_head_and_admin_esop_list_tab_container"]');

            uiClassCompanyFilterDashboard = $('.company_filter');
        
            uiTriggerUploadPaymentFile = $('#trigger_payment_upload_file');
            uiUploadPaymentModalBtn = $('[modal-target="upload-payment"]');
            uiUploadPaymentModal = $('[modal-id="upload-payment"]');
            uiCancelUploadPaymentBtn = $('#cancel_upload_payment_btn');
            uiConfirmUploadPaymentBtn = $('#confirm_upload_payment_btn');
            uiOpenPaymentFileBtn = $('#upload_payment');
            uiUploadPaymentContainer = $('[data-container="upload_payment_details_container"]');
            uiPaymentNoErrorsFound = $('.no_upload_payment_error');
            uiUploadPaymentErrorContainer = $('[data-container="upload_payment_error_container"]');
            uiPaymentUploadEsopDropdown = $('.upload_esop_dropdown'),
            uiCompanyDropdown = $('.add_company_payment_record_dropdown'),
            uiUsersDropdown = $('.add_employee_name_payment_record_dropdown');
            uiAddPaymentPerUser = $('div[modal-id="add-payment-record"]'),
            uiPaymentTable = $('#payment_table'),
            uiGratuityTable = $('#gratuity_table');


             roxas.esop.get_payment_method()

            uiGroupWideSearchInput = $('button.btn-normal');

            /*bind on load data*/
            roxas.esop.bind_events_on_load();
            roxas.esop.bind_sorting_events();

            if(window.location.href.indexOf('esop/view_esop_batch') > -1)
            {
               roxas.esop.extend_offer_expiration({status : undefined});
            }
            


            /*initialize events*/

            var uiDisable = $(".date-picker").find(".dividend-date");
            uiDisable.attr("readonly", true);

            uiEsopBatchContainer.on('click', '.batch_add_distribute_shares', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.parents('.esop_batch:first').attr('data-esop-id');
                roxas.esop.add_edit_batch_set_local_storage(iEsopID);
            });

            uiEsopBatchContainer.on('click', '.batch_edit_distribute_shares', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.parents('.esop_batch:first').attr('data-esop-id');
                roxas.esop.add_edit_batch_set_local_storage(iEsopID, true);
            });

            /*for viewing of esop details*/
            uiEsopListContainer.on('click', '.list_view_esop', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.parents('.esop_list:first').attr('data-esop-id');
                var sEsopName = uiThis.parents('.esop_list:first').attr('data-esop-name');
                var bIsLetterSent = uiThis.parents('.esop_list:first').attr('is_letter_sent');
                roxas.esop.view_esop(iEsopID, bIsLetterSent, sEsopName);
            });

            uiEsopGridContainer.on('click', '.grid_view_esop', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.parents('.esop_grid:first').attr('data-esop-id');
                var sEsopName = uiThis.parents('.esop_grid:first').attr('data-esop-name');
                var bIsLetterSent = uiThis.parents('.esop_grid:first').attr('is_letter_sent');
                roxas.esop.view_esop(iEsopID, bIsLetterSent, sEsopName);
            });

            uiEsopGridContainer.on('click', '.grid_view_personal_stock , .list_view_personal_stock', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.parents('.esop_grid:first').attr('data-esop-id');
                roxas.esop.view_personal_stocks(iEsopID);
            });

            /*for viewing of offer details*/
            uiEsopListContainer.on('click', '.list_view_stock_offer', function(){
                var uiThis = $(this);
                var iOfferID = uiThis.parents('.esop_list:first').attr('data-stock-offer-id');
                roxas.esop.view_stock_offer(iOfferID);
            });

            uiEsopGridContainer.on('click', '.grid_view_stock_offer', function(){
                var uiThis = $(this);
                var iOfferID = uiThis.parents('.esop_grid:first').attr('data-stock-offer-id');
                roxas.esop.view_stock_offer(iOfferID);
            });

            moment.lang('en'); //russian sya eh hindi ko alam kung bakit

            uiEsopListContainer.on('click', '.list_view_personal_stocks', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.parents('.esop_list:first').attr('data-esop-id');
                roxas.esop.view_personal_stocks(iEsopID);
            });

            $vestingRightClaimContainer.on('change' , 'input[data-label="year_no"]' , function() {
                $vestingRightClaimButton.prop('disabled' , true);
                if($vestingRightClaimContainer.find('input[data-label="year_no"]:checked').length > 0)
                {
                     $vestingRightClaimButton.prop('disabled' , false);
                }
            })

            $claimDocumentsModal.on('click' , 'input[name="claim_id"]' , function() {
                $claimDocumentButton.prop('disabled' , false);
            })

            $claimDocumentButton.on('click' , function() {
                localStorage.setItem('claim_id' , $claimDocumentsModal.find('input[name="claim_id"]:checked').val());

                if($('select[name="document_view"]').val() == 'Statement of Account')
                {
                    localStorage.removeItem('soa_from_personal');
                    localStorage.setItem('soa_from_personal', true);
                    window.location.href = roxas.config('url.server.base') + "claims/view";
                }else{
                    window.location.href = roxas.config('url.server.base') + "esop/claim_form";
                }


            })

            $('button.print_claim').on('click' , function() {

                $('#divcontents').parents('.inside-sub:first').parents('.sub-container1:first').find('*').each(function() {
                    var style = cr8v.css($(this));
                    $(this).css(style)
                });

                // var content = $('#divcontents').parents('.inside-sub:first').parents('.sub-container1:first');
                // var pri = document.getElementById("ifmcontentstoprint").contentWindow;
                // pri.document.open();
                // pri.document.write(content.html());
                // pri.document.close();
                // pri.focus();
                // pri.print();

                var oParams = {'html' : $('#divcontents').parents('.inside-sub:first').parents('.sub-container1:first').html()};

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/download_claim_form",
                    "beforeSend": function () {
                        cr8v.show_spinner($('button.print_claim'), true);
                    },
                    "success" : function(oData) {
                        console.log(oData)
                        if(oData.status == true)
                        {
                            if(cr8v.var_check(oData.url) && cr8v.var_check(oData.file_name))
                            {
                                window.location = oData.url;

                                var sFilePathForDelete = './assets/templates/' + oData.file_name;

                                var oParams = {
                                    "file_path" : sFilePathForDelete
                                };

                                setTimeout(function(){
                                    cr8v.delete_file(oParams, function(oData){
                                    
                                    });
                                }, 2000);
                            }
                        }
                    },
                    "complete" : function() {
                        cr8v.show_spinner($('button.print_claim'), false);
                    }
                }
                roxas.esop.ajax(oAjaxConfig)
            })

            

            $vestingRightClaimButton.on('click' , function() {
                var aCheckedInputs = $vestingRightClaimContainer.find('input[data-label="year_no"]:checked');
                var aVestingIds = [];
                var aVestingYearsName = [];
                if(aCheckedInputs.length > 0)
                {
                        for (var i = 0 ; i < aCheckedInputs.length ; i++)
                        {
                            aVestingIds.push( $(aCheckedInputs[i]).val())    
                            aVestingYearsName.push( 'Year '+ $(aCheckedInputs[i]).parents('.vesting_rights_claim:first').find('data[data-label="year_no"]').text() )    
                        }

                        var oParams = {
                                'user_id' : iUserID,
                                'vesting_rights_id' : aVestingIds,
                                'vesting_rights_name' : aVestingYearsName.join(', '),
                                'user_name' : $('section[data-container="main-info"]:first').attr('data-user-full-name'),
                                'esop_id' : $('section[data-container="main-info"]:first').attr('data-esop-id'),
                                'esop_name' : $('section[data-container="main-info"]:first').attr('data-esop-name')
                        }

                        var oAjaxConfig = {
                            "type"   : "POST",
                            "data"   : oParams,
                            "url"    : roxas.config('url.server.base') + "esop/add_vesting_claims",
                            "beforeSend": function () {
                                if (cr8v.var_check($vestingRightClaimButton)) {
                                    cr8v.show_spinner($vestingRightClaimButton, true);
                                }
                            },
                            "success" : function(oData) {
                                if(oData.status)
                                {
                                    for(var i = 0; i < aCheckedInputs.length; i++)
                                    {
                                        $(aCheckedInputs[i]).parents('tr:first').remove();
                                            
                                    }

                                    $vestingRightClaimModal.find('.close-me').trigger('click');

                                        
                                }
                                cr8v.show_spinner($vestingRightClaimButton, false);
                                window.location.reload();
                            },
                            "error" : function() {
                                cr8v.show_spinner($vestingRightClaimButton, false);
                            }
                        }
                        roxas.esop.ajax(oAjaxConfig)
                }    
            })
            
            $('body').on('click','button[modal-target="payment-record"]' , function(e) {
                var sEsopName = $('body').find('a[data-label="esop_name"]:first').text();
                uiAddPaymentRecordModal.find('[data-label="esop_name"]').text(sEsopName);
                uiAddPaymentRecordModal.attr('data-esop-name', sEsopName);
                
            });

            uiAddPaymentRecordModal.on('click','.btn-add-payment-record' , function(e) {
                var uiBtn = $(this);
                var iPaymentTypeID = uiAddPaymentRecordModal.find('div#payment_method_dropdown').attr('payment_type_id');
                var iCurrencyID = uiAddPaymentRecordModal.find('div#currency_dropdown').attr('currency_id');
                var sCurrencyName = uiAddPaymentRecordModal.find('div#currency_dropdown').attr('currency_id');
                var iAmountPaid = uiAddPaymentRecordModal.find('input[name="amount_paid"]').val();
                var iActiveYearID = $('body').data('data-active-vesting-year-id').active_year_id;
                var iEmployeeID = $('body').data('data-employee-id');
                var iEsopID = $('section[data-container="main-info"]:first').attr('data-esop-id');
                var sFullName = $('section[data-container="main-info"]:first').attr('data-user-full-name');
                var sEsopName = uiAddPaymentRecordModal.attr('data-esop-name');
                var oCurrentVestingRights = $('.table-roxas').data('vesting_rights');

                if(roxas.esop.validate_add_payment_record() == true)
                {
                    
                        
                    var oParams = {
                                'payment_type_id'            : iPaymentTypeID,
                                'currency'                   : iCurrencyID,
                                'currency_name'                   : sCurrencyName,
                                'amount_paid'                : iAmountPaid,
                                'vesting_rights_id'          : iActiveYearID,
                                'placed_by'                  : iUserID,
                                'user_id'                    : iEmployeeID,
                                'esop_id'                    : iEsopID,
                                'user_name'                  : sFullName,
                                'esop_name'                  : sEsopName,
                                'payment_type'               : uiAddPaymentRecordModal.find('div#payment_method_dropdown').find('input').val(),
                                'vesting_rights'             : oCurrentVestingRights
                                };
                    roxas.esop.add_payment_record(oParams,uiBtn);    
                }

            });

            uiAddEsopModal.on('change' , 'input.share_allotment_input' , function() {
                roxas.esop.compute_share_allotment();
                $(this).val(number_format($(this).val(), 2)) 
            });

            uiAddEsopModal.on('change' , 'input[name="total_share_qty"]', function() {
                $(this).attr('original_value' , $(this).val());
                roxas.esop.compute_share_allotment();

            });

            uiAddEsopModal.on('change' , 'input[type="checkbox"][name="esop_name"] , #share_allotment_checkbox', function() {
                roxas.esop.compute_share_allotment();        
            });


            $('#download_acceptance_letter').on('click', function(){
                var iStockOfferID = $('section[data-container="main-info"]:first').attr('data-user-stock-offer-id');
                var iUserID = $('section[data-container="main-info"]:first').attr('data-user-id');
                var iTemp_shares = localStorage.getItem("temp_shares");
                roxas.esop.download_acceptance_letter(iStockOfferID, iUserID, iTemp_shares);
            });
        },
         

        /**
         * bind_send_letter_events
         * @description This function is for binding events for send offer letter
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_send_letter_events : function(){
            uiEsopSendLetterBtn.on('click', function(){
                roxas.esop.assemble_send_letter(uiEsopSendLetterBtn);
            });
        },

        /**
         * assemble_send_letter
         * @description This function is for binding events for send offer letter
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_send_letter : function(uiEsopSendLetterBtn){
            var oParams = {
                "esop_id" : localStorage.getItem('esop_id'),
                "esop_name" : $('[data-container="esop_information"]').attr('data-esop-name')
            };

            roxas.esop.update_stock_offer_status(oParams, uiEsopSendLetterBtn);
        },

        /**
         * update_stock_offer_status
         * @description This function is for updating status of stock offer
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        update_stock_offer_status : function(oParams, uiBtn, bFromEsopBatch){
            if (cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type": "POST",
                    "data": oParams,
                    "url": roxas.config('url.server.base') + "esop/update_stock_offer_status",
                    "beforeSend": function () {
                        if(cr8v.var_check(uiBtn)){
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData);
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bFromEsopBatch) && bFromEsopBatch)
                                {
                                    uiEsopSendLetterBatchModal.find('.send_letter_success_message').removeClass('hidden').html('<p class="font-15 success_message">Letter sent successfully</p>');

                                    setTimeout(function(){
                                        if(uiEsopSendLetterBatchModal.hasClass('showed'))
                                        {
                                            uiEsopSendLetterBatchModal.find('.close-me').trigger('click');
                                        }

                                        window.location = roxas.config('url.server.base') + 'esop/view_esop_batch';
                                    }, 1000);

                                    var uiBatchTemplate = $('.esop_batch:not(.template)[data-esop-id="'+oData.data.id+'"]');

                                    uiBatchTemplate.find('.undistributed_esop').addClass('hidden');
                                    uiBatchTemplate.find('.distributed_esop').addClass('hidden');
                                    uiBatchTemplate.find('.expiration_set').addClass('hidden');
                                }
                                else
                                {                                
                                    uiEsopSendLetterModal.find('.send_letter_success_message').removeClass('hidden').html('<p class="font-15 success_message">Letter sent successfully</p>');

                                    setTimeout(function(){
                                        if(uiEsopSendLetterModal.hasClass('showed'))
                                        {
                                            uiEsopSendLetterModal.find('.close-me').trigger('click');
                                        }

                                        $('.undistributed_esop').addClass('hidden');
                                        $('.distributed_esop').addClass('hidden');
                                        $('.expiration_set').addClass('hidden');
                                        /*redirect to other page*/

                                        window.location = roxas.config('url.server.base') + 'esop/view_esop_batch';
                                    }, 1000);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if(cr8v.var_check(uiBtn)){
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * bind_set_offer_events
         * @description This function is for binding events for set offer expiration
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_set_offer_events : function(){

            uiSetOfferExpBtn.on('click', function(){
                var sDate = uiSetOfferExpForm.find('input[name="offer_expiration"]').val();
                var bValidDate = cr8v.validate_date(sDate, 'MM/DD/YYYY');

                if(sDate != '')
                {
                    if(bValidDate)
                    {
                        cr8v.get_time(function (sTime){
                            var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
                            var oCurrentDate = moment(sCurrentDate, 'YYYY-MM-DD');
                            var oOfferExpiration = moment(sDate, 'MM/DD/YYYY');

                            if(oOfferExpiration.diff(oCurrentDate, 'days') > 0)
                            {
                                cr8v.show_spinner(uiSetOfferExpBtn, true);
                                roxas.esop.assemble_set_offer_expiration(uiSetOfferExpModal, uiSetOfferExpBtn);
                            }
                            else
                            {
                                var uiErrorContainer = uiSetOfferExpModal.find('.set_offer_expiration_error_message');
                                var sErrorMessages = '<p class="font-15 error_message">Date below has already passed. Please choose another date.</p>';
                                cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                            }
                        }); 
                    }
                    else
                    {
                        var uiErrorContainer = uiSetOfferExpModal.find('.set_offer_expiration_error_message');
                        var sErrorMessages = '<p class="font-15 error_message">Date is invalid.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);                    
                    }
                }
                else
                {
                    var uiErrorContainer = uiSetOfferExpModal.find('.set_offer_expiration_error_message');
                    var sErrorMessages = '<p class="font-15 error_message">Date is required.</p>';
                    uiSetOfferExpForm.find('input[name="offer_expiration"]').addClass('input-error');
                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);                    
                }
            });

            cr8v.get_time(function (sTime){
                var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY');
                var oCurrentDate = moment(sCurrentDate, 'MM/DD/YYYY').add(1, 'days');

                var iMinDate = oCurrentDate.format('MM/DD/YYYY');
                uiSetOfferExpForm.find('input[name="offer_expiration"]').datetimepicker({pickTime: false, minDate: iMinDate});
                if(cr8v.var_check(uiSetOfferExpForm.find('input[name="offer_expiration"]').data("DateTimePicker")))
                {
                    uiSetOfferExpForm.find('input[name="offer_expiration"]').data("DateTimePicker").setMinDate(iMinDate);
                    uiSetOfferExpForm.find('input[name="offer_expiration"]').data("DateTimePicker").setDate(iMinDate);
                }
            });
        },

        /**
         * bind_sorting_events
         * @description This function is for binding events for sorting
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_sorting_events : function(){
            uiSortEsopContainer.on('click', '.sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');

                cr8v.clear_sorting_classes(uiSortEsopContainer.find('.sort_field'));

                if(uiEsopGridContainer.is(':visible'))
                {
                    var uiTemplatesGrid = uiEsopGridContainer.find('.esop_grid:not(.template)');                    
                    cr8v.ui_sorting(uiThis, sSortBy, uiTemplatesGrid.length, uiTemplatesGrid, uiEsopGridContainer);
                }
                else
                {
                    var uiTemplatesList = uiEsopListContainer.find('.esop_list:not(.template)');
                    cr8v.ui_sorting(uiThis, sSortBy, uiTemplatesList.length, uiTemplatesList, uiEsopListContainer);
                }

            });
            
            cr8v.clear_sorting_classes(uiSortEsopContainer.find('.sort_field'));

            $(".view-by .grid").off("click").on("click", function() {       
                cr8v.clear_sorting_classes(uiSortEsopContainer.find('.sort_field'));
                
                $(".grid-content").fadeIn();
                $(".table-content").fadeOut();


                $(".dash-table").fadeOut();
                $(".dash-grid").fadeIn();

                $(this).css({
                    'color':'#BDC3C7',
                    'font-size':'20px'
                });
                $(this).next(".list").css({
                    'font-size':'15px',
                    'color':'#fff'
                });
            });

            $(".view-by .list").off("click").on("click", function() {
                cr8v.clear_sorting_classes(uiSortEsopContainer.find('.sort_field'));

                $(".table-content").fadeIn();
                $(".grid-content").fadeOut();   

                $(".dash-grid").fadeOut();
                $(".dash-table").fadeIn();  

                $(this).css({
                    'color':'#BDC3C7',
                    'font-size':'22px'
                });
                $(this).prev(".grid").css({
                    'font-size':'15px',
                    'color': '#fff'
                });
            });

            uiEsopCompaniesContainer.on('click', '.sort_esop_employees .sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');

                cr8v.clear_sorting_classes(uiThis.parents('.esop_companies:first').find('.sort_esop_employees').find('.sort_field'));
                
                var uiTemplates = uiThis.parents('.esop_companies:first').find('[data-container="esop_employees_container"]').find('.esop_employees:not(.template)');
                cr8v.ui_sorting(uiThis, sSortBy, uiTemplates.length, uiTemplates, uiThis.parents('.esop_companies:first').find('[data-container="esop_employees_container"]'));
            });

            cr8v.clear_sorting_classes(uiSortEsopEmployeesContainer.find('.sort_field'));

            
            $('body').on('click', '.go_to_view_employee_by_esop_batch', function(){
                //console.log($(this));
                var uiParent = $(this).closest('tr.esop_employees');
                var sSharesAvailed = uiParent.find('[data-label="employee_shares_availed"]').text();
                
                if(sSharesAvailed !== '0.00')
                {
                  var iUserID = uiParent.attr('data-user-id');
                  localStorage.removeItem('view_employee_by_esop_batch_user_id');
                  localStorage.setItem('view_employee_by_esop_batch_user_id', iUserID);
                  localStorage.removeItem('view_employee_by_esop_batch_esop_id');
                  localStorage.setItem('view_employee_by_esop_batch_esop_id', $(this).parents('tr.esop_employees:not(.template):first').attr('data-esop-id'));
                  
                  window.location.href = roxas.config('url.server.base') + "esop/view_employee_by_esop_batch";      
                }
                
            });


        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('esop/esop_list') > -1)
            {
                var oGetEsopParams = {
                    "limit" : 100,
                    "where" : [{
                        "field" : "e.parent_id",
                        "value" : 0
                    }]
                };
                roxas.esop.get_all_esop();
                roxas.esop.get_currency();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_add_esop_events();
                roxas.esop.bind_search_event();
                roxas.esop.fetch_available_share_allotment();


            }
            else if(window.location.href.indexOf('esop/view_esop') > -1 && localStorage.getItem('esop_id') != null)
            {
                roxas.esop.get_currency();
                roxas.esop.bind_dropdown_events();

                var oGetEsopParams = {
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('esop_id')
                };

                if(window.location.href.indexOf('esop/view_esop_batch') > -1)
                {
                    roxas.esop.get_esop(oGetEsopParams, undefined, true, undefined, undefined, true);
                    roxas.esop.bind_set_offer_batch_events();
                    roxas.esop.bind_create_esop_batch_events();
                    roxas.esop.bind_send_letter_batch_events();
                    roxas.esop.bind_edit_esop_batch_events();
                    roxas.esop.bind_batch_upload_share_distribution_events();

                    var oGetUserParams = {
                        "limit" : 999999,
                        "esop_batch_parent_id" : localStorage.getItem('esop_batch_parent_id')
                    };

                    roxas.esop.get_users_grouped_by_company(oGetUserParams, undefined, undefined, true);

                    roxas.esop.bind_add_new_employee_events();
                    roxas.esop.bind_upload_payment_events();
                }
                else
                {
                    roxas.esop.get_esop(oGetEsopParams, undefined, true);
                    roxas.esop.bind_set_offer_events();
                    roxas.esop.bind_edit_esop_events();
                    roxas.esop.bind_send_letter_events();
                    roxas.esop.bind_upload_share_distribution_events();
                }
            }
            else if(window.location.href.indexOf('esop/upload_payment') > -1 && localStorage.getItem('esop_id') != null)
            {
                var oDeleteUserListCsvParams = {
                    "directory" : "./assets/uploads/payment_record/csv",
                    'exception_files' : []
                };

                if(localStorage.getItem('upload_payment_path') != null && localStorage.getItem('upload_payment_file_name') != null)
                {
                    var oGetEsopParams = {
                        "search_field" : 'id',
                        "keyword" : localStorage.getItem('upload_payment_esop_id')
                    };

                    roxas.esop.get_esop(oGetEsopParams, undefined, true, undefined, undefined, undefined, undefined, true);

                    $('[data-label="upload_payment_file_name"]').text(localStorage.getItem('upload_payment_file_name'));

                    roxas.esop.bind_upload_payment_events();

                    var oGetCsvParams = {
                        "file_path" : localStorage.getItem('upload_payment_path'),
                        "esop_id" : localStorage.getItem('upload_payment_esop_id')
                    };

                    roxas.esop.open_payment_csv(oGetCsvParams);

                    var sFilePath = localStorage.getItem('upload_payment_path').replace( roxas.config('url.server.base')+ '/assets/uploads/payment_record/csv/', '');
                    oDeleteUserListCsvParams.exception_files.push(sFilePath);
                }
                else
                {
                    // window.location = roxas.config('url.server.base') + 'users/user_list';
                }

                // cr8v.delete_all_except(oDeleteUserListCsvParams);
            }
            else if(window.location.href.indexOf('esop/upload_share_distribution') > -1 && localStorage.getItem('esop_id') != null)
            {
                var oDeleteUserListCsvParams = {
                    "directory" : "./assets/uploads/share_distribution/csv",
                    'exception_files' : []
                };

                if(localStorage.getItem('upload_share_distribution_path') != null && localStorage.getItem('upload_share_distribution_file_name') != null)
                {
                    var oGetEsopParams = {
                        "search_field" : 'id',
                        "keyword" : localStorage.getItem('esop_id')
                    };

                    roxas.esop.get_esop(oGetEsopParams, undefined, true);

                    $('[data-label="upload_share_distribution_file_name"]').text(localStorage.getItem('upload_share_distribution_file_name'));

                    roxas.esop.bind_upload_share_distribution_events();

                    var oGetCsvParams = {
                        "file_path" : localStorage.getItem('upload_share_distribution_path'),
                        "esop_id" : localStorage.getItem('esop_id')
                    };

                    roxas.esop.open_csv(oGetCsvParams);

                    var sFilePath = localStorage.getItem('upload_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                    oDeleteUserListCsvParams.exception_files.push(sFilePath);
                }
                else
                {
                    // window.location = roxas.config('url.server.base') + 'users/user_list';
                }

                // cr8v.delete_all_except(oDeleteUserListCsvParams);
            }
            else if(window.location.href.indexOf('esop/batch_upload_share_distribution') > -1 && localStorage.getItem('esop_batch_id') != null)
            {
                var oDeleteUserListCsvParams = {
                    "directory" : "./assets/uploads/share_distribution/csv",
                    'exception_files' : []
                };

                if(localStorage.getItem('upload_batch_share_distribution_path') != null && localStorage.getItem('upload_batch_share_distribution_file_name') != null && localStorage.getItem('parent_esop_name') != null)
                {
                    var oGetEsopParams = {
                        "search_field" : 'id',
                        "keyword" : localStorage.getItem('esop_batch_id')
                    };

                    roxas.esop.get_esop(oGetEsopParams, undefined, true);

                    $('[data-label="upload_batch_share_distribution_file_name"]').text(localStorage.getItem('upload_batch_share_distribution_file_name'));

                    roxas.esop.bind_batch_upload_share_distribution_events();

                    var oGetCsvParams = {
                        "file_path" : localStorage.getItem('upload_batch_share_distribution_path'),
                        "esop_id" : localStorage.getItem('esop_batch_id'),
                        "esop_batch_parent_id" : localStorage.getItem('esop_batch_parent_id')
                    };

                    roxas.esop.open_batch_csv(oGetCsvParams);

                    var sFilePath = localStorage.getItem('upload_batch_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                    oDeleteUserListCsvParams.exception_files.push(sFilePath);
                    $('[data-label="parent_esop_name"]').text(localStorage.getItem('parent_esop_name'));
                }
                else
                {
                    // window.location = roxas.config('url.server.base') + 'users/user_list';
                }

                // cr8v.delete_all_except(oDeleteUserListCsvParams);
            }
            else if(window.location.href.indexOf('esop/batch_add_distribute_shares') > -1 && localStorage.getItem('esop_batch_id') != null && localStorage.getItem('esop_batch_parent_id') != null && localStorage.getItem('parent_esop_name') != null)
            {
                roxas.esop.get_currency();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_add_distribute_share_events();

                var oGetOfferLetterParams = {
                    "limit" : 999999,
                    "search_field" : 'status',
                    "keyword" : 1
                };

                roxas.esop.get_offer_letter(oGetOfferLetterParams);

                var oGetEsopParams = {
                    "limit" : 999999,
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('esop_batch_id')
                };

                roxas.esop.get_esop(oGetEsopParams, undefined, true, true);

                var oGetUserParams = {
                    "limit" : 999999,
                    "esop_batch_parent_id" : localStorage.getItem('esop_batch_parent_id')
                };

                roxas.esop.get_users_grouped_by_company(oGetUserParams);
                $('[data-label="parent_esop_name"]').text(localStorage.getItem('parent_esop_name'));


            }
            else if(window.location.href.indexOf('esop/batch_edit_distribute_shares') > -1 && localStorage.getItem('esop_batch_id') != null && localStorage.getItem('esop_batch_parent_id') != null && localStorage.getItem('parent_esop_name') != null)
            {
                roxas.esop.get_currency();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_edit_distribute_share_events();

                var oGetOfferLetterParams = {
                    "limit" : 999999,
                    "search_field" : 'status',
                    "keyword" : 1
                };

                roxas.esop.get_offer_letter(oGetOfferLetterParams);

                var oGetEsopParams = {
                    "limit" : 999999,
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('esop_batch_id')
                };

                roxas.esop.get_esop(oGetEsopParams, undefined, true, undefined, true);

                var oGetUserParams = {
                    "limit" : 999999,
                    "esop_id" : localStorage.getItem('esop_batch_id'),
                    "esop_batch_parent_id" : localStorage.getItem('esop_batch_parent_id')
                };

                roxas.esop.get_users_grouped_by_company(oGetUserParams, undefined, true);
                $('[data-label="parent_esop_name"]').text(localStorage.getItem('parent_esop_name'));
            }
            else if(window.location.href.indexOf('esop/add_distribute_shares') > -1 && localStorage.getItem('esop_id') != null)
            {
                roxas.esop.get_currency();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_add_distribute_share_events();

                var oGetOfferLetterParams = {
                    "limit" : 999999,
                    "search_field" : 'status',
                    "keyword" : 1
                };

                roxas.esop.get_offer_letter(oGetOfferLetterParams);

                var oGetEsopParams = {
                    "limit" : 999999,
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('esop_id')
                };

                roxas.esop.get_esop(oGetEsopParams, undefined, true, true);

                var oGetUserParams = {
                    "limit" : 999999
                };

                roxas.esop.get_users_grouped_by_company(oGetUserParams);


            }
            else if(window.location.href.indexOf('esop/edit_distribute_shares') > -1 && localStorage.getItem('esop_id') != null)
            {
                roxas.esop.get_currency();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_edit_distribute_share_events();

                var oGetOfferLetterParams = {
                    "limit" : 999999,
                    "search_field" : 'status',
                    "keyword" : 1
                };

                roxas.esop.get_offer_letter(oGetOfferLetterParams);

                var oGetEsopParams = {
                    "limit" : 999999,
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('esop_id')
                };

                roxas.esop.get_esop(oGetEsopParams, undefined, true, undefined, true);

                var oGetUserParams = {
                    "limit" : 999999,
                    "esop_id" : localStorage.getItem('esop_id')
                };

                roxas.esop.get_users_grouped_by_company(oGetUserParams, undefined, true);
            }
            else if(window.location.href.indexOf('esop/stock_offers') > -1)
            {
                roxas.esop.get_currency();
                roxas.esop.bind_search_event();

                var oGetUserStockOfferParams = {
                    "limit" : 999999,
                    "search_field" : 'user_id',
                    "keyword" : iUserID,
                    "where" : [
                        {
                            "field" : "e.offer_expiration",
                            "operator" : '!=',
                            "value" : null
                        },
                        {
                            "field" : "uso.status",
                            "value" : 1
                        }
                    ]
                };
                roxas.esop.get_user_stock_offer(oGetUserStockOfferParams);
            }
            else if(window.location.href.indexOf('esop/view_stock_offer') > -1 && localStorage.getItem("stock_offer_id") != null)
            {
                roxas.esop.get_currency();

                var oGetUserStockOfferParams = {
                    "limit" : 999999,
                    "search_field" : 'user_id',
                    "keyword" : iUserID,
                    "where" : [
                        {
                            "field" : "uso.id",
                            "value" : localStorage.getItem("stock_offer_id")
                        },
                        {
                            "field" : "e.offer_expiration",
                            "operator" : '!=',
                            "value" : null
                        },
                        {
                            "field" : "uso.status",
                            "value" : 1
                        }
                    ]
                };
                roxas.esop.get_user_stock_offer(oGetUserStockOfferParams, undefined, true);

                roxas.esop.bind_accept_offer_events();
            }
            else if((window.location.href.indexOf('esop/personal_stocks') > -1 || window.location.href.indexOf('esop/personal_stock_view') > -1))
            {
                var oGetEsopParams,
                    bFromViewEsop;

                if(window.location.href.indexOf('esop/personal_stocks') > -1)
                {
                    oGetEsopParams = {
                        "limit" : 999999,
                        "where" : [
                        {
                            'field' : 'user_id',
                            'value' : iUserID
                        }
                    ],
                        group_by : "esop_id"
                    },
                    bFromViewEsop = false;

                }
                else if(window.location.href.indexOf('esop/personal_stock_view') > -1 && localStorage.getItem('esop_id') != null)
                {
                    oGetEsopParams = {
                        "limit" : 999999,
                        "where" : [
                            {
                                'field' : 'esop_id',
                                'value' : localStorage.getItem('esop_id')
                            },
                            {
                                'field' : 'user_id',
                                'value' : iUserID
                            },
                        ],
                        "group_by" : 'user_id'
                    },
                    bFromViewEsop = true;

                    roxas.esop.get_submitted_claims(oGetEsopParams);
                }

                roxas.esop.get_personal_stocks(oGetEsopParams, undefined , bFromViewEsop);


               
                roxas.esop.get_currency();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_add_esop_events();
                roxas.esop.bind_search_event();
                       
                roxas.esop.get_payment_per_user(localStorage.getItem('view_employee_by_esop_batch_esop_id'), localStorage.getItem('view_employee_by_esop_batch_user_id'));
                roxas.esop.get_data_for_payment_application(localStorage.getItem('esop_id'));
                
            }
            else if(window.location.href.indexOf('esop/claim_form') > -1 && localStorage.getItem("claim_id") != null)
            {
                roxas.esop.get_currency();

                oGetEsopParams = {
                    "limit" : 999999,
                    "where" : [
                        {
                            'field' : 'esop_id',
                            'value' : localStorage.getItem('esop_id')
                        }
                    ],
                    "group_by" : 'user_id'
                }

                roxas.esop.get_submitted_claims(oGetEsopParams);
                roxas.esop.get_personal_stocks(oGetEsopParams, undefined , true , false, false, true);

                //roxas.esop.bind_accept_offer_events();
            }

            else if(window.location.href.indexOf('esop/view_employee_by_esop_batch') > -1)
            {
                var oGetEsopParams,
                    bFromViewEsop;

                /*if(window.location.href.indexOf('esop/view_employee_by_esop_batch') > -1)
                {
                    oGetEsopParams = {
                        "limit" : 999999,
                        "where" : [
                        {
                            'field' : 'user_id',
                            'value' : iUserID
                        }
                    ],
                        group_by : "esop_id"
                    },
                    bFromViewEsop = false;

                }
                else*/ if(window.location.href.indexOf('esop/view_employee_by_esop_batch') > -1)
                {
                    oGetEsopParams = {
                        "limit" : 999999,
                        "where" : [
                            {
                                'field' : 'esop_id',
                                'value' : localStorage.getItem('view_employee_by_esop_batch_esop_id')
                            },
                            {
                                'field' : 'user_id',
                                'value' : localStorage.getItem('view_employee_by_esop_batch_user_id')
                            },
                            
                        ],
                        "group_by" : 'user_id'
                    },
                    bFromViewEsop = true;

                   // console.log(localStorage.getItem('view_employee_by_esop_batch_esop_id'));

                    //roxas.esop.get_submitted_claims(oGetEsopParams);
                }

                roxas.esop.view_employee_by_esop_batch_get_personal_stocks(oGetEsopParams, undefined , bFromViewEsop);
                
                //roxas.esop.get_submitted_claims(oGetEsopParams);

               
               
                roxas.esop.get_currency();
                roxas.esop.get_payment_method();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_add_esop_events();
                roxas.esop.bind_search_event();

                roxas.esop.get_payment_per_user(localStorage.getItem('view_employee_by_esop_batch_esop_id'), localStorage.getItem('view_employee_by_esop_batch_user_id'));
                roxas.esop.get_data_for_payment_application(localStorage.getItem('view_employee_by_esop_batch_esop_id'));

                
                


            }
            else if(window.location.href.indexOf('esop/dashboard') > -1)
            {
                if(iUserRole == 1)
                {
                    var bFromDashboard = true;
                    var oGetEsopParams = {
                        "limit" : 100,
                        "where" : [
                            {
                                'field' : 'user_id',
                                'value' : iUserID
                            }
                        ],
                        group_by : "esop_id"
                    };

                    roxas.esop.get_personal_stocks(oGetEsopParams, undefined , undefined, bFromDashboard);

                    roxas.esop.get_currency();
                }
                else if(iUserRole == 2 || iUserRole == 3 || iUserRole == 4)
                {
                    var oGetEsopParams = {
                        "limit" : 100,
                        "where" : [{
                            "field" : "e.parent_id",
                            "value" : 0
                        }]
                    };
                    roxas.esop.get_esop(oGetEsopParams, undefined, undefined, undefined, undefined, undefined, true);
                }
            }
            else if(window.location.href.indexOf('esop/group_wide_stocks') > -1)
            {
               roxas.esop.group_wide_stocks_bind_events();
               roxas.esop.group_wide_stocks_filter_by_company();
            }
            else if(window.location.href.indexOf('esop/view_group_wide_stock_offers') > -1)
            {
                 roxas.esop.group_wide_stocks_view_bind_events();
            }
            else if(window.location.href.indexOf('esop/esop_gratuity_list') > -1)
            {
                roxas.esop.get_currency();
                roxas.esop.get_all_esop();
                roxas.esop.bind_dropdown_events();
                roxas.esop.bind_get_all_gratuity();
            }
            else if(window.location.href.indexOf('esop/payment_records') > -1)
            {
                roxas.esop.bind_dropdown_events();
                roxas.esop.get_currency();
                roxas.esop.get_all_esop();
                roxas.esop.get_companies();
                roxas.esop.get_users_by_company();
                roxas.esop.bind_upload_payment_events();
                roxas.esop.get_payment_records();
            }
        },

        /**
         * get_payment_per_user
         * @description This function will get payment per user
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @developer Nelson Estuesta Jr
         * @method_id N/A
         */

        get_payment_per_user : function(esop_id, user_id)
        {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : {"esop_id": esop_id, "user_id": user_id},
                    "url"    : roxas.config('url.server.base') + "esop/get_payment_per_user",
                    "beforeSend": function () {
                      
                    },
                    "success": function (oData) {   
                        console.log(JSON.stringify(oData.data));
                        roxas.esop.display_payment_summary_user_payments_soa(oData.data);
                    },
                    "complete" : function(){

                    }
                }

                 roxas.esop.ajax(oAjaxConfig);
        },

         /**
         * display_payment_summary_user_payments_soa
         * @description This function will display user payment SOA
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @developer Nelson Estuesta Jr
         * @method_id N/A
         */
        display_payment_summary_user_payments_soa : function(oData)
        {
                var uiPaymentSummaryContainer = $('[data-container="summary_vesting"]'),
                    uiPaymentWithAttr = uiPaymentSummaryContainer.find(".payments:not('.template')"),
                    uiLastContent = uiPaymentSummaryContainer.find(".last-content"),
                    uiTemplate = uiPaymentSummaryContainer.find(".payments:last");


                  $.each(uiPaymentWithAttr, function(){
                          var uiThis = $(this),
                               uiPaymentAttr = uiThis.attr("custom-attr-id");
                          
                           if(typeof uiPaymentAttr !== typeof undefined && uiPaymentAttr !== false)
                           {
                                   uiThis.remove();
                           }
                  });
                    
                  for(var x in oData)
                  {
                          var item = oData[x],
                          uiCloneTemplate = uiTemplate.clone();

                          uiCloneTemplate.removeClass("template").show();
                          uiCloneTemplate.find('[data-label="payment_name"]').html(item.payment_method_name);
                          uiCloneTemplate.find('[data-label="date_of_or"]').html(item.dividend_date_formatted);
                          uiCloneTemplate.find('[data-label="amount_paid"]').html(number_format(item.amount_paid, 2));

                          uiCloneTemplate.insertBefore(uiLastContent);
                  }

                  roxas.esop.display_user_payment_by_year(oData);
                     
        },

        /**
         * display_user_payment_by_year
         * @description This function will display user payment by year
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Nelson Estuesta Jr
         * @method_id N/A
         */
        display_user_payment_by_year : function(oData)
        {
                var uiPaymentSummaryContainer = $('[data-container="vesting_payments"]').find('[data-template="vesting_payments"]:not(.template)'),
                    uiTemplateContainer = uiPaymentSummaryContainer.find('[data-container="payments"]');

                        $.each(uiPaymentSummaryContainer, function(){
                                var uiThis = $(this),
                                    uiPaymentContainer = uiThis.find('[data-container="payments"]'),
                                    uiPaymentWithAttr = uiThis.find(".payments:not('.template')"), 
                                    uiLastContent = uiPaymentContainer.find(".last-content"),
                                    uiTemplate = uiPaymentContainer.find(".payments:last"),
                                    uiPaymentYear = uiThis.attr("custom-attr-year"),
                                    sConvertPaymentYear = uiPaymentYear.split(", "),
                                    sPaymentYear = sConvertPaymentYear[1].trim();

                                    
                                     $.each(uiPaymentWithAttr, function(){
                                          var uiThis = $(this),
                                               uiPaymentAttr = uiThis.attr("custom-attr-id");

                                           if(typeof uiPaymentAttr !== typeof undefined && uiPaymentAttr !== false)
                                           {
                                                   uiThis.remove();
                                           }
                                  });

                                    for(var x in oData)
                                    {

                                         if(sPaymentYear == oData[x]["dividend_date_year"]){
                                             var items = oData[x],
                                                 uiCloneTemplate = uiTemplate.clone();

                                                 uiCloneTemplate.removeClass("template").show();

                                                 uiCloneTemplate.find('[data-label="payment_name"]').html(items.payment_method_name);
                                                 uiCloneTemplate.find('[data-label="date_of_or"]').html(items.dividend_date_formatted);
                                                 uiCloneTemplate.find('[data-label="amount_paid"]').html(number_format(items.amount_paid, 2));

                                                 if(uiPaymentContainer.find('.last-content').length > 0)
                                                 {
                                                    uiCloneTemplate.insertBefore(uiPaymentContainer.find('.last-content'));
                                                  }else{
                                                    uiPaymentContainer.append(uiCloneTemplate);
                                                  }

                                                  uiPaymentContainer.find(".no-payment").hide();

                                            }
                                                  
                                    }
                                        
                        });
                     
        },

        

        /**
         * bind_accept_offer_events
         * @description This function will bind the events for the accept offer
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_accept_offer_events : function(){

            uiAcceptOfferModal.on('click', '[name="offer_type"]', function(){
                var uiThis = $(this);
                if(uiThis.attr('id') == 'only')
                {
                    uiAcceptOfferModal.find('[name="stock_amount"]').val('').attr('disabled', false);
                    uiViewAcceptanceLetterBtn.attr('disabled', true);
                }
                else
                {
                    uiAcceptOfferModal.find('[name="stock_amount"]').val('').attr('disabled', true);
                    uiViewAcceptanceLetterBtn.attr('disabled', false);
                }
            });

            uiAcceptOfferModal.find('[name="offer_type"][id="all"]').trigger('click');

            cr8v.input_numeric(uiAcceptOfferModal.find('[name="stock_amount"]'));

            uiAcceptOfferModal.on('blur', '[name="stock_amount"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {
                    uiThis.val(number_format(uiThis.val(), 2));
                    uiViewAcceptanceLetterBtn.attr('disabled', false);
                }
                else
                {
                    uiViewAcceptanceLetterBtn.attr('disabled', true);
                }
            });

            uiViewAcceptanceLetterBtn.on('click', function(){
                var uiThis = $(this);
                var iEsopID = $('[data-container="esop_information"]').attr('data-stock-offer-esop-id');
                var iEsopPricePerShare = $('[data-container="esop_information"]').attr('data-stock-offer-esop-price-per-share');
                var temp_shares = 0;

                var oParams = {"esop_id" : iEsopID};
                var iMaxStockAmount = cr8v.remove_commas($('[data-container="esop_information"]').attr('data-stock-offer-stock-amount'));
                var uiStockAmountOnly = cr8v.remove_commas(uiAcceptOfferModal.find('[name="stock_amount"]').val());

                if(uiAcceptOfferModal.find('[name="offer_type"][id="only"]').is(':checked'))
                {
                    if(parseFloat(uiStockAmountOnly) > parseFloat(iMaxStockAmount))
                    {
                        var uiErrorContainer = uiAcceptOfferModal.find('.accept_offer_error_message');
                        var sErrorMessages = '<p class="font-15 error_message">Stock amount is greater than the offer</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                        return;
                    }else if(uiStockAmountOnly != "" && parseFloat(uiStockAmountOnly) <= parseFloat(iMaxStockAmount))
                    {
                        temp_shares = parseFloat(uiStockAmountOnly);
                        localStorage.setItem('temp_shares', temp_shares);
                    }
                }else{
                        temp_shares = 0;
                        localStorage.setItem("temp_shares", temp_shares);
                       
                }

                roxas.esop.check_offer_expiration(oParams, function(oData){
                    if(oData.status == true)
                    {
                        // uiAcceptOfferModal.find('.accept_offer_success_message').removeClass('hidden').html('<p class="font-15 success_message">Please validate</p>');

                        setTimeout(function(){
                            // roxas.esop.clear_accept_offer_modal();

                            if(uiAcceptOfferModal.hasClass('showed'))
                            {
                                uiAcceptOfferModal.find('.close-me').trigger('click');
                            }

                            uiOfferLetterContainer.addClass('hidden');
                            uiAcceptanceLetterContainer.removeClass('hidden');

                            var uiAcceptanceLetterText = uiAcceptanceLetterHTMLContainer[0].outerHTML;


                            if(uiAcceptOfferModal.find('[name="offer_type"][id="only"]').is(':checked'))
                            {

                                    if(uiAcceptanceLetterText.indexOf('[accepted_shares]') != -1){
                                              var text = uiAcceptanceLetterText.replace('[accepted_shares]', "<span id='accepted_share'>"+number_format(uiStockAmountOnly, 2)+"</span>" );  
                                               var newHTML = text.replace('[value_of_accepted_shares]', "<span id='value_accepted_share'>"+number_format(uiStockAmountOnly * iEsopPricePerShare, 2)+"</span>");  

                                                uiAcceptanceLetterHTMLContainer.html(newHTML);
                                    }else{
                                         uiAcceptanceLetterHTMLContainer.find("#accepted_share").html(number_format(uiStockAmountOnly, 2));
                                         uiAcceptanceLetterHTMLContainer.find("#value_accepted_share").html(number_format(uiStockAmountOnly * iEsopPricePerShare, 2));
                                      
                                    }
                                
                               
                            }
                            else
                            {   
                                if(uiAcceptanceLetterText.indexOf('[accepted_shares]') != -1){
                                      var text = uiAcceptanceLetterText.replace('[accepted_shares]', "<span id='accepted_share'>"+number_format(temp_shares, 2)+"</span>" );  
                                       var newHTML = text.replace('[value_of_accepted_shares]', "<span id='value_accepted_share'>"+number_format(iMaxStockAmount * iEsopPricePerShare, 2)+"</span>");  

                                        uiAcceptanceLetterHTMLContainer.html(newHTML);
                                }else{
                                       uiAcceptanceLetterHTMLContainer.find("#accepted_share").html(number_format(temp_shares));
                                       uiAcceptanceLetterHTMLContainer.find("#value_accepted_share").html(number_format(iMaxStockAmount * iEsopPricePerShare, 2));


                                }

//                                 var text = uiAcceptanceLetterText.replace('[accepted_shares]', number_format(iMaxStockAmount, 2));  
//                                 var newHTML = text.replace('[value_of_accepted_shares]', number_format(iMaxStockAmount * iEsopPricePerShare, 2));  

//                                 uiAcceptanceLetterHTMLContainer.html(newHTML);
                            }
                        }, 1000);
                    }
                    else
                    {
                        var uiErrorContainer = uiAcceptOfferModal.find('.accept_offer_error_message');
                        var sErrorMessages = '<p class="font-15 error_message">Accepting of offer for this ESOP is already expired.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }

                }, uiThis);
            });

            uiBackToOfferLetterContainerBtn.on('click', function(){
                uiOfferLetterContainer.removeClass('hidden');
                uiAcceptanceLetterContainer.addClass('hidden');
            });

            uiSendLetterBtn.on('click', function(){
                var uiThis = $(this);

                roxas.esop.assemble_accept_offer_information(uiAcceptOfferModal, uiThis);
            });

            uiPrintOfferLetterBtn.on('click', function(){
                roxas.esop.download_offer_letter(localStorage.getItem("stock_offer_id"), iUserID);
            });

            uiPrintAcceptanceLetterBtn.on('click', function(){
                roxas.esop.download_acceptance_letter(localStorage.getItem("stock_offer_id"), iUserID, localStorage.getItem("temp_shares"));
            });
        },

        /**
         * clear_accept_offer_modal
         * @description This function is for clearing data in accept offer modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_accept_offer_modal : function(){
            /*clear data*/
            uiAcceptOfferModal.find('input').removeClass('input-error').val('');
            uiAcceptOfferModal.find('.accept_offer_success_message').addClass('hidden');
            uiAcceptOfferModal.find('.accept_offer_error_message').addClass('hidden');
        },

        /**
         * assemble_accept_offer_information
         * @description This function will assemble adding of user esop data.
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_accept_offer_information : function(uiAcceptOfferModal, uiSendLetterBtn){
            var iEsopID = $('[data-container="esop_information"]').attr('data-stock-offer-esop-id');
            var iEsopParentID = $('[data-container="esop_information"]').attr('data-stock-offer-esop-parent-id');

            var iMaxStockAmount = $('[data-container="esop_information"]').attr('data-stock-offer-stock-amount');
            var uiStockAmountOnly = uiAcceptOfferModal.find('[name="stock_amount"]').val();
            var iStockAmount = 0;

            if(uiAcceptOfferModal.find('[name="offer_type"][id="only"]').is(':checked'))
            {
                iStockAmount = uiStockAmountOnly;
            }
            else
            {
                iStockAmount = iMaxStockAmount;
            }

            var oParams = {
                "esop_id" : iEsopID,
                "esop_log_id" : ((parseFloat(iEsopParentID) > 0) ? iEsopParentID : iEsopID ),
                "user_id" : iUserID,
                "user_name" : sFullName,
                "stock" : iStockAmount
            };

            roxas.esop.accept_offer(oParams, uiSendLetterBtn);
        },

        /**
         * accept_offer
         * @description This function will accept the offer
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        accept_offer : function(oParams, uiSendLetterBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/accept_offer",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiSendLetterBtn)) {
                            cr8v.show_spinner(uiSendLetterBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                $('[data-container="acceptance_letter"]').find('.accept_offer_success_message').removeClass('hidden').html('<p class="font-15 success_message">Offer Accepted successfully</p>');
                               
                                setTimeout(function(){
                                    window.location =  roxas.config('url.server.base') + "esop/stock_offers";
                                }, 1000)
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = $('[data-container="acceptance_letter"]').find('.accept_offer_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiSendLetterBtn)) {
                            cr8v.show_spinner(uiSendLetterBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * clear_set_offer_expiration_modal
         * @description This function is for clearing data in edit esop modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_set_offer_expiration_modal : function(){
            /*clear data*/
            uiSetOfferExpModal.find('input').removeClass('input-error').val('');
            uiSetOfferExpModal.find('.set_offer_expiration_success_message').addClass('hidden');
            uiSetOfferExpModal.find('.set_offer_expiration_error_message').addClass('hidden');
        },

        /**
         * assemble_set_offer_expiration
         * @description This function will assemble edit esop information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_set_offer_expiration : function(uiSetOfferExpModal, uiSetOfferExpBtn){
            var oParams = {
                'id' : uiEditEsopModal.attr('data-esop-id'),
                'esop_name' : uiEditEsopModal.attr('data-esop-name'),
                'type' : 'set_offer_expiration',
                'data' : [{
                    'offer_expiration' : uiSetOfferExpModal.find('input[name="offer_expiration"]').val()
                }]
            };

            roxas.esop.set_offer_expiration(oParams, uiSetOfferExpBtn);
        },

        /**
         * set_offer_expiration
         * @description This function will edit esop info
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        set_offer_expiration : function(oParams, uiSetOfferExpBtn, bSetOfferBatch){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/edit",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiSetOfferExpBtn)) {
                            cr8v.show_spinner(uiSetOfferExpBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bSetOfferBatch) && bSetOfferBatch)
                                {
                                    uiSetOfferExpBatchModal.find('.set_offer_expiration_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                                   
                                    setTimeout(function(){
                                        /*clear data*/
                                        if(uiSetOfferExpBatchModal.hasClass('showed'))
                                        {
                                            roxas.esop.clear_set_offer_expiration_modal();
                                            uiSetOfferExpBatchModal.find('.close-me').trigger('click');
                                        }
                                    }, 1000)

                                    var oEsopData = oData.data;
                                    
                                    var uiBatchTemplate = $('.esop_batch:not(.template)[data-esop-id="'+oEsopData.id+'"]');

                                    uiBatchTemplate.find('.undistributed_esop').addClass('hidden');
                                    uiBatchTemplate.find('.distributed_esop').addClass('hidden');
                                    uiBatchTemplate.find('.expiration_set').removeClass('hidden');
                                }
                                else
                                {
                                    uiSetOfferExpModal.find('.set_offer_expiration_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                                   
                                    setTimeout(function(){
                                        /*clear data*/
                                        if(uiSetOfferExpModal.hasClass('showed'))
                                        {
                                            roxas.esop.clear_set_offer_expiration_modal();
                                            uiSetOfferExpModal.find('.close-me').trigger('click');
                                        }
                                    }, 1000)

                                    var oEsopData = oData.data;
                                    
                                    roxas.esop.check_esop_buttons(oEsopData, true);
                                }
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    if(cr8v.var_check(bSetOfferBatch) && bSetOfferBatch)
                                    {
                                        var arrMessages = oData.message;
                                        var sErrorMessages = '';
                                        var uiErrorContainer = uiSetOfferExpBatchModal.find('.set_offer_expiration_error_message');
                                        for(var i = 0, max = arrMessages.length; i < max; i++)
                                        {
                                            var error_message = arrMessages[i];

                                            sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                        }

                                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                    }
                                    else
                                    {
                                        var arrMessages = oData.message;
                                        var sErrorMessages = '';
                                        var uiErrorContainer = uiSetOfferExpModal.find('.set_offer_expiration_error_message');
                                        for(var i = 0, max = arrMessages.length; i < max; i++)
                                        {
                                            var error_message = arrMessages[i];

                                            sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                        }

                                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                    }
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiSetOfferExpBtn)) {
                            cr8v.show_spinner(uiSetOfferExpBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * extend_offer_expiration
         * @description This function will extend the offer expiration
         * @dependencies N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Dru Moncatar
         * @method_id N/A
         */
        extend_offer_expiration : function(oParams) {
            if(oParams.status != undefined) {

                var oEsopData = oParams.data,
                oEsopDetails = oParams.details,
                uiModal = {},
                btnSubmitExtend = {},
                btnCloseModal = {},
                sNewExpirationDate = '',
                btnExtendExpiration = $('.btn-extend-offer-expiration'),
                oAjaxConfig = {
                   "type"   : "POST",
                    "data"   : {},
                    "url"    : roxas.config('url.server.base') + "esop/submit_offer_extension",
                    "success": function (oResult) {
                        console.log(oResult);
                        uiModal.find('.extend-success').html('<p class="font-15 success_message display-inline-mid">' + oResult.message + '</p>');
                        setTimeout(function() {
                            // uiModal.find('.success_message').html('').hide();
                            // uiModal.removeClass('showed');
                            window.location.href = roxas.config('url.server.base') + "esop/view_esop_batch";
                        },1000);
                    }    
                };

                if(oEsopData[0].offer_expiration !== '0000-00-00') {

                    btnExtendExpiration.off('click').on('click', function() {
                    // console.log(oEsopData);
                    // console.log(oEsopDetails);

                        uiModal = $('.modal-extend-expiration');
                        btnExtendExpiration = uiModal.find('#submit_extend_offer');
                        btnCloseModal = uiModal.find('.close-me')
                        uiModal.addClass('showed');
                        // uiModal.find('input').val(oEsopData[0].offer_expiration);
                        uiModal.find('.old-expiry-date').html(oEsopData[0].offer_expiration);

                        btnExtendExpiration.off('click').on('click', function() {
                            sNewExpirationDate = uiModal.find('input').val().split('/');
                            sNewExpirationDate = sNewExpirationDate[2] + '/' + sNewExpirationDate[0] + '/' + sNewExpirationDate[1];
                            oAjaxConfig['data'] = {
                                offer_expiration : sNewExpirationDate,
                                esop_id : oEsopDetails.esop_id
                            };

                            roxas.esop.ajax(oAjaxConfig)
                        });

                        btnCloseModal.off('click').on('click', function() {
                            $(this).closest('.modal-container').removeClass('showed');
                        });
                    });
                    
                } else {
                    btnExtendExpiration.hide();
                }

                


            } else {

                setTimeout(function() {
                var uiDiv = $('[data-container="esop_information"]:visible'),
                    oEsopDetails = {
                        container : uiDiv.data('container'),
                        esop_currency : uiDiv.data('esopCurrency'),
                        esop_grant_date : uiDiv.data('esopGrantDate'),
                        esop_id : uiDiv.data('esopId'),
                        esop_name : uiDiv.data('esopName'),
                        esop_price_per_share : uiDiv.data('esopPricePerShare'),
                        esop_total_share_qty : uiDiv.data('esopTotalShareQty'),
                        esop_vesting_years : uiDiv.data('esopVestingYear')
                    },
                    oAjaxConfig = {
                        "type"   : "GET",
                        "data"   : {id : oEsopDetails.esop_id},
                        "url"    : roxas.config('url.server.base') + "esop/get_esop_info",
                        "success": function (oResult) {
                            var oData = {
                                status : 'set',
                                data : oResult.data,
                                details : oEsopDetails
                            };

                            roxas.esop.extend_offer_expiration(oData);
                        }
                    }


                    // console.log(uiDiv.attr('data-esop-id'));
                    console.log(oEsopDetails);
                    roxas.esop.ajax(oAjaxConfig);
                },3000);

            }
            
        },

        /**
         * view_esop
         * @description This function is for viewing the esop
         * @dependencies 
         * @param {int} iEsopID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_esop : function(iEsopID, bIsLetterSent, sEsopName){
            if(cr8v.var_check(iEsopID) && cr8v.var_check(sEsopName))
            {
                localStorage.removeItem("esop_id");
                localStorage.setItem('esop_id', iEsopID);

                localStorage.removeItem("parent_esop_name");
                localStorage.setItem('parent_esop_name', sEsopName);

                if(cr8v.var_check(bIsLetterSent))
                {
                    if(bIsLetterSent == 'true')
                    {
                        window.location =  roxas.config('url.server.base') + "esop/view_esop_batch";
                    }
                    else
                    {
                        window.location =  roxas.config('url.server.base') + "esop/view_esop";
                    }
                }
                else
                {
                    window.location =  roxas.config('url.server.base') + "esop/view_esop";
                }
            }
        },

        /**
         * add_edit_batch_set_local_storage
         * @description This function is for viewing the esop
         * @dependencies 
         * @param {int} iEsopID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_edit_batch_set_local_storage : function(iEsopID, bIsEdit){
            if(cr8v.var_check(iEsopID))
            {
                localStorage.removeItem("esop_batch_id");
                localStorage.setItem('esop_batch_id', iEsopID);

                if(cr8v.var_check(bIsEdit) && bIsEdit)
                {
                    window.location =  roxas.config('url.server.base') + "esop/batch_edit_distribute_shares";
                }
                else
                {
                    window.location =  roxas.config('url.server.base') + "esop/batch_add_distribute_shares";
                }
            }
        },

        /**
         * view_stock_offer
         * @description This function is for viewing the offer
         * @dependencies 
         * @param {int} iOfferID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_stock_offer : function(iOfferID){
            if(cr8v.var_check(iOfferID))
            {
                localStorage.removeItem("stock_offer_id");
                localStorage.setItem('stock_offer_id', iOfferID);
                window.location =  roxas.config('url.server.base') + "esop/view_stock_offer";
            }
        },

        /**
         * bind_dropdown_events
         * @description This function will bind the events for the dropdowns
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_dropdown_events : function(){
            uiClassCurrencyDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiCurrencyDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiOfferLetterDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiAcceptanceLetterDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiClassPaymentDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiPaymentDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiEsopListDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled',true);
            uiUsersDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled',true);
            uiCompanyDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled',true);

            uiCompanyDropdown.on('click','option.company',function(){
                var uiThis = $(this);
                var scompanyID = uiThis.attr('data-value');

                uiCompanyDropdown.attr('company_id',scompanyID);
            });

            uiUsersDropdown.on('click','option.user',function(){
                var uiThis = $(this);
                var sUserlist = uiThis.attr('data-value');

                uiUsersDropdown.attr('user_id',sUserlist);
            });

            uiEsopListDropdown.on('click','option.esop',function(){
                var uiThis = $(this);
                var sEsoplist = uiThis.attr('data-value');

                uiEsopListDropdown.attr('esop_list',sEsoplist);
            });
            
            uiCurrencyDropdown.on('click', '.option.currency', function(){
                var uiThis = $(this);
                var iCurrencyID = uiThis.attr('data-value');

                uiCurrencyDropdown.attr('currency_id', iCurrencyID);
            });

            uiPaymentDropdown.on('click', '.option.payment_type', function(){
                var uiThis = $(this);
                var iValue = uiThis.attr('data-value');

                uiPaymentDropdown.attr('payment_type_id', iValue);
            });

            uiEsopCompaniesContainer.on('click', '.offer_letter_dropdown .option', function(){
                var uiThis = $(this);
                var iOfferLetterID = uiThis.attr('data-value');

                uiThis.parents('.offer_letter_dropdown:first').attr('offer_letter_id', iOfferLetterID);
            });

            uiEsopCompaniesContainer.find('.offer_letter_dropdown').find('.option:first').trigger('click');

            uiEsopCompaniesContainer.on('click', '.acceptance_letter_dropdown .option', function(){
                var uiThis = $(this);
                var iAcceptanceLetterID = uiThis.attr('data-value');

                uiThis.parents('.acceptance_letter_dropdown:first').attr('acceptance_letter_id', iAcceptanceLetterID);
            });

            uiEsopCompaniesContainer.find('.acceptance_letter_dropdown').find('.option:first').trigger('click');

            uiClassCurrencyDropdown.on('click', '.option.currency', function(){
                var uiThis = $(this);
                var iCurrencyID = uiThis.attr('data-value');

                uiClassCurrencyDropdown.attr('currency_id', iCurrencyID);
            });
        },

        /**
         * bind_edit_distribute_share_events
         * @description This function will bind the events for the distributing shares
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_edit_distribute_share_events : function(){

            uiEsopCompaniesContainer.on('change', '[name="company_alloted_shares"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {
                    uiThis
                        .val(number_format(uiThis.val(), 2))
                        .parents('.esop_companies:first')
                        .find('[data-container="esop_employees_container"]')
                        .find('input[name="employee_alloted_shares"]').attr('disabled', false);
                }
                else
                {
                    uiThis
                        .parents('.esop_companies:first')
                        .find('[data-container="esop_employees_container"]')
                        .find('input[name="employee_alloted_shares"]').attr('disabled', true).val('');
                }
            });

            uiEsopCompaniesContainer.on('blur', '[name="employee_alloted_shares"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            uiFinalizeEditDistributionSharesBtn.on('click', function(){
                roxas.esop.validate_edit_distribution_shares_information(uiFinalizeEditDistributionSharesBtn);
            });
        },

        /**
         * validate_edit_distribution_shares_information
         * @description This function will validate distribute shares information
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        validate_edit_distribution_shares_information : function(uiFinalizeEditDistributionSharesBtn){

            var iEsopTotalShares = $('[data-container="esop_information"]').attr('data-esop-total-share-qty');
            var iCompanyAllotedShares = 0;
            var sErrorMessages = '';
            var errorCtr = 0;

            uiEsopCompaniesContainer.find('.esop_companies:not(.template)').each(function (k, company) {
                var iOfferLetterID = $(company).find('.offer_letter_dropdown').attr('offer_letter_id');
                var iAcceptanceLetterID = $(company).find('.acceptance_letter_dropdown').attr('acceptance_letter_id');

                if(!cr8v.var_check(iOfferLetterID) && sErrorMessages.indexOf('<p class="font-15 error_message">There are no offer letters yet in the system. Please add an offer letter first.</p>') == -1)
                {
                    $('.offer_letter_dropdown').find('input').val('');
                    sErrorMessages += '<p class="font-15 error_message">There are no offer letters yet in the system. Please add an offer letter first.</p>';
                    errorCtr++;
                }

                if(!cr8v.var_check(iAcceptanceLetterID) && sErrorMessages.indexOf('<p class="font-15 error_message">There are no acceptance letters yet in the system. Please add an acceptance letter first.</p>') == -1)
                {
                    $('.acceptance_letter_dropdown').find('input').val('');
                    sErrorMessages += '<p class="font-15 error_message">There are no acceptance letters yet in the system. Please add an acceptance letter first.</p>';
                    errorCtr++;
                }

                if($(company).find('[name="company_alloted_shares"]').val() != '')
                {
                    var iCompanyAllotedShare = cr8v.remove_commas($(company).find('[name="company_alloted_shares"]').val());
                    iCompanyAllotedShares = parseFloat(iCompanyAllotedShares) + parseFloat(iCompanyAllotedShare);

                    var iEmployeeAllotedShares = 0;

                    $(company).find('.esop_employees:not(.template)').find('[name="employee_alloted_shares"]').each(function (i, employee){

                        if($(employee).val() != '')
                        {
                            var iEmployeeAllotedShare = cr8v.remove_commas($(employee).val());
                            iEmployeeAllotedShares = parseFloat(iEmployeeAllotedShares) + parseFloat(iEmployeeAllotedShare);
                        }
                    });

                    if(iEmployeeAllotedShares == 0)
                    {
                        sErrorMessages += '<p class="font-15 error_message">There are no distributed shares to '+$(company).attr('data-company-name')+'</p>';
                        errorCtr++;
                    }
                    else if(parseFloat(iEmployeeAllotedShares) > parseFloat(iCompanyAllotedShare))
                    {
                        sErrorMessages += '<p class="font-15 error_message">Shares distributed to the employees of '+$(company).attr('data-company-name')+' have exceeded total alloted shares. Please re-distribute the shares accordingly</p>';
                        errorCtr++;
                    }
                }
            });

            iCompanyAllotedShares = parseFloat(iCompanyAllotedShares);
            iEsopTotalShares = parseFloat(iEsopTotalShares);

            if(iCompanyAllotedShares == 0)
            {
                sErrorMessages += '<p class="font-15 error_message">There are no shares distributed</p>';
                errorCtr++;
            }
            else if(parseFloat(iCompanyAllotedShares) > parseFloat(iEsopTotalShares))
            {
                //Shares of Companies have exceeded the Total Alloted Shares. Please re-distribute the shares accordingly
                sErrorMessages += '<p class="font-15 error_message">Shares of Companies have exceeded the Total Alloted Shares. Please re-distribute the shares accordingly</p>';
                errorCtr++;
            }

            var uiErrorContainer = $('[data-container="esop_information"]').find('.finalize_distribution_shares_error_message');
            cr8v.add_error_message(false, uiErrorContainer);

            if(errorCtr > 0)
            {
                cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
            }
            else
            {
                roxas.esop.assemble_edit_distribution_shares_information(uiFinalizeEditDistributionSharesBtn);
            }
        },

        /**
         * assemble_edit_distribution_shares_information
         * @description This function will assemble add distribute shares information
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_edit_distribution_shares_information : function(uiFinalizeEditDistributionSharesBtn){

            var iEsopID = $('[data-container="esop_information"]').attr('data-esop-id');
            var sEsopName = $('[data-container="esop_information"]').attr('data-esop-name');
        
            var oParams = { "esop_id" : iEsopID, "esop_name" : sEsopName, "employee_data" : [], "allotment_data" : [] };

            uiEsopCompaniesContainer.find('.esop_companies:not(.template)').each(function (k, company) {
                if($(company).find('[name="company_alloted_shares"]').val() != '')
                {
                    var iTotalAllotment = $(company).find('[name="company_alloted_shares"]').val();
                    var iCompanyID = $(company).attr('data-company-id');

                    $(company).find('.esop_employees:not(.template)').find('[name="employee_alloted_shares"]').each(function (i, employee){
                        var iUserID = $(employee).parents('.esop_employees:first').attr('data-user-id');
                        var iOfferLetterID = $(employee).parents('.esop_companies:first').find('.offer_letter_dropdown').attr('offer_letter_id');
                        var iAcceptanceLetterID = $(employee).parents('.esop_companies:first').find('.acceptance_letter_dropdown').attr('acceptance_letter_id');
                        var iStockAmount = $(employee).val();

                        if(iStockAmount != '')
                        {
                            var oEmployeeData = {
                                "esop_id": iEsopID,
                                "user_id": iUserID,
                                "stock_amount": iStockAmount,
                                "offer_letter_id": iOfferLetterID,
                                "acceptance_letter_id": iAcceptanceLetterID
                            };
                            oParams.employee_data.push(oEmployeeData);
                        }
                    });

                    var oCompanyData = {
                        "esop_id": iEsopID,
                        "company_id" : iCompanyID,
                        "department_id" : '',
                        "allotment" : iTotalAllotment
                    };
                    oParams.allotment_data.push(oCompanyData);
                }
            });

            roxas.esop.edit_distribution_shares(oParams, uiFinalizeEditDistributionSharesBtn);
        },

        /**
         * edit_distribution_shares
         * @description This function will add distribution shares
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        edit_distribution_shares : function(oParams, uiFinalizeEditDistributionSharesBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/edit_distribute_share",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiFinalizeEditDistributionSharesBtn)) {
                            cr8v.show_spinner(uiFinalizeEditDistributionSharesBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                $('[data-container="esop_information"]').find('.finalize_distribution_shares_success_message').removeClass('hidden').html('<p class="font-15 success_message">Distribution shares edited successfully.</p>');

                                setTimeout(function(){
                                    if(window.location.href.indexOf('esop/batch_edit_distribute_shares') > -1)
                                    {
                                        window.location =  roxas.config('url.server.base') + "esop/view_esop_batch";
                                    }
                                  else
                                    {
                                        window.location =  roxas.config('url.server.base') + "esop/view_esop";
                                    }
                                }, 1000)
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiAddEsopModal.find('.finalize_distribution_shares_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiFinalizeEditDistributionSharesBtn)) {
                            cr8v.show_spinner(uiFinalizeEditDistributionSharesBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * bind_add_distribute_share_events
         * @description This function will bind the events for the distributing shares
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_add_distribute_share_events : function(){

            uiEsopCompaniesContainer.on('change', '[name="company_alloted_shares"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {
                    uiThis
                        .val(number_format(uiThis.val(), 2))
                        .parents('.esop_companies:first')
                        .find('[data-container="esop_employees_container"]')
                        .find('input[name="employee_alloted_shares"]').attr('disabled', false);
                }
                else
                {
                    uiThis
                        .parents('.esop_companies:first')
                        .find('[data-container="esop_employees_container"]')
                        .find('input[name="employee_alloted_shares"]').attr('disabled', true).val('');
                }
            });

            uiEsopCompaniesContainer.on('blur', '[name="employee_alloted_shares"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            uiFinalizeAddDistributionSharesBtn.on('click', function(){
                roxas.esop.validate_add_distribution_shares_information(uiFinalizeAddDistributionSharesBtn);
            });
        },

        /**
         * validate_add_distribution_shares_information
         * @description This function will validate distribute shares information
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        validate_add_distribution_shares_information : function(uiFinalizeAddDistributionSharesBtn){
            
            var iEsopTotalShares = $('[data-container="esop_information"]').attr('data-esop-total-share-qty');
            var iCompanyAllotedShares = 0;
            var sErrorMessages = '';
            var errorCtr = 0;

            uiEsopCompaniesContainer.find('.esop_companies:not(.template)').each(function (k, company) {
                var iOfferLetterID = $(company).find('.offer_letter_dropdown').attr('offer_letter_id');
                var iAcceptanceLetterID = $(company).find('.acceptance_letter_dropdown').attr('acceptance_letter_id');

                if(!cr8v.var_check(iOfferLetterID) && sErrorMessages.indexOf('<p class="font-15 error_message">There are no offer letters yet in the system. Please add an offer letter first.</p>') == -1)
                {
                    $('.offer_letter_dropdown').find('input').val('');
                    sErrorMessages += '<p class="font-15 error_message">There are no offer letters yet in the system. Please add an offer letter first.</p>';
                    errorCtr++;
                }

                if(!cr8v.var_check(iAcceptanceLetterID) && sErrorMessages.indexOf('<p class="font-15 error_message">There are no acceptance letters yet in the system. Please add an acceptance letter first.</p>') == -1)
                {
                    $('.acceptance_letter_dropdown').find('input').val('');
                    sErrorMessages += '<p class="font-15 error_message">There are no acceptance letters yet in the system. Please add an acceptance letter first.</p>';
                    errorCtr++;
                }

                if($(company).find('[name="company_alloted_shares"]').val() != '')
                {
                    var iCompanyAllotedShare = cr8v.remove_commas($(company).find('[name="company_alloted_shares"]').val());
                    iCompanyAllotedShares = parseFloat(iCompanyAllotedShares) + parseFloat(iCompanyAllotedShare);

                    var iEmployeeAllotedShares = 0;

                    $(company).find('.esop_employees:not(.template)').find('[name="employee_alloted_shares"]').each(function (i, employee){

                        if($(employee).val() != '')
                        {
                            var iEmployeeAllotedShare = cr8v.remove_commas($(employee).val());
                            iEmployeeAllotedShares = parseFloat(iEmployeeAllotedShares) + parseFloat(iEmployeeAllotedShare);
                        }
                    });
            
                    if(iEmployeeAllotedShares == 0)
                    {
                        sErrorMessages += '<p class="font-15 error_message">There are no distributed shares to '+$(company).attr('data-company-name')+'</p>';
                        errorCtr++;
                    }
                    else if(parseFloat(iEmployeeAllotedShares) > parseFloat(iCompanyAllotedShare))
                    {
                        sErrorMessages += '<p class="font-15 error_message">Shares distributed to the employees of '+$(company).attr('data-company-name')+' have exceeded total alloted shares. Please re-distribute the shares accordingly</p>';
                        errorCtr++;
                    }
                }
            });
            
            iCompanyAllotedShares = parseFloat(iCompanyAllotedShares);
            iEsopTotalShares = parseFloat(iEsopTotalShares);

            if(iCompanyAllotedShares == 0)
            {
                sErrorMessages += '<p class="font-15 error_message">There are no shares distributed</p>';
                errorCtr++;
            }
            else if(parseFloat(iCompanyAllotedShares) > parseFloat(iEsopTotalShares))
            {
                //Shares of Companies have exceeded the Total Alloted Shares. Please re-distribute the shares accordingly
                sErrorMessages += '<p class="font-15 error_message">Shares of Companies have exceeded the Total Alloted Shares. Please re-distribute the shares accordingly</p>';
                errorCtr++;
            }

            var uiErrorContainer = $('[data-container="esop_information"]').find('.finalize_distribution_shares_error_message');
            cr8v.add_error_message(false, uiErrorContainer);

            if(errorCtr > 0)
            {
                cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
            }
            else
            {
                roxas.esop.assemble_add_distribution_shares_information(uiFinalizeAddDistributionSharesBtn);
            }
        },

        /**
         * assemble_add_distribution_shares_information
         * @description This function will assemble add distribute shares information
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_add_distribution_shares_information : function(uiFinalizeAddDistributionSharesBtn){

            var iEsopID = $('[data-container="esop_information"]').attr('data-esop-id');
            var sEsopName = $('[data-container="esop_information"]').attr('data-esop-name');

            var oParams = { "esop_id" : iEsopID, "esop_name" : sEsopName, "employee_data" : [], "allotment_data" : [] };

            uiEsopCompaniesContainer.find('.esop_companies:not(.template)').each(function (k, company) {
                if($(company).find('[name="company_alloted_shares"]').val() != '')
                {
                    var iTotalAllotment = $(company).find('[name="company_alloted_shares"]').val();
                    var iCompanyID = $(company).attr('data-company-id');
                    
                    $(company).find('.esop_employees:not(.template)').find('[name="employee_alloted_shares"]').each(function (i, employee){
                        var iUserID = $(employee).parents('.esop_employees:first').attr('data-user-id');
                        var iOfferLetterID = $(employee).parents('.esop_companies:first').find('.offer_letter_dropdown').attr('offer_letter_id');
                        var iAcceptanceLetterID = $(employee).parents('.esop_companies:first').find('.acceptance_letter_dropdown').attr('acceptance_letter_id');
                        var iStockAmount = $(employee).val();

                        if(iStockAmount != '')
                        {
                            var oEmployeeData = {
                                "esop_id": iEsopID, 
                                "user_id": iUserID, 
                                "stock_amount": iStockAmount, 
                                "offer_letter_id": iOfferLetterID,
                                "acceptance_letter_id": iAcceptanceLetterID
                            };
                            oParams.employee_data.push(oEmployeeData);
                        }
                    });

                    var oCompanyData = {
                        "esop_id": iEsopID,
                        "company_id" : iCompanyID,
                        "department_id" : '0',
                        "allotment" : iTotalAllotment
                    };
                    oParams.allotment_data.push(oCompanyData);
                }
            });

            roxas.esop.add_distribution_shares(oParams, uiFinalizeAddDistributionSharesBtn);
        },

        /**
         * add_distribution_shares
         * @description This function will add distribution shares
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_distribution_shares : function(oParams, uiFinalizeAddDistributionSharesBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/add_distribute_share",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiFinalizeAddDistributionSharesBtn)) {
                            cr8v.show_spinner(uiFinalizeAddDistributionSharesBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                $('[data-container="esop_information"]').find('.finalize_distribution_shares_success_message').removeClass('hidden').html('<p class="font-15 success_message">Distribution shares added successfully.</p>');
                               
                                setTimeout(function(){
                                    if(window.location.href.indexOf('esop/batch_add_distribute_shares') > -1)
                                    {
                                        window.location =  roxas.config('url.server.base') + "esop/view_esop_batch";
                                    }
                                    else
                                    {
                                        window.location =  roxas.config('url.server.base') + "esop/view_esop";
                                    }
                                }, 1000)
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiAddEsopModal.find('.finalize_distribution_shares_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiFinalizeAddDistributionSharesBtn)) {
                            cr8v.show_spinner(uiFinalizeAddDistributionSharesBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * bind_edit_esop_events
         * @description This function is for binding events for edit esop info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_edit_esop_events : function(){

            /*save edit esop*/
            uiEditEsopBtn.on('click', function(){
                uiEditEsopForm.submit();
            });

            /*prevent on submit of form*/
            uiEditEsopForm.on('submit', function (e) {
                e.preventDefault();
            });

            /*add a validation to editing esop*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiEditEsopBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiEditEsopModal.find('.edit_esop_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiEditEsopModal.find('.edit_esop_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    var sDate = uiEditEsopForm.find('input[name="grant_date"]').val();
                    var bValidGrantDate = cr8v.validate_date(sDate, 'MM/DD/YYYY');

                    if(bValidGrantDate)
                    {
                        cr8v.show_spinner(uiEditEsopBtn, true);
                        roxas.esop.assemble_edit_esop_information(uiEditEsopModal, uiEditEsopBtn);
                    }
                    
                }
            };

            uiEditEsopForm.on('blur', 'input[name="total_share_qty"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {                
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            uiEditEsopForm.on('blur', 'input[name="price_per_share"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {                
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            /*bind inputs that should prevent non numeric values*/
            cr8v.input_numeric($('input[name="total_share_qty"]'));
            cr8v.input_numeric($('input[name="price_per_share"]'));
            cr8v.input_numeric($('input[name="vesting_years"]'));

            /*bind the validation to edit esop form*/
            cr8v.validate_form(uiEditEsopForm, oValidationConfig);
        },

        /**
         * clear_edit_esop_modal
         * @description This function is for clearing data in edit esop modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_edit_esop_modal : function(){
            /*clear data*/
            uiEditEsopModal.find('input').removeClass('input-error');
            uiEditEsopModal.find('.edit_esop_success_message').addClass('hidden');
            uiEditEsopModal.find('.edit_esop_error_message').addClass('hidden');
        },

        /**
         * assemble_edit_esop_information
         * @description This function will assemble edit esop information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_edit_esop_information : function(uiEditEsopModal, uiEditEsopBtn){
            var oParams = {
                'id' : uiEditEsopModal.attr('data-esop-id'),
                'type' : 'esop_normal_edit',
                'data' : [{
                    'name'              : uiEditEsopModal.find('input[name="esop_name"]').val(),
                    'grant_date'        : uiEditEsopModal.find('input[name="grant_date"]').val(),
                    'total_share_qty'   : uiEditEsopModal.find('input[name="total_share_qty"]').val(),
                    'price_per_share'   : uiEditEsopModal.find('input[name="price_per_share"]').val(),
                    'currency'          : uiCurrencyDropdown.attr('currency_id'),
                    'currency_name'     : uiCurrencyDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                    'vesting_years'     : uiEditEsopModal.find('input[name="vesting_years"]').val(),
                    'created_by'        : iUserID
                }]
            };

            roxas.esop.edit_esop(oParams, uiEditEsopBtn);
        },

        /**
         * edit_esop
         * @description This function will edit esop info
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        edit_esop : function(oParams, uiEditEsopBtn, bEditBatch){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/edit",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiEditEsopBtn)) {
                            cr8v.show_spinner(uiEditEsopBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bEditBatch) && bEditBatch)
                                {
                                    uiEditEsopBatchModal.find('.edit_esop_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                                   
                                    setTimeout(function(){
                                        /*clear data*/
                                        if(uiEditEsopBatchModal.hasClass('showed'))
                                        {
                                            roxas.esop.clear_edit_esop_batch_modal();
                                            uiEditEsopBatchModal.find('.close-me').trigger('click');
                                        }
                                    }, 1000)
                                    
                                    var oEsopData = oData.data;
                                    var uiBatchTemplate = $('.esop_batch:not(.template)[data-esop-id="'+oEsopData.id+'"]');
                                    var uiBatchAttr = uiBatchTemplate.attr('data-attr');

                                    uiBatchTemplate.attr('data-esop-name', oEsopData.name);
                                    $('[data-container="esop_batch_container"]').find('label[data-attr="'+uiBatchAttr+'"]').text(oEsopData.name);
                                }
                                else
                                {                                
                                    uiEditEsopModal.find('.edit_esop_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                                   
                                    setTimeout(function(){
                                        /*clear data*/
                                        if(uiEditEsopModal.hasClass('showed'))
                                        {
                                            roxas.esop.clear_edit_esop_modal();
                                            uiEditEsopModal.find('.close-me').trigger('click');
                                        }
                                    }, 1000)

                                    var oEsopData = oData.data;
                                    var uiTemplate = $('[data-container="esop_information"]');
                                    var uiManipulatedTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiTemplate);
                                    
                                    var uiTemplateForEdit = uiEditEsopModal;
                                    var uiManipulatedTemplateForEdit = roxas.esop.populate_edit_info(oEsopData, uiTemplateForEdit);
                                }
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    if(cr8v.var_check(bEditBatch) && bEditBatch)
                                    {
                                        var arrMessages = oData.message;
                                        var sErrorMessages = '';
                                        var uiErrorContainer = uiEditEsopBatchModal.find('.edit_esop_error_message');
                                        for(var i = 0, max = arrMessages.length; i < max; i++)
                                        {
                                            var error_message = arrMessages[i];

                                            sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                        }

                                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                    }
                                    else
                                    {
                                        var arrMessages = oData.message;
                                        var sErrorMessages = '';
                                        var uiErrorContainer = uiEditEsopModal.find('.edit_esop_error_message');
                                        for(var i = 0, max = arrMessages.length; i < max; i++)
                                        {
                                            var error_message = arrMessages[i];

                                            sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                        }

                                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                    }
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiEditEsopBtn)) {
                            cr8v.show_spinner(uiEditEsopBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * bind_add_esop_events
         * @description This function is for binding events for add esop info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_add_esop_events : function(){

            /*save new esop*/
            uiAddEsopBtn.on('click', function(){
                uiAddEsopForm.submit();
            });

            /*prevent on submit of form*/
            uiAddEsopForm.on('submit', function (e) {
                e.preventDefault();
            });

            /*add a validation to adding a new esop*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiAddEsopBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiAddEsopModal.find('.add_esop_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiAddEsopModal.find('.add_esop_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    var sDate = uiAddEsopForm.find('input[name="grant_date"]').val();
                    var bValidGrantDate = cr8v.validate_date(sDate, 'MM/DD/YYYY');

                    if(bValidGrantDate)
                    {
                        cr8v.show_spinner(uiAddEsopBtn, true);
                        roxas.esop.assemble_add_esop_information(uiAddEsopForm, uiAddEsopBtn);
                    }
                    
                }
            };

            uiAddEsopForm.on('blur', 'input[name="total_share_qty"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {                
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            uiAddEsopForm.on('blur', 'input[name="price_per_share"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {                
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            /*bind inputs that should prevent non numeric values*/
            cr8v.input_numeric($('input[name="total_share_qty"]'));
            cr8v.input_numeric($('input[name="price_per_share"]'));
            cr8v.input_numeric($('input[name="vesting_years"]'));

            /*bind the validation to add new esop form*/
            cr8v.validate_form(uiAddEsopForm, oValidationConfig);
        },

        /**
         * clear_add_esop_modal
         * @description This function is for clearing data in add esop modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_add_esop_modal : function(){
            /*clear data*/
            uiAddEsopModal.find('input:not(.dd-txt)').removeClass('input-error').val('');
            uiAddEsopModal.find('.add_esop_success_message').addClass('hidden');
            uiAddEsopModal.find('.add_esop_error_message').addClass('hidden');
        },

        /**
         * assemble_add_esop_information
         * @description This function will assemble add esop information
         * @dependencies N/A
         * @param {jQuery} uiAddEsopForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_add_esop_information : function(uiAddEsopForm, uiAddEsopBtn){
            var aShareAllotment = [],
                oParams = {
                'name'              : uiAddEsopForm.find('input[name="esop_name"]').val(),
                'grant_date'        : uiAddEsopForm.find('input[name="grant_date"]').val(),
                'total_share_qty'   : uiAddEsopForm.find('input[name="total_share_qty"]').val(),
                'price_per_share'   : uiAddEsopForm.find('input[name="price_per_share"]').val(),
                'currency'          : uiCurrencyDropdown.attr('currency_id'),
                'vesting_years'     : uiAddEsopForm.find('input[name="vesting_years"]').val(),
                'created_by'        : iUserID,
                'status'            : 1,
                'is_deleted'        : 0,
                'parent_id'         : 0,
                'type'              : 'main_esop'
            };

            var uiShareAllotmentInputs = uiAddEsopModal.find('tr[data-template="share_allotment"]:not(.template)');

            if($('#share_allotment_checkbox').is(':checked'))
            {
                $.each(uiShareAllotmentInputs, function() {
                    if($(this).find('input[data-label="esop_id"]').is(':checked'))
                    {
                        if($(this).find('input.share_allotment_input').val() != "")
                        {
                            var iEsopID = $(this).find('input[data-label="esop_id"]').attr('esop_id'),
                                iEsopTotalShare = $(this).find('input[data-label="esop_id"]').attr('total_share_qty'),
                                iEsopDeduction = cr8v.remove_commas($(this).find('input.share_allotment_input').val());
                            
                            aShareAllotment.push({
                                    'id' : iEsopID,
                                    'total_share_qty' : (iEsopTotalShare - iEsopDeduction),
                                    'deduction' : iEsopDeduction
                            })    
                        }
                    }
                })


            }

            oParams.share_allotment = aShareAllotment;

            roxas.esop.add_esop(oParams, uiAddEsopBtn);

        },

        /**
         * add_esop
         * @description This function will add department code, name and description
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_esop : function(oParams, uiAddEsopBtn, bFromEsopBatch){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/add",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiAddEsopBtn)) {
                            cr8v.show_spinner(uiAddEsopBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bFromEsopBatch) && bFromEsopBatch)
                                {
                                    uiCreateEsopBatchModal.find('.create_esop_batch_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                                    setTimeout(function(){
                                        /*clear data*/
                                        if(uiCreateEsopBatchModal.hasClass('showed'))
                                        {
                                            roxas.esop.clear_create_esop_batch_modal();
                                            uiCreateEsopBatchModal.find('.close-me').trigger('click');
                                        }
                                        window.location = roxas.config('url.server.base') + 'esop/view_esop_batch';
                                    }, 1000)
                                }
                                else
                                {
                                    uiAddEsopModal.find('.add_esop_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');

                                    setTimeout(function(){
                                        /*clear data*/
                                        if(uiAddEsopModal.hasClass('showed'))
                                        {
                                            roxas.esop.clear_add_esop_modal();
                                            uiAddEsopModal.find('.close-me').trigger('click');
                                        }
                                    }, 1000)
                                    
                                    var oEsopData = oData.data;

                                    var uiTemplateList = $('.esop_list.template').clone().removeClass('template hidden');
                                    var uiManipulatedTemplateList = roxas.esop.manipulate_template_list(oEsopData, uiTemplateList);

                                    var uiTemplateGrid = $('.esop_grid.template').clone().removeClass('template hidden');
                                    var uiManipulatedTemplateGrid = roxas.esop.manipulate_template_grid(oEsopData, uiTemplateGrid);

                                    uiEsopListContainer.prepend(uiManipulatedTemplateList);
                                    uiEsopGridContainer.prepend(uiManipulatedTemplateGrid);
                                    
                                    uiNoResultsFound.addClass('template hidden');
                                }
                            }
                            else
                            {
                                if(cr8v.var_check(bFromEsopBatch) && bFromEsopBatch)
                                {
                                    if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                    {
                                        var arrMessages = oData.message;
                                        var sErrorMessages = '';
                                        var uiErrorContainer = uiCreateEsopBatchModal.find('.create_esop_batch_error_message');
                                        for(var i = 0, max = arrMessages.length; i < max; i++)
                                        {
                                            var error_message = arrMessages[i];

                                            sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                        }

                                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                    }
                                }
                                else
                                {
                                    if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                    {
                                        var arrMessages = oData.message;
                                        var sErrorMessages = '';
                                        var uiErrorContainer = uiAddEsopModal.find('.add_esop_error_message');
                                        for(var i = 0, max = arrMessages.length; i < max; i++)
                                        {
                                            var error_message = arrMessages[i];

                                            sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                        }

                                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                    }
                                }
                            }
                        }

                        roxas.esop.fetch_available_share_allotment();
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiAddEsopBtn)) {
                            cr8v.show_spinner(uiAddEsopBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * get_esop
         * @description This function is for getting and rendering esop on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_esop : function(oParams, uiThis, bFromViewEsop, bFromAdd, bFromEdit, bFromEsopBatch, bFromDashboard, bUploadPayment){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }

                        if(window.location.href.indexOf('esop/esop_list') > -1 || window.location.href.indexOf('esop/dashboard') > -1)
                        {
                            $("body").css({overflow:'hidden'});

                            $("#show-loading-data-shares").addClass("showed");
        
                            $("#show-loading-data-shares .close-me").on("click",function(){
                                $("body").css({'overflow-y':'initial'});
                                $("#show-loading-data-shares").removeClass("showed");
                            });
                        }

                        
                             
                    },
                    "success": function (oData) {
                        if(cr8v.var_check(oData))
                        {   
                            console.log(oData)
                            uiEsopGridContainer.find('.esop_grid:not(.template)').remove();
                            uiEsopListContainer.find('.esop_list:not(.template)').remove();
                            uiNoResultsFound.addClass('template hidden');

                            if(oData.status == true)
                            {
                                /*if from view esop page*/
                                if(cr8v.var_check(bFromViewEsop) && bFromViewEsop)
                                {
                                    for(var i in oData.data)
                                    {
                                        var oEsopData = oData.data[i];
                                        var uiTemplate = $('[data-container="esop_information"]');
                                        var uiManipulatedTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiTemplate);
                                        
                                        var uiTemplateForEdit = uiEditEsopModal;
                                        var uiManipulatedTemplateForEdit = roxas.esop.populate_edit_info(oEsopData, uiTemplateForEdit);

                                        if(!cr8v.var_check(bUploadPayment) && !bUploadPayment)
                                        {
                                            roxas.esop.check_esop_buttons(oEsopData);
                                        }

                                        if(cr8v.var_check(bFromEdit) && bFromEdit && count(oEsopData.distribution) > 0)
                                        {
                                            if(cr8v.var_check(oEsopData.offer_expiration) && oEsopData.offer_expiration != '0000-00-00')
                                            {                                            
                                                window.location =  roxas.config('url.server.base') + "auth/forbidden_403";
                                            }
                                        }

                                        if(cr8v.var_check(bFromAdd) && bFromAdd && count(oEsopData.distribution) > 0)
                                        {
                                            window.location =  roxas.config('url.server.base') + "auth/forbidden_403";
                                        }

                                        if(cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0 && !cr8v.var_check(bFromEdit) && !bFromEdit && !cr8v.var_check(bFromEsopBatch) && !bFromEsopBatch)
                                        {
                                            var iEsopTotalAccepted = 0;
                                            var iEsopTotalAvailed = 0;

                                            for(var i in oEsopData.distribution)
                                            {
                                                var iCompanyTotalAccepted = 0;
                                                var iCompanyTotalAvailed = 0;
    
                                                /*populate companies first*/
                                                var oCompanyData = oEsopData.distribution[i];
                                                var uiCompanyTemplate = uiEsopCompaniesContainer.find('.esop_companies.template.hidden').clone().removeClass('template hidden');
                                                var uiManipulatedCompanyTemplate = roxas.esop.manipulate_esop_companies(oCompanyData, uiCompanyTemplate, undefined, bFromViewEsop);
                                                uiEsopCompaniesContainer.append(uiManipulatedCompanyTemplate);
                                                
                                                for(var k in oCompanyData.employees)
                                                {
                                                    /*then populate the employees in that company*/
                                                    var oUserData = oCompanyData.employees[k];
                                                    var uiEmployeeTemplate = uiManipulatedCompanyTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                                                    var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                                                    uiManipulatedCompanyTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                                    uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopData.id);

                                                    if(oUserData.status == 2)
                                                    {
                                                        iEsopTotalAccepted++;
                                                        iCompanyTotalAccepted++;
                                                        iEsopTotalAvailed = parseFloat(iEsopTotalAvailed) + parseFloat(oUserData.accepted);
                                                        iCompanyTotalAvailed = parseFloat(iCompanyTotalAvailed) + parseFloat(oUserData.accepted);
                                                        var iTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopData.price_per_share)), 2);
                                                        uiManipulatedEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iTotalAmountAvailed);
                                                    }
                                                }

                                                var iCompanyTotalAlloted = parseFloat(oCompanyData.company_allotment) - parseFloat(iCompanyTotalAvailed);

                                                uiManipulatedCompanyTemplate.find('[data-label="company_total_employees_accepted"]').text(iCompanyTotalAccepted);
                                                uiManipulatedCompanyTemplate.find('[data-label="company_total_shares_availed"]').text(number_format(iCompanyTotalAvailed, 2));
                                                uiManipulatedCompanyTemplate.find('[data-label="company_total_shares_unavailed"]').text(number_format(iCompanyTotalAlloted, 2));
                                            }
                                            
                                            var iEsopTotalUnavailed = parseFloat(oEsopData.total_share_qty) - parseFloat(iEsopTotalAvailed);

                                            $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_employees_accepted"]').text(iEsopTotalAccepted);
                                            $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_shares_availed"]').text(number_format(iEsopTotalAvailed, 2));
                                            $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_shares_unavailed"]').text(number_format(iEsopTotalUnavailed, 2));
                                        }

                                        /*if from view esop batch page*/
                                        if(cr8v.var_check(bFromEsopBatch) && bFromEsopBatch)
                                        {
                                            roxas.esop.manipulate_esop_batch_data(oEsopData, bFromEsopBatch);
                                        }
                                        else
                                        {
                                            arrLogIDS.push(oEsopData.id);

                                            var oGetLogsParams = {
                                                "where" : [
                                                    {
                                                        "field" : "ref_id",
                                                        "operator" : "IN",
                                                        "value" : "("+arrLogIDS.join()+")"
                                                    },
                                                    {
                                                        "field" : "type",
                                                        "operator" : "IN",
                                                        "value" : "('esop', 'esop/user')"
                                                    }
                                                ]
                                            };
                                            cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
                                        }
                                    }
                                }
                                else if(cr8v.var_check(bFromDashboard) && bFromDashboard)
                                {
                                    if(oData.data.length > 0)
                                    {
                                        $('[data-container="with_esop_dashboard"]').removeClass('hidden');
                                        $('[data-container="without_esop_dashboard"]').addClass('hidden');
                                        if(iUserRole == 2)
                                        {
                                            roxas.esop.manipulate_hr_department_dashboard(oData.data);
                                        }
                                        else if(iUserRole == 3 || iUserRole == 4)
                                        {
                                            roxas.esop.manipulate_hr_head_and_admin_dashboard(oData.data);
                                        }
                                    }
                                    else
                                    {
                                        $('[data-container="with_esop_dashboard"]').addClass('hidden');
                                        $('[data-container="without_esop_dashboard"]').removeClass('hidden');
                                    }
                                }
                                else
                                {                                
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i < max; i++)
                                        {
                                            var oEsopData = oData.data[i];

                                            var uiTemplateList = $('.esop_list.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateList = roxas.esop.manipulate_template_list(oEsopData, uiTemplateList);

                                            var uiTemplateGrid = $('.esop_grid.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateGrid = roxas.esop.manipulate_template_grid(oEsopData, uiTemplateGrid);

                                            uiEsopListContainer.prepend(uiManipulatedTemplateList);
                                            uiEsopGridContainer.prepend(uiManipulatedTemplateGrid);
                                        }
                                    }
                                    else
                                    {
                                        uiNoResultsFound.removeClass('template hidden');
                                    }
                                }

                            }
                            else
                            {
                                uiNoResultsFound.removeClass('template hidden');
                                $('[data-container="without_esop_dashboard"]').removeClass('hidden');
                                $('[data-container="with_esop_dashboard"]').addClass('hidden');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }

                        $(".modal-close.close-me").trigger("click");
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },
        /**
         * manipulate_hr_head_and_admin_dashboard
         * @description This function will manipulate the data for the hr head and esop admin dashboard
         * @dependencies N/A
         * @param {object} oData
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_hr_head_and_admin_dashboard : function(oData){
            // console.log(oData);
            if(cr8v.var_check(oData))
            {
                var iTotalMainAvailed = 0;
                var iTotalMainUnavailed = 0;
                var iTotalMainAlloted = 0;
                var arrEsopParentIds = [];

                for(var i in oData)
                {
                    var oEsopData = oData[i];

                    /*grid tab*/
                    var uiEsopGrid = $('.hr_head_and_admin_esop_grid.template.hidden').clone().removeClass('template hidden');
                    var uiManipulatedEsopGridTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiEsopGrid);
                    var uiEsopGridInput = $('.hr_head_and_admin_esop_grid_tab_input.template.hidden').clone().removeClass('template hidden');
                    var uiEsopGridLabel = $('.hr_head_and_admin_esop_grid_tab_label.template.hidden').clone().removeClass('template hidden');
                    var iTabNumber = cr8v.generate_random_keys(8);
                    
                    uiEsopGrid.attr('data-attr', iTabNumber);
                    uiEsopGridInput.attr('data-attr', iTabNumber);
                    uiEsopGridLabel.attr('data-attr', iTabNumber);
                    uiManipulatedEsopGridTemplate.find('.tab').attr('data-attr', iTabNumber);
                    uiManipulatedEsopGridTemplate.data('full_data', oEsopData);
                    
                    arrEsopParentIds.push(oEsopData.id);

                    uiEsopGridLabel.text(oEsopData.name);
                    uiManipulatedEsopGridTemplate.find('[data-label="hr_head_and_admin_esop_name"]').text(oEsopData.name);

                    uiEsopAdminGridTabContainer.after(uiManipulatedEsopGridTemplate);
                    uiEsopAdminGridTabContainer.append(uiEsopGridInput);
                    uiEsopAdminGridTabContainer.append(uiEsopGridLabel);
                    uiEsopGridLabel.trigger('click');

                    /*list tab*/
                    var uiEsopList = $('.hr_head_and_admin_esop_list.template.hidden').clone().removeClass('template hidden');
                    var uiManipulatedEsopListTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiEsopList);
                    var uiEsopListInput = $('.hr_head_and_admin_esop_list_tab_input.template.hidden').clone().removeClass('template hidden');
                    var uiEsopListLabel = $('.hr_head_and_admin_esop_list_tab_label.template.hidden').clone().removeClass('template hidden');
                    var iTabNumber = cr8v.generate_random_keys(8);
                    
                    uiEsopList.attr('data-attr', iTabNumber);
                    uiEsopListInput.attr('data-attr', iTabNumber);
                    uiEsopListLabel.attr('data-attr', iTabNumber);
                    uiManipulatedEsopListTemplate.find('.tab').attr('data-attr', iTabNumber);
                    uiManipulatedEsopListTemplate.data('full_data', oEsopData);

                    uiEsopListLabel.text(oEsopData.name);
                    uiManipulatedEsopListTemplate.find('[data-label="hr_head_and_admin_esop_name"]').text(oEsopData.name);

                    uiEsopAdminListTabContainer.after(uiManipulatedEsopListTemplate);
                    uiEsopAdminListTabContainer.append(uiEsopListInput);
                    uiEsopAdminListTabContainer.append(uiEsopListLabel);
                    uiEsopListLabel.trigger('click');

                    var iEsopTotalAvailed = 0;

                    if(cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0)
                    {
                        for(var x in oEsopData.distribution)
                        {
                            var oCompanyData = oEsopData.distribution[x];
                            var uiCompanyGridTemplate = uiManipulatedEsopGridTemplate.find('.esop_companies.template.hidden').clone().removeClass('template hidden');
                            var uiManipulatedCompanyGridTemplate = roxas.esop.manipulate_esop_companies(oCompanyData, uiCompanyGridTemplate, undefined, true);
                            uiManipulatedEsopGridTemplate.find('[data-container="esop_companies_container"]').append(uiManipulatedCompanyGridTemplate);

                            var oDropdownData = {
                                'name' : oCompanyData.company_name,
                                'id' : oCompanyData.company_id,
                            };
                            cr8v.append_dropdown(oDropdownData, uiManipulatedEsopListTemplate.find('.company_filter'), false, 'companies', 'data-company-id', oCompanyData.company_id);
                            cr8v.append_dropdown(oDropdownData, uiManipulatedEsopGridTemplate.find('.company_filter'), false, 'companies', 'data-company-id', oCompanyData.company_id);

                            uiManipulatedEsopGridTemplate.find('.company_filter').find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
                            uiManipulatedEsopListTemplate.find('.company_filter').find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);

                            uiManipulatedEsopGridTemplate.find('.company_filter').off('click').on('click', '.option', function(){
                                var uiThis = $(this);
                                if(uiThis.attr('data-value') == 'All')
                                {
                                    uiThis.parents('.tab:first').find('.esop_companies:not(.template)').removeClass('hidden');
                                }
                                else
                                {
                                    uiThis.parents('.tab:first').find('.esop_companies:not(.template)').addClass('hidden');
                                    uiThis.parents('.tab:first').find('.esop_companies:not(.template)[data-company-id="'+uiThis.attr('data-value')+'"]').removeClass('hidden');
                                }
                            });

                            uiManipulatedEsopListTemplate.find('.company_filter').off('click').on('click', '.option', function(){
                                var uiThis = $(this);
                                if(uiThis.attr('data-value') == 'All')
                                {
                                    uiThis.parents('.tab:first').find('.esop_employees:not(.template)').removeClass('hidden');
                                    uiThis.parents('.tab:first').find('.esop_special:not(.template)').removeClass('hidden');
                                    uiThis.parents('.tab:first').find('[data-container="esop_special_container"]').find('tr.no_results_found').remove();
                                }
                                else
                                {
                                    uiThis.parents('.tab:first').find('.esop_employees:not(.template)').addClass('hidden');
                                    uiThis.parents('.tab:first').find('.esop_employees:not(.template)[data-user-company-id="'+uiThis.attr('data-value')+'"]').removeClass('hidden');

                                    if(uiThis.parents('.tab:first').find('.esop_special:not(.template)').length > 0)
                                    {
                                        uiThis.parents('.tab:first').find('.esop_special:not(.template)').addClass('hidden');
                                        uiThis.parents('.tab:first').find('.esop_special:not(.template)[data-user-company-id="'+uiThis.attr('data-value')+'"]').removeClass('hidden');

                                        if(uiThis.parents('.tab:first').find('.esop_special:not(.template):not(.hidden)').length == 0)
                                        {
                                            uiThis.parents('.tab:first').find('[data-container="esop_special_container"]').append('<tr class="no_results_found"><td colspan="7">No Results found</td></tr>');
                                        }
                                        else
                                        {
                                            uiThis.parents('.tab:first').find('[data-container="esop_special_container"]').find('tr.no_results_found').remove();
                                        }
                                    }
                                }
                            });

                            var iCompanyTotalAvailed = 0;

                            if(cr8v.var_check(oCompanyData.employees) && count(oCompanyData.employees) > 0)
                            {
                                for(var k in oCompanyData.employees)
                                {
                                    /*then populate the employees in that company*/
                                    var oUserData = oCompanyData.employees[k];
                                    var uiEmployeeListTemplate = uiManipulatedEsopListTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                                    var uiManipulatedEmployeeListTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeListTemplate);
                                    uiManipulatedEsopListTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeListTemplate);
                                    uiManipulatedEmployeeListTemplate.attr('data-esop-id', oEsopData.id);

                                    if(oUserData.status == 2)
                                    {
                                        iTotalMainAvailed = parseFloat(iTotalMainAvailed) + parseFloat(oUserData.accepted);
                                        iEsopTotalAvailed = parseFloat(iEsopTotalAvailed) + parseFloat(oUserData.accepted);
                                        iCompanyTotalAvailed = parseFloat(iCompanyTotalAvailed) + parseFloat(oUserData.accepted);
                                        var iEmployeeTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopData.price_per_share)), 2);
                                        uiManipulatedEmployeeListTemplate.find('[data-label="employee_total_amount_availed"]').text(iEmployeeTotalAmountAvailed);
                                    }
                                }

                                var iCompanyTotalUnavailed = parseFloat(oCompanyData.company_allotment) - parseFloat(iCompanyTotalAvailed);

                                uiManipulatedCompanyGridTemplate.find('[data-label="company_total_shares_availed"]').text(number_format(iCompanyTotalAvailed, 2));
                                uiManipulatedCompanyGridTemplate.find('[data-label="company_total_shares_unavailed"]').text(number_format(iCompanyTotalUnavailed, 2));

                                roxas.esop.render_pie_chart(oCompanyData.company_allotment, iCompanyTotalAvailed, uiManipulatedCompanyGridTemplate.find('.chart-container'));
                            }
                        }
                    }

                    iTotalMainAlloted = parseFloat(iTotalMainAlloted) + parseFloat(oEsopData.total_share_qty);

                    var iEsopTotalUnavailed = parseFloat(oEsopData.total_share_qty) - parseFloat(iEsopTotalAvailed);

                    uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_availed"]').text(number_format(iEsopTotalAvailed, 2));
                    uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_unavailed"]').text(number_format(iEsopTotalUnavailed, 2));

                    uiManipulatedEsopListTemplate.find('[data-label="esop_total_shares_availed"]').text(number_format(iEsopTotalAvailed, 2));
                    uiManipulatedEsopListTemplate.find('[data-label="esop_total_shares_unavailed"]').text(number_format(iEsopTotalUnavailed, 2));
                    
                    roxas.esop.render_pie_chart(oEsopData.total_share_qty, iEsopTotalAvailed, uiManipulatedEsopGridTemplate.find('[data-container="grand_total_container"]').find('.chart-container'));

                    if(cr8v.var_check(oEsopData.esop_batches) && count(oEsopData.esop_batches) > 0)
                    {
                        for(var b in oEsopData.esop_batches)
                        {
                            var oEsopBatchData = oEsopData.esop_batches[b];

                            /*grid tab*/
                            var uiEsopGrid = $('.hr_head_and_admin_esop_grid.template.hidden').clone().removeClass('template hidden');
                            var uiManipulatedEsopGridTemplate = roxas.esop.manipulate_esop_data(oEsopBatchData, uiEsopGrid);
                            var uiEsopGridInput = $('.hr_head_and_admin_esop_grid_tab_input.template.hidden').clone().removeClass('template hidden');
                            var uiEsopGridLabel = $('.hr_head_and_admin_esop_grid_tab_label.template.hidden').clone().removeClass('template hidden');
                            var iTabNumber = cr8v.generate_random_keys(8);
                            
                            uiEsopGrid.attr('data-attr', iTabNumber);
                            uiEsopGridInput.attr('data-attr', iTabNumber);
                            uiEsopGridLabel.attr('data-attr', iTabNumber);
                            uiManipulatedEsopGridTemplate.find('.tab').attr('data-attr', iTabNumber);
                            uiManipulatedEsopGridTemplate.data('full_data', oEsopBatchData);

                            arrEsopParentIds.push(oEsopBatchData.id);

                            uiEsopGridLabel.text(oEsopBatchData.name);
                            uiManipulatedEsopGridTemplate.find('[data-label="hr_head_and_admin_esop_name"]').text(oEsopBatchData.name);

                            uiEsopAdminGridTabContainer.after(uiManipulatedEsopGridTemplate);
                            uiEsopAdminGridTabContainer.append(uiEsopGridInput);
                            uiEsopAdminGridTabContainer.append(uiEsopGridLabel);
                            uiEsopGridLabel.trigger('click');

                            /*list tab*/
                            var uiEsopList = $('.hr_head_and_admin_esop_list.template.hidden').clone().removeClass('template hidden');
                            var uiManipulatedEsopListTemplate = roxas.esop.manipulate_esop_data(oEsopBatchData, uiEsopList);
                            var uiEsopListInput = $('.hr_head_and_admin_esop_list_tab_input.template.hidden').clone().removeClass('template hidden');
                            var uiEsopListLabel = $('.hr_head_and_admin_esop_list_tab_label.template.hidden').clone().removeClass('template hidden');
                            var iTabNumber = cr8v.generate_random_keys(8);
                            
                            uiEsopList.attr('data-attr', iTabNumber);
                            uiEsopListInput.attr('data-attr', iTabNumber);
                            uiEsopListLabel.attr('data-attr', iTabNumber);
                            uiManipulatedEsopListTemplate.find('.tab').attr('data-attr', iTabNumber);
                            uiManipulatedEsopListTemplate.data('full_data', oEsopBatchData);

                            uiEsopListLabel.text(oEsopBatchData.name);
                            uiManipulatedEsopListTemplate.find('[data-label="hr_head_and_admin_esop_name"]').text(oEsopBatchData.name);

                            uiEsopAdminListTabContainer.after(uiManipulatedEsopListTemplate);
                            uiEsopAdminListTabContainer.append(uiEsopListInput);
                            uiEsopAdminListTabContainer.append(uiEsopListLabel);
                            uiEsopListLabel.trigger('click');

                            var iEsopTotalAvailed = 0;

                            if(cr8v.var_check(oEsopBatchData.distribution) && count(oEsopBatchData.distribution) > 0)
                            {
                                for(var d in oEsopBatchData.distribution)
                                {
                                    var oCompanyData = oEsopBatchData.distribution[d];
                                    var uiCompanyGridTemplate = uiManipulatedEsopGridTemplate.find('.esop_companies.template.hidden').clone().removeClass('template hidden');
                                    var uiManipulatedCompanyGridTemplate = roxas.esop.manipulate_esop_companies(oCompanyData, uiCompanyGridTemplate, undefined, true);
                                    uiManipulatedEsopGridTemplate.find('[data-container="esop_companies_container"]').append(uiManipulatedCompanyGridTemplate);

                                    var oDropdownData = {
                                        'name' : oCompanyData.company_name,
                                        'id' : oCompanyData.company_id,
                                    };
                                    cr8v.append_dropdown(oDropdownData, uiManipulatedEsopListTemplate.find('.company_filter'), false, 'companies', 'data-company-id', oCompanyData.company_id);
                                    cr8v.append_dropdown(oDropdownData, uiManipulatedEsopGridTemplate.find('.company_filter'), false, 'companies', 'data-company-id', oCompanyData.company_id);

                                    uiManipulatedEsopGridTemplate.find('.company_filter').find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
                                    uiManipulatedEsopListTemplate.find('.company_filter').find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
                                    
                                    uiManipulatedEsopGridTemplate.find('.company_filter').off('click').on('click', '.option', function(){
                                        var uiThis = $(this);
                                        if(uiThis.attr('data-value') == 'All')
                                        {
                                            uiThis.parents('.tab:first').find('.esop_companies:not(.template)').removeClass('hidden');
                                        }
                                        else
                                        {
                                            uiThis.parents('.tab:first').find('.esop_companies:not(.template)').addClass('hidden');
                                            uiThis.parents('.tab:first').find('.esop_companies:not(.template)[data-company-id="'+uiThis.attr('data-value')+'"]').removeClass('hidden');
                                        }
                                    });

                                    uiManipulatedEsopListTemplate.find('.company_filter').off('click').on('click', '.option', function(){
                                        var uiThis = $(this);
                                        if(uiThis.attr('data-value') == 'All')
                                        {
                                            uiThis.parents('.tab:first').find('.esop_employees:not(.template)').removeClass('hidden');
                                            uiThis.parents('.tab:first').find('.esop_special:not(.template)').removeClass('hidden');
                                            uiThis.parents('.tab:first').find('[data-container="esop_special_container"]').find('tr.no_results_found').remove();
                                        }
                                        else
                                        {
                                            uiThis.parents('.tab:first').find('.esop_employees:not(.template)').addClass('hidden');
                                            uiThis.parents('.tab:first').find('.esop_employees:not(.template)[data-user-company-id="'+uiThis.attr('data-value')+'"]').removeClass('hidden');

                                            if(uiThis.parents('.tab:first').find('.esop_special:not(.template)').length > 0)
                                            {
                                                uiThis.parents('.tab:first').find('.esop_special:not(.template)').addClass('hidden');
                                                uiThis.parents('.tab:first').find('.esop_special:not(.template)[data-user-company-id="'+uiThis.attr('data-value')+'"]').removeClass('hidden');

                                                if(uiThis.parents('.tab:first').find('.esop_special:not(.template):not(.hidden)').length == 0)
                                                {
                                                    uiThis.parents('.tab:first').find('[data-container="esop_special_container"]').append('<tr class="no_results_found"><td colspan="7">No Results found</td></tr>');
                                                }
                                                else
                                                {
                                                    uiThis.parents('.tab:first').find('[data-container="esop_special_container"]').find('tr.no_results_found').remove();
                                                }
                                            }
                                        }
                                    });

                                    var iCompanyTotalAvailed = 0;

                                    if(cr8v.var_check(oCompanyData.employees) && count(oCompanyData.employees) > 0)
                                    {
                                        for(var k in oCompanyData.employees)
                                        {
                                            /*then populate the employees in that company*/
                                            var oUserData = oCompanyData.employees[k];
                                            var uiEmployeeListTemplate = uiManipulatedEsopListTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                                            var uiManipulatedEmployeeListTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeListTemplate);
                                            uiManipulatedEsopListTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeListTemplate);
                                            uiManipulatedEmployeeListTemplate.attr('data-esop-id', oEsopBatchData.id);

                                            if(oUserData.status == 2)
                                            {
                                                iTotalMainAvailed = parseFloat(iTotalMainAvailed) + parseFloat(oUserData.accepted);
                                                iEsopTotalAvailed = parseFloat(iEsopTotalAvailed) + parseFloat(oUserData.accepted);
                                                iCompanyTotalAvailed = parseFloat(iCompanyTotalAvailed) + parseFloat(oUserData.accepted);
                                                var iEmployeeTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopBatchData.price_per_share)), 2);
                                                uiManipulatedEmployeeListTemplate.find('[data-label="employee_total_amount_availed"]').text(iEmployeeTotalAmountAvailed);
                                            }
                                        }

                                        var iCompanyTotalUnavailed = parseFloat(oCompanyData.company_allotment) - parseFloat(iCompanyTotalAvailed);

                                        uiManipulatedCompanyGridTemplate.find('[data-label="company_total_shares_availed"]').text(number_format(iCompanyTotalAvailed, 2));
                                        uiManipulatedCompanyGridTemplate.find('[data-label="company_total_shares_unavailed"]').text(number_format(iCompanyTotalUnavailed, 2));

                                        roxas.esop.render_pie_chart(oCompanyData.company_allotment, iCompanyTotalAvailed, uiManipulatedCompanyGridTemplate.find('.chart-container'));
                                    }
                                }
                            }

                            iTotalMainAlloted = parseFloat(iTotalMainAlloted) + parseFloat(oEsopBatchData.total_share_qty);

                            var iEsopTotalUnavailed = parseFloat(oEsopBatchData.total_share_qty) - parseFloat(iEsopTotalAvailed);

                            uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_availed"]').text(number_format(iEsopTotalAvailed, 2));
                            uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_unavailed"]').text(number_format(iEsopTotalUnavailed, 2));

                            uiManipulatedEsopListTemplate.find('[data-label="esop_total_shares_availed"]').text(number_format(iEsopTotalAvailed, 2));
                            uiManipulatedEsopListTemplate.find('[data-label="esop_total_shares_unavailed"]').text(number_format(iEsopTotalUnavailed, 2));
                            
                            roxas.esop.render_pie_chart(oEsopBatchData.total_share_qty, iEsopTotalAvailed, uiManipulatedEsopGridTemplate.find('[data-container="grand_total_container"]').find('.chart-container'));                            
                        }
                    }
                }

                var arrFilteredParentIds = cr8v.check_duplicate(arrEsopParentIds);

                var oGetSpecialParams = {
                    "parent_ids" : arrFilteredParentIds.data.join()
                };

                roxas.esop.get_special(oGetSpecialParams, undefined, function(oData){
                    if(oData.status == true)
                    {
                        for(var i in oData.data)
                        {
                            var oEsopData = oData.data[i];
                            var uiManipulatedEsopGridTemplate = $('.hr_head_and_admin_esop_grid[data-esop-id="'+oEsopData.parent_id+'"]');
                            var uiManipulatedEsopListTemplate = $('.hr_head_and_admin_esop_list[data-esop-id="'+oEsopData.parent_id+'"]');

                            for(var x in oEsopData.stock_offers)
                            {
                                var oUserData = oEsopData.stock_offers[x];
                                var uiCompanyTemplate = uiManipulatedEsopGridTemplate.find('.esop_companies[data-company-id="'+oUserData.company_id+'"]');
                                var uiEmployeeTemplate = uiManipulatedEsopListTemplate.find('.esop_employees[data-user-id="'+oUserData.user_id+'"]');

                                if(uiEmployeeTemplate.length == 0)
                                {
                                    var uiEmployeeTemplate = uiManipulatedEsopListTemplate.find('.esop_employees.template').clone().removeClass('template hidden');
                                    var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);

                                    uiManipulatedEsopListTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                    uiManipulatedEmployeeTemplate.find('[data-label="company_code"]').prepend('<i class="fa fa-exclamation-triangle default-cursor" title="This employee has been added with different details. Please check additionals for more info."></i> ');
                                    uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopData.id);
                                }
                                else
                                {
                                    roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                                    uiEmployeeTemplate.find('[data-label="company_code"]').prepend('<i class="fa fa-exclamation-triangle default-cursor" title="This employee has been added with different details. Please check additionals for more info."></i> ');
                                    uiEmployeeTemplate.attr('data-esop-id', oEsopData.id);
                                }
                                
                                if(cr8v.var_check(oUserData.accepted))
                                {
                                    var iTotalEsopAlloted = uiManipulatedEsopGridTemplate.find('[data-label="total_share_qty"]:first').text();
                                    var iTotalEsopAvailed = uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_availed"]:first').text();
                                    var iTotalEsopUnavailed = uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_unavailed"]:first').text();

                                    var newTotalEsopAvailed = number_format((parseFloat(cr8v.remove_commas(iTotalEsopAvailed)) + parseFloat(oUserData.accepted)), 2);
                                    var newTotalEsopUnavailed = number_format((parseFloat(cr8v.remove_commas(iTotalEsopUnavailed)) - parseFloat(oUserData.accepted)), 2);
                                    uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_availed"]:first').text(newTotalEsopAvailed);
                                    uiManipulatedEsopGridTemplate.find('[data-label="esop_total_shares_unavailed"]').text(newTotalEsopUnavailed);
                                    uiManipulatedEsopListTemplate.find('[data-label="esop_total_shares_availed"]:first').text(newTotalEsopAvailed);
                                    uiManipulatedEsopListTemplate.find('[data-label="esop_total_shares_unavailed"]').text(newTotalEsopUnavailed);

                                    roxas.esop.render_pie_chart(cr8v.remove_commas(number_format(iTotalEsopAlloted, 2)), cr8v.remove_commas(number_format(newTotalEsopAvailed, 2)), uiManipulatedEsopGridTemplate.find('[data-container="grand_total_container"]').find('.chart-container'));

                                    var iTotalCompanyAlloted = uiCompanyTemplate.find('[data-label="company_alloted_shares"]').text();
                                    var iTotalCompanyAvailed = uiCompanyTemplate.find('[data-label="company_total_shares_availed"]').text();
                                    var iTotalCompanyUnavailed = uiCompanyTemplate.find('[data-label="company_total_shares_unavailed"]').text();

                                    var newTotalCompanyAvailed = number_format((parseFloat(cr8v.remove_commas(iTotalCompanyAvailed)) + parseFloat(oUserData.accepted)), 2);
                                    var newTotalCompanyUnavailed = number_format((parseFloat(cr8v.remove_commas(iTotalCompanyUnavailed)) - parseFloat(oUserData.accepted)), 2);
                                    uiCompanyTemplate.find('[data-label="company_total_shares_availed"]').text(newTotalCompanyAvailed);
                                    uiCompanyTemplate.find('[data-label="company_total_shares_unavailed"]').text(newTotalCompanyUnavailed);

                                    roxas.esop.render_pie_chart(cr8v.remove_commas(number_format(iTotalCompanyAlloted, 2)), cr8v.remove_commas(number_format(newTotalCompanyAvailed, 2)), uiCompanyTemplate.find('.chart-container'));

                                    var iEmployeeTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopData.price_per_share)), 2);
                                    uiEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iEmployeeTotalAmountAvailed);
                                    uiManipulatedEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iEmployeeTotalAmountAvailed);

                                    iTotalMainAvailed = parseFloat(iTotalMainAvailed) + parseFloat(oUserData.accepted);
                                }

                                uiManipulatedEsopListTemplate.find('[data-container="additionals_container"]').removeClass('hidden');
                                var uiSpecialTemplate = uiManipulatedEsopListTemplate.find('[data-container="additionals_container"]').find('.esop_special.template').clone().removeClass('hidden template');
                                var uiManipulatedSpecialTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiSpecialTemplate);

                                var sCurrencyName = cr8v.var_check(oEsopData.currency_name) ? oEsopData.currency_name : roxas.esop.get_currency_name(oEsopData.currency) ;
                                var sPricePerShare = sCurrencyName +' '+ oEsopData.price_per_share;

                                uiManipulatedSpecialTemplate.find('[data-label="grant_date"]').text(moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY'));
                                uiManipulatedSpecialTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                                uiManipulatedSpecialTemplate.find('[data-label="vesting_years"]').text(oEsopData.vesting_years);
                                uiManipulatedSpecialTemplate.attr('data-esop-id', oEsopData.id);
                                uiManipulatedSpecialTemplate.attr('data-esop-parent-id', oEsopData.parent_id);

                                uiManipulatedEsopListTemplate.find('[data-container="esop_special_container"]').append(uiSpecialTemplate);
                            }
                        }
                    }

                    var iTotalMainUnavailed = parseFloat(iTotalMainAlloted) - parseFloat(iTotalMainAvailed);
                    $('[data-container="esop_total_shares_container"]').find('[data-label="main_total_alloted_shares"]').text(number_format(iTotalMainAlloted, 2));
                    $('[data-container="esop_total_shares_container"]').find('[data-label="main_total_shares_availed"]').text(number_format(iTotalMainAvailed, 2));
                    $('[data-container="esop_total_shares_container"]').find('[data-label="main_total_shares_unavailed"]').text(number_format(iTotalMainUnavailed, 2));

                    roxas.esop.render_pie_chart(iTotalMainAlloted, iTotalMainAvailed, $('[data-container="esop_total_shares_container"]').find('.chart-container'));
                    initialize_pie_chart();
                }, true);
            }
        },

        /**
         * manipulate_hr_department_dashboard
         * @description This function will manipulate the data for the hr department dashboard
         * @dependencies N/A
         * @param {object} oData
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_hr_department_dashboard : function(oData){
            if(cr8v.var_check(oData))
            {
                var iTotalMainAvailed = 0;
                var iTotalMainUnavailed = 0;
                var iTotalMainAlloted = 0;
                var arrEsopParentIds = [];

                for(var i = 0, max = oData.length; i < max; i++)
                {
                    var oEsopData = oData[i];

                    /*main esop batch tab*/
                    var uiEsop = $('.hr_department_esop.template.hidden').clone().removeClass('template hidden');
                    var uiManipulatedEsopTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiEsop);
                    var uiEsopInput = $('.hr_department_esop_tab_input.template.hidden').clone().removeClass('template hidden');
                    var uiEsopLabel = $('.hr_department_esop_tab_label.template.hidden').clone().removeClass('template hidden');
                    var iTabNumber = cr8v.generate_random_keys(8)/*Math.floor((Math.random() * 10000000000000000000) + 1)*/;
                    
                    uiEsop.attr('data-attr', iTabNumber);
                    uiEsopInput.attr('data-attr', iTabNumber);
                    uiEsopLabel.attr('data-attr', iTabNumber);
                    uiManipulatedEsopTemplate.find('.tab').attr('data-attr', iTabNumber);
                    uiManipulatedEsopTemplate.data('full_data', oEsopData);

                    uiEsopLabel.text(oEsopData.name);
                    uiManipulatedEsopTemplate.find('[data-label="hr_department_esop_name"]').text(oEsopData.name);

                    var iEsopTotalAvailed = 0;

                    if(cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0)
                    {
                        for(var x in oEsopData.distribution)
                        {
                            var oCompanyData = oEsopData.distribution[x];

                            if(oCompanyData.company_id == iCompanyID)
                            {
                                arrEsopParentIds.push(oEsopData.id);
                                
                                uiManipulatedEsopTemplate.find('[data-label="company_name"]').html(' (Alloted to '+oCompanyData.company_name+')');
                                uiManipulatedEsopTemplate.find('[data-label="company_alloted_shares"]').text(number_format(oCompanyData.company_allotment, 2));

                                iTotalMainAlloted = parseFloat(iTotalMainAlloted) + parseFloat(oCompanyData.company_allotment);

                                var iCompanyTotalAvailed = 0;

                                if(cr8v.var_check(oCompanyData.employees) && count(oCompanyData.employees) > 0)
                                {
                                    for(var k in oCompanyData.employees)
                                    {
                                        /*then populate the employees in that company*/
                                        var oUserData = oCompanyData.employees[k];
                                        var uiEmployeeTemplate = uiManipulatedEsopTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                                        var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                                        uiManipulatedEsopTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                        uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopData.id);

                                        if(oUserData.status == 2)
                                        {
                                            iTotalMainAvailed = parseFloat(iTotalMainAvailed) + parseFloat(oUserData.accepted);
                                            iEsopTotalAvailed = parseFloat(iEsopTotalAvailed) + parseFloat(oUserData.accepted);
                                            iCompanyTotalAvailed = parseFloat(iCompanyTotalAvailed) + parseFloat(oUserData.accepted);
                                            var iEmployeeTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopData.price_per_share)), 2);
                                            uiManipulatedEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iEmployeeTotalAmountAvailed);
                                        }
                                    }

                                    var iCompanyTotalUnavailed = parseFloat(oCompanyData.company_allotment) - parseFloat(iCompanyTotalAvailed);

                                    uiManipulatedEsopTemplate.find('[data-label="company_total_shares_availed"]').text(number_format(iCompanyTotalAvailed, 2));
                                    uiManipulatedEsopTemplate.find('[data-label="company_total_shares_unavailed"]').text(number_format(iCompanyTotalUnavailed, 2));
                                }

                                uiHRDeptEsopTabContainer.after(uiManipulatedEsopTemplate);
                                uiHRDeptEsopTabContainer.append(uiEsopInput);
                                uiHRDeptEsopTabContainer.append(uiEsopLabel);
                                uiEsopLabel.trigger('click');
                            }
                        }
                    }

                    if(cr8v.var_check(oEsopData.esop_batches) && count(oEsopData.esop_batches) > 0)
                    {
                        for(var b in oEsopData.esop_batches)
                        {
                            var oEsopBatchData = oEsopData.esop_batches[b];

                            var uiEsop = $('.hr_department_esop.template.hidden').clone().removeClass('template hidden');
                            var uiManipulatedEsopTemplate = roxas.esop.manipulate_esop_data(oEsopBatchData, uiEsop);
                            var uiEsopInput = $('.hr_department_esop_tab_input.template.hidden').clone().removeClass('template hidden');
                            var uiEsopLabel = $('.hr_department_esop_tab_label.template.hidden').clone().removeClass('template hidden');
                            var iTabNumber = cr8v.generate_random_keys(8)/*Math.floor((Math.random() * 10000000000000000000) + 1)*/;
                            
                            uiEsop.attr('data-attr', iTabNumber);
                            uiEsopInput.attr('data-attr', iTabNumber);
                            uiEsopLabel.attr('data-attr', iTabNumber);
                            uiManipulatedEsopTemplate.find('.tab').attr('data-attr', iTabNumber);
                            uiManipulatedEsopTemplate.data('full_data', oEsopBatchData);

                            uiEsopLabel.text(oEsopBatchData.name);
                            uiManipulatedEsopTemplate.find('[data-label="hr_department_esop_name"]').text(oEsopBatchData.name);

                            var iEsopTotalAvailed = 0;

                            if(cr8v.var_check(oEsopBatchData.distribution) && count(oEsopBatchData.distribution) > 0)
                            {
                                for(var x in oEsopBatchData.distribution)
                                {
                                    var oCompanyData = oEsopBatchData.distribution[x];

                                    if(oCompanyData.company_id == iCompanyID)
                                    {
                                        arrEsopParentIds.push(oEsopBatchData.id);
                                        
                                        uiManipulatedEsopTemplate.find('[data-label="company_name"]').html(' (Alloted to '+oCompanyData.company_name+')');
                                        uiManipulatedEsopTemplate.find('[data-label="company_alloted_shares"]').text(number_format(oCompanyData.company_allotment, 2));

                                        iTotalMainAlloted = parseFloat(iTotalMainAlloted) + parseFloat(oCompanyData.company_allotment);

                                        var iCompanyTotalAvailed = 0;

                                        if(cr8v.var_check(oCompanyData.employees) && count(oCompanyData.employees) > 0)
                                        {
                                            for(var k in oCompanyData.employees)
                                            {
                                                /*then populate the employees in that company*/
                                                var oUserData = oCompanyData.employees[k];
                                                var uiEmployeeTemplate = uiManipulatedEsopTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                                                var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                                                uiManipulatedEsopTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                                uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopBatchData.id);

                                                if(oUserData.status == 2)
                                                {
                                                    iTotalMainAvailed = parseFloat(iTotalMainAvailed) + parseFloat(oUserData.accepted);
                                                    iEsopTotalAvailed = parseFloat(iEsopTotalAvailed) + parseFloat(oUserData.accepted);
                                                    iCompanyTotalAvailed = parseFloat(iCompanyTotalAvailed) + parseFloat(oUserData.accepted);
                                                    var iEmployeeTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopBatchData.price_per_share)), 2);
                                                    uiManipulatedEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iEmployeeTotalAmountAvailed);
                                                }
                                            }

                                            var iCompanyTotalUnavailed = parseFloat(oCompanyData.company_allotment) - parseFloat(iCompanyTotalAvailed);

                                            uiManipulatedEsopTemplate.find('[data-label="company_total_shares_availed"]').text(number_format(iCompanyTotalAvailed, 2));
                                            uiManipulatedEsopTemplate.find('[data-label="company_total_shares_unavailed"]').text(number_format(iCompanyTotalUnavailed, 2));
                                        }

                                        uiHRDeptEsopTabContainer.after(uiManipulatedEsopTemplate);
                                        uiHRDeptEsopTabContainer.append(uiEsopInput);
                                        uiHRDeptEsopTabContainer.append(uiEsopLabel);
                                        uiEsopLabel.trigger('click');
                                    }
                                }
                            }
                        }
                    }
                }

                var arrFilteredParentIds = cr8v.check_duplicate(arrEsopParentIds);

                var oGetSpecialParams = {
                    "parent_ids" : arrFilteredParentIds.data.join()
                };

                roxas.esop.get_special(oGetSpecialParams, undefined, function(oData){
                    if(oData.status == true)
                    {
                        for(var i in oData.data)
                        {
                            var oEsopData = oData.data[i];
                            var uiManipulatedEsopTemplate = $('.hr_department_esop[data-esop-id="'+oEsopData.parent_id+'"]');

                            for(var x in oEsopData.stock_offers)
                            {
                                var oUserData = oEsopData.stock_offers[x];
                                if(oUserData.company_id == iCompanyID)
                                {
                                    var uiEmployeeTemplate = uiManipulatedEsopTemplate.find('.esop_employees[data-user-id="'+oUserData.user_id+'"]');

                                    if(uiEmployeeTemplate.length == 0)
                                    {
                                        var uiEmployeeTemplate = uiManipulatedEsopTemplate.find('.esop_employees.template').clone().removeClass('template hidden');
                                        var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);

                                        uiManipulatedEsopTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                        uiManipulatedEmployeeTemplate.find('[data-label="employee_code"]').prepend('<i class="fa fa-exclamation-triangle default-cursor" title="This employee has been added with different details. Please check additionals for more info."></i> ');
                                        uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopData.id);
                                    }
                                    else
                                    {
                                        roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                                        uiEmployeeTemplate.find('[data-label="employee_code"]').prepend('<i class="fa fa-exclamation-triangle default-cursor" title="This employee has been added with different details. Please check additionals for more info."></i> ');
                                        uiEmployeeTemplate.attr('data-esop-id', oEsopData.id);
                                    }
                                    
                                    if(cr8v.var_check(oUserData.accepted))
                                    {
                                        var iTotalEsopAvailed = uiManipulatedEsopTemplate.find('[data-label="esop_total_shares_availed"]:first').text();
                                        var iTotalEsopUnavailed = uiManipulatedEsopTemplate.find('[data-label="esop_total_shares_unavailed"]:first').text();

                                        var newTotalEsopAvailed = number_format((parseFloat(cr8v.remove_commas(iTotalEsopAvailed)) + parseFloat(oUserData.accepted)), 2);
                                        var newTotalEsopUnavailed = number_format((parseFloat(cr8v.remove_commas(iTotalEsopUnavailed)) - parseFloat(oUserData.accepted)), 2);
                                        uiManipulatedEsopTemplate.find('[data-label="esop_total_shares_availed"]:first').text(newTotalEsopAvailed);
                                        uiManipulatedEsopTemplate.find('[data-label="esop_total_shares_unavailed"]').text(newTotalEsopUnavailed);

                                        var iTotalCompanyAvailed = uiManipulatedEsopTemplate.find('[data-label="company_total_shares_availed"]').text();
                                        var iTotalCompanyUnavailed = uiManipulatedEsopTemplate.find('[data-label="company_total_shares_unavailed"]').text();

                                        var newTotalCompanyAvailed = number_format((parseFloat(cr8v.remove_commas(iTotalCompanyAvailed)) + parseFloat(oUserData.accepted)), 2);
                                        var newTotalCompanyUnavailed = number_format((parseFloat(cr8v.remove_commas(iTotalCompanyUnavailed)) - parseFloat(oUserData.accepted)), 2);
                                        uiManipulatedEsopTemplate.find('[data-label="company_total_shares_availed"]').text(newTotalCompanyAvailed);
                                        uiManipulatedEsopTemplate.find('[data-label="company_total_shares_unavailed"]').text(newTotalCompanyUnavailed);

                                        var iEmployeeTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopData.price_per_share)), 2);
                                        uiEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iEmployeeTotalAmountAvailed);

                                        iTotalMainAvailed = parseFloat(iTotalMainAvailed) + parseFloat(oUserData.accepted);
                                    }

                                    uiManipulatedEsopTemplate.find('[data-container="additionals_container"]').removeClass('hidden');
                                    var uiSpecialTemplate = uiManipulatedEsopTemplate.find('[data-container="additionals_container"]').find('.esop_special.template').clone().removeClass('hidden template');
                                    var uiManipulatedSpecialTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiSpecialTemplate);

                                    var sCurrencyName = cr8v.var_check(oEsopData.currency_name) ? oEsopData.currency_name : roxas.esop.get_currency_name(oEsopData.currency) ;
                                    var sPricePerShare = sCurrencyName +' '+ oEsopData.price_per_share;

                                    uiManipulatedSpecialTemplate.find('[data-label="grant_date"]').text(moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY'));
                                    uiManipulatedSpecialTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                                    uiManipulatedSpecialTemplate.find('[data-label="vesting_years"]').text(oEsopData.vesting_years);
                                    uiManipulatedSpecialTemplate.attr('data-esop-id', oEsopData.id);
                                    uiManipulatedSpecialTemplate.attr('data-esop-parent-id', oEsopData.parent_id);

                                    uiManipulatedEsopTemplate.find('[data-container="esop_special_container"]').append(uiSpecialTemplate);
                                }
                            }
                        }
                    }

                    var iTotalMainUnavailed = parseFloat(iTotalMainAlloted) - parseFloat(iTotalMainAvailed);
                    $('[data-container="esop_total_shares_container"]').find('[data-label="main_total_alloted_shares"]').text(number_format(iTotalMainAlloted, 2));
                    $('[data-container="esop_total_shares_container"]').find('[data-label="main_total_shares_availed"]').text(number_format(iTotalMainAvailed, 2));
                    $('[data-container="esop_total_shares_container"]').find('[data-label="main_total_shares_unavailed"]').text(number_format(iTotalMainUnavailed, 2));

                    roxas.esop.render_pie_chart(iTotalMainAlloted, iTotalMainAvailed, $('[data-container="esop_total_shares_container"]').find('.chart-container'), true);
                }, true);
            }
        },

        /**
         * check_esop_buttons
         * @description This function will put the hide/show the required buttons of view esop
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        check_esop_buttons : function(oEsopData, bFromSetOffer){
            if(cr8v.var_check(oEsopData))
            {
                if((cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0) || (cr8v.var_check(bFromSetOffer) && bFromSetOffer))
                {
                    if(cr8v.var_check(oEsopData.offer_expiration) && oEsopData.offer_expiration != '0000-00-00')
                    {
                        if(cr8v.var_check(oEsopData.letter_sent) && oEsopData.letter_sent > 0)
                        {
                            $('.undistributed_esop').addClass('hidden');
                            $('.distributed_esop').addClass('hidden');
                            $('.expiration_set').addClass('hidden');
                            /*redirect to other page*/

                            if(window.location.href.indexOf('esop/view_esop_batch') == -1)
                            {
                                window.location = roxas.config('url.server.base') + 'esop/view_esop_batch';
                            }
                        }
                        else
                        {
                            $('.undistributed_esop').addClass('hidden');
                            $('.distributed_esop').addClass('hidden');
                            $('.expiration_set').removeClass('hidden');
                            if(window.location.href.indexOf('esop/view_esop_batch') > -1)
                            {
                                window.location = roxas.config('url.server.base') + "auth/forbidden_403";
                            }
                        }
                    }
                    else
                    {
                        $('.undistributed_esop').addClass('hidden');
                        $('.distributed_esop').removeClass('hidden');
                        $('.expiration_set').addClass('hidden');
                        if(window.location.href.indexOf('esop/view_esop_batch') > -1)
                        {
                            window.location = roxas.config('url.server.base') + "auth/forbidden_403";
                        }
                    }
                }
                else
                {
                    $('.undistributed_esop').removeClass('hidden');
                    $('.distributed_esop').addClass('hidden');
                    $('.expiration_set').addClass('hidden');
                    if(window.location.href.indexOf('esop/view_esop_batch') > -1)
                    {
                        window.location = roxas.config('url.server.base') + "auth/forbidden_403";
                    }
                }
            }
        },

        /**
         * check_esop_batch_tab_buttons
         * @description This function will put the hide/show the required buttons of view esop batch tabs
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        check_esop_batch_tab_buttons : function(oEsopData, uiTemplate){
            if(cr8v.var_check(oEsopData) && cr8v.var_check(uiTemplate))
            {
                if((cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0))
                {
                    if(cr8v.var_check(oEsopData.offer_expiration) && oEsopData.offer_expiration != '0000-00-00')
                    {
                        if(cr8v.var_check(oEsopData.letter_sent) && oEsopData.letter_sent > 0)
                        {
                            uiTemplate.find('.undistributed_esop').addClass('hidden');
                            uiTemplate.find('.distributed_esop').addClass('hidden');
                            uiTemplate.find('.expiration_set').addClass('hidden');
                            /*redirect to other page*/

                        }
                        else
                        {
                            uiTemplate.find('.undistributed_esop').addClass('hidden');
                            uiTemplate.find('.distributed_esop').addClass('hidden');
                            uiTemplate.find('.expiration_set').removeClass('hidden');
                        }
                    }
                    else
                    {
                        uiTemplate.find('.undistributed_esop').addClass('hidden');
                        uiTemplate.find('.distributed_esop').removeClass('hidden');
                        uiTemplate.find('.expiration_set').addClass('hidden');
                    }
                }
                else
                {
                    uiTemplate.find('.undistributed_esop').removeClass('hidden');
                    uiTemplate.find('.distributed_esop').addClass('hidden');
                    uiTemplate.find('.expiration_set').addClass('hidden');
                }
            }
        },

        /**
         * check_esop_batch_buttons
         * @description This function will put the hide/show the required buttons of view esop batch
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        check_esop_batch_buttons : function(oEsopData, bFromEsopBatch, iEsopTotalUnavailed){
            if(cr8v.var_check(oEsopData))
            {                
                if((cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0) || (cr8v.var_check(bFromEsopBatch) && bFromEsopBatch))
                {
                    if(cr8v.var_check(oEsopData.offer_expiration) && oEsopData.offer_expiration != '0000-00-00')
                    {
                        if(cr8v.var_check(oEsopData.letter_sent) && oEsopData.letter_sent > 0)
                        {
                            var oOfferExpiration = moment(oEsopData.offer_expiration, 'YYYY-MM-DD');

                            cr8v.get_time(function (sTime){
                                var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
                                var oCurrentDate = moment(sCurrentDate, 'YYYY-MM-DD');

                                if(iCreateEsopBatchID == oEsopData.id)
                                {
                                    if(oCurrentDate.diff(oOfferExpiration, 'days') > 0)
                                    {
                                        if(iEsopTotalUnavailed > 0)
                                        {
                                            // uiCreateEsopBatchModalBtn.removeClass('hidden');
                                            uiCreateEsopBatchModal.find('[data-container="valid-create-batch"]').removeClass('hidden');
                                            uiCreateEsopBatchModal.find('[data-container="invalid-create-batch"]').addClass('hidden');
                                            uiCreateEsopBatchModal.find('[data-container="invalid-create-batch-max"]').addClass('hidden');

                                            uiAddNewEmployeeModal.find('[data-container="valid-add-employee"]').removeClass('hidden');
                                            uiAddNewEmployeeModal.find('[data-container="invalid-add-employee"]').addClass('hidden');
                                            uiAddNewEmployeeModal.find('[data-container="invalid-add-employee-max"]').addClass('hidden');
                                        }
                                        else
                                        {
                                            // uiCreateEsopBatchModalBtn.addClass('hidden');
                                            uiCreateEsopBatchModal.find('[data-container="valid-create-batch"]').addClass('hidden');
                                            uiCreateEsopBatchModal.find('[data-container="invalid-create-batch"]').addClass('hidden');
                                            uiCreateEsopBatchModal.find('[data-container="invalid-create-batch-max"]').removeClass('hidden');

                                            uiAddNewEmployeeModal.find('[data-container="valid-add-employee"]').addClass('hidden');
                                            uiAddNewEmployeeModal.find('[data-container="invalid-add-employee"]').addClass('hidden');
                                            uiAddNewEmployeeModal.find('[data-container="invalid-add-employee-max"]').removeClass('hidden');
                                        }
                                    }
                                    else
                                    {
                                        // uiCreateEsopBatchModalBtn.addClass('hidden');
                                        uiCreateEsopBatchModal.find('[data-container="valid-create-batch"]').addClass('hidden');
                                        uiCreateEsopBatchModal.find('[data-container="invalid-create-batch"]').removeClass('hidden');
                                        uiCreateEsopBatchModal.find('[data-container="invalid-create-batch-max"]').addClass('hidden');

                                        uiAddNewEmployeeModal.find('[data-container="valid-add-employee"]').addClass('hidden');
                                        uiAddNewEmployeeModal.find('[data-container="invalid-add-employee"]').removeClass('hidden');
                                        uiAddNewEmployeeModal.find('[data-container="invalid-add-employee-max"]').addClass('hidden');
                                    }
                                }
                            });
                        }
                        else
                        {
                            // uiCreateEsopBatchModalBtn.addClass('hidden');
                            uiCreateEsopBatchModal.find('[data-container="valid-create-batch"]').addClass('hidden');
                            uiCreateEsopBatchModal.find('[data-container="invalid-create-batch"]').removeClass('hidden');
                            uiCreateEsopBatchModal.find('[data-container="invalid-create-batch-max"]').addClass('hidden');

                            uiAddNewEmployeeModal.find('[data-container="valid-add-employee"]').addClass('hidden');
                            uiAddNewEmployeeModal.find('[data-container="invalid-add-employee"]').removeClass('hidden');
                            uiAddNewEmployeeModal.find('[data-container="invalid-add-employee-max"]').addClass('hidden');
                        }
                    }
                    else
                    {
                        // uiCreateEsopBatchModalBtn.addClass('hidden');
                        uiCreateEsopBatchModal.find('[data-container="valid-create-batch"]').addClass('hidden');
                        uiCreateEsopBatchModal.find('[data-container="invalid-create-batch"]').removeClass('hidden');
                        uiCreateEsopBatchModal.find('[data-container="invalid-create-batch-max"]').addClass('hidden');

                        uiAddNewEmployeeModal.find('[data-container="valid-add-employee"]').addClass('hidden');
                        uiAddNewEmployeeModal.find('[data-container="invalid-add-employee"]').removeClass('hidden');
                        uiAddNewEmployeeModal.find('[data-container="invalid-add-employee-max"]').addClass('hidden');
                    }
                }
            }
        },

        /**
         * check_esop_batch_buttons
         * @description This function will put the esop data to dropdown of upload payment record
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        check_esop_if_expired : function(oEsopData){
            if(cr8v.var_check(oEsopData))
            {
                if(cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0)
                {
                    if(cr8v.var_check(oEsopData.offer_expiration) && oEsopData.offer_expiration != '0000-00-00')
                    {
                        if(cr8v.var_check(oEsopData.letter_sent) && oEsopData.letter_sent > 0)
                        {
                            var oOfferExpiration = moment(oEsopData.offer_expiration, 'YYYY-MM-DD');

                            cr8v.get_time(function (sTime){
                                var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
                                var oCurrentDate = moment(sCurrentDate, 'YYYY-MM-DD');
                                
                                if(oCurrentDate.diff(oOfferExpiration, 'days') > 0)
                                {
                                    iExpiredEsop++;
                                    cr8v.append_dropdown(oEsopData, uiPaymentUploadEsopDropdown, false, 'esop');
                                    uiPaymentUploadEsopDropdown.find('.option:first').trigger('click');
                                }

                                uiPaymentUploadEsopDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);

                                if(iExpiredEsop > 0)
                                {
                                    uiUploadPaymentModal.find('[data-container="valid-upload-payment"]').removeClass('hidden');
                                    uiUploadPaymentModal.find('[data-container="invalid-upload-payment"]').addClass('hidden');
                                }
                                else
                                {
                                    uiUploadPaymentModal.find('[data-container="valid-upload-payment"]').addClass('hidden');
                                    uiUploadPaymentModal.find('[data-container="invalid-upload-payment"]').removeClass('hidden');
                                }
                            });
                        }
                    }
                }
            }
        },

        /**
         * manipulate_esop_batch_data
         * @description This function will manipulate the data in view esop batch page
         * @dependencies N/A
         * @param {object} oEsopData
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_esop_batch_data : function (oEsopData, bFromEsopBatch) {
            if(cr8v.var_check(oEsopData) && cr8v.var_check(bFromEsopBatch) && bFromEsopBatch)
            {
                var arrEsopBatchParentIds = [];

                var uiTemplate = $('[data-container="esop_information"]');
                var uiManipulatedTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiTemplate);

                var iTotalSharesAllotedBatches = oEsopData.total_share_qty;
                var iTotalSharesAvailedBatches = 0;

                /*main esop batch tab*/
                var uiEsopBatch = $('.esop_batch.template.hidden').clone().removeClass('template hidden');
                var uiManipulatedBatchTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiEsopBatch);
                var uiEsopBatchInput = $('.esop_batch_tab_input.template.hidden').clone().removeClass('template hidden');
                var uiEsopBatchLabel = $('.esop_batch_tab_label.template.hidden').clone().removeClass('template hidden');
                var iTabNumber = cr8v.generate_random_keys(8)/*Math.floor((Math.random() * 10000000000000000000) + 1)*/;
                
                uiEsopBatch.attr('data-attr', iTabNumber);
                uiEsopBatchInput.attr('data-attr', iTabNumber);
                uiEsopBatchLabel.attr('data-attr', iTabNumber);
                uiManipulatedBatchTemplate.find('.tab').attr('data-attr', iTabNumber);
                uiManipulatedBatchTemplate.data('full_data', oEsopData);
                
                uiEsopBatchLabel.text(oEsopData.name);

                uiEsopBatchContainer.find('.esop_batch:not(.template)').remove();
                uiEsopBatchTabContainer.find('.esop_batch_tab_input:not(.template), .esop_batch_tab_label:not(.template)').remove();

                uiEsopBatchTabContainer.after(uiManipulatedBatchTemplate);
                uiEsopBatchTabContainer.append(uiEsopBatchInput);
                uiEsopBatchTabContainer.append(uiEsopBatchLabel);

                uiEsopBatchLabel.trigger('click');
                
                roxas.esop.bind_get_gratuity(uiManipulatedBatchTemplate, oEsopData.id);

                if(cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0)
                {
                    var iEsopTotalAccepted = 0;
                    var iEsopTotalAvailed = 0;

                    for(var x in oEsopData.distribution)
                    {
                        var iCompanyTotalAccepted = 0;
                        var iCompanyTotalAvailed = 0;

                        /*populate companies first*/
                        var oCompanyData = oEsopData.distribution[x];
                        var uiCompanyTemplate = uiManipulatedBatchTemplate.find('[data-container="esop_companies_container"]').find('.esop_companies.template.hidden').clone().removeClass('template hidden');
                        var uiManipulatedCompanyTemplate = roxas.esop.manipulate_esop_companies(oCompanyData, uiCompanyTemplate, undefined, bFromEsopBatch);
                        uiManipulatedBatchTemplate.find('[data-container="esop_companies_container"]').append(uiManipulatedCompanyTemplate);
                        
                        for(var k in oCompanyData.employees)
                        {
                            /*then populate the employees in that company*/
                            var oUserData = oCompanyData.employees[k];
                            var uiEmployeeTemplate = uiManipulatedCompanyTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                            var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                            uiManipulatedCompanyTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                            uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopData.id);

                            if(oUserData.status == 2)
                            {
                                iEsopTotalAccepted++;
                                iCompanyTotalAccepted++;
                                iEsopTotalAvailed = parseFloat(iEsopTotalAvailed) + parseFloat(oUserData.accepted);
                                iCompanyTotalAvailed = parseFloat(iCompanyTotalAvailed) + parseFloat(oUserData.accepted);
                                var iTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopData.price_per_share)), 2);
                                uiManipulatedEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iTotalAmountAvailed);
                            }
                        }

                        var iCompanyTotalAlloted = parseFloat(oCompanyData.company_allotment) - parseFloat(iCompanyTotalAvailed);

                        uiManipulatedCompanyTemplate.find('[data-label="company_total_employees_accepted"]').text(iCompanyTotalAccepted);
                        uiManipulatedCompanyTemplate.find('[data-label="company_total_shares_availed"]').text(number_format(iCompanyTotalAvailed, 2));
                        uiManipulatedCompanyTemplate.find('[data-label="company_total_shares_unavailed"]').text(number_format(iCompanyTotalAlloted, 2));
                    }
                    
                    var iEsopTotalUnavailed = parseFloat(oEsopData.total_share_qty) - parseFloat(iEsopTotalAvailed);

                    iTotalSharesAvailedBatches = parseFloat(iTotalSharesAvailedBatches) + parseFloat(iEsopTotalAvailed);

                    /*for the total shares per esop batch tab*/
                    uiManipulatedBatchTemplate.find('[data-label="esop_total_employees_accepted"]').text(iEsopTotalAccepted);
                    uiManipulatedBatchTemplate.find('[data-label="esop_total_shares_availed"]').text(number_format(iEsopTotalAvailed, 2));
                    uiManipulatedBatchTemplate.find('[data-label="esop_total_shares_unavailed"]').text(number_format(iEsopTotalUnavailed, 2));
                       
                    uiManipulatedBatchTemplate.on('click', '[modal-target="add-gratuity"]', function(){
                        var uiThis = $(this);
                        var uiParent = uiThis.parents('.esop_batch:first');

                        uiAddGratuityModal.attr('data-esop-id', uiParent.attr('data-esop-id'));
                        uiAddGratuityModal.attr('data-esop-total-availed', uiParent.find('[data-label="esop_total_shares_availed"]').text());
                        uiAddGratuityModal.find('[data-label="esop_name"]').text(uiParent.attr('data-esop-name'));
                        uiAddGratuityModal.attr('data-esop-name', uiParent.attr('data-esop-name'));
                    });
                }

                arrEsopBatchParentIds.push(oEsopData.id);
                roxas.esop.check_esop_if_expired(oEsopData);

                var oGetLogsParams = {
                    "where" : [
                        {
                            "field" : "ref_id",
                            "operator" : "IN",
                            "value" : "("+oEsopData.id+")"
                        },
                        {
                            "field" : "type",
                            "operator" : "IN",
                            "value" : "('esop', 'esop/user')"
                        }
                    ]
                };
                cr8v.get_audit_logs(oGetLogsParams, uiManipulatedBatchTemplate);

                var bHasBatch = false;
                if(cr8v.var_check(oEsopData.esop_batches) && count(oEsopData.esop_batches) > 0)
                {
                    bHasBatch = true;
                    /*baby esop batch tabs*/
                    for (var i in oEsopData.esop_batches) {
                        var oEsopBatchData = oEsopData.esop_batches[i];            
                                    
                        var uiEsopBatch = $('.esop_batch.template.hidden').clone().removeClass('template hidden');
                        var uiManipulatedBatchTemplate = roxas.esop.manipulate_esop_data(oEsopBatchData, uiEsopBatch);
                        var uiEsopBatchInput = $('.esop_batch_tab_input.template.hidden').clone().removeClass('template hidden');
                        var uiEsopBatchLabel = $('.esop_batch_tab_label.template.hidden').clone().removeClass('template hidden');
                        var iTabNumber = cr8v.generate_random_keys(8)/*Math.floor((Math.random() * 10000000000000000000) + 1)*/;

                        roxas.esop.check_esop_batch_tab_buttons(oEsopBatchData, uiManipulatedBatchTemplate);
                        
                        uiEsopBatch.attr('data-attr', iTabNumber);
                        uiEsopBatchInput.attr('data-attr', iTabNumber);
                        uiEsopBatchLabel.attr('data-attr', iTabNumber);
                        uiEsopBatchLabel.text(oEsopBatchData.name);
                        uiManipulatedBatchTemplate.find('.tab').attr('data-attr', iTabNumber);
                        uiManipulatedBatchTemplate.data('full_data', oEsopBatchData);
                        
                        // uiEsopBatchContainer.find('.esop_batch:not(.template)').remove();
                        // uiEsopBatchTabContainer.find('.esop_batch_tab_input:not(.template), .esop_batch_tab_label:not(.template)').remove();

                        uiEsopBatchTabContainer.after(uiManipulatedBatchTemplate);
                        uiEsopBatchTabContainer.append(uiEsopBatchInput);
                        uiEsopBatchTabContainer.append(uiEsopBatchLabel);

                        roxas.esop.bind_get_gratuity(uiManipulatedBatchTemplate, oEsopBatchData.id);

                        uiManipulatedBatchTemplate.on('click', '[modal-target="edit-esop-batch"]', function(){
                            var uiThis = $(this);
                            var uiParent = uiThis.parents('.esop_batch:first');
                            
                            roxas.esop.clear_edit_esop_batch_modal();

                            uiEditEsopBatchModal.find('[name="esop_name"]').val(uiParent.attr('data-esop-name'));
                            uiEditEsopBatchModal.attr('data-esop-id', uiParent.attr('data-esop-id'));
                        });

                        uiManipulatedBatchTemplate.on('click', '[modal-target="offer-expiration-batch"]', function(){
                            var uiThis = $(this);
                            var uiParent = uiThis.parents('.esop_batch:first');
                            
                            roxas.esop.clear_set_offer_expiration_modal();

                            uiSetOfferExpBatchModal.attr('data-esop-id', uiParent.attr('data-esop-id'));
                            uiSetOfferExpBatchModal.attr('data-esop-name', uiParent.attr('data-esop-name'));
                        });

                        uiManipulatedBatchTemplate.on('click', '[modal-target="send-letter-batch"]', function(){
                            var uiThis = $(this);
                            var uiParent = uiThis.parents('.esop_batch:first');

                            uiEsopSendLetterBatchModal.attr('data-esop-id', uiParent.attr('data-esop-id'));
                            uiEsopSendLetterBatchModal.attr('data-esop-name', uiParent.attr('data-esop-name'));
                        });

                        uiManipulatedBatchTemplate.on('click', '[modal-target="batch-share-distribution-template"]', function(){
                            var uiThis = $(this);
                            var uiParent = uiThis.parents('.esop_batch:first');

                            uiUploadBatchShareDistributionModal.attr('data-esop-id', uiParent.attr('data-esop-id'));

                            roxas.esop.clear_batch_upload_share_distribution_modal();
                        });

                        // uiEsopBatchLabel.trigger('click');
                        
                        arrEsopBatchParentIds.push(oEsopBatchData.id);
                        roxas.esop.check_esop_if_expired(oEsopBatchData);

                        var oGetLogsParams = {
                            "where" : [
                                {
                                    "field" : "ref_id",
                                    "operator" : "IN",
                                    "value" : "("+oEsopBatchData.id+")"
                                },
                                {
                                    "field" : "type",
                                    "operator" : "IN",
                                    "value" : "('esop', 'esop/user')"
                                }
                            ]
                        };
                        cr8v.get_audit_logs(oGetLogsParams, uiManipulatedBatchTemplate);

                        var iEsopTotalAccepted = 0;
                        var iEsopTotalAvailed = 0;

                        if(cr8v.var_check(oEsopBatchData.distribution) && count(oEsopBatchData.distribution) > 0)
                        {
                            for(var x in oEsopBatchData.distribution)
                            {
                                var iCompanyTotalAccepted = 0;
                                var iCompanyTotalAvailed = 0;

                                /*populate companies first*/
                                var oCompanyData = oEsopBatchData.distribution[x];
                                var uiCompanyTemplate = uiManipulatedBatchTemplate.find('[data-container="esop_companies_container"]').find('.esop_companies.template.hidden').clone().removeClass('template hidden');
                                var uiManipulatedCompanyTemplate = roxas.esop.manipulate_esop_companies(oCompanyData, uiCompanyTemplate, undefined, bFromEsopBatch);
                                uiManipulatedBatchTemplate.find('[data-container="esop_companies_container"]').append(uiManipulatedCompanyTemplate);
                                
                                for(var k in oCompanyData.employees)
                                {
                                    /*then populate the employees in that company*/
                                    var oUserData = oCompanyData.employees[k];
                                    var uiEmployeeTemplate = uiManipulatedCompanyTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                                    var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                                    uiManipulatedCompanyTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                    uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopBatchData.id);

                                    if(oUserData.status == 2)
                                    {
                                        iEsopTotalAccepted++;
                                        iCompanyTotalAccepted++;
                                        iEsopTotalAvailed = parseFloat(iEsopTotalAvailed) + parseFloat(oUserData.accepted);
                                        iCompanyTotalAvailed = parseFloat(iCompanyTotalAvailed) + parseFloat(oUserData.accepted);
                                        var iTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopBatchData.price_per_share)), 2);
                                        uiManipulatedEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iTotalAmountAvailed);
                                    }
                                }

                                var iCompanyTotalAlloted = parseFloat(oCompanyData.company_allotment) - parseFloat(iCompanyTotalAvailed);

                                uiManipulatedCompanyTemplate.find('[data-label="company_total_employees_accepted"]').text(iCompanyTotalAccepted);
                                uiManipulatedCompanyTemplate.find('[data-label="company_total_shares_availed"]').text(number_format(iCompanyTotalAvailed, 2));
                                uiManipulatedCompanyTemplate.find('[data-label="company_total_shares_unavailed"]').text(number_format(iCompanyTotalAlloted, 2));
                            }
                            
                        }

                        var iEsopTotalUnavailed = parseFloat(oEsopBatchData.total_share_qty) - parseFloat(iEsopTotalAvailed);

                        iTotalSharesAvailedBatches = parseFloat(iTotalSharesAvailedBatches) + parseFloat(iEsopTotalAvailed);

                        /*for the total shares per esop batch tab*/
                        uiManipulatedBatchTemplate.find('[data-label="esop_total_employees_accepted"]').text(iEsopTotalAccepted);
                        uiManipulatedBatchTemplate.find('[data-label="esop_total_shares_availed"]').text(number_format(iEsopTotalAvailed, 2));
                        uiManipulatedBatchTemplate.find('[data-label="esop_total_shares_unavailed"]').text(number_format(iEsopTotalUnavailed, 2));
                        iCreateEsopBatchID = oEsopBatchData.id;
                        roxas.esop.check_esop_batch_buttons(oEsopBatchData, bFromEsopBatch, iEsopTotalUnavailed);

                        uiManipulatedBatchTemplate.on('click', '[modal-target="add-gratuity"]', function(){
                            var uiThis = $(this);
                            var uiParent = uiThis.parents('.esop_batch:first');

                            uiAddGratuityModal.attr('data-esop-id', uiParent.attr('data-esop-id'));
                            uiAddGratuityModal.attr('data-esop-total-availed', uiParent.find('[data-label="esop_total_shares_availed"]').text());
                            uiAddGratuityModal.find('[data-label="esop_name"]').text(uiParent.attr('data-esop-name'));
                            uiAddGratuityModal.attr('data-esop-name', uiParent.attr('data-esop-name'));
                        });
                    }
                }

                var iTotalSharesUnvailedBatches = iTotalSharesAllotedBatches - iTotalSharesAvailedBatches;

                /*for the main total shares container at the top*/
                $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_shares_availed"]').text(number_format(iTotalSharesAvailedBatches, 2));
                $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_shares_unavailed"]').text(number_format(iTotalSharesUnvailedBatches, 2));
                uiCreateEsopBatchBtn.attr('esop-total-unavailed-batches', iTotalSharesUnvailedBatches);
                uiCreateEsopBatchModal.attr('esop-total-unavailed-batches', iTotalSharesUnvailedBatches);
                uiCreateEsopBatchModal.find('.esop_batch_total_share_qty_tool_tip').attr('tt-html', '<p>Available Shares:</p><p><small>'+number_format(iTotalSharesUnvailedBatches, 2)+' Shares</small></p>');

                if(!bHasBatch)
                {
                    iCreateEsopBatchID = oEsopData.id;
                    roxas.esop.check_esop_batch_buttons(oEsopData, bFromEsopBatch, iTotalSharesUnvailedBatches);
                }

                /*for removing of previous users that accepted stock offers in the previous batches*/
                var arrFilteredParentIds = cr8v.check_duplicate(arrEsopBatchParentIds);
                localStorage.removeItem("esop_batch_parent_id");
                localStorage.setItem('esop_batch_parent_id', arrFilteredParentIds.data);

                
                var oGetSpecialParams = {
                    "parent_ids" : localStorage.getItem('esop_batch_parent_id')
                };

                roxas.esop.get_special(oGetSpecialParams, undefined, function(oData){
                    roxas.esop.compute_shares_taken_from_others();
                    roxas.esop.render_pie_chart(iTotalSharesAllotedBatches, iTotalSharesAvailedBatches, $('[data-container="esop_total_shares_container"]').find('.chart-container'), true);
                });

            }
        },
        /**
         * get_special
         * @description This function is for getting and rendering of special esop
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_special : function(oParams, uiThis, fnCallback, bFromDashboard){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/get_special",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        //  console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bFromDashboard) && bFromDashboard)
                                {

                                }
                                else
                                {
                                    for(var i in oData.data)
                                    {
                                        var oEsopData = oData.data[i];
                                        var uiBatchTemplate = $('.esop_batch[data-esop-id="'+oEsopData.parent_id+'"]');
                                        // console.log(uiBatchTemplate);

                                        for(var x in oEsopData.stock_offers)
                                        {
                                            var oUserData = oEsopData.stock_offers[x];
                                            var uiCompanyTemplate = uiBatchTemplate.find('.esop_companies[data-company-id="'+oUserData.company_id+'"]');
                                            var uiEmployeeTemplate = uiBatchTemplate.find('.esop_employees[data-user-id="'+oUserData.user_id+'"]');

                                            if(uiEmployeeTemplate.length == 0)
                                            {
                                                var uiEmployeeTemplate = uiCompanyTemplate.find('.esop_employees.template').clone().removeClass('template hidden');
                                                var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);

                                                uiCompanyTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                                uiManipulatedEmployeeTemplate.find('[data-label="company_code"]').prepend('<i class="fa fa-exclamation-triangle default-cursor" title="This employee has been added with different details. Please check additionals for more info."></i> ');
                                                uiManipulatedEmployeeTemplate.attr('data-esop-id', oEsopData.id);
                                            }
                                            else
                                            {
                                                roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate);
                                                uiEmployeeTemplate.find('[data-label="company_code"]').prepend('<i class="fa fa-exclamation-triangle default-cursor" title="This employee has been added with different details. Please check additionals for more info."></i> ');
                                                uiEmployeeTemplate.attr('data-esop-id', oEsopData.id);
                                            }
                                            
                                            if(cr8v.var_check(oUserData.accepted))
                                            {
                                                var iTotalEsopAvailed = uiBatchTemplate.find('[data-label="esop_total_shares_availed"]:first').text();
                                                var iTotalEsopUnavailed = uiBatchTemplate.find('[data-label="esop_total_shares_unavailed"]:first').text();

                                                var newTotalEsopAvailed = number_format((parseFloat(cr8v.remove_commas(iTotalEsopAvailed)) + parseFloat(oUserData.accepted)), 2);
                                                var newTotalEsopUnavailed = number_format((parseFloat(cr8v.remove_commas(iTotalEsopUnavailed)) - parseFloat(oUserData.accepted)), 2);
                                                uiBatchTemplate.find('[data-label="esop_total_shares_availed"]:first').text(newTotalEsopAvailed);
                                                uiBatchTemplate.find('[data-label="esop_total_shares_unavailed"]').text(newTotalEsopUnavailed);

                                                var iTotalCompanyAvailed = uiCompanyTemplate.find('[data-label="company_total_shares_availed"]').text();
                                                var iTotalCompanyUnavailed = uiCompanyTemplate.find('[data-label="company_total_shares_unavailed"]').text();

                                                var newTotalCompanyAvailed = number_format((parseFloat(cr8v.remove_commas(iTotalCompanyAvailed)) + parseFloat(oUserData.accepted)), 2);
                                                var newTotalCompanyUnavailed = number_format((parseFloat(cr8v.remove_commas(iTotalCompanyUnavailed)) - parseFloat(oUserData.accepted)), 2);
                                                uiCompanyTemplate.find('[data-label="company_total_shares_availed"]').text(newTotalCompanyAvailed);
                                                uiCompanyTemplate.find('[data-label="company_total_shares_unavailed"]').text(newTotalCompanyUnavailed);

                                                var iTotalAmountAvailed = number_format((parseFloat(oUserData.accepted) * parseFloat(oEsopData.price_per_share)), 2);
                                                uiEmployeeTemplate.find('[data-label="employee_total_amount_availed"]').text(iTotalAmountAvailed);

                                                uiClassEmployeeDropDown.find('[data-value="'+oUserData.user_id+'"]').remove();

                                                var iTotalMainAvailed = $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_shares_availed"]:first').text();
                                                // var iTotalMainUnavailed = $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_shares_unavailed"]:first').text();

                                                var newTotalMainAvailed = number_format((parseFloat(cr8v.remove_commas(iTotalMainAvailed)) + parseFloat(oUserData.accepted)), 2);
                                                // var newTotalMainUnavailed = number_format((parseFloat(cr8v.remove_commas(iTotalMainUnavailed)) - parseFloat(oUserData.accepted)), 2);
                                                $('[data-container="esop_total_shares_container"]').find('[data-label="esop_total_shares_availed"]:first').text(newTotalMainAvailed);
                                                // uiBatchTemplate.find('[data-label="esop_total_shares_unavailed"]').text(newTotalMainUnavailed);

                                            }

                                            uiCompanyTemplate.find('[data-container="additionals_container"]').removeClass('hidden');
                                            var uiSpecialTemplate = uiCompanyTemplate.find('[data-container="additionals_container"]').find('.esop_special.template').clone().removeClass('hidden template');
                                            var uiManipulatedSpecialTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiSpecialTemplate);

                                            var sCurrencyName = cr8v.var_check(oEsopData.currency_name) ? oEsopData.currency_name : roxas.esop.get_currency_name(oEsopData.currency) ;
                                            var sPricePerShare = sCurrencyName +' '+ oEsopData.price_per_share;

                                            uiManipulatedSpecialTemplate.find('[data-label="grant_date"]').text(moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY'));
                                            uiManipulatedSpecialTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                                            uiManipulatedSpecialTemplate.find('[data-label="vesting_years"]').text(oEsopData.vesting_years);
                                            uiManipulatedSpecialTemplate.attr('data-esop-id', oEsopData.id);
                                            uiManipulatedSpecialTemplate.attr('data-esop-parent-id', oEsopData.parent_id);

                                            uiCompanyTemplate.find('[data-container="esop_special_container"]').append(uiSpecialTemplate);
                                        }
                                    }
                                }
                            }
                            else
                            {

                            }
                            if(typeof(fnCallback) == 'function')
                            {
                                fnCallback(oData);
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * compute_shares_taken_from_others
         * @description This function will compute the shares taken from other esop page and data for add new employee
         * @dependencies N/A
         * @param {object} oDepartmentData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        compute_shares_taken_from_others : function(){
            var uiSorted = $('.esop_batch:not(.template)').sort(function(a,b){
                var akey = $(a).attr('data-esop-id');
                var bkey = $(b).attr('data-esop-id');
                if(cr8v.var_check(akey) && cr8v.var_check(bkey) && akey.length > 0 && bkey.length > 0)
                {
                    akey = parseFloat(akey);
                    bkey = parseFloat(bkey);
                    if (akey == bkey)
                    {
                        return 0;
                    }
                    if (akey < bkey)
                    {
                        return -1;
                    } 
                    if (akey > bkey)
                    {
                        return 1;
                    } 
                }
                else
                {
                    return 0;
                }
            });


            $(uiSorted).each(function (key, elem){

                var uiCurrentElem = $(elem);
                var uiNextElem = $($(uiSorted)[key + 1]);

                if(cr8v.var_check($(uiSorted)[key + 1]))
                {
                    // uiCurrentElem.find('[data-container="esop_batch_total_shares_container"]').find('[data-container="shares_taken"]').removeClass('hidden');
                    // uiCurrentElem.find('[data-container="esop_batch_total_shares_container"]').find('[data-container="shares_not_taken"]').addClass('hidden');

                    var elemTotalUnavailed = uiCurrentElem.find('[data-container="esop_batch_total_shares_container"]').find('[data-container="shares_taken"]').find('[data-label="esop_total_shares_unavailed"]');
                    var nextElemTotalTaken = uiNextElem.find('[data-label="total_share_qty"]');

                    // console.log(elemTotalUnavailed)
                    // console.log(nextElemTotalTaken)

                    var iElemTotalUnavailed = parseFloat(cr8v.remove_commas(elemTotalUnavailed.text()));
                    var iNextElemTotalTaken = parseFloat(cr8v.remove_commas(nextElemTotalTaken.text()));

                    var iSharesAvailable = iElemTotalUnavailed - iNextElemTotalTaken;

                    uiCurrentElem.find('[data-container="esop_batch_total_shares_container"]')
                        .find('[data-container="shares_taken"]')
                        .find('.shares_taken_by_other_esop_tool_tip')
                        .attr('tt-html', "<p class='font-15'>Shares Taken by "+uiNextElem.attr('data-esop-name')+":</p><p class='text-center font-20'>"+number_format(iNextElemTotalTaken, 2)+" Shares</p>")

                    if(iSharesAvailable > 0)
                    {
                        cr8v.append_dropdown(uiCurrentElem.data('full_data'), uiClassEsopDropDown, false, 'esop', 'data-esop-available', iSharesAvailable);
                        var oEsopData = uiCurrentElem.data('full_data');
                        uiClassEsopDropDown.find('.option.esop[data-value="'+oEsopData.id+'"]')
                            .attr({
                                'data-esop-grant-date': oEsopData.grant_date,
                                'data-esop-price-per-share': oEsopData.price_per_share,
                                'data-esop-vesting-year': oEsopData.vesting_years,
                                'data-esop-currency': oEsopData.currency
                            });
                    }
                }
                else
                {
                    // uiCurrentElem.find('[data-container="esop_batch_total_shares_container"]').find('[data-container="shares_taken"]').addClass('hidden');
                    // uiCurrentElem.find('[data-container="esop_batch_total_shares_container"]').find('[data-container="shares_not_taken"]').removeClass('hidden');

                    var elemTotalUnavailed = uiCurrentElem.find('[data-container="esop_batch_total_shares_container"]').find('[data-container="shares_taken"]').find('[data-label="esop_total_shares_unavailed"]');
                    var elemTotalTaken = uiCurrentElem.find('[data-label="total_share_qty"]');

                    var iElemTotalUnavailed = parseFloat(cr8v.remove_commas(elemTotalUnavailed.text()));
                    var iElemTotalTaken = parseFloat(cr8v.remove_commas(elemTotalTaken.text()));

                    var iSharesAvailable = iElemTotalUnavailed - iElemTotalTaken;

                    if(iElemTotalUnavailed > 0)
                    {
                        cr8v.append_dropdown(uiCurrentElem.data('full_data'), uiClassEsopDropDown, false, 'esop', 'data-esop-available', iElemTotalUnavailed);
                        var oEsopData = uiCurrentElem.data('full_data');
                        uiClassEsopDropDown.find('.option.esop[data-value="'+oEsopData.id+'"]')
                            .attr({
                                'data-esop-grant-date': oEsopData.grant_date,
                                'data-esop-price-per-share': oEsopData.price_per_share,
                                'data-esop-vesting-year': oEsopData.vesting_years,
                                'data-esop-currency': oEsopData.currency
                            });
                    }
                }
            });
        },

        /**
         * render_pie_chart
         * @description This function will compute the percentage and change the pie chart in view esop batch page and also in hr department dashboard
         * @dependencies N/A
         * @param {object} oDepartmentData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        render_pie_chart : function(iTotalSharesAllotedBatches, iTotalSharesAvailedBatches, uiContainer, bRenderImmediately){
            if(cr8v.var_check(iTotalSharesAllotedBatches) && cr8v.var_check(iTotalSharesAvailedBatches) && cr8v.var_check(uiContainer))
            {
                var iTotalSharesUnvailedBatches = parseFloat(cr8v.remove_commas(number_format(iTotalSharesAllotedBatches, 2))) - parseFloat(cr8v.remove_commas(number_format(iTotalSharesAvailedBatches, 2)));
                var iPercentageUnavailed = (parseFloat(iTotalSharesUnvailedBatches) / parseFloat(cr8v.remove_commas(number_format(iTotalSharesAllotedBatches, 2)))) * 100;
                var iPercentageAvailed = parseFloat(100) - parseFloat(cr8v.remove_commas(number_format(iPercentageUnavailed, 2)));
                var iPercentageUnavailedPie = Math.ceil(iPercentageUnavailed);
                var iPercentageAvailedPie = Math.ceil(iPercentageAvailed);

                var uiChartDataContainer = uiContainer.find('.data-container');
                uiChartDataContainer.find('span').remove();

                // var iPercentageUnavailed = 100;
                // var iPercentageAvailed = 0;

                uiChartDataContainer.append('<span data-value="'+cr8v.remove_commas(number_format(iPercentageAvailedPie, 2))+'%" class="">Total Shares Availed : <strong>'+number_format(iPercentageAvailed, 2)+'%</strong></span>');
                uiChartDataContainer.append('<span data-value="'+cr8v.remove_commas(number_format(iPercentageUnavailedPie, 2))+'%" class="margin-top-30">Total Shares Unavailed : <strong>'+number_format(iPercentageUnavailed, 2)+'%</strong></span>');

                if(cr8v.var_check(bRenderImmediately) && bRenderImmediately)
                {
                    initialize_pie_chart(uiContainer);
                }
            }
        },

        /**
         * populate_edit_info
         * @description This function will put the data of the esop needed on the edit info
         * @dependencies N/A
         * @param {object} oDepartmentData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        populate_edit_info : function(oEsopData, uiForm){
            if(cr8v.var_check(oEsopData) && cr8v.var_check(uiForm))
            {
                var iCurrencyID = oEsopData.currency;
                var sGrantDate = moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MM/DD/YYYY');

                uiForm.attr('data-esop-id', oEsopData.id);
                uiForm.attr('data-esop-name', oEsopData.name);
                uiForm.attr('data-esop-grant-date', oEsopData.grant_date);
                uiForm.attr('data-esop-total-share-qty', oEsopData.total_share_qty);
                uiForm.attr('data-esop-price-per-share', oEsopData.price_per_share);
                uiForm.attr('data-esop-vesting-year', oEsopData.vesting_years);

                uiForm.find('[name="esop_name"]').val(oEsopData.name);
                uiForm.find('[name="grant_date"]').val(sGrantDate);
                uiForm.find('[name="total_share_qty"]').val(number_format(oEsopData.total_share_qty, 2));
                uiForm.find('[name="price_per_share"]').val(oEsopData.price_per_share);
                uiForm.find('[name="vesting_years"]').val(oEsopData.vesting_years);

                uiCurrencyDropdown.find('[data-value="'+iCurrencyID+'"]').trigger('click');

                return uiForm;
            }
        },

        /**
         * manipulate_esop_data
         * @description This function will put the data of the esop view
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_esop_data : function(oEsopData, uiTemplate){
            if(cr8v.var_check(oEsopData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oEsopData.currency_name) ? oEsopData.currency_name : roxas.esop.get_currency_name(oEsopData.currency) ;
                var sPricePerShare = sCurrencyName +' '+ oEsopData.price_per_share;
                var sGrantDate = moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('data-esop-id', oEsopData.id);
                uiTemplate.attr('data-esop-name', oEsopData.name);
                uiTemplate.attr('data-esop-grant-date', oEsopData.grant_date);
                uiTemplate.attr('data-esop-total-share-qty', oEsopData.total_share_qty);
                uiTemplate.attr('data-esop-price-per-share', oEsopData.price_per_share);
                uiTemplate.attr('data-esop-vesting-year', oEsopData.vesting_years);
                uiTemplate.attr('data-esop-currency', oEsopData.currency);

                uiTemplate.find('[data-label="esop_name"]').text(oEsopData.name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oEsopData.total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiTemplate.find('[data-label="vesting_years"]').text(oEsopData.vesting_years);

                uiCreateEsopBatchModal.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiUploadBatchShareDistributionModal.attr('data-esop-id', oEsopData.id);

                return uiTemplate;
            }
        },

        /**
         * manipulate_template_list
         * @description This function will put the data of the esop list view
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template_list : function(oEsopData, uiTemplate){
            if(cr8v.var_check(oEsopData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oEsopData.currency_name) ? oEsopData.currency_name : roxas.esop.get_currency_name(oEsopData.currency) ;
                var sPricePerShare = sCurrencyName +' '+ oEsopData.price_per_share;
                var sGrantDate = moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('is_letter_sent', false);
                uiTemplate.attr('data-esop-id', oEsopData.id);
                uiTemplate.attr('data-esop-name', oEsopData.name);
                uiTemplate.attr('data-esop-grant-date', oEsopData.grant_date);
                uiTemplate.attr('data-esop-total-share-qty', oEsopData.total_share_qty);
                uiTemplate.attr('data-esop-price-per-share', oEsopData.price_per_share);
                uiTemplate.attr('data-esop-vesting-year', oEsopData.vesting_years);

                uiTemplate.find('[data-label="esop_name"]').text(oEsopData.name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oEsopData.total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiTemplate.find('[data-label="vesting_years"]').text(oEsopData.vesting_years);

                if(cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0)
                {
                    if(cr8v.var_check(oEsopData.offer_expiration) && oEsopData.offer_expiration != '0000-00-00')
                    {
                        if(cr8v.var_check(oEsopData.letter_sent) && oEsopData.letter_sent > 0)
                        {
                            uiTemplate.attr('is_letter_sent', true);
                        }
                    }
                }

                return uiTemplate;
            }
        },

        /**
         * manipulate_template_grid
         * @description This function will put the data of the esop list view
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template_grid : function(oEsopData, uiTemplate){
            if(cr8v.var_check(oEsopData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oEsopData.currency_name) ? oEsopData.currency_name : roxas.esop.get_currency_name(oEsopData.currency) ;
                var sPricePerShare = sCurrencyName +' '+ oEsopData.price_per_share;
                var sGrantDate = moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('is_letter_sent', false);
                uiTemplate.attr('data-esop-id', oEsopData.id);
                uiTemplate.attr('data-esop-name', oEsopData.name);
                uiTemplate.attr('data-esop-grant-date', oEsopData.grant_date);
                uiTemplate.attr('data-esop-total-share-qty', oEsopData.total_share_qty);
                uiTemplate.attr('data-esop-price-per-share', oEsopData.price_per_share);
                uiTemplate.attr('data-esop-vesting-year', oEsopData.vesting_years);

                uiTemplate.find('[data-label="esop_name"]').text(oEsopData.name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oEsopData.total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiTemplate.find('[data-label="vesting_years"]').text(oEsopData.vesting_years);

                if(cr8v.var_check(oEsopData.distribution) && count(oEsopData.distribution) > 0)
                {
                    if(cr8v.var_check(oEsopData.offer_expiration) && oEsopData.offer_expiration != '0000-00-00')
                    {
                        if(cr8v.var_check(oEsopData.letter_sent) && oEsopData.letter_sent > 0)
                        {
                            uiTemplate.attr('is_letter_sent', true);
                        }
                    }
                }

                return uiTemplate;
            }
        },

        /**
         * manipulate_esop_employees
         * @description This function will put the data of the esop company employees
         * @dependencies N/A
         * @param {object} oUserData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_esop_employees : function(oUserData, uiTemplate, bFromEditShares){
            if(cr8v.var_check(oUserData) && cr8v.var_check(uiTemplate))
            {
                var sFullName = '';
                if(cr8v.var_check(oUserData.full_name))
                {
                    sFullName = oUserData.full_name;
                }
                else
                {
                    var sFirstName = ((cr8v.var_check(oUserData.first_name) && oUserData.first_name.length > 0) ? oUserData.first_name+' ' : '');
                    var sMiddleName = ((cr8v.var_check(oUserData.middle_name) && oUserData.middle_name.length > 0) ? oUserData.middle_name+' ' : '');
                    var sLastName = ((cr8v.var_check(oUserData.last_name) && oUserData.last_name.length > 0) ? oUserData.last_name+' ' : '');

                    sFullName = sFirstName+''+sMiddleName+''+sLastName;
                }

                uiTemplate.attr('data-user-id', oUserData.user_id);
                uiTemplate.attr('data-user-full-name', sFullName);
                uiTemplate.attr('data-user-name', oUserData.user_name);
                uiTemplate.attr('data-user-employee-code', oUserData.employee_code);
                uiTemplate.attr('data-user-company-id', oUserData.company_id);
                uiTemplate.attr('data-user-company-code', oUserData.company_code);
                uiTemplate.attr('data-user-department-id', oUserData.department_id);
                uiTemplate.attr('data-user-department-name', oUserData.department_name);
                uiTemplate.attr('data-user-rank-id', oUserData.rank_id);
                uiTemplate.attr('data-user-rank-name', oUserData.rank_name);

                uiTemplate.find('[data-label="company_code"]').text(oUserData.company_code);
                uiTemplate.find('[data-label="employee_code"]').text(oUserData.employee_code);
                uiTemplate.find('[data-label="department_name"]').text(oUserData.department_name);
                uiTemplate.find('[data-label="full_name"]').text(sFullName);
                uiTemplate.find('[data-label="rank_name"]').text(oUserData.rank_name);
                uiTemplate.find('[data-label="employee_alloted_shares"]').text(number_format(oUserData.employee_alloted_shares, 2));

                if(cr8v.var_check(oUserData.accepted) && oUserData.accepted != '0.00')
                {
                    uiTemplate.find('[data-label="employee_shares_availed"]').text(number_format(oUserData.accepted, 2));
                }
                else
                {
                    uiTemplate.find('.go_to_view_employee_by_esop_batch').addClass('hidden');
                }

                var iEsopID = $('[data-container="esop_information"]').attr('data-esop-id');

                if(cr8v.var_check(bFromEditShares) && bFromEditShares)
                {
                    if(cr8v.var_check(oUserData.user_stock_offers) && count(oUserData.user_stock_offers) > 0)
                    {
                        for(var i in oUserData.user_stock_offers)
                        {
                            var oUserStockOffer = oUserData.user_stock_offers[i];

                            if(cr8v.var_check(oUserStockOffer.esop_id) && oUserStockOffer.esop_id == iEsopID)
                            {
                                if(cr8v.var_check(oUserStockOffer.employee_alloted_shares))
                                {
                                    uiTemplate.find('[name="employee_alloted_shares"]').val(oUserStockOffer.employee_alloted_shares);
                                }
                            }
                        }
                    }
                }

                return uiTemplate;
            }
        },

        /**
         * manipulate_esop_companies
         * @description This function will put the data of the esop list view
         * @dependencies N/A
         * @param {object} oEsopData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_esop_companies : function(oCompanyData, uiTemplate, bFromEditShares, bFromView){
            if(cr8v.var_check(oCompanyData) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.attr('data-company-id', oCompanyData.company_id);
                uiTemplate.attr('data-company-name', oCompanyData.company_name);
                uiTemplate.attr('data-company-allotment', oCompanyData.company_allotment);

                uiTemplate.find('[data-label="company_name"]').text(oCompanyData.company_name);

                var iEsopID = $('[data-container="esop_information"]').attr('data-esop-id');
                
                if(cr8v.var_check(oCompanyData.company_allotment))
                {
                    if(cr8v.var_check(bFromEditShares) && bFromEditShares)
                    {
                        for(var k in oCompanyData.employees)
                        {
                            var oUserData = oCompanyData.employees[k];

                            if(cr8v.var_check(oUserData.user_stock_offers) && count(oUserData.user_stock_offers) > 0)
                            {
                                for(var i in oUserData.user_stock_offers)
                                {
                                    var oUserStockOffer = oUserData.user_stock_offers[i];

                                    if(cr8v.var_check(oUserStockOffer.esop_id) && oUserStockOffer.esop_id == iEsopID)
                                    {
                                        if(cr8v.var_check(oUserStockOffer.company_allotment))
                                        {
                                            uiTemplate.find('[name="company_alloted_shares"]').val(oUserStockOffer.company_allotment);
                                            uiTemplate.find('[data-label="company_alloted_shares"]').text(number_format(oUserStockOffer.company_allotment, 2));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if(cr8v.var_check(bFromView) && bFromView)
                        {
                            uiTemplate.find('[name="company_alloted_shares"]').val(oCompanyData.company_allotment);
                            uiTemplate.find('[data-label="company_alloted_shares"]').text(number_format(oCompanyData.company_allotment, 2));
                        }
                    }
                }

                return uiTemplate;
            }
        },

        /**
         * bind_search_event
         * @description This function is for binding search event
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_search_event : function(){
            
            uiEsopSearchDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiStockOfferSearchDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);

            uiSearchEsopBtn.on('click', function(){
                var sSearchField = uiEsopSearchDropdown.find('.frm-custom-dropdown-txt').find('input').val();
                if(sSearchField == 'ESOP Name')
                {
                    if(uiSearchEsopnameInput.val().length > 0)
                    {
                        roxas.esop.assemble_search_esop_params(uiSearchEsopBtn, sSearchField, uiSearchEsopnameInput.val());
                    }
                }
                else if(sSearchField == 'Vesting Years')
                {
                    if(uiSearchEsopVestingYearsInput.val().length > 0)
                    {
                        roxas.esop.assemble_search_esop_params(uiSearchEsopBtn, sSearchField, uiSearchEsopVestingYearsInput.val());
                    }
                }
                else if(sSearchField == 'Grant Date')
                {
                    var sDateFrom = uiSearchEsopGrantFromInput.val();
                    var bValidGrantDateFrom = cr8v.validate_date(sDateFrom, 'MM/DD/YYYY');

                    var sDateTo = uiSearchEsopGrantToInput.val();
                    var bValidGrantDateTo = cr8v.validate_date(sDateTo, 'MM/DD/YYYY');

                    if(bValidGrantDateFrom && bValidGrantDateTo)
                    {
                        roxas.esop.assemble_search_esop_params(uiSearchEsopBtn, sSearchField);
                    }
                }
                else if(sSearchField == 'Price per Share')
                {
                    if(uiSearchEsopPricePerShareinput.val().length > 0)
                    {
                        roxas.esop.assemble_search_esop_params(uiSearchEsopBtn, sSearchField, uiSearchEsopPricePerShareinput.val());
                    }
                }
            });

            uiSearchEsopnameInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchEsopnameInput.siblings('.search_esop_btn').trigger('click');
                }
            });

            uiSearchEsopVestingYearsInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchEsopVestingYearsInput.siblings('.search_esop_btn').trigger('click');
                }
            });

            uiSearchEsopGrantFromInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchEsopGrantFromInput.siblings('.search_esop_btn').trigger('click');
                }
            });

            uiSearchEsopGrantToInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchEsopGrantToInput.siblings('.search_esop_btn').trigger('click');
                }
            });

            uiSearchEsopPricePerShareinput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchEsopPricePerShareinput.siblings('.search_esop_btn').trigger('click');
                }
            });

            uiSearchStockOfferBtn.on('click', function(){
                var sSearchField = uiStockOfferSearchDropdown.find('.frm-custom-dropdown-txt').find('input').val();
                if(sSearchField == 'ESOP Name')
                {
                    if(uiSearchStockOffernameInput.val().length > 0)
                    {
                        roxas.esop.assemble_search_stock_offer_params(uiSearchStockOfferBtn, sSearchField, uiSearchStockOffernameInput.val());
                    }
                }
                else if(sSearchField == 'Vesting Years')
                {
                    if(uiSearchStockOfferVestingYearsInput.val().length > 0)
                    {
                        roxas.esop.assemble_search_stock_offer_params(uiSearchStockOfferBtn, sSearchField, uiSearchStockOfferVestingYearsInput.val());
                    }
                }
                else if(sSearchField == 'Grant Date')
                {
                    var sDateFrom = uiSearchStockOfferGrantFromInput.val();
                    var bValidGrantDateFrom = cr8v.validate_date(sDateFrom, 'MM/DD/YYYY');

                    var sDateTo = uiSearchStockOfferGrantToInput.val();
                    var bValidGrantDateTo = cr8v.validate_date(sDateTo, 'MM/DD/YYYY');

                    if(bValidGrantDateFrom && bValidGrantDateTo)
                    {
                        roxas.esop.assemble_search_stock_offer_params(uiSearchStockOfferBtn, sSearchField);
                    }
                }
                else if(sSearchField == 'Price per Share')
                {
                    if(uiSearchStockOfferPricePerShareinput.val().length > 0)
                    {
                        roxas.esop.assemble_search_stock_offer_params(uiSearchStockOfferBtn, sSearchField, uiSearchStockOfferPricePerShareinput.val());
                    }
                }
            });

            uiSearchStockOffernameInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchStockOffernameInput.siblings('.search_stock_offer_btn').trigger('click');
                }
            });

            uiSearchStockOfferVestingYearsInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchStockOfferVestingYearsInput.siblings('.search_stock_offer_btn').trigger('click');
                }
            });

            uiSearchStockOfferGrantFromInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchStockOfferGrantFromInput.siblings('.search_stock_offer_btn').trigger('click');
                }
            });

            uiSearchStockOfferGrantToInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchStockOfferGrantToInput.siblings('.search_stock_offer_btn').trigger('click');
                }
            });

            uiSearchStockOfferPricePerShareinput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchStockOfferPricePerShareinput.siblings('.search_stock_offer_btn').trigger('click');
                }
            });

            // uiFilterByCompany.off('change').on('change', function() {
            //     alert('test');
            // });

        },
        /**
         * assemble_search_stock_offer_params
         * @description This function is for assembling params for search
         * @dependencies 
         * @param {jQuery} uiButton
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_search_stock_offer_params : function(uiButton, sSearchField , sSearchValue, sSorting, sSortField){
            /*sample params*/
            /*{
                "keyword" : "string", 
                "search_field" : "string", 
                "sorting" : "string", 
                "sort_field" : "string", 
                "offset" : "int", 
                "limit" : "int"
            }*/

            var oGetEsopParams = {
                'where' : [{
                    "field" : 'uso.user_id',
                    "value" : iUserID
                }]
            };

            if(sSearchField == 'ESOP Name')
            {
                oGetEsopParams.search_field = 'esop_name';
                oGetEsopParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Vesting Years')
            {
                oGetEsopParams.search_field = 'esop_vesting_years';
                oGetEsopParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Grant Date')
            {
                var sDateFrom = moment(uiSearchStockOfferGrantFromInput.val(), 'MM/DD/YYYY').format('YYYY-MM-DD');
                var sDateTo = moment(uiSearchStockOfferGrantToInput.val(), 'MM/DD/YYYY').format('YYYY-MM-DD');

                sSearchValue = "'" + sDateFrom + "' AND '" + sDateTo + "'";

                oGetEsopParams.search_field = 'esop_grant_date';
                oGetEsopParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Price per Share')
            {
                oGetEsopParams.search_field = 'esop_price_per_share';
                oGetEsopParams.keyword = sSearchValue;
            }

            if(cr8v.var_check(sSorting))
            {
                oGetEsopParams.sorting = sSorting;
            }

            if(cr8v.var_check(sSortField))
            {
                oGetEsopParams.sort_field = sSortField;
            }

            roxas.esop.get_user_stock_offer(oGetEsopParams, uiButton);
        },

        /**
         * assemble_search_esop_params
         * @description This function is for assembling params for search
         * @dependencies 
         * @param {jQuery} uiButton
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_search_esop_params : function(uiButton, sSearchField , sSearchValue, sSorting, sSortField){
            /*sample params*/
            /*{
                "keyword" : "string", 
                "search_field" : "string", 
                "sorting" : "string", 
                "sort_field" : "string", 
                "offset" : "int", 
                "limit" : "int"
            }*/

            var oGetEsopParams = {
                'where' : [{
                    "field" : 'e.parent_id',
                    "value" : 0
                }]
            };

            if(sSearchField == 'ESOP Name')
            {
                oGetEsopParams.search_field = 'name';
                oGetEsopParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Vesting Years')
            {
                oGetEsopParams.search_field = 'vesting_years';
                oGetEsopParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Grant Date')
            {
                var sDateFrom = moment(uiSearchEsopGrantFromInput.val(), 'MM/DD/YYYY').format('YYYY-MM-DD');
                var sDateTo = moment(uiSearchEsopGrantToInput.val(), 'MM/DD/YYYY').format('YYYY-MM-DD');

                sSearchValue = "'" + sDateFrom + "' AND '" + sDateTo + "'";

                oGetEsopParams.search_field = 'grant_date';
                oGetEsopParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Price per Share')
            {
                oGetEsopParams.search_field = 'price_per_share';
                oGetEsopParams.keyword = sSearchValue;
            }

            if(cr8v.var_check(sSorting))
            {
                oGetEsopParams.sorting = sSorting;
            }

            if(cr8v.var_check(sSortField))
            {
                oGetEsopParams.sort_field = sSortField;
            }


            /*if personal stocks*/
            if(window.location.href.indexOf('esop/personal_stocks') > -1){
                oGetEsopParams.where.push(
                    {
                        'field' : 'ue.user_id',
                        'value' : iUserID
                    }
                )

                oGetEsopParams.group_by = "esop_id";
            }


            roxas.esop.get_esop(oGetEsopParams, uiButton);
        },

        /**
         * get_currency
         * @description This function is for getting and rendering currency on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_currency : function(){
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : {'data' : []},
                "url"    : roxas.config('url.server.base') + "esop/get_currency",
                "beforeSend": function () {

                },
                "success": function (oData) {
                    if(cr8v.var_check(oData))
                    {
                        if(oData.status == true)
                        {
                            oCurrencyName = oData.data;
                            uiCurrencyDropdown.find('div.option').remove();
                            uiClassCurrencyDropdown.find('div.option').remove();
                            for(var i = 0, max = oData.data.length ; i < max; i++)
                            {
                                var oCurrencyData = oData.data[i];

                                var uiTemplate = uiCurrencyDropdown;
                                cr8v.append_dropdown(oCurrencyData, uiTemplate, false, 'currency');
                                cr8v.append_dropdown(oCurrencyData, uiClassCurrencyDropdown, false, 'currency');

                            }
                            uiCurrencyDropdown.find('.option:first').trigger('click');
                            uiClassCurrencyDropdown.find('.option:first').trigger('click');
                        }
                    }
                },
                "complete": function () {

                }
            };

            roxas.esop.ajax(oAjaxConfig);
        },

        /**
         * get_payment_method
         * @description This function is for getting and rendering payment method on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         */
        get_payment_method : function(){
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : {'data' : []},
         
                "url"    : roxas.config('url.server.base') + "esop/get_payment_method",
                "beforeSend": function () {

                },
                "success": function (oData) {
                    //console.log(oData.data);

                     if(oData.status == true)
                            {
                                if(typeof uiEsopDropdown != 'undefined')
                                {
                                    uiEsopDropdown.find('div.option').remove();
                                }
                                
                                
                                for(var x in oData.data)
                                {
                                    var oEsopData = oData.data[x];
                                    var uiTemplate = uiPaymentMethodDropdown;
                                    cr8v.append_dropdown(oEsopData, uiTemplate, false, "esop");
                                }


                     }

                    if(cr8v.var_check(oData))
                    {
                        var oPayment = [{ "id" : 0, "name" : "no payment method yet" }];

                        if(oData.status == true)
                        {
                            oPaymentName = oData.data;
                            oPayment = oData.data;
                        }

                        uiPaymentDropdown.find('div.option').remove();
                        uiClassPaymentDropdown.find('div.option').remove();
                        for(var i = 0, max = oPayment.length ; i < max; i++)
                        {
                            var uiTemplate = uiPaymentDropdown;
                            cr8v.append_dropdown(oPayment[i], uiTemplate, false, 'payment_type');
                            cr8v.append_dropdown(oPayment[i], uiClassPaymentDropdown, false, 'payment_type');

                        }
                        uiPaymentDropdown.find('.option:first').trigger('click');
                        uiClassPaymentDropdown.find('.option:first').trigger('click');

                        if(oPayment[0]['name'] == 'no payment method yet'){
                            var uiSelectDropdown = uiPaymentDropdown.find('.frm-custom-dropdown'),
                                uiCurrencyCustomDropdown = uiCurrencyDropdown.find('.frm-custom-dropdown'),
                                uiBtn = $('.btn-add-payment-record'),
                                uiInput = $('[name="amount_paid"]');

                            uiSelectDropdown.css("pointer-events", "none");
                            uiCurrencyCustomDropdown.css("pointer-events", "none");
                            uiInput.css("cursor", "not-allowed");
                            uiInput.prop('disabled', true);
                            uiBtn.prop('disabled', true);
                            uiPaymentDropdown.css("cursor", "not-allowed");
                            uiCurrencyDropdown.css("cursor", "not-allowed");
                        }
                    }
                },
                "complete": function () {

                }
            };

            roxas.esop.ajax(oAjaxConfig);
        },

        /**
         * get_offer_letter
         * @description This function is for getting and rendering offer letters on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_offer_letter : function(oParams){
            if(cr8v.var_check(oParams))
            {            
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "settings/get_offer_letters",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiOfferLetterDropdown.find('div.option').remove();
                                uiAcceptanceLetterDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oLetterData = oData.data[i];

                                    if(oLetterData.type == 1)
                                    {
                                        var uiTemplate = uiOfferLetterDropdown;
                                        cr8v.append_dropdown(oLetterData, uiTemplate, false, 'offer_letter');
                                    }
                                    else if(oLetterData.type == 2)
                                    {
                                        var uiTemplate = uiAcceptanceLetterDropdown;
                                        cr8v.append_dropdown(oLetterData, uiTemplate, false, 'acceptance_letter');
                                    }
                                    else if(oLetterData.type == 3)
                                    {
                                        
                                    }
                                }

                                uiOfferLetterDropdown.find('.option:first').trigger('click');
                                uiAcceptanceLetterDropdown.find('.option:first').trigger('click');
                            }
                            else
                            {
                                var sErrorMessages = '<p class="font-15 error_message">There are no offer and acceptance letters yet in the system. Please add letters first.</p>';
                                var uiErrorContainer = $('[data-container="esop_information"]').find('.finalize_distribution_shares_error_message');
                                cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);

                                uiOfferLetterDropdown.find('input').val('');
                                uiAcceptanceLetterDropdown.find('input').val('');

                            }
                        }
                    },
                    "complete": function () {

                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * get_currency_name
         * @description This function is for getting currency name based on the object returned of get_currency
         * @dependencies N/A
         * @param {int} iCurrencyID
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_currency_name : function(iCurrencyID)
        {
            var sReturn = '';
            if(cr8v.var_check(iCurrencyID))
            {
                for(var i = 0, max = oCurrencyName.length ; i < max; i++)
                {
                    var sCurrencyData = oCurrencyName[i];
                    if(sCurrencyData.id == iCurrencyID)
                    {
                        sReturn = sCurrencyData.name;
                    }
                }
            }
            return sReturn;
        },

        /**
         * get_users_grouped_by_company
         * @description This function is for getting and rendering users
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_users_grouped_by_company : function(oParams, uiThis, bFromEditShares, bForAddNewEmployee){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "users/get_users_grouped_by_company",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }

                        if(window.location.href.indexOf('esop/add_distribute_shares') > -1 && localStorage.getItem('esop_id') != null)
                        {
                            $("body").css({overflow:'hidden'});

                            $("#show-loading-data-shares").addClass("showed");
        
                            $("#show-loading-data-shares .close-me").on("click",function(){
                                $("body").css({'overflow-y':'initial'});
                                $("#show-loading-data-shares").removeClass("showed");
                            });
                             
                        }
                    },
                    "success": function (oData) {
                        //console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bForAddNewEmployee) && bForAddNewEmployee)
                                {
                                    for(var i in oData.data)
                                    {
                                        var oCompanyData = oData.data[i];

                                        cr8v.append_dropdown(oCompanyData, uiClassCompanyDropdown, false, 'companies', 'data-company-id', oCompanyData.company_id);

                                        uiClassCompanyDropdown.find('.option.companies[data-value="'+oCompanyData.id+'"]')
                                            .attr({
                                                'data-company-offer-id': oCompanyData.offer_letter_id,
                                                'data-company-acceptance-id': oCompanyData.acceptance_letter_id,
                                                'data-company-allotment': oCompanyData.company_allotment
                                            });

                                        for(var k in oCompanyData.employees)
                                        {
                                            var oUserData = oCompanyData.employees[k];
                                            cr8v.append_dropdown(oUserData, uiClassEmployeeDropDown, true, 'employees', 'data-company-id', oCompanyData.company_id);
                                        }
                                    }
                                }
                                else
                                {
                                    for(var i in oData.data)
                                    {
                                        /*populate companies first*/
                                        var oCompanyData = oData.data[i];
                                        var uiCompanyTemplate = uiEsopCompaniesContainer.find('.esop_companies.template.hidden').clone().removeClass('template hidden');
                                        var uiManipulatedCompanyTemplate = roxas.esop.manipulate_esop_companies(oCompanyData, uiCompanyTemplate, bFromEditShares);
                                        uiEsopCompaniesContainer.append(uiManipulatedCompanyTemplate);
                                        uiManipulatedCompanyTemplate.find('[name="company_alloted_shares"]').trigger('change');

                                        if(cr8v.var_check(oCompanyData.offer_letter_id) && cr8v.var_check(bFromEditShares) && bFromEditShares)
                                        {
                                            uiManipulatedCompanyTemplate.find('.offer_letter_dropdown').find('.option.offer_letter[data-value="'+oCompanyData.offer_letter_id+'"]').trigger('click');
                                        }
                                        if(cr8v.var_check(oCompanyData.acceptance_letter_id) && cr8v.var_check(bFromEditShares) && bFromEditShares)
                                        {
                                            uiManipulatedCompanyTemplate.find('.acceptance_letter_dropdown').find('.option.acceptance_letter[data-value="'+oCompanyData.acceptance_letter_id+'"]').trigger('click');
                                        }
                                        
                                        for(var k in oCompanyData.employees)
                                        {
                                            /*then populate the employees in that company*/
                                            var oUserData = oCompanyData.employees[k];
                                            var uiEmployeeTemplate = uiManipulatedCompanyTemplate.find('.esop_employees.template.hidden').clone().removeClass('template hidden');
                                            var uiManipulatedEmployeeTemplate = roxas.esop.manipulate_esop_employees(oUserData, uiEmployeeTemplate, bFromEditShares);
                                            uiManipulatedCompanyTemplate.find('[data-container="esop_employees_container"]').append(uiManipulatedEmployeeTemplate);
                                            uiManipulatedCompanyTemplate.find('[name="employee_alloted_shares"]').trigger('blur');
                                        }
                                    }

                                    cr8v.input_numeric($('input[name="company_alloted_shares"]'));
                                    cr8v.input_numeric($('input[name="employee_alloted_shares"]'));
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }

                        if(window.location.href.indexOf('esop/add_distribute_shares') > -1 && localStorage.getItem('esop_id') != null)
                        {
                           $(".modal-close.close-me").trigger("click");
                        }
                        
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * get_user_stock_offer
         * @description This function is for getting and rendering esop on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_user_stock_offer : function(oParams, uiThis, bFromViewOffer){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/get_user_stock_offer",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        //  console.log('drukanor');

                        

                        if(cr8v.var_check(oData))
                        {
                            uiEsopGridContainer.find('.esop_grid:not(.template)').remove();
                            uiEsopListContainer.find('.esop_list:not(.template)').remove();
                            uiNoResultsFound.addClass('template hidden');

                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bFromViewOffer) && bFromViewOffer)
                                {
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i < max; i++)
                                        {
                                            var oUserStockOfferData = oData.data[i];
                                            
                                            $('[data-container="esop_information"]').find('[data-label="esop_name"]').text(oUserStockOfferData.esop_name);

                                            var uiTemplateList = $('[data-container="esop_information"]');
                                            var uiManipulatedTemplateList = roxas.esop.manipulate_template_stock_offer_list(oUserStockOfferData, uiTemplateList);
                                            
                                            uiOfferLetterHTMLContainer.html(oUserStockOfferData.offer_letter_content);
                                            uiAcceptanceLetterHTMLContainer.html(oUserStockOfferData.acceptance_letter_content);

                                            var oParams = {"esop_id" : oUserStockOfferData.esop_id};
                                            roxas.esop.check_offer_expiration(oParams, function(oData){
                                                if(oData.status == true)
                                                {
                                                    $('[data-container="unexpired_offer"]').removeClass('hidden');
                                                }
                                                else
                                                {
                                                    $('[data-container="unexpired_offer"]').addClass('hidden');
                                                }
                                            });
                                        }
                                    }
                                    else
                                    {
                                        uiNoResultsFound.removeClass('template hidden');
                                    }
                                }
                                else
                                {                                    
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i < max; i++)
                                        {
                                            var oUserStockOfferData = oData.data[i];
                                            
                                            $('[data-container="esop_information"]').find('[data-label="esop_name"]').text(oUserStockOfferData.esop_name);

                                            var uiTemplateList = $('.esop_list.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateList = roxas.esop.manipulate_template_stock_offer_list(oUserStockOfferData, uiTemplateList);

                                            var uiTemplateGrid = $('.esop_grid.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateGrid = roxas.esop.manipulate_template_stock_offer_grid(oUserStockOfferData, uiTemplateGrid);

                                            uiEsopListContainer.prepend(uiManipulatedTemplateList);
                                            uiEsopGridContainer.prepend(uiManipulatedTemplateGrid);
                                        }
                                    }
                                    else
                                    {
                                        uiNoResultsFound.removeClass('template hidden');
                                    }
                                }
                            }
                            else
                            {
                                uiNoResultsFound.removeClass('template hidden');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * manipulate_template_stock_offer_list
         * @description This function will put the data of the esop list view
         * @dependencies N/A
         * @param {object} oUserStockOfferData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template_stock_offer_list : function(oUserStockOfferData, uiTemplate){
            if(cr8v.var_check(oUserStockOfferData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oUserStockOfferData.esop_currency_name) ? oUserStockOfferData.esop_currency_name : roxas.esop.get_currency_name(oUserStockOfferData.esop_currency) ;
                var sPricePerShare = sCurrencyName +' '+ oUserStockOfferData.esop_price_per_share;
                var sGrantDate = moment(oUserStockOfferData.esop_grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('data-stock-offer-id', oUserStockOfferData.id);
                uiTemplate.attr('data-stock-offer-esop-id', oUserStockOfferData.esop_id);
                uiTemplate.attr('data-stock-offer-esop-parent-id', oUserStockOfferData.esop_parent_id);
                uiTemplate.attr('data-stock-offer-esop-name', oUserStockOfferData.esop_name);
                uiTemplate.attr('data-stock-offer-esop-grant-date', oUserStockOfferData.esop_grant_date);
                uiTemplate.attr('data-stock-offer-esop-total-share-qty', oUserStockOfferData.esop_total_share_qty);
                uiTemplate.attr('data-stock-offer-esop-price-per-share', oUserStockOfferData.esop_price_per_share);
                uiTemplate.attr('data-stock-offer-esop-vesting-year', oUserStockOfferData.esop_vesting_years);
                uiTemplate.attr('data-stock-offer-stock-amount', oUserStockOfferData.stock_amount);

                uiTemplate.find('[data-label="esop_name"]').text(oUserStockOfferData.esop_name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oUserStockOfferData.esop_total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiTemplate.find('[data-label="vesting_years"]').text(oUserStockOfferData.esop_vesting_years);

                return uiTemplate;
            }
        },

        /**
         * group_wide_stocks_view_bind_events
         * @description This function will delegate events in the the esop group wide stock offer -esop view
         * @dependencies N/A
         * @param N/A
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality NORMAL
         * @software_architect N/A
         * @developer Chummy Christian Galvan
         * @method_id N/A
         */
        group_wide_stocks_view_bind_events : function()
        {   
            _get_logs();
            
            var sEsopid = window.location.href.split('/').pop();

            var sValue  = isNaN(sEsopid) ?  "(1)" : "("+sEsopid+")" ;

            function addCommas(nStr){

                 nStr += '';
                 var x = nStr.split('.');
                 var x1 = x[0];
                 var x2 = x.length > 1 ? '.' + x[1] : '';
                 var rgx = /(\d+)(\d{3})/;
                 while (rgx.test(x1)) {
                  x1 = x1.replace(rgx, '$1' + ',' + '$2');
                 }
                 return x1 + x2;
            }

            function _get_logs (argument) {
                var oGetLogsParams = {
                    "where" : [
                        {
                            "field" : "ref_id",
                            "operator" : "IN",
                            "value" : sValue
                        },
                        {
                            "field" : "type",
                            "operator" : "IN",
                            "value" : "('esop-group')"
                        }
                    ]
                };
               cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
            }

            $.fn.CreatePagination = function( uiTable, oOptions )
            {   

                        //Default Configurations
                        var defaults = 
                        {
                            limit: {
                                row: 10,
                                paginations : 6
                            },
                            class : {
                                table  : '.table-roxas',
                                filter : '.filtered'
                            },
                            color       : {
                                tr: {
                                    'even' : 'even',
                                    'odd'  : 'odd'
                                },
                                pagination : {
                                    'active' : 'active'
                                }
                            },
                            onFadeIn : false
                        },

                        uiTableParent,
                        uiTableRows,
                        uiThis;

                        //Merge the given defaults to the original defaults
                        $.extend(defaults, oOptions || {} );

                        //Check if the passed string or element is a table if not then we specify the table by the given defaults
                        $(uiTable).is('table') ? ( uiTableParent = $(uiTable) ) : (uiTableParent = $(this).find('table'+defaults.class.table));
                        (defaults.onFadeIn) ? uiTableParent.hide() : uiTableParent.show();
                        uiThis      = $(this);
                        console.log(uiTableParent);
                        // Table rows
                        uiTableRows = uiTableParent.find('tbody tr:not(.no-results)');

                        var method = {
                            initialize    : function() {
                                method.draw();
                                method.delegate();
                                uiThis.trigger('table.paginate');
                                (defaults.onFadeIn) ? uiTableParent.fadeIn() : uiTableParent.show();
                               
                            },
                            _currentIndex : function()
                            {
                                if(typeof uiTableParent.data('current-index') == 'undefined')
                                {
                                    uiTableParent.data('current-index', 1);
                                }
                                return isNaN(uiTableParent.data('current-index')) ? 1 : uiTableParent.data('current-index');
                            },
                            _registerIndex : function(sIndex)
                            {
                                uiTableParent.data('current-index',sIndex);
                            },
                            draw : function()
                            {

                                //Lets create the template
                                var uiTemplatePaginationParent     = method._createElement('div', {'class' : 'paganation-container'}),
                                    uiTemplatePaginationContent    = method._createElement('div',{'class': 'paganation-content'}),
                                    uiTemplateSelectorHolder       = method._createElement('div',{'class': 'btn-page-selector-holder'}),
                                    uiTemplateInputHolder          = method._createElement('div',{'class':'input-holder'}),
                                    uiTemplateInputHolderCaret     = method._createElement('div',{'class':'input-caret'}),
                                    uiTemplateOptionsHolder        = method._createElement('div',{'class':'options-holder'}),
                                    uiTemplateInput                = method._createElement('input');

                                    uiTemplateInput.val('Page '+1).appendTo(uiTemplateInputHolder);
                                    uiTemplateInputHolderCaret.appendTo(uiTemplateInputHolder);
                                    uiTemplateInputHolder.appendTo(uiTemplateSelectorHolder);

                                method._checkifFiltered();

                                // The total rows in the table
                                var iRowCount   = uiTableRows.length,

                                    iTotalPages = Math.ceil(iRowCount / defaults.limit.row);


                                for (var i = 1; i <= iTotalPages; i++) {
                                    var uiTemplateOptions = method._createElement('div',{'class':'options','text': i });
                                    uiTemplateOptions.appendTo(uiTemplateOptionsHolder);
                                };

                                iTotalPages <= 7 ? uiTemplateOptionsHolder.css({'top':'auto','box-shadow':'none','overflow':'inherit'}) : uiTemplateOptionsHolder.removeAttr('style');
                                
                                    
                               
                                uiTemplateOptionsHolder.appendTo(uiTemplateInputHolder);

                                var iCurrentPagesTo = method._currentIndex() * defaults.limit.row;

                                defaults.max = {pages:iTotalPages};

                                var uiTemplatePageButtonPrev        = method._createElement('div',{'class':'btn-page','text':'Prev'});  
                             

                                uiTemplateSelectorHolder.append(' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of '+iTotalPages+' pages ').appendTo(uiTemplatePaginationContent);

                                var uiTemplatePageButtonNext        = method._createElement('div',{'class':'btn-page','text':'Next'});  

                                uiTemplatePaginationParent.append(uiTemplatePaginationContent);
                               
                                uiThis.find('.paganation-container').remove();

                                uiTableParent.after(uiTemplatePaginationParent);  

                            },
                            delegate : function()
                            {

                               
                                var fnOnCaretClickEvent     = function()
                                {
                                    uiThis.find('div.options-holder').toggle();
                                }

                                var fnOnOptionsClickEvent  = function()
                                {
                                    uiThis.find('div.options-holder').toggle();

                                    var uiOptions = $(this),

                                        sText  = uiOptions.text();

                                    uiThis.find('.input-holder input').val('Page ' + sText);

                                    method._registerIndex(sText);

                                    uiThis.trigger('table.paginate');
                                }

                                var fnPaginateEvent = function()
                                {
                                    var iIndex = method._currentIndex(),
                                        iTo    = iIndex * defaults.limit.row,
                                        iFrom  = iTo - (defaults.limit.row -1 );

                                    uiTableRows.removeClass('odd').removeClass('even');

                                    method._checkifFiltered();
                                    uiTableRows.each(function(i,o){

                                            var index = i+1;

                                            if( index >= iFrom && index <= iTo)
                                            {
                                                $(this).removeClass('display-none');
                                               
                                            }
                                            else{
                                                $(this).addClass('display-none');
                                            }

                                    });

                                    method.paint();

                                }

                                uiThis.on('table.paginate',fnPaginateEvent);
                                uiThis.on('click','.input-caret',fnOnCaretClickEvent);
                                uiThis.on('click','.options',fnOnOptionsClickEvent);
                            },

                            paint : function(min,max)
                            {
                               method._checkifFiltered();
                               uiTableRows.removeClass(defaults.color.tr.odd).removeClass(defaults.color.tr.even);

                               uiTableRows.each(function(i,o){

                                    var index = i+1;
                                    index % 2 == 0 ? $(this).addClass(defaults.color.tr.odd) : $(this).addClass(defaults.color.tr.even) ;
                                    
                                });
                            },

                            _createElement : function(tag,oData)
                            {

                                var keys = {
                                    'id'    : 'id',
                                    'class' : 'className',
                                    'className':'className',
                                    'text':'innerHTML',
                                    'html':'innerHTML'
                                };
                                var el = document.createElement(tag.toUpperCase());

                                if(typeof oData !== 'undefined' && oData){ 

                                    for(var index in oData){
                                        el[keys[index]] = oData[index];
                                    }
                                }

                                return $(el);

                            },
                            _checkifFiltered : function()
                            {
                                if(uiTableParent.find(defaults.class.filter).length > 0)
                                {       
                                    uiTableRows = uiTableParent.find(defaults.class.filter);
                                }
                                else
                                {
                                     uiTableRows = uiTableParent.find('tbody tr');
                                }
                            }
                        }

                        this.reInitialize = function(){
                            method.draw();
                            uiThis.trigger('table.paginate');
                            return this;
                        }
                        this.rePaint   = function()
                        {
                            method.paint();
                            return this;
                        }
                        //Execute the method

                        method.initialize();

                        return this;
            }

            var Opaginate = $('section[section-style=content-panel] .content .tbl-rounded').CreatePagination('table.table-roxas');
            
            setTimeout(function(){
                var Opaginates = $('div[data-container=audit_logs]').CreatePagination('table.table-roxas-2');
            },2000);
            

            var fnTableEventListener = (function(){

                function fnDelegate()
                {

                    var fnOnEditClick = function()
                    {
                        //Invoker
                        var uiThis = $(this),
                            uiParentRow = uiThis.parents('tr'),
                            uiAcceptedSharesTd = uiParentRow.find('td:nth-child(4)');

                        uiThis.addClass('display-none').removeClass('display-block');
                        uiParentRow.find('div.save-panel').removeClass('display-none');
                        uiAcceptedSharesTd.find('input').removeClass('display-none').end()
                        .find('label').addClass('display-none').removeClass('display-block');

                    }

                    uiTableRoxas.on('click','a.a-edit',fnOnEditClick);

                    var fnOnCancelClick   = function()
                    {
                        var uiThis = $(this),
                            uiParentRow = uiThis.parents('tr'),
                            uiAcceptedSharesTd = uiParentRow.find('td:nth-child(4)');

                        uiThis.parents('div.save-panel').addClass('display-none').removeClass('display-block');

                        uiParentRow.find('a.a-edit').removeClass('display-none');

                        uiAcceptedSharesTd.find('input').addClass('display-none').removeClass('display-block').end()

                        .find('label').removeClass('display-none').addClass('display-block');

                    }

                    uiTableRoxas.on('click','a.cancel',fnOnCancelClick);

                    var fnOnSaveClick   = function()
                    {
                        var uiThis = $(this),
                            uiParentRow = uiThis.parents('tr'),
                            uiAcceptedSharesTd = uiParentRow.find('td:nth-child(4)'),
                            sLimitShares       = uiParentRow.find('td:nth-child(3)').text();
                        var iLimitShares       = sLimitShares.replace(/[Shares,]/g,'');
                        
                        uiThis.parents('div.save-panel').addClass('display-none').removeClass('display-block');
                        uiParentRow.find('a.a-edit').removeClass('display-none');
                        uiAcceptedSharesTd.find('input').addClass('display-none').removeClass('display-block').end()
                                          .find('label').removeClass('display-none').addClass('display-block');

                        var oParams = {};
                        oParams.esop_id = trim(uiParentRow.data('id'));
                        oParams.user_id = uiParentRow.data('user');
                        oParams.shares  = uiAcceptedSharesTd.find('input.shares').val().replace(/,/g,'');
                        oParams.status  = uiParentRow.find('td:nth-child(7)').text();
                        oParams.empname  = uiParentRow.find('td:nth-child(2)').text();

                        if(typeof uiParentRow.attr('data-user-esop-id') != 'undefined' && uiParentRow.attr('data-user-esop-id') != '')
                        {
                            oParams.user_esop_id = uiParentRow.attr('data-user-esop-id');
                        }
                        
                        var oAjaxConfig  = {
                            "type"   : "POST",
                            "data"   : oParams,
                            "url"    : roxas.config('url.server.base') + "esop/save_upon_edit_stock_offers",
                            success  : function(sResponse)
                            {
                                var oData   = sResponse;
                                if(oData.status == 'success')
                                {
                                    if(oData.type == 'update')
                                    {
                                         uiAcceptedSharesTd.find('label').text(uiAcceptedSharesTd.find('input').val() +' Shares');
                                    }
                                    else
                                    {
                                        uiAcceptedSharesTd.find('label').text(uiAcceptedSharesTd.find('input.shares').val() +' Shares');

                                        uiParentRow.attr('data-user-esop-id',oData.id);

                                        uiParentRow.find('td:nth-child(7)').text('Accepted');

                                        uiParentRow.removeAttr('class').addClass('tr-Accepted');
                                    }

                                    _get_logs();
                                }
                                
                            }

                        }

                        roxas.esop.ajax(oAjaxConfig);
                    }

                    uiTableRoxas.on('click','a.save',fnOnSaveClick);

                    var fnOnKeyUpInputShares = function()
                    {
                         var uiThis = $(this),
                            uiParentRow = uiThis.parents('tr'),
                            uiAcceptedSharesTd = uiParentRow.find('td:nth-child(4)'),
                            sLimitShares       = uiParentRow.find('td:nth-child(3)').text();

                         var iLimitShares       = sLimitShares.replace(/[Shares,]/g,'');

                         iLimitShares = parseFloat(iLimitShares);

                         var sValue = uiThis.val().replace(/[^0-9\.]/g,'');

                         if(sValue > iLimitShares)
                         {
                            sValue = iLimitShares;
                         }

                         sValue = addCommas(sValue);
                         uiThis.val(sValue);
                    } 

                    uiTableRoxas.on('keyup','input',fnOnKeyUpInputShares);

                    var fnOnclickCheckAll = function()
                    {
                        var uiThis = $(this);
                        if(uiThis.is(':checked'))
                        {
                            uiTableRoxas.find('input[type=checkbox]:not(:disabled)').prop('checked',true);
                        }
                        else
                        {
                            uiTableRoxas.find('input[type=checkbox]:not(:disabled)').removeProp('checked');
                        }
                    }

                    uiTableRoxas.on('click','thead input[type=checkbox]',fnOnclickCheckAll);

                    var fnOnCheckBoxesClick = function()
                    {
                        var iCount = $('.tr-Waiting').length;
                        (iCount > 0 ) ? $('.btn-normal:disabled').removeProp('disabled') : $('.btn-normal.modal-trigger').prop('disabled',true) ;
                    }
                    var uiHeader = $('.content .header-effect');

                    $('body').on('click','table.table-roxas input:checkbox',fnOnCheckBoxesClick);

                    var fnOnHeaderButtonClick = function()
                    {

                        var uiBtn      = $(this),
                            uiCheckbox = $('input[name="esop-checkbox[]"]:checked'),
                            sText      = uiBtn.text();
                        var array = [];

                        uiCheckbox.each(function(){
                            var iUserId = $(this).parents('tr').data('user');
                            var oData = {'stock_offer_id':this.value,'user_id':iUserId};
                            array.push(oData);
                        });
                       
                        if(sText == 'Download Selected Acceptance Letter')
                        {
                            var oAjaxConfig = {
                                "url"    : roxas.config('url.server.base') + "esop/download_letters",
                                'type':'POST',
                                'data': {data:JSON.stringify(array),type:'acceptance_letter_content'},
                                 success:function(sResponse)
                                 {
                                    if(sResponse.status ='success')
                                    {
                                        window.location = sResponse.url;
                                    }
                                 }
                            }
                            roxas.esop.ajax(oAjaxConfig);
                        }
                        else if(sText == 'Download Selected Offer Letter')
                        {
                             var oAjaxConfig = {
                                "url"    : roxas.config('url.server.base') + "esop/download_letters",
                                'type':'POST',
                                'data': {data:JSON.stringify(array),type:'offer_letter_content'},
                                 success:function(sResponse)
                                 {
                                    if(sResponse.status ='success')
                                    {
                                        window.location = sResponse.url;
                                    }
                                 }
                            }

                            roxas.esop.ajax(oAjaxConfig);
                        }
                        else if(sText == 'Accept Offer')
                        {
                            uiModalShowed.find('.modal-content .success').addClass('display-none').end()
                                         .find('button.btn-accept-offer').removeProp('disabled').end()
                                         .find('p.p-alert').removeClass('display-none').end()
                                         .find('.f-right *').removeClass('hidden');
                        }
                    }

                    uiHeader.on('click','button',fnOnHeaderButtonClick);

                    var uiModalShowed  = $('div.modal-container');

                    var fnOnAcceptOffer = function()
                    {
                       var  uiCheckbox = $('input[name="esop-checkbox[]"]:checked'),
                            array = [],
                            uiThis = $(this);

                       uiCheckbox.each(function(){

                            var uiRowParent = $(this).parents('tr');
                            var iUserId = uiRowParent.data('user'),
                                iEsopId = uiRowParent.data('id'),
                                iStockOffer = uiRowParent.data('stock-offer'),
                                sShares = uiRowParent.find('input.shares').val().replace(/[,]/g,'');
                                
                            var oParams = {};
                                oParams.esop_log_id = iEsopId;
                                oParams.user_id = iUserId;
                                oParams.esop_id = iEsopId;
                                oParams.stock = sShares;
                                oParams.stock_offer = iStockOffer;
                                oParams.empname  = uiRowParent.find('td:nth-child(2)').text();

                            if(uiRowParent.find('td:nth-child(7)').text() != 'Accepted')
                            {
                                array.push(oParams);
                            }
                        });

                        var sStringifiedJSON = JSON.stringify(array);
                        

                        if(array.length > 0)
                        {
                             var oAjaxConfig = {
                                "url"    : roxas.config('url.server.base') + "esop/accept_multiple_offer",
                                'type':'POST',
                                'data': { data: sStringifiedJSON },
                                 beforeSend : function  ()
                                 {
                                     cr8v.show_spinner(uiThis, true);
                                 },
                                 success:function(oResponse)
                                 {
                                    uiModalShowed.find('.modal-content .success.display-none').removeClass('display-none').end()
                                                 .find('button.btn-accept-offer').prop('disabled',true).end()
                                                 .find('p.p-alert').addClass('display-none').end()
                                                 .find('.f-right *').addClass('hidden');

                                    if(oResponse.status == 'success')
                                    {

                                        setTimeout(function(){
                                       
                                        if(uiModalShowed.hasClass('showed'))
                                        {
                                            uiModalShowed.find('.close-me').trigger('click');
                                        }
                                    }, 1000);
                                            
                                        for(var i in oResponse.data2)
                                        {
                                            var uiRow = $('tr[data-stock-offer='+oResponse.data2[i].stock_offer_id+']');
                                            
                                            uiRow.attr('data-user-esop-id',oResponse.data2[i].uso_id)
                                                 .find('label.shares').text(addCommas(oResponse.data2[i].stock) + ' Shares ').end()
                                                 .find('input.shares').val( oResponse.data2[i].stock ).end()
                                                 .find('label.gray-color').text('Accepted').end()
                                                 .find('input.shares').addClass('display-none').removeClass('display-block').end()
                                                 .find('label.shares').removeClass('display-none').addClass('display-block').end()
                                                 .find('input[type=checkbox]').removeProp('checked').prop('disabled',true);
                                            uiRow.removeAttr('class').addClass('tr-Accepted').find('a.a-edit').removeClass('display-none');
                                            fnOnCheckBoxesClick();
                                        }
                                    }
                                    
                                 },
                                 complete: function()
                                 {
                                     cr8v.show_spinner(uiThis, false);
                                 }
                            }

                            roxas.esop.ajax(oAjaxConfig);
                        }
                        else
                        {
                             $("div[modal-id~='modal_update_offer_list']").removeClass("showed");
                        }
                    }
                    uiModalShowed.on('click','button.btn-accept-offer',fnOnAcceptOffer);



                    var uiInputFilterbyStatus = $('input.dd-txt');

                    var fnOnChangeInputFilterbyStatus = function()
                    {
                        var uiThis = $(this),
                            sValue = trim(uiThis.text());
                  
                        uiTableRoxas.find('tbody tr:not(.no-results)').removeClass('display-none').removeClass('odd').removeClass('even');
                        
                        if(sValue == 'Waiting' || sValue == 'Accepted')
                        {   

                            uiTableRoxas.find('tbody tr.tr-'+sValue+':not(.no-results)').addClass('filtered').removeClass('display-none');

                            uiTableRoxas.find('tbody tr:not(.tr-'+sValue+'):not(.no-results)').removeClass('filtered').addClass('display-none');

                        }
                        else{
                            uiTableRoxas.find('tbody tr:not(.no-results)').removeClass('display-none').removeClass('filtered');
                        }   

                        if(uiTableRoxas.find('.filtered').length > 0)
                        {
                            uiTableRoxas.find('.filtered').each(function(i,o){
                                var index = i+1;
                                if(index % 2 == 0)
                                {
                                    $(this).addClass('odd');
                                }
                                else{
                                    $(this).addClass('even');
                                }
                            })
                        }

                        Opaginate.reInitialize();
                    }

                    var uiContent = $('.content');
                    uiContent.on('click','.option.ellipsis-dropdown ',fnOnChangeInputFilterbyStatus);

                    $.fn.checkTable = function()
                    {
                        var uiThis = $(this);
                        var _check = function()
                        {
                            var iCount = uiThis.find('tbody tr:not(.no-results):not(.display-none)').length;
                            uiThis.find('.no-results').hide();
                            if(iCount == 0)
                            {
                                uiThis.find('.no-results').show();
                            }

                        }
                        _check();

                    }
                }
                return {
                    'delegate'   : fnDelegate
                }
            })();
            
            fnTableEventListener.delegate();


           

            

        },  
        /**
         * group_wide_stocks_bind_events
         * @description This function will delegate events in the the esop group wide stock offer view
         * @dependencies N/A
         * @param N/A
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Chummy Christian Galvan
         * @method_id N/A
         */
        group_wide_stocks_bind_events : function()
        {
            
                 var uiTable = $('table.table-roxas'),
                        uiBtn   = $('.btn-page');

                    $.fn.updatePagination = function()
                    {
                        var uiThis = $(this);
                            
                        if(!$(uiThis).is('table')) {
                            uiTable = uiThis.find('table.table-roxas');
                        }

                        var uiPaginationContainer = uiThis.find('div.paganation-container .paganation-content');

                        var redraw  = function(iCount)
                        {
                            var uiPrev  = uiPaginationContainer.find('.btn-page:first-child'),
                                uiNext  = uiPaginationContainer.find('.btn-page:last-child');
                                uiPaginationContainer.html('');
                                uiPaginationContainer.append(uiPrev);
                                console.log(uiNext);
                                if(iCount <= 5)
                                {
                                     for (var i = 1 ; i <= iCount ; i++) {
                                        var uiBtn   = $('<div>',{'class':'btn-page','text':i});
                                        if(i == 1) uiBtn.addClass('active');
                                        uiPaginationContainer.append(uiBtn);
                                    };
                                }
                                else
                                {
                                    for (var i = 1 ; i <= 5 ; i++) {
                                        var uiBtn   = $('<div>',{'class':'btn-page','text':i});
                                        if(i == 1) uiBtn.addClass('active');
                                        
                                        uiPaginationContainer.append(uiBtn);
                                    };
                                    $('<div>',{'class':'btn-page ellipsis','text':'...'});
                                }

                                uiPaginationContainer.append(uiNext);
                                uiThis.find('tr.no-results').hide();
                        }

                        var _checkTable = function()
                        {
                            var uiTr    = uiTable.find('tr.filtered'),
                            iCount  = uiTr.length;
                            if(iCount == 0)
                            {
                                uiThis.find('tr.no-results').show();
                            }
                            else
                            {
                                var iPage   = Math.ceil(iCount/10);
                                redraw(iPage);
                            }
                        }
                        var _checkRows = function()
                        {
                            uiTable.find('tr.filtered').removeClass('odd').removeClass('even').each(function(i,o){
                                var iIndex = i+1;
                                if( iIndex % 2 == 0)
                                {
                                    $(this).addClass('odd');
                                }
                                else
                                {
                                    $(this).addClass('even');
                                }
                            })
                        }
                        _checkTable();
                        _checkRows();
                    }

                    function _filterTables(oParams)
                    {
                        
                        function onFilter(uiTr, oParams)
                        {
                            if(oParams.index == 2)
                            {
                             
                                 uiTr.each(function(){

                                    var uiThis  = $(this),
                                        uiTd_name = uiThis.find('td:nth-child('+oParams.index+')');
                                    var sTdName   = uiTd_name.text();
                                    var timeFrom  = moment(uiDateGrantedFrom.val(), 'MM/DD/YYYY').format('YYYY-MM-DD'); 
                                    var timeTo    = moment(uiDateGrantedTo.val(), 'MM/DD/YYYY').format('YYYY-MM-DD'); ;

                                    var time      = moment(sTdName, 'YYYY-MM-DD').format('YYYY-MM-DD'); 
                                    if(time >= timeFrom && time <= timeTo)
                                    {
                                         uiThis.removeClass('display_hide').addClass('filtered');
                                    }
                                    else
                                    {
                                        uiThis.addClass('display_hide').removeClass('filtered');
                                    }

                                });
                                
                            } 
                            else
                            {

                                var regex = new RegExp(oParams.regex,'g'),
                                    iItemCount = 0;

                                uiTr.each(function(){
                                    var uiThis  = $(this),
                                        td_name = uiThis.find('td:nth-child('+oParams.index+')');

                                    if(!regex.test(td_name.text().toLowerCase()))
                                    {
                                        uiThis.addClass('display_hide').removeClass('filtered');
                                    }
                                    else{
                                        if(iItemCount <= 10)
                                        {
                                            uiThis.removeClass('display_hide').addClass('filtered');
                                        }
                                        else{
                                            uiThis.addClass('filtered').addClass("display_hide");
                                        }
                                      
                                        iItemCount++;
                                    }
                                });
                            }
                            
                        }

                        var self     = this;
                        var uiSearch = $('input.search:visible'),

                            uiDateGrantedFrom = $('input.search_grant_date_from_input'),

                            uiDateGrantedTo   = $('input.search_grant_date_to_input')

                        var sValue    = uiSearch.val(),

                            sDateFrom = uiDateGrantedFrom.val(),

                            sDateTo   = uiDateGrantedTo.val();

                        var oData  = {};
                            oData.regex = trim(sValue).toLowerCase();

                        var arrItems = [];
                            arrItems["ESOP Name"] = 1;
                            arrItems["Grant Date"] = 2;
                            arrItems["Vesting Years"] = 5;
                            arrItems["Price per Share"] = 4;
                            oData.index = arrItems[oParams.type];

                        var fnTableCheck = function()
                        {
                            var uiContent  = $(this),
                                uiTr     = uiContent.find('table.table-roxas tbody tr:not(.no-results)');

                             onFilter(uiTr,oData);
                             uiContent.updatePagination();

                        }
                        $('section[section-style=content-panel] div.content').each(fnTableCheck);
                            
                    }
                    var fnSerchButtonClick = function (e)
                    {   
                        var $uiInvoker = $(this),
                            uiFilterBy = $('input.dd-txt'),
                            sValue     = uiFilterBy.val(),
                            sFilter    = uiFilterBy.val(),
                            oParams    = {};
                            oParams.type = sFilter;
                            oParams.value = sValue;

                         _filterTables(oParams)
                    }

                    var fnPaginationBtn  = function(e)
                    {
                        var uiThis    = $(this),
                            uiTable   = uiThis.parents('div.content').find('table.table-roxas'),
                            iFrom    = 0,
                            iTo    = 10;

                        var sText = $('.btn-page.active').text();

                        if(uiThis.text() == 'Previous')
                        {

                            var iActive = parseFloat(sText);
                            iActive = (iActive - 1) == 0 ? 1 : (iActive -1);
                            iTo = iActive * 10,
                            iFrom = (iTo - 9 ) == 0  ? 1 : (iTo - 9); 
                            $('.btn-page.active').removeClass('active');
                            uiThis.next('.btn-page').addClass('active');
                        }
                        else if(uiThis.text() == 'Next')
                        {  
                            var iActive = parseFloat(sText),
                                iMax    = uiThis.prev('.btn-page').text();
                                iActive = (iActive + 1) > parseFloat(iMax) ? parseFloat(iMax) : (iActive + 1);
                                iTo = iActive * 10,
                                iFrom = (iTo - 9 ) == 0  ? 1 : (iTo - 9); 

                            $('.btn-page.active').removeClass('active');
                            uiThis.prev('.btn-page').addClass('active');

                        }
                        else
                        {
                            $('.btn-page.active').removeClass('active');
                            uiThis.addClass('active');
                            var sText = uiThis.text(),
                                iTo = parseFloat(sText) * 10,
                                iFrom = (iTo - 9 ) == 0  ? 1 : (iTo - 9); 
                           
                        }   

                        uiTable.find('tbody tr:not(.no-results)').addClass('display_hide');
                        if($('.filtered').length > 0)
                        {
                            for (var i = iFrom; i <= iTo; i++) {
                                var uiFilter = $('.filtered')[i-1];
                                $(uiFilter).removeClass('display_hide');
                            };
                        }
                        else{
                            uiTable.find('tbody tr:not(.no-results):nth-child(n+'+iFrom+'):nth-child(-n+'+iTo+')').removeClass('filtered').removeClass('display_hide');
                        }
                    }

                    $('.paganation-content').on('click','.btn-page',fnPaginationBtn);
                    uiGroupWideSearchInput.on('click',fnSerchButtonClick);

        },
        /**
         * group_wide_stocks_filter_by_company
         * @description This function will filter the companies thru a dropdown.
         * @dependencies N/A
         * @response N/A
         * @criticality 
         * @software_architect N/A
         * @developer Dru Moncatar
         * @method_id N/A
         */
        group_wide_stocks_filter_by_company : function() {
            var uiSiblings = uiFilterByCompany.siblings(),
            uiCustomDropdown = uiSiblings.find('.ellipsis-dropdown'),
            uiChosenCompany = uiSiblings.find('.dd-txt'),
            uiCompanyDiv = {},
            uiAllCompanyDivs = {};

            uiCustomDropdown.off('click').on('click', function() {
                uiAllCompanyDivs = $('.content').find('[data-company-id]');
                uiCompanyDiv = $('.content').find('[data-company-id="' + $(this).data('value') + '"]');

                uiAllCompanyDivs.hide();
                uiCompanyDiv.show();

                console.log($(this).data('value'));

                if($(this).data('value') == 'All companies') {
                    uiAllCompanyDivs.show();
                } else {
                    uiAllCompanyDivs.hide();
                    uiCompanyDiv.show();
                }
            });
        },
        /**
         * manipulate_template_stock_offer_grid
         * @description This function will put the data of the esop list view
         * @dependencies N/A
         * @param {object} oUserStockOfferData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template_stock_offer_grid : function(oUserStockOfferData, uiTemplate){
            if(cr8v.var_check(oUserStockOfferData) && cr8v.var_check(uiTemplate))
            {
                var sCurrencyName = cr8v.var_check(oUserStockOfferData.esop_currency_name) ? oUserStockOfferData.esop_currency_name : roxas.esop.get_currency_name(oUserStockOfferData.esop_currency) ;
                var sPricePerShare = sCurrencyName +' '+ oUserStockOfferData.esop_price_per_share;
                var sGrantDate = moment(oUserStockOfferData.esop_grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('data-stock-offer-id', oUserStockOfferData.id);
                uiTemplate.attr('data-stock-offer-esop-id', oUserStockOfferData.esop_id);
                uiTemplate.attr('data-stock-offer-esop-parent-id', oUserStockOfferData.esop_parent_id);
                uiTemplate.attr('data-stock-offer-esop-name', oUserStockOfferData.esop_name);
                uiTemplate.attr('data-stock-offer-esop-grant-date', oUserStockOfferData.esop_grant_date);
                uiTemplate.attr('data-stock-offer-esop-total-share-qty', oUserStockOfferData.esop_total_share_qty);
                uiTemplate.attr('data-stock-offer-esop-price-per-share', oUserStockOfferData.esop_price_per_share);
                uiTemplate.attr('data-stock-offer-esop-vesting-year', oUserStockOfferData.esop_vesting_years);

                uiTemplate.find('[data-label="esop_name"]').text(oUserStockOfferData.esop_name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oUserStockOfferData.esop_total_share_qty, 2));
                uiTemplate.find('[data-label="price_per_share"]').text(sPricePerShare);
                uiTemplate.find('[data-label="vesting_years"]').text(oUserStockOfferData.esop_vesting_years);

                return uiTemplate;
            }
        },

        /**
         * check_offer_expiration
         * @description This function is for checking if esop is expired
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        check_offer_expiration : function(oParams, fnCallback, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/check_offer_expiration",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            fnCallback(oData);
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        /**
         * get_personal_stocks
         * @description This function is for getting and rendering esop on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Manuel Pereira
         * @method_id N/A
         */
        get_personal_stocks : function(oParams, uiThis, bFromViewEsop, bFromDashboard){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/get_personal_stocks",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        //  console.log(JSON.stringify(oData));
                        // var oTempVestingRights = parseInt(oData.data[0].vesting_rights);
                        // console.log(oTempVestingRights);
                        
                        if(typeof(bClaimForm) != 'undefined' && bClaimForm == true && typeof(oData.data[0]) != 'undefiend')
                        {
                            //cr8v.esop.populate_claim_form(oData.data[0]);
                        }
                        else{
                            if(cr8v.var_check(oData))
                            {
                                uiEsopGridContainer.find('.esop_grid:not(.template)').remove();
                                uiEsopListContainer.find('.esop_list:not(.template)').remove();
                                uiNoResultsFound.addClass('template hidden');


                                if(oData.status == true)
                                {
                                    if(cr8v.var_check(bFromViewEsop) && bFromViewEsop)
                                    {
                                        roxas.esop.populate_main_info(oData.data);
                                        for (var i = 0 ; i < oData.data[0].vesting_rights.length ; i++ )
                                        {
                                            if(typeof(oData.data[0].vesting_rights[i].total) != 'undefined')
                                            {
                                                // console.log('dito');
                                                cr8v.populate_data(oData.data[0].vesting_rights[i] , $('tr.template.last-content ') , $('tbody[data-container="vesting"]'))

                                            }
                                            else
                                            {
                                                // console.log('dito');
                                                var template = $('[data-template="vesting_payments"].template');
                                                template.find('span.uc_status_notes_tool_tip').attr('data-id',oData.data[0].vesting_rights[i].id);
                                                
                                                cr8v.populate_data(oData.data[0].vesting_rights[i] , $('tr.template.vesting-rights') , $('tbody[data-container="vesting"]'))
                                                
                                                var uiNoPayment = template.find('tr.no-payment');
                                                if(oData.data[0].vesting_rights[i]['payments'].length > 0)
                                                {
                                                        
                                                        uiNoPayment.hide();
                                                }
                                                else
                                                {
                                                        uiNoPayment.show();
                                                }
                                                
                                                var uiPaymentDiv = template.find('.payments-total-vesting');

                                                cr8v.populate_data(oData.data[0].vesting_rights[i] , $('[data-template="vesting_payments"].template') , $('[data-container="vesting_payments"]') , function() { $('[data-container="vesting_payments"]').attr('year_no', oData.data[0].vesting_rights[i].year_no ) })
                                                
                                                if(oData.data[0].vesting_rights[i].uc_status_notes == null)
                                                {
                                                    $('[data-container="vesting_payments"]').find('.uc_status_notes_tool_tip[data-id='+oData.data[0].vesting_rights[i].id+']').css({ 'display' : 'none' });
                                                }
                                                else
                                                {
                                                    $('[data-container="vesting_payments"]').find('.uc_status_notes_tool_tip[data-id='+oData.data[0].vesting_rights[i].id+']').attr('tt-html', '<p>Claim Status:</p><p><small>'+oData.data[0].vesting_rights[i].uc_status_notes+'</small></p>');
                                                }

                                            }
                                        }
                                        roxas.esop.populate_totals_info(oData.data)
                                        roxas.esop.populate_vesting_rights_claim(oData.data)
                                        roxas.esop.populate_summary(oData.data);
                                        roxas.esop.vesting_rights_identification();

                                    }
                                    else if(cr8v.var_check(bFromDashboard) && bFromDashboard)
                                    {
                                        if(oData.data.length > 0)
                                        {
                                            roxas.esop.manipulate_employee_dashboard(oData);
                                        }
                                        else
                                        {
                                            $('[data-container="no_personal_stocks"]').removeClass('hidden');
                                            uiPersonalStocksContainer.addClass('hidden');
                                        }
                                    }
                                    else
                                    {
                                        if(oData.data.length > 0)
                                        {
                                            for(var i = 0, max = oData.data.length ; i <= max; i++)
                                            {
                                                var oEsopData = oData.data[i];

                                                var uiTemplateList = $('.esop_list.template').clone().removeClass('template hidden');
                                                var uiManipulatedTemplateList = roxas.esop.manipulate_template_list(oEsopData, uiTemplateList);

                                                var uiTemplateGrid = $('.esop_grid.template').clone().removeClass('template hidden');
                                                var uiManipulatedTemplateGrid = roxas.esop.manipulate_template_grid(oEsopData, uiTemplateGrid);

                                                uiEsopListContainer.prepend(uiManipulatedTemplateList);
                                                uiEsopGridContainer.prepend(uiManipulatedTemplateGrid);
                                            }
                                        }
                                        else
                                        {
                                            uiNoResultsFound.removeClass('template hidden');
                                        }
                                    }
                                }
                                else
                                {
                                    uiNoResultsFound.removeClass('template hidden');
                                    $('[data-container="no_personal_stocks"]').removeClass('hidden');
                                    uiPersonalStocksContainer.addClass('hidden');
                                }
                            }
                        }


                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * manipulate_employee_dashboard
         * @description This function is for manipulating and rendering of employee dashboard
         * @dependencies N/A
         * @param {object} oData
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_employee_dashboard : function(oData){
            if(cr8v.var_check(oData))
            {
                $('[data-container="no_personal_stocks"]').addClass('hidden');

                for (var x = 0 ; x < oData.data.length ; x++ )
                {
                    var oEsopData = oData.data[x];

                    /*main esop batch tab*/
                    var uiPersonalStocksTemplate = $('.personal_stocks.template.hidden').clone().removeClass('template hidden');
                    var uiManipulatedTemplate = roxas.esop.manipulate_esop_data(oEsopData, uiPersonalStocksTemplate);
                    var uiPersonalStocksInput = $('.personal_stocks_tab_input.template.hidden').clone().removeClass('template hidden');
                    var uiPersonalStocksLabel = $('.personal_stocks_tab_label.template.hidden').clone().removeClass('template hidden');
                    var iTabNumber = cr8v.generate_random_keys(8);
                    
                    uiPersonalStocksTemplate.attr('data-attr', iTabNumber);
                    uiPersonalStocksInput.attr('data-attr', iTabNumber);
                    uiPersonalStocksLabel.attr('data-attr', iTabNumber);
                    uiManipulatedTemplate.find('.tab').attr('data-attr', iTabNumber);
                    uiManipulatedTemplate.data('full_data', oEsopData);
                    
                    uiPersonalStocksLabel.text(oEsopData.name);

                    // uiPersonalStocksContainer.find('.personal_stocks:not(.template)').remove();
                    // uiPersonalStocksTabContainer.find('.personal_stocks_tab_input:not(.template), .personal_stocks_tab_label:not(.template)').remove();

                    uiPersonalStocksTabContainer.after(uiManipulatedTemplate);
                    uiPersonalStocksTabContainer.append(uiPersonalStocksInput);
                    uiPersonalStocksTabContainer.append(uiPersonalStocksLabel);

                    var sNotAvailable = 'N/A';
                    var sCurrencyName = cr8v.var_check(oEsopData.currency) ? oEsopData.currency : '' ;

                    uiManipulatedTemplate.find('[data-label="grant_date"]').html(cr8v.var_check(oEsopData.grant_date) ? moment(oEsopData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY') : sNotAvailable);
                    uiManipulatedTemplate.find('[data-label="price_per_share"]').html(cr8v.var_check(oEsopData.price_per_share) ? sCurrencyName+' '+number_format(oEsopData.price_per_share, 2) : sNotAvailable);
                    uiManipulatedTemplate.find('[data-label="accepted"]').html(cr8v.var_check(oEsopData.accepted) ? number_format(parseFloat(oEsopData.accepted).toFixed(2), 2) : sNotAvailable);
                    uiManipulatedTemplate.find('[data-label="value"]').html(cr8v.var_check(oEsopData.accepted) ? sCurrencyName+' '+number_format(parseFloat(oEsopData.accepted * oEsopData.price_per_share).toFixed(2), 2) : sNotAvailable);

                    if(x == 0)
                    {
                        uiPersonalStocksLabel.trigger('click');
                    }

                    for (var i = 0 ; i < oEsopData.vesting_rights.length ; i++ )
                    {
                        if(typeof(oEsopData.vesting_rights[i].total) != 'undefined')
                        {
                            cr8v.populate_data(oEsopData.vesting_rights[i] , uiManipulatedTemplate.find('tr.template.last-content ') , uiManipulatedTemplate.find('tbody[data-container="vesting"]'))
                        }
                        else
                        {
                            cr8v.populate_data(oEsopData.vesting_rights[i] , uiManipulatedTemplate.find('tr.template.vesting-rights') , uiManipulatedTemplate.find('tbody[data-container="vesting"]'))
                            cr8v.populate_data(oEsopData.vesting_rights[i] , uiManipulatedTemplate.find('[data-template="vesting_payments"].template') , uiManipulatedTemplate.find('[data-container="vesting_payments"]') , function() {
                                uiManipulatedTemplate.find('[data-container="vesting_payments"]').attr('year_no', oEsopData.vesting_rights[i].year_no ) 
                            })
                        }
                    }

                    roxas.esop.populate_totals_info(oEsopData, uiManipulatedTemplate)
                    roxas.esop.populate_summary(oEsopData, uiManipulatedTemplate);

                    if(uiManipulatedTemplate.find('td[data-label="stocks"]').length > 0)
                    {
                            
                        uiManipulatedTemplate.find('td[data-label="stocks"]').each(function (key, elem){
                            $(elem).text(number_format(cr8v.remove_commas($(elem).text()), 2));
                        });
                    }
                }
            }
        },
        
        /**
         * view_employee_by_esop_batch_get_personal_stocks
         * @description This function is for getting and rendering esop on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        view_employee_by_esop_batch_get_personal_stocks : function(oParams, uiThis, bFromViewEsop, bFromAdd, bFromEdit , bClaimForm){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/get_personal_stocks",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        // console.log(JSON.stringify(oData.data));
                        // console.log(oData);
                        if(typeof(bClaimForm) != 'undefined' && bClaimForm == true && typeof(oData.data[0]) != 'undefiend')
                        {
                            cr8v.esop.populate_claim_form(oData.data[0]);
                            
                            var oTempVestingRights = oData.data[0].vesting_rights,
                            sTempShares = '',
                            iDecimalValue = 0;

                            //roxas.esop.extend_offer_expiration(oData);

                            // for(var a in oTempVestingRights) {
                            //     sTempShares = String(oTempVestingRights[a].no_of_shares);
                            //     oTempVestingRights[a].no_of_shares = sTempShares.replace('.00', '');
                            // }

                        }
                        else{
                            if(cr8v.var_check(oData))
                            {
                                uiEsopGridContainer.find('.esop_grid:not(.template)').remove();
                                uiEsopListContainer.find('.esop_list:not(.template)').remove();
                                uiNoResultsFound.addClass('template hidden');

                                // console.log(oData);

                                var oTempVestingRights = oData.data[0].vesting_rights,
                                sTempShares = '',
                                iDecimalValue = 0,
                                iTotalDecimal = 0,
                                iTotalNoOfShares = 0,
                                sNoOfShares = '',
                                iLastNoOfShare = 0,
                                iOtherNoOfShare = 0,
                                y = 0,
                                oValuesOfShare = '',
                                iVestingRightLength = parseInt(Object.keys(oTempVestingRights).length) - parseInt(2);

                                roxas.esop.extend_offer_expiration(oData);

                                // console.log(oData.data[0]);
                                // console.log(oTempVestingRights);

                                for(var a in oTempVestingRights) {
                                    // NO OF SHARES MANIPULATION.
                                    // sTempShares = String(oTempVestingRights[a].no_of_shares);
                                    // oTempVestingRights[a].no_of_shares = sTempShares.replace('.00', '');

                                    // VALUES OF SHARE MANIPULATION.
                                    // oValuesOfShare = String(oTempVestingRights[a].value_of_shares);
                                    // oValuesOfShare = oValuesOfShare.replace('PHP ', '');
                                    // oValuesOfShare = oValuesOfShare.replace(',', '');
                                    // oValuesOfShare = oValuesOfShare.split('.');

                                    if(oTempVestingRights[a].no_of_shares != undefined 
                                    && oTempVestingRights[a].no_of_shares != NaN
                                    && oTempVestingRights[a].no_of_shares != 'undefined') {  
                                            sNoOfShares = oTempVestingRights[a].no_of_shares.split(",").join("");
                                            sNoOfShares = parseFloat(sNoOfShares);
                                            iTotalNoOfShares = parseFloat(iTotalNoOfShares) + parseFloat(sNoOfShares);
                                    }

                                    // if(iVestingRightLength != a) {
                                        
                                    //     //CALCULATE THE DECIMAL VALUE.
                                    //     iDecimalValue = '0.' + oValuesOfShare[1];
                                    //     iDecimalValue = parseFloat(iDecimalValue);
                                    //     iTotalDecimal = parseFloat(iTotalDecimal) + parseFloat(iDecimalValue);
                                    //     oValuesOfShare[0] = roxas.esop.convert_thousands(parseFloat(oValuesOfShare[0]));

                                    //     oTempVestingRights[a].value_of_shares = 'PHP ' + oValuesOfShare[0];
                                    //     // oTempVestingRights[a].value_of_shares = oValuesOfShare[0];
                                    // } else {

                                    //     //CALCULATE THE DECIMAL VALUE FOR THE LAST ROW.
                                    //     iDecimalValue = '0.' + oValuesOfShare[1];
                                    //     iDecimalValue = parseFloat(iDecimalValue);
                                    //     iTotalDecimal = parseFloat(iTotalDecimal) + parseFloat(iDecimalValue);
                                    //     oValuesOfShare = parseFloat(oValuesOfShare[0]) + parseFloat(iTotalDecimal);
                                    //     oValuesOfShare = roxas.esop.convert_thousands(parseFloat(oValuesOfShare));
                                    //     //oValuesOfShare = parse(oValuesOfShare);

                                    //     oTempVestingRights[a].value_of_shares = 'PHP ' + oValuesOfShare + '0';
                                    //     // oTempVestingRights[a].value_of_shares = oValuesOfShare + '0';
                                    // }
                                }
                                
                                //console.log(iTotalNoOfShares);
                                iOtherNoOfShare = parseFloat(iTotalNoOfShares) / parseFloat(5);
                                iOtherNoOfShare = String(iOtherNoOfShare);
                                iOtherNoOfShare = iOtherNoOfShare.split('.');
                                iOtherNoOfShare = iOtherNoOfShare[0] + '.00';
                                iOtherNoOfShare = parseInt(iOtherNoOfShare);
                                //console.log(iOtherNoOfShare + '-');
                                y = parseFloat(iOtherNoOfShare) * parseFloat(4);
                                //console.log(y + '-');
                                y = parseFloat(iTotalNoOfShares) - parseFloat(y);
                                iLastNoOfShare = parseFloat(y);
                                //console.log(iLastNoOfShare);

                                // for(var b in oTempVestingRights) {
                                //     if(iVestingRightLength != b) {
                                //         oTempVestingRights[b].no_of_shares = roxas.esop.convert_thousands(iOtherNoOfShare.toFixed(2));
                                //     } else {
                                //         oTempVestingRights[b].no_of_shares = roxas.esop.convert_thousands(iLastNoOfShare.toFixed(2));
                                //     }
                                // }
                                // oData.data[0].vesting_rights = oTempVestingRights;

                                if(oData.status == true)
                                {
                                    if(cr8v.var_check(bFromViewEsop) && bFromViewEsop)
                                    {
                                        roxas.esop.populate_main_info(oData.data);
                                        for (var i = 0 ; i < oData.data[0].vesting_rights.length - 1 ; i++ )
                                        {
                                            if(typeof(oData.data[0].vesting_rights[i].total) != 'undefined')
                                            {
                                                cr8v.populate_data(oData.data[0].vesting_rights[i] , $('tr.template.last-content ') , $('tbody[data-container="vesting"]'))
                                            }
                                            else
                                            {
                                                var template = $('[data-template="vesting_payments"].template');
                                                template.find('span.uc_status_notes_tool_tip').attr('data-id',oData.data[0].vesting_rights[i].id);

                                                cr8v.populate_data(oData.data[0].vesting_rights[i] , $('tr.template.vesting-rights') , $('tbody[data-container="vesting"]'));
                                                var uiNoPayment = template.find('tr.no-payment');
                                                if(oData.data[0].vesting_rights[i]['payments'].length > 0)
                                                {
                                                        
                                                        uiNoPayment.hide();
                                                }
                                                else
                                                {
                                                        uiNoPayment.show();
                                                }  

                                                cr8v.populate_data(oData.data[0].vesting_rights[i] ,template, $('[data-container="vesting_payments"]') , function() { $('[data-container="vesting_payments"]').attr('year_no', oData.data[0].vesting_rights[i].year_no ) })
                                                
                                                if(oData.data[0].vesting_rights[i].uc_status_notes == null)
                                                {
                                                    $('[data-container="vesting_payments"]').find('.uc_status_notes_tool_tip[data-id='+oData.data[0].vesting_rights[i].id+']').css({ 'display' : 'none' });
                                                }
                                                else
                                                {
                                                    $('[data-container="vesting_payments"]').find('.uc_status_notes_tool_tip[data-id='+oData.data[0].vesting_rights[i].id+']').attr('tt-html', '<p>Claim Status:</p><p><small>'+oData.data[0].vesting_rights[i].uc_status_notes+'</small></p>');
                                                }
                                                
                                            }

                                        }
                                        roxas.esop.populate_totals_info(oData.data)
                                        roxas.esop.populate_vesting_rights_claim(oData.data)
                                        roxas.esop.populate_summary(oData.data);
                                        roxas.esop.get_active_vesting_rights_year(oData.data);
                                        roxas.esop.vesting_rights_identification();
//                                         roxas.esop.display_outstanding_balance(oData.data);

                                    }
                                    else
                                    {
                                        if(oData.data.length > 0)
                                        {
                                            for(var i = 0, max = oData.data.length ; i <= max; i++)
                                            {
                                                var oEsopData = oData.data[i];

                                                var uiTemplateList = $('.esop_list.template').clone().removeClass('template hidden');
                                                var uiManipulatedTemplateList = roxas.esop.manipulate_template_list(oEsopData, uiTemplateList);

                                                var uiTemplateGrid = $('.esop_grid.template').clone().removeClass('template hidden');
                                                var uiManipulatedTemplateGrid = roxas.esop.manipulate_template_grid(oEsopData, uiTemplateGrid);

                                                uiEsopListContainer.prepend(uiManipulatedTemplateList);
                                                uiEsopGridContainer.prepend(uiManipulatedTemplateGrid);
                                            }
                                        }
                                        else
                                        {
                                            uiNoResultsFound.removeClass('template hidden');
                                        }
                                    }
                                }
                                else
                                {
                                    uiNoResultsFound.removeClass('template hidden');
                                }
                            }
                        }


                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * display_outstanding_balance
         * @description This function displays the outstanding balance of each vesting years.
         * @dependencies N/A
         * @param {object} oParams
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Dru Moncatar
         * @method_id N/A
         */
        display_outstanding_balance : function() {
            var uiVestingDiv = $('.margin-top-20[data-template="vesting_payments"]').not('.template'),
                iCountMain = uiVestingDiv.length,
                 d = new Date(),
                 strDate = d.toDateString(),
//                 oVestingRights = oParams[0].vesting_rights,
//                 sCurrencyName = oParams[0].currency,
                icountIt = 0;

            uiVestingDiv.each(function() {

                    icountIt++;

               var uiThis = $(this),
                   uiTotalAmount = uiThis.attr("custom-attr-value_of_shares"),
                   uiTotalAmountPerPayment = uiTotalAmount.replace("PHP ",""),
                   iTotalAmountPerPayment = parseFloat(uiTotalAmountPerPayment.split(",").join("")),
                   uiDataContainer = uiThis.find('[data-container="payments"]').find('.payments:not(.template)'),
                   uiCountContainer = uiDataContainer.length,
                   uiPaymentdiv = uiThis.find('.payments-total-vesting'),
                   uiTotalPaymentColumn = uiPaymentdiv.find('.text-left'),
                   uiSibling = uiTotalPaymentColumn.siblings(),
                   iSum = 0, 
                   sOutstandingBalance = 0;
                     

                   if(uiCountContainer > 0)
                   {
                      $.each(uiDataContainer, function(){
                              var uiPaymentDetailsThis = $(this),
                                  uiPaymentContainer = uiPaymentDetailsThis.find('[data-label="amount_paid"]'),
                                  iPaymentAmount = parseFloat(uiPaymentContainer.text().split(",").join(""));

                                  iSum += iPaymentAmount

                      });  

                   }else{

                   }

                   sOutstandingBalance = iTotalAmountPerPayment - iSum;

                       
                   if(sOutstandingBalance > 0)
                   {
                           
                        uiTotalPaymentColumn.find(".has-outstanding").remove();
                        uiTotalPaymentColumn.append('<p class="margin-left-30 has-outstanding">Outstanding Balance (as of  ' + strDate + ')</p>');
                        uiSibling.append('<p>' + number_format(sOutstandingBalance, 2) + ' PHP</p>');
                      
                       
                   }else{
                       uiTotalPaymentColumn.append('<p class="margin-left-30">Outstanding Balance (as of  ' + strDate + ')</p>');
                       uiSibling.append('<p>' + number_format(sOutstandingBalance, 2) + ' PHP</p>');
                   }

        


//                for(var a in oVestingRights) {
//                     if(oVestingRights[a].id == uiThis.attr('custom-attr-id')) {
//                         uiPaymentdiv = uiThis.find('.payments-total-vesting');
//                         uiTotalPaymentColumn = uiPaymentdiv.find('.text-left');
//                         uiSibling = uiTotalPaymentColumn.siblings();

// //                         console.log(oVestingRights[a]);

//                         oPaymentsTotal = oVestingRights[a].payments_totals;
//                         if(oPaymentsTotal.total_outstanding != 0) {
//                             sOutstandingBalance = Math.abs(oPaymentsTotal.total_outstanding).toFixed(2);
//                             sOutstandingBalance = roxas.esop.convert_thousands(sOutstandingBalance);
//                             uiTotalPaymentColumn.append('<p class="margin-left-30">Outstanding Balance (as of  ' + oVestingRights[a].year + ')</p>');
//                             uiSibling.append('<p>' + sOutstandingBalance + ' ' + sCurrencyName + '</p>');
//                         } else {
//                             sOutstandingBalance = Math.abs(oPaymentsTotal.total_outstanding).toFixed(2);
//                             sOutstandingBalance = roxas.esop.convert_thousands(sOutstandingBalance);
//                             uiTotalPaymentColumn.append('<p class="margin-left-30">Outstanding Balance (as of  ' + oVestingRights[a].year + ')</p>');
//                             uiSibling.append('<p>0.00 ' + sCurrencyName + '</p>');
//                         }
//                     }
//                }
            });

           
        },

        /**
         * convert_thousands
         * @description This function adds the character ',' to a integer if it reaches 4 digits and more.
         * @dependencies N/A
         * @param {variable} n
         * @response N/A
         * @criticality 
         * @software_architect N/A
         * @developer Dru Moncatar
         * @method_id N/A
         */
        convert_thousands : function(n) {
          n = n.toString()
          while (true) {
            var n2 = n.replace(/(\d)(\d{3})($|,|\.)/g, '$1,$2$3')
            if (n == n2) break
            n = n2
          }
          return n
        },

        /**
         * populate_main_info
         * @description Populates statement of account informatio
         * @dependencies
         * @param {object} oMainInfo
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Lorenz p
         * @method_id N/A
         */
        populate_main_info : function(oMainInfo) {
             if(cr8v.var_check(oMainInfo) && cr8v.var_check(oMainInfo[0]))
             {
                     oMainInfo = oMainInfo[0];
                     var uiMainInfoContainer = $('section[data-container="main-info"]:first'),
                         uiDetailsInfoContainer = $('section[data-container="details-info"]'),
                         sNotAvailable = 'N/A';

                     uiMainInfoContainer.attr('data-esop-id', oMainInfo.id);
                     uiMainInfoContainer.attr('data-esop-name', oMainInfo.name);
                     uiMainInfoContainer.attr('data-user-full-name', oMainInfo.full_name);
                     uiMainInfoContainer.attr('data-user-stock-offer-id', oMainInfo.stock_offer_id);
                     uiMainInfoContainer.attr('data-user-id', oMainInfo.user_id);

                     var sCurrencyName = cr8v.var_check(oMainInfo.currency) ? oMainInfo.currency : '' ;

                     uiMainInfoContainer.find('[data-label="esop_name"]').html(cr8v.var_check(oMainInfo.name) /*!= 'undefined'*/ ? oMainInfo.name : sNotAvailable);
                     // uiMainInfoContainer.find('img[alt="user-picture"]').attr('src', cr8v.var_check(oMainInfo.img) /*!= 'undefined'*/ ? roxas.config('url.server.base') +''+ oMainInfo.img : sNotAvailable);
                     uiMainInfoContainer.find('[data-label="full_name"]').html(cr8v.var_check(oMainInfo.full_name) /*!= 'undefined'*/ ? oMainInfo.full_name : sNotAvailable);
                     uiMainInfoContainer.find('[data-label="employee_code"]').html(cr8v.var_check(oMainInfo.employee_code) /*!= 'undefined'*/ ? oMainInfo.employee_code : sNotAvailable);
                     uiMainInfoContainer.find('[data-label="company"]').html(cr8v.var_check(oMainInfo.company_name) /*!= 'undefined'*/ ? oMainInfo.company_name : sNotAvailable);
                     uiMainInfoContainer.find('[data-label="rank"]').html(cr8v.var_check(oMainInfo.rank_name) /*!= 'undefined'*/ ? oMainInfo.rank_name : sNotAvailable);
                     uiMainInfoContainer.find('[data-label="department"]').html(cr8v.var_check(oMainInfo.department_name) /*!= 'undefined'*/ ? oMainInfo.department_name : sNotAvailable);
                     uiDetailsInfoContainer.find('[data-label="grant_date"]').html(cr8v.var_check(oMainInfo.grant_date) /*!= 'undefined'*/ ? moment(oMainInfo.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY') : sNotAvailable);
                     uiDetailsInfoContainer.find('[data-label="price_per_share"]').html(cr8v.var_check(oMainInfo.price_per_share) /*!= 'undefined'*/ ? sCurrencyName+' '+number_format(oMainInfo.price_per_share, 2) : sNotAvailable);
                     uiDetailsInfoContainer.find('[data-label="accepted"]').html(cr8v.var_check(oMainInfo.accepted) /*!= 'undefined'*/ ? number_format(parseFloat(oMainInfo.accepted).toFixed(2), 2) : sNotAvailable);
                     uiDetailsInfoContainer.find('[data-label="value"]').html(cr8v.var_check(oMainInfo.accepted) /*!= 'undefined'*/ ? sCurrencyName+' '+number_format(parseFloat(oMainInfo.accepted * oMainInfo.price_per_share).toFixed(2), 2) : sNotAvailable);
                     
                    if(cr8v.var_check(oMainInfo.img) && oMainInfo.img.length > 0)
                    {
                        uiMainInfoContainer.find('img[alt="user-picture"]').attr('src', roxas.config('url.server.base') +''+ oMainInfo.img).removeClass('hidden');
                    }

                    arrLogIDS.push(oMainInfo.user_id);

                    var oGetLogsParams = {
                        "where" : [
                            {
                                "field" : "ref_id",
                                "operator" : "IN",
                                "value" : "("+arrLogIDS.join()+")"
                            },
                            {
                                "field" : "type",
                                "operator" : "IN",
                                "value" : "('esop/user')"
                            }
                        ]
                    };
                    cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
             }           
        },

        /**
         * view_esop
         * @description This function is for viewing the esop
         * @dependencies
         * @param {int} iEsopID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_personal_stocks : function(iEsopID){
            if(cr8v.var_check(iEsopID))
            {
                localStorage.removeItem("esop_id");
                localStorage.setItem('esop_id', iEsopID);
                window.location =  roxas.config('url.server.base') + "esop/personal_stock_view";
            }
        },

        /**
         * download_acceptance_letter
         * @description This function is for downloading specific html
         * @dependencies N/A
         * @param {int} iStockOfferID
         * @param {int} iUserID
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        download_acceptance_letter : function(iStockOfferID, iUserID, iTemp_shares){
                var ishares = "";
                if(iTemp_shares != 0){
                    ishares = '/'+iTemp_shares;  
                }
                
            if(cr8v.var_check(iStockOfferID) && cr8v.var_check(iUserID))
            {
                window.location = roxas.config('url.server.base') + "esop/download_acceptance_letter/"+iStockOfferID+'/'+iUserID+ishares;
            }
        },

        /**
         * download_offer_letter
         * @description This function is for downloading specific html
         * @dependencies N/A
         * @param {int} iStockOfferID
         * @param {int} iUserID
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        download_offer_letter : function(iStockOfferID, iUserID){
            if(cr8v.var_check(iStockOfferID) && cr8v.var_check(iUserID))
            {
                window.location = roxas.config('url.server.base') + "esop/download_offer_letter/"+iStockOfferID+'/'+iUserID;
            }
        },

        populate_summary_claim_form : function (uiContainer, iTotal , iMaxStockAmount , sBalanceDate , sCurrencyName ) {

            var $summaryTemplate = $('.claim_form_summary.template'),
                sBalance,
                sBalancest,
                iTotalBalance = iMaxStockAmount - iTotal;

            if(iTotalBalance > 0)
            {
                sBalance = number_format(iTotalBalance , 2);
                sBalancest = '<p class="margin-left-30">Outstanding Balance (as of '+sBalanceDate+' )</p>';
            }
            else
            {
                 sBalance = number_format(iTotalBalance , 2);
                sBalancest = '<p class="margin-left-30">Outstanding Balance (as of '+sBalanceDate+' )</p>';    
            }

            uiContainer.append(
                '<tr class="last-content">' +
                '<td colspan="3" class="text-left">'+
                '<p class="margin-left-30 padding-bottom-5">Total Payment: </p>'+ sBalancest +
                '</td>'+
                '<td>'+
                '<p class="font-15 padding-bottom-5">'+number_format(iTotal, 2)+' '+ sCurrencyName + '</p>'+
                '<p class="font-15">'+sBalance+' '+ sCurrencyName + '</p>'+
                '</td>'+
                '</tr>'
            );

        },


        populate_summary : function(oData, uiContainer) {
            
            if(cr8v.var_check(oData))
            {

                if(cr8v.var_check(uiContainer))
                {
                    var $payments = uiContainer.find('tr.payments:not(.template)').clone(),
                        iTotal = 0,
                        iTotalBalance = 0,
                        sDate = '',
                        sBalance = '',
                        sBalancest = '',

                        iPaymentType = 0;

                        uiNoResultsFound = '<tr><td colspan="5" class="text-center">No Results found</td></tr>';
                        //uiParent = uiContainer.closest('.table-roxas'),
                        //uiNoPayment = $payments.siblings('.no-payment');
                        
                        //uiNoPayment.hide();

                        //if(oData[0].vesting_rights[0].payments[0].name != undefined 
                        //|| oData[0].vesting_rights[0].payments[0].name != null) {
                                //iPaymentType = oData[0].vesting_rights[0].payments[0].name;
                        //}

                       // console.log(Object.keys(oData.vesting_rights[0].payments[0]).length);
                        
                    if(oData.vesting_rights[0].payments.length > 0) {
                        if(oData.vesting_rights[0].payments[0].name !== undefined){
                                iPaymentType = oData.vesting_rights[0].payments[0].name;       
                        }
                    }
                        console.log(iPaymentType);

                    uiContainer.find('tbody[data-container="summary_vesting"]').append(($payments.length > 0) ? $payments : uiNoResultsFound);

                    uiContainer.find('[custom_attr_payments]').each(function() {
                        iTotal += parseFloat($(this).attr('custom_attr_payments'));
                    })

                    if(uiContainer.find('[custom_attr_balance]').length > 0)
                    {
                        uiContainer.find('[custom_attr_balance]').each(function() {
                            iTotalBalance += parseFloat($(this).attr('custom_attr_balance'));
                            sDate = $(this).attr('custom_date');
                        })
                    }

                    if(iTotalBalance > 0)
                    {
                        sBalance = number_format((parseFloat(oData.accepted) * parseFloat(oData.price_per_share)) - iTotal, 2);
                        sBalancest = '<p class="margin-left-30">Outstanding Balance (as of '+sDate+' )</p>';
                    }
                    else
                    {
                        sBalance = '0.00';
                        sBalancest = '<p class="margin-left-30">Outstanding Balance</p>';
                    }

                    uiContainer.find('tbody[data-container="summary_vesting"]').append(
                        '<tr class="last-content">' +
                        '<td colspan="4" class="text-left">'+
                        '<p class="margin-left-30 padding-bottom-5">Total Payment: </p>'+ sBalancest +
                        '</td>'+
                        '<td>'+
                        '<p class="font-15 padding-bottom-5">'+number_format(iTotal, 2)+' '+ oData.currency + '</p>'+
                        '<p class="font-15">'+sBalance+' '+ oData.currency + '</p>'+
                        '</td>'+
                        '</tr>'
                    );
                    
                    console.log(iPaymentType);
                    uiContainer.find('[data-label="payment_name"]').html(iPaymentType);
                    uiContainer.find('[data-label="summary_amount"]').html(number_format(oData.accepted , 2))
                    uiContainer.find('[data-label="summary_price_per_share"]').html(number_format(oData.price_per_share , 2))
                    uiContainer.find('[data-label="summary_currency"]').html(oData.currency)
                    uiContainer.find('[data-label="summary_total"]').html(number_format( parseFloat(oData.accepted) * parseFloat(oData.price_per_share) , 2))
                }
                else
                {
                    var $payments = $('tr.payments').clone(),
                        iTotal = 0,
                        iTotalBalance = 0,
                        sDate = '',
                        iPaymentType = 0,
                        sBalancest = '',
                        iPaymentType = 0,
                        arrAllGratuityId = [];

                        //uiParent = uiContainer.closest('.table-roxas'),
                        //uiNoPayment = $payments.siblings('.no-payment');
                        
                        //uiNoPayment.hide();

                    $.each($payments, function(){
                        var id = parseInt($(this).attr('custom-attr-gratuity_id'));
                        if( id > 0){
                            arrAllGratuityId.push(id);  
                        }
                    });

                    // console.log($payments);


                       // if(oData[0].vesting_rights[0].payments[0].name != undefined 
                        //|| oData[0].vesting_rights[0].payments[0].name != null) {
                               // iPaymentType = oData[0].vesting_rights[0].payments[0].name;
                        //}

                    arrAllGratuityId.forEach(function(id, key){
                        var ui = $paymentSummaryContainer.find('[custom-attr-gratuity_id="'+id+'"]');
                        if(ui.length > 1){
                            ui[0].remove();
                        }
                    });


                    $paymentSummaryContainer.append($payments);
                    console.log(oData)
                    if(oData[0].vesting_rights[0].payments.length > 0) {
                        if(oData[0].vesting_rights[0].payments[0].name !== undefined){
                                iPaymentType = oData[0].vesting_rights[0].payments[0].name;       
                        }
                    }

                    if(oData[0].vesting_rights[0].payments.length > 0) {
                        if(oData[0].vesting_rights[0].payments[0].name !== undefined){
                                iPaymentType = oData[0].vesting_rights[0].payments[0].name;       
                        }
                    }

                    $paymentSummaryContainer.append($payments);
                    // iPaymentType = oData[0].vesting_rights[0].payments[0].name;


                    $('[custom_attr_payments]').each(function() {
                        iTotal += parseFloat($(this).attr('custom_attr_payments'));
                    })

                    if($('[custom_attr_balance]').length > 0)
                    {
                        $('[custom_attr_balance]').each(function() {
                            iTotalBalance += parseFloat($(this).attr('custom_attr_balance'));
                            sDate = $(this).attr('custom_date');
                        })
                    }

                    if(iTotal > 0)
                    {
                        sBalance = number_format((parseFloat(oData[0].accepted) * parseFloat(oData[0].price_per_share)) - iTotal, 2);
                        // console.log(sBalance);
                        sBalance = String(sBalance);
                        sBalance = sBalance.replace('-', '');
                        // console.log(sBalance);
                        // sBalance = Math.abs(sBalance);
                        sBalancest = '<p class="margin-left-30">Outstanding Balance (as of '+sDate+' )</p>';
                    }
                     else
                    {
                        sBalance = '0.00';
                        sBalancest = '<p class="margin-left-30">Outstanding Balance</p>';
                        /*sBalance = number_format((parseFloat(oData[0].accepted) * parseFloat(oData[0].price_per_share)) - iTotal, 2);
                        alert(sBalance);*/
                    }

                    $paymentSummaryContainer.append(
                        '<tr class="last-content">' +
                        '<td colspan="3" class="text-left">'+
                        '<p class="margin-left-30 padding-bottom-5">Total Payment: </p>'+ sBalancest +
                        '</td>'+
                        '<td>'+
                        // '<p class="font-15 padding-bottom-5">'+sBalance+' '+ oData[0].currency + '</p>'+
                        '<p class="font-15 padding-bottom-5">'+number_format(iTotal, 2)+' '+ oData[0].currency + '</p>'+
                        // '<p class="font-15">'+sBalance+' '+ oData[0].currency + '</p>'+
                        '</td>'+
                        '</tr>'
                    );
                    
                        //console.log(iPaymentType);
                    $('[data-label="payment_name"]').html(iPaymentType);
                    $('[data-label="summary_amount"]').html(number_format(oData[0].accepted , 2))
                    $('[data-label="summary_price_per_share"]').html(number_format(oData[0].price_per_share , 2))
                    $('[data-label="summary_currency"]').html(oData[0].currency)
                    $('[data-label="summary_total"]').html(number_format( parseFloat(oData[0].accepted) * parseFloat(oData[0].price_per_share) , 2))
                    $('[data-label="share_price"]').html(number_format(oData[0].price_per_share , 2))
                }
            }


        },

        balance: function(oVestingRights , oEsopInfo) {
            if(cr8v.var_check(oVestingRights) && cr8v.var_check(oEsopInfo))
            {       

                    var sBalance = '',
                        sBalanceAmount = '',
                        sConvertedBalance = '';

                    if(moment().format('YYYY-MM-DD') > moment(oVestingRights.year).format('YYYY-MM-DD'))
                    {
                        sConvertedBalance = Math.abs(oVestingRights.payments_totals.total_outstanding);
                        // sConvertedBalance = oVestingRights.payments_totals.total_outstanding;
//                         sBalance = '<p class="margin-left-30">Outstanding Balance (as of '+moment(oVestingRights.year).format('MMMM DD, YYYY')+')</p>';
//                         sBalanceAmount = '<p class="font-15" custom_date="'+oVestingRights.year+'" custom_attr_balance="'+oVestingRights.payments_totals.total_outstanding+'"> '+ number_format(sConvertedBalance , 2)+' '+oEsopInfo.currency+'</p>';
                        // roxas.esop.add_outstanding_balance({
                        //     outstanding_balance : sConvertedBalance
                        //   });
                    }

                    var oBalance = {
                            'sbalance' : sBalance,
                            'sBalanceAmount' : sBalanceAmount,
                            'sConvertedBalance' : sConvertedBalance
                    };

                    return oBalance;  
                     
            }    
        },

        populate_totals_info : function(oData, uiContainer) {
               //console.log(JSON.stringify(oData));
            if(cr8v.var_check(oData))
            {
                $('.table-roxas').data('vesting_rights', oData.vesting_rights);

                if(cr8v.var_check(uiContainer))
                {
                    var $parent = 0;
                    for(var x = 0 ; x < oData.vesting_rights.length ; x++)
                    {
                        var $vesting
                        var iVestingRightId =  oData.vesting_rights[x].id;
                        $vesting = uiContainer.find('tr.payments:not(.template)').filter(function(el, val) {
                               return $(val).data('vesting_rights_id') == iVestingRightId
                        })

                        if(typeof($vesting) != 'undefined')
                        {
                             $parent = $vesting.parents('tbody');
                             if(typeof(oData.vesting_rights[x].total) == 'undefined')
                             {
                                   var oBalance = roxas.esop.balance(oData.vesting_rights[x] , oData),
                                   oCurrentVestingRights = oData.vesting_rights;
                                    // alert(iVestingRightId);
                                   $parent.append(
                                         '<tr class="last-content payments-total-vesting">' +
                                         '<td colspan="4" class="text-left">' +
                                         '<p class="margin-left-30 padding-bottom-5">Total Payment: </p>' + oBalance.sbalance +
                                         '</td>' +
                                         '<td>' +
                                         '<p class="font-15 padding-bottom-5" data-label="payment" custom_attr_payments="'+oData.vesting_rights[x].payments_totals.total_paid+'">'+ number_format(oData.vesting_rights[x].payments_totals.total_paid , 2)+' ' + oData.currency + '</p>' + oBalance.sBalanceAmount +
                                         '</td>' +
                                         '</tr>'
                                   );

                                   if(oBalance.sConvertedBalance > 0) {
                                        for(var a in oCurrentVestingRights) {
                                            if(oCurrentVestingRights[a].id == iVestingRightId) {
                                                var iNextRow = parseInt(a) + parseInt(1),
                                                iNextVestingRightsId = null,
                                                oOutstanding = {},
                                                oPaymentDetails = {},
                                                iPaymentLength = 0,
                                                oCurrentPayment = {};

                                                oPaymentDetails = oCurrentVestingRights[a].payments;
                                                iPaymentLength = parseInt(Object.keys(oPaymentDetails).length) - parseInt(1);
                                                oCurrentPayment = oPaymentDetails[ iPaymentLength ];
                                                
                                                console.log(oCurrentPayment);

                                                // oOutstanding['user_id'] = oCurrentVestingRights[a].user_id
                                                // oOutstanding['current_balance'] = oBalance.sConvertedBalance;
                                                // oOutstanding['vesting_rights_id'] = oCurrentVestingRights[ iNextRow ].id;

                                                oOutstanding['vesting_rights'] = oData.vesting_rights[0];
                                                oOutstanding['payment_type'] = oCurrentPayment.payment_type_id;
                                                oOutstanding['placed_by'] = oCurrentPayment.placed_by;
                                                oOutstanding['user_id'] = oCurrentPayment.user_id;
                                                oOutstanding['currency'] = oCurrentPayment.currency;
                                                oOutstanding['gratuity_id'] = oCurrentPayment.gratuity_id;
                                                oOutstanding['current_balance'] = oBalance.sConvertedBalance;
                                                oOutstanding['vesting_rights_id'] = oCurrentVestingRights[ iNextRow ].id;
                                                oOutstanding['current_vesting_rights'] = oCurrentVestingRights[a].id;
                                                roxas.esop.add_outstanding_balance(oOutstanding);
                                            }
                                        }
                                   }
                                    
                             }
                        }
                    }
                }
                else
                {
                    for(var i = 0 ; i < oData.length ; i++)
                    {
                            var $parent = 0;
                            for(var x = 0 ; x < oData[i].vesting_rights.length ; x++)
                            {
                                    var $vesting
                                    var iVestingRightId =  oData[i].vesting_rights[x].id;
                                    //var $totalsTemplate = $('tr.template.payments-total:first').clone().removeClass('template')
                                    $vesting = $('tr.payments:not(.template)').filter(function(el, val) {
                                           return $(val).data('vesting_rights_id') == iVestingRightId
                                    })

                                    if(typeof($vesting) != 'undefined')
                                    {
                                        // alert(iVestingRightId);
                                         $parent = $vesting.parents('tbody');
                                         if(typeof(oData[i].vesting_rights[x].total) == 'undefined')
                                         {
                                                var oBalance = roxas.esop.balance(oData[i].vesting_rights[x] , oData[i]),
                                                oCurrentVestingRights = oData[i].vesting_rights;
                                                
                                               $parent.append(
                                                     '<tr class="last-content payments-total-vesting">' +
                                                     '<td colspan="3" class="text-left">' +
                                                     '<p class="margin-left-30 padding-bottom-5">Total Payment: </p>' + oBalance.sbalance +
                                                     '</td>' +
                                                     '<td>' +
                                                     '<p class="font-15 padding-bottom-5" data-label="payment" custom_attr_payments="'+oData[i].vesting_rights[x].payments_totals.total_paid+'">'+ number_format(oData[i].vesting_rights[x].payments_totals.total_paid , 2)+' ' + oData[i].currency + '</p>' + oBalance.sBalanceAmount +
                                                     '</td>' +
                                                     '</tr>'
                                               ); 

                                               if(oBalance.sConvertedBalance > 0) {
                                                    for(var a in oCurrentVestingRights) {
                                                        if(oCurrentVestingRights[a].id == iVestingRightId) {
                                                            var iNextRow = parseInt(a) + parseInt(1),
                                                            iNextVestingRightsId = null,
                                                            oOutstanding = {},
                                                            oPaymentDetails = {},
                                                            iPaymentLength = 0,
                                                            oCurrentPayment = {};

                                                            oPaymentDetails = oCurrentVestingRights[a].payments;
                                                            iPaymentLength = parseInt(Object.keys(oPaymentDetails).length) - parseInt(1);
                                                            oCurrentPayment = oPaymentDetails[ iPaymentLength ];

                                                            console.log(oCurrentPayment);

                                                            oOutstanding['vesting_rights'] = oData[0].vesting_rights;
                                                            oOutstanding['payment_type'] = oCurrentPayment.payment_type_id;
                                                            oOutstanding['placed_by'] = oCurrentPayment.placed_by;
                                                            oOutstanding['user_id'] = oCurrentPayment.user_id;
                                                            oOutstanding['currency'] = oCurrentPayment.currency;
                                                            oOutstanding['gratuity_id'] = oCurrentPayment.gratuity_id;
                                                            oOutstanding['current_balance'] = oBalance.sConvertedBalance;
                                                            oOutstanding['vesting_rights_id'] = oCurrentVestingRights[ iNextRow ].id;
                                                            oOutstanding['current_vesting_rights'] = oCurrentVestingRights[a].id;
                                                            roxas.esop.add_outstanding_balance(oOutstanding);
                                                        }
                                                    }
                                               }
                                         }
                                    }
                                    //oData[i].vesting_rights[x]


                            }


                    }
                }
            }
        },

        populate_vesting_rights_claim : function(oData) {
            if(cr8v.var_check(oData))
            {
                
                if(cr8v.var_check(oData[0]))
                {
                        if(cr8v.var_check(oData[0].vesting_rights))
                        {
                              for(var i = 0; i < oData[0].vesting_rights.length ; i++)
                              {
                                      if(typeof(oData[0].vesting_rights[i].total) == 'undefined')
                                      {
                                              if(moment().format('YYYY-MM-DD') > moment(oData[0].vesting_rights[i].year).format('YYYY-MM-DD'))
                                              {

                                                  if(oData[0].vesting_rights[i].no_of_claim == 0){
                                                      var uiTemplate = $vestingRightTemplate.clone().removeClass('template');
                                                      uiTemplate.find('[data-label="year_no"]').html(oData[0].vesting_rights[i].year_no)
                                                      uiTemplate.find('[data-label="year_date"]').html(oData[0].vesting_rights[i].year)
                                                      uiTemplate.find('input[type="checkbox"]').attr('value' , oData[0].vesting_rights[i].id)
                                                      $vestingRightClaimContainer.append(uiTemplate);
                                                  }

                                              }
                                      }
                                      
                              }  
                        }
                }
                
                
            }
        },

        /**
         * populate_payment_application
         * @description This function will populate the payment application table
         * @dependencies N/A
         * @param N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jomar Tanael
         * @method_id N/A
         */

         get_data_for_payment_application : function (esop_id) {
             var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : {"esop_id":esop_id},
                    "url"    : roxas.config('url.server.base') + "esop/get_all_gratuity",
                    "dataType" : "json",
                    "success": function (oData) {
                        // console.log(JSON.stringify(oData));

                         roxas.esop.populate_payment_application(oData);
                          
                    }
                };
                roxas.esop.ajax(oAjaxConfig);
         },

        populate_payment_application : function(oData){

                // console.log("dito un");
               
               var uiTemplateContainer = $('[data-container="vesting_payments"]').find('[data-template="vesting_payments"]:not(.template)'),
                   iCount = 1,
                   uiTempCountAll = uiTemplateContainer.length,
                   uiTempCount = 1,
                   iTotalGratuity = 0;

                   

                  

                   $.each(uiTemplateContainer, function(){
                        var uiThis = $(this),
                            uiContainer = uiThis.find('[data-container="payments"]'),
                            uiTemplate = uiThis.find('[data-container="payments"]').find(".payments:last"),
                            oDataYear = uiThis.attr("custom-attr-year"),
                            oNumShares = uiThis.attr("custom-attr-no_of_shares"),
                            iNumShares = parseFloat(oNumShares.split(",").join("")),
                            sSplitDateAttr = oDataYear.split(", "),
                            iLengthData = Object.keys(oData).length;

                                // console.log(iNumShares);

                        for(var x in oData)
                        {
                            var item = oData[x],
                                splited_date = item.grant_date.split(", ");

                                        var uiCloneTemplate = uiTemplate.clone(),
                                            iCountYears = parseFloat(item.total_value_of_gratuity_given) / uiTempCountAll,
                                            iGetAmount = iNumShares * parseFloat(item.price_per_share);
                                             


                                                // console.log(iGetAmount);

                                       uiCloneTemplate.removeClass("template").show();
                                       uiCloneTemplate.find('[data-label="number"]').html(item.id);
                                       uiCloneTemplate.find('[data-label="payment_name"]').html("Gratuity @ "+item.price_per_share+" ("+uiTempCount+" of "+uiTempCountAll+")");
                                       uiCloneTemplate.find('[data-label="date_of_or"]').html(item.dividend_date_formatted);
                                       uiCloneTemplate.find('[data-label="amount_paid"]').html(number_format(iGetAmount, 2));


                                        if(uiContainer.find('.last-content').length > 0)
                                        {
                                           uiCloneTemplate.insertBefore(uiContainer.find('.last-content'));
                                        }else{
                                             uiContainer.append(uiCloneTemplate);
                                        }

                                       

                                       uiContainer.find(".no-payment").hide();


                                       oData[x]["gratuity_amount"] = iGetAmount;
                            

                        }
                        uiTempCount++;

                        var uiVestingPayments = $('[data-template="vesting_payments"]').find('[data-container="payments"]').find('tr'),
                            iCountNumbersPay = -1;
                                   

                        $.each(uiVestingPayments, function(){
                                var uiThis = $(this);

                               
                                // alert('test');
                                if(uiThis.not(".template") && uiThis.hasClass("payments"))
                                {
                                        
                                    uiThis.find('[data-label="number"]').text(iCountNumbersPay);
                                        //  console.log(iCountNumbersPay);  
                                         iCountNumbersPay++; 
                                   
                                }else if(uiThis.hasClass("last-content")){

                                        iCountNumbersPay = 0;
                                        // console.log("mai");
                                }

                        });
                      


                        var uiTotalPayment = uiThis.find('[data-container="payments"]').find('.payments:not(".template")').find('[data-label="amount_paid"]'),
                               iTotal = 0;

                               $.each(uiTotalPayment, function(){
                                       var uiThisTotal = $(this),
                                            iPerAmount = parseFloat(uiThisTotal.text().split(",").join(""));


                                        iTotal += iPerAmount;
                               });

                               if(uiContainer.find('.last-content').length > 0)
                               {
                                    uiContainer.find('.last-content').find('[data-label="payment"]').html(number_format(iTotal, 2) + " PHP");
                                     
                               }else{

                                    uiContainer.append(
                                         '<tr class="last-content payments-total-vesting">' +
                                         '<td colspan="3" class="text-left">' +
                                         '<p class="margin-left-30 padding-bottom-5">Total Payment: </p>'+
                                         '</td>' +
                                         '<td>' +
                                         '<p class="font-15 padding-bottom-5" data-label="payment" custom_attr_payments="'+iTotal+'">'+ number_format(iTotal , 2)+' PHP</p>'+
                                         '</td>' +
                                         '</tr>'
                                   );

                               }
                             
                   });           

                   roxas.esop.display_gratuity_summary(oData);


        },
         /**
         * display_gratuity_summary
         * @description This function will display gratuity summary
         * @dependencies N/A
         * @param {int} oData
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Nelson Estuesta Jr
         * @method_id N/A
         */

        display_gratuity_summary : function(oData)
        {

                var uiContainer = $('[data-container="vesting_payments"]').find('[data-template="vesting_payments"]:not(.template)'),
                    uiSummaryTemplate = $('[data-container="summary_vesting"]').find(".payments:last"),
                    uiSummaryContainer = $('[data-container="summary_vesting"]'),
                    iCountSummaryContainer = uiSummaryContainer.length,
                    iCountContainer = uiContainer.length,
                    iCountNumbers = 1;

                    for(var x in oData)
                    {
                         var item = oData[x],
                             iTotal = 0,
                             iTotalPerItem = parseFloat(item.total_value_of_gratuity_given) / iCountContainer,
                             iGetAmount = iTotalPerItem * parseFloat(item.price_per_share);

                          iTotal = parseFloat(item.gratuity_amount) * iCountContainer;


                          var uiCloneTemplate = uiSummaryTemplate.clone();

                          uiCloneTemplate.removeClass("template").show();
                          uiCloneTemplate.find('[data-label="number"]').html("1");
                          uiCloneTemplate.find('[data-label="payment_name"]').html("Gratuity @ "+item.price_per_share);
                          uiCloneTemplate.find('[data-label="date_of_or"]').html(item.dividend_date_formatted);
                          uiCloneTemplate.find('[data-label="amount_paid"]').html(number_format(iTotal, 2));
                              
                            
                           if(uiSummaryContainer.find('.last-content').length > 0)
                           {
                              uiCloneTemplate.insertBefore(uiSummaryContainer.find('.last-content'));
                           }else{
                              uiSummaryContainer.append(uiCloneTemplate);
                           }
                    }

                    

                        var uiSetNumSummary = $('[data-container="summary_vesting"]').find('.payments:not(.template)').find('[data-label="number"]');

                        $.each(uiSetNumSummary, function(){
                                var uiThis = $(this);
                                
                                uiThis.text(iCountNumbers);
                                 //console.log(iCountNumbers);  
                                  
                                iCountNumbers++; 

                        });
                      


                     var uiTotalPaymentSummaryAmount = $('.long-panel.border-10px').find('.first-text').find('[data-label="summary_amount"]'),
                         uiTotalPaymentSummaryPerShare = $('.long-panel.border-10px').find('.first-text').find('[data-label="summary_price_per_share"]'),
                         uiContainerTotal = $('[data-container="summary_vesting"]').find('.payments:not(".template")').find('[data-label="amount_paid"]'),
                         iTotalPaymentSummaryAmount = parseFloat(uiTotalPaymentSummaryAmount.text().split(",").join("")),
                         iTotalPaymentSummaryPerShare = parseFloat(uiTotalPaymentSummaryPerShare.text().split(",").join("")),
                         iTotalPaymentSummary = iTotalPaymentSummaryAmount * iTotalPaymentSummaryPerShare,
                         iTotalPayment = 0,
                         d = new Date(),
                         strDate = d.toDateString();

                          $.each(uiContainerTotal, function(){
                                       var uiThisTotal = $(this),
                                            iPerAmount = parseFloat(uiThisTotal.text().split(",").join(""));


                                        iTotalPayment += iPerAmount;
                               });

                               var iTotalSub = iTotalPaymentSummary - iTotalPayment;
                                

                               if(uiSummaryContainer.find('.last-content').length > 0)
                               {
                                    uiSummaryContainer.find('.last-content').find('.font-15.padding-bottom-5').html(number_format(iTotalPayment, 2) + " PHP");
                                    
                                     uiSummaryContainer.find('.last-content').find("td:nth-of-type(1)").find("p:nth-of-type(2)").html("Outstanding Balance (as of  "+strDate+")");

                                     uiSummaryContainer.find('.last-content').find("td:nth-of-type(2)").append('<p class="font-15 padding-bottom-5">'+number_format(iTotalSub, 2)+' PHP</p>');

                               }else{

                                    uiSummaryContainer.append(
                                        '<tr class="last-content">'+
                                        '       <td colspan="3" class="text-left">'+
                                        '              <p class="margin-left-30 padding-bottom-5">Total Payment: </p>'+
                                        '              <p class="margin-left-30">Outstanding Balance (as of  )</p>'+
                                        '        </td>'+
                                        '        <td>'+
                                        '                <p class="font-15 padding-bottom-5" total-payment='+iTotalPayment+'>'+number_format(iTotalPayment, 2) +' PHP</p>'+
                                        '                <p class="font-15 padding-bottom-5" >'+iTotalSub+'</p>'+
                                        '        </td>'+
                                        '</tr>'
                                   );
                               }


                               var oBuildForPayment = {
                                       date_today : strDate,
                               }


                               roxas.esop.display_outstanding_balance();
                               
                               
                    
        },
        
        /**
         * get_active_vesting_rights_year
         * @description This function will return the current active vesting year
         * @dependencies N/A
         * @param {int} iStockOfferID
         * @param {int} iUserID
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_active_vesting_rights_year : function(oData) {
            if(cr8v.var_check(oData))
            {
                
                if(cr8v.var_check(oData[0]))
                {
                        var arrActiveYears = [];

                        if(cr8v.var_check(oData[0].vesting_rights))
                        {
                              for(var i = 0; i < oData[0].vesting_rights.length ; i++)
                              {
                                      if(oData[0].vesting_rights[i].status == 0)
                                      {
                                          //console.log(oData[0].vesting_rights[i].id +'->'+ oData[0].vesting_rights[i].year_no)    
                                          var sKey = 'active_year_id';
                                          
                                          var o = {};
                                          o[sKey] = oData[0].vesting_rights[i].id; 

                                          arrActiveYears.push(o);
                                      }
                                      
                              }  
                              if(arrActiveYears.length > 0)
                              {
                                //  console.log(arrActiveYears[0])
                                $('body').data('data-active-vesting-year-id',arrActiveYears[0]);
                                $('body').data('data-employee-id',oData[0].user_id);
                                //console.log($('body').data('data-active-vesting-year-id').active_year_id);
                              }



                             
                        }
                }
                
                
            }
        },

        get_submitted_claims :  function(oAjaxParams) {
            if (cr8v.var_check(oAjaxParams)) {

                var oAjaxConfig = {
                    "type": "POST",
                    "data": oAjaxParams,
                    "url": roxas.config('url.server.base') + "esop/get_submitted_claims",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        roxas.esop.populate_claim_documents(oData);
                        if(window.location.href.indexOf('esop/claim_form') > -1)
                        {
                            roxas.esop.populate_payments_per_claim(oData);

                        }
                    },
                    "complete": function () {

                    }
                };

                roxas.esop.ajax(oAjaxConfig);

            }
        },

        populate_payments_per_claim : function(oData) {
            var iClaimId = localStorage.getItem('claim_id');    

            if(typeof(iClaimId) != 'undefined')
            {
                 var oClaim =   oData.find(function(el){
                         return el['id'] == iClaimId
                 });

                 if(cr8v.var_check(oClaim))
                 {
                         if(cr8v.var_check(oClaim.data_claim_vesting_rights))
                         {
                                 for (var i = 0; i < oClaim.data_claim_vesting_rights.length ; i++)
                                 {
                                       var oPayment = oClaim.data_claim_vesting_rights[i],
                                           $payment = $claimFormPaymentTemplate.clone().removeClass('template');

                                       $payment.find('[data-label="year_no"]').html('Year ' + oPayment.year_no); 
                                       $payment.find('[data-label="year_date"]').html(moment(oPayment.year).format('MMMM DD, YYYY')); 
                                       $payment.find('[data-label="vesting"]').html((moment().format('YYYY-MM-DD') > moment(oPayment.year).format('YYYY-MM-DD')) ? 'With Vesting Rights' : 'Without Vesting Rights' )
                                       $payment.find('[data-label="claim_mature_stocks"]').html(number_format(oPayment.no_of_shares, 2));
                                       $payment.find('[data-label="price_per_share"]').html(number_format(oPayment.price_per_share, 2)); 
                                       $payment.find('[data-label="claim_stocks_value"]').html(oPayment.currency_name +' ' + number_format( (parseFloat(oPayment.no_of_shares) * parseFloat(oPayment.price_per_share)), 2));

                                       if(oPayment.data_user_payments.length > 0)
                                       {
                                                var $paymentContainer = $payment.find('tbody[data-container="payment_container_claim_form"]'),
                                                iTotal = 0,
                                                sDate = '',
                                                sCurrencyName = '';
                                                for(var x = 0 ; x < oPayment.data_user_payments.length ; x++)
                                                {
                                                     var uiTrPaymentTemplate = $payment.find('tr.payments_claim_form.template').clone().removeClass('template');
                                                     
                                                     uiTrPaymentTemplate.find('[data-label="year_no"]').html(x + 1)
                                                     uiTrPaymentTemplate.find('[data-label="payment_type"]').html(oPayment.data_user_payments[x].payment_name) 
                                                     uiTrPaymentTemplate.find('[data-label="date"]').html(moment(oPayment.data_user_payments[x].date_added).format('MMMM DD, YYYY'))    
                                                     uiTrPaymentTemplate.find('[data-label="payment_value"]').html(oPayment.data_user_payments[x].currency_name + ' ' + number_format(oPayment.data_user_payments[x].amount_paid, 2))   

                                                     iTotal += parseFloat(oPayment.data_user_payments[x].amount_paid);

                                                     $paymentContainer.append(uiTrPaymentTemplate)
                                                     sDate = moment(oPayment.year).format('MMMM DD, YYYY');
                                                     sCurrencyName = oPayment.data_user_payments[x].currency_name;
                                                }

                                                roxas.esop.populate_summary_claim_form($paymentContainer,  iTotal , parseFloat(oPayment.value_of_shares) , sDate, sCurrencyName)



                                       }; 
                                       
                                       $claimFormPaymentContainer.prepend($payment)
                                 }
                         }
                 } 

                 var test = 0;
            }
        },

        populate_claim_documents : function(oData) {
            if(typeof(oData) != 'undefined')
            {
                for (var o = 0 ; o < oData.length ; o++)
                {
                    var iTotalShare = 0,
                        iTotalPayment = 0;
                            
                    for (var i = 0 ; i <  oData[o].claim_group.length ; i++)
                    {
                        var $template = $claimDocumentsTemplate.clone().removeClass('template');

                        $template.find('input[name="claim_id"]').attr('value' , oData[o].claim_group[i].claim_id);
                        $template.find('[data-label="year_text"]').html(oData[o].claim_group[i].year_no);
                        $template.find('[data-label="date_text"]').html(oData[o].claim_group[i].date);

                        $claimDocumentsContainer.append($template);
                    }

                    for (var y = 0 ; y <  oData[o].data_claim_vesting_rights.length ; y++)
                    {
                        iTotalShare += parseFloat(cr8v.remove_commas(oData[o].data_claim_vesting_rights[y].no_of_shares));
                        iTotalPayment += parseFloat(cr8v.remove_commas(oData[o].data_claim_vesting_rights[y].value_of_shares));
                    }
                    


                    var oClaimData = {
                        'full_name' : oData[o].first_name + ' ' + oData[o].middle_name + ' ' + oData[o].last_name,
                        'mature_stock' : iTotalShare,
                        'total_payment' : iTotalPayment,
                        'esop_name' : oData[o].esop_name,
                        'grant_date' : '',
                        'currency' : ''
                    };

                    cr8v.esop.populate_claim_form(oClaimData);
                }


            }
        },

        populate_claim_form : function(oData) {
            if(cr8v.var_check(oData))
            {
                var oEsopData = oData,
                    $claimForm = $('section.claim-form');

                $('[data-label="esop_name"]').html(cr8v.var_check(oEsopData.esop_name) ? oEsopData.esop_name : '');
                $claimForm.find('[data-label="mature_stocks"]').html(cr8v.var_check(oEsopData.mature_stock) ? number_format(oEsopData.mature_stock, 2): '');
                $claimForm.find('[data-label="stocks_value"]').html(cr8v.var_check(oEsopData.total_payment) ? number_format(oEsopData.total_payment, 2): '');
                $claimForm.find('[data-label="full_name"]').html(cr8v.var_check(oEsopData.full_name) ? oEsopData.full_name : '');
                $claimForm.find('[data-label="date"]').html(moment().format('MMMM DD, YYYY'));

   

            }
        },

        /**
         * bind_upload_share_distribution_events
         * @description This function is for binding upload share distribution events
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_upload_share_distribution_events : function () {
            uiTriggerUploadShareDistribution.on('click', function(){
                $(this).next('label').find('input[name="upload_share_distribution"]').trigger('click');
            });

            /*upload user list*/
            var last_file_path = '';
            var last_file_name = '';
            uiUploadShareDistributionModal.on('change', 'input[name="upload_share_distribution"]', function(){
                var uiThis = $(this);
                var file = uiUploadShareDistributionModal.find('input[type=file][name="upload_share_distribution"]')[0];
                var file_path = '';
                var file_name = '';
                var formdata = false;
                var uiLogoContainer = uiUploadShareDistributionModal.find('[data-container="file_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();
                var uiErrorContainer = uiUploadShareDistributionModal.find('.upload_share_distribution_error_message');

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'csv'/* || sValueExt == 'xls' || sValueExt == 'xlsx'*/)
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('csv_file', file.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'esop/upload_share_distribution_csv',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData))
                                    {
                                        uiUploadShareDistributionBtn.attr('disabled', true);
                                        uiThis.val('');
                                        cr8v.add_error_message(false, uiErrorContainer);   

                                        if(cr8v.var_check(oData.status) && oData.status == true)
                                        {
                                            if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                            {
                                                uiUploadShareDistributionModal.find('.upload_share_distribution_success_message').removeClass('hidden').show().delay(1000).fadeOut()
                                                    .html('<p class="font-15 success_message"> ' + oData.message[0] + '</p>');
                                            }

                                            var sOrigFileName = (cr8v.var_check(oData.data.original_file_name)) ? oData.data.original_file_name : 'No file uploaded yet';
                                            var sFilePath = (cr8v.var_check(oData.data.file_path) ? oData.data.file_path : '');

                                            if(cr8v.var_check(sShareDistributionFilePath) && sShareDistributionFilePath.length > 0)
                                            {
                                                var sFilePathForDelete = sShareDistributionFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                                                sFilePathForDelete = './assets/uploads/share_distribution/csv/' + sFilePathForDelete;

                                                var oParams = {
                                                    "file_path" : sFilePathForDelete
                                                };

                                                cr8v.delete_file(oParams, function(oData){
                                                
                                                });
                                            }

                                            sShareDistributionFilePath = sFilePath;
                                            sShareDistributionFileName = sOrigFileName;

                                            uiUploadShareDistributionModal.find('#file_name').html('<i>'+sOrigFileName+'</i>');

                                            uiUploadShareDistributionBtn.attr('disabled', false);
                                        }
                                        else
                                        {
                                            sShareDistributionFilePath = '';
                                            sShareDistributionFileName = '';

                                            uiUploadShareDistributionModal.find('#file_name').html('<i>No File Uploaded</i>');

                                            var sErrorMessages = '';
                                            for(var i = 0, max = oData.message.length; i < max; i++)
                                            {
                                                var message = oData.message[i];
                                                sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                            }

                                            cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                        }
                                    }
                                    else
                                    {
                                        file_path = last_file_path;
                                        file_name = last_file_name;
                                    }
                                },
                                complete: function(){

                                }
                            };

                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        sShareDistributionFilePath = '';
                        sShareDistributionFileName = '';
                        uiUploadShareDistributionModal.find('#file_name').html('<i>No File Uploaded</i>');
                        uiUploadShareDistributionBtn.attr('disabled', true);
                        var sErrorMessages = '<p class="font-15 error_message">Please upload the correct template file.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });
    
            uiUploadShareDistributionBtn.on('click', function(){
                roxas.esop.view_upload_share_distribution();
            });

            uiConfirmUploadBtn.on('click', function(){
                var oGetCsvParams = {
                    "file_path" : localStorage.getItem('upload_share_distribution_path'),
                    "esop_id" : localStorage.getItem('esop_id')
                };

                cr8v.show_spinner(uiConfirmUploadBtn, true);

                roxas.esop.open_csv(oGetCsvParams, undefined, true, function(oData){

                    cr8v.show_spinner(uiConfirmUploadBtn, false);

                    if(cr8v.var_check(oData))
                    {
                        var iErrorCtr = 0;

                        if(oData.status == true)
                        {
                            if(oData.data.length > 0)
                            {                                    
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oShareDistributionInfo = oData.data[i];

                                    var iRow = i + 1;

                                    if(oShareDistributionInfo.valid == 0 && count(oShareDistributionInfo.error_message) > 0)
                                    {
                                        iErrorCtr++;
                                    }
                                }

                                if(iErrorCtr > 0)
                                {
                                    uiConfirmUploadBtn.attr('disabled', true);
                                }
                                else
                                {
                                    uiConfirmUploadBtn.attr('disabled', false);
                                    roxas.esop.assemble_share_distribution_information(uiConfirmUploadBtn);
                                }
                            }
                        }
                    }
                });
            });

            uiUploadShareDistributionModalBtn.on('click', function(){
                roxas.esop.clear_upload_share_distribution_modal();
            });

            uiCancelUploadBtn.on('click', function(){
                if(cr8v.var_check(localStorage.getItem('upload_share_distribution_path')) && localStorage.getItem('upload_share_distribution_path') != null)
                {
                    var sFilePath = localStorage.getItem('upload_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                    sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    localStorage.removeItem('upload_share_distribution_path');
                    localStorage.removeItem('upload_share_distribution_file_name');

                    cr8v.delete_file(oParams, function(oData){
                        window.location = roxas.config('url.server.base') + 'esop/view_esop';
                    });
                }
            });

            uiUploadShareDistributionModal.on('click', '.close-me', function(){
                if(cr8v.var_check(sShareDistributionFilePath) && sShareDistributionFilePath.length > 0)
                {
                    var sFilePath = sShareDistributionFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                    sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    sShareDistributionFilePath = '';
                    sShareDistributionFileName = '';

                    cr8v.delete_file(oParams, function(oData){
                        
                    });
                }
            });
        },

        /**
         * assemble_share_distribution_information
         * @description This function is for assembling the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_share_distribution_information : function (uiConfirmUploadBtn){
            var iEsopID = $('[data-container="esop_information"]').attr('data-esop-id');
            var sEsopName = $('[data-container="esop_information"]').attr('data-esop-name');

            var oParams = { "esop_id" : iEsopID,"esop_name" : sEsopName, "employee_data" : [], "allotment_data" : [] };

            uiUploadShareDistributionContainer.find('.upload_share_distribution:not(.template)').each(function (key, elem){
                var iCompanyID = $(elem).data('company_id');

                var oData = {
                    "esop_id" : iEsopID,
                    "user_id" : $(elem).data('employee_id'),
                    "stock_amount" : cr8v.remove_commas(number_format(parseFloat($(elem).data('alloted_shares')), 2)),
                    "offer_letter_id" : $(elem).data('offer_letter_id'),
                    "acceptance_letter_id" : $(elem).data('acceptance_letter_id')
                };

                oParams.employee_data.push(oData);

                var oAllotmentData = {
                    "esop_id" : iEsopID,
                    "department_id" : '',
                    "company_id" : $(elem).data('company_id'),
                    "allotment" : cr8v.remove_commas(number_format(parseFloat($(elem).data('alloted_shares')), 2))
                }

                if(oParams.allotment_data.length > 0)
                {
                    var oFiltered = oParams.allotment_data.filter(function (el){
                        return eval( el['company_id'] == $(elem).data('company_id') );
                    });

                    if(count(oFiltered) > 0)
                    {
                        if(cr8v.var_check(oFiltered[0]))
                        {
                            oFiltered[0].allotment = parseFloat(oFiltered[0].allotment) + parseFloat($(elem).data('alloted_shares'));
                        }
                        else
                        {
                            oParams.allotment_data.push(oAllotmentData);
                        }
                    }
                    else
                    {
                        oParams.allotment_data.push(oAllotmentData);
                    }
                }
                else
                {
                    oParams.allotment_data.push(oAllotmentData);
                }

                // if(oParams.allotment_data.length > 0)
                // {
                //     for(var i in oParams.allotment_data)
                //     {
                //         var oParamsAllotmentData = oParams.allotment_data[i];

                //         var oFiltered = oParams.allotment_data.filter(function (el){
                //             return eval( el['company_id'] == $(elem).data('company_id') );
                //         });

                //         if(oParamsAllotmentData.company_id == $(elem).data('company_id'))
                //         {
                //             oParams.allotment_data[i]['allotment'] = parseFloat(oParams.allotment_data[i]['allotment']) + parseFloat($(elem).data('alloted_shares'));
                //         }
                //         else
                //         {
                //             if(count(oFiltered) > 0)
                //             {
                //                 oFiltered.allotment = parseFloat(oFiltered.allotment) + parseFloat($(elem).data('alloted_shares'));
                //             }
                //             else
                //             {
                //                 oParams.allotment_data.push(oAllotmentData);
                //             }
                //         }
                //     }
                // }
                // else
                // {
                //     oParams.allotment_data.push(oAllotmentData);
                // }
            });
            
            for(var i in oParams.allotment_data)
            {
                var oParamsAllotmentData = oParams.allotment_data[i];

                oParams.allotment_data[i]['allotment'] = cr8v.remove_commas(number_format(oParams.allotment_data[i]['allotment'], 2));
            }
            // console.log(oParams)
            roxas.esop.insert_upload_share_distribution(oParams, uiConfirmUploadBtn);
        },

        /**
         * insert_upload_share_distribution
         * @description This function is for inserting the validated user list
         * @dependencies 
         * @param {object} oUserUploadList
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        insert_upload_share_distribution : function (oParams, uiBtn, bFromEsopBatch) {
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/insert_upload_share_distribution",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bFromEsopBatch) && bFromEsopBatch)
                                {
                                    $('.insert_batch_share_distribution_success_message').removeClass('hidden').html('<p class="font-15 success_message">Distribution shares added successfully.</p>');
                                   
                                    setTimeout(function(){
                                        if(cr8v.var_check(localStorage.getItem('upload_batch_share_distribution_path')) && localStorage.getItem('upload_batch_share_distribution_path') != null)
                                        {
                                            var sFilePath = localStorage.getItem('upload_batch_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                                            sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                                            var oParams = {
                                                "file_path" : sFilePath
                                            };

                                            localStorage.removeItem('upload_batch_share_distribution_path');
                                            localStorage.removeItem('upload_batch_share_distribution_file_name');

                                            cr8v.delete_file(oParams, function(oData){
                                                window.location = roxas.config('url.server.base') + 'esop/view_esop_batch';
                                            });
                                        }
                                    }, 1000)
                                }
                                else
                                {
                                    $('.insert_share_distribution_success_message').removeClass('hidden').html('<p class="font-15 success_message">Distribution shares added successfully.</p>');
                                   
                                    setTimeout(function(){
                                        if(cr8v.var_check(localStorage.getItem('upload_share_distribution_path')) && localStorage.getItem('upload_share_distribution_path') != null)
                                        {
                                            var sFilePath = localStorage.getItem('upload_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                                            sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                                            var oParams = {
                                                "file_path" : sFilePath
                                            };

                                            localStorage.removeItem('upload_share_distribution_path');
                                            localStorage.removeItem('upload_share_distribution_file_name');

                                            cr8v.delete_file(oParams, function(oData){
                                                window.location = roxas.config('url.server.base') + 'esop/view_esop';
                                            });
                                        }
                                    }, 1000)
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * view_upload_share_distribution
         * @description This function is for viewing the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_upload_share_distribution : function (){
            if(cr8v.var_check(localStorage.getItem('upload_share_distribution_path')) && localStorage.getItem('upload_share_distribution_path') != null)
            {
                var sFilePath = localStorage.getItem('upload_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                var oParams = {
                    "file_path" : sFilePath
                };

                cr8v.delete_file(oParams, function(oData){
                
                });
            }
            
            localStorage.removeItem('upload_share_distribution_path');
            localStorage.setItem('upload_share_distribution_path' , sShareDistributionFilePath);

            localStorage.removeItem('upload_share_distribution_file_name');
            localStorage.setItem('upload_share_distribution_file_name' , sShareDistributionFileName);

            window.location = roxas.config('url.server.base') + "esop/upload_share_distribution";
        },

        /**
         * clear_upload_share_distribution_modal
         * @description This function is for clearing data in upload user list modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_upload_share_distribution_modal : function(){
            /*clear data*/
            uiUploadShareDistributionBtn.attr('disabled', true);
            uiUploadShareDistributionModal.find('#file_name').html('<i>No File Uploaded</i>');
            uiUploadShareDistributionModal.find('.upload_share_distribution_error_message').addClass('hidden');
            uiUploadShareDistributionModal.find('.upload_share_distribution_success_message').addClass('hidden');
        },

        /**
         * bind_create_esop_batch_events
         * @description This function is for binding events for create esop batch
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_create_esop_batch_events : function () {
            uiCreateEsopBatchBtn.on('click', function(){
                if(uiCreateEsopBatchModal.find('input[name="esop_batch_name"]').val() != '' && uiCreateEsopBatchModal.find('input[name="esop_batch_total_share_qty"]').val() != '')
                {
                    var uiErrorContainer = uiCreateEsopBatchModal.find('.create_esop_batch_error_message');
                    var iInputValue = cr8v.remove_commas(uiCreateEsopBatchModal.find('input[name="esop_batch_total_share_qty"]').val());
                    var iTotalSharesUnvailedBatches = uiCreateEsopBatchModal.attr('esop-total-unavailed-batches');

                    if(parseFloat(iInputValue) > parseFloat(iTotalSharesUnvailedBatches))
                    {
                        var sErrorMessages = '<p class="font-15 error_message">Total Share Quantity have exceeded available shares</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                        uiCreateEsopBatchModal.find('input[name="esop_batch_total_share_qty"]').addClass('input-error');
                    }
                    else
                    {
                        cr8v.add_error_message(false, uiErrorContainer);
                        roxas.esop.assemble_create_esop_batch_information(uiCreateEsopBatchBtn);
                    }
                }
            });

            uiCreateEsopBatchModal.find('input[name="esop_batch_name"]').on('blur', function(){
                var uiThis = $(this);
                if(uiThis.val() != '' && uiCreateEsopBatchModal.find('input[name="esop_batch_total_share_qty"]').val() != '')
                {
                    uiCreateEsopBatchBtn.attr('disabled', false);
                }
                else
                {
                    uiCreateEsopBatchBtn.attr('disabled', true);
                }
            });

            uiCreateEsopBatchModalBtn.on('click', function(){
                roxas.esop.clear_create_esop_batch_modal();
            });

            // uiEsopBatchContainer.on('click','button[modal-target="add-gratuity"]', function(){
            //     var uiThis = $(this);
            //     var sEsopName = $('body').find('a[data-label="esop_name"]:first').text();
            //     uiAddGratuityModal.find('[data-label="esop_name"]').text(sEsopName);

            // });

            cr8v.input_numeric($('[name="esop_batch_total_share_qty"]'));
            
            uiCreateEsopBatchModal.find('input[name="esop_batch_total_share_qty"]').on('blur', function(){
                var uiThis = $(this);
                if(uiThis.val() != '' && uiCreateEsopBatchModal.find('input[name="esop_batch_name"]') != '')
                {
                    uiThis.val(number_format(uiThis.val(), 2));
                    uiCreateEsopBatchBtn.attr('disabled', false);
                }
                else
                {
                    uiCreateEsopBatchBtn.attr('disabled', true);
                }
            });
        },


        /**
         * @description This function is for adding new gratuity in gratuity page
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jomar Tanael
         * @method_id N/A
         */
         bind_add_new_gratuity : function (oData) {
            cr8v.input_numeric($('[name="price_per_share"]'));

            var uiModal = $('[modal-id="add-gratuity"]');
               
               uiModal.data(oData);

               var esopData = uiModal.data();


            uiAddGratuityModal.on('blur', 'input[name="price_per_share"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {                
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            uiAddGratuityModal.on('click','.btn-add-gratuity', function(){
                var uiBtn = $(this),
                    sEsoplist = uiAddGratuityModal.find('.esop_dropdown').find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
                    sGetEsopName = uiAddGratuityModal.find('.esop_dropdown').find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt");
        
                for(var i = 0, max = Object.keys(esopData).length ; i < max; i++)
                {
                    var esop_id,
                        esopTotalSharesAvailed;

                    if(sGetEsopName.val().trim() == esopData[i].name)
                    {
                        esop_id = esopData[i].id;
                        esopTotalSharesAvailed = esopData[i].total_share_qty;
                    }
                }
                var iEsopID = esop_id;
                var sDividendDate = uiAddGratuityModal.find('input[name="dividend_date"]').val();
                var iPricePerShare = uiAddGratuityModal.find('input[name="price_per_share"]').val();
                    iPricePerShare = parseFloat(cr8v.remove_commas(iPricePerShare));
                var iCurrency = uiAddGratuityModal.find('div.currency_dropdown').attr('currency_id');
                var iStatus = 0;
                var iTotalValueOfGratuityGiven = 0;
                var iEsopTotalSharesAvailed = esopTotalSharesAvailed;
                    iEsopTotalSharesAvailed = parseFloat(cr8v.remove_commas(iEsopTotalSharesAvailed));
                    
                    iTotalValueOfGratuityGiven = iPricePerShare * iEsopTotalSharesAvailed;


                
                if(roxas.esop.validate_add_gratuity() == true)
                {
                     
                
                     var oParams = {
                                        'esop_id'                       : iEsopID,
                                        'dividend_date'                 : sDividendDate,
                                        'price_per_share'               : iPricePerShare,
                                        'statement'                     : iStatus,
                                        'currency_id'                   : iCurrency,
                                        'currency_name'                 : uiAddGratuityModal.find('.currency_dropdown').find('.frm-custom-dropdown-txt').find('input').val(),
                                        'esop_name'                     : sGetEsopName.val().trim(),
                                        'status'                        : iStatus,
                                        'total_value_of_gratuity_given' : iTotalValueOfGratuityGiven
                                   };  
                                           
                      roxas.esop.insert_gratuity(oParams,uiBtn);
                 }
            });
         },

         bind_get_all_gratuity : function () {
            var oGetParams = {
                        "search_field" : 'esop_id',
                        "limit" : 30
            };

            if(cr8v.var_check(oGetParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oGetParams,
                    "url"    : roxas.config('url.server.base') + "esop/get_gratuity",
                    "success": function (oData) {

                        if(cr8v.var_check(oData))
                        {

                            if(oData.status == true)
                            { 
                                var uiTable = $('[data-container="gratuity_list_container"]'),
                                 uiTemplate = uiTable.find('.tmp_gratuity_list.template'),
                                 uiCloned = {};
                                 uiTable.find('.tmp_gratuity_list:not(.template)').remove();
                                 uiTable.find('.no_results_found').addClass('hidden');
                                
                                 for(var a = 0, max = oData.data.length ; a < max; a++) {

                                    var oEsopData = oData.data[a];

                                    var uiTemplateList = uiTable.find('.tmp_gratuity_list.template').clone().removeClass('template hidden');
                                    var uiManipulatedTemplateList = roxas.esop.manipulate_gratuity_template_list(oEsopData, uiTemplateList);

                                    uiTable.prepend(uiManipulatedTemplateList);
                                    roxas.esop.bind_gratuity_action_events(uiTable);
                                 }
                                 roxas.esop.filter_gratuity_table(oData);                          

                            }
                            else
                            {
//                                 uiTable.find('.no_results_found').removeClass('hidden');
                            }
                        }
                    },

                };
                roxas.esop.ajax(oAjaxConfig);
            }
         },

         filter_gratuity_table : function(oParams) {
                uiGratuityTable.on('click','#btn_gratuity_table',function(){
                    var uiEsopDropdownData =  $(".upload_esop_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(),
                        uiStatusDropdownData = $(".gratuity_status_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(),
                        oData = oParams.data,
                        iCounter = 0,
                        oNewData = {},
                        sStatus = "",
                        uiMainTableContainer = $('.payment_cloned');

                    for(var a in oData) {
                        if (oData[a].status == 0){
                            sStatus = "Pending";
                        }
                        else if (oData[a].status == 1){
                            sStatus = "Approved";
                        }
                        else if (oData[a].status == 2){
                            sStatus = "Gratuity Sent";
                        }
                        else if (oData[a].status == 3){
                            sStatus = "Rejected";
                        }
                        else{
                            sStatus = "All Status";    
                        }
                        if(oData[a].esop_name == uiEsopDropdownData && sStatus == uiStatusDropdownData) {
                            oNewData[ iCounter ] = oData[a];
                            ++iCounter;
                        }
                    }
                    roxas.esop.display_filtered_gratuity(oNewData);
                });
        },

        display_filtered_gratuity : function (oData) {
             var uiTable = $('[data-container="gratuity_list_container"]'),
                     uiTemplate = uiTable.find('.tmp_gratuity_list.template'),
                     uiCloned = {};

             if(Object.keys(oData).length > 0){
                     uiTable.find('.tmp_gratuity_list:not(.template)').remove();
                     uiTable.find('.no_results_found').addClass('hidden');
        
                     for(var a in oData) {

                        var oEsopData = oData[a];

                        var uiTemplateList = uiTable.find('.tmp_gratuity_list.template').clone().removeClass('template hidden');
                        var uiManipulatedTemplateList = roxas.esop.manipulate_gratuity_template_list(oEsopData, uiTemplateList);

                        uiTable.prepend(uiManipulatedTemplateList);
                        roxas.esop.bind_gratuity_action_events(uiTable);
                     }
            }
            else if(Object.keys(oData).length == 0) {
                    uiTable.find('.tmp_gratuity_list:not(.template)').remove();
                    uiTable.find('.no_results_found').addClass('hidden');
        
                    for(var a in oData) {

                        var oEsopData = oData[a];

                        var uiTemplateList = uiTable.find('.tmp_gratuity_list.template').clone().removeClass('template hidden');
                        var uiManipulatedTemplateList = roxas.esop.manipulate_gratuity_template_list(oEsopData, uiTemplateList);

                        uiTable.prepend(uiManipulatedTemplateList);
                        roxas.esop.bind_gratuity_action_events(uiTable);
                     }
                uiTable.find('.no_results_found').removeClass('hidden');
            }
        },

         get_payment_records : function () {
            cr8v.input_numeric($('[name="amount_paid"]'));
            uiAddPaymentPerUser.on('blur', 'input[name="amount_paid"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {                
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });
            
            var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : {'data' : []},
                    "url"    : roxas.config('url.server.base') + "esop/get_payment_records",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                 roxas.esop.display_payment_records(oData.data);
                                 roxas.esop.filter_payment_table(oData);
                            }
                        }
                    },
                    "complete": function () {

                    },
                };

                roxas.esop.ajax(oAjaxConfig);
         },

         get_all_esop : function () {
            var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : {'data' : []},
                    "url"    : roxas.config('url.server.base') + "esop/get_all_esop",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        if(cr8v.var_check(oData))
                        {
                            roxas.esop.bind_add_new_gratuity(oData.data);
                            if(oData.status == true)
                            {
                                //for gratuity page
                                uiEsopListDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oEsopData = oData.data[i];

                                    var uiTemplate = uiEsopListDropdown;
                                    cr8v.append_dropdown(oEsopData,uiTemplate,false,'Esop');
                                }
                                 uiEsopListDropdown.find('.option:first').trigger('click');

                                 //for payment records page upload payment records
                                uiPaymentUploadEsopDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oEsopData = oData.data[i];

                                    var uiTemplate = uiPaymentUploadEsopDropdown;
                                    cr8v.append_dropdown(oEsopData,uiTemplate,false, 'Esop', 'Grant_date',oEsopData.grant_date);
                                }
                                uiPaymentUploadEsopDropdown.find('.option:first').trigger('click');

                                if(window.location.href.indexOf('esop/esop_list') > -1)
                                {
                                    if(oData.data.length > 0)
                                            {
                                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                                {
                                                    var oEsopData = oData.data[i];

                                                    var uiTemplateList = $('.esop_list.template').clone().removeClass('template hidden');
                                                    var uiManipulatedTemplateList = roxas.esop.manipulate_template_list(oEsopData, uiTemplateList);

                                                    var uiTemplateGrid = $('.esop_grid.template').clone().removeClass('template hidden');
                                                    var uiManipulatedTemplateGrid = roxas.esop.manipulate_template_grid(oEsopData, uiTemplateGrid);

                                                    uiEsopListContainer.prepend(uiManipulatedTemplateList);
                                                    uiEsopGridContainer.prepend(uiManipulatedTemplateGrid);
                                                }
                                            }
                                            else
                                            {
                                                uiNoResultsFound.removeClass('template hidden');
                                            }
                                }
                            }
                        }
                    },
                    "complete": function () {

                    },
                };
                roxas.esop.ajax(oAjaxConfig);
         },

         display_payment_records : function(oData)
         {
           var uiMainTableContainer = $('#main-record-container'),
                 uiTable = uiMainTableContainer.find('[data-container="payment_record_container"]'),
                 uiTemplate = uiTable.find('.tmp_payment_list.template'),
                 uiCloned = {};
                 if(Object.keys(oData).length > 0){
                         uiTable.find('.tmp_payment_list:not(.template)').remove();
                         uiTable.find('.no_results_found').addClass('hidden');
                        for(var x in oData)
                        {
                                var item = oData[x];

                                uiCloned = uiTemplate.clone().removeClass('template hidden');
                                uiCloned.addClass('payment_cloned');
                                uiCloned.removeClass('tmp_payment_list');
                                uiCloned.find('td[data-label="dividend_added"]').html(item.dividend_date);
                                uiCloned.find('td[data-label="employee_name"]').html(item.name);
                                uiCloned.find('td[data-label="esop_name"]').html(item.esop);
                                uiCloned.find('td[data-label="payment_method"]').html(item.payment_method);
                                uiCloned.find('td[data-label="amount"]').html(number_format(item.amount,2));
                                uiTable.append(uiCloned);
                         }      
                 }
                 else{
                         uiTable.find('.no_results_found').removeClass('hidden');
                 }
                
         },

          get_companies : function(){
            var oParams = {
                    "limit" : 999999
                };
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : {'data' : []},
                    "url"    : roxas.config('url.server.base') + "companies/get",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiCompanyDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oCompanyData = oData.data[i];

                                    var uiTemplate = uiCompanyDropdown;
                                    cr8v.append_dropdown(oCompanyData, uiTemplate, false, 'company');
                                   
                                }
                                 roxas.esop.bind_payment_records_event(); 
                                uiCompanyDropdown.find('.option:first').trigger('click');
                            }
                        }
                    },
                    "complete": function () {

                    },
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        get_users_by_company : function(company_id=0){
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : {"company_id":company_id},
                    "url"    : roxas.config('url.server.base') + "esop/get_users_by_company",
                    "success": function (oData) {

                        if(cr8v.var_check(oData))
                        {

                            if(oData.status == true)
                            {
                                uiUsersDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oUsersData = oData.data[i];

                                    var uiTemplate = uiUsersDropdown;
                                    cr8v.append_dropdown(oUsersData, uiTemplate, false, 'user');
                                }
                                uiUsersDropdown.find('.option:first').trigger('click');

                            }
                        }
                    },
                    "complete": function () {
                    }


               
            }
             roxas.esop.ajax(oAjaxConfig);
        },


         bind_payment_records_event : function () {
            var uiDropdown = $(".add_company_payment_record_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option");
                
                uiDropdown.off('click.getDropdown').on('click.getDropdown',function () {
                    var uiThis = $(this),
                        iValue = uiThis.attr("data-value");

                        roxas.esop.get_users_by_company(iValue);

                }); 

                uiAddPaymentPerUser.on('click','.btn-add-payment-record' , function() {

                var iSelected = $(".add_employee_name_payment_record_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(),
                iEmployeeID = $(".add_employee_name_payment_record_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
                iValue  = "";

                $.each(iEmployeeID, function(){
                        var uiThisOption = $(this),
                          iGetValue =  uiThisOption.attr("data-value"); 

                           if(iSelected == uiThisOption.text())
                           {
                                 iValue = iGetValue;
                           }         
                });


                var Selected = $('div[modal-id="add-payment-record"]').find(".upload_esop_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(),
                iEsopID = $('div[modal-id="add-payment-record"]').find(".upload_esop_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
                Value  = "",
                esopDateGranted = "";
                
                $.each(iEsopID, function(){
                        var uiThisOption1 = $(this),
                            GetValue =  uiThisOption1.attr("data-value"),
                            getEsopDateGranted = uiThisOption1.attr("grant_date"); 

                           if(Selected == uiThisOption1.text())
                           {
                                 Value = GetValue;
                                 esopDateGranted = getEsopDateGranted;
                           }
                });

                var uiBtn = $(this),
                iPaymentTypeID = uiAddPaymentPerUser.find('div#payment_method_dropdown').attr('payment_type_id'),
                iCurrencyID = uiAddPaymentPerUser.find('div#currency_dropdown').attr('currency_id'),
                iAmountPaid = uiAddPaymentPerUser.find('input[name="amount_paid"]').val(),
                iEmployeeID = iValue,
                iEsopID = Value,
                dDividendDate = uiAddPaymentPerUser.find('input[name="dividend_date"]').val(),
                dDateGranted = esopDateGranted,
                sCurrencyName = "",
//                 var iActiveYearID = $('body').data('data-active-vesting-year-id').active_year_id;
                sFullName = uiAddPaymentPerUser.find('div.add_employee_name_payment_record_dropdown').find('input').val(),
                sEsopName = uiAddPaymentPerUser.find('div.upload_esop_dropdown').find('input').val();

                iAmountPaid = parseFloat(cr8v.remove_commas(iAmountPaid));
                
                if(isNaN(iAmountPaid) == true){
                        iAmountPaid = 0;
                } 

                if(roxas.esop.validate_add_payment_record() == true)
                {
                    
                        
                    var oParams = {
                                'payment_type_id'            : iPaymentTypeID,
                                'currency'                   : iCurrencyID,
                                'currency_name'              : sCurrencyName,
                                'amount_paid'                : iAmountPaid,
                                'vesting_rights_id'          : iEmployeeID,
                                'placed_by'                  : oUserInfo.id,
                                'user_id'                    : iEmployeeID,
                                'esop_id'                    : iEsopID,
                                'user_name'                  : sFullName,
                                'esop_name'                  : sEsopName,
                                'payment_type'               : uiAddPaymentPerUser.find('div#payment_method_dropdown').find('input').val(),
                                'vesting_rights'             : iEmployeeID,
                                'dividend_date'              : dDividendDate,
                                'date_granted'               : dDateGranted
                                };
                    console.log(oParams)
                    roxas.esop.add_payment_record(oParams,uiBtn);    
                    }

                });
         },

         filter_payment_table : function(oParams) {
                uiPaymentTable.on('click','#btn_filter_payment_table',function(){
                    var uiEsopDropdownData =  $(".upload_esop_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(),
                        uiPaymentMethodDropdownData = $(".payment_method_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(),
                        oData = oParams.data,
                        iCounter = 0,
                        oNewData = {},
                        uiMainTableContainer = $('.payment_cloned');

                    for(var a in oData) {
                        if(oData[a].esop == uiEsopDropdownData
                        && oData[a].payment_method == uiPaymentMethodDropdownData) {
                            oNewData[ iCounter ] = oData[a];
                            ++iCounter;
                        }
                    }
                    
                    uiMainTableContainer.html('');
                    roxas.esop.display_payment_records(oNewData);
                });
                

        },


        /**
         * clear_create_esop_batch_modal
         * @description This function is for clearing data in create esop batch modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_create_esop_batch_modal : function(){
            /*clear data*/
            uiCreateEsopBatchModal.find('input:not(.dd-txt)').removeClass('input-error').val('');
            uiCreateEsopBatchModal.find('.create_esop_batch_success_message').addClass('hidden');
            uiCreateEsopBatchModal.find('.create_esop_batch_error_message').addClass('hidden');
        },

        /**
         * assemble_create_esop_batch_information
         * @description This function will assemble create esop batch information
         * @dependencies N/A
         * @param {jQuery} uiAddEsopForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_create_esop_batch_information : function(uiCreateEsopBatchBtn){
            var oParams = {
                'name'              : uiCreateEsopBatchModal.find('input[name="esop_batch_name"]').val(),
                'grant_date'        : $('[data-container="esop_information"]').attr('data-esop-grant-date'),
                'total_share_qty'   : uiCreateEsopBatchModal.find('input[name="esop_batch_total_share_qty"]').val()/*uiCreateEsopBatchModal.attr('esop-total-unavailed-batches')*/,
                'price_per_share'   : $('[data-container="esop_information"]').attr('data-esop-price-per-share'),
                'currency'          : $('[data-container="esop_information"]').attr('data-esop-currency'),
                'vesting_years'     : $('[data-container="esop_information"]').attr('data-esop-vesting-year'),
                'created_by'        : iUserID,
                'status'            : 1,
                'is_deleted'        : 0,
                'parent_id'         : $('[data-container="esop_information"]').attr('data-esop-id'),
                'type'              : 'batch_esop'
            };
            roxas.esop.add_esop(oParams, uiCreateEsopBatchBtn, true);
        },

        /**
         * open_csv
         * @description This function is for opening and viewing of the csv passed as parameter
         * @dependencies 
         * @param {object} oParams
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        open_csv : function (oParams, uiThis, bValidateOnly, fnCallback){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/open_csv",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)

                        if(typeof(fnCallback) == 'function')
                        {
                            fnCallback(oData);
                        }

                        if(cr8v.var_check(bValidateOnly) && bValidateOnly)
                        {
                            return false;
                        }

                        if(cr8v.var_check(oData))
                        {
                            uiUploadShareDistributionContainer.find('.upload_share_distribution:not(.template)').remove();
                            uiNoErrorsFound.addClass('template hidden');
                            var iErrorCtr = 0;

                            if(oData.status == true)
                            {
                                if(oData.data.length > 0)
                                {                                    
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oShareDistributionInfo = oData.data[i];

                                        var iRow = i + 1;
    
                                        var uiTemplate = $('.upload_share_distribution.template').clone().removeClass('template hidden');
                                        var uiManipulatedTemplate = roxas.esop.manipulate_upload_share_distribution(oShareDistributionInfo, uiTemplate);

                                        uiManipulatedTemplate.find('[data-label="row"]').text(iRow);

                                        if(oShareDistributionInfo.valid == 0 && count(oShareDistributionInfo.error_message) > 0)
                                        {
                                            iErrorCtr++;
                                            // uiManipulatedTemplate.css({'border' : '1px solid red'});

                                            var uiErrorTemplate = uiUploadShareDistributionErrorContainer.find('.upload_share_distribution_error.template').clone().removeClass('template hidden');
                                            var uiManipulatedErrorTemplate = roxas.esop.manipulate_upload_share_distribution_error(oShareDistributionInfo, uiErrorTemplate, uiManipulatedTemplate);

                                            uiManipulatedErrorTemplate.find('[data-label="row_error"]').text(iRow);

                                            uiUploadShareDistributionErrorContainer.append(uiManipulatedErrorTemplate);
                                        }

                                        uiUploadShareDistributionContainer.append(uiManipulatedTemplate);
                                    }

                                    if(iErrorCtr > 0)
                                    {
                                        uiConfirmUploadBtn.attr('disabled', true);
                                    }
                                    else
                                    {
                                        uiConfirmUploadBtn.attr('disabled', false);
                                        uiNoErrorsFound.removeClass('template hidden');
                                    }
                                }
                                else
                                {
                                    uiNoErrorsFound.removeClass('template hidden');
                                }
                            }
                            else
                            {
                                uiNoErrorsFound.removeClass('template hidden');

                                if(cr8v.var_check(oData.message))
                                {
                                    var sErrorMessages = '';
                                    var uiErrorContainer = $('.insert_share_distribution_error_message');
                                    for(var i = 0, max = oData.message.length; i < max; i++)
                                    {
                                        var message = oData.message[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);

            }
        },

        /**
         * manipulate_upload_user_list
         * @description This function is for rendering upload share distribution details
         * @dependencies 
         * @param {object} oShareDistributionInfo
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_upload_share_distribution : function (oShareDistributionInfo, uiTemplate){
            if(cr8v.var_check(oShareDistributionInfo) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.data('company_code', oShareDistributionInfo.company_code);
                uiTemplate.data('company_id', oShareDistributionInfo.company_id);
                uiTemplate.data('employee_code', oShareDistributionInfo.employee_code);
                uiTemplate.data('employee_id', oShareDistributionInfo.employee_id);
                uiTemplate.data('employee_name', oShareDistributionInfo.employee_name);
                uiTemplate.data('alloted_shares', cr8v.remove_commas(number_format(oShareDistributionInfo.alloted_shares, 2)));
                uiTemplate.data('offer_letter_id', oShareDistributionInfo.offer_letter_id);
                uiTemplate.data('acceptance_letter_id', oShareDistributionInfo.acceptance_letter_id);

                uiTemplate.find('[data-label="company_code"]').text(oShareDistributionInfo.company_code);
                uiTemplate.find('[data-label="employee_code"]').text(oShareDistributionInfo.employee_code);
                uiTemplate.find('[data-label="employee_name"]').text(oShareDistributionInfo.employee_name);
                uiTemplate.find('[data-label="alloted_shares"]').text(number_format(oShareDistributionInfo.alloted_shares, 2));
                uiTemplate.find('[data-label="offer_letter_id"]').text(oShareDistributionInfo.offer_letter_id);
                uiTemplate.find('[data-label="acceptance_letter_id"]').text(oShareDistributionInfo.acceptance_letter_id);

                return uiTemplate;
            }
        },

        /**
         * manipulate_upload_share_distribution_error
         * @description This function is for rendering upload share distribution error messages
         * @dependencies 
         * @param {object} oShareDistributionInfo
         * @param {jQuery} uiTemplate
         * @param {jQuery} uiPreviewTemplate
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_upload_share_distribution_error : function (oShareDistributionInfo, uiTemplate, uiPreviewTemplate){
            if(cr8v.var_check(oShareDistributionInfo) && cr8v.var_check(uiTemplate))
            {
                var uiRowNameErrorContainer = uiTemplate.find('[data-container="row_name_error"]');
                var uiRowMessageErrorContainer = uiTemplate.find('[data-container="row_message_error"]');

                for (var k in oShareDistributionInfo.error_message) {
                    var oErrorMessage = oShareDistributionInfo.error_message[k];

                    uiTemplate.find('[data-label="row_name_error"]').append(k+'</br> ');
                    uiTemplate.find('[data-label="row_message_error"]').append(oErrorMessage+'</br> ');
                    uiPreviewTemplate.find('[data-error="'+k+'"]').attr({'style' : 'border: 1px solid red;'});
                }


                return uiTemplate;
            }
        },

        /**
         * validate_add_payment_record
         * @description This function is validate_add_payment_record
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        validate_add_payment_record : function(){
            var iError = 0;
            var sErrorMsg = '';

            var iPaymentTypeID = uiAddPaymentRecordModal.find('div#payment_method_dropdown').find('input.dd-txt').val();
            var iCurrencyID = uiAddPaymentRecordModal.find('div#currency_dropdown').find('input.dd-txt').val();
            var iAmountPaid = uiAddPaymentRecordModal.find('input[name="amount_paid"]').val();

            if(iPaymentTypeID=='')
            {
               sErrorMsg += 'Payment method is required.<br/>';
               iError++;
            }

            if(iCurrencyID=='')
            {
               sErrorMsg += 'Currency is required.<br/>';
               iError++;
            }

            if(iAmountPaid==NaN)
            {
               sErrorMsg += 'Payment value is required.<br/>';
               iError++;
            }

            

            if(iError > 0)
            {
               uiAddPaymentRecordModal.find('.error_message').removeClass('hidden');
               uiAddPaymentRecordModal.find('.error_message').html(sErrorMsg);
               return false;     
            }
            else
            {
               uiAddPaymentRecordModal.find('.error_message').addClass('hidden');
               uiAddPaymentRecordModal.find('.error_message').html('');
               return true;
            }


        },

        /**
         * add_payment_record
         * @description This function will add_payment_record
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        add_payment_record : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "esop/add_payment_record",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddPaymentPerUser.find('.error_message').addClass('hidden').html('');
                                uiAddPaymentPerUser.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiAddPaymentPerUser.hasClass('showed'))
                                    {
                                        uiAddPaymentPerUser.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'esop/payment_records';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                // var oLength = oData.message.length;
                                console.log(oData);

                                uiAddPaymentPerUser.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message.join("<br />") + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * validate_add_gratuity
         * @description This function will validate_add_gratuity
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        validate_add_gratuity : function(){
            var iError = 0;
            var sErrorMsg = '';
            var uiPricePerShare = uiAddGratuityModal.find('input[name="price_per_share"]');
            var uiDividendDate = uiAddGratuityModal.find('input[name="dividend_date"]');
            var uiCurrency = uiAddGratuityModal.find('div.currency_dropdown').find('input.dd-txt');
            var bValidGrantDate = cr8v.validate_date(uiDividendDate, 'MM/DD/YYYY');

          
            if(uiPricePerShare.val() == '')
            {
               sErrorMsg += 'Dividend Price per Share is required.<br/>';
               iError++;
            }

            if(uiDividendDate.val() == '')
            {
               sErrorMsg += 'Dividend Date is required.<br/>';
               iError++;
            }

            if(uiCurrency.val() == '')
            {
               sErrorMsg += 'Currency required.<br/>';
               iError++;
            }
            

            if(iError > 0)
            {
               uiAddGratuityModal.find('.error_message').removeClass('hidden');
               uiAddGratuityModal.find('.error_message').html(sErrorMsg);
               return false;     
            }
            else
            {
               uiAddGratuityModal.find('.error_message').addClass('hidden');
               uiAddGratuityModal.find('.error_message').html('');
               return true;
            }
    
        },

        /**
         * insert_gratuity
         * @description This function will insert_gratuity
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        insert_gratuity : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "esop/insert_gratuity",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddGratuityModal.find('.error_message').addClass('hidden').html('');
                                uiAddGratuityModal.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiAddGratuityModal.hasClass('showed'))
                                    {
                                        uiAddGratuityModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'esop/esop_gratuity_list';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiAddGratuityModal.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },


        
        /**
         * bind_get_gratuity
         * @description This function will bind_get_gratuity
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        bind_get_gratuity : function(uiBatchTemplate , iEsopId){
                var oGetParams = {
                        "search_field" : 'esop_id',
                        "keyword" : iEsopId,
                        "limit" : 30
                };

                roxas.esop.get_gratuity(oGetParams, undefined, uiBatchTemplate); 

                roxas.esop.bind_gratuity_action_events(uiBatchTemplate);
       
        },

        /**
         * bind_gratuity_action_events
         * @description This function will bind_gratuity_action_events
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        bind_gratuity_action_events : function(uiBatchTemplate){

                uiBatchTemplate.on("click",'[modal-target="approve-gratuity"]', function() {       
                    var uiThis = $(this);  
                    var uiParent = uiThis.closest('.tmp_gratuity_list');  
                    var iID = uiParent.attr('data-id');
                    var sEsopName = uiParent.attr('data-esop-name');
                    var iEsopId = uiParent.attr('data-esop-id');
                    var sCurrencyName = uiParent.attr('data-currency-name');
                    var sPricePerShare = uiParent.attr('data-price-per-share');
                    
                    uiApproveGratuityModal.data('data-id',iID);
                    uiApproveGratuityModal.data('data-price-per-share',sPricePerShare);
                    uiApproveGratuityModal.data('data-currency-name',sCurrencyName);
                    uiApproveGratuityModal.data('data-esop-id',iEsopId);
                    uiApproveGratuityModal.data('data-esop-name',sEsopName);
                    uiApproveGratuityModal.find('[data-label="esop_name"]').text(sEsopName);
                    uiApproveGratuityModal.find('[data-label="price_per_share"]').text(sCurrencyName +" "+ sPricePerShare +" / share");
                    
                 });

                 uiApproveGratuityModal.off('click').on("click",'.btn-approve-gratuity', function() {       
                    var uiBtn = $(this);  
                    var iID = uiApproveGratuityModal.data('data-id');
                    var iStatus = 1;
                    console.log(iID)

                    var oParams = {
                                "id" : iID,
                                "status" : iStatus,
                                "price_per_share" : uiApproveGratuityModal.data('data-price-per-share'),
                                "currency_name" : uiApproveGratuityModal.data('data-currency-name'),
                                "esop_id" : uiApproveGratuityModal.data('data-esop-id'),
                                "esop_name" : uiApproveGratuityModal.data('data-esop-name')
                    };

                    roxas.esop.approve_gratuity(oParams,uiBtn);

                 });

                 
                 uiBatchTemplate.on("click",'[modal-target="reject-grant"]', function() {       
                    var uiThis = $(this);  
                    var uiParent = uiThis.closest('.tmp_gratuity_list');  
                    var iID = uiParent.attr('data-id');
                    var sEsopName = uiParent.attr('data-esop-name');
                    var iEsopId = uiParent.attr('data-esop-id');
                    var sCurrencyName = uiParent.attr('data-currency-name');
                    var sPricePerShare = uiParent.attr('data-price-per-share');
                    
                    uiRejectGratuityModal.data('data-id',iID);
                    uiRejectGratuityModal.data('data-price-per-share',sPricePerShare);
                    uiRejectGratuityModal.data('data-currency-name',sCurrencyName);
                    uiRejectGratuityModal.data('data-esop-id',iEsopId);
                    
                    var uiRejectReason = uiRejectGratuityModal.find('textarea[name="reject_reason"]');
                    uiRejectReason.removeClass('input-error');  
                    uiRejectGratuityModal.find('.error_message').addClass('hidden').html('');
                 });

                 uiRejectGratuityModal.off('click').on("click",'.btn-reject-grant', function() {       
                    var uiBtn = $(this);  
                    var iID = uiRejectGratuityModal.data('data-id');
                    var uiRejectReason = uiRejectGratuityModal.find('textarea[name="reject_reason"]');
                    var iStatus = 3;

                    if(uiRejectReason.val() !=="")
                    {
                          uiRejectReason.removeClass('input-error');  
                          uiRejectGratuityModal.find('.error_message').addClass('hidden').html('');
                            
                            var oParams = {
                                        "id" : iID,
                                        "reject_reason" : uiRejectReason.val(),
                                        "status" : iStatus,
                                        "price_per_share" : uiRejectGratuityModal.data('data-price-per-share'),
                                        "currency_name" : uiRejectGratuityModal.data('data-currency-name'),
                                        "esop_id" : uiRejectGratuityModal.data('data-esop-id')
                            };

                            roxas.esop.reject_gratuity(oParams,uiBtn);
                    }

                    else
                    {
                          uiRejectReason.addClass('input-error');  
                          uiRejectGratuityModal.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> Reject reason is required.</p>');
                    }

                    
                    
                 });

                 uiBatchTemplate.on("click",'[modal-target="view-reject-reason"]', function() {       
                    var uiThis = $(this);  
                    var uiParent = uiThis.closest('.tmp_gratuity_list');  
                    var iID = uiParent.attr('data-id');
                    var sEsopName = uiParent.attr('data-esop-name');
                    var sCurrencyName = uiParent.attr('data-currency-name');
                    var sPricePerShare = uiParent.find('[data-label="price_per_share"]').text();
                    var sRejectReason = uiParent.attr('data-reject-reason');
                    var sGrantDate = uiParent.find('[data-label="grant_date"]').text();
                    
                    uiViewGratuityRejectReasonModal.find('[data-label="esop_name"]').text(sEsopName);
                    uiViewGratuityRejectReasonModal.find('[data-label="price_per_share"]').text(sPricePerShare);
                    uiViewGratuityRejectReasonModal.find('[data-label="grant_date"]').text(sGrantDate);
                    uiViewGratuityRejectReasonModal.find('[data-label="reject_reason"]').text(sRejectReason);
                 });

                 uiBatchTemplate.on("click",'[modal-target="send-gratuity-to-employees"]', function() {       
                    var uiThis = $(this);  
                    var uiParent = uiThis.closest('.tmp_gratuity_list');  
                    var iID = uiParent.attr('data-id');
                    var sEsopName = uiParent.attr('data-esop-name');
                    var iEsopID = uiParent.attr('data-esop-id');
                    var iVestingYears = uiParent.attr('data-vesting-years');
                    var sCurrencyName = uiParent.attr('data-currency-name');
                    var iCurrencyID = uiParent.attr('data-currency-id');
                    var sPricePerShare = uiParent.attr('data-price-per-share');
                    
                    uiSendGratuityToEmployeesModal.data('data-id',iID);
                    uiSendGratuityToEmployeesModal.data('data-price-per-share',sPricePerShare);
                    uiSendGratuityToEmployeesModal.data('data-esop-id',iEsopID);
                    uiSendGratuityToEmployeesModal.data('data-currency-name',sCurrencyName);
                    uiSendGratuityToEmployeesModal.data('data-vesting-years',iVestingYears);
                    uiSendGratuityToEmployeesModal.data('data-currency-id',iCurrencyID);
                    uiSendGratuityToEmployeesModal.data('data-esop-name',sEsopName);
                    
                 });

                 uiSendGratuityToEmployeesModal.off('click').on("click",'.btn-send-gratuity-to-employees', function() {       
                    var uiBtn = $(this);  
                    var iID = uiSendGratuityToEmployeesModal.data('data-id');
                    var sPricePerShare = uiSendGratuityToEmployeesModal.data('data-price-per-share');
                    var iEsopID = uiSendGratuityToEmployeesModal.data('data-esop-id');
                    var sEsopName = uiSendGratuityToEmployeesModal.data('data-esop-name');
                    var iVestingYears = uiSendGratuityToEmployeesModal.data('data-vesting-years');
                    var iStatus = 2;

                    var oParams = {
                                "id" : iID,
                                "esop_id" : iEsopID,
                                "esop_name" : sEsopName,
                                "price_per_share" : sPricePerShare,
                                "vesting_years" : iVestingYears,
                                "status" : iStatus,
                                "currency_name" : uiSendGratuityToEmployeesModal.data('data-currency-name'),
                                "currency" : uiSendGratuityToEmployeesModal.data('data-currency-id')
                    };

                    roxas.esop.send_gratuity_to_employees(oParams,uiBtn);

                 });

                 
                
        },

        /**
         * bind_edit_esop_batch_events
         * @description This function is for binding events for edit esop info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_edit_esop_batch_events : function(){

            /*save edit esop*/
            uiEditEsopBatchBtn.on('click', function(){
                uiEditEsopBatchForm.submit();
            });

            /*prevent on submit of form*/
            uiEditEsopBatchForm.on('submit', function (e) {
                e.preventDefault();
            });

            /*add a validation to editing esop*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiEditEsopBatchBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiEditEsopBatchModal.find('.edit_esop_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiEditEsopBatchModal.find('.edit_esop_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiEditEsopBatchBtn, true);
                    roxas.esop.assemble_edit_esop_batch_information(uiEditEsopBatchModal, uiEditEsopBatchBtn);
                    
                }
            };

            /*bind the validation to edit esop form*/
            cr8v.validate_form(uiEditEsopBatchForm, oValidationConfig);
        },

        /**
         * get_gratuity
         * @description This function is for getting and rendering gratuity on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_gratuity : function(oParams, uiThis, uiBatchTemplate){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/get_gratuity",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        //console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                         uiBatchTemplate.find('.tmp_gratuity_list:not(.template)').remove();
                         uiBatchTemplate.find('.no_results_found').addClass('hidden');

                            if(oData.status == true)
                            {                             
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i < max; i++)
                                        {
                                            var oEsopData = oData.data[i];

                                            var uiTemplateList = uiBatchTemplate.find('.tmp_gratuity_list.template').clone().removeClass('template hidden');
                                            var uiManipulatedTemplateList = roxas.esop.manipulate_gratuity_template_list(oEsopData, uiTemplateList);

                                            uiBatchTemplate.find('[data-container="gratuity_list_container"]').prepend(uiManipulatedTemplateList);
                                        }
                                    }
                                    else
                                    {
                                        uiBatchTemplate.find('.no_results_found').removeClass('hidden');
                                    }

                            }
                            else
                            {
                                uiBatchTemplate.find('.no_results_found').removeClass('hidden');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };
                roxas.esop.ajax(oAjaxConfig);

            }
        },
        
         
        /**
         * manipulate_gratuity_template_list
         * @description This function will put the data of the list view
         * @dependencies N/A
         * @param {object} oData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        manipulate_gratuity_template_list : function(oData, uiTemplate){
            if(cr8v.var_check(oData) && cr8v.var_check(uiTemplate))
            {
//                 console.log(oData)
                var sGrantDate = moment(oData.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');
                var sDividendDate = moment(oData.dividend_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');
                
                uiTemplate.attr('data-esop-id', oData.esop_id);
                uiTemplate.attr('data-id', oData.id);
                uiTemplate.attr('data-currency-name',oData.currency_name);
                uiTemplate.attr('data-currency-id',oData.currency_id);
                uiTemplate.attr('data-price-per-share',oData.price_per_share);
                uiTemplate.attr('data-esop-name',oData.esop_name);
                uiTemplate.attr('data-reject-reason',oData.reject_reason);
                uiTemplate.attr('data-vesting-years',oData.vesting_years);

                if(oData.status == 0)
                {
                   uiTemplate.find('[data-label="status"]').addClass('color-pending').text('PENDING');
                }
                else if (oData.status == 1)
                {
                   uiTemplate.find('[data-label="status"]').addClass('color-final').text('APPROVED');
                }
                else if (oData.status == 2)
                {
                   uiTemplate.find('[data-label="status"]').addClass('color-complete').text('GRATUITY SENT');
                }
                else if (oData.status == 3)
                {
                   uiTemplate.find('[data-label="status"]').addClass('color-reject').text('REJECTED');          
                }
                
                //buttons
                if(iUserRole == 4)//admin
                {
                        if(oData.status != 0)
                        {
                              uiTemplate.find('[data-container="admin_gratuity_action_btns"]').remove();  
                        }
                }

                else if(iUserRole == 3)//hr
                {
                        if(oData.status == 0 || oData.status == 2)
                        {
                            uiTemplate.find('[data-container="view_reject_reason"]').remove();
                            uiTemplate.find('[data-container="send_gratuity_to_employees"]').remove(); 
                        }           
                        else if(oData.status == 1)
                        {
                              uiTemplate.find('[data-container="view_reject_reason"]').remove();  
                        }
                        else if(oData.status == 3)
                        {
                              uiTemplate.find('[data-container="send_gratuity_to_employees"]').remove();  
                        }
                }

                
                uiTemplate.find('[data-label="esop_name"]').text(oData.esop_name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="dividend_date"]').text(sDividendDate);
                uiTemplate.find('[data-label="price_per_share"]').text(oData.currency_name +" "+ oData.price_per_share + " / share");
                var sTotalValueOfGratuity = number_format(oData.total_value_of_gratuity_given,2);
                sTotalValueOfGratuity = oData.currency_name +" "+ sTotalValueOfGratuity;

                uiTemplate.find('[data-label="total_value_of_gratuity_given"]').text(sTotalValueOfGratuity);

                return uiTemplate;
            }
        },

        /**
         * approve_gratuity
         * @description This function will approve_gratuity
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        approve_gratuity : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "esop/approve_gratuity",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiApproveGratuityModal.find('.error_message').addClass('hidden').html('');
                                uiApproveGratuityModal.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiApproveGratuityModal.hasClass('showed'))
                                    {
                                        uiApproveGratuityModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'esop/esop_gratuity_list';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiApproveGratuityModal.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * reject_gratuity
         * @description This function will approve_gratuity
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        reject_gratuity : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "esop/reject_gratuity",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiRejectGratuityModal.find('.error_message').addClass('hidden').html('');
                                uiRejectGratuityModal.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiRejectGratuityModal.hasClass('showed'))
                                    {
                                        uiRejectGratuityModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'esop/view_esop_batch';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiRejectGratuityModal.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        
        /**
         * send_gratuity_to_employees
         * @description This function will send_gratuity_to_employees
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        send_gratuity_to_employees : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    //"processData" : false,
                    //"contentType" : false,
                    "url"    : roxas.config('url.server.base') + "esop/send_gratuity_to_employees",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiSendGratuityToEmployeesModal.find('.error_message').addClass('hidden').html('');
                                uiSendGratuityToEmployeesModal.find('.success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiSendGratuityToEmployeesModal.hasClass('showed'))
                                    {
                                        uiSendGratuityToEmployeesModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'esop/view_esop_batch';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiSendGratuityToEmployeesModal.find('.error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },


         /**
         * clear_edit_esop_batch_modal
         * @description This function is for clearing data in edit esop modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_edit_esop_batch_modal : function(){
            /*clear data*/
            uiEditEsopBatchModal.find('input').removeClass('input-error');
            uiEditEsopBatchModal.find('.edit_esop_success_message').addClass('hidden');
            uiEditEsopBatchModal.find('.edit_esop_error_message').addClass('hidden');
        },

        /**
         * assemble_edit_esop_information
         * @description This function will assemble edit esop information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_edit_esop_batch_information : function(uiEditEsopBatchModal, uiEditEsopBatchBtn){
            var oParams = {
                'id' : uiEditEsopBatchModal.attr('data-esop-id'),
                'type' : 'esop_normal_edit',
                'data' : [{
                    'name' : uiEditEsopBatchModal.find('input[name="esop_name"]').val()
                }]
            };

            roxas.esop.edit_esop(oParams, uiEditEsopBatchBtn, true);
        },

        /**
         * bind_set_offer_batch_events
         * @description This function is for binding events for set offer expiration
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_set_offer_batch_events : function(){

            uiSetOfferExpBatchBtn.on('click', function(){
                var sDate = uiSetOfferExpBatchForm.find('input[name="offer_expiration"]').val();
                var bValidDate = cr8v.validate_date(sDate, 'MM/DD/YYYY');

                if(sDate != '')
                {
                    if(bValidDate)
                    {
                        cr8v.get_time(function (sTime){
                            var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
                            var oCurrentDate = moment(sCurrentDate, 'YYYY-MM-DD');
                            var oOfferExpiration = moment(sDate, 'MM/DD/YYYY');

                            if(oOfferExpiration.diff(oCurrentDate, 'days') > 0)
                            {
                                cr8v.show_spinner(uiSetOfferExpBatchBtn, true);
                                roxas.esop.assemble_set_offer_batch_expiration(uiSetOfferExpBatchModal, uiSetOfferExpBatchBtn);
                            }
                            else
                            {
                                var uiErrorContainer = uiSetOfferExpBatchModal.find('.set_offer_expiration_error_message');
                                var sErrorMessages = '<p class="font-15 error_message">Date below has already passed. Please choose another date.</p>';
                                cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                            }
                        }); 
                    }
                    else
                    {
                        var uiErrorContainer = uiSetOfferExpBatchModal.find('.set_offer_expiration_error_message');
                        var sErrorMessages = '<p class="font-15 error_message">Date is invalid.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);                    
                    }
                }
                else
                {
                    var uiErrorContainer = uiSetOfferExpBatchModal.find('.set_offer_expiration_error_message');
                    var sErrorMessages = '<p class="font-15 error_message">Date is required.</p>';
                    uiSetOfferExpBatchForm.find('input[name="offer_expiration"]').addClass('input-error');
                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);                    
                }
            });

            cr8v.get_time(function (sTime){
                var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY');
                var oCurrentDate = moment(sCurrentDate, 'MM/DD/YYYY').add(1, 'days');

                var iMinDate = oCurrentDate.format('MM/DD/YYYY');
                uiSetOfferExpBatchForm.find('input[name="offer_expiration"]').datetimepicker({pickTime: false, minDate: iMinDate});
                if(cr8v.var_check(uiSetOfferExpBatchForm.find('input[name="offer_expiration"]').data("DateTimePicker")))
                {
                    uiSetOfferExpBatchForm.find('input[name="offer_expiration"]').data("DateTimePicker").setMinDate(iMinDate);
                    uiSetOfferExpBatchForm.find('input[name="offer_expiration"]').data("DateTimePicker").setDate(iMinDate);
                }

                uiAddNewEmployeeModal.find('input[name="offer_expiration"]').datetimepicker({pickTime: false, minDate: iMinDate});
                if(cr8v.var_check(uiAddNewEmployeeModal.find('input[name="offer_expiration"]').data("DateTimePicker")))
                {
                    uiAddNewEmployeeModal.find('input[name="offer_expiration"]').data("DateTimePicker").setMinDate(iMinDate);
                    uiAddNewEmployeeModal.find('input[name="offer_expiration"]').data("DateTimePicker").setDate(iMinDate);
                }
            });
        },

        /**
         * assemble_set_offer_batch_expiration
         * @description This function will assemble edit esop information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_set_offer_batch_expiration : function(uiSetOfferExpBatchModal, uiSetOfferExpBatchBtn){
            var oParams = {
                'id' : uiSetOfferExpBatchModal.attr('data-esop-id'),
                'esop_name' : uiSetOfferExpBatchModal.attr('data-esop-name'),
                'type' : 'set_offer_expiration',
                'data' : [{
                    'offer_expiration' : uiSetOfferExpBatchModal.find('input[name="offer_expiration"]').val()
                }]
            };

            roxas.esop.set_offer_expiration(oParams, uiSetOfferExpBatchBtn, true);
        },

        /**
         * bind_send_letter_batch_events
         * @description This function is for binding events for send offer letter
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_send_letter_batch_events : function(){
            uiEsopSendLetterBatchBtn.on('click', function(){
                roxas.esop.assemble_send_batch_letter(uiEsopSendLetterBatchModal, uiEsopSendLetterBatchBtn);
            });
        },

        /**
         * assemble_send_batch_letter
         * @description This function is for binding events for send offer letter
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_send_batch_letter : function(uiEsopSendLetterBatchModal, uiEsopSendLetterBatchBtn){
            var oParams = {
                "esop_id" : uiEsopSendLetterBatchModal.attr('data-esop-id'),
                "esop_name" : uiEsopSendLetterBatchModal.attr('data-esop-name')
            };

            roxas.esop.update_stock_offer_status(oParams, uiEsopSendLetterBatchBtn, true);
        },

        /**
         * bind_add_new_employee_events
         * @description This function is for binding add new employee events
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_add_new_employee_events : function () {

            /*save edit esop*/
            uiAddNewEmployeeBtn.on('click', function(){
                uiAddNewEmployeeForm.submit();
            });

            /*prevent on submit of form*/
            uiAddNewEmployeeForm.on('submit', function (e) {
                e.preventDefault();
            });

            /*add a validation to add new employee*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiAddNewEmployeeBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiAddNewEmployeeModal.find('.add_new_employee_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiAddNewEmployeeModal.find('.add_new_employee_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    var iEsopTotalUnavailed = parseFloat(cr8v.remove_commas(uiAddNewEmployeeModal.find('[name="total_share_unavailed"]').val()));
                    var iTotalQty = parseFloat(cr8v.remove_commas(uiAddNewEmployeeModal.find('[name="total_share_qty"]').val()));

                    if(iTotalQty > iEsopTotalUnavailed)
                    {
                        var sErrorMessages = '<p class="font-15 error_message">Shares you want to avail exceeds the total share unavailed.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                    else
                    {
                        var sOfferExpirationDate = uiAddNewEmployeeModal.find('input[name="offer_expiration"]').val();
                        var bValidOfferExpiration = cr8v.validate_date(sOfferExpirationDate, 'MM/DD/YYYY');

                        if(bValidOfferExpiration)
                        {
                            cr8v.get_time(function (sTime){
                                var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
                                var oCurrentDate = moment(sCurrentDate, 'YYYY-MM-DD');
                                var oOfferExpiration = moment(sOfferExpirationDate, 'MM/DD/YYYY');

                                if(oOfferExpiration.diff(oCurrentDate, 'days') > 0)
                                {
                                    if(uiAddNewEmployeeModal.find('[name="copy_initial"]').is(':checked'))
                                    {
                                        cr8v.show_spinner(uiAddNewEmployeeBtn, true);
                                        roxas.esop.assemble_add_new_employee_information(uiAddNewEmployeeModal, uiAddNewEmployeeBtn, true);
                                    }
                                    else
                                    {
                                        var sGrantDate = uiAddNewEmployeeModal.find('input[name="grant_date"]').val();
                                        var bValidGrantDate = cr8v.validate_date(sGrantDate, 'MM/DD/YYYY');

                                        if(bValidGrantDate)
                                        {
                                            cr8v.show_spinner(uiAddNewEmployeeBtn, true);
                                            roxas.esop.assemble_add_new_employee_information(uiAddNewEmployeeModal, uiAddNewEmployeeBtn);
                                        }
                                        else
                                        {
                                            var sErrorMessages = '<p class="font-15 error_message">Grant Date is invalid.</p>';
                                            cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                        }
                                    }
                                }
                                else
                                {
                                    var sErrorMessages = '<p class="font-15 error_message">Offer Expiration date has already passed. Please choose another date.</p>';
                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            });
                        }
                        else
                        {
                            var sErrorMessages = '<p class="font-15 error_message">Offer Expiration is invalid.</p>';
                            cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                        }
                    }
                }
            };

            cr8v.validate_form(uiAddNewEmployeeForm, oValidationConfig);

            uiAddNewEmployeeModal.on('change', '[name="copy_initial"]', function(){
                var uiThis = $(this);

                if(uiThis.is(':checked'))
                {
                    uiAddNewEmployeeModal.find('tr.copy_initial').addClass('hidden');
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('input').removeClass('input-error');
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="grant_date"]').removeAttr('datavalid labelinput');
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="price_per_share"]').removeAttr('datavalid labelinput');
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="vesting_years"]').removeAttr('datavalid labelinput');
                }
                else
                {
                    uiAddNewEmployeeModal.find('tr.copy_initial').removeClass('hidden');
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('input').removeClass('input-error');
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="grant_date"]').attr({'datavalid' : 'required', 'labelinput' : 'Grant Date'});
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="price_per_share"]').attr({'datavalid' : 'required', 'labelinput' : 'Price Per Share'});
                    uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="vesting_years"]').attr({'datavalid' : 'required', 'labelinput' : 'Vesting Years'});
                    uiClassCurrencyDropdown.find('.option:first').trigger('click');
                }
            });
            
            uiAddNewEmployeeModalBtn.on('click', function(){
                roxas.esop.clear_add_new_employee_modal();
            });

            uiClassCompanyDropdown.find('.frm-custom-dropdown-txt').find('input').val('').attr('disabled', true).attr({'datavalid' : 'required', 'labelinput' : 'Company'});
            uiClassEmployeeDropDown.find('.frm-custom-dropdown-txt').find('input').val('').attr('disabled', true).attr({'datavalid' : 'required', 'labelinput' : 'Employee'});
            uiClassEsopDropDown.find('.frm-custom-dropdown-txt').find('input').val('').attr('disabled', true).attr({'datavalid' : 'required', 'labelinput' : 'Esop'});

            cr8v.input_numeric(uiAddNewEmployeeModal.find('[name="total_share_qty"]'));
            cr8v.input_numeric(uiAddNewEmployeeModal.find('[name="price_per_share"]'));
            cr8v.input_numeric(uiAddNewEmployeeModal.find('[name="vesting_years"]'));

            uiAddNewEmployeeModal.on('blur', '[name="total_share_qty"], [name="price_per_share"]', function(){
                var uiThis = $(this);
                if(uiThis.val() != '')
                {
                    uiThis.val(number_format(uiThis.val(), 2));
                }
            });

            uiClassCompanyDropdown.on('click', '.option.companies', function(){
                var uiThis = $(this);
                var iCompanyID = uiThis.attr('data-value');

                uiClassCompanyDropdown.attr('company_id', iCompanyID);
                uiClassCompanyDropdown.attr('offer_letter_id', uiThis.attr('data-company-offer-id'));
                uiClassCompanyDropdown.attr('acceptance_letter_id', uiThis.attr('data-company-acceptance-id'));
                uiClassCompanyDropdown.attr('allotment', uiThis.attr('data-company-allotment'));

                if(cr8v.var_check(iCompanyID))
                {
                    uiClassEmployeeDropDown.removeAttr('employee_id');

                    uiClassEmployeeDropDown.find('.frm-custom-dropdown-txt').find('input').val('');
                    uiClassEmployeeDropDown.find('.option.employees').addClass('hidden');
                    uiClassEmployeeDropDown.find('.option.employees[data-company-id="'+iCompanyID+'"]').removeClass('hidden');
                    uiClassEsopDropDown.find('.option.esop').removeClass('hidden');
                }
            });

            uiClassEmployeeDropDown.on('click', '.option.employees', function(){
                var uiThis = $(this);
                var iEmployeeID = uiThis.attr('data-value');

                uiClassEmployeeDropDown.attr('employee_id', iEmployeeID);
            });

            uiClassEsopDropDown.on('click', '.option.esop', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.attr('data-value');
                var iEsopTotalUnavailed = uiThis.attr('data-esop-available');
                var iEsopCompanyTotalUnavailed = $('.esop_batch:not(.template)[data-esop-id="'+iEsopID+'"]').find('[data-company-id="'+uiClassCompanyDropdown.attr('company_id')+'"]').find('[data-label="company_total_shares_unavailed"]').text();
                var iEsopTotalUnavailed = $('.esop_batch:not(.template)[data-esop-id="'+iEsopID+'"]').find('[data-label="esop_total_shares_unavailed"]:first').text();

                uiClassEsopDropDown.attr('esop_id', iEsopID);
                uiClassEsopDropDown.attr('esop_grant_date', uiThis.attr('data-esop-grant-date'));
                uiClassEsopDropDown.attr('esop_price_per_share', uiThis.attr('data-esop-price-per-share'));
                uiClassEsopDropDown.attr('esop_vesting_years', uiThis.attr('data-esop-vesting-year'));
                uiClassEsopDropDown.attr('esop_currency', uiThis.attr('data-esop-currency'));

                /*if adding a new employee and there are no company details for the specific esop because there are no distribution to that company*/
                /*if adding a new employee to the esop in another company even if the company chosen has no share distribution*/
                // uiAddNewEmployeeModal.find('[name="total_share_unavailed"]').val(number_format(cr8v.remove_commas(iEsopTotalUnavailed), 2));

                uiAddNewEmployeeModal.find('[name="total_share_unavailed"]').val(number_format(cr8v.remove_commas(iEsopCompanyTotalUnavailed), 2));
            });
        },

        /**
         * assemble_add_new_employee_information
         * @description This function will assemble add new employee information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_add_new_employee_information : function(uiAddNewEmployeeModal, uiAddNewEmployeeBtn, bCopyInitial){
            var oParams = {
                "esop_information" : {}, 
                "stock_offer_information" : {}, 
            };
            if(cr8v.var_check(bCopyInitial) && bCopyInitial)
            {
                oParams.esop_information = {
                    'name'              : uiClassEsopDropDown.find('.frm-custom-dropdown-txt').find('input').val(),
                    'grant_date'        : uiClassEsopDropDown.attr('esop_grant_date'),
                    'total_share_qty'   : uiAddNewEmployeeModal.find('input[name="total_share_qty"]').val(),
                    'price_per_share'   : uiClassEsopDropDown.attr('esop_price_per_share'),
                    'currency'          : uiClassEsopDropDown.attr('esop_currency'),
                    'vesting_years'     : uiClassEsopDropDown.attr('esop_vesting_years'),
                    'created_by'        : iUserID,
                    'status'            : 1,
                    'is_deleted'        : 0,
                    'parent_id'         : uiClassEsopDropDown.attr('esop_id'),
                    'is_special'        : 1,
                    'offer_expiration'  : uiAddNewEmployeeModal.find('[name="offer_expiration"]').val()
                };
            }
            else
            {
                oParams.esop_information = {
                    'name'              : uiClassEsopDropDown.find('.frm-custom-dropdown-txt').find('input').val(),
                    'grant_date'        : uiAddNewEmployeeModal.find('input[name="grant_date"]').val(),
                    'total_share_qty'   : uiAddNewEmployeeModal.find('input[name="total_share_qty"]').val(),
                    'price_per_share'   : uiAddNewEmployeeModal.find('input[name="price_per_share"]').val(),
                    'currency'          : uiClassCurrencyDropdown.attr('currency_id'),
                    'vesting_years'     : uiAddNewEmployeeModal.find('input[name="vesting_years"]').val(),
                    'created_by'        : iUserID,
                    'status'            : 1,
                    'is_deleted'        : 0,
                    'parent_id'         : uiClassEsopDropDown.attr('esop_id'),
                    'is_special'        : 1,
                    'offer_expiration'  : uiAddNewEmployeeModal.find('[name="offer_expiration"]').val()
                };
            }

            var uiBatchTemplate = $('.esop_batch[data-esop-id="'+uiClassEsopDropDown.attr('esop_id')+'"]');
            var iSpecialEsopIDs = [uiClassEsopDropDown.attr('esop_id')];
            
            if(uiBatchTemplate.find('.esop_special:not(.template)').length > 0)
            {
                $.each(uiBatchTemplate.find('.esop_special:not(.template)[data-user-id="'+uiClassEmployeeDropDown.attr('employee_id')+'"]'), function (key, elem){
                    iSpecialEsopIDs.push($(elem).attr('data-esop-id'));
                });
            }            
            // console.log(iSpecialEsopIDs.join());
            // cr8v.show_spinner(uiAddNewEmployeeBtn, false);
            // return;

            oParams.stock_offer_information = {
                "user_id" : uiClassEmployeeDropDown.attr('employee_id'),
                "esop_id" : iSpecialEsopIDs.join(), 
                "user_name" : uiClassEmployeeDropDown.find('.frm-custom-dropdown-txt').find('input').val(), 
                "employee_data" : []
            };

            var oEmployeeData = {
                // "esop_id": uiClassEsopDropDown.attr('esop_id'),
                "user_id": uiClassEmployeeDropDown.attr('employee_id'), 
                "stock_amount": uiAddNewEmployeeModal.find('input[name="total_share_qty"]').val(), 
                "offer_letter_id": uiClassCompanyDropdown.attr('offer_letter_id'),
                "acceptance_letter_id": uiClassCompanyDropdown.attr('acceptance_letter_id')
            };
            oParams.stock_offer_information.employee_data.push(oEmployeeData);

            // var oCompanyData = {
            //     "esop_id": iEsopID,
            //     "company_id" : iCompanyID,
            //     "department_id" : '',
            //     "allotment" : iTotalAllotment
            // };
            // oParams.stock_offer_information.allotment_data.push(oCompanyData);

            roxas.esop.add_new_employee(oParams, uiAddNewEmployeeBtn, undefined, true);
        },

        /**
         * clear_add_new_employee_modal
         * @description This function is for clearing data in add new employee modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_add_new_employee_modal : function(){
            /*clear data*/
            uiAddNewEmployeeModal.find('input').removeClass('input-error').val('');
            uiAddNewEmployeeModal.find('.add_new_employee_error_message').addClass('hidden');
            uiAddNewEmployeeModal.find('.add_new_employee_success_message').addClass('hidden');

            uiAddNewEmployeeModal.find('[name="copy_initial"]').prop('checked', true);
            uiAddNewEmployeeModal.find('tr.copy_initial').addClass('hidden');
            uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="grant_date"]').removeAttr('datavalid labelinput');
            uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="price_per_share"]').removeAttr('datavalid labelinput');
            uiAddNewEmployeeModal.find('tr.copy_initial').find('[name="vesting_years"]').removeAttr('datavalid labelinput');

            uiClassEsopDropDown.find('.option.esop').addClass('hidden');
            uiClassEmployeeDropDown.find('.option.employees').addClass('hidden');

            cr8v.get_time(function (sTime){
                var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY');
                var oCurrentDate = moment(sCurrentDate, 'MM/DD/YYYY').add(1, 'days');
                var iMinDate = oCurrentDate.format('MM/DD/YYYY');

                uiAddNewEmployeeModal.find('input[name="offer_expiration"]').datetimepicker({pickTime: false, minDate: iMinDate});
                if(cr8v.var_check(uiAddNewEmployeeModal.find('input[name="offer_expiration"]').data("DateTimePicker")))
                {
                    uiAddNewEmployeeModal.find('input[name="offer_expiration"]').data("DateTimePicker").setMinDate(iMinDate);
                    uiAddNewEmployeeModal.find('input[name="offer_expiration"]').data("DateTimePicker").setDate(iMinDate);
                }

            });
        },

        /**
         * add_new_employee
         * @description This function will add new employee esop and stock offer
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_new_employee : function(oParams, uiBtn, bFromEsopBatch){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/add_new_employee",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                    uiAddNewEmployeeModal.find('.add_new_employee_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');

                                    setTimeout(function(){
                                        /*clear data*/
                                        if(uiAddNewEmployeeModal.hasClass('showed'))
                                        {
                                            roxas.esop.clear_add_esop_modal();
                                            uiAddNewEmployeeModal.find('.close-me').trigger('click');
                                        }
                                        window.location = roxas.config('url.server.base') + 'esop/view_esop_batch';
                                    }, 1000);
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiAddNewEmployeeModal.find('.add_new_employee_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * bind_batch_upload_share_distribution_events
         * @description This function is for binding upload share distribution events
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_batch_upload_share_distribution_events : function () {
            uiTriggerBatchUploadShareDistribution.on('click', function(){
                $(this).next('label').find('input[name="upload_batch_share_distribution"]').trigger('click');
            });

            /*upload user list*/
            var last_file_path = '';
            var last_file_name = '';
            uiUploadBatchShareDistributionModal.on('change', 'input[name="upload_batch_share_distribution"]', function(){
                var uiThis = $(this);
                var file = uiUploadBatchShareDistributionModal.find('input[type=file][name="upload_batch_share_distribution"]')[0];
                var file_path = '';
                var file_name = '';
                var formdata = false;
                var uiLogoContainer = uiUploadBatchShareDistributionModal.find('[data-container="file_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();
                var uiErrorContainer = uiUploadBatchShareDistributionModal.find('.upload_batch_share_distribution_error_message');

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'csv'/* || sValueExt == 'xls' || sValueExt == 'xlsx'*/)
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('csv_file', file.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'esop/upload_share_distribution_csv',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData))
                                    {
                                        uiUploadBatchShareDistributionBtn.attr('disabled', true);
                                        uiThis.val('');
                                        cr8v.add_error_message(false, uiErrorContainer);   

                                        if(cr8v.var_check(oData.status) && oData.status == true)
                                        {
                                            if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                            {
                                                uiUploadBatchShareDistributionModal.find('.upload_batch_share_distribution_success_message').removeClass('hidden').show().delay(1000).fadeOut()
                                                    .html('<p class="font-15 success_message"> ' + oData.message[0] + '</p>');
                                            }

                                            var sOrigFileName = (cr8v.var_check(oData.data.original_file_name)) ? oData.data.original_file_name : 'No file uploaded yet';
                                            var sFilePath = (cr8v.var_check(oData.data.file_path) ? oData.data.file_path : '');

                                            if(cr8v.var_check(sBatchShareDistributionFilePath) && sBatchShareDistributionFilePath.length > 0)
                                            {
                                                var sFilePathForDelete = sBatchShareDistributionFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                                                sFilePathForDelete = './assets/uploads/share_distribution/csv/' + sFilePathForDelete;

                                                var oParams = {
                                                    "file_path" : sFilePathForDelete
                                                };

                                                cr8v.delete_file(oParams, function(oData){
                                                
                                                });
                                            }

                                            sBatchShareDistributionFilePath = sFilePath;
                                            sBatchShareDistributionFileName = sOrigFileName;

                                            uiUploadBatchShareDistributionModal.find('#file_name').html('<i>'+sOrigFileName+'</i>');

                                            uiUploadBatchShareDistributionBtn.attr('disabled', false);
                                        }
                                        else
                                        {
                                            sBatchShareDistributionFilePath = '';
                                            sBatchShareDistributionFileName = '';

                                            uiUploadBatchShareDistributionModal.find('#file_name').html('<i>No File Uploaded</i>');

                                            var sErrorMessages = '';
                                            for(var i = 0, max = oData.message.length; i < max; i++)
                                            {
                                                var message = oData.message[i];
                                                sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                            }

                                            cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                        }
                                    }
                                    else
                                    {
                                        file_path = last_file_path;
                                        file_name = last_file_name;
                                    }
                                },
                                complete: function(){

                                }
                            };

                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        sBatchShareDistributionFilePath = '';
                        sBatchShareDistributionFileName = '';
                        uiUploadBatchShareDistributionModal.find('#file_name').html('<i>No File Uploaded</i>');
                        uiUploadBatchShareDistributionBtn.attr('disabled', true);
                        var sErrorMessages = '<p class="font-15 error_message">Please upload the correct template file.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });
    
            uiUploadBatchShareDistributionBtn.on('click', function(){
                var iEsopID = uiUploadBatchShareDistributionModal.attr('data-esop-id');
                roxas.esop.view_batch_upload_share_distribution(iEsopID);
            });

            uiConfirmBatchUploadBtn.on('click', function(){
                var oGetCsvParams = {
                    "file_path" : localStorage.getItem('upload_batch_share_distribution_path'),
                    "esop_id" : uiUploadBatchShareDistributionModal.attr('data-esop-id'),
                    "esop_batch_parent_id" : localStorage.getItem('esop_batch_parent_id')
                };

                cr8v.show_spinner(uiConfirmBatchUploadBtn, true);

                roxas.esop.open_batch_csv(oGetCsvParams, undefined, true, function(oData){

                    cr8v.show_spinner(uiConfirmBatchUploadBtn, false);

                    if(cr8v.var_check(oData))
                    {
                        var iErrorCtr = 0;

                        if(oData.status == true)
                        {
                            if(oData.data.length > 0)
                            {                                    
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oShareDistributionInfo = oData.data[i];

                                    var iRow = i + 1;

                                    if(oShareDistributionInfo.valid == 0 && count(oShareDistributionInfo.error_message) > 0)
                                    {
                                        iErrorCtr++;
                                    }
                                }

                                if(iErrorCtr > 0)
                                {
                                    uiConfirmBatchUploadBtn.attr('disabled', true);
                                }
                                else
                                {
                                    uiConfirmBatchUploadBtn.attr('disabled', false);
                                    roxas.esop.assemble_batch_share_distribution_information(uiConfirmBatchUploadBtn);
                                }
                            }
                        }
                    }
                });
            });

            uiCancelBatchUploadBtn.on('click', function(){
                if(cr8v.var_check(localStorage.getItem('upload_batch_share_distribution_path')) && localStorage.getItem('upload_batch_share_distribution_path') != null)
                {
                    var sFilePath = localStorage.getItem('upload_batch_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                    sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    localStorage.removeItem('upload_batch_share_distribution_path');
                    localStorage.removeItem('upload_batch_share_distribution_file_name');

                    cr8v.delete_file(oParams, function(oData){
                        window.location = roxas.config('url.server.base') + 'esop/view_esop_batch';
                    });
                }
            });

            uiUploadBatchShareDistributionModal.on('click', '.close-me', function(){
                if(cr8v.var_check(sBatchShareDistributionFilePath) && sBatchShareDistributionFilePath.length > 0)
                {
                    var sFilePath = sBatchShareDistributionFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                    sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    sBatchShareDistributionFilePath = '';
                    sBatchShareDistributionFileName = '';

                    cr8v.delete_file(oParams, function(oData){
                        
                    });
                }
            });
        },

        /**
         * assemble_batch_share_distribution_information
         * @description This function is for assembling the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_batch_share_distribution_information : function (uiConfirmBatchUploadBtn){
            var iEsopID = $('[data-container="esop_information"]').attr('data-esop-id');
            var sEsopName = $('[data-container="esop_information"]').attr('data-esop-name');

            var oParams = { "esop_id" : iEsopID,"esop_name" : sEsopName, "employee_data" : [], "allotment_data" : [] };

            uiUploadBatchShareDistributionContainer.find('.upload_batch_share_distribution:not(.template)').each(function (key, elem){
                var iCompanyID = $(elem).data('company_id');

                var oData = {
                    "esop_id" : iEsopID,
                    "user_id" : $(elem).data('employee_id'),
                    "stock_amount" : cr8v.remove_commas(number_format(parseFloat($(elem).data('alloted_shares')), 2)),
                    "offer_letter_id" : $(elem).data('offer_letter_id'),
                    "acceptance_letter_id" : $(elem).data('acceptance_letter_id')
                };

                oParams.employee_data.push(oData);

                var oAllotmentData = {
                    "esop_id" : iEsopID,
                    "department_id" : '',
                    "company_id" : $(elem).data('company_id'),
                    "allotment" : cr8v.remove_commas(number_format(parseFloat($(elem).data('alloted_shares')), 2))
                }

                if(oParams.allotment_data.length > 0)
                {
                    var oFiltered = oParams.allotment_data.filter(function (el){
                        return eval( el['company_id'] == $(elem).data('company_id') );
                    });

                    if(count(oFiltered) > 0)
                    {
                        if(cr8v.var_check(oFiltered[0]))
                        {
                            oFiltered[0].allotment = parseFloat(oFiltered[0].allotment) + parseFloat($(elem).data('alloted_shares'));
                        }
                        else
                        {
                            oParams.allotment_data.push(oAllotmentData);
                        }
                    }
                    else
                    {
                        oParams.allotment_data.push(oAllotmentData);
                    }
                }
                else
                {
                    oParams.allotment_data.push(oAllotmentData);
                }

                // if(oParams.allotment_data.length > 0)
                // {
                //     for(var i in oParams.allotment_data)
                //     {
                //         var oParamsAllotmentData = oParams.allotment_data[i];

                //         var oFiltered = oParams.allotment_data.filter(function (el){
                //             return eval( el['company_id'] == $(elem).data('company_id') );
                //         });

                //         if(oParamsAllotmentData.company_id == $(elem).data('company_id'))
                //         {
                //             oParams.allotment_data[i]['allotment'] = parseFloat(oParams.allotment_data[i]['allotment']) + parseFloat($(elem).data('alloted_shares'));
                //         }
                //         else
                //         {
                //             if(count(oFiltered) > 0)
                //             {
                //                 oFiltered.allotment = parseFloat(oFiltered.allotment) + parseFloat($(elem).data('alloted_shares'));
                //             }
                //             else
                //             {
                //                 oParams.allotment_data.push(oAllotmentData);
                //             }
                //         }
                //     }
                // }
                // else
                // {
                //     oParams.allotment_data.push(oAllotmentData);
                // }
            });
            
            for(var i in oParams.allotment_data)
            {
                var oParamsAllotmentData = oParams.allotment_data[i];

                oParams.allotment_data[i]['allotment'] = cr8v.remove_commas(number_format(oParams.allotment_data[i]['allotment'], 2));
            }
            // console.log(oParams)
            roxas.esop.insert_upload_share_distribution(oParams, uiConfirmBatchUploadBtn, true);
        },

        /**
         * view_batch_upload_share_distribution
         * @description This function is for viewing the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_batch_upload_share_distribution : function (iEsopID){
            if(cr8v.var_check(localStorage.getItem('upload_batch_share_distribution_path')) && localStorage.getItem('upload_batch_share_distribution_path') != null)
            {
                var sFilePath = localStorage.getItem('upload_batch_share_distribution_path').replace( roxas.config('url.server.base')+ '/assets/uploads/share_distribution/csv/', '');
                sFilePath = './assets/uploads/share_distribution/csv/' + sFilePath;

                var oParams = {
                    "file_path" : sFilePath
                };

                cr8v.delete_file(oParams, function(oData){
                
                });
            }

            localStorage.removeItem("esop_batch_id");
            localStorage.setItem('esop_batch_id', iEsopID);

            localStorage.removeItem('upload_batch_share_distribution_path');
            localStorage.setItem('upload_batch_share_distribution_path' , sBatchShareDistributionFilePath);

            localStorage.removeItem('upload_batch_share_distribution_file_name');
            localStorage.setItem('upload_batch_share_distribution_file_name' , sBatchShareDistributionFileName);

            window.location = roxas.config('url.server.base') + "esop/batch_upload_share_distribution";
        },

        /**
         * clear_batch_upload_share_distribution_modal
         * @description This function is for clearing data in upload user list modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_batch_upload_share_distribution_modal : function(){
            /*clear data*/
            uiUploadBatchShareDistributionBtn.attr('disabled', true);
            uiUploadBatchShareDistributionModal.find('#file_name').html('<i>No File Uploaded</i>');
            uiUploadBatchShareDistributionModal.find('.upload_batch_share_distribution_error_message').addClass('hidden');
            uiUploadBatchShareDistributionModal.find('.upload_batch_share_distribution_success_message').addClass('hidden');
        },

        /**
         * open_batch_csv
         * @description This function is for opening and viewing of the csv passed as parameter
         * @dependencies 
         * @param {object} oParams
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        open_batch_csv : function (oParams, uiThis, bValidateOnly, fnCallback){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/open_batch_csv",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)

                        if(typeof(fnCallback) == 'function')
                        {
                            fnCallback(oData);
                        }

                        if(cr8v.var_check(bValidateOnly) && bValidateOnly)
                        {
                            return false;
                        }

                        if(cr8v.var_check(oData))
                        {
                            uiUploadBatchShareDistributionContainer.find('.upload_batch_share_distribution:not(.template)').remove();
                            uiBatchNoErrorsFound.addClass('template hidden');
                            var iErrorCtr = 0;

                            if(oData.status == true)
                            {
                                if(oData.data.length > 0)
                                {                                    
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oShareDistributionInfo = oData.data[i];

                                        var iRow = i + 1;
    
                                        var uiTemplate = $('.upload_batch_share_distribution.template').clone().removeClass('template hidden');
                                        var uiManipulatedTemplate = roxas.esop.manipulate_upload_share_distribution(oShareDistributionInfo, uiTemplate);

                                        uiManipulatedTemplate.find('[data-label="row"]').text(iRow);

                                        if(oShareDistributionInfo.valid == 0 && count(oShareDistributionInfo.error_message) > 0)
                                        {
                                            iErrorCtr++;
                                            // uiManipulatedTemplate.css({'border' : '1px solid red'});

                                            var uiErrorTemplate = uiUploadBatchShareDistributionErrorContainer.find('.upload_batch_share_distribution_error.template').clone().removeClass('template hidden');
                                            var uiManipulatedErrorTemplate = roxas.esop.manipulate_upload_share_distribution_error(oShareDistributionInfo, uiErrorTemplate, uiManipulatedTemplate);

                                            uiManipulatedErrorTemplate.find('[data-label="row_error"]').text(iRow);

                                            uiUploadBatchShareDistributionErrorContainer.append(uiManipulatedErrorTemplate);
                                        }

                                        uiUploadBatchShareDistributionContainer.append(uiManipulatedTemplate);
                                    }

                                    if(iErrorCtr > 0)
                                    {
                                        uiConfirmBatchUploadBtn.attr('disabled', true);
                                    }
                                    else
                                    {
                                        uiConfirmBatchUploadBtn.attr('disabled', false);
                                        uiBatchNoErrorsFound.removeClass('template hidden');
                                    }
                                }
                                else
                                {
                                    uiBatchNoErrorsFound.removeClass('template hidden');
                                }
                            }
                            else
                            {
                                uiBatchNoErrorsFound.removeClass('template hidden');

                                if(cr8v.var_check(oData.message))
                                {
                                    var sErrorMessages = '';
                                    var uiErrorContainer = $('.insert_batch_share_distribution_error_message');
                                    for(var i = 0, max = oData.message.length; i < max; i++)
                                    {
                                        var message = oData.message[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);

            }
        },

        fetch_available_share_allotment : function() {

            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : {"params" : "none"},
                "url"    : roxas.config('url.server.base') + "esop/offer_allotments",
                "beforeSend": function () {

                },

                "success": function (oData) {
                    if(typeof(oData) != 'undefined')
                    {
                        $('#share_allotment_checkbox').prop('checked' , false);
                        uiAddEsopModal.find('div.add-esop-dash').hide();
                        $shareAllotmentContainer.find('tr[data-template="share_allotment"]:not(.template)').remove();
                        if(typeof(oData.status))
                        {
                            for (var i = 0; i < oData.data.length ; i++)
                            {
                                var $template = $shareAllotmentTemplate.clone().removeClass('template');
                                var oEsopData = oData.data[i];

                                


                                if(oEsopData.total_unavailed_main_esop != null && oEsopData.total_unavailed_main_esop > 0)
                                {
                                    //if(oEsopData.main_esop == 1) //if main esop
                                    //{
                                    $template.find('[data-label="esop_name"]').html(cr8v.var_check(oEsopData.name, 1) ? oEsopData.name : '');
                                    $template.find('[data-label="esop_unavailed"]').html(cr8v.var_check(oEsopData.total_unavailed_main_esop, 1) ? number_format(oEsopData.total_unavailed_main_esop, 2) : '');
                                    //}
                                    //else{
                                    //    $template.find('[data-label="esop_name"]').html(cr8v.var_check(oEsopData.name, 1) ? oEsopData.name + ' (Batch)' : '');
                                    //    $template.find('[data-label="esop_unavailed"]').html(cr8v.var_check(oEsopData.total_unavailed_batch_esop, 1) ? number_format(oEsopData.total_unavailed_batch_esop, 2) : '');
                                    //}
                                    $template.find('[data-label="esop_id"]').attr('total_share_qty' , oEsopData.total_share_qty);
                                    $template.find('[data-label="esop_id"]').attr('esop_id' , cr8v.var_check(oEsopData.id, 1) ? oEsopData.id : 0);

                                    $shareAllotmentContainer.append($template);
                                }

                            }
                        }
                    }
                },

                "complete": function () {

                }
            };

            roxas.esop.ajax(oAjaxConfig);
        },

        compute_share_allotment : function() {
            var uiShareAllotmentInputs = uiAddEsopModal.find('tr[data-template="share_allotment"]:not(.template)').find('input.share_allotment_input'),
                iAllotmentSum = 0,
                $totalShareQuantityInput = uiAddEsopModal.find('[name="total_share_qty"]'),
                itotalShareQuantityInput = (cr8v.remove_commas($totalShareQuantityInput.attr('original_value')) > 0) ? cr8v.remove_commas($totalShareQuantityInput.attr('original_value')) : 0;

            iAllotmentSum += parseFloat(itotalShareQuantityInput);
            
            if($('#share_allotment_checkbox').is(':checked'))
            {
                $.each(uiShareAllotmentInputs, function() {
                    if($(this).parents('tr:first').find('input[data-label="esop_id"]').is(':checked'))
                    {
                        if($(this).val() != "")
                        {
                             iAllotmentSum += parseFloat(cr8v.remove_commas($(this).val()))   
                        }    
                    }
                })
            }

            $totalShareQuantityInput.val(number_format(iAllotmentSum , 2))
        },

        vesting_rights_identification : function() {
            var $paymentUI = $('div[data-template="vesting_payments"]:not(.template)');

            cr8v.get_time(function (sTime){
                 $.each($paymentUI , function() {
                    var sCurrentDate = moment(sTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD'),
                        oCurrentDate = moment(sCurrentDate, 'YYYY-MM-DD'),
                        recordDate = moment($(this).attr('custom-attr-year')).format('YYYY-MM-DD'),
                        oRecordDate = moment(recordDate, 'YYYY-MM-DD'),
                        unixRecordDate = oRecordDate.unix(),
                        unixThisDate = oCurrentDate.unix(),
                        sResult = 'With Vesting Rights';

                    if(unixThisDate < unixRecordDate){
                        sResult =  'Without Vesting Rights';
                    }

                    $(this).find('p.vesting_status').html(sResult);
                    
                })

            });

           
        },

        /**
         * bind_upload_payment_events
         * @description This function is for binding upload payment events
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_upload_payment_events : function () {
            uiTriggerUploadPaymentFile.on('click', function(){
                $(this).next('label').find('input[name="upload_payment_record"]').trigger('click');
            });

            /*upload user list*/
            var last_file_path = '';
            var last_file_name = '';
            uiUploadPaymentModal.on('change', 'input[name="upload_payment_record"]', function(){
                var uiThis = $(this);
                var file = uiUploadPaymentModal.find('input[type=file][name="upload_payment_record"]')[0];
                var file_path = '';
                var file_name = '';
                var formdata = false;
                var uiLogoContainer = uiUploadPaymentModal.find('[data-container="file_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();
                var uiErrorContainer = uiUploadPaymentModal.find('.upload_payment_record_error_message');

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'csv'/* || sValueExt == 'xls' || sValueExt == 'xlsx'*/)
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('csv_file', file.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'esop/upload_payment_record_csv',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData))
                                    {
                                        uiOpenPaymentFileBtn.attr('disabled', true);
                                        uiThis.val('');
                                        cr8v.add_error_message(false, uiErrorContainer);   

                                        if(cr8v.var_check(oData.status) && oData.status == true)
                                        {
                                            if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                            {
                                                uiUploadPaymentModal.find('.upload_payment_record_success_message').removeClass('hidden').show().delay(1000).fadeOut()
                                                    .html('<p class="font-15 success_message"> ' + oData.message[0] + '</p>');
                                            }

                                            var sOrigFileName = (cr8v.var_check(oData.data.original_file_name)) ? oData.data.original_file_name : 'No file uploaded yet';
                                            var sFilePath = (cr8v.var_check(oData.data.file_path) ? oData.data.file_path : '');

                                            if(cr8v.var_check(sUploadPaymentFilePath) && sUploadPaymentFilePath.length > 0)
                                            {
                                                var sFilePathForDelete = sUploadPaymentFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/payment_record/csv/', '');
                                                sFilePathForDelete = './assets/uploads/payment_record/csv/' + sFilePathForDelete;

                                                var oParams = {
                                                    "file_path" : sFilePathForDelete
                                                };

                                                cr8v.delete_file(oParams, function(oData){
                                                
                                                });
                                            }

                                            sUploadPaymentFilePath = sFilePath;
                                            sUploadPaymentFileName = sOrigFileName;

                                            uiUploadPaymentModal.find('#upload_payment_file_name').html('<i>'+sOrigFileName+'</i>');

                                            uiOpenPaymentFileBtn.attr('disabled', false);
                                        }
                                        else
                                        {
                                            sUploadPaymentFilePath = '';
                                            sUploadPaymentFileName = '';

                                            uiUploadPaymentModal.find('#upload_payment_file_name').html('<i>No File Uploaded</i>');

                                            var sErrorMessages = '';
                                            for(var i = 0, max = oData.message.length; i < max; i++)
                                            {
                                                var message = oData.message[i];
                                                sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                            }

                                            cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                        }
                                    }
                                    else
                                    {
                                        file_path = last_file_path;
                                        file_name = last_file_name;
                                    }
                                },
                                complete: function(){

                                }
                            };

                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        sUploadPaymentFilePath = '';
                        sUploadPaymentFileName = '';
                        uiUploadPaymentModal.find('#upload_payment_file_name').html('<i>No File Uploaded</i>');
                        uiOpenPaymentFileBtn.attr('disabled', true);
                        var sErrorMessages = '<p class="font-15 error_message">Please upload the correct template file.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });
    
            uiOpenPaymentFileBtn.on('click', function(){
                if(window.location.href.indexOf('esop/payment_records') > -1){
                var iSelected = $(".upload_esop_dropdown").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val(),
                    iEsopID = uiPaymentUploadEsopDropdown.find(".frm-custom-dropdown").find(".frm-custom-dropdown-option").find(".option"),
                    iValue  = "";

                    $.each(iEsopID, function(){
                            var uiThisOption = $(this),
                              iGetValue =  uiThisOption.attr("data-value"); 

                               if(iSelected == uiThisOption.text())
                               {
                                     iValue = iGetValue;
                               }         
                    });             
                    
                    localStorage.setItem('upload_payment_esop_id',iValue);
                }
                
                else if(window.location.href.indexOf('esop/upload_payment_record') > -1)
                {
                    var iValue = localStorage.getItem('upload_payment_esop_id');
                }

                roxas.esop.view_upload_payment(iValue);
            });
            
            uiPaymentUploadEsopDropdown.on('click', '.option.esop',function(){
                var uiThis = $(this);
                var iEsopID = uiThis.attr('data-value');
                uiPaymentUploadEsopDropdown.attr('esop_id', iEsopID);
            });

            uiConfirmUploadPaymentBtn.on('click', function(){
                var oGetCsvParams = {
                    "file_path" : localStorage.getItem('upload_payment_path'),
                    "esop_id" : localStorage.getItem('upload_payment_esop_id')
                };

                cr8v.show_spinner(uiConfirmUploadPaymentBtn, true);

                roxas.esop.open_payment_csv(oGetCsvParams, undefined, true, function(oData){

                    cr8v.show_spinner(uiConfirmUploadPaymentBtn, false);

                    if(cr8v.var_check(oData))
                    {
                        var iErrorCtr = 0;

                        if(oData.status == true)
                        {
                            if(oData.data.length > 0)
                            {                                    
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oUploadPaymentData = oData.data[i];

                                    var iRow = i + 1;

                                    if(oUploadPaymentData.valid == 0 && count(oUploadPaymentData.error_message) > 0)
                                    {
                                        iErrorCtr++;
                                    }
                                }

                                if(iErrorCtr > 0)
                                {
                                    uiConfirmUploadPaymentBtn.attr('disabled', true);
                                }
                                else
                                {
                                    uiConfirmUploadPaymentBtn.attr('disabled', false);
                                    roxas.esop.assemble_upload_payment_data(uiConfirmUploadPaymentBtn);
                                }
                            }
                        }
                    }
                });
            });

            uiUploadPaymentModalBtn.on('click', function(){
                roxas.esop.clear_upload_payment_modal();
            });

            uiCancelUploadPaymentBtn.on('click', function(){
                if(cr8v.var_check(localStorage.getItem('upload_payment_path')) && localStorage.getItem('upload_payment_path') != null)
                {
                    var sFilePath = localStorage.getItem('upload_payment_path').replace( roxas.config('url.server.base')+ '/assets/uploads/payment_record/csv/', '');
                    sFilePath = './assets/uploads/payment_record/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    localStorage.removeItem('upload_payment_path');
                    localStorage.removeItem('upload_payment_file_name');

                    cr8v.delete_file(oParams, function(oData){
                        window.location = roxas.config('url.server.base') + 'esop/payment_records';
                    });
                }
            });

            uiUploadPaymentModal.on('click', '.close-me', function(){
                if(cr8v.var_check(sUploadPaymentFilePath) && sUploadPaymentFilePath.length > 0)
                {
                    var sFilePath = sUploadPaymentFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/payment_record/csv/', '');
                    sFilePath = './assets/uploads/payment_record/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    sUploadPaymentFilePath = '';
                    sUploadPaymentFileName = '';

                    cr8v.delete_file(oParams, function(oData){
                        
                    });
                }
            });
        },

        /**
         * assemble_upload_payment_data
         * @description This function is for assembling the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_upload_payment_data : function (uiConfirmUploadPaymentBtn){

            var oParams = {"data" : []};
            uiUploadPaymentContainer.find('.upload_payment_details:not(.template)').each(function (key, elem){
                var oData = {
                    "payment_type_id"       : $(elem).attr('payment_method_id'),
                    "currency"              : $(elem).attr('currency_id'),
                    "amount_paid"           : $(elem).attr('payment_value'),
                    "vesting_rights_id"     : $(elem).attr('vesting_rights_id'),
                    "placed_by"             : iUserID,
                    "user_id"               : $(elem).attr('employee_id'),
                    "esop_id"               : localStorage.getItem('upload_payment_esop_id'),
                    "user_name"             : $(elem).attr('employee_name'),
                    "dividend_date"         : $(elem).attr('dividend_date')
                };
                console.log(oData)
                oParams.data.push(oData);
            });

            roxas.esop.insert_upload_payment_record(oParams, uiConfirmUploadPaymentBtn);
        },

        /**
         * insert_upload_payment_record
         * @description This function is for inserting the validated payment record
         * @dependencies 
         * @param {object} oUserUploadList
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        insert_upload_payment_record : function (oParams, uiBtn, bFromEsopBatch) {
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/insert_upload_payment_record",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                $('.insert_payment_success_message').removeClass('hidden').html('<p class="font-15 success_message">Payment records added successfully.</p>');
                               
                                setTimeout(function(){
                                    if(cr8v.var_check(localStorage.getItem('upload_payment_path')) && localStorage.getItem('upload_payment_path') != null)
                                    {
                                        var sFilePath = localStorage.getItem('upload_payment_path').replace( roxas.config('url.server.base')+ '/assets/uploads/payment_record/csv/', '');
                                        sFilePath = './assets/uploads/payment_record/csv/' + sFilePath;

                                        var oParams = {
                                            "file_path" : sFilePath
                                        };

                                        localStorage.removeItem('upload_payment_path');
                                        localStorage.removeItem('upload_payment_file_name');

                                        cr8v.delete_file(oParams, function(oData){
                                            window.location = roxas.config('url.server.base') + 'esop/payment_records';
                                        });
                                    }
                                }, 1000)
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);
            }
        },

        /**
         * view_upload_payment
         * @description This function is for viewing the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_upload_payment : function (iEsopID){
            if(cr8v.var_check(iEsopID))
            {
                if(cr8v.var_check(localStorage.getItem('upload_payment_path')) && localStorage.getItem('upload_payment_path') != null)
                {
                    var sFilePath = localStorage.getItem('upload_payment_path').replace( roxas.config('url.server.base')+ '/assets/uploads/payment_record/csv/', '');
                    sFilePath = './assets/uploads/payment_record/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    cr8v.delete_file(oParams, function(oData){
                    
                    });
                }
                
                localStorage.removeItem('upload_payment_path');
                localStorage.setItem('upload_payment_path' , sUploadPaymentFilePath);

                localStorage.removeItem('upload_payment_file_name');
                localStorage.setItem('upload_payment_file_name' , sUploadPaymentFileName);

//                 localStorage.removeItem('upload_payment_esop_id');
//                 localStorage.setItem('upload_payment_esop_id', iEsopID);

                window.location = roxas.config('url.server.base') + "esop/upload_payment_record";
            }
        },

        /**
         * clear_upload_payment_modal
         * @description This function is for clearing data in upload user list modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_upload_payment_modal : function(){
            /*clear data*/
            uiOpenPaymentFileBtn.attr('disabled', true);
            uiUploadPaymentModal.find('#upload_payment_file_name').html('<i>No File Uploaded</i>');
            uiUploadPaymentModal.find('.upload_payment_record_error_message').addClass('hidden');
            uiUploadPaymentModal.find('.upload_payment_record_success_message').addClass('hidden');
        },

        /**
         * open_payment_csv
         * @description This function is for opening and viewing of the csv passed as parameter
         * @dependencies 
         * @param {object} oParams
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        open_payment_csv : function (oParams, uiThis, bValidateOnly, fnCallback){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/open_payment_csv",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)

                        if(typeof(fnCallback) == 'function')
                        {
                            fnCallback(oData);
                        }

                        if(cr8v.var_check(bValidateOnly) && bValidateOnly)
                        {
                            return false;
                        }

                        if(cr8v.var_check(oData))
                        {
                            uiUploadPaymentContainer.find('.upload_payment_details:not(.template)').remove();
                            uiPaymentNoErrorsFound.addClass('template hidden');
                            var iErrorCtr = 0;

                            if(oData.status == true)
                            {
                                if(oData.data.length > 0)
                                {                                    
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oUploadPaymentData = oData.data[i];

                                        var iRow = i + 1;
    
                                        var uiTemplate = $('.upload_payment_details.template').clone().removeClass('template hidden');
                                        var uiManipulatedTemplate = roxas.esop.manipulate_upload_payment_data(oUploadPaymentData, uiTemplate);

                                        uiManipulatedTemplate.find('[data-label="row"]').text(iRow);

                                        if(oUploadPaymentData.valid == 0 && count(oUploadPaymentData.error_message) > 0)
                                        {
                                            iErrorCtr++;
                                            // uiManipulatedTemplate.css({'border' : '1px solid red'});

                                            var uiErrorTemplate = uiUploadPaymentErrorContainer.find('.upload_payment_error.template').clone().removeClass('template hidden');
                                            var uiManipulatedErrorTemplate = roxas.esop.manipulate_upload_payment_error(oUploadPaymentData, uiErrorTemplate, uiManipulatedTemplate);

                                            uiManipulatedErrorTemplate.find('[data-label="row_error"]').text(iRow);

                                            uiUploadPaymentErrorContainer.append(uiManipulatedErrorTemplate);
                                        }

                                        uiUploadPaymentContainer.append(uiManipulatedTemplate);
                                    }

                                    if(iErrorCtr > 0)
                                    {
                                        uiConfirmUploadPaymentBtn.attr('disabled', true);
                                    }
                                    else
                                    {
                                        uiConfirmUploadPaymentBtn.attr('disabled', false);
                                        uiPaymentNoErrorsFound.removeClass('template hidden');
                                    }
                                }
                                else
                                {
                                    uiPaymentNoErrorsFound.removeClass('template hidden');
                                }
                            }
                            else
                            {
                                uiPaymentNoErrorsFound.removeClass('template hidden');

                                if(cr8v.var_check(oData.message))
                                {
                                    var sErrorMessages = '';
                                    var uiErrorContainer = $('.insert_payment_error_message');
                                    for(var i = 0, max = oData.message.length; i < max; i++)
                                    {
                                        var message = oData.message[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.esop.ajax(oAjaxConfig);

            }
        },

        /**
         * manipulate_upload_payment_data
         * @description This function is for rendering upload details
         * @dependencies 
         * @param {object} oUploadPaymentData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_upload_payment_data : function (oUploadPaymentData, uiTemplate){
            if(cr8v.var_check(oUploadPaymentData) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.attr('company_code', oUploadPaymentData.company_code);
                uiTemplate.attr('company_id', oUploadPaymentData.company_id);
                uiTemplate.attr('employee_code', oUploadPaymentData.employee_code);
                uiTemplate.attr('employee_name', oUploadPaymentData.employee_name);
                uiTemplate.attr('payment_method_id', oUploadPaymentData.payment_method_id);
                uiTemplate.attr('payment_value', cr8v.remove_commas(number_format(oUploadPaymentData.payment_value, 2)));
                uiTemplate.attr('currency', oUploadPaymentData.currency);
                uiTemplate.attr('currency_id', oUploadPaymentData.currency_id);
                uiTemplate.attr('employee_id', oUploadPaymentData.employee_id);
                uiTemplate.attr('employee_name', oUploadPaymentData.employee_name);
                uiTemplate.attr('vesting_rights_id', oUploadPaymentData.vesting_rights_id);
                uiTemplate.attr('dividend_date', oUploadPaymentData.dividend_date);

                uiTemplate.find('[data-label="company_code"]').text(oUploadPaymentData.company_code);
                uiTemplate.find('[data-label="employee_code"]').text(oUploadPaymentData.employee_code);
                uiTemplate.find('[data-label="employee_name"]').text(oUploadPaymentData.employee_name);
                uiTemplate.find('[data-label="payment_method_id"]').text(oUploadPaymentData.payment_method_id);
                uiTemplate.find('[data-label="payment_value"]').text(number_format(oUploadPaymentData.payment_value, 2));
                uiTemplate.find('[data-label="currency"]').text(oUploadPaymentData.currency);
                uiTemplate.find('[data-label="dividend_date"]').text(oUploadPaymentData.dividend_date);

                return uiTemplate;
            }
        },

        /**
         * manipulate_upload_payment_error
         * @description This function is for rendering upload error messages
         * @dependencies 
         * @param {object} oUploadPaymentData
         * @param {jQuery} uiTemplate
         * @param {jQuery} uiPreviewTemplate
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_upload_payment_error : function (oUploadPaymentData, uiTemplate, uiPreviewTemplate){
            if(cr8v.var_check(oUploadPaymentData) && cr8v.var_check(uiTemplate))
            {
                var uiRowNameErrorContainer = uiTemplate.find('[data-container="row_name_error"]');
                var uiRowMessageErrorContainer = uiTemplate.find('[data-container="row_message_error"]');

                for (var k in oUploadPaymentData.error_message) {
                    var oErrorMessage = oUploadPaymentData.error_message[k];

                    uiTemplate.find('[data-label="row_name_error"]').append(k+'</br> ');
                    uiTemplate.find('[data-label="row_message_error"]').append(oErrorMessage+'</br> ');
                    uiPreviewTemplate.find('[data-error="'+k+'"]').attr({'style' : 'border: 1px solid red;'});
                }


                return uiTemplate;
            }
        },

        /**
         * add_outstanding_balance
         * @description This function is for adding the outstanding balance to the next vesting year.
         * @dependencies 
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Dru Moncatar
         * @method_id N/A
         */
        add_outstanding_balance : function(oParams) {
            var iOutstandingBalance = oParams.outstanding_balance,
            iNextVestingRightId = oParams.vesting_rights_id,
            oAjaxConfig = {};
            
            if(iNextVestingRightId > 0 
            && iNextVestingRightId !== undefined) {
                // console.log(JSON.stringify(oParams));
                console.log(oParams);

                // oAjaxConfig = {
                //     "type"   : "POST",
                //     "data"   : oParams,
                //     "url"    : roxas.config('url.server.base') + "esop/insert_outstanding_balance_as_payment",
                //     "success" : function(oResult) {
                //         console.log(oResult);
                //         // if(oData.status)
                //         // {
                //         //     for(var i = 0; i < aCheckedInputs.length; i++)
                //         //     {
                //         //         $(aCheckedInputs[i]).parents('tr:first').remove();
                                    
                //         //     }

                //         //     $vestingRightClaimModal.find('.close-me').trigger('click');

                                
                //         // }
                //         // cr8v.show_spinner($vestingRightClaimButton, false);
                //         // window.location.reload();
                //     }
                // }
                // roxas.esop.ajax(oAjaxConfig);

            }

            
        },

    }

}());

$(window).load(function (){
    roxas.esop.initialize();
});