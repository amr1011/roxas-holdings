/**
 *  Departments Class
 *
 *
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var uiAddDepartmentModalBtn,
        uiAddDepartmentBtn,
        uiAddDepartmentModal,
        uiAddDepartmentForm,

        uiEditDepartmentModalBtn,
        uiEditDepartmentBtn,
        uiEditDepartmentModal,
        uiEditDepartmentForm,

        uiDepartmentListContainer,

        uiEditCompanyModal,
        
        uiNoResultsFound,
        sGeneratedCode,

        uiSortDepartmentContainer

        ;


    CPlatform.prototype.departments = {

        initialize : function() {

            /*declare variables*/
            uiAddDepartmentModalBtn = $('[modal-target="add-department"]');
            uiAddDepartmentBtn = $('#add_department_btn');
            uiAddDepartmentModal = $('[modal-id="add-department"]');
            uiAddDepartmentForm = $('form#add_department_form');

            uiEditDepartmentModalBtn = $('[modal-target="edit-department"]');
            uiEditDepartmentBtn = $('#edit_department_btn');
            uiEditDepartmentModal = $('[modal-id="edit-department"]');
            uiEditDepartmentForm = $('form#edit_department_form');

            uiDepartmentListContainer = $('[data-container="department_list_container"]');

            uiEditCompanyModal = $('[modal-id="edit-company"]');

            uiNoResultsFound = $('.no_results_found.template');

            uiSortDepartmentContainer = $('#sort_department');


            /*bind on load data*/
            roxas.departments.bind_events_on_load();
            roxas.departments.bind_sorting_events();

            /*initialize events*/
            cr8v.input_numeric($('[name="department_code"]'));

            /*for viewing of department details and its ranks*/
            uiDepartmentListContainer.on('click', '.view_ranks', function(){
                var uiThis = $(this);
                var iDepartmentID = uiThis.parents('.department:first').attr('data-department-id');
                roxas.departments.view_ranks(iDepartmentID);
            });
        },
        /**
         * bind_sorting_events
         * @description This function is for binding events for sorting
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_sorting_events : function(){
            uiSortDepartmentContainer.on('click', '.sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');
                var uiTemplates = uiDepartmentListContainer.find('.department:not(.template)');

                cr8v.clear_sorting_classes(uiSortDepartmentContainer.find('.sort_field'));
                cr8v.ui_sorting(uiThis, sSortBy, uiTemplates.length, uiTemplates, uiDepartmentListContainer)
            });
            
            cr8v.clear_sorting_classes(uiSortDepartmentContainer.find('.sort_field'));
        },
        /**
         * view_ranks
         * @description This function is for viewing the departments of a specific company
         * @dependencies 
         * @param {int} iCompanyID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_ranks : function(iDepartmentID){
            if(cr8v.var_check(iDepartmentID))
            {
                localStorage.removeItem("department_id");
                localStorage.setItem('department_id', iDepartmentID);
                window.location =  roxas.config('url.server.base') + "ranks/rank_list/";
            }
        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('rank') > -1 && localStorage.getItem('department_id') != null)
            {
                // console.log('rank')
                var oGetDepartmentParams = {
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('department_id'),
                    "limit" : 999999
                };
                roxas.departments.get_departments(oGetDepartmentParams, undefined, true);

                /*for edit of department events*/
                roxas.departments.bind_edit_department_events();
            }
            else
            {
                if(localStorage.getItem('company_id') != null)
                {
                    // console.log('rank')
                    var oGetDepartmentParams = {
                        "search_field" : 'company_id',
                        "keyword" : localStorage.getItem('company_id'),
                        "limit" : 999999
                    };
                    roxas.departments.get_departments(oGetDepartmentParams);

                    /*for add of department events*/
                    roxas.departments.bind_add_department_events();
                }
            }
        },

        /**
         * clear_add_department_modal
         * @description This function is for clearing data in add department modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_add_department_modal : function(){
            /*clear data*/
            uiAddDepartmentModal.find('input').removeClass('input-error');
            uiAddDepartmentModal.find('textarea').removeClass('input-error');

            uiAddDepartmentModal.find('input[name="department_name"]').val('');
            uiAddDepartmentModal.find('input[name="department_code"]').val('');
            uiAddDepartmentModal.find('[name="department_description"]').val('');
            uiAddDepartmentModal.find('.add_department_success_message').addClass('hidden');
            uiAddDepartmentModal.find('.add_department_error_message').addClass('hidden');
        },

        /**
         * clear_edit_department_modal
         * @description This function is for clearing data in edit department modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_edit_department_modal : function(){
            /*clear data*/
            uiEditDepartmentModal.find('input').removeClass('input-error');
            uiEditDepartmentModal.find('.edit_department_success_message').addClass('hidden');
            uiEditDepartmentModal.find('.edit_department_error_message').addClass('hidden');
        },

        /**
         * bind_add_department_events
         * @description This function is for binding events for add depertment info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_add_department_events : function(){

            uiAddDepartmentModalBtn.on('click', function(){
                /*clear data*/
                roxas.departments.clear_add_department_modal();
                roxas.departments.generate_new_code({table : 'department', column : 'department_code'});
            });

            /*save new department*/
            uiAddDepartmentBtn.on('click', function(){
                uiAddDepartmentForm.submit();
            });

            /*prevent on submit of form*/
            uiAddDepartmentModal.on('submit', uiAddDepartmentForm, function (e) {
                e.preventDefault();
            });

            /*add a validation to adding a new department*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiAddDepartmentBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiAddDepartmentModal.find('.add_department_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiAddDepartmentModal.find('.add_department_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiAddDepartmentBtn, true);
                    
                    roxas.departments.assemble_add_department_information(uiAddDepartmentModal, uiAddDepartmentBtn);
                }
            };

            /*bind the validation to add new department form*/
            cr8v.validate_form(uiAddDepartmentForm, oValidationConfig);
        },

        /**
         * bind_edit_department_events
         * @description This function is for binding events for add depertment info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_edit_department_events : function(){

            uiEditDepartmentModalBtn.on('click', function(){
                /*clear data*/
                roxas.departments.clear_edit_department_modal();
            });

            /*save edit department*/
            uiEditDepartmentBtn.on('click', function(){
                uiEditDepartmentForm.submit();
            });

            /*prevent on submit of form*/
            uiEditDepartmentModal.on('submit', uiEditDepartmentForm, function (e) {
                e.preventDefault();
            });

            /*add a validation to editing a department*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiEditDepartmentBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiEditDepartmentModal.find('.edit_department_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiEditDepartmentModal.find('.edit_department_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiEditDepartmentBtn, true);
                    
                    roxas.departments.assemble_edit_department_information(uiEditDepartmentModal, uiEditDepartmentBtn);
                }
            };

            /*bind the validation to edit department form*/
            cr8v.validate_form(uiEditDepartmentForm, oValidationConfig);
        },

        /**
         * assemble_add_department_information
         * @description This function will assemble add department information
         * @dependencies N/A
         * @param {jQuery} uiAddDepartmentModal
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_add_department_information : function(uiAddDepartmentModal, uiAddDepartmentBtn){
            var oParams = {
                'name' : uiAddDepartmentModal.find('input[name="department_name"]').val(),
                'department_code' : uiAddDepartmentModal.find('input[name="department_code"]').val(),
                'description' : uiAddDepartmentModal.find('[name="department_description"]').val().replace(/['"\/\\]/g, ""),
                'company_id' : uiEditCompanyModal.attr('data-company-id'),
            };
            roxas.departments.add_department(oParams, uiAddDepartmentBtn);
        },

        /**
         * assemble_edit_department_information
         * @description This function will assemble edit department information
         * @dependencies N/A
         * @param {jQuery} uiEditDepartmentModal
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_edit_department_information : function(uiEditDepartmentModal, uiEditDepartmentBtn){
            var oParams = {
                'id' : uiEditDepartmentModal.attr('data-department-id'),
                'data' : [
                    {
                        'name' : uiEditDepartmentModal.find('input[name="department_name"]').val(),
                        'department_code' : uiEditDepartmentModal.find('input[name="department_code"]').val(),
                        'description' : uiEditDepartmentModal.find('[name="department_description"]').val().replace(/['"\/\\]/g, "")
                    }
                ]
            };
            roxas.departments.edit_department(oParams, uiEditDepartmentBtn);
        },

        /**
         * add_department
         * @description This function will add department code, name and description
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_department : function(oParams, uiAddDepartmentBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "departments/add",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiAddDepartmentBtn)) {
                            cr8v.show_spinner(uiAddDepartmentBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddDepartmentModal.find('.add_department_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiAddDepartmentModal.hasClass('showed'))
                                    {
                                        roxas.departments.clear_add_department_modal();
                                        uiAddDepartmentModal.find('.close-me').trigger('click');
                                    }
                                }, 1000)
                                
                                var oDepartmentData = oData.data;
                                var uiTemplate = $('.department.template').clone().removeClass('template hidden').show();
                                var uiManipulatedTemplate = roxas.departments.manipulate_template(oDepartmentData, uiTemplate);
                                uiDepartmentListContainer.prepend(uiManipulatedTemplate);
                                
                                uiNoResultsFound.addClass('template hidden');
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiAddDepartmentModal.find('.add_department_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiAddDepartmentBtn)) {
                            cr8v.show_spinner(uiAddDepartmentBtn, false);
                        }
                    },
                };

                roxas.departments.ajax(oAjaxConfig);
            }
        },

        /**
         * edit_department
         * @description This function will edit department code, name and description
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        edit_department : function(oParams, uiEditDepartmentBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "departments/edit",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiEditDepartmentBtn)) {
                            cr8v.show_spinner(uiEditDepartmentBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiEditDepartmentModal.find('.edit_department_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiEditDepartmentModal.hasClass('showed'))
                                    {
                                        roxas.departments.clear_edit_department_modal();
                                        uiEditDepartmentModal.find('.close-me').trigger('click');
                                    }
                                }, 1000)

                                var oDepartmentData = oData.data;
                                var uiTemplate = $('[data-container="department_information"]');
                                var uiManipulatedTemplate = roxas.departments.manipulate_template(oDepartmentData, uiTemplate);

                                var uiTemplateForEdit = uiEditCompanyModal;
                                var uiManipulatedTemplateForEdit = roxas.departments.populate_edit_info(oDepartmentData, uiTemplateForEdit);
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiEditDepartmentModal.find('.edit_department_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiEditDepartmentBtn)) {
                            cr8v.show_spinner(uiEditDepartmentBtn, false);
                        }
                    },
                };

                roxas.departments.ajax(oAjaxConfig);
            }
        },

        /**
         * populate_edit_info
         * @description This function will put the data of the department needed on the edit info
         * @dependencies N/A
         * @param {object} oDepartmentData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        populate_edit_info : function(oDepartmentData, uiForm){
            if(cr8v.var_check(oDepartmentData) && cr8v.var_check(uiForm))
            {
                /*for breadcrumbs*/
                $('.back_to_previous_company').text(oDepartmentData.company_name);
                $('[data-label="department_name"]').text(oDepartmentData.name);

                uiForm.attr('data-department-name', oDepartmentData.name);
                uiForm.attr('data-department-code', oDepartmentData.department_code);
                uiForm.attr('data-department-id', oDepartmentData.id);

                uiForm.find('input[name="department_name"]').val(oDepartmentData.name);
                uiForm.find('input[name="department_code"]').val(oDepartmentData.department_code);
                uiForm.find('[name="department_description"]').val(oDepartmentData.description);

                return uiForm;
            }
        },

        /**
         * manipulate_template
         * @description This function will put the data of the department needed on the template
         * @dependencies N/A
         * @param {object} oDepartmentData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template : function(oDepartmentData, uiTemplate){
            if(cr8v.var_check(oDepartmentData) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.attr('data-department-name', oDepartmentData.name);
                uiTemplate.attr('data-department-code', oDepartmentData.department_code);
                uiTemplate.attr('data-department-id', oDepartmentData.id);
                uiTemplate.attr('data-department-rank-count', oDepartmentData.rank_count);

                uiTemplate.find('[data-label="department_name"]').text(oDepartmentData.name);
                uiTemplate.find('[data-label="department_code"]').text(oDepartmentData.department_code);
                uiTemplate.find('[data-label="department_description"]').text(oDepartmentData.description);
                uiTemplate.find('[data-label="department_rank_count"]').text(oDepartmentData.rank_count);

                return uiTemplate;
            }
        },

        /**
         * get_departments
         * @description This function is for getting and rendering departments on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @param {boolean} bFromRanks
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_departments : function(oParams, uiThis, bFromRanks){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "departments/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiDepartmentListContainer.find('.department:not(.template)').remove();
                                uiNoResultsFound.addClass('template hidden');

                                if(cr8v.var_check(bFromRanks) && bFromRanks)
                                {
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oDepartmentData = oData.data[i];
                                        var uiTemplate = $('[data-container="department_information"]');
                                        var uiManipulatedTemplate = roxas.departments.manipulate_template(oDepartmentData, uiTemplate);
                                        
                                        var uiTemplateForEdit = uiEditDepartmentModal;
                                        var uiManipulatedTemplateForEdit = roxas.departments.populate_edit_info(oDepartmentData, uiTemplateForEdit);
                                        arrLogIDS.push(oDepartmentData.id);
                                    }
                                }
                                else
                                {  
                                    var arrLogDeptIDS = [];
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i < max; i++)
                                        {
                                            var oDepartmentData = oData.data[i];
                                            var uiTemplate = $('.department.template').clone().removeClass('template hidden').show();
                                            var uiManipulatedTemplate = roxas.departments.manipulate_template(oDepartmentData, uiTemplate);
                                            uiDepartmentListContainer.prepend(uiManipulatedTemplate);
                                            // arrLogIDS.push(oDepartmentData.id);
                                            arrLogDeptIDS.push(oDepartmentData.id);
                                        }
                                    }
                                    else
                                    {
                                        uiNoResultsFound.removeClass('template hidden');
                                    }
                                    
                                    var oGetLogsParams = {
                                        "where" : [
                                            {
                                                "field" : "ref_id",
                                                "operator" : "IN",
                                                "value" : "("+arrLogIDS.join()+")"
                                            },
                                            {
                                                "field" : "type",
                                                "operator" : "IN",
                                                "value" : "('company')"
                                            }
                                        ]
                                    };
                                    cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));

                                    if(cr8v.var_check(arrLogDeptIDS) && count(arrLogDeptIDS) > 0)
                                    {
                                        var oGetLogsParams = {
                                            "where" : [
                                                {
                                                    "field" : "ref_id",
                                                    "operator" : "IN",
                                                    "value" : "("+arrLogDeptIDS.join()+")"
                                                },
                                                {
                                                    "field" : "type",
                                                    "operator" : "IN",
                                                    "value" : "('department')"
                                                }
                                            ]
                                        };
                                        cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
                                    }
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.departments.ajax(oAjaxConfig);
            }
        },

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        generate_new_code : function(oParams) {
            var oData = {
                table : oParams.table,
                column : oParams.column
            },
            oAjaxConfig = {
                "type"   : "POST",
                "data"   : oData,
                "url"    : roxas.config('url.server.base') + "esop/generate_new_code",
                "success": function (oResult) {
                    // console.log(oResult);
                    if(oResult.status) {
                        sGeneratedCode = oResult.data;    
                        // console.log(oResult.data);
                        $('.code-generated').val(oResult.data);
                    }
                }
            };

            roxas.companies.ajax(oAjaxConfig);
        }
    }

}());

$(window).load(function () {
    roxas.departments.initialize();
});
