/**
 *  Users Class
 *
 *
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var uiCompanyDropdown,
        uiDepartmentDropdown,
        uiRankDropdown,
        uiContactDropdown,
        uiRoleDropdown,
        uiContactTypeDropdown,
        uiEmploymentStatusDropdown,

        uiAddUserForm,
        uiAddUserBtn,

        uiUserGridContainer,
        uiUserListContainer,

        uiUserSearchDropdown,
        uiUserCompanyUserRoleDropdown,
        uiUserRoleDropdown,

        uiSearchUserBtn,
        uiSearchUsernameInput,
        uiSearchUserFullnameInput,

        uiDivContent,

        uiEditUserForm,
        uiEditUserBtn,

        uiSortUserContainer,

        uiChangePassForm,
        uiChangePassBtn,
        uiChangePassModalBtn,
        uiChangePassModal,

        uiTriggerUploadList,
        uiUploadUserListModal,
        uiUploadUserListModalBtn,
        uiUploadUserListBtn,
        uiUploadUserListContainer,
        uiUploadUserListErrorContainer,
        uiConfirmUploadBtn,
        uiNoErrorsFound,
        uiCancelUploadBtn,
        sUserListFilePath,
        sUserListFileName,
        oUserUploadList = {},

        uiNoResultsFound,

        uiInputCapitalize,

        uiPersonalStocksContainer,

        uiSortPersonalStocksContainer,

        iTotalSharesOffered = 0,
        iTotalSharesTaken = 0,
        iTotalSharesUntaken = 0

        ;

    CPlatform.prototype.users = {

        initialize : function() {
            /*declare variables*/
            uiCompanyDropdown = $('#company_dropdown');
            uiDepartmentDropdown = $('#department_dropdown');
            uiRankDropdown = $('#rank_dropdown');
            uiContactDropdown = $('#contact_dropdown');
            uiRoleDropdown = $('#user_role_dropdown');
            uiContactTypeDropdown = $('#contact_dropdown');
            uiEmploymentStatusDropdown = $('#employment_status_dropdown');

            uiAddUserForm = $('#add_user_form');
            uiAddUserBtn = $('#add_user_btn');

            uiUserGridContainer = $('[data-container="user_view_by_grid"]');
            uiUserListContainer = $('[data-container="user_view_by_list"]');
            
            uiUserSearchDropdown = $('#search_user_dropdown');
            uiUserCompanyUserRoleDropdown = $('#company_user_role_dropdown');
            uiUserRoleDropdown = $('#user_role_search_dropdown');

            uiSearchUserBtn = $('.search_user_btn');
            uiSearchUsernameInput = $('.search_user_name_input');
            uiSearchUserFullnameInput = $('.search_user_full_name_input');

            uiDivContent = $('div.content');

            uiEditUserForm = $('#edit_user_form');
            uiEditUserBtn = $('#edit_user_btn');

            uiChangePassForm = $('#change_password_form');
            uiChangePassBtn = $('#change_password_btn');
            uiChangePassModalBtn = $('[modal-target="change-pass"]');
            uiChangePassModal = $('[modal-id="change-pass"]');

            uiSortUserContainer = $('#sort_user');

            uiTriggerUploadList = $('#trigger_upload_file');
            uiUploadUserListModal = $('[modal-id="upload-user-csv"]');
            uiUploadUserListModalBtn = $('[modal-target="upload-user-csv"]');
            uiUploadUserListBtn = $('#upload_list');
            uiUploadUserListContainer = $('[data-container="upload_user_list_container"]');
            uiUploadUserListErrorContainer = $('[data-container="upload_user_list_error_container"]');
            uiConfirmUploadBtn = $('#confirm_upload_btn');
            uiNoErrorsFound = $('.no_upload_user_list_error');
            uiCancelUploadBtn = $('#cancel_upload_btn');

            uiNoResultsFound = $('.no_results_found.template');

            uiInputCapitalize = $('.capitalize');

            uiPersonalStocksContainer = $('[data-container="personal_stocks_container"]');

            uiSortPersonalStocksContainer = $('#sort_personal_stocks');

            /*bind on load data*/
            roxas.users.bind_events_on_load();
            roxas.users.bind_sorting_events();

            /*initialize events*/
            uiInputCapitalize.on('blur', function(){
                var uiThis = $(this);
                var sTransformed = cr8v.capitalize(uiThis.val());
                uiThis.val(sTransformed);
            });

            uiPersonalStocksContainer.on('click', '.view_personal_stock', function(){
                var uiThis = $(this);
                var iEsopID = uiThis.parents('.personal_stocks:first').attr('data-esop-id');
                roxas.users.view_personal_stocks(iEsopID);
            });

        },

        /**
         * view_personal_stocks
         * @description This function is for viewing the esop
         * @dependencies
         * @param {int} iEsopID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_personal_stocks : function(iEsopID){
            if(cr8v.var_check(iEsopID))
            {
                localStorage.removeItem("esop_id");
                localStorage.setItem('esop_id', iEsopID);
                window.location =  roxas.config('url.server.base') + "esop/personal_stock_view";
            }
        },

        /**
         * bind_sorting_events
         * @description This function is for binding events for sorting
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_sorting_events : function(){
            uiSortUserContainer.on('click', '.sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');
                
                cr8v.clear_sorting_classes(uiSortUserContainer.find('.sort_field'));

                if(uiUserGridContainer.is(':visible'))
                {
                    var uiTemplatesGrid = uiUserGridContainer.find('.user_grid:not(.template)');
                    cr8v.ui_sorting(uiThis, sSortBy, uiTemplatesGrid.length, uiTemplatesGrid, uiUserGridContainer);
                }
                else
                {
                    var uiTemplatesList = uiUserListContainer.find('.user_list:not(.template)');
                    cr8v.ui_sorting(uiThis, sSortBy, uiTemplatesList.length, uiTemplatesList, uiUserListContainer);
                }

            });
            
            cr8v.clear_sorting_classes(uiSortUserContainer.find('.sort_field'));

            $(".view-by .grid").off("click").on("click", function() {       
                cr8v.clear_sorting_classes(uiSortUserContainer.find('.sort_field'));

                $(".grid-content").fadeIn();
                $(".table-content").fadeOut();


                $(".dash-table").fadeOut();
                $(".dash-grid").fadeIn();

                $(this).css({
                    'color':'#BDC3C7',
                    'font-size':'20px'
                });
                $(this).next(".list").css({
                    'font-size':'15px',
                    'color':'#fff'
                });
            });

            $(".view-by .list").off("click").on("click", function() {
                cr8v.clear_sorting_classes(uiSortUserContainer.find('.sort_field'));

                $(".table-content").fadeIn();
                $(".grid-content").fadeOut();   

                $(".dash-grid").fadeOut();
                $(".dash-table").fadeIn();  

                $(this).css({
                    'color':'#BDC3C7',
                    'font-size':'22px'
                });
                $(this).prev(".grid").css({
                    'font-size':'15px',
                    'color': '#fff'
                });
            });

            uiSortPersonalStocksContainer.on('click', '.sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');
                
                cr8v.clear_sorting_classes(uiSortPersonalStocksContainer.find('.sort_field'));

                var uiTemplates = uiPersonalStocksContainer.find('.personal_stocks:not(.template)');
                cr8v.ui_sorting(uiThis, sSortBy, uiTemplates.length, uiTemplates, uiPersonalStocksContainer);
            });
            
            cr8v.clear_sorting_classes(uiSortPersonalStocksContainer.find('.sort_field'));
        },

        /**
         * bind_upload_user_list_events
         * @description This function is for binding upload user list events
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_upload_user_list_events : function () {
            uiTriggerUploadList.on('click', function(){
                $(this).next('label').find('input[name="upload_user_list"]').trigger('click');
            });

            /*upload user list*/
            var last_file_path = '';
            var last_file_name = '';
            uiUploadUserListModal.on('change', 'input[name="upload_user_list"]', function(){
                var uiThis = $(this);
                var file = uiUploadUserListModal.find('input[type=file][name="upload_user_list"]')[0];
                var file_path = '';
                var file_name = '';
                var formdata = false;
                var uiLogoContainer = uiUploadUserListModal.find('[data-container="file_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();
                var uiErrorContainer = uiUploadUserListModal.find('.upload_user_list_error_message');

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'csv'/* || sValueExt == 'xls' || sValueExt == 'xlsx'*/)
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('csv_file', file.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'users/upload_user_list_csv',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData))
                                    {
                                        uiUploadUserListBtn.attr('disabled', true);
                                        uiThis.val('');
                                        cr8v.add_error_message(false, uiErrorContainer);   

                                        if(cr8v.var_check(oData.status) && oData.status == true)
                                        {
                                            if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                            {
                                                uiUploadUserListModal.find('.upload_user_list_success_message').removeClass('hidden').show().delay(1000).fadeOut()
                                                    .html('<p class="font-15 success_message"> ' + oData.message[0] + '</p>');
                                            }

                                            var sOrigFileName = (cr8v.var_check(oData.data.original_file_name)) ? oData.data.original_file_name : 'No file uploaded yet';
                                            var sFilePath = (cr8v.var_check(oData.data.file_path) ? oData.data.file_path : '');

                                            if(cr8v.var_check(sUserListFilePath) && sUserListFilePath.length > 0)
                                            {
                                                var sFilePathForDelete = sUserListFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/users/csv/', '');
                                                sFilePathForDelete = './assets/uploads/users/csv/' + sFilePathForDelete;

                                                var oParams = {
                                                    "file_path" : sFilePathForDelete
                                                };

                                                cr8v.delete_file(oParams, function(oData){
                                                
                                                });
                                            }

                                            sUserListFilePath = sFilePath;
                                            sUserListFileName = sOrigFileName;

                                            oUserUploadList = (cr8v.var_check(oData.validate)) ? oData.validate : {};

                                            uiUploadUserListModal.find('#file_name').html('<i>'+sOrigFileName+'</i>');

                                            uiUploadUserListBtn.attr('disabled', false);
                                        }
                                        else
                                        {
                                            sUserListFilePath = '';
                                            sUserListFileName = '';

                                            uiUploadUserListModal.find('#file_name').html('<i>No File Uploaded</i>');

                                            var sErrorMessages = '';
                                            for(var i = 0, max = oData.message.length; i < max; i++)
                                            {
                                                var message = oData.message[i];
                                                sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                            }

                                            cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                        }
                                    }
                                    else
                                    {
                                        file_path = last_file_path;
                                        file_name = last_file_name;
                                    }
                                },
                                complete: function(){

                                }
                            };

                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        sUserListFilePath = '';
                        sUserListFileName = '';
                        uiUploadUserListModal.find('#file_name').html('<i>No File Uploaded</i>');
                        uiUploadUserListBtn.attr('disabled', true);
                        var sErrorMessages = '<p class="font-15 error_message">Please upload the correct template file.</p>';
                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });
    
            uiUploadUserListBtn.on('click', function(){
                roxas.users.view_upload_user_list();
            });

            uiConfirmUploadBtn.on('click', function(){
                var oGetCsvParams = {
                    "file_path" : localStorage.getItem('upload_user_list_path')
                };

                cr8v.show_spinner(uiConfirmUploadBtn, true);

                roxas.users.open_csv(oGetCsvParams, undefined, true, function(oData){

                    cr8v.show_spinner(uiConfirmUploadBtn, false);

                    if(cr8v.var_check(oData))
                    {
                        var iErrorCtr = 0;

                        if(oData.status == true)
                        {
                            if(oData.data.length > 0)
                            {                                    
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oUserInfo = oData.data[i];

                                    var iRow = i + 1;

                                    if(oUserInfo.valid == 0 && count(oUserInfo.error_message) > 0)
                                    {
                                        iErrorCtr++;
                                    }
                                }

                                if(iErrorCtr > 0)
                                {
                                    uiConfirmUploadBtn.attr('disabled', true);
                                }
                                else
                                {
                                    uiConfirmUploadBtn.attr('disabled', false);
                                    roxas.users.assemble_upload_user_list_information(uiConfirmUploadBtn);
                                }
                            }
                        }
                    }
                });
            });

            uiUploadUserListModalBtn.on('click', function(){
                roxas.users.clear_upload_user_list_modal();
            });

            uiCancelUploadBtn.on('click', function(){
                if(cr8v.var_check(localStorage.getItem('upload_user_list_path')) && localStorage.getItem('upload_user_list_path') != null)
                {
                    var sFilePath = localStorage.getItem('upload_user_list_path').replace( roxas.config('url.server.base')+ '/assets/uploads/users/csv/', '');
                    sFilePath = './assets/uploads/users/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    localStorage.removeItem('upload_user_list_path');
                    localStorage.removeItem('upload_user_list_file_name');

                    cr8v.delete_file(oParams, function(oData){
                        window.location = roxas.config('url.server.base') + 'users/user_list';
                    });
                }
            });

            uiUploadUserListModal.on('click', '.close-me', function(){
                if(cr8v.var_check(sUserListFilePath) && sUserListFilePath.length > 0)
                {
                    var sFilePath = sUserListFilePath.replace( roxas.config('url.server.base')+ '/assets/uploads/users/csv/', '');
                    sFilePath = './assets/uploads/users/csv/' + sFilePath;

                    var oParams = {
                        "file_path" : sFilePath
                    };

                    sUserListFilePath = '';
                    sUserListFileName = '';

                    cr8v.delete_file(oParams, function(oData){
                        
                    });
                }
            });
        },

        /**
         * clear_upload_user_list_modal
         * @description This function is for clearing data in upload user list modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_upload_user_list_modal : function(){
            /*clear data*/
            uiUploadUserListBtn.attr('disabled', true);
            uiUploadUserListModal.find('#file_name').html('<i>No File Uploaded</i>');
            uiUploadUserListModal.find('.upload_user_list_error_message').addClass('hidden');
            uiUploadUserListModal.find('.upload_user_list_success_message').addClass('hidden');
        },

        /**
         * assemble_upload_user_list_information
         * @description This function is for assembling the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_upload_user_list_information : function (uiConfirmUploadBtn){
            var oParams = {"data" : []};
            uiUploadUserListContainer.find('.upload_user_list:not(.template)').each(function (key, elem){
                var oData = {
                    "first_name" : cr8v.capitalize($(elem).data('first_name')),
                    "last_name" : cr8v.capitalize($(elem).data('last_name')),
                    "user_name" : $(elem).data('user_name'),
                    "user_role" : $(elem).data('user_role'),
                    "email" : $(elem).data('email'),
                    "contact_number" : $(elem).data('contact_number'),
                    "contact_number_type" : $(elem).data('contact_number_type'),
                    "password" : $(elem).data('password'),
                    "company_id" : $(elem).data('company_id'),
                    "department_id" : $(elem).data('department_id'),
                    "rank_id" : $(elem).data('rank_id'),
                    "employee_code" : $(elem).data('employee_code')
                };
                oParams.data.push(oData);
            });
            roxas.users.insert_upload_user_list(oParams, uiConfirmUploadBtn);
        },

        /**
         * view_upload_user_list
         * @description This function is for viewing the list to be uploaded
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_upload_user_list : function (){
            if(cr8v.var_check(localStorage.getItem('upload_user_list_path')) && localStorage.getItem('upload_user_list_path') != null)
            {
                var sFilePath = localStorage.getItem('upload_user_list_path').replace( roxas.config('url.server.base')+ '/assets/uploads/users/csv/', '');
                sFilePath = './assets/uploads/users/csv/' + sFilePath;

                var oParams = {
                    "file_path" : sFilePath
                };

                cr8v.delete_file(oParams, function(oData){
                
                });
            }
            
            localStorage.removeItem('upload_user_list_path');
            localStorage.setItem('upload_user_list_path' , sUserListFilePath);

            localStorage.removeItem('upload_user_list_file_name');
            localStorage.setItem('upload_user_list_file_name' , sUserListFileName);

            window.location = roxas.config('url.server.base') + "users/upload_user_list";
        },

        /**
         * insert_upload_user_list
         * @description This function is for inserting the validated user list
         * @dependencies 
         * @param {object} oUserUploadList
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        insert_upload_user_list : function (oParams, uiBtn) {
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "users/insert_upload_user_list",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                $('.insert_upload_list_success_message').removeClass('hidden').html('<p class="font-15 success_message">Users Added successfully.</p>');
                               
                                setTimeout(function(){
                                    if(cr8v.var_check(localStorage.getItem('upload_user_list_path')) && localStorage.getItem('upload_user_list_path') != null)
                                    {
                                        var sFilePath = localStorage.getItem('upload_user_list_path').replace( roxas.config('url.server.base')+ '/assets/uploads/users/csv/', '');
                                        sFilePath = './assets/uploads/users/csv/' + sFilePath;

                                        var oParams = {
                                            "file_path" : sFilePath
                                        };

                                        localStorage.removeItem('upload_user_list_path');
                                        localStorage.removeItem('upload_user_list_file_name');

                                        cr8v.delete_file(oParams, function(oData){
                                            window.location = roxas.config('url.server.base') + 'users/user_list';
                                        });
                                    }
                                }, 1000)
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * open_csv
         * @description This function is for opening and viewing of the csv passed as parameter
         * @dependencies 
         * @param {object} oParams
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        open_csv : function (oParams, uiThis, bValidateOnly, fnCallback){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "users/open_csv",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)

                        if(typeof(fnCallback) == 'function')
                        {
                            fnCallback(oData);
                        }

                        if(cr8v.var_check(bValidateOnly) && bValidateOnly)
                        {
                            return false;
                        }

                        if(cr8v.var_check(oData))
                        {
                            uiUploadUserListContainer.find('.upload_user_list:not(.template)').remove();
                            uiNoErrorsFound.addClass('template hidden');
                            var iErrorCtr = 0;

                            if(oData.status == true)
                            {
                                if(oData.data.length > 0)
                                {                                    
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oUserInfo = oData.data[i];

                                        var iRow = i + 1;
    
                                        var uiTemplate = $('.upload_user_list.template').clone().removeClass('template hidden');
                                        var uiManipulatedTemplate = roxas.users.manipulate_upload_user_list(oUserInfo, uiTemplate);

                                        uiManipulatedTemplate.find('[data-label="row"]').text(iRow);

                                        if(oUserInfo.valid == 0 && count(oUserInfo.error_message) > 0)
                                        {
                                            iErrorCtr++;
                                            // uiManipulatedTemplate.css({'border' : '1px solid red'});

                                            var uiErrorTemplate = uiUploadUserListErrorContainer.find('.upload_user_list_error.template').clone().removeClass('template hidden');
                                            var uiManipulatedErrorTemplate = roxas.users.manipulate_upload_user_list_error(oUserInfo, uiErrorTemplate, uiManipulatedTemplate);

                                            uiManipulatedErrorTemplate.find('[data-label="row_error"]').text(iRow);

                                            uiUploadUserListErrorContainer.append(uiManipulatedErrorTemplate);
                                        }

                                        uiUploadUserListContainer.append(uiManipulatedTemplate);
                                    }

                                    if(iErrorCtr > 0)
                                    {
                                        uiConfirmUploadBtn.attr('disabled', true);
                                    }
                                    else
                                    {
                                        uiConfirmUploadBtn.attr('disabled', false);
                                        uiNoErrorsFound.removeClass('template hidden');
                                    }
                                }
                                else
                                {
                                    uiNoErrorsFound.removeClass('template hidden');
                                }
                            }
                            else
                            {
                                uiNoErrorsFound.removeClass('template hidden');

                                if(cr8v.var_check(oData.message))
                                {
                                    var sErrorMessages = '';
                                    var uiErrorContainer = $('.insert_upload_list_error_message');
                                    for(var i = 0, max = oData.message.length; i < max; i++)
                                    {
                                        var message = oData.message[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.users.ajax(oAjaxConfig);

            }
        },

        /**
         * manipulate_upload_user_list
         * @description This function is for rendering upload user list details
         * @dependencies 
         * @param {object} oUserInfo
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_upload_user_list : function (oUserInfo, uiTemplate){
            if(cr8v.var_check(oUserInfo) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.data('first_name', oUserInfo.first_name);
                uiTemplate.data('last_name', oUserInfo.last_name);
                uiTemplate.data('user_name', oUserInfo.user_name);
                uiTemplate.data('user_role', oUserInfo.user_role);
                uiTemplate.data('email', oUserInfo.email);
                uiTemplate.data('contact_number', oUserInfo.contact_number);
                uiTemplate.data('contact_number_type', oUserInfo.contact_number_type);
                uiTemplate.data('password', oUserInfo.password);
                uiTemplate.data('company_id', oUserInfo.company_id);
                uiTemplate.data('department_id', oUserInfo.department_id);
                uiTemplate.data('rank_id', oUserInfo.rank_id);
                uiTemplate.data('employee_code', oUserInfo.employee_code);

                uiTemplate.find('[data-label="employee_code"]').text(oUserInfo.employee_code);
                uiTemplate.find('[data-label="first_name"]').text(oUserInfo.first_name);
                uiTemplate.find('[data-label="last_name"]').text(oUserInfo.last_name);
                uiTemplate.find('[data-label="company_code"]').text(oUserInfo.company_code);
                uiTemplate.find('[data-label="department_code"]').text(oUserInfo.department_code);
                uiTemplate.find('[data-label="rank_code"]').text(oUserInfo.rank_code);
                uiTemplate.find('[data-label="user_name"]').text(oUserInfo.user_name);
                uiTemplate.find('[data-label="user_role_name"]').text(oUserInfo.user_role_name);
                uiTemplate.find('[data-label="password"]').text(oUserInfo.password);
                uiTemplate.find('[data-label="contact_number_type_name"]').text(oUserInfo.contact_number_type_name);
                uiTemplate.find('[data-label="contact_number"]').text(oUserInfo.contact_number);
                uiTemplate.find('[data-label="email"]').text(oUserInfo.email);

                return uiTemplate;
            }
        },

        /**
         * manipulate_upload_user_list_error
         * @description This function is for rendering upload user list error messages
         * @dependencies 
         * @param {object} oUserInfo
         * @param {jQuery} uiTemplate
         * @param {jQuery} uiPreviewTemplate
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_upload_user_list_error : function (oUserInfo, uiTemplate, uiPreviewTemplate){
            if(cr8v.var_check(oUserInfo) && cr8v.var_check(uiTemplate))
            {
                var uiRowNameErrorContainer = uiTemplate.find('[data-container="row_name_error"]');
                var uiRowMessageErrorContainer = uiTemplate.find('[data-container="row_message_error"]');

                for (var k in oUserInfo.error_message) {
                    var oErrorMessage = oUserInfo.error_message[k];

                    uiTemplate.find('[data-label="row_name_error"]').append(k+'</br> ');
                    uiTemplate.find('[data-label="row_message_error"]').append(oErrorMessage+'</br> ');
                    uiPreviewTemplate.find('[data-error="'+k+'"]').attr({'style' : 'border: 1px solid red;'});
                }


                return uiTemplate;
            }
        },

         makeid : function()
        {
            var text = "";
            var possible = "0123456789";

            for( var i=0; i < 5; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('users/add_user') > -1)
            {
                var text = "";
                var possible = "0123456789";

                for( var i=0; i < 5; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));

                localStorage.removeItem("user_id");

                var oGetCompanyParams = {
                    "limit" : 999999
                };
                roxas.users.get_companies(oGetCompanyParams);

                var oGetDepartmentParams = {
                    "limit" : 999999
                };
                roxas.users.get_departments(oGetDepartmentParams);

                var oGetRankParams = {
                    "limit" : 999999
                };
                roxas.users.get_ranks(oGetRankParams);

                /*for dropdown of companies*/
                roxas.users.bind_dropdown_events();

                /*for add of user*/
                roxas.users.bind_add_user_events();
                       
               /* For employee Code numbers only*/
               var uiAddUserForm = $("#add_user_form"),
                   uiEmployeeCode = uiAddUserForm.find('input[name="employee_code"]');
                   uiEmployeeCode.val(text);
               roxas.users.numbers_only(uiEmployeeCode);
                        
            }
            else if(window.location.href.indexOf('users/upload_user_list') > -1)
            {
                var oDeleteUserListCsvParams = {
                    "directory" : "./assets/uploads/users/csv",
                    'exception_files' : []
                };

                if(localStorage.getItem('upload_user_list_path') != null && localStorage.getItem('upload_user_list_file_name') != null)
                {
                    localStorage.removeItem("user_id");

                    $('[data-label="upload_user_list_file_name"]').text(localStorage.getItem('upload_user_list_file_name'));

                    roxas.users.bind_upload_user_list_events();

                    var oGetCsvParams = {
                        "file_path" : localStorage.getItem('upload_user_list_path')
                    };

                    roxas.users.open_csv(oGetCsvParams);

                    var sFilePath = localStorage.getItem('upload_user_list_path').replace( roxas.config('url.server.base')+ '/assets/uploads/users/csv/', '');
                    oDeleteUserListCsvParams.exception_files.push(sFilePath);
                }
                else
                {
                    window.location = roxas.config('url.server.base') + 'users/user_list';
                }

                // cr8v.delete_all_except(oDeleteUserListCsvParams);
            }
            else if(window.location.href.indexOf('users/user_list') > -1)
            {
                localStorage.removeItem("user_id");

                var oGetCompanyParams = {
                    "limit" : 999999
                };
                roxas.users.get_companies(oGetCompanyParams);

                var oGetUserParams = {
                    "limit" : 999999
                };
                roxas.users.get_users(oGetUserParams);
                
//                 roxas.users.get_users_info(oGetUserParams);

                /*for dropdown of companies*/
                roxas.users.bind_dropdown_events();

                roxas.users.bind_search_event();

                roxas.users.bind_upload_user_list_events();

                uiDivContent.on('click', '.view_user_info', function(){
                    var uiThis = $(this);
                    var iUserID = uiThis.parents('[data-user-id]:first').attr('data-user-id');
                    roxas.users.view_user_info(iUserID);
                });
            }
            else if(window.location.href.indexOf('users/user_info') > -1 && localStorage.getItem('user_id') != null)
            {
                var oGetUserParams = {
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('user_id'),
                };
                roxas.users.get_users(oGetUserParams, undefined, true);

                var oGetEsopParams = {
                        "limit" : 999999,
                        "where" : [
                        {
                            'field' : 'user_id',
                            'value' : localStorage.getItem('user_id')
                        }
                    ],
                    group_by : "esop_id"
                };

                roxas.users.get_personal_stocks(oGetEsopParams);
            }
            else if(window.location.href.indexOf('users/edit_user') > -1 && localStorage.getItem('user_id') != null)
            {
                var oGetCompanyParams = {
                    "limit" : 999999
                };
                roxas.users.get_companies(oGetCompanyParams);

                var oGetDepartmentParams = {
                    "limit" : 999999
                };
                roxas.users.get_departments(oGetDepartmentParams);

                var oGetRankParams = {
                    "limit" : 999999
                };
                roxas.users.get_ranks(oGetRankParams);

                /*for dropdown of companies*/
                roxas.users.bind_dropdown_events();

                roxas.users.bind_edit_user_events();

                var oGetUserParams = {
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('user_id'),
                };

                roxas.users.get_users(oGetUserParams, undefined, true);
            }
            else if(window.location.href.indexOf('users/user_profile') > -1)
            {
                var uiTemplate = $('[data-container="user_info_container"]');
                var uiManipulatedTemplate = roxas.users.manipulate_template_list(oUserInfo, uiTemplate);

                roxas.users.bind_user_profile_events();
            }
            
        },

        /**
         * clear_change_password_modal
         * @description This function is for clearing data in change password modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_change_password_modal : function(){
            /*clear data*/
            uiChangePassForm.find('input').removeClass('input-error').val('').attr('type', 'password');
            uiChangePassForm.find('.change_password_success_message').addClass('hidden');
            uiChangePassForm.find('.change_password_error_message').addClass('hidden');
        },

        /**
         * view_user_info
         * @description This function is for viewing the user info
         * @dependencies 
         * @param {int} iCompanyID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_user_info : function(iUserID){
            if(cr8v.var_check(iUserID))
            {
                localStorage.removeItem("user_id");
                localStorage.setItem('user_id', iUserID);
                window.location =  roxas.config('url.server.base') + "users/user_info/";
            }
        },

        /**
         * bind_dropdown_events
         * @description This function will bind the events for the dropdown of ranks companies and departments
         * @dependencies N/A
         * @param N/A
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_dropdown_events : function(){

            uiCompanyDropdown.find('.frm-custom-dropdown-txt').find('input').val('').attr('disabled', true).attr({'datavalid' : 'required', 'labelinput' : 'Company'});
            uiDepartmentDropdown.find('.frm-custom-dropdown-txt').find('input').val('').attr('disabled', true).attr({'datavalid' : 'required', 'labelinput' : 'Department'});
            uiRankDropdown.find('.frm-custom-dropdown-txt').find('input').val('').attr('disabled', true).attr({'datavalid' : 'required', 'labelinput' : 'Rank'});
            uiContactDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiRoleDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiUserSearchDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiUserCompanyUserRoleDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiUserRoleDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            uiEmploymentStatusDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);

            uiCompanyDropdown.on('click', '.option.companies', function(){
                var uiThis = $(this);
                var iCompanyID = uiThis.attr('data-value');

                uiCompanyDropdown.attr('company_id', iCompanyID);

                if(cr8v.var_check(iCompanyID))
                {
                    uiDepartmentDropdown.removeAttr('department_id');

                    uiDepartmentDropdown.find('.frm-custom-dropdown-txt').find('input').val('');
                    uiDepartmentDropdown.find('.option.departments').addClass('hidden');
                    uiDepartmentDropdown.find('.option.departments[data-company-id="'+iCompanyID+'"]').removeClass('hidden');

                    uiRankDropdown.removeAttr('rank_id');
                    uiRankDropdown.find('.frm-custom-dropdown-txt').find('input').val('');
                    uiRankDropdown.find('.option.ranks').addClass('hidden');
                }

                uiUserCompanyUserRoleDropdown.find('.option:first').trigger('click');
            });

            uiDepartmentDropdown.on('click', '.option.departments', function(){
                var uiThis = $(this);
                var iDepartmentID = uiThis.attr('data-value');

                uiDepartmentDropdown.attr('department_id', iDepartmentID);

                if(cr8v.var_check(iDepartmentID))
                {
                    uiRankDropdown.removeAttr('rank_id');
                    uiRankDropdown.find('.frm-custom-dropdown-txt').find('input').val('');
                    uiRankDropdown.find('.option.ranks').addClass('hidden');
                    uiRankDropdown.find('.option.ranks[data-department-id="'+iDepartmentID+'"]').removeClass('hidden');
                }
            });


            uiRankDropdown.on('click', '.option.ranks', function(){
                var uiThis = $(this);
                var iRankID = uiThis.attr('data-value');

                uiRankDropdown.attr('rank_id', iRankID);

            });

            uiRoleDropdown.on('click', '.option', function(){
                var uiThis = $(this);
                var sRoleName = uiThis.attr('data-value');

                var iUserRole = roxas.users.validate_user_role(false, sRoleName);
                uiRoleDropdown.attr('user_role', iUserRole);
            });

            uiRoleDropdown.find('.option:first').trigger('click');

            uiContactTypeDropdown.on('click', '.option', function(){
                var uiThis = $(this);
                var sContactType = uiThis.attr('data-value');

                var iContactType = roxas.users.validate_contact_type(false, sContactType);
                uiContactTypeDropdown.attr('contact_type', iContactType);
            });

            uiContactTypeDropdown.find('.option:first').trigger('click');

            uiUserSearchDropdown.on('click', '.option', function(){
                if($(this).attr('data-value') == 'Company')
                {
                    uiCompanyDropdown.find('.option:first').trigger('click');
                }
                else if($(this).attr('data-value') == 'User Role')
                {
                    uiUserRoleDropdown.find('.option:first').trigger('click');
                }
            });

            uiUserCompanyUserRoleDropdown.on('click', '.option', function(){
                var uiThis = $(this);
                var sRoleName = uiThis.attr('data-value');

                var iUserRole = roxas.users.validate_user_role(false, sRoleName);
                uiUserCompanyUserRoleDropdown.attr('user_role', iUserRole);
            });

            uiUserRoleDropdown.on('click', '.option', function(){
                var uiThis = $(this);
                var sRoleName = uiThis.attr('data-value');

                var iUserRole = roxas.users.validate_user_role(false, sRoleName);
                uiUserRoleDropdown.attr('user_role', iUserRole);
            });

            uiEmploymentStatusDropdown.on('click', '.option', function(){
                var uiThis = $(this);
                var iEmploymentStatus = uiThis.attr('data-value');

                uiEmploymentStatusDropdown.attr('employement_status', iEmploymentStatus);
            });
        },

        /**
         * bind_add_user_events
         * @description This function is for binding events for add user info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_add_user_events : function(){

            /*save new user*/
            uiAddUserBtn.on('click', function(){
                uiAddUserForm.submit();
            });

            /*prevent on submit of form*/
            uiAddUserForm.on('submit', function (e) {
                e.preventDefault();
            });

            /*add a validation to adding a new rank*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiAddUserBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiAddUserForm.find('.add_user_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiAddUserForm.find('.add_user_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiAddUserBtn, true);
                    
                    var bIsPasswordMatch = roxas.users.validate_password(uiAddUserForm);
                    if(bIsPasswordMatch)
                    {
                       roxas.users.assemble_add_user_information(uiAddUserForm, uiAddUserBtn);
                    }
                    else
                    {
                        cr8v.show_spinner(uiAddUserBtn, false);

                        uiAddUserForm.find('[name="password"]').addClass('input-error');
                        uiAddUserForm.find('[name="confirm_password"]').addClass('input-error');

                        var sErrorMessages = '<p class="font-15 error_message">Password did not match.</p>';
                        var uiErrorContainer = uiAddUserForm.find('.add_user_error_message');

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                }
            };

            /*bind the validation to add new user form*/
            cr8v.validate_form(uiAddUserForm, oValidationConfig);

            /*trigger input file*/
            uiAddUserForm.on('click', '.upload-photo-ver2', function(){
                $(this).next('label').find('input').trigger('click');
            });

            /*upload user image*/
            var last_image_path = ''/*sImagePath*/;
            var last_image_name = ''/*sImageFile*/;
            uiAddUserForm.on('change', 'input[name="user_image"]', function(){
                var uiThis = $(this);
                var image = uiAddUserForm.find('input[type=file][name="user_image"]')[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = uiAddUserForm.find('[data-container="image_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'jpg' || sValueExt == 'jpeg' || sValueExt == 'gif' || sValueExt == 'png')
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('client_img', image.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'users/ajax_upload_image',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData.data))
                                    {
                                        image_path = (cr8v.var_check(oData.data.image_path)) ? oData.data.image_path : last_image_path;
                                        image_name = (cr8v.var_check(oData.data.image_name)) ? oData.data.image_name : '';
                                        last_image_path = image_path;
                                        last_image_name = image_name;

                                        if(cr8v.var_check(oData.status))
                                        {
                                            var tthtml = oData.message;
                                            var ttx = uiLogoContainer.offset().left + 50;
                                            var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                                            $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                                            setTimeout(function() {
                                                $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                                            }, 2000);
                                        }
                                    }
                                    else
                                    {
                                        image_path = last_image_path;
                                        image_name = last_image_name;
                                    }

                                },
                                complete: function(){
                                    uiLogoContainer.find('.preloader').remove();
                                    if(cr8v.var_check(image_path))
                                    {
                                        var sStyle = '';
                                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ image_path).removeClass('hidden');
                                        uiThis.attr('image_name', image_name);
                                        uiThis.data('image_name', image_name);

                                        uiThis.attr('image_path', image_path);
                                        uiThis.data('image_path', image_path);
                                    }

                                }
                            };
                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        image_name = last_image_name;
                        uiThis.val('');
                        uiThis.attr('image_name', image_name);
                        uiThis.data('image_name', image_name);

                        uiThis.attr('image_path', image_path);
                        uiThis.data('image_path', image_path);

                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ last_image_path).removeClass('hidden');

                        var tthtml = 'Invalid image file, please try again.';
                        var ttx = uiLogoContainer.offset().left + 50;
                        var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                        $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                        setTimeout(function() {
                            $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                        }, 2000);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });

            uiAddUserForm.on('click', '.show_confirm_password', function(){
                var uiConfirmPassword = uiAddUserForm.find('[name="confirm_password"]');
                if(uiConfirmPassword.attr('type') == 'text')
                {
                    uiConfirmPassword.attr('type', 'password');
                }
                else
                {
                    uiConfirmPassword.attr('type', 'text');
                }
            });

            uiAddUserForm.on('click', '.show_password', function(){
                var uiPassword = uiAddUserForm.find('[name="password"]');
                if(uiPassword.attr('type') == 'text')
                {
                    uiPassword.attr('type', 'password');
                }
                else
                {
                    uiPassword.attr('type', 'text');
                }
            });
        },

        /**
         * bind_edit_user_events
         * @description This function is for binding events for edit user info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_edit_user_events : function(){

            /*save new user*/
            uiEditUserBtn.on('click', function(){
                uiEditUserForm.submit();
            });

            /*prevent on submit of form*/
            uiEditUserForm.on('submit', function (e) {
                e.preventDefault();
            });

            /*add a validation to adding a new rank*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiEditUserBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiEditUserForm.find('.add_user_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiEditUserForm.find('.add_user_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiEditUserBtn, true);
                    
                    var bIsPasswordMatch = roxas.users.validate_password(uiEditUserForm);
                    if(bIsPasswordMatch)
                    {
                       roxas.users.assemble_edit_user_information(uiEditUserForm, uiEditUserBtn);
                    }
                    else
                    {
                        cr8v.show_spinner(uiEditUserBtn, false);

                        uiEditUserForm.find('[name="password"]').addClass('input-error');
                        uiEditUserForm.find('[name="confirm_password"]').addClass('input-error');

                        var sErrorMessages = '<p class="font-15 error_message">Password did not match.</p>';
                        var uiErrorContainer = uiEditUserForm.find('.add_user_error_message');

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                }
            };

            /*bind the validation to add new user form*/
            cr8v.validate_form(uiEditUserForm, oValidationConfig);

            /*trigger input file*/
            uiEditUserForm.on('click', '.upload-photo-ver2', function(){
                $(this).next('label').find('input').trigger('click');
            });

            /*upload user image*/
            var last_image_path = ''/*sImagePath*/;
            var last_image_name = ''/*sImageFile*/;
            uiEditUserForm.on('change', 'input[name="user_image"]', function(){
                var uiThis = $(this);
                var image = uiEditUserForm.find('input[type=file][name="user_image"]')[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = uiEditUserForm.find('[data-container="image_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'jpg' || sValueExt == 'jpeg' || sValueExt == 'gif' || sValueExt == 'png')
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('client_img', image.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'users/ajax_upload_image',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData.data))
                                    {
                                        image_path = (cr8v.var_check(oData.data.image_path)) ? oData.data.image_path : last_image_path;
                                        image_name = (cr8v.var_check(oData.data.image_name)) ? oData.data.image_name : '';
                                        last_image_path = image_path;
                                        last_image_name = image_name;

                                        if(cr8v.var_check(oData.status))
                                        {
                                            var tthtml = oData.message;
                                            var ttx = uiLogoContainer.offset().left + 50;
                                            var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                                            $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                                            setTimeout(function() {
                                                $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                                            }, 2000);
                                        }
                                    }
                                    else
                                    {
                                        image_path = last_image_path;
                                        image_name = last_image_name;
                                    }

                                },
                                complete: function(){
                                    uiLogoContainer.find('.preloader').remove();
                                    if(cr8v.var_check(image_path))
                                    {
                                        var sStyle = '';
                                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ image_path).removeClass('hidden');
                                        uiThis.attr('image_name', image_name);
                                        uiThis.data('image_name', image_name);

                                        uiThis.attr('image_path', image_path);
                                        uiThis.data('image_path', image_path);
                                    }

                                }
                            };
                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        image_name = last_image_name;
                        uiThis.val('');
                        uiThis.attr('image_name', image_name);
                        uiThis.data('image_name', image_name);

                        uiThis.attr('image_path', image_path);
                        uiThis.data('image_path', image_path);

                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ last_image_path).removeClass('hidden');

                        var tthtml = 'Invalid image file, please try again.';
                        var ttx = uiLogoContainer.offset().left + 50;
                        var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                        $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                        setTimeout(function() {
                            $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                        }, 2000);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });

            uiEditUserForm.on('click', '.show_confirm_password', function(){
                var uiConfirmPassword = uiEditUserForm.find('[name="confirm_password"]');
                if(uiConfirmPassword.attr('type') == 'text')
                {
                    uiConfirmPassword.attr('type', 'password');
                }
                else
                {
                    uiConfirmPassword.attr('type', 'text');
                }
            });

            uiEditUserForm.on('click', '.show_password', function(){
                var uiPassword = uiEditUserForm.find('[name="password"]');
                if(uiPassword.attr('type') == 'text')
                {
                    uiPassword.attr('type', 'password');
                }
                else
                {
                    uiPassword.attr('type', 'text');
                }
            });
        },


        /**
         * bind_user_profile_events
         * @description This function is for binding events for user profile page
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_user_profile_events : function(){

            uiChangePassModalBtn.on('click', function(){
                /*clear data*/
                roxas.users.clear_change_password_modal();
            });

            uiChangePassForm.on('click', '.show_current_password', function(){
                var uiOldPassword = uiChangePassForm.find('[name="current_password"]');
                if(uiOldPassword.attr('type') == 'text')
                {
                    uiOldPassword.attr('type', 'password');
                }
                else
                {
                    uiOldPassword.attr('type', 'text');
                }
            });

            uiChangePassForm.on('click', '.show_password', function(){
                var uiNewPassword = uiChangePassForm.find('[name="password"]');
                if(uiNewPassword.attr('type') == 'text')
                {
                    uiNewPassword.attr('type', 'password');
                }
                else
                {
                    uiNewPassword.attr('type', 'text');
                }
            });

            uiChangePassForm.on('click', '.show_confirm_password', function(){
                var uiConfirmNewPassword = uiChangePassForm.find('[name="confirm_password"]');
                if(uiConfirmNewPassword.attr('type') == 'text')
                {
                    uiConfirmNewPassword.attr('type', 'password');
                }
                else
                {
                    uiConfirmNewPassword.attr('type', 'text');
                }
            });

            /*save change password*/
            uiChangePassBtn.on('click', function(){
                uiChangePassForm.submit();
            });

            /*prevent on submit of form*/
            uiChangePassForm.on('submit', function (e) {
                e.preventDefault();
            });

            /*add a validation to changing password*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiChangePassBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiChangePassForm.find('.change_password_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiChangePassForm.find('.change_password_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiChangePassBtn, true);
                    
                    var bIsPasswordMatch = roxas.users.validate_password(uiChangePassForm);
                    if(bIsPasswordMatch)
                    {
                       roxas.users.assemble_change_password_information(uiChangePassForm, uiChangePassBtn);
                    }
                    else
                    {
                        cr8v.show_spinner(uiChangePassBtn, false);

                        uiChangePassForm.find('[name="password"]').addClass('input-error');
                        uiChangePassForm.find('[name="confirm_password"]').addClass('input-error');

                        var sErrorMessages = '<p class="font-15 error_message">Passwords did not match.</p>';
                        var uiErrorContainer = uiChangePassForm.find('.change_password_error_message');

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                }
            };

            /*bind the validation to change password form*/
            cr8v.validate_form(uiChangePassForm, oValidationConfig);
        },

        /**
         * validate_password
         * @description This function will validate password
         * @dependencies N/A
         * @param {jQuery} uiForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        validate_password : function(uiForm){
            /*password*/
            var bReturn = false;
            var sPassword = uiForm.find('[name="password"]').val();
            var sConfirmPassword = uiForm.find('[name="confirm_password"]').val();

            if(sPassword === sConfirmPassword)
            {
                bReturn = true;
            }

            return bReturn;
        },

        /**
         * validate_user_role
         * @description This function will validate user role
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        validate_user_role : function(bPassedIsID, sRoleName, iUserRole){
            if(cr8v.var_check(bPassedIsID) && bPassedIsID)
            {
                if(cr8v.var_check(iUserRole))
                {
                    var sRoleName = '';
                    if(iUserRole == '1')
                    {
                        sRoleName = 'employee';
                    }
                    else if(iUserRole == '2')
                    {
                        sRoleName = 'hr department';
                    }
                    else if(iUserRole == '3')
                    {
                        sRoleName = 'hr head';
                    }
                    else if(iUserRole == '4')
                    {
                        sRoleName = 'esop admin';
                    }

                    return sRoleName;
                }
            }
            else
            {  
                if(cr8v.var_check(sRoleName))
                {
                    var iUserRole = 0;
                    if(sRoleName.toLowerCase() == 'employee')
                    {
                        iUserRole = 1;
                    }
                    else if(sRoleName.toLowerCase() == 'hr department')
                    {
                        iUserRole = 2;
                    }
                    else if(sRoleName.toLowerCase() == 'hr head')
                    {
                        iUserRole = 3;
                    }
                    else if(sRoleName.toLowerCase() == 'esop admin')
                    {
                        iUserRole = 4;
                    }

                    return iUserRole;
                }
            }
        },

        /**
         * validate_contact_type
         * @description This function will validate contact type
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        validate_contact_type : function(bPassedIsID, sContactType, iContactType){
            if(cr8v.var_check(bPassedIsID) && bPassedIsID)
            {
                if(cr8v.var_check(iContactType))
                {
                    var sContactType = '';
                    if(iContactType == '1')
                    {
                        sContactType = 'mobile';
                    }
                    else if(iContactType == '2')
                    {
                        sContactType = 'house';
                    }
                    else if(iContactType == '3')
                    {
                        sContactType = 'company';
                    }
                    return sContactType;
                }
            }
            else
            {      
                if(cr8v.var_check(sContactType))
                {
                    var iContactType = 0;
                    if(sContactType.toLowerCase() == 'mobile')
                    {
                        iContactType = 1;
                    }
                    else if(sContactType.toLowerCase() == 'house')
                    {
                        iContactType = 2;
                    }
                    else if(sContactType.toLowerCase() == 'company')
                    {
                        iContactType = 3;
                    }
                    return iContactType;
                }
            }
        },

        /**
         * assemble_add_user_information
         * @description This function will assemble add user information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_add_user_information : function(uiAddUserForm, uiAddUserBtn){
            var iCompanyID = '';
            var iDepartmentID = '';
            var iRankID = '';
            var iUserRole = '';
            var iContactType = '';
            if(cr8v.var_check(uiCompanyDropdown.attr('company_id')))
            {
                iCompanyID = uiCompanyDropdown.attr('company_id');
            }

            if(cr8v.var_check(uiDepartmentDropdown.attr('department_id')))
            {
                iDepartmentID = uiDepartmentDropdown.attr('department_id');
            }

            if(cr8v.var_check(uiRankDropdown.attr('rank_id')))
            {
                iRankID = uiRankDropdown.attr('rank_id');
            }

            if(cr8v.var_check(uiRoleDropdown.attr('user_role')))
            {
                iUserRole = uiRoleDropdown.attr('user_role');
            }

            if(cr8v.var_check(uiContactTypeDropdown.attr('contact_type')))
            {
                iContactType = uiContactTypeDropdown.attr('contact_type');
            }

            var oParams = {
                'first_name'              : uiAddUserForm.find('[name="first_name"]').val(),
                'middle_name'             : uiAddUserForm.find('[name="middle_name"]').val(),
                'last_name'               : uiAddUserForm.find('[name="last_name"]').val(),
                'employee_code'           : uiAddUserForm.find('[name="employee_code"]').val(),
                'company_id'              : iCompanyID,
                'company_name'            : uiCompanyDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                'department_id'           : iDepartmentID,
                'department_name'         : uiDepartmentDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                'rank_id'                 : iRankID,
                'rank_name'               : uiRankDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                'user_role'               : iUserRole,
                'user_role_name'          : uiRoleDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                'user_name'               : uiAddUserForm.find('[name="user_name"]').val(),
                'password'                : uiAddUserForm.find('[name="password"]').val(),
                'contact_number_type'     : iContactType,
                'contact_number_type_name': uiContactTypeDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                'contact_number'          : uiAddUserForm.find('[name="contact_number"]').val(),
                'email'                   : uiAddUserForm.find('[name="email"]').val(),
                'img'                     : uiAddUserForm.find('input[name="user_image"]').attr('image_path'),
            };

            roxas.users.add_user(oParams, uiAddUserBtn);
        },

        /**
         * assemble_edit_user_information
         * @description This function will assemble edit user information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_edit_user_information : function(uiEditUserForm, uiEditUserBtn){
            var iCompanyID = '';
            var iDepartmentID = '';
            var iRankID = '';
            var iUserRole = '';
            var iContactType = '';
            var iEmploymentStatus = '';
            if(cr8v.var_check(uiCompanyDropdown.attr('company_id')))
            {
                iCompanyID = uiCompanyDropdown.attr('company_id');
            }

            if(cr8v.var_check(uiDepartmentDropdown.attr('department_id')))
            {
                iDepartmentID = uiDepartmentDropdown.attr('department_id');
            }

            if(cr8v.var_check(uiRankDropdown.attr('rank_id')))
            {
                iRankID = uiRankDropdown.attr('rank_id');
            }

            if(cr8v.var_check(uiRoleDropdown.attr('user_role')))
            {
                iUserRole = uiRoleDropdown.attr('user_role');
            }

            if(cr8v.var_check(uiContactTypeDropdown.attr('contact_type')))
            {
                iContactType = uiContactTypeDropdown.attr('contact_type');
            }

            if(cr8v.var_check(uiEmploymentStatusDropdown.attr('employement_status')))
            {
                iEmploymentStatus = uiEmploymentStatusDropdown.attr('employement_status');
            }

            var oParams = {
                'id' : localStorage.getItem('user_id'),
                'data' : [{
                    'first_name'              : uiEditUserForm.find('[name="first_name"]').val(),
                    'middle_name'             : uiEditUserForm.find('[name="middle_name"]').val(),
                    'last_name'               : uiEditUserForm.find('[name="last_name"]').val(),
                    'employee_code'           : uiEditUserForm.find('[name="employee_code"]').val(),
                    'company_id'              : iCompanyID,
                    'company_name'            : uiCompanyDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                    'department_id'           : iDepartmentID,
                    'department_name'         : uiDepartmentDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                    'rank_id'                 : iRankID,
                    'rank_name'               : uiRankDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                    'user_role'               : iUserRole,
                    'user_role_name'          : strtolower(uiRoleDropdown.find('.frm-custom-dropdown-txt').find('input').val()),
                    'user_name'               : uiEditUserForm.find('[name="user_name"]').val(),
                    'password'                : uiEditUserForm.find('[name="password"]').val(),
                    'contact_number_type'     : iContactType,
                    'contact_number_type_name': uiContactTypeDropdown.find('.frm-custom-dropdown-txt').find('input').val(),
                    'contact_number'          : uiEditUserForm.find('[name="contact_number"]').val(),
                    'email'                   : uiEditUserForm.find('[name="email"]').val(),
                    'img'                     : uiEditUserForm.find('input[name="user_image"]').attr('image_path'),
                    'employment_status'       : iEmploymentStatus
                }]
            };

            roxas.users.edit_user(oParams, uiEditUserBtn);
        },


        /**
         * assemble_change_password_information
         * @description This function will assemble add user information
         * @dependencies N/A
         * @param {jQuery} uiAddUserForm
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_change_password_information : function(uiChangePassForm, uiChangePassBtn){

            var oParams = {
                'id' : iUserID, // from global var / session at header.php
                'current_password' : uiChangePassForm.find('[name="current_password"]').val(),
                'data' : [{
                    'password' : uiChangePassForm.find('[name="password"]').val(),
                }]
            };

            roxas.users.change_password(oParams, uiChangePassBtn);
        },

        /**
         * change_password
         * @description This function will change the password
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        change_password : function(oParams, uiChangePassBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "users/change_password",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiChangePassBtn)) {
                            cr8v.show_spinner(uiChangePassBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiChangePassForm.find('.change_password_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    uiChangePassModal.find('.close-me').trigger('click');
                                    roxas.users.clear_change_password_modal();
                                }, 1000);
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiChangePassForm.find('.change_password_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiChangePassBtn)) {
                            cr8v.show_spinner(uiChangePassBtn, false);
                        }
                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * edit_user
         * @description This function will edit user info
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        edit_user : function(oParams, uiEditUserBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "users/edit",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiEditUserBtn)) {
                            cr8v.show_spinner(uiEditUserBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiEditUserForm.find('.edit_user_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    var oUserData = oData.data;
                                    roxas.users.view_user_info(oUserData.id);
                                }, 1000)
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiEditUserForm.find('.edit_user_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiEditUserBtn)) {
                            cr8v.show_spinner(uiEditUserBtn, false);
                        }
                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * add_user
         * @description This function will add user info
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_user : function(oParams, uiAddUserBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "users/add",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiAddUserBtn)) {
                            cr8v.show_spinner(uiAddUserBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddUserForm.find('.add_user_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    var oUserData = oData.data;
                                    roxas.users.view_user_info(oUserData.id);
                                }, 1000)
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiAddUserForm.find('.add_user_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiAddUserBtn)) {
                            cr8v.show_spinner(uiAddUserBtn, false);
                        }
                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * get_companies
         * @description This function is for getting and rendering companies on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_companies : function(oParams){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "companies/get",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiCompanyDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oCompanyData = oData.data[i];

                                    var uiTemplate = uiCompanyDropdown;
                                    cr8v.append_dropdown(oCompanyData, uiTemplate, false, 'companies');
                                }
                            }
                        }
                    },
                    "complete": function () {

                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * get_departments
         * @description This function is for getting and rendering departments on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_departments : function(oParams){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "departments/get",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiDepartmentDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oDepartmentData = oData.data[i];

                                    var uiTemplate = uiDepartmentDropdown;
                                    cr8v.append_dropdown(oDepartmentData, uiTemplate, true, 'departments', 'data-company-id', oDepartmentData.company_id);
                                }
                            }
                        }
                    },
                    "complete": function () {

                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * get_ranks
         * @description This function is for getting and rendering ranks on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_ranks : function(oParams){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "ranks/get",
                    "beforeSend": function () {

                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiRankDropdown.find('div.option').remove();
                                for(var i = 0, max = oData.data.length ; i < max; i++)
                                {
                                    var oRankData = oData.data[i];

                                    var uiTemplate = uiRankDropdown;
                                    cr8v.append_dropdown(oRankData, uiTemplate, true, 'ranks', 'data-department-id', oRankData.department_id);
                                }
                            }
                        }
                    },
                    "complete": function () {

                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * bind_search_event
         * @description This function is for binding search event
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_search_event : function(){
            uiSearchUserBtn.on('click', function(){
                var sSearchField = uiUserSearchDropdown.find('.frm-custom-dropdown-txt').find('input').val();
                if(sSearchField == 'Username')
                {
                    if(uiSearchUsernameInput.val().length > 0)
                    {
                        roxas.users.assemble_search_params(uiSearchUserBtn, sSearchField, uiSearchUsernameInput.val());
                    }
                }
                else if(sSearchField == 'Name')
                {
                    if(uiSearchUserFullnameInput.val().length > 0)
                    {
                        roxas.users.assemble_search_params(uiSearchUserBtn, sSearchField, uiSearchUserFullnameInput.val());
                    }
                }
                else if(sSearchField == 'Company')
                {
                    roxas.users.assemble_search_params(uiSearchUserBtn, sSearchField);
                }
                else if(sSearchField == 'User Role')
                {
                    roxas.users.assemble_search_params(uiSearchUserBtn, sSearchField);
                }
            });

            uiSearchUsernameInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchUsernameInput.siblings('.search_user_btn').trigger('click');
                }
            });

            uiSearchUserFullnameInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchUserFullnameInput.siblings('.search_user_btn').trigger('click');
                }
            });

        },

        /**
         * assemble_search_params
         * @description This function is for assembling params for search
         * @dependencies 
         * @param {jQuery} uiButton
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_search_params : function(uiButton, sSearchField , sSearchValue, sSorting, sSortField){
            /*sample params*/
            /*{
                "keyword" : "string", 
                "search_field" : "string", 
                "sorting" : "string", 
                "sort_field" : "string", 
                "offset" : "int", 
                "limit" : "int"
            }*/

            var oGetUserParams = {
                'where' : []
            };

            if(sSearchField == 'Name')
            {
                oGetUserParams.search_field = 'name';
                oGetUserParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Username')
            {
                oGetUserParams.search_field = 'user_name';
                oGetUserParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Company')
            {
                if(cr8v.var_check(uiCompanyDropdown.attr('company_id')))
                {
                    oGetUserParams.where.push({
                        "field" : 'u.company_id',
                        "value" : uiCompanyDropdown.attr('company_id')
                    });
                }
                if(cr8v.var_check(uiUserCompanyUserRoleDropdown.attr('user_role')) && uiUserCompanyUserRoleDropdown.attr('user_role') > 0)
                {
                    oGetUserParams.where.push({
                        "field" : 'u.user_role',
                        "value" : uiUserCompanyUserRoleDropdown.attr('user_role')
                    });
                }
            }
            else if(sSearchField == 'User Role')
            {
                if(cr8v.var_check(uiUserRoleDropdown.attr('user_role')))
                {
                    oGetUserParams.where.push({
                        "field" : 'u.user_role',
                        "value" : uiUserRoleDropdown.attr('user_role')
                    });
                }
            }

            if(cr8v.var_check(sSorting))
            {
                oGetUserParams.sorting = sSorting;
            }

            if(cr8v.var_check(sSortField))
            {
                oGetUserParams.sort_field = sSortField;
            }

            roxas.users.get_users(oGetUserParams, uiButton);
        },

        /**
         * manipulate_template_list
         * @description This function will put the data of the user list view
         * @dependencies N/A
         * @param {object} oUserData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template_list : function(oUserData, uiTemplate){
            if(cr8v.var_check(oUserData) && cr8v.var_check(uiTemplate))
            {
                var sFirstName = ((cr8v.var_check(oUserData.first_name) && oUserData.first_name.length > 0) ? oUserData.first_name+' ' : '');
                var sMiddleName = ((cr8v.var_check(oUserData.middle_name) && oUserData.middle_name.length > 0) ? oUserData.middle_name+' ' : '');
                var sLastName = ((cr8v.var_check(oUserData.last_name) && oUserData.last_name.length > 0) ? oUserData.last_name+' ' : '');

                var sFullName = sFirstName+''+sMiddleName+''+sLastName;

                uiTemplate.attr('data-user-id', oUserData.id);
                uiTemplate.attr('data-user-full-name', sFullName);
                uiTemplate.attr('data-user-name', oUserData.user_name);
                uiTemplate.attr('data-user-employee-code', oUserData.employee_code);

                uiTemplate.find('[data-label="employee_code"]').text(oUserData.employee_code);
                uiTemplate.find('[data-label="full_name"]').text(sFullName);
                uiTemplate.find('[data-label="user_name"]').text(oUserData.user_name);
                uiTemplate.find('[data-label="user_role_name"]').text(oUserData.user_role_name);
                uiTemplate.find('[data-label="company_name"]').text(oUserData.company_name);
                uiTemplate.find('[data-label="department_name"]').text(oUserData.department_name);
                uiTemplate.find('[data-label="rank_name"]').text(oUserData.rank_name);
                uiTemplate.find('[data-label="email"]').text(oUserData.email);
                uiTemplate.find('[data-label="contact_number"]').text(oUserData.contact_number);

                if(cr8v.var_check(oUserData.img) && oUserData.img.length > 0)
                {
                    uiTemplate.find('[data-container="image_container"]').find('img').attr('src', roxas.config('url.server.base') +''+ oUserData.img).removeClass('hidden');
                }

                return uiTemplate;
            }
        },

        /**
         * manipulate_template_grid
         * @description This function will put the data of the user list view
         * @dependencies N/A
         * @param {object} oUserData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template_grid : function(oUserData, uiTemplate){
            if(cr8v.var_check(oUserData) && cr8v.var_check(uiTemplate))
            {
                var sFirstName = ((cr8v.var_check(oUserData.first_name) && oUserData.first_name.length > 0) ? oUserData.first_name+' ' : '');
                var sMiddleName = ((cr8v.var_check(oUserData.middle_name) && oUserData.middle_name.length > 0) ? oUserData.middle_name+' ' : '');
                var sLastName = ((cr8v.var_check(oUserData.last_name) && oUserData.last_name.length > 0) ? oUserData.last_name+' ' : '');

                var sFullName = sFirstName+''+sMiddleName+''+sLastName;

                uiTemplate.attr('data-user-id', oUserData.id);
                uiTemplate.attr('data-user-full-name', sFullName);
                uiTemplate.attr('data-user-name', oUserData.user_name);
                uiTemplate.attr('data-user-employee-code', oUserData.employee_code);

                uiTemplate.find('[data-label="employee_code"]').text(oUserData.employee_code);
                uiTemplate.find('[data-label="full_name"]').text(sFullName);
                uiTemplate.find('[data-label="user_name"]').text(oUserData.user_name);
                uiTemplate.find('[data-label="user_role_name"]').text(oUserData.user_role_name);
                uiTemplate.find('[data-label="company_name"]').text(oUserData.company_name);
                uiTemplate.find('[data-label="department_name"]').text(oUserData.department_name);
                uiTemplate.find('[data-label="rank_name"]').text(oUserData.rank_name);
                uiTemplate.find('[data-label="email"]').text(oUserData.email);
                uiTemplate.find('[data-label="contact_number"]').text(oUserData.contact_number);

                if(cr8v.var_check(oUserData.img) && oUserData.img.length > 0)
                {
                    uiTemplate.find('[data-container="image_container"]').find('img').attr('src', roxas.config('url.server.base') +''+ oUserData.img).removeClass('hidden');
                }

                return uiTemplate;
            }
        },

        /**
         * populate_edit_info
         * @description This function will put the data of the company needed on the edit info
         * @dependencies N/A
         * @param {object} oCompanyData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        populate_edit_info : function(oUserData, uiForm){
            if(cr8v.var_check(oUserData) && cr8v.var_check(uiForm))
            {
                uiForm.find('[name="first_name"]').val(oUserData.first_name);
                uiForm.find('[name="middle_name"]').val(oUserData.middle_name);
                uiForm.find('[name="last_name"]').val(oUserData.last_name);
                uiForm.find('[name="employee_code"]').val(oUserData.employee_code);
                uiForm.find('[name="user_name"]').val(oUserData.user_name);
                uiForm.find('[name="password"]').val('');
                uiForm.find('[name="confirm_password"]').val('');
                uiForm.find('[name="contact_number"]').val(oUserData.contact_number);
                uiForm.find('[name="email"]').val(oUserData.email);

                uiCompanyDropdown.find('[data-value="'+oUserData.company_id+'"]').trigger('click');
                uiDepartmentDropdown.find('[data-value="'+oUserData.department_id+'"]').trigger('click');
                uiRankDropdown.find('[data-value="'+oUserData.rank_id+'"]').trigger('click');

                var sContactType = roxas.users.validate_contact_type(true, undefined, oUserData.contact_number_type);
                uiContactDropdown.find('[data-value="'+sContactType+'"]').trigger('click');

                var sRoleName = roxas.users.validate_user_role(true, undefined, oUserData.user_role);
                uiRoleDropdown.find('[data-value="'+sRoleName+'"]').trigger('click');

                uiEmploymentStatusDropdown.find('[data-value="'+oUserData.employment_status+'"]').trigger('click');

                if(cr8v.var_check(oUserData.img) && oUserData.img.length > 0)
                {
                    uiForm.find('input[name="user_image"]').attr('image_path', oUserData.img);
                    uiForm.find('[data-container="image_container"]').find('img').attr('src', roxas.config('url.server.base') +''+ oUserData.img).removeClass('hidden');

                }
            }
        },

        /**
         * get_users
         * @description This function is for getting and rendering users
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_users : function(oParams, uiThis, bFromUserInfo){
            
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "users/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }

                        if(window.location.href.indexOf('users/user_list') > -1)
                        {
                            $("body").css({overflow:'hidden'});

                            $("#show-loading-data-shares").addClass("showed");
        
                            $("#show-loading-data-shares .close-me").on("click",function(){
                                $("body").css({'overflow-y':'initial'});
                                $("#show-loading-data-shares").removeClass("showed");
                            });
                             

                             $("#show-loading-data-shares").find(".modal-body").addClass("animated slideIn");
                        }
                    },
                    "success": function (oData) {
                        console.log(JSON.stringify(oData.data));
                        if(cr8v.var_check(oData))
                        {
                            uiUserGridContainer.find('.user_grid:not(.template)').remove();
                            uiUserListContainer.find('.user_list:not(.template)').remove();
                            uiNoResultsFound.addClass('template hidden');

                            if(oData.status == true)
                            {
                                if(cr8v.var_check(bFromUserInfo) && bFromUserInfo)
                                {
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oUserData = oData.data[i];

                                        var uiTemplate = $('[data-container="user_info_container"]');
                                        var uiManipulatedTemplate = roxas.users.manipulate_template_list(oUserData, uiTemplate);
                                        
                                        var uiTemplateForEdit = uiEditUserForm;
                                        var uiManipulatedTemplateForEdit = roxas.users.populate_edit_info(oUserData, uiTemplateForEdit);
                                     
                                        arrLogIDS.push(oUserData.id);
                                    }
                                }
                                else
                                {
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oUserData = oData.data[i];

                                        var uiTemplateList = $('.user_list.template').clone().removeClass('template hidden');
                                        var uiManipulatedTemplateList = roxas.users.manipulate_template_list(oUserData, uiTemplateList);

                                        var uiTemplateGrid = $('.user_grid.template').clone().removeClass('template hidden');
                                        var uiManipulatedTemplateGrid = roxas.users.manipulate_template_grid(oUserData, uiTemplateGrid);

                                        uiUserListContainer.prepend(uiManipulatedTemplateList);
                                        uiUserGridContainer.prepend(uiManipulatedTemplateGrid);
                                    }
                                }
                            }
                            else
                            {
                                uiNoResultsFound.removeClass('template hidden');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }

                        $(".modal-close.close-me").trigger("click");
                    },
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * get_personal_stocks
         * @description This function is for getting and rendering personal stocks on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_personal_stocks : function(oParams, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "esop/get_personal_stocks",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            uiPersonalStocksContainer.find('.personal_stocks:not(.template)').remove();
                            uiNoResultsFound.addClass('template hidden');

                            if(oData.status == true)
                            {
                                if(oData.data.length > 0)
                                {
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oPersonalStocks = oData.data[i];
                                        
                                        var uiTemplatePersonalStocks = $('.personal_stocks.template').clone().removeClass('template hidden');
                                        var uiManipulatedTemplatePersonalStocks = roxas.users.manipulate_personal_stocks(oPersonalStocks, uiTemplatePersonalStocks);

                                        roxas.users.compute_shares_offered_taken_untaken(oPersonalStocks, $('[data-container="total_shares_container"]'));
                                        
                                        uiPersonalStocksContainer.prepend(uiManipulatedTemplatePersonalStocks);
                                    }
                                }
                                else
                                {
                                    uiNoResultsFound.removeClass('template hidden');
                                }
                            }
                            else
                            {
                                uiNoResultsFound.removeClass('template hidden');
                            }

                            var oGetLogsParams = {
                                "where" : [
                                    {
                                        "field" : "ref_id",
                                        "operator" : "IN",
                                        "value" : "("+arrLogIDS.join()+")"
                                    },
                                    {
                                        "field" : "type",
                                        "operator" : "IN",
                                        "value" : "('user')"
                                    }
                                ]
                            };
                            cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    }
                };

                roxas.users.ajax(oAjaxConfig);
            }
        },

        /**
         * compute_shares_offered_taken_untaken
         * @description This function will compute the total shares offered, taken and untaken
         * @dependencies N/A
         * @param {object} oPersonalStocks
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        compute_shares_offered_taken_untaken : function(oPersonalStocks, uiTemplate){
            if(cr8v.var_check(oPersonalStocks) && cr8v.var_check(uiTemplate))
            {
                iTotalSharesOffered = parseFloat(iTotalSharesOffered) + parseFloat(oPersonalStocks.stock_offer_amount);
                uiTemplate.find('[data-label="total_shares_offered"]').text(number_format(iTotalSharesOffered, 2));

                iTotalSharesTaken = parseFloat(iTotalSharesTaken) + parseFloat(oPersonalStocks.accepted);
                uiTemplate.find('[data-label="total_shares_taken"]').text(number_format(iTotalSharesTaken, 2));

                iTotalSharesUntaken = parseFloat(iTotalSharesOffered) - parseFloat(iTotalSharesTaken);
                uiTemplate.find('[data-label="total_shares_untaken"]').text(number_format(iTotalSharesUntaken, 2));

                return uiTemplate;
            }
        },

        /**
         * numbers_only
         * @description: This function will allow numbers only in an input box 
         * @dependencies N/A
         * @response N/A
         * @criticality NORMAL
         * @software_architect N/A
         * @developer Nelson Estuesta Jr
         * @method_id N/A
         */
        numbers_only : function(uiInputBox){
             

             uiInputBox.off("keypress.enterNumbers").on("keypress.enterNumbers", function(e){
                     var uiThis = $(this),
                         iValue = uiThis.val().trim();

                     if(e.keyCode >= 48 && e.keyCode <= 57)
                     {
                             return true;
                     }
                     else{
                             return false;
                     }
                        
                        
             });    
        },

        /**
         * manipulate_personal_stocks
         * @description This function will put the data of the personal stocks
         * @dependencies N/A
         * @param {object} oPersonalStocks
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_personal_stocks : function(oPersonalStocks, uiTemplate){
            if(cr8v.var_check(oPersonalStocks) && cr8v.var_check(uiTemplate))
            {
                var sGrantDate = moment(oPersonalStocks.grant_date, 'YYYY-MM-DD').format('MMMM DD, YYYY');

                uiTemplate.attr('data-esop-id', oPersonalStocks.id);
                uiTemplate.attr('data-esop-name', oPersonalStocks.name);
                uiTemplate.attr('data-esop-grant-date', oPersonalStocks.grant_date);
                uiTemplate.attr('data-esop-total-share-qty', oPersonalStocks.total_share_qty);
                uiTemplate.attr('data-esop-vesting-year', oPersonalStocks.vesting_years);

                uiTemplate.find('[data-label="accepted"]').text(number_format(oPersonalStocks.accepted, 2) +' '+ oPersonalStocks.currency);
                uiTemplate.find('[data-label="esop_name"]').text(oPersonalStocks.name);
                uiTemplate.find('[data-label="grant_date"]').text(sGrantDate);
                uiTemplate.find('[data-label="total_share_qty"]').text(number_format(oPersonalStocks.total_share_qty, 2));
                uiTemplate.find('[data-label="vesting_years"]').text(oPersonalStocks.vesting_years);

                return uiTemplate;
            }
        },

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },
    }

}());

$(window).load(function () {
    roxas.users.initialize();
});
