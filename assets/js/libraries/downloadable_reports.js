/*
*
* Class : Downloadable Reports
*/

(function(){
	"use strict";

	/*variable declaration*/
    var 
        uiModalShareSummary,
        uiModalEsopSummary,
        uiModalSubscription,
        uiModalClaim,
        Class;

	/*end variable declaration*/


	/*
	*  - SAMPLE AJAX
	var oAjaxConfig = {
        "type": "POST",
        "data": {},
        "url": roxas.config('url.server.base') + "url_here",
        "beforeSend": function() {
        },
        "success": function(oData) {

        },
        "error": function() {
        }
    }

    roxas.downloadable_reports.ajax(oAjaxConfig);
    */

	CPlatform.prototype.downloadable_reports = {

		initialize : function(){

             /*initialize events*/

		},

		'ajax': function(oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        generate_share_summary : function(oParams, uiBtn)
        {
            if (cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type": "POST",
                    "data": oParams,
                    "url": roxas.config('url.server.base') + "reports/download_share_summary",
                    "beforeSend": function () {
                        if(cr8v.var_check(uiBtn)){
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        if(cr8v.var_check(oData))
                        {
                            
                        }
                    },
                    "complete": function () {
                        if(cr8v.var_check(uiBtn)){
                            cr8v.show_spinner(uiBtn, false);
                        }
                    }
                };

                roxas.downloadable_reports.ajax(oAjaxConfig);
            }
        },

        get_all_esop : function(callBack, oWhere)
        {
            var oGetEsopParams = {
                "limit" : 999999,
                "where" : [{
                    "field" : "e.parent_id",
                    "value" : 0
                }]
            };

            if(oWhere && typeof oWhere == 'object'){
                oGetEsopParams["where"] = [oWhere];
            }

            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oGetEsopParams,
                "url"    : roxas.config('url.server.base') + "esop/get",
                "beforeSend": function () { },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        callBack(oData.data);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        get_esop_search : function(sValue, callBack){


            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : {"s_value":sValue},
                "url"    : roxas.config('url.server.base') + "esop/get_esop_search",
                "beforeSend": function () { },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        callBack(oData);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        get_companies : function(oWhere, callBack){
            if(!oWhere){
                oWhere = { }
            }

            var oGetParams = {
                "limit" : 999999,
                "where" : [oWhere]
            };

            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oGetParams,
                "url"    : roxas.config('url.server.base') + "companies/get",
                "beforeSend": function () { },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        callBack(oData.data);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        get_departments : function(oWhere, callBack){
            if(!oWhere){
                oWhere = { }
            }

            var oGetDepartParams = {
                "limit" : 999999,
                "where" : [oWhere]
            };

            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oGetDepartParams,
                "url"    : roxas.config('url.server.base') + "departments/get",
                "beforeSend": function () { },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        callBack(oData.data);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        get_ranks : function(oWhere, callBack){
            if(!oWhere){
                oWhere = { }
            }

            var oGetDepartParams = {
                "limit" : 999999,
                "where" : [oWhere]
            };

            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oGetDepartParams,
                "url"    : roxas.config('url.server.base') + "ranks/get",
                "beforeSend": function () { },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        callBack(oData.data);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },


        //================== ESOP SUMMARY REPORT ===================//
        set_esop_summary_modal : function(action, oData){

            if(action == 'esop_name'){
                var selectEsopName = uiModalEsopSummary.find('.select-esop-name'),
                    customDropDown = selectEsopName.siblings('.frm-custom-dropdown'),
                    oSelectData = {};
                
                customDropDown.remove();
                selectEsopName.html('');
                selectEsopName.removeClass('frm-custom-dropdown-origin');
                for(var i in oData){
                    var option = '<option value="'+oData[i]['name']+'" data-id="'+oData[i]['id']+'"> '+oData[i]['name']+'</option>';
                    selectEsopName.append(option);
                }

                selectEsopName.transformDD();

                
            }

            if(action == 'esop_company'){
                var selectCompanyName = uiModalEsopSummary.find('.select-company-name'),
                    customDropDown = selectCompanyName.siblings('.frm-custom-dropdown'),
                    oSelectData = {};
                
                customDropDown.remove();
                selectCompanyName.html('');
                selectCompanyName.removeClass('frm-custom-dropdown-origin');
                for(var i in oData){
                    var option = '<option value="'+oData[i]['name']+'" data-id="'+oData[i]['id']+'"> '+oData[i]['name']+'</option>';
                    selectCompanyName.append(option);
                }

                selectCompanyName.transformDD();
            }

            if(action == 'esop_departments'){

                var departmentListContainer = uiModalEsopSummary.find('.department-list-container'),
                    uiDisplayNames = uiModalEsopSummary.find('.display-selected-department-names'),
                    listTemplate = departmentListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                departmentListContainer.find('.department-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.addClass('marked');
                    clone.attr({
                        'department-id' : data['department_id'],
                        'department-name' : data['department_name']
                    });
                    uiName.html(data['department_name']);
                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    departmentListContainer.append(clone);
                }

                // For Appending All
                 var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                     clone_all.addClass('marked');

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });     
                    departmentListContainer.prepend(clone_all);

                    setTimeout(function(){
                        var uiRemoveTheAll = departmentListContainer.find('.department-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".department-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);


                 // End For Appending All




                var displayName = function(){
                     var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                }

                departmentListContainer.off('click.selectdepartment').on('click.selectdepartment', '.department-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                        uiProfileName = uiThis.find(".profile_name");

                    if(bHasClass){
                        
                        if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = departmentListContainer.find('.department-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('department-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{
                                uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('department-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = departmentListContainer.find('.department-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".department-item:not(.template)").removeClass("marked");
                                        }                                      
                                })
                          }



                        
                    }else{

                          if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = departmentListContainer.find('.department-item:not(.template)');
                               arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        
                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('department-id')] = {
                                            "name" : uiAddOnce.attr('department-name'),
                                            "id" : uiAddOnce.attr('department-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('department-name'));

                                });

                          }else{
                                uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('department-id')] = {
                                    "name" : uiThis.attr('department-name'),
                                    "id" : uiThis.attr('department-id')
                                };
                                arrNames.push(uiThis.attr('department-name'));
                          }
                       
                    }

                    displayName();
                });

                departmentListContainer.find('.department-item:not(.template)').trigger('click.selectdepartment');
            }

            if(action == 'esop_ranks'){
                var rankListContainer = uiModalEsopSummary.find('.rank-list-container'),
                    uiDisplayNames = uiModalEsopSummary.find('.display-selected-rank-names'),
                    listTemplate = rankListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                rankListContainer.find('.rank-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.addClass('marked');
                    clone.attr({
                        'rank-id' : data['rank_id'],
                        'rank-name' : data['rank_name']
                    });
                    uiName.html(data['rank_name']);
                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    rankListContainer.append(clone);
                }

                // For Appending All
                 var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                     clone_all.addClass('marked');

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });     
                    rankListContainer.prepend(clone_all);

                     setTimeout(function(){
                        var uiRemoveTheAll = rankListContainer.find('.rank-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".rank-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);


                 // End For Appending All

                var displayName = function(){
                     var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                }

                rankListContainer.off('click.selectrank').on('click.selectrank', '.rank-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                        uiProfileName = uiThis.find(".profile_name");


                    if(bHasClass){
                         if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = rankListContainer.find('.rank-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('rank-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{
                                 uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('rank-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = rankListContainer.find('.rank-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".rank-item:not(.template)").removeClass("marked");
                                        }                                      
                                })

                          }

                        
                    }else{

                        if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = rankListContainer.find('.rank-item:not(.template)');
                                arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        

                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('rank-id')] = {
                                            "name" : uiAddOnce.attr('rank-name'),
                                            "id" : uiAddOnce.attr('rank-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('rank-name'));

                                });

                          }else{
                                 uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('rank-id')] = {
                                    "name" : uiThis.attr('rank-name'),
                                    "id" : uiThis.attr('rank-id')
                                };
                                arrNames.push(uiThis.attr('rank-name'));
                          }

                    }

                    displayName();
                });

                rankListContainer.find('.rank-item:not(.template)').trigger('click.selectrank');
            }
        },

        esop_summary_modal_esop_name_event : function(){
            var uiInputEsopName = uiModalEsopSummary.find('.input-esop-name'),
                iTimeout = {},
                searchAction = function(sValue){
                    Class.get_esop_search(sValue, function(oResult){
                        for(var i in oResult){
                            oResult[i]['name'] = oResult[i]['name'];
                            oResult[i]['id'] = oResult[i]['id']
                        }
                        console.log(JSON.stringify(oResult));
                        Class.set_esop_summary_modal('esop_name', oResult);
                    });
                };

            uiInputEsopName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var sValue = {};
                    if(val.trim().length > 0){
//                         oWhere['field'] = "name";
//                         oWhere['operator'] = "LIKE";
//                         oWhere['value'] = '%'+val+'%';
                        sValue = val;
                    }

                    searchAction(sValue);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        esop_summary_modal_company_event : function(){
            var uiInputCompanyName = uiModalEsopSummary.find('.input-company-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_companies(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['company_name'] = oResult[i]['name'];
                            oResult[i]['company_id'] = oResult[i]['id']
                        }
                        Class.set_esop_summary_modal('esop_company', oResult);
                    });
                };

            uiInputCompanyName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "c.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        esop_summary_modal_department_event : function(){
            var uiInputDeptName = uiModalEsopSummary.find('.input-department-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_departments(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['department_name'] = oResult[i]['name'];
                            oResult[i]['department_id'] = oResult[i]['id']
                        }
                        Class.set_esop_summary_modal('esop_departments', oResult);
                    });
                };

            uiInputDeptName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "d.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },


        esop_summary_modal_ranks_event : function(){
            var uiInputRankName = uiModalEsopSummary.find('.input-rank-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_ranks(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['rank_name'] = oResult[i]['name'];
                            oResult[i]['rank_id'] = oResult[i]['id']
                        }
                        Class.set_esop_summary_modal('esop_ranks', oResult);
                    });
                };

            uiInputRankName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "r.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        esop_summary_modal_submit_event : function(){
            var btn = uiModalEsopSummary.find('.btn-download-esop-summary'),
                errorContainer = uiModalEsopSummary.find('.add_esop_error_message');

            btn.off('click').on('click', function(){
                var uiEsopID = uiModalEsopSummary.find('.select-esop-name option:selected'),
                    companyID = uiModalEsopSummary.find('.select-company-name option:selected'),
                    departmentList = uiModalEsopSummary.find('.department-item.marked'),
                    rankList = uiModalEsopSummary.find('.rank-item.marked'),
                    error = 0,
                    errMessage = [],
                   oReportParam = {
                    'esop_id' : uiEsopID.attr('data-id'),
                    'company_id' : companyID.attr('data-id'),
                    'department' : {},
                    'rank' : {}
                  };


                if(departmentList.length > 0){
                   $.each(departmentList, function(){
                           var sAttr = $(this).attr('department-id');
                           
                           if (typeof sAttr !== typeof undefined && sAttr !== false) {
                          
                              oReportParam['department'][Object.keys(oReportParam['department']).length] = { "department_id" : $(this).attr('department-id') }
                           }
                        
                    });
                }

                if(rankList.length > 0){
                    $.each(rankList, function(){
                            var sAttr = $(this).attr('rank-id');
                           
                           if (typeof sAttr !== typeof undefined && sAttr !== false) {
                          
                               oReportParam['rank'][Object.keys(oReportParam['rank']).length] = { "rank_id" : $(this).attr('rank-id') }
                           }
                       
                    });
                }
                
//                 oReportParam['esop'] = JSON.stringify(oReportParam['esop']);
//                 oReportParam['company'] = JSON.stringify(oReportParam['company']);
                oReportParam['department'] = JSON.stringify(oReportParam['department']);
                oReportParam['rank'] = JSON.stringify(oReportParam['rank']);

                roxas.downloadable_reports.generate_esop_summary(oReportParam, function(oResponse, status){
                        if(status){
                         window.location = oResponse.file_url;
                        }
                    }); 



                

            });
        },

        generate_esop_summary : function(oParam, callBack){

            if(!oParam || Object.keys(oParam).length == 0){
                return false;
            }

            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : oParam,
                "url"    : roxas.config('url.server.base') + "reports/extract_esop_summary",
//                 "url"    : roxas.config('url.server.base') + "reports/generate_esop_summary",
                "beforeSend": function () {
                    $('.btn-download-esop-summary').html('Downloading...');
                },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        $('.btn-download-esop-summary').html('Download Report');
                        callBack(oData.data, oData.status);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        initialize_esop_summary : function(){
            Class = roxas.downloadable_reports;

            uiModalEsopSummary = $('[modal-id="esop-summary"]');

            Class.get_all_esop(function(esopList){
                Class.set_esop_summary_modal('esop_name', esopList);
                Class.set_subscription_modal('esop_name', esopList);
                Class.set_claim_summary_modal('esop_name', esopList);
            });

             Class.esop_summary_modal_esop_name_event();

            Class.esop_summary_modal_company_event();

            Class.get_departments({}, function(oDepartments){
                for(var i in oDepartments){
                    oDepartments[i]['department_name'] = oDepartments[i]['name'];
                    oDepartments[i]['department_id'] = oDepartments[i]['id'];
                }
                Class.set_esop_summary_modal('esop_departments', oDepartments);
                Class.esop_summary_modal_department_event();
                Class.set_claim_summary_modal('esop_departments', oDepartments);
            });

            Class.get_ranks({}, function(oRanks){
                for(var i in oRanks){
                    oRanks[i]['rank_name'] = oRanks[i]['name'];
                    oRanks[i]['rank_id'] = oRanks[i]['id'];
                }
                Class.set_esop_summary_modal('esop_ranks', oRanks);
                Class.esop_summary_modal_ranks_event();
                Class.set_claim_summary_modal('esop_ranks', oRanks);
            });

            /*Class.get_companies({}, function(oResult){
                for(var i in oResult){
                    oResult[i]['company_name'] = oResult[i]['name'];
                    oResult[i]['company_id'] = oResult[i]['id'];
                }
                Class.set_esop_summary_modal('esop_company', oResult);
                Class.esop_summary_modal_company_event();
                Class.set_esop_summary_modal('esop_company', oResult);
            });*/

            var oWhere = {};
            oWhere['field'] = "c.name";
            oWhere['operator'] = "LIKE";
            oWhere['value'] = '%%';
            Class.get_companies(oWhere, function(oResult){
                for(var i in oResult){
                    oResult[i]['company_name'] = oResult[i]['name'];
                    oResult[i]['company_id'] = oResult[i]['id']
                }
                Class.set_esop_summary_modal('esop_company', oResult);
            });

            Class.esop_summary_modal_submit_event();
        },

        //================== ESOP SUBSCRIPTION REPORT ===================//
        

        subscription_modal_esopname_event : function(){
            var uiInputEsopName = uiModalSubscription.find('.esop-search-input'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_all_esop(function(oResult){
                        for(var i in oResult){
                            oResult[i]['name'] = oResult[i]['name'];
                            oResult[i]['id'] = oResult[i]['id']
                        }
                        Class.set_subscription_modal('esop_name', oResult);
                    }, oWhere);
                };

            uiInputEsopName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "e.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        subscription_modal_department_event : function(){
            var uiInputCompanyName = uiModalSubscription.find('.input-company-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_companies(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['company_name'] = oResult[i]['name'];
                            oResult[i]['company_id'] = oResult[i]['id']
                        }
                        Class.set_subscription_modal('esop_company', oResult);
                    });
                };

            uiInputCompanyName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "c.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        set_subscription_modal : function(action, oData){
            if(action == 'esop_name'){
                var subscriptionListContainer = uiModalSubscription.find('.subscription-esop-list-container'),
                    uiDisplayNames = uiModalSubscription.find('.display-selected-esop-names'),
                    listTemplate = subscriptionListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                subscriptionListContainer.find('.esop-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.attr({
                        'esop-id' : data['id'],
                        'esop-name' : data['name']
                    });
                    uiName.html(data['name']);
                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    subscriptionListContainer.append(clone);
                }

                // For Appending All
                 var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                   

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });   
                    
                    clone_all.addClass('marked');  
                    subscriptionListContainer.prepend(clone_all);


                    setTimeout(function(){
                        var uiRemoveTheAll = subscriptionListContainer.find('.esop-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".esop-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);
                    
                    


                 // End For Appending All

                var displayName = function(){
                    var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                };

                subscriptionListContainer.off('click.selectesop').on('click.selectesop', '.esop-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                    uiProfileName = uiThis.find(".profile_name");
                    if(bHasClass){

                        if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = subscriptionListContainer.find('.esop-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('esop-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{ 
                                uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('esop-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = subscriptionListContainer.find('.esop-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".esop-item:not(.template)").removeClass("marked");
                                        }                                      
                                })
                          }
                    }else{
                         if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = subscriptionListContainer.find('.esop-item:not(.template)');
                                arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        
                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('esop-id')] = {
                                            "name" : uiAddOnce.attr('esop-name'),
                                            "id" : uiAddOnce.attr('esop-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('esop-name'));

                                });

                          }else{
                                uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('esop-id')] = {
                                    "name" : uiThis.attr('esop-name'),
                                    "id" : uiThis.attr('esop-id')
                                };
                                arrNames.push(uiThis.attr('esop-name'));
                          }
                    }

                    displayName();
                });

                subscriptionListContainer.find('.esop-item:not(.template)').trigger('click.selectesop');
            }
            
            if(action == 'esop_company'){
                var companyListContainer = uiModalSubscription.find('.company-list-container'),
                    uiDisplayNames = uiModalSubscription.find('.display-selected-company-names'),
                    listTemplate = companyListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                companyListContainer.find('.company-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.attr({
                        'company-id' : data['company_id'],
                        'company-name' : data['company_name']
                    });
                    uiName.html(data['company_name']);
                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    companyListContainer.append(clone);
                }

                 // For Appending All
                 var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                   

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });   
                    
                    clone_all.addClass('marked');  
                     companyListContainer.prepend(clone_all);


                    setTimeout(function(){
                        var uiRemoveTheAll = companyListContainer.find('.company-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".company-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);
                    
                    


                 // End For Appending All

                var displayName = function(){
                    var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                };

                companyListContainer.off('click.selectcompany').on('click.selectcompany', '.company-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                    uiProfileName = uiThis.find(".profile_name");

                    if(bHasClass){

                            if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = companyListContainer.find('.company-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('company-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{ 
                                uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('company-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = companyListContainer.find('.company-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".company-item:not(.template)").removeClass("marked");
                                        }                                      
                                })
                          }
                    }else{
                        if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = companyListContainer.find('.company-item:not(.template)');
                                arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        
                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('company-id')] = {
                                            "name" : uiAddOnce.attr('company-name'),
                                            "id" : uiAddOnce.attr('company-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('company-name'));

                                });

                          }else{
                                uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('company-id')] = {
                                    "name" : uiThis.attr('company-name'),
                                    "id" : uiThis.attr('company-id')
                                };
                                arrNames.push(uiThis.attr('company-name'));
                          }
                    }

                    displayName();
                });

                companyListContainer.find('.company-item:not(.template)').trigger('click.selectcompany');
            }
        },

        initialize_subscription_summary : function(){
            Class = roxas.downloadable_reports;

            uiModalSubscription = $('[modal-id="subscription-summary"]');

            Class.subscription_modal_esopname_event();

            Class.subscription_modal_department_event();

            Class.subscription_modal_submit_event();

             var oWhere = {};
            oWhere['field'] = "c.name";
            oWhere['operator'] = "LIKE";
            oWhere['value'] = '%%';
            Class.get_companies(oWhere, function(oResult){
                for(var i in oResult){
                    oResult[i]['company_name'] = oResult[i]['name'];
                    oResult[i]['company_id'] = oResult[i]['id']
                }
                Class.set_subscription_modal('esop_company', oResult);
            });
        },

        subscription_modal_submit_event : function(){
            var btn = uiModalSubscription.find('.btn-generate-subscription'),
                errorContainer = uiModalSubscription.find('.sucscription_error_message');

            btn.off('click').on('click', function(){
                var uiEsops = uiModalSubscription.find('.subscription-esop-list-container'),
                companyList = uiModalSubscription.find('.company-item.marked'),
                esopList = uiModalSubscription.find('.esop-item.marked'),
                error = 0,
                errMessage = [],
                oReportParam = {
                    'esop' :  {},
                    'company' : {}
                };

                        if(esopList.length > 0){ 
                       $.each(esopList, function(){ 
                               var sAttr = $(this).attr('esop-id'); 
     
                               if (typeof sAttr !== typeof undefined && sAttr !== false) { 
                               
                                  oReportParam['esop'][Object.keys(oReportParam['esop']).length] = { "esop_id" : $(this).attr('esop-id') } 
                               } 
                             
                        }); 
                        } 
     
                        if(companyList.length > 0){ 
                           $.each(companyList, function(){ 
                               var sAttr = $(this).attr('company-id'); 
                                
                               if (typeof sAttr !== typeof undefined && sAttr !== false) { 
                               
                                      oReportParam['company'][Object.keys(oReportParam['company']).length] = { "company_id" : $(this).attr('company-id') } 
                               } 
                            
                            }); 
                        } 

                oReportParam['company'] = JSON.stringify(oReportParam['company']);

                oReportParam['esop'] = oReportParam['esop'];
             
                roxas.downloadable_reports.generate_subscription_summary(oReportParam, function(oResponse, status){ 
                                if(status){ 
                                 window.location = oResponse.file_url; 
                                } 
                            });  
              

            });
        },

        generate_subscription_summary : function(oParam, callBack){

            if(!oParam || Object.keys(oParam).length == 0){
                return false;
            }

            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : oParam,
                "url"    : roxas.config('url.server.base') + "reports/generate_subscription_summary",
                "beforeSend": function () {
                    $('.btn-generate-subscription').html('Downloading...');
                },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        $('.btn-generate-subscription').html('Download Report');
                        callBack(oData.data, oData.status);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        //================== CLAIM SUMMARY REPORT ===================//

        set_claim_summary_modal : function(action, oData){

            if(action == 'esop_name'){

                var selectEsopName = uiModalClaim.find('.select-esop-name'),
                    customDropDown = selectEsopName.siblings('.frm-custom-dropdown'),
                    oSelectData = {};
                
                customDropDown.remove();
                selectEsopName.html('');
                selectEsopName.removeClass('frm-custom-dropdown-origin');
                for(var i in oData){
                    var option = '<option value="'+oData[i]['name']+'" data-id="'+oData[i]['id']+'"> '+oData[i]['name']+'</option>';
                    selectEsopName.append(option);
                }

                selectEsopName.transformDD();

               
            }

            if(action == 'esop_company'){
                var selectEsopName = uiModalClaim.find('.select-company-name'),
                    customDropDown = selectEsopName.siblings('.frm-custom-dropdown'),
                    oSelectData = {};
                
                customDropDown.remove();
                selectEsopName.html('');
                selectEsopName.removeClass('frm-custom-dropdown-origin');
                for(var i in oData){
                    var option = '<option value="'+oData[i]['name']+'" data-id="'+oData[i]['id']+'"> '+oData[i]['name']+'</option>';
                    selectEsopName.append(option);
                }

                selectEsopName.transformDD();
            }

            if(action == 'esop_departments'){

                var departmentListContainer = uiModalClaim.find('.department-list-container'),
                    uiDisplayNames = uiModalClaim.find('.display-selected-department-names'),
                    listTemplate = departmentListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                departmentListContainer.find('.department-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.attr({
                        'department-id' : data['department_id'],
                        'department-name' : data['department_name']
                    });
                    uiName.html(data['department_name']);
                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    departmentListContainer.append(clone);
                }

                // For Appending All
                 var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                     clone_all.addClass('marked');

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });     
                    departmentListContainer.prepend(clone_all);

                     setTimeout(function(){
                        var uiRemoveTheAll = departmentListContainer.find('.department-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".department-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);


                 // End For Appending All

                var displayName = function(){
                     var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                }

                departmentListContainer.off('click.selectdepartment').on('click.selectdepartment', '.department-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                        uiProfileName = uiThis.find(".profile_name");

                    if(bHasClass){
                        
                        if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = departmentListContainer.find('.department-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('department-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{
                                uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('department-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = departmentListContainer.find('.department-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".department-item:not(.template)").removeClass("marked");
                                        }                                      
                                })
                          }

                        
                    }else{
                           
                          if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = departmentListContainer.find('.department-item:not(.template)');
                                arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        

                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('department-id')] = {
                                            "name" : uiAddOnce.attr('department-name'),
                                            "id" : uiAddOnce.attr('department-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('department-name'));

                                });

                          }else{
                                uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('department-id')] = {
                                    "name" : uiThis.attr('department-name'),
                                    "id" : uiThis.attr('department-id')
                                };
                                arrNames.push(uiThis.attr('department-name'));
                          }

                       
                    }

                    displayName();
                });

                departmentListContainer.find('.department-item:not(.template)').trigger('click.selectdepartment');
            }

            if(action == 'esop_ranks'){
                var rankListContainer = uiModalClaim.find('.rank-list-container'),
                    uiDisplayNames = uiModalClaim.find('.display-selected-rank-names'),
                    listTemplate = rankListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                rankListContainer.find('.rank-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.attr({
                        'rank-id' : data['rank_id'],
                        'rank-name' : data['rank_name']
                    });
                    uiName.html(data['rank_name']);

                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    rankListContainer.append(clone);

                   
                }
                

                // For Appending All
                 var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                     clone_all.addClass('marked');

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });     
                    rankListContainer.prepend(clone_all);

                     setTimeout(function(){
                        var uiRemoveTheAll = rankListContainer.find('.rank-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".rank-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);


                 // End For Appending All
                 

                var displayName = function(){
                    var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                }

                rankListContainer.off('click.selectrank').on('click.selectrank', '.rank-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                        uiProfileName = uiThis.find(".profile_name");

                    if(bHasClass){

                          if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = rankListContainer.find('.rank-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('rank-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{
                                uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('rank-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = rankListContainer.find('.rank-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".rank-item:not(.template)").removeClass("marked");
                                        }                                      
                                })
                          }
                       
                    }else{

                          if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = rankListContainer.find('.rank-item:not(.template)');
                                arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        
                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('rank-id')] = {
                                            "name" : uiAddOnce.attr('rank-name'),
                                            "id" : uiAddOnce.attr('rank-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('rank-name'));

                                });

                          }else{
                                uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('rank-id')] = {
                                    "name" : uiThis.attr('rank-name'),
                                    "id" : uiThis.attr('rank-id')
                                };
                                arrNames.push(uiThis.attr('rank-name'));
                          }
                        
                    }

                    displayName();
                });

                rankListContainer.find('.rank-item:not(.template)').trigger('click.selectrank');
            }
        },

        claim_summary_modal_esop_name_event : function(){
            var uiInputEsopName = uiModalClaim.find('.input-esop-name'),
                iTimeout = {},
                searchAction = function(sValue){
                    Class.get_esop_search(sValue, function(oResult){
                        for(var i in oResult){
                            oResult[i]['name'] = oResult[i]['name'];
                            oResult[i]['id'] = oResult[i]['id']
                        }
                        console.log(JSON.stringify(oResult));
                        Class.set_claim_summary_modal('esop_name', oResult);
                    });
                };

            uiInputEsopName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var sValue = {};
                    if(val.trim().length > 0){

                        sValue = val;
                    }

                    searchAction(sValue);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        claim_summary_modal_company_event : function(){
            var uiInputCompanyName = uiModalClaim.find('.input-company-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_companies(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['company_name'] = oResult[i]['name'];
                            oResult[i]['company_id'] = oResult[i]['id']
                        }
                        Class.set_claim_summary_modal('esop_company', oResult);
                    });
                };

            uiInputCompanyName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "c.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        claim_summary_modal_department_event : function(){
            var uiInputDeptName = uiModalClaim.find('.input-department-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_departments(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['department_name'] = oResult[i]['name'];
                            oResult[i]['department_id'] = oResult[i]['id']
                        }
                        Class.set_claim_summary_modal('esop_departments', oResult);
                    });
                };

            uiInputDeptName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "d.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },


        claim_summary_modal_ranks_event : function(){
            var uiInputRankName = uiModalClaim.find('.input-rank-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_ranks(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['rank_name'] = oResult[i]['name'];
                            oResult[i]['rank_id'] = oResult[i]['id']
                        }
                        Class.set_claim_summary_modal('esop_ranks', oResult);
                    });
                };

            uiInputRankName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "r.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        claim_summary_modal_submit_event : function(){
            var btn = uiModalClaim.find('.btn-download-esop-summary'),
                errorContainer = uiModalClaim.find('.add_esop_error_message');

            btn.off('click').on('click', function(){
                var uiEsopID = uiModalClaim.find('.select-esop-name option:selected'),
                companyList = uiModalClaim.find('.select-company-name option:selected'),
                departmentList = uiModalClaim.find('.department-item.marked'),
                rankList = uiModalClaim.find('.rank-item.marked'),
                error = 0,
                errMessage = [],
                oReportParam = {
                    'esop_id' : uiEsopID.attr('data-id'),
                    'company_id' : companyList.attr('data-id'),
                    'department' : {},
                    'rank' : {}
                };

//                 if(companyList.length > 0){
//                    $.each(companyList, function(){
//                         oReportParam['company'][Object.keys(oReportParam['company']).length] = { "company_id" : $(this).attr('company-id') }
//                     });
//                 }

                if(departmentList.length > 0){
                   $.each(departmentList, function(){
                        oReportParam['department'][Object.keys(oReportParam['department']).length] = { "department_id" : $(this).attr('department-id') }
                    });
                }

                if(rankList.length > 0){
                    $.each(rankList, function(){
                        oReportParam['rank'][Object.keys(oReportParam['rank']).length] = { "rank_id" : $(this).attr('rank-id') }
                    });
                }

//                 oReportParam['company'] = JSON.stringify(oReportParam['company']);
                oReportParam['department'] = JSON.stringify(oReportParam['department']);
                oReportParam['rank'] = JSON.stringify(oReportParam['rank']);
                roxas.downloadable_reports.generate_claim_summary(oReportParam, function(oResponse, status){
                    if(status){
                        window.location = oResponse.file_url;
                    }
                });

            });
        },

        generate_claim_summary : function(oParam, callBack){

            if(!oParam || Object.keys(oParam).length == 0){
                return false;
            }

            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : oParam,
                "url"    : roxas.config('url.server.base') + "reports/generate_claim_summary",
                "beforeSend": function () {
                    $('.btn-download-esop-summary').html('Downloading...');
                },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        $('.btn-download-esop-summary').html('Download Report');
                        callBack(oData.data, oData.status);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        initialize_claim_summary : function(){
            Class = roxas.downloadable_reports;

            uiModalClaim = $('[modal-id="claim-summary"]');

            Class.claim_summary_modal_esop_name_event();

            Class.claim_summary_modal_company_event();

            Class.claim_summary_modal_ranks_event();

            Class.claim_summary_modal_submit_event();

            var oWhere = {};
            oWhere['field'] = "c.name";
            oWhere['operator'] = "LIKE";
            oWhere['value'] = '%%';
            Class.get_companies(oWhere, function(oResult){
                for(var i in oResult){
                    oResult[i]['company_name'] = oResult[i]['name'];
                    oResult[i]['company_id'] = oResult[i]['id']
                }
                Class.set_claim_summary_modal('esop_company', oResult);
            });
        },

        //================== SHARE SUMMARY REPORT ===================//


        sharesummary_modal_esopname_event : function(){
            var uiInputEsopName = uiModalShareSummary.find('.esop-search-input'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_all_esop(function(oResult){
                        for(var i in oResult){
                            oResult[i]['name'] = oResult[i]['name'];
                            oResult[i]['id'] = oResult[i]['id']
                        }
                        Class.set_share_summary_modal('esop_name', oResult);
                    }, oWhere);
                };

            uiInputEsopName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "e.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },
        set_share_summary_modal : function(action, oData){

            if(action == 'esop_name'){
                var selectEsopName = uiModalShareSummary.find('.select-esop-name'),
                    customDropDown = selectEsopName.siblings('.frm-custom-dropdown'),
                    oSelectData = {};
                
                customDropDown.remove();
                selectEsopName.html('');
                selectEsopName.removeClass('frm-custom-dropdown-origin');
                for(var i in oData){
                    var option = '<option value="'+oData[i]['name']+'" data-id="'+oData[i]['id']+'"> '+oData[i]['name']+'</option>';
                    selectEsopName.append(option);
                }

                selectEsopName.transformDD();
            }
                /*var sharesummaryListContainer = uiModalShareSummary.find('.sharesummary-esop-list-container'),
                    uiDisplayNames = uiModalShareSummary.find('.display-selected-esop-names'),
                    listTemplate = sharesummaryListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                sharesummaryListContainer.find('.esop-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.attr({
                        'esop-id' : data['id'],
                        'esop-name' : data['name']
                    });
                    uiName.html(data['name']);
                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    sharesummaryListContainer.append(clone);*/
                
                
                // For Appending All
                /* var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                   

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });   
                    
                    clone_all.addClass('marked');  
                    sharesummaryListContainer.prepend(clone_all);


                    setTimeout(function(){
                        var uiRemoveTheAll = sharesummaryListContainer.find('.esop-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".esop-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);*/
                    
                    


                 // End For Appending All

                /*var displayName = function(){
                    var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                };

                sharesummaryListContainer.off('click.selectesop').on('click.selectesop', '.esop-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                        uiProfileName = uiThis.find(".profile_name");
                    if(bHasClass){
                        uiThis.removeClass('marked');
                        delete oSelectedValues[uiThis.attr('esop-id')];

                        arrNames = [];
                        for(var n in oSelectedValues){
                            arrNames.push(oSelectedValues[n]['name']);
                        }

                        if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = sharesummaryListContainer.find('.esop-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('esop-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{ 
                                uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('esop-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = sharesummaryListContainer.find('.esop-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".esop-item:not(.template)").removeClass("marked");
                                        }                                      
                                })
                          }
                    }else{
                        uiThis.addClass('marked');
                        oSelectedValues[uiThis.attr('company-id')] = {
                            "name" : uiThis.attr('esop-name'),
                            "id" : uiThis.attr('esop-id')
                        };
                        arrNames.push(uiThis.attr('esop-name'));

                        if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = sharesummaryListContainer.find('.esop-item:not(.template)');
                                arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        
                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('esop-id')] = {
                                            "name" : uiAddOnce.attr('esop-name'),
                                            "id" : uiAddOnce.attr('esop-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('esop-name'));

                                });

                          }else{
                                uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('esop-id')] = {
                                    "name" : uiThis.attr('esop-name'),
                                    "id" : uiThis.attr('esop-id')
                                };
                                arrNames.push(uiThis.attr('esop-name'));
                          }

                    }

                    displayName();
                });

                sharesummaryListContainer.find('.esop-item:not(.template)').trigger('click.selectesop');*/
            

            if(action == 'esop_company'){
                var companyListContainer = uiModalShareSummary.find('.company-list-container'),
                    uiDisplayNames = uiModalShareSummary.find('.display-selected-company-names'),
                    listTemplate = companyListContainer.find('.template'),
                    oSelectedValues = {},
                    arrNames = [];

                companyListContainer.find('.company-item:not(.template)').remove();
                for(var x in oData){
                    var data = oData[x],
                        clone = listTemplate.clone(),
                        uiName = clone.find('.name');
                    clone.removeClass('template');
                    clone.attr({
                        'company-id' : data['company_id'],
                        'company-name' : data['company_name']
                    });
                    uiName.html(data['company_name']);
                    uiName.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                    companyListContainer.append(clone);
                }

                // For Appending All
                 var clone_all = listTemplate.clone(),  
                     uiName_all = clone_all.find('.name');
                     
                    clone_all.removeClass('template');
                   

                    uiName_all.html("All");

                    uiName_all.show().css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });   
                    
                    clone_all.addClass('marked');  
                     companyListContainer.prepend(clone_all);


                    setTimeout(function(){
                        var uiRemoveTheAll = companyListContainer.find('.company-item:not(.template)').find(".profile_name");
                        $.each(uiRemoveTheAll, function(){
                             var uiAddMarked = $(this);

                                if(uiAddMarked.text().trim() == "All")
                                {
                                    uiAddMarked.closest(".company-item:not(.template)").addClass("marked");
                                }                                      
                        })
                        
                    },300);
                    
                    


                 // End For Appending All

                var displayName = function(){
                    var oNewArrNames = $.grep(arrNames,function(n){return(n);});
                        
                    uiDisplayNames.html(oNewArrNames.join(','));
                    uiDisplayNames.css({
                        'text-transform' : 'capitalize',
                        'width' : '263px',
                        'text-overflow' : 'ellipsis',
                        'overflow' : 'hidden',
                        'white-space' : 'nowrap'
                    });
                }

                companyListContainer.off('click.selectcompany').on('click.selectcompany', '.company-item', function(){
                    var uiThis = $(this),
                        bHasClass = uiThis.hasClass('marked'),
                        uiProfileName = uiThis.find(".profile_name");

                    if(bHasClass){

                            if(uiProfileName.text().trim() == "All")
                          {
                                var uiRemoveAll = companyListContainer.find('.company-item.marked:not(.template)');

                                $.each(uiRemoveAll, function(){
                                        var uiRemoveOnce = $(this);
                                        
                                        uiRemoveOnce.removeClass('marked');
                                        delete oSelectedValues[uiRemoveOnce.attr('company-id')];

                                        arrNames = [];
                                        for(var n in oSelectedValues){
                                            arrNames.push(oSelectedValues[n]['name']);
                                        }

                                });

                          }else{ 
                                uiThis.removeClass('marked');
                                delete oSelectedValues[uiThis.attr('company-id')];

                                arrNames = [];
                                for(var n in oSelectedValues){
                                    arrNames.push(oSelectedValues[n]['name']);
                                }

                                var uiRemoveTheAll = companyListContainer.find('.company-item.marked:not(.template)').find(".profile_name");
                                $.each(uiRemoveTheAll, function(){
                                     var uiThisAllRemove = $(this);

                                        if(uiThisAllRemove.text().trim() == "All")
                                        {
                                            uiThisAllRemove.closest(".company-item:not(.template)").removeClass("marked");
                                        }                                      
                                })
                          }


                    }else{

                            if(uiProfileName.text().trim() == "All")
                          {
                                var uiAddeAll = companyListContainer.find('.company-item:not(.template)');
                                arrNames = [];
                                $.each(uiAddeAll, function(){
                                        var uiAddOnce = $(this);
                                        
                                        uiAddOnce.addClass('marked');
                                        oSelectedValues[uiAddOnce.attr('company-id')] = {
                                            "name" : uiAddOnce.attr('company-name'),
                                            "id" : uiAddOnce.attr('company-id')
                                        };
                                        arrNames.push(uiAddOnce.attr('company-name'));

                                });

                          }else{
                                uiThis.addClass('marked');
                                oSelectedValues[uiThis.attr('company-id')] = {
                                    "name" : uiThis.attr('company-name'),
                                    "id" : uiThis.attr('company-id')
                                };
                                arrNames.push(uiThis.attr('company-name'));
                          }
                    }

                    displayName();
                });

                $('.company-item:not(.template)').trigger('click.selectcompany');
            }

        },

        share_summary_modal_company_event : function(){
            var uiInputCompanyName = uiModalShareSummary.find('.input-company-name'),
                iTimeout = {},
                searchAction = function(oWhere){
                    Class.get_companies(oWhere, function(oResult){
                        for(var i in oResult){
                            oResult[i]['company_name'] = oResult[i]['name'];
                            oResult[i]['company_id'] = oResult[i]['id']
                        }
                        Class.set_subscription_modal('esop_company', oResult);
                    });
                };

            uiInputCompanyName.on('keyup', function(e){
                var val = $(this).val(),
                    keycode = e.keyCode;
                clearTimeout(iTimeout);
                if(keycode == 13){
                    clearTimeout(iTimeout);
                }
                iTimeout = setTimeout(function(){
                    var oWhere = {};
                    if(val.trim().length > 0){
                        oWhere['field'] = "c.name";
                        oWhere['operator'] = "LIKE";
                        oWhere['value'] = '%'+val+'%';
                    }

                    searchAction(oWhere);
                    clearTimeout(iTimeout);
                }, 500);
            });
        },

        share_summary_modal_submit_event : function(){
            var btn = uiModalShareSummary.find('.btn-download-share-summary'),
                errorContainer = uiModalShareSummary.find('.add_esop_error_message');


            btn.off('click').on('click', function(){
                var uiEsopID = uiModalShareSummary.find('.select-esop-name option:selected'),
                companyList = uiModalShareSummary.find('.company-item.marked'),
                error = 0,
                errMessage = [],
                oReportParam = {
                    'esop_id' : uiEsopID.attr('data-id'),
                    'company_id' : {}
                };

                /*if(uiEsopID.length > 0){ 
                       $.each(uiEsopID, function(){ 
                               var sAttr = $(this).attr('esop-id'); 
     
                               if (typeof sAttr !== typeof undefined && sAttr !== false) { 
                               
                                  oReportParam['esop_id'][Object.keys(oReportParam['esop_id']).length] = { "esop_id" : $(this).attr('esop-id') } 
                               } 
                             
                        }); 
                        } */

                if(companyList.length > 0){ 
                       $.each(companyList, function(){ 
                               var sAttr = $(this).attr('company-id'); 
     
                               if (typeof sAttr !== typeof undefined && sAttr !== false) { 
                               
                                  oReportParam['company_id'][Object.keys(oReportParam['company_id']).length] = { "company_id" : $(this).attr('company-id') } 
                               } 
                             
                        }); 
                        } 

                oReportParam['company_id'] = JSON.stringify(oReportParam['company_id']);
                /*oReportParam['esop_id'] = JSON.stringify(oReportParam['esop_id']);
                roxas.downloadable_reports.generate_share_summary(oReportParam);*/
                roxas.downloadable_reports.generate_share_summary(oReportParam, function(oResponse, status){
                        if(status){
                         window.location = oResponse.file_url;
                        }
                    }); 

            });
        },

        generate_share_summary : function(oParam,callBack){

            /*if(!oParam || Object.keys(oParam).length == 0){
                return false;
            }

            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : oParam,
                "url"    : roxas.config('url.server.base') + "reports/generate_share_summary",
                "beforeSend": function () { },
                "success": function (oData) {

                    var link = document.createElement('a');
                    link.href = oData.url;
                    link.download = oData.file;
                    link.className = 'download_xls_ss';
                    document.body.appendChild(link);
                    
                    window.location = link.href;

                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);*/

            if(!oParam || Object.keys(oParam).length == 0){
                return false;
            }

            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : oParam,
                "url"    : roxas.config('url.server.base') + "reports/extract_share_summary",
//                 "url"    : roxas.config('url.server.base') + "reports/generate_esop_summary",
                "beforeSend": function () {
                    $('.btn-download-share-summary').html('Downloading...');
                },
                "success": function (oData) {
                    if(typeof callBack == 'function'){
                        $('.btn-download-share-summary').html('Download Report');
                        callBack(oData.data, oData.status);
                    }
                }
            }

            roxas.downloadable_reports.ajax(oAjaxConfig);
        },

        initialize_share_summary : function(){
            Class = roxas.downloadable_reports;

            uiModalShareSummary = $('[modal-id="share-summary"]');

            Class.get_all_esop(function(esopList){
                Class.set_share_summary_modal('esop_name', esopList);
                var oEsopCompanies = [];
                for(var i in esopList){
                    var distribution = esopList[i]['distribution'];
                    for(var x in distribution){
                        oEsopCompanies.push(distribution[x]);   
                    };
                }
                Class.set_share_summary_modal('esop_company', oEsopCompanies);
            });

            var oWhere = {};
            oWhere['field'] = "c.name";
            oWhere['operator'] = "LIKE";
            oWhere['value'] = '%%';
            Class.get_companies(oWhere, function(oResult){
                for(var i in oResult){
                    oResult[i]['company_name'] = oResult[i]['name'];
                    oResult[i]['company_id'] = oResult[i]['id']
                }
                Class.set_share_summary_modal('esop_company', oResult);
            });

            Class.share_summary_modal_company_event();

            Class.share_summary_modal_submit_event();

            Class.sharesummary_modal_esopname_event();
        }

	}

})();

$(window).load(function(){
	roxas.downloadable_reports.initialize();
    roxas.downloadable_reports.initialize_esop_summary();
    roxas.downloadable_reports.initialize_share_summary();
    roxas.downloadable_reports.initialize_subscription_summary();
    roxas.downloadable_reports.initialize_subscription_summary();
    roxas.downloadable_reports.initialize_claim_summary();
});