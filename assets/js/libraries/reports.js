/**
 *  Reports Class
 *
 *
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var
        $gratuityDateFrom,
        $gratuityDateTo,
        $gratuityGrantDateFrom,
        $gratuityGrantDateTo,
        $gratuityGenerateButton,
        $gratuityGrantDateButton,
        $gratuityContainer,
        $gratuityTemplate,
        $gratuityBody,
        $gratutiyEmpty,
        $gratHeader,
        $gratEsopName,
        $gratPriceperShare,
        oGratTable,

        filler;


    CPlatform.prototype.reports = {

        initialize : function() {

                $gratuityGenerateButton = $('button.generate-gratuity');
                $gratuityGrantDateButton = $('button.grant_date_search');
                $gratuityContainer = $('tbody[data-container="gratuity"]');
                $gratuityTemplate = $('tr.template.gratuity');
                $gratuityBody = $('div.gratuity.tbl-rounded');
                $gratutiyEmpty = $('div.empty.calendar-icon');
                $gratHeader = $('#gratuity_search_params');
                $gratEsopName = $gratHeader.find('input[name="esop_name"]');
                $gratPriceperShare = $gratHeader.find('input[name="price_share"]');

             //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said

            //oGratTable = $('#gratuity').DataTable();

            // $('#gratuity').DataTable({
            // });
            var uiDisable = $(".gratselect").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt");
            uiDisable.attr("readonly", true);


            $("#esop_name").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".esop_name_search").click();
                    }
                });

            $("#price_share").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".price_search").click();
                    }
                });

            $("#total_value").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".total_value_search").click();
                    }
                });
           
            $gratHeader.on( "click", '.esop_name_search',function() {

                $('tbody.gratuity').find('tr.no_results_found').remove();

                var rows = $('tbody.gratuity').find('tr:not(".template")');
                var uiSearchFilter = $gratHeader.find('input[name="esop_name"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('custom-attr-gratuity_esop').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.reports.check_results();
            });


            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                $gratuityGrantDateFrom = $('#grant_from');
                $gratuityGrantDateTo = $('#grant_to');

                $gratuityGrantDateFrom.datetimepicker(dateTimePickerOptions);
                $gratuityGrantDateTo.datetimepicker(dateTimePickerOptions);


                $gratuityGrantDateFrom.on("dp.change", function (e) {
                    $gratuityGrantDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                $gratuityGrantDateTo.on("dp.change", function (e) {
                    $gratuityGrantDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            $gratHeader.on( "click", '.grant_date_search',function() {

                var uiSearchFrom = $gratHeader.find('input[name="grant_from"]');
                var uiSearchTo = $gratHeader.find('input[name="grant_to"]');

                 $('tbody.gratuity').find('tr.no_results_found').remove();

                if(uiSearchFrom.val() != '' && uiSearchTo.val() != '')
                {
                     var oParams = {
                         'date_from' : uiSearchFrom.val(),
                         'date_to' : uiSearchTo.val()
                     };

                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_gratuity",
                        "beforeSend": function () {
                            if (cr8v.var_check($gratuityGrantDateButton)) {
                                cr8v.show_spinner($gratuityGrantDateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($gratuityGrantDateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $gratutiyEmpty.addClass('hidden');
                                $gratuityBody.removeClass('hidden');
                                $gratuityContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $gratuityTemplate, $gratuityContainer );
                                }

                                    roxas.reports.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                        },
                        "error" : function() {
                            cr8v.show_spinner($gratuityGrantDateButton, false);
                        }
                    }

                    roxas.reports.ajax(oAjaxConfig)
                }

           

            });




            $gratHeader.on( "click", '.price_search',function() {

                 $('tbody.gratuity').find('tr.no_results_found').remove();

                var rows = $('tbody.gratuity').find('tr:not(".template")');
                var uiSearchFilter = $gratHeader.find('input[name="price_share"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                    val = parseFloat(cr8v.remove_commas(val)).toFixed(2);
                rows.show().filter(function() {
                    var text = cr8v.remove_commas($(this).attr('custom-attr-gratuity_price')).replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();
                
                roxas.reports.check_results();
            });

            $gratHeader.on( "click", '.total_value_search',function() {

                 $('tbody.gratuity').find('tr.no_results_found').remove();

                var rows = $('tbody.gratuity').find('tr:not(".template")');
                var uiSearchFilter = $gratHeader.find('input[name="total_value"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                    val = parseFloat(cr8v.remove_commas(val)).toFixed(2);
                rows.show().filter(function() {
                    var text = cr8v.remove_commas($(this).attr('custom-attr-gratuity_value')).replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();
                
                roxas.reports.check_results();
            });

            $('.btn_export').on('click', function(){
                 var $clonedTable = $(".gratuity").clone();
                 $clonedTable.find('[style*="display: none"]').remove();

                 $($clonedTable).table2excel({
                     exclude: ".template",
                     name: "Gratuity Report",
                     filename: "Gratuity_Summary_Report"

                });
            });

            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                $gratuityDateFrom = $('#gratuity_from');
                $gratuityDateTo = $('#gratuity_to');

                $gratuityDateFrom.datetimepicker(dateTimePickerOptions);
                $gratuityDateTo.datetimepicker(dateTimePickerOptions);


                $gratuityDateFrom.on("dp.change", function (e) {
                    $gratuityDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                $gratuityDateTo.on("dp.change", function (e) {
                    $gratuityDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            $gratuityGenerateButton.on('click' , function() {

                 $('tbody.gratuity').find('tr.no_results_found').remove();

                if($gratuityDateFrom.val() != '' && $gratuityDateTo.val() != '')
                {
                     var oParams = {
                         'date_from' : $gratuityDateFrom.val(),
                         'date_to' : $gratuityDateTo.val()
                     };

                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_gratuity",
                        "beforeSend": function () {
                            if (cr8v.var_check($gratuityGenerateButton)) {
                                cr8v.show_spinner($gratuityGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($gratuityGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $gratutiyEmpty.addClass('hidden');
                                $gratuityBody.removeClass('hidden');
                                $gratuityContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $gratuityTemplate, $gratuityContainer );
                                }
                                roxas.reports.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();


                            $('#gratuity_search_params').removeClass('hidden');
                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($gratuityGenerateButton, false);
                        }
                    }

                    roxas.reports.ajax(oAjaxConfig)
                }

            })

        },
        

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'check_results' : function() {

            if($('tr.gratuity:visible').length==0){

                var uiNoresults =
                        "<tr class='no_results_found'>"+
                            "<td colspan='6'>No Results found</td>"+
                          "</tr>";

                $gratuityContainer.append(uiNoresults);
            }

        }
 
    };

}());

$(window).load(function () {
    roxas.reports.initialize();
});
