(function () {
    "use strict";

     var uiEsopDropdown,
     uiEsopSelect,
     takenContainer,
     empTakenShareTemplate,
     empTakenShareBody,
     empTakenShareEmpty,
     uiemptakenShareGenerateButton,
     esopSelectOptions,
     uiFilterTable,
     takenHeader ;
        

CPlatform.prototype.employee_taken_share = {

        initialize : function() {
            /*declare variables*/
           
            roxas.employee_taken_share.get_esop_list();
           

            uiEsopDropdown = $('#esop_dropdown');
           
            uiFilterTable =$('#filterTable');
            uiemptakenShareGenerateButton = $('button.generate-emp-taken-share');
            takenContainer = $('tbody[data-container="takenshare"]');
            empTakenShareTemplate = $('tr.template.takenshare');
            empTakenShareBody = $('div.takenshare.tbl-rounded');
            empTakenShareEmpty = $('div.empty.calendar-icon');

            takenHeader = $('#show-search');

            var uiDisable = $(".test").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt");
            uiDisable.attr("readonly", true);

            
        var x;
        setTimeout(function() {
                esopSelectOptions =  uiEsopDropdown.find('div.option.esop');
                uiEsopDropdown.on('click', 'div.option.esop', function() {
                    uiEsopDropdown.attr('esop_id' , $(this).attr('data-value'))
                     x = $(this).attr('data-value')
                    // console.log(x);
                    uiemptakenShareGenerateButton.on('click' , function() {
                var oStoredData = [];

                esopSelectOptions =  uiEsopDropdown.find('div.option.esop');

              

                 $('tbody.takenshare').find('tr.no_results_found').remove();

                 var oParams = {
                    "limit" : 999999,
                    "id" : 1,
                    "where" : [{ "field" : "e.parent_id", "operator" : '=', "value" : 0 ,"field":"e.id", 'operator' : 
                    '=', "value": x }]
                 }

                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/get_by_esop",
                        
                        "success" : function(oData) {
                           

                           if(cr8v.var_check(oData))
                            {
                                empTakenShareEmpty.addClass('hidden');
                                empTakenShareBody.removeClass('hidden');
                                takenContainer.html('');
                                

                                for(var i = 0 ; i < oData.data.length ; i++ )
                                {
                                    cr8v.populate_data(oData.data[i] , empTakenShareTemplate, takenContainer );
                                   
                                      /*var oCollectData = {};

                                            oCollectData = {
                                               //first_name:oData.data[i]["esop_name"],
                                                first_name : oData.data[i]["first_name"],
                                                total_share_offered :oData.data[i]["total_share_offered"],
                                                total_shares_availed :oData.data[i]["total_share_availed"],
                                                total_share_unavailed :oData.data[i]["total_share_unavailed"],
                                            }

                                            oStoredData.push(oCollectData);*/
                                }

                                 localStorage.setItem('esopdata', JSON.stringify(oStoredData));
                                
                                $('#show-search').removeClass('hidden');
                                $('.btn_export').removeClass('hidden');
                                $('#calendar').addClass('hidden');
                                    //roxas.reports.check_results();

                            }



                        },
                        "error" : function() {
                            cr8v.show_spinner(empTakenShareGenerateButton, false);
                        }
                    }

                     roxas.employee_taken_share.ajax(oAjaxConfig);
            });

                })
                
            } , 500);
           console.log(x);
            $("#employee_name").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".employee_name_search").click();
                    }
                });

            $("#total_shares_offered").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".total_shares_offered_search").click();
                    }
                });

            $("#total_shares_availed").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".total_shares_availed_search").click();
                    }
                });

            $("#total_shares_unavailed").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".total_shares_unavailed_search").click();
                    }
                });


            takenHeader.on( "click", '.employee_name_search',function() {

                $('tbody.takenshare').find('tr.no_results_found').remove();

                var rows = $('tbody.takenshare').find('tr:not(".template")');
                var uiSearchFilter = takenHeader.find('input[name="employee_name"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('custom-attr-first_name').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_taken_share.check_results();
            });


            takenHeader.on( "click", '.total_shares_offered_search',function() {

                $('tbody.takenshare').find('tr.no_results_found').remove();

                var rows = $('tbody.takenshare').find('tr:not(".template")');
                var uiSearchFilter = takenHeader.find('input[name="total_shares_offered"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                    val = parseFloat(cr8v.remove_commas(val)).toFixed(2);
                rows.show().filter(function() {
                    var text = cr8v.remove_commas($(this).attr('custom-attr-total_share_offered')).replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_taken_share.check_results();
            });


            takenHeader.on( "click", '.total_shares_availed_search',function() {

                $('tbody.takenshare').find('tr.no_results_found').remove();

                var rows = $('tbody.takenshare').find('tr:not(".template")');
                var uiSearchFilter = takenHeader.find('input[name="total_shares_availed"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                    val = parseFloat(cr8v.remove_commas(val)).toFixed(2);
                rows.show().filter(function() {
                    var text = cr8v.remove_commas($(this).attr('custom-attr-total_shares_availed')).replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_taken_share.check_results();
            });


            takenHeader.on( "click", '.total_shares_unavailed_search',function() {

                $('tbody.takenshare').find('tr.no_results_found').remove();

                var rows = $('tbody.takenshare').find('tr:not(".template")');
                var uiSearchFilter = takenHeader.find('input[name="total_shares_unavailed"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                    val = parseFloat(cr8v.remove_commas(val)).toFixed(2);
                rows.show().filter(function() {
                    var text = cr8v.remove_commas($(this).attr('custom-attr-total_share_unavailed')).replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                roxas.employee_taken_share.check_results();
            });

            



            $('.btn_export').on('click', function(){
                 var $clonedTable = $(".takenshare").clone();
                 $clonedTable.find('[style*="display: none"]').remove();

                 $($clonedTable).table2excel({
                     exclude: ".template",
                     name: "Employee Taken Share Report",
                     filename: "Employee_Taken_Share_Report"

                });
            });
            
            
        },

        bind_dropdown_events : function(){

        },
        'check_results' : function() {

            if($('tr.takenshare:visible').length==0){

                var uiNoresults =
                        "<tr class='no_results_found'>"+
                            "<td colspan='6'>No Results found</td>"+
                          "</tr>";

                takenContainer.append(uiNoresults);
            }

        },

        get_esop_list : function(){
          
            var oGetEsopParams = {
                    "limit" : 999999,
                    "where" : [{
                        "field" : "e.parent_id",
                        "value" : 0
                    }]
            };
            
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oGetEsopParams,
                "url"    : roxas.config('url.server.base') + "reports/get_esop_list",
                "beforeSend": function () {

                },
                "success": function (oData) {
                   // console.log(oData.data[1]);
                    
                   
                    if(oData.status == true)
                            {
                                uiEsopDropdown.find('div.option').remove();
                                
                                for(var x in oData.data)
                                {
                                    var oEsopData = oData.data[x];
                                    var uiTemplate = uiEsopDropdown;
                                    cr8v.append_dropdown(oEsopData, uiTemplate, false, "esop");
                                }
                               
                            }
                   
                },
                "complete": function () {

                },
            };

            roxas.employee_taken_share.ajax(oAjaxConfig);
            
        },

        get_esop_by_name : function(){
          
            var oGetEsopParams = {
                    "limit" : 999999,
                    "where" : [{
                        "field" : "e.parent_id",
                        "value" : 0
                    }]
            };
            
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oGetEsopParams,
                "url"    : roxas.config('url.server.base') + "reports/get_by_esop",
                "beforeSend": function () {

                },
                "success": function (oData) {
                    //console.log(oData.data)
                    if(oData.status == true)
                            {
                                uiEsopDropdown.find('div.option').remove();
                                
                                for(var x in oData.data)
                                {
                                    var oEsopData = oData.data[x];
                                    var uiTemplate = uiEsopDropdown;
                                    cr8v.append_dropdown(oEsopData, uiTemplate, false, 'esop');
                                }
                            }
                   
                },
                "complete": function () {

                },
            };

            roxas.employee_taken_share.ajax(oAjaxConfig);
            
        },

       

        'ajax': function (oAjaxConfig) {
                if (cr8v.var_check(oAjaxConfig)) {
                    roxas.CconnectionDetector.ajax(oAjaxConfig);
                }
        }



}


}());

$(window).load(function () {
    
    roxas.employee_taken_share.initialize();
});