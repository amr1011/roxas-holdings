/**
 *  Settings Class
 *
 *  @author Jechonias Alejandro
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var
        uiAddLetterModal,
        uiEditLetterModal,
        uiArchiveLetterModal,
        uiSearchLetterContainer,
        uiPaymentMethodListTable,
        uiAddPaymentMethodModal,
        uiEditPaymentMethodModal,
        uiDeletePaymentMethodModal,
        uiSearchPaymentMethodContainer
        ;


    CPlatform.prototype.settings = {

        initialize : function() {
            /*declare variables*/
           uiAddLetterModal = $('div[modal-id="add-letter"]');
           uiEditLetterModal = $('div[modal-id="edit-letter"]');
           uiArchiveLetterModal = $('div[modal-id="archive-letter"]');
           uiSearchLetterContainer = $('table.search_letter_container');
           uiPaymentMethodListTable = $('table#payment_method_list');
           uiAddPaymentMethodModal = $('[modal-id="add-payment"]');
           uiEditPaymentMethodModal = $('[modal-id="edit-payment"]');
           uiDeletePaymentMethodModal = $('[modal-id="delete-payment"]');
           uiSearchPaymentMethodContainer = $('table.search_payment_method_container');

           uiAddLetterModal.find('div.select input').attr('disabled', true);
           uiEditLetterModal.find('div.select input').attr('disabled', true);

           uiAddLetterModal.on('click','button.submit-letter',function(e){
               var sName = uiAddLetterModal.find('input[name="name"]').val();
               var sType = uiAddLetterModal.find('input[name="type"]').attr('value');
               var sContent = CKEDITOR.instances.editor1.getData();
               var uiBtn = $(this);
               
                var oParams = {
                'name'              : sName,
                'type'              : sType,
                'content'           : sContent,
                'user_id'           : iUserID,
                'status'            : 1
                };

                roxas.settings.add_offer_letter(oParams, uiBtn);
           })

           uiAddLetterModal.on('click','div.option',function(e){
                var sValue = $(this).attr('data-value');
                uiAddLetterModal.find('input[name="type"]').attr('value',sValue);
           })

           uiAddLetterModal.on('click','ul.tags li',function(e){
                var sValue = ' '+$(this).attr('data-value');
                CKEDITOR.instances.editor1.insertText(sValue);
           })

           uiEditLetterModal.on('click','button.update-letter',function(e){
               var sID = $(this).attr('data-id');
               var sName = uiEditLetterModal.find('input[name="name"]').val();
               var sType = uiEditLetterModal.find('div.select input').attr('value');
               var sContent = CKEDITOR.instances.editor1.getData();
               var uiBtn = $(this);
               
                var oParams = {
                'id'                : sID,
                'name'              : sName,
                'type'              : sType,
                'content'           : sContent,
                'user_id'           : iUserID,
                'user_name'         : sFullName,
                'type_name'         : uiEditLetterModal.find('div.select input').val(),
                'status'            : 1
                };

                roxas.settings.update_offer_letter(oParams, uiBtn);
           })
            
           uiEditLetterModal.on('click','div.option',function(e){
                var sValue = $(this).attr('data-value');
                uiEditLetterModal.find('div.select input').attr('value',sValue);
           })

           uiEditLetterModal.on('click','ul.tags li',function(e){
                var sValue = ' '+$(this).attr('data-value');
                CKEDITOR.instances.editor1.insertText(sValue);
           })

           uiArchiveLetterModal.on('click','button.archive-letter',function(e){
                var sID = $(this).attr('data-id');
                var sName = $(this).attr('data-name');
                var uiBtn = $(this);
                var oParams = {
                'id'                : sID,
                'status'            : 0,
                'name'              : sName
                };

                roxas.settings.archive_offer_letter(oParams, uiBtn);
           })
           
           if(cr8v.var_check(sOfferLetterType))
           {
            uiEditLetterModal.find('div.option[data-value="'+sOfferLetterType+'"]').trigger('click');
           }

           uiSearchLetterContainer.on('click','button.btn-search',function(e){
                var uiBtn = $(this);
                cr8v.show_spinner(uiBtn, true);
                var rows = $('div.offer_letter_list').find('div.data-box');
                var uiSearchFilter = uiSearchLetterContainer.find('input[name="search"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('data-name').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                setTimeout(function(){
                   cr8v.show_spinner(uiBtn, false);
                }, 200)
           })

           uiSearchLetterContainer.on('keyup','input[name="search"]',function(e){
                if(e.keyCode == 13)
                {
                   uiSearchLetterContainer.find('button.btn-search').trigger('click');
                } 
           })
           
           uiAddPaymentMethodModal.on("click","button.add_payment", function() {       
                var uiName = uiAddPaymentMethodModal.find('input[name="name"]');
                var uiBtn = $(this);

                var sName = uiName.val();

                var oParams = {
                'name'              : sName,
                'status'            : 1
                };

                roxas.settings.add_payment_method(oParams, uiBtn);

           });

           $(".edit_payment").on("click", function() {       
                var uiThis = $(this);
                var sID = uiThis.attr('data-id');
                var sName = uiThis.attr('data-name');
                uiEditPaymentMethodModal.attr('data-id',sID);
                uiEditPaymentMethodModal.find('input[name="name"]').val(sName);
           });

           uiEditPaymentMethodModal.on("click","button.update_payment", function() {       
                var sID = uiEditPaymentMethodModal.attr('data-id');
                var uiName = uiEditPaymentMethodModal.find('input[name="name"]');
                var uiBtn = $(this);

                var sName = uiName.val();

                var oParams = {
                'id'                : sID,
                'name'              : sName
                };

                roxas.settings.update_payment_method(oParams, uiBtn);

           });

           $(".delete_payment").on("click", function() {       
                var uiThis = $(this);
                var sID = uiThis.attr('data-id');
                var sName = uiThis.attr('data-name');

                uiDeletePaymentMethodModal.attr('data-id',sID); 
                uiDeletePaymentMethodModal.attr('data-name',sName); 
           });

           uiDeletePaymentMethodModal.on("click","button.remove_payment", function() {       
                var sID = uiDeletePaymentMethodModal.attr('data-id');
                var sName = uiDeletePaymentMethodModal.attr('data-name');
                var uiBtn = $(this);

                var oParams = {
                'id'                : sID,
                'status'            : 0,
                'name'              : sName
                };

                roxas.settings.delete_payment_method(oParams, uiBtn);

           });

           uiSearchPaymentMethodContainer.on('click','button.btn-search',function(e){
                var uiBtn = $(this);
                cr8v.show_spinner(uiBtn, true);
                var rows = $('tbody.payment_method_list_body').find('tr');
                var uiSearchFilter = uiSearchPaymentMethodContainer.find('input[name="search"]');

                var val = $.trim(uiSearchFilter.val()).replace(/ +/g, ' ').toLowerCase();
                rows.show().filter(function() {
                    var text = $(this).attr('data-name').replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();

                setTimeout(function(){
                   cr8v.show_spinner(uiBtn, false);
                }, 200)
           })

           uiSearchPaymentMethodContainer.on('keyup','input[name="search"]',function(e){
                if(e.keyCode == 13)
                {
                   uiSearchPaymentMethodContainer.find('button.btn-search').trigger('click');
                } 
           })
        },

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

         /**
         * add_offer_letter
         * @description This function will add letter
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        add_offer_letter : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "settings/add_offer_letter",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddLetterModal.find('.add_letter_error_message').addClass('hidden').html('');
                                uiAddLetterModal.find('.add_letter_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiAddLetterModal.hasClass('showed'))
                                    {
                                        uiAddLetterModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'settings/offer_letter';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiAddLetterModal.find('.add_letter_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.settings.ajax(oAjaxConfig);
            }
        },

        /**
         * update_offer_letter
         * @description This function will update letter
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        update_offer_letter : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "settings/update_offer_letter",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiEditLetterModal.find('.update_letter_error_message').addClass('hidden').html('');
                                uiEditLetterModal.find('.update_letter_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiEditLetterModal.hasClass('showed'))
                                    {
                                        uiEditLetterModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'settings/view_offer_letter/' + oParams.id;
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiEditLetterModal.find('.update_letter_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.settings.ajax(oAjaxConfig);
            }
        },

        /**
         * archive_offer_letter
         * @description This function will archive letter
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        archive_offer_letter : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "settings/archive_offer_letter",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiArchiveLetterModal.find('.archive_letter_error_message').addClass('hidden').html('');
                                uiArchiveLetterModal.find('.archive_letter_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiArchiveLetterModal.hasClass('showed'))
                                    {
                                        uiArchiveLetterModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'settings/archive_letter/';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiArchiveLetterModal.find('.archive_letter_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.settings.ajax(oAjaxConfig);
            }
        },

        /**
         * add_payment_method
         * @description This function will add_payment_method
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        add_payment_method : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "settings/add_payment_method",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddPaymentMethodModal.find('.add_payment_method_error_message').addClass('hidden').html('');
                                uiAddPaymentMethodModal.find('.add_payment_method_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiAddPaymentMethodModal.hasClass('showed'))
                                    {
                                        uiAddPaymentMethodModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'settings/payment_method_list';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiAddPaymentMethodModal.find('.add_payment_method_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.settings.ajax(oAjaxConfig);
            }
        },

        /**
         * update_payment_method
         * @description This function will update_payment_method
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        update_payment_method : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "settings/update_payment_method",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiEditPaymentMethodModal.find('.update_payment_method_error_message').addClass('hidden').html('');
                                uiEditPaymentMethodModal.find('.update_payment_method_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiEditPaymentMethodModal.hasClass('showed'))
                                    {
                                        uiEditPaymentMethodModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'settings/payment_method_list';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiEditPaymentMethodModal.find('.update_payment_method_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.settings.ajax(oAjaxConfig);
            }
        },

        /**
         * delete_payment_method
         * @description This function will delete_payment_method
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        delete_payment_method : function(oParams, uiBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "settings/delete_payment_method",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiDeletePaymentMethodModal.find('.delete_payment_method_error_message').addClass('hidden').html('');
                                uiDeletePaymentMethodModal.find('.delete_payment_method_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiDeletePaymentMethodModal.hasClass('showed'))
                                    {
                                        uiDeletePaymentMethodModal.find('.close-me').trigger('click');
                                        window.location.href = roxas.config('url.server.base') + 'settings/payment_method_list/';
                                    }
                                }, 1000)

                            }
                            else
                            {
                                uiDeletePaymentMethodModal.find('.delete_payment_method_error_message').removeClass('hidden').html('<p class="font-15 error_message"> ' + oData.message + '</p>');
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiBtn)) {
                            cr8v.show_spinner(uiBtn, false);
                        }
                    },
                };

                roxas.settings.ajax(oAjaxConfig);
            }
        },



    }

}());

$(window).load(function () {
    roxas.settings.initialize();
});
