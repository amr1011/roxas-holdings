/**
 *  Companies Class
 *
 *
 *
 */
(function () {
    "use strict";

    /*variable declaration*/
    var uiAddCompanyModalBtn,
        uiAddCompanyBtn,
        uiAddCompanyModal,
        uiAddCompanyForm,

        uiEditCompanyModalBtn,
        uiEditCompanyBtn,
        uiEditCompanyModal,
        uiEditCompanyForm,
        
        uiCompanyListContainer,

        uiSearchCompanyInput,
        uiSearchCompanyBtn,
        uiSearchCompanyDropdown,

        uiSortCompanyContainer,
        sGeneratedCode,

        uiNoResultsFound
        ;

    CPlatform.prototype.companies = {

        initialize : function() {
            /*declare variables*/
            uiAddCompanyModalBtn = $('[modal-target="add-company"]');
            uiAddCompanyBtn = $('#add_company_btn');
            uiAddCompanyModal = $('[modal-id="add-company"]');
            uiAddCompanyForm = $('form#add_company_form');
            
            uiEditCompanyModalBtn = $('[modal-target="edit-company"]');
            uiEditCompanyBtn = $('#edit_company_btn');
            uiEditCompanyModal = $('[modal-id="edit-company"]');
            uiEditCompanyForm = $('form#edit_company_form');
            
            uiCompanyListContainer = $('[data-container="company_list_container"]');

            uiSearchCompanyInput = $('#search_company_input');
            uiSearchCompanyBtn = $('#search_company_btn');
            uiSearchCompanyDropdown = $('#search_field_dropdown');

            uiSortCompanyContainer = $('#sort_company');

            uiNoResultsFound = $('.no_results_found.template');

            /*bind on load data*/
            roxas.companies.bind_events_on_load();

            /*initialize events*/
            cr8v.input_numeric($('[name="company_code"]'));

            /*for viewing of company details and its departments*/
            uiCompanyListContainer.on('click', '.view_departments', function(){
                var uiThis = $(this);
                var iCompanyID = uiThis.parents('.company:first').attr('data-company-id');
                roxas.companies.view_departments(iCompanyID);
            });

            uiSearchCompanyDropdown.find('.frm-custom-dropdown-txt').find('input').attr('disabled', true);
            
        },

        /**
         * bind_search_event
         * @description This function is for binding search event
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_search_event : function(){
            uiSearchCompanyBtn.on('click', function(){
                if(uiSearchCompanyInput.val().length > 0)
                {
                    roxas.companies.assemble_search_params(uiSearchCompanyBtn);
                }
            });

            uiSearchCompanyInput.on('keyup', function (e){
                if(e.keyCode == 13)
                {
                    uiSearchCompanyBtn.trigger('click');
                }
            });

            roxas.companies.bind_sorting_events();
        },

        /**
         * bind_sorting_events
         * @description This function is for binding events for sorting
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_sorting_events : function(){
            uiSortCompanyContainer.on('click', '.sort_field', function(){
                console.log($(this).text())
                var uiThis = $(this);
                var sSortBy = uiThis.attr('data-sort-by');
                var uiTemplates = uiCompanyListContainer.find('.company:not(.template)');

                cr8v.clear_sorting_classes(uiSortCompanyContainer.find('.sort_field'));
                cr8v.ui_sorting(uiThis, sSortBy, uiTemplates.length, uiTemplates, uiCompanyListContainer)
            });
            
            cr8v.clear_sorting_classes(uiSortCompanyContainer.find('.sort_field'));
        },

        /**
         * assemble_search_params
         * @description This function is for assembling params for search
         * @dependencies 
         * @param {jQuery} uiButton
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_search_params : function(uiButton, sSorting, sSortField){
            /*sample params*/
            /*{
                "keyword" : "string", 
                "search_field" : "string", 
                "sorting" : "string", 
                "sort_field" : "string", 
                "offset" : "int", 
                "limit" : "int"
            }*/

            var oGetCompanyParams = {

            };

            var sSearchField = uiSearchCompanyDropdown.find('input').val();
            var sSearchValue = uiSearchCompanyInput.val();

            if(sSearchField == 'Company Name')
            {
                oGetCompanyParams.search_field = 'name';
                oGetCompanyParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'Company Code')
            {
                oGetCompanyParams.search_field = 'company_code';
                oGetCompanyParams.keyword = sSearchValue;
            }
            else if(sSearchField == 'No. of Departments')
            {
                oGetCompanyParams.search_field = 'department_count';
                oGetCompanyParams.keyword = sSearchValue;
            }

            if(cr8v.var_check(sSorting))
            {
                oGetCompanyParams.sorting = sSorting;
            }

            if(cr8v.var_check(sSortField))
            {
                oGetCompanyParams.sort_field = sSortField;
            }

            roxas.companies.get_companies(oGetCompanyParams, uiButton);
        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('departments') > -1 && localStorage.getItem('company_id') != null)
            {
                // console.log('dep')
                var oGetCompanyParams = {
                    "search_field" : 'id',
                    "keyword" : localStorage.getItem('company_id'),
                };
                roxas.companies.get_companies(oGetCompanyParams, undefined, true);

                /*for edit of company events*/
                roxas.companies.bind_edit_company_events();
            }
            else if(window.location.href.indexOf('companies') > -1)
            {
                // console.log('comp')
                var oGetCompanyParams = {
                    "limit" : 999999
                };
                roxas.companies.get_companies(oGetCompanyParams);

                /*for adding of company events*/
                roxas.companies.bind_add_company_events();

                /*for search of company events*/
                roxas.companies.bind_search_event();
            }
            else if(window.location.href.indexOf('users') > -1)
            {

            }
        },

        /**
         * bind_add_company_events
         * @description This function is for binding events for add company info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_add_company_events : function(){

            uiAddCompanyModalBtn.on('click', function(){
                /*clear data*/
                roxas.companies.clear_add_company_modal();
                roxas.companies.generate_new_code({table : 'company', column : 'company_code'});
            })

            /*save new company*/
            uiAddCompanyBtn.on('click', function(){
                uiAddCompanyForm.submit();
            });

            /*prevent on submit of form*/
            uiAddCompanyModal.on('submit', uiAddCompanyForm, function (e) {
                e.preventDefault();
            });

            /*add a validation to adding a new company*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiAddCompanyBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiAddCompanyModal.find('.add_company_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiAddCompanyModal.find('.add_company_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiAddCompanyBtn, true);
                    
                    roxas.companies.assemble_add_company_information(uiAddCompanyModal, uiAddCompanyBtn);
                }
            };

            /*bind the validation to add new company form*/
            cr8v.validate_form(uiAddCompanyForm, oValidationConfig);

            /*trigger input file*/
            uiAddCompanyModal.on('click', '.upload-photo.upload-photo-modal', function(){
                $(this).next('label').find('input').trigger('click');
            });

            /*upload company image*/
            var last_image_path = ''/*sImagePath*/;
            var last_image_name = ''/*sImageFile*/;
            uiAddCompanyModal.on('change', 'input[name="company_image"]', function(){
                var uiThis = $(this);
                var image = uiAddCompanyModal.find('input[type=file][name="company_image"]')[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = uiAddCompanyModal.find('[data-container="image_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'jpg' || sValueExt == 'jpeg' || sValueExt == 'gif' || sValueExt == 'png')
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('client_img', image.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'companies/ajax_upload_image',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData.data))
                                    {
                                        image_path = (cr8v.var_check(oData.data.image_path)) ? oData.data.image_path : last_image_path;
                                        image_name = (cr8v.var_check(oData.data.image_name)) ? oData.data.image_name : '';
                                        last_image_path = image_path;
                                        last_image_name = image_name;

                                        if(cr8v.var_check(oData.status))
                                        {
                                            var tthtml = oData.message;
                                            var ttx = uiLogoContainer.offset().left + 50;
                                            var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                                            $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                                            setTimeout(function() {
                                                $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                                            }, 2000);
                                        }
                                    }
                                    else
                                    {
                                        image_path = last_image_path;
                                        image_name = last_image_name;
                                    }

                                },
                                complete: function(){
                                    uiLogoContainer.find('.preloader').remove();
                                    if(cr8v.var_check(image_path))
                                    {
                                        var sStyle = '';
                                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ image_path);
                                        uiThis.attr('image_name', image_name);
                                        uiThis.data('image_name', image_name);

                                        uiThis.attr('image_path', image_path);
                                        uiThis.data('image_path', image_path);
                                    }

                                }
                            };
                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        image_name = last_image_name;
                        uiThis.val('');
                        uiThis.attr('image_name', image_name);
                        uiThis.data('image_name', image_name);

                        uiThis.attr('image_path', image_path);
                        uiThis.data('image_path', image_path);

                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ last_image_path);

                        var tthtml = 'Invalid image file, please try again.';
                        var ttx = uiLogoContainer.offset().left + 50;
                        var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                        $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                        setTimeout(function() {
                            $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                        }, 2000);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });
        },

        /**
         * bind_edit_company_events
         * @description This function is for binding events for edit company info
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        bind_edit_company_events : function(){

            uiEditCompanyModalBtn.on('click', function(){
                /*clear data*/
                roxas.companies.clear_edit_company_modal();
            })

            /*save new company*/
            uiEditCompanyBtn.on('click', function(){
                uiEditCompanyForm.submit();
            });

            /*prevent on submit of form*/
            uiEditCompanyModal.on('submit', uiEditCompanyForm, function (e) {
                e.preventDefault();
            });

            /*add a validation to adding a new company*/
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'input-error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    // console.log(arrMessages)
                    cr8v.show_spinner(uiEditCompanyBtn, false);

                    if(cr8v.var_check(arrMessages))
                    {
                        var sErrorMessages = '';
                        var uiErrorContainer = uiEditCompanyModal.find('.edit_company_error_message');
                        for(var i = 0, max = arrMessages.length; i < max; i++)
                        {
                            var message = arrMessages[i];

                            sErrorMessages += '<p class="font-15 error_message"> ' + message.error_message + '</p>';
                        }

                        cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                    }
                },
                'onValidationSuccess': function () {
                    var uiErrorContainer = uiEditCompanyModal.find('.edit_company_error_message');
                    cr8v.add_error_message(false, uiErrorContainer);
                    
                    cr8v.show_spinner(uiEditCompanyBtn, true);
                    
                    roxas.companies.assemble_edit_company_information(uiEditCompanyModal, uiEditCompanyBtn);
                }
            };

            /*bind the validation to add new company form*/
            cr8v.validate_form(uiEditCompanyForm, oValidationConfig);

            /*trigger input file*/
            uiEditCompanyModal.on('click', '.upload-photo.upload-photo-modal', function(){
                $(this).next('label').find('input').trigger('click');
            });

            /*upload company image*/
            var last_image_path = ''/*sImagePath*/;
            var last_image_name = ''/*sImageFile*/;
            uiEditCompanyModal.on('change', 'input[name="company_image"]', function(){
                var uiThis = $(this);
                var image = uiEditCompanyModal.find('input[type=file][name="company_image"]')[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = uiEditCompanyModal.find('[data-container="image_container"]:visible');
                var sValue = uiThis.val();
                var sValueExt = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();

                /* check if cancel is clicked because it changes also the input value if you click cancel */
                if(sValue != '')
                {
                    /* check the extensions */
                    if(sValueExt == 'jpg' || sValueExt == 'jpeg' || sValueExt == 'gif' || sValueExt == 'png')
                    {
                        if(window.FormData)
                        {
                            formdata = new FormData();
                            formdata.append('client_img', image.files[0]);
                            formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                            var oSettings = {
                                type: 'POST',
                                url: roxas.config('url.server.base') + 'companies/ajax_upload_image',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                beforeSend: function(){

                                },
                                success: function(sData){
                                    var oData = $.parseJSON(sData);
                                    if(cr8v.var_check(oData.data))
                                    {
                                        image_path = (cr8v.var_check(oData.data.image_path)) ? oData.data.image_path : last_image_path;
                                        image_name = (cr8v.var_check(oData.data.image_name)) ? oData.data.image_name : '';
                                        last_image_path = image_path;
                                        last_image_name = image_name;

                                        if(cr8v.var_check(oData.status))
                                        {
                                            var tthtml = oData.message;
                                            var ttx = uiLogoContainer.offset().left + 50;
                                            var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                                            $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                                            setTimeout(function() {
                                                $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                                            }, 2000);
                                        }
                                    }
                                    else
                                    {
                                        image_path = last_image_path;
                                        image_name = last_image_name;
                                    }

                                },
                                complete: function(){
                                    uiLogoContainer.find('.preloader').remove();
                                    if(cr8v.var_check(image_path))
                                    {
                                        var sStyle = '';
                                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ image_path);
                                        uiThis.attr('image_name', image_name);
                                        uiThis.data('image_name', image_name);

                                        uiThis.attr('image_path', image_path);
                                        uiThis.data('image_path', image_path);
                                    }

                                }
                            };
                            $.ajax(oSettings);
                        }
                    }
                    else
                    {
                        image_name = last_image_name;
                        uiThis.val('');
                        uiThis.attr('image_name', image_name);
                        uiThis.data('image_name', image_name);

                        uiThis.attr('image_path', image_path);
                        uiThis.data('image_path', image_path);

                        uiLogoContainer.find('img').attr('src', roxas.config('url.server.base') +''+ last_image_path);

                        var tthtml = 'Invalid image file, please try again.';
                        var ttx = uiLogoContainer.offset().left + 50;
                        var tty = uiLogoContainer.offset().top - ($(".show-dialogue").height() + 10) ;
                        $(".show-dialogue").css({top:tty,left:ttx}).html(tthtml).stop().fadeIn(300);
                        setTimeout(function() {
                            $(".show-dialogue").stop().html(tthtml).fadeOut(300);
                        }, 2000);
                    }
                }
                else
                {
                    console.log('no value');
                }
            });
        },

        /**
         * clear_add_company_modal
         * @description This function is for clearing data in add company modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_add_company_modal : function(){
            /*clear data*/
            uiAddCompanyModal.find('input').removeClass('input-error');
            uiAddCompanyModal.find('input[name="company_name"]').val('');
            uiAddCompanyModal.find('input[name="company_code"]').val('');
            uiAddCompanyModal.find('input[name="company_image"]').removeData();
            uiAddCompanyModal.find('.add_company_success_message').addClass('hidden');
            uiAddCompanyModal.find('.add_company_error_message').addClass('hidden');
            uiAddCompanyModal.find('[data-container="image_container"]').find('img').removeAttr('src');
        },


        /**
         * clear_edit_company_modal
         * @description This function is for clearing data in add company modal
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        clear_edit_company_modal : function(){
            /*clear data*/
            uiEditCompanyModal.find('input').removeClass('input-error');
            uiEditCompanyModal.find('.edit_company_success_message').addClass('hidden');
            uiEditCompanyModal.find('.edit_company_error_message').addClass('hidden');
        },

        /**
         * view_departments
         * @description This function is for viewing the departments of a specific company
         * @dependencies 
         * @param {int} iCompanyID
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        view_departments : function(iCompanyID){
            if(cr8v.var_check(iCompanyID))
            {
                localStorage.removeItem("company_id");
                localStorage.setItem('company_id', iCompanyID);
                window.location =  roxas.config('url.server.base') + "departments/department_list/";
            }
        },

        /**
         * assemble_add_company_information
         * @description This function will assemble add company information
         * @dependencies N/A
         * @param {jQuery} uiAddCompanyModal
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_add_company_information : function(uiAddCompanyModal, uiAddCompanyBtn){
            var oParams = {
                'name' : uiAddCompanyModal.find('input[name="company_name"]').val(),
                'company_code' : uiAddCompanyModal.find('input[name="company_code"]').val(),
                'img' : uiAddCompanyModal.find('input[name="company_image"]').data('image_path'),
            };
            roxas.companies.add_company(oParams, uiAddCompanyBtn);
        },

        /**
         * add_company
         * @description This function will add company code, name and image
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        add_company : function(oParams, uiAddCompanyBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "companies/add",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiAddCompanyBtn)) {
                            cr8v.show_spinner(uiAddCompanyBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiAddCompanyModal.find('.add_company_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiAddCompanyModal.hasClass('showed'))
                                    {
                                        roxas.companies.clear_add_company_modal();
                                        uiAddCompanyModal.find('.close-me').trigger('click');
                                    }
                                }, 1000)
                                
                                var oCompanyData = oData.data;
                                var uiTemplate = $('.company.template').clone().removeClass('template').show();
                                var uiManipulatedTemplate = roxas.companies.manipulate_template(oCompanyData, uiTemplate);
                                uiCompanyListContainer.prepend(uiManipulatedTemplate);

                                uiNoResultsFound.addClass('template hidden');
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiAddCompanyModal.find('.add_company_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiAddCompanyBtn)) {
                            cr8v.show_spinner(uiAddCompanyBtn, false);
                        }
                    },
                };

                roxas.companies.ajax(oAjaxConfig);
            }
        },
        
        /**
         * assemble_edit_company_information
         * @description This function will assemble add company information
         * @dependencies N/A
         * @param {jQuery} uiAddCompanyModal
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        assemble_edit_company_information : function(uiEditCompanyModal, uiEditCompanyBtn){
            var oParams = {
                'id' : uiEditCompanyModal.attr('data-company-id'),
                'data' : [
                    {
                        'name' : uiEditCompanyModal.find('input[name="company_name"]').val(),
                        'company_code' : uiEditCompanyModal.find('input[name="company_code"]').val(),
                        'img' : uiEditCompanyModal.find('input[name="company_image"]').data('image_path')
                    }
                ]
            };
            roxas.companies.edit_company(oParams, uiEditCompanyBtn);
        },

        /**
         * edit_company
         * @description This function will edit company code, name and image
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        edit_company : function(oParams, uiEditCompanyBtn){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "companies/edit",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiEditCompanyBtn)) {
                            cr8v.show_spinner(uiEditCompanyBtn, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiEditCompanyModal.find('.edit_company_success_message').removeClass('hidden').html('<p class="font-15 success_message"> ' + oData.message + '</p>');
                               
                                setTimeout(function(){
                                    /*clear data*/
                                    if(uiEditCompanyModal.hasClass('showed'))
                                    {
                                        roxas.companies.clear_edit_company_modal();
                                        uiEditCompanyModal.find('.close-me').trigger('click');
                                    }
                                }, 1000)

                                var oCompanyData = oData.data;
                                var uiTemplate = $('[data-container="company_information"]');
                                var uiManipulatedTemplate = roxas.companies.manipulate_template(oCompanyData, uiTemplate);

                                var uiTemplateForEdit = uiEditCompanyModal;
                                var uiManipulatedTemplateForEdit = roxas.companies.populate_edit_info(oCompanyData, uiTemplateForEdit);
                            }
                            else
                            {
                                if(cr8v.var_check(oData.message) && count(oData.message) > 0)
                                {
                                    var arrMessages = oData.message;
                                    var sErrorMessages = '';
                                    var uiErrorContainer = uiEditCompanyModal.find('.edit_company_error_message');
                                    for(var i = 0, max = arrMessages.length; i < max; i++)
                                    {
                                        var error_message = arrMessages[i];

                                        sErrorMessages += '<p class="font-15 error_message"> ' + error_message + '</p>';
                                    }

                                    cr8v.add_error_message(true, uiErrorContainer, sErrorMessages);
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiEditCompanyBtn)) {
                            cr8v.show_spinner(uiEditCompanyBtn, false);
                        }
                    },
                };

                roxas.companies.ajax(oAjaxConfig);
            }
        },

        /**
         * manipulate_template
         * @description This function will put the data of the company needed on the template
         * @dependencies N/A
         * @param {object} oCompanyData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        manipulate_template : function(oCompanyData, uiTemplate){
            if(cr8v.var_check(oCompanyData) && cr8v.var_check(uiTemplate))
            {
                uiTemplate.attr('data-company-name', oCompanyData.name);
                uiTemplate.attr('data-company-code', oCompanyData.company_code);
                uiTemplate.attr('data-company-id', oCompanyData.id);
                uiTemplate.attr('data-company-dept-count', oCompanyData.department_count);

                uiTemplate.find('[data-label="company_code"]').text(oCompanyData.company_code);
                uiTemplate.find('[data-label="company_name"]').text(oCompanyData.name);
                uiTemplate.find('[data-label="company_id"]').text(oCompanyData.id);
                uiTemplate.find('[data-label="company_department_count"]').text(oCompanyData.department_count);

                if(cr8v.var_check(oCompanyData.img) && oCompanyData.img.length > 0)
                {
                    uiTemplate.find('[data-label="company_image"]').find('img').attr('src', roxas.config('url.server.base') +''+ oCompanyData.img);
                }

                return uiTemplate;
            }
        },

        /**
         * populate_edit_info
         * @description This function will put the data of the company needed on the edit info
         * @dependencies N/A
         * @param {object} oCompanyData
         * @param {jQuery} uiTemplate
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        populate_edit_info : function(oCompanyData, uiForm){
            if(cr8v.var_check(oCompanyData) && cr8v.var_check(uiForm))
            {
                uiForm.attr('data-company-name', oCompanyData.name);
                uiForm.attr('data-company-code', oCompanyData.company_code);
                uiForm.attr('data-company-id', oCompanyData.id);

                uiForm.find('[name="company_code"]').val(oCompanyData.company_code);
                uiForm.find('[name="company_name"]').val(oCompanyData.name);

                if(cr8v.var_check(oCompanyData.img) && oCompanyData.img.length > 0)
                {
                    uiForm.find('[data-container="image_container"]').find('img').attr('src', roxas.config('url.server.base') +''+ oCompanyData.img);
                    uiForm.find('[data-container="image_container"]').data('image_path', oCompanyData.img);
                    uiForm.find('input[name="company_image"]').data('image_path', oCompanyData.img);
                }

                return uiForm;
            }
        },

        /**
         * get_companies
         * @description This function is for getting and rendering companies on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @param {boolean} bFromDepartments
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Randall Bondoc
         * @method_id N/A
         */
        get_companies : function(oParams, uiThis, bFromDepartments){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "companies/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        if(cr8v.var_check(oData))
                        {
                            if(oData.status == true)
                            {
                                uiCompanyListContainer.find('.company:not(.template)').remove();
                                uiNoResultsFound.addClass('template hidden');

                                if(cr8v.var_check(bFromDepartments) && bFromDepartments)
                                {
                                    for(var i = 0, max = oData.data.length ; i < max; i++)
                                    {
                                        var oCompanyData = oData.data[i];
                                        var uiTemplate = $('[data-container="company_information"]');
                                        var uiManipulatedTemplate = roxas.companies.manipulate_template(oCompanyData, uiTemplate);
                                        
                                        var uiTemplateForEdit = uiEditCompanyModal;
                                        var uiManipulatedTemplateForEdit = roxas.companies.populate_edit_info(oCompanyData, uiTemplateForEdit);
                                        arrLogIDS.push(oCompanyData.id);
                                    }
                                }
                                else
                                {
                                    if(oData.data.length > 0)
                                    {
                                        for(var i = 0, max = oData.data.length ; i < max; i++)
                                        {
                                            var oCompanyData = oData.data[i];
                                            var uiTemplate = $('.company.template').clone().removeClass('template').show();
                                            var uiManipulatedTemplate = roxas.companies.manipulate_template(oCompanyData, uiTemplate);
                                            uiCompanyListContainer.prepend(uiManipulatedTemplate);
                                        }
                                    }
                                    else
                                    {
                                        uiNoResultsFound.removeClass('template hidden');
                                    }
                                }
                            }
                        }
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.companies.ajax(oAjaxConfig);
            }
        },

        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        generate_new_code : function(oParams) {
            var oData = {
                table : oParams.table,
                column : oParams.column
            },
            oAjaxConfig = {
                "type"   : "POST",
                "data"   : oData,
                "url"    : roxas.config('url.server.base') + "esop/generate_new_code",
                "success": function (oResult) {
                    // console.log(oResult);
                    if(oResult.status) {
                        sGeneratedCode = oResult.data;    
                        // console.log(oResult.data);
                        $('.code-generated').val(oResult.data);
                    }
                }
            };

            roxas.companies.ajax(oAjaxConfig);
        }
    }

}());

$(window).load(function () {
    roxas.companies.initialize();
});
