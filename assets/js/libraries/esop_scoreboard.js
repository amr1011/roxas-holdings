

(function () {
    "use strict";

    /*variable declaration*/
    var
        uiDefaultContainer,
        $emplpaymentDateFrom,
        $emplpaymentDateTo,
        $emplpaymentDateAddedFrom,
        $emplpaymentDateAddedTo,
        $esopGenerateButton,
        $totalsharequantityGenerateButton,
        $pricepershareGenerateButton,
        $vestingyearsGenerateButton,
        $noofemployeesofferedGenerateButton,
        $totalsharestakenGenerateButton,
        $totalsharesuntakenGenerateButton,
        $emplpaymentDateAddedButton,
        $esopContainer,
        $esopTemplate,
        $esopBody,
        $esopSelect,
        $txtEsopName,
        $txtTotalShareQty,
        $txtPricePerShare,
        $txtVestingYears,
        $txtNoofEmployee,
        $txtTotalSharesTaken,
        $txtTotalSharesUntaken,
        $esopSelectOptions,
        $emplpaymentHeader,
        $esopscoreboardHeader,
        oCurrencyName = {}
        ;


    CPlatform.prototype.esop_scoreboard = {

        initialize : function() {
            /*declare variables*/

                $esopGenerateButton = $('button.search-esop');
                $totalsharequantityGenerateButton = $('button.total_share_qty_search');
                $pricepershareGenerateButton = $('button.price_search');
                $vestingyearsGenerateButton = $('button.vesting_years_search');
                $noofemployeesofferedGenerateButton = $('button.no_of_employee_search');
                $totalsharestakenGenerateButton = $('button.total_shares_taken_search');
                $totalsharesuntakenGenerateButton = $('button.total_shares_untaken_search');
                $esopContainer = $('tbody[data-container="esop"]');
                $esopTemplate = $('tr.template.esop');
                $esopBody = $('div.esop.tbl-rounded');
                $esopSelect = $('#esop_dropdown');
                $esopscoreboardHeader = $('#esopscoreboard_search_params');
                $txtEsopName = $esopscoreboardHeader.find('input[name="esop_name"]');
                $txtTotalShareQty = $esopscoreboardHeader.find('input[name="total_share_qty"]');
                $txtPricePerShare = $esopscoreboardHeader.find('input[name="price_share"]');
                $txtVestingYears = $esopscoreboardHeader.find('input[name="vesting_years"]');
                $txtNoofEmployee = $esopscoreboardHeader.find('input[name="no_of_employee"]');
                $txtTotalSharesTaken = $esopscoreboardHeader.find('input[name="total_shares_taken"]');
                $txtTotalSharesUntaken = $esopscoreboardHeader.find('input[name="total_shares_untaken"]');

                

           /*bind on load data*/
            roxas.esop_scoreboard.bind_events_on_load();

           /*initialize events*/

            var uiDisable = $(".esopselect").find(".frm-custom-dropdown").find(".frm-custom-dropdown-txt").find(".dd-txt").val('ESOP Name');
            uiDisable.attr("readonly", true);

            setTimeout(function() {
                $esopSelectOptions =  $esopSelect.find('div.option.esop');
                $esopSelect.on('click', 'div.option.esop', function() {
                    $esopSelect.attr('esop_id' , $(this).attr('data-value'))
                })
            } , 500)

             
            cr8v.input_numeric($txtTotalShareQty);
            cr8v.input_numeric($txtPricePerShare);
            cr8v.input_numeric($txtVestingYears);
            cr8v.input_numeric($txtNoofEmployee);
            cr8v.input_numeric($txtTotalSharesTaken);
            cr8v.input_numeric($txtTotalSharesUntaken);

            $("#esop_name").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".search-esop").click();
                    }
                });

            $("#vesting_years").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".vesting_years_search").click();
                    }
                });
            $("#total_share_qty").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".total_share_qty_search").click();
                    }
                });
            $("#no_of_employee").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".no_of_employee_search").click();
                    }
                });
            $("#price_share").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".price_search").click();
                    }
                });
            $("#total_shares_taken").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".total_shares_taken_search").click();
                    }
                });
            $("#total_shares_untaken").keyup(function(event){
                    if(event.keyCode == 13){
                        $(".total_shares_untaken_search").click();
                    }
                });

           $esopGenerateButton.on('click' , function() {
        
                 $('tbody.esop').find('tr.no_results_found').remove();

                    var oParams = {
                         'where' : [
                                    {
                                        'field' : 'esopname',
                                        'operator' : '=',
                                        'value' : $txtEsopName.val()
                                    }
                                ]
                            };

                    
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_esop_scoreboard",
                        "beforeSend": function () {
                            if (cr8v.var_check($esopGenerateButton)) {
                                cr8v.show_spinner($esopGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($esopGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $esopBody.removeClass('hidden');
                                $esopContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $esopTemplate, $esopContainer );
                                }
                                roxas.esop_scoreboard.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($esopGenerateButton, false);
                        }
                    }

                    roxas.esop_scoreboard.ajax(oAjaxConfig)
                
               
            })

           $totalsharequantityGenerateButton.on('click' , function() {

                 $('tbody.esop').find('tr.no_results_found').remove();

                    var oParams = {
                         'where' : [
                                    {
                                        'field' : 'esopquantity',
                                        'operator' : '=',
                                        'value' : $txtTotalShareQty.val()
                                    }
                                ]
                            };

                    
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_esop_scoreboard",
                        "beforeSend": function () {
                            if (cr8v.var_check($esopGenerateButton)) {
                                cr8v.show_spinner($esopGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($esopGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $esopBody.removeClass('hidden');
                                $esopContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $esopTemplate, $esopContainer );
                                }
                                roxas.esop_scoreboard.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($esopGenerateButton, false);
                        }
                    }

                    roxas.esop_scoreboard.ajax(oAjaxConfig)
                
               
            })

           $pricepershareGenerateButton.on('click' , function() {

                 $('tbody.esop').find('tr.no_results_found').remove();

                    var oParams = {
                         'where' : [
                                    {
                                        'field' : 'price_per_share',
                                        'operator' : '=',
                                        'value' : $txtPricePerShare.val()
                                    }
                                ]
                            };

                    
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_esop_scoreboard",
                        "beforeSend": function () {
                            if (cr8v.var_check($esopGenerateButton)) {
                                cr8v.show_spinner($esopGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($esopGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $esopBody.removeClass('hidden');
                                $esopContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $esopTemplate, $esopContainer );
                                }
                                roxas.esop_scoreboard.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($esopGenerateButton, false);
                        }
                    }

                    roxas.esop_scoreboard.ajax(oAjaxConfig)
                
               
            })

           $vestingyearsGenerateButton.on('click' , function() {

                 $('tbody.esop').find('tr.no_results_found').remove();

                    var oParams = {
                         'where' : [
                                    {
                                        'field' : 'vesting_years',
                                        'operator' : '=',
                                        'value' : $txtVestingYears.val()
                                    }
                                ]
                            };

                    
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_esop_scoreboard",
                        "beforeSend": function () {
                            if (cr8v.var_check($esopGenerateButton)) {
                                cr8v.show_spinner($esopGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($esopGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $esopBody.removeClass('hidden');
                                $esopContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $esopTemplate, $esopContainer );
                                }
                                roxas.esop_scoreboard.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($esopGenerateButton, false);
                        }
                    }

                    roxas.esop_scoreboard.ajax(oAjaxConfig)
                
               
            })

           $noofemployeesofferedGenerateButton.on('click' , function() {

                 $('tbody.esop').find('tr.no_results_found').remove();

                    var oParams = {
                         'where' : [
                                    {
                                        'field' : 'employee_count',
                                        'operator' : '=',
                                        'value' : $txtNoofEmployee.val()
                                    }
                                ]
                            };

                    
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_esop_scoreboard",
                        "beforeSend": function () {
                            if (cr8v.var_check($esopGenerateButton)) {
                                cr8v.show_spinner($esopGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($esopGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $esopBody.removeClass('hidden');
                                $esopContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $esopTemplate, $esopContainer );
                                }
                                roxas.esop_scoreboard.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($esopGenerateButton, false);
                        }
                    }

                    roxas.esop_scoreboard.ajax(oAjaxConfig)
                
               
            })

           $totalsharestakenGenerateButton.on('click' , function() {

                 $('tbody.esop').find('tr.no_results_found').remove();

                    var oParams = {
                         'where' : [
                                    {
                                        'field' : 'total_share_accepted',
                                        'operator' : '=',
                                        'value' : $txtTotalSharesTaken.val()
                                    }
                                ]
                            };

                    
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_esop_scoreboard",
                        "beforeSend": function () {
                            if (cr8v.var_check($esopGenerateButton)) {
                                cr8v.show_spinner($esopGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($esopGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $esopBody.removeClass('hidden');
                                $esopContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $esopTemplate, $esopContainer );
                                }
                                roxas.esop_scoreboard.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($esopGenerateButton, false);
                        }
                    }

                    roxas.esop_scoreboard.ajax(oAjaxConfig)
                
               
            })

           $totalsharesuntakenGenerateButton.on('click' , function() {

                 $('tbody.esop').find('tr.no_results_found').remove();

                    var oParams = {
                         'where' : [
                                    {
                                        'field' : 'total_share_untaken',
                                        'operator' : '=',
                                        'value' : $txtTotalSharesUntaken.val()
                                    }
                                ]
                            };

                    
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "url"    : roxas.config('url.server.base') + "reports/generate_esop_scoreboard",
                        "beforeSend": function () {
                            if (cr8v.var_check($esopGenerateButton)) {
                                cr8v.show_spinner($esopGenerateButton, true);
                            }
                        },
                        "success" : function(oData) {
                            cr8v.show_spinner($esopGenerateButton, false);

                            if(cr8v.var_check(oData))
                            {
                                $esopBody.removeClass('hidden');
                                $esopContainer.html('');

                                for(var i = 0 ; i < oData.length ; i++ )
                                {
                                    cr8v.populate_data(oData[i] , $esopTemplate, $esopContainer );
                                }
                                roxas.esop_scoreboard.check_results();
                            }

                            // var table;

                            // if ( $.fn.dataTable.isDataTable( '#gratuity' ) ) {
                            //     table = $('#gratuity').DataTable();
                            // }
                            // else {
                            //     table = $('#gratuity').DataTable( {
                            //         paging: false
                            //     } );
                            // }

                            // table.draw();

                            $('.btn_export').removeClass('hidden');

                        },
                        "error" : function() {
                            cr8v.show_spinner($esopGenerateButton, false);
                        }
                    }

                    roxas.esop_scoreboard.ajax(oAjaxConfig)
                
               
            })

           $('.btn_export').on('click', function(){
                 var $clonedTable = $("#esop").clone();
                 $clonedTable.find('[style*="display: none"]').remove();

                 $($clonedTable).table2excel({
                     exclude: ".template",
                     name: "ESOP Scoreboard Report",
                     filename: "ESOP_Scoreboard_Summary_Report"

                });
            });



        },

        /*created custom functions*/
        
        'ajax': function (oAjaxConfig) {
            if (cr8v.var_check(oAjaxConfig)) {
                roxas.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'check_results' : function() {

            if($('tr.esop:visible').length==0){

                var uiNoresults =
                        "<tr class='no_results_found'>"+
                            "<td colspan='7'>No Results found</td>"+
                          "</tr>";

                $esopContainer.append(uiNoresults);
            }

        },

        /**
         * bind_events_on_load
         * @description This function is for binding events depending on the window.locatio.href
         * @dependencies 
         * @param N/A
         * @response N/A
         * @criticality N/A
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        bind_events_on_load : function(){
            if(window.location.href.indexOf('index') > -1)
            {
                localStorage.removeItem('claim_id');
                roxas.esop_scoreboard.get_currency();
                var oGetParams = {
                    "limit" : 999999
                };
                roxas.esop_scoreboard.get(oGetParams, undefined);
                roxas.esop_scoreboard.bind_search_event();
            }
            else if(window.location.href.indexOf('view') > -1)
            {
                var iClaimID = localStorage.getItem('claim_id');
                if(cr8v.var_check(iClaimID) == false)
                {
                   window.location =  roxas.config('url.server.base') + "employee_payment_report/index";     
                }
                var oGetParams = {
                "search_field" : 'id',
                "keyword" : iClaimID,
                "limit" : "1"
                };
                roxas.esop_scoreboard.get_currency();

                roxas.esop_scoreboard.view_get(oGetParams, undefined);
            }

        },

        /**
         * get
         * @description This function is for getting and rendering employee_payment_report on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get : function(oParams, uiThis){
            if(cr8v.var_check(oParams))
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "url"    : roxas.config('url.server.base') + "employee_payment_report/get",
                    "beforeSend": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, true);
                        }
                    },
                    "success": function (oData) {
                        console.log(oData)
                        
                        
                    },
                    "complete": function () {
                        if (cr8v.var_check(uiThis)) {
                            cr8v.show_spinner(uiThis, false);
                        }
                    },
                };

                roxas.esop_scoreboard.ajax(oAjaxConfig);
            }
        },

        /**
         * get_currency
         * @description This function is for getting and rendering currency on load
         * @dependencies N/A
         * @param {object} oParams
         * @param {jQuery} uiThis
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_currency : function(){
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : {'data' : []},
                "url"    : roxas.config('url.server.base') + "esop/get_currency",
                "beforeSend": function () {

                },
                "success": function (oData) {
                    console.log(oData)
                    if(cr8v.var_check(oData))
                    {
                        if(oData.status == true)
                        {
                            oCurrencyName = oData.data;
                        }
                    }
                },
                "complete": function () {

                },
            };

            roxas.esop_scoreboard.ajax(oAjaxConfig);
        },
        
        /**
         * get_currency_name
         * @description This function is for getting currency name based on the object returned of get_currency
         * @dependencies N/A
         * @param {int} iCurrencyID
         * @response N/A
         * @criticality CRITICAL
         * @software_architect N/A
         * @developer Jechonias Alejandro
         * @method_id N/A
         */
        get_currency_name : function(iCurrencyID)
        {
            var sReturn = '';
            if(cr8v.var_check(iCurrencyID))
            {
                for(var i = 0, max = oCurrencyName.length ; i < max; i++)
                {
                    var sCurrencyData = oCurrencyName[i];
                    if(sCurrencyData.id == iCurrencyID)
                    {
                        sReturn = sCurrencyData.name;
                    }
                }
            }
            return sReturn;
        },

        

       
    }

}());

$(window).load(function () {
    roxas.esop_scoreboard.initialize();
});
