
(function(){
	"use strict";

	var iCount = 0,
		iLimit = 0,
		iOffset = 0;

	CPlatform.prototype.notifications = {

		initialize : function(){

			if(window.location.href.indexOf('auth') > -1)
            {
               
            }else{
              	roxas.notifications.triggerNotifications();

        		roxas.notifications.get_user_notif();
            }

		},
		triggerNotifications : function()
		{
			var btnDisplayNotificaitons = $("#display-notificaitons"),
				uiNotifContainer = btnDisplayNotificaitons.find(".pop-up.notifications-holder");
			
				btnDisplayNotificaitons.off("click.shownotif").on("click.shownotif", function(e){

					if(uiNotifContainer.css('display') == 'none')
					{
						uiNotifContainer.css({"display":"block"});
					}else{
						uiNotifContainer.css({"display":"none"});
					}

					// uiNotifContainer.toggle();
					e.stopPropagation();

					$(document).click(function() {
					 	$(".pop-up.notifications-holder").css({"display":"none"});
					});
				})
		},
		selectNotificationView : function()
		{
			var uiDisplayNotificaitons = $(".notif-items");

			uiDisplayNotificaitons.off("click.showNotifModal").on("click.showNotifModal", function(){
				var uiThis = $(this),
					uiParent = uiThis.closest(".contents"),
					oItemData = uiParent.data();
					
					// 	console.log(JSON.stringify(oItemData));

					if(oItemData.is_viewed == 0)
					{
						roxas.notifications.saveUserNotifIDs(oItemData.notif_id);

						iCount = iCount - 1;

						roxas.notifications.displayNotifCounts(iCount);
					}

					uiParent.css({"background-color":"#fff"});
					uiParent.data("is_viewed", "1");


					var uiNoticationViewContainerModal =  $("#notication-view-container-modal"),
						uiModalBody = uiNoticationViewContainerModal.find(".modal-body"),
						uiNoticationViewContainerModalBody = uiNoticationViewContainerModal.find("#notication-view-container-modal-body");


						uiModalBody.css({"animation":"none !important"});


				$("body").css({overflow:'hidden'});



				$("#notication-view-container-modal").addClass("showed");

				

				uiNoticationViewContainerModalBody.addClass("animated zoomIn");

				$("#notication-view-container-modal .close-me").on("click",function(){
					$("body").css({'overflow-y':'initial'});
					$("#notication-view-container-modal").removeClass("showed");
				});



				roxas.notifications.displaySelectedNotifRecords(oItemData);


			});
		},
		get_user_notif : function()
		{
			var oAjaxConfig = {
	            "type"   : "GET",
	            "data"   : {"none":"none"},
	            "url"    : roxas.config('url.server.base') + "auth/get_user_notif",
	            "success": function (oData) {

	                roxas.notifications.displayNotifRecords(oData.data);
	                
	            }
	           
	        };

	       roxas.CconnectionDetector.ajax(oAjaxConfig);

		},
		displayNotifRecords : function(oData)
		{
			var uiDisplayNotifRecords = $(".display-notif-records"),
				uiTemplate = $(".display-notif-items");



				uiDisplayNotifRecords.html("");

				//console.log(JSON.stringify(oData["notif_info"]));

				iCount = oData["notif_count"];

				roxas.notifications.displayNotifCounts(iCount);

				roxas.notifications.displayNotificationLinkMore(oData["count_all_records"]);

				// 	console.log(JSON.stringify(oData["notif_info"]));
				
				for(var x in oData["notif_info"])
				{
					var list = oData["notif_info"][x],
						uiCloneTemplate = uiTemplate.clone(),
						sDescription = "",
						sValue = list.value;
						
						if(sValue.length > 60)
						{
							sDescription = list.value.substring(0, 60)+"...";
						}else{
							sDescription = list.value;
						}

						uiCloneTemplate.removeClass("display-notif-items").show();
						uiCloneTemplate.find(".display-notification-user-image").attr("src", roxas.config('url.server.base')+list.image);
						uiCloneTemplate.find(".display-notification-user").html(list.user_fullname);
						uiCloneTemplate.find(".display-notification-note").html(list.action_note);
						uiCloneTemplate.find(".display-notification-description").html(sDescription);
						uiCloneTemplate.find(".display-notification-date-created").html(list.date_created);
						uiCloneTemplate.attr("notif-id", list.id);
						uiCloneTemplate.data(list);

						if(list.is_viewed == 0)
						{
							uiCloneTemplate.css({"background-color": "#F3F3F3"});
						}
						uiDisplayNotifRecords.append(uiCloneTemplate);

						roxas.notifications.selectNotificationView();
				}

		},
		displaySelectedNotifRecords : function(oData)
		{
	
			var uiDisplaySelectedNotifUserImg = $("#display-selected-notif-user-img"),
				uiDisplaySelectedNotifUser = $("#display-selected-notif-user"),
				uiDisplaySelectedNotifDateCreated = $("#display-selected-notif-date-created"),
				uiDisplaySelectedNotifDescription = $("#display-selected-notif-description"),
				uiDisplaySelectedNotifNote = $("#display-selected-notif-note");

				uiDisplaySelectedNotifUserImg.attr("src", roxas.config('url.server.base')+oData.image);
				uiDisplaySelectedNotifUser.html(oData.user_fullname);
				uiDisplaySelectedNotifDateCreated.html(oData.date_created);
				uiDisplaySelectedNotifDescription.html(oData.value);
				uiDisplaySelectedNotifNote.html(oData.action_note);
			
		},
		saveUserNotifIDs : function(notif_id, user_id)
		{
			var oAjaxConfig = {
				type : "POST",
				url : roxas.config('url.server.base') + "auth/save_user_notif_ids",
				data : {"notif_id":notif_id, "user_id":user_id},
				success : function(oResponse){
					
				}

			};
			roxas.CconnectionDetector.ajax(oAjaxConfig);
		},
		displayNotifCounts : function(iCount)
		{
			var uiNotifCounts = $(".notif-counts"),
				uiNotifNumbers = $(".notif-numbers");

				if(iCount > 0)
				{
					uiNotifCounts.css({"display":"block"});
					uiNotifNumbers.html(iCount);

				}else{
					uiNotifCounts.css({"display":"none"});
					uiNotifNumbers.html("0");
				}
		},
		displayNotificationLinkMore : function(icountAll)
		{
			var btnDisplayNotifViewMore = $("#display-notif-view-more");

			if(icountAll > 5)
			{
				btnDisplayNotifViewMore.css({"display":"block"});

				roxas.notifications.triggerNotifLoadMore();

			}
		},
		triggerNotifLoadMore : function()
		{
			var btnDisplayNotifViewMore = $("#display-notif-view-more"),
				uiDisplayNotificaitons = $("#display-notificaitons"),
				uiNotifContainer = uiDisplayNotificaitons.find(".pop-up.notifications-holder");

			btnDisplayNotifViewMore.off("click.loadMore").on("click.loadMore", function(){


				if(uiNotifContainer.css('display') == 'none')
					{
						uiNotifContainer.css({"display":"block"});
					}else{
						uiNotifContainer.css({"display":"none"});
					}

				iLimit = iLimit + 5;
				iOffset = iOffset + 5;

				roxas.notifications.addMoreNotif(iLimit, iOffset);
			});
		},
		addMoreNotif : function(iLimit, iOffset)
		{
			var uiDisplayNotifViewMore = $("#display-notif-view-more");
			
			var oAjaxConfig  = {
				type : "POST",
				url : roxas.config('url.server.base') + "auth/add_more_notif",
				data : {"limit":iLimit, "offset":iOffset},
				beforeSend : function(){
					uiDisplayNotifViewMore.html('<i class="fa fa-spinner fa-spin fa-3x" style="color: #000; border: 0px !important; font-size: 5px !mportant;"></i>');
				},
				success : function(oResponse){
					// 	console.log(JSON.stringify(oResponse.data));
					roxas.notifications.displayMoreNotifLinks(oResponse.data);

					if(oResponse.data.notif_info.length == 0)
					{
						uiDisplayNotifViewMore.remove();
					}else{

						uiDisplayNotifViewMore.html('View More');
					}
				},
				complete : function(){
					
				}

			};
			roxas.CconnectionDetector.ajax(oAjaxConfig);
		},
		displayMoreNotifLinks : function(oData)
		{
			var uiDisplayNotifRecords = $(".display-notif-records");

			for(var x in oData["notif_info"])
			{
				var list = oData["notif_info"][x],
					sDescription = "",
					sValue = list.value;
						
					if(sValue.length > 60)
					{
						sDescription = list.value.substring(0, 60)+"...";
					}else{
						sDescription = list.value;
					}

				var sHtml = '<div class="contents notif-items">'+
		                    '    <a class="notication-view-modal" style="width: 100%;">'+
		                    '        <img class="profile img-responsive display-notification-user-image" src="'+roxas.config('url.server.base')+list.image+'">'+
		                    '        <p  style="width: 70%;">'+
		                    '            <strong class="display-notification-user">'+list.user_fullname+'</strong> posted '+
		                    '            <strong class="display-notification-note">'+list.action_note+'</strong>: '+
		                    '			<span class="display-notification-description"></span>'+                                      
		                    '        </p>'+
		                    '       <p class="time-of-post display-notification-date-created">'+list.date_created+'</p>'+
		                    '    </a>'+
		                    '</div>';

	                var uiTemplate = $(sHtml);

	                uiTemplate.find(".display-notification-description").html(sDescription);

	                uiTemplate.attr("notif-id", list.id);
					uiTemplate.data(list);

	                    if(list.is_viewed == 0)
						{
							uiTemplate.css({"background-color": "#F3F3F3"});
						}

	           	 uiDisplayNotifRecords.append(uiTemplate);

	           	 roxas.notifications.selectNotificationView();

			}

		}

	}

})();

$(window).ready(function(){
	roxas.notifications.initialize();
});



