<?php include "../construct/header.php"; ?>
<!-- for navigation  -->
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>						
						<li><a href="../employee/personal-stocks.php">Personal Stocks</a></li>
						<li><a href="../employee/stock-offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>		
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT	
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="profile-page.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="stock-offer.php">Stock Offers</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>ESOP 1</a>				
			</div>			
			<div class="clear"></div>
		</div>


		<div class="margin-top-30 margin-bottom-15">
			<h2 class="f-left margin-top-0 margin-bottom-0">Offer Letter</h2>
		
			<div class="f-right">
				<button class="btn-normal margin-right-10">Print Offer</button>
				<button class="btn-normal modal-trigger" modal-target="accept-offer">Accept Offer</button>
			</div>
			<div class="clear"></div>
		</div>
			
	</div>
</section>

<section section-style="content-panel">

	<div class="content mother-container">

		<div class="main-container">

			<div class="sub-container">			
				<div class="head-center">
					<a href="view-offer.php">
						<i class="fa fa-angle-left"></i>
					</a>
					<p>Page</p>
					<a href="view-offer.php"><p>1</p></a>
					<p class="white-color">/</p>
					<p class="black-color">2</p>
					<i class="fa fa-angle-right"></i>
				</div>				
				<div class="head-right">
					<i class="fa fa-search-plus"></i>
					<i class="fa fa-search-minus"></i>
				</div>			
				<div class="clear"></div>
			</div>
			<div class="sub-container1">
				<div class="inside-sub big-paper">						
					
					<p class="margin-top-10 font-bold">6. Disposition of Dividends</p>
					<p class="margin-left-20 margin-top-10">a. Cash Dividends</p>
					<p class="margin-left-20">Any cash dividends declared and paid out by the Company will be 
					applied against your subscription.</p>
					<p class="margin-left-20 margin-top-10">b. Stock Dividends</p>
					<p class="margin-left-20">In case of stock dividend declaration, the corresponding stock 
					dividends payable on the shares shall be withheld until your full 
					payment of the shares subscribed</p>
					
					
					
					
					<p class="margin-top-10 font-bold">7. Withdrawals from the Plan.</p>
					<p class="margin-left-20 margin-top-10">In case of retirement, resignation or dismissal other 
					than for just cause, you may exercise any Option that you have accepted within fifteen (15) 
					trading days from the date of retirement, separation or dismissal and pay the Subscription Price 
					in full. Transfer of employment within the Group shall not be deemed separation from employment.</p>
					
					<p class="margin-left-20 margin-top-10">In case of dismissal for a just cause, you shall, on the date of dismissal, 
					lose the right to exercise any unexercised Option.</p>

					<p class="margin-left-20 margin-top-10">In case of disability which renders you unfit to perform your functions, 
					you may exercise any Option that you have accepted and pay the Subscription Price no later than six 
					(6) months therefrom (extendible for a maximum period of one (1) year at the determination of the 
					Administrator).</p>

					<p class="margin-top-10 font-bold">8. Company Option to Repurchase</p>
					<p class="margin-left-20 margin-top-10">The Company has the option to repurchase all or a portion of the shares 
					issued or transferred to you pursuant to this Plan upon the cessation of your employment from the Group. The repurchase
					 price shall be i) the cash that have paid and/or the amount equivalent to the stock purchase gratuity applied to 
					 your subscription price if the shares are not yet fully paid at the time of repurchase or ii) the market price of the 
					 shares if already fully paid.
					</p>

					<p class="margin-top-30">Should you accept this offer, kindly signify your acceptance by accomplishing the attached Letter of Acceptance and 
					submit the same to the Human Resource Department (HRD) within thirty (30) days from your receipt of this letter together 
					with the sum of Ten Pesos (Php10.00) as acceptance fee.</p>
					
					<div class="address-paper">
						<p>6/F Cacho-Gonzales Bldg., 101 Aguirre Street, Legaspi Village, Makati City 1229 Philippines</p>
						<p>Trunk Lines: (632)810 8901 to 06 Fax No.: (632) 8171875</p>
					</div>
					
					<div class="f-right margin-top-50 margin-right-10">
						<p>Very truly yours,</p>
						<p class="margin-top-10">_____________________________</p>
						<p>RAMON M. DE LEON</p>
						<p>SVP, Human Resources &amp ESOP Administrator</p>
					</div>
				</div>
			</div>
		</div>

	</div>

</section>

<!-- Accept Offer -->
<div class="modal-container" modal-id="accept-offer">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ACCEPT OFFER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<div class="">

				<div class="error margin-bottom-10">Quantity is invalid. <br /> Please type numbers only in the textbox</div>
				<p class="font-18 margin-bottom-10"	>I will accept the offer and take:</p>

				<div class="margin-left-30">
					<input type="radio" name="offer" id="all" />
					<label for="all" class="black-color txt-normal font-15 margin-left-10">All of the Shares Offered</label>				
				</div>

				<div class="margin-left-30 margin-top-5">
					<input type="radio" name="offer" id="only" />
					<label for="only" class="black-color txt-normal font-15 margin-left-10">
						Only <input type="text" class="small width-100px margin-right-5 margin-left-5 add-border-radius-5px" />Shares from Shares Offered
					</label>				
				</div>													
			</div>	
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			<a href="accept-offer.php">
				<button type="button" class="display-inline-mid btn-normal">View Acceptance Letter</button>
			</a>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>