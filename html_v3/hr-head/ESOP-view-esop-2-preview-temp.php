<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="ESOP-esop.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="ESOp-view-esop-2.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>Share Distribution Template</a>
			</div>
			<div class="f-right">
				<a href="ESOP-view-esop-2.php">
					<button class="btn-normal margin-right-10">Cancel Upload</button>
				</a>
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="upload-template">Upload Another Template</button>
				<a href="ESOP-view-esop-3.php">
					<button class="btn-normal">Confirm Upload</button>				
				</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	
	<div class="content">
	
		<h2 class="f-left">Preview of ESOP_Payment.xls</h2>
		<h2 class="f-right">Granted: July 29, 2015</h2>
		<div class="clear"></div>

		<div class="big-tbl-cont">
			<div class="tbl-rounded ">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>No.</th>
						<th>Company Code</th>
						<th>Department Code</th>						
						<th>Department Name</th>						
						<th>Employee Code</th>						
						<th>Employee Name</th>						
						<th>Alloted Shares</th>												
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>						
					</tr>
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
					</tr>			
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
					</tr>		
				</tbody>
			</table>
		</div>
		</div>

		<h2 class="margin-top-30">Error on the Document</h2>
		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Row Number</th>
						<th>Column Name</th>
						<th>Issue</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Employee Name, Employee Code</td>
						<td>Employee Name does not match code</td>						
					</tr>
					<tr>
						<td>2</td>
						<td>Department Name, Employee Code</td>
						<td>Department Name does not match code</td>
					</tr>					
				</tbody>
			</table>
		</div>

		

	<div>
</section>


<!-- share distribution template -->
<div class="modal-container" modal-id="upload-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	
			<div class="margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>	
			
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
			
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>