<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="ESOP-CLAIM-hr.php">ESOP Claim</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Jose Protacio</a>				
			</div>			
			<div class="clear"></div>
		</div>
		<h1 class="f-left">View Employee Claim</h1>
		<div class="f-right">			
			<button class="btn-normal display-inline-mid modal-trigger" modal-target="send-claim">Send Claim to ESOP Admin</button>
		</div>
		<div class="clear"></div>
	</div>
</section>

<section section-style="content-panel">	
	

	<div class="content print-statement">


		<div class="user-profile margin-bottom-30">
			<div class="margin-bottom-20 margin-top-50">
				<h2><abbr title="Employee Stock Option Plan">ESOP</abbr> 1 Statement of Account</h2>
			</div>
			<div class="user-name">
				<p class="name">Joselito Salazar</p>
				<p class="position">Employee No: <span>132</span></p>
			</div>
			<div class="user-info">
				<table>
					<tbody>
						<tr>
							<td>Company</td>
							<td>ROXOL</td>
						</tr>
						<tr>
							<td>Department</td>
							<td>ISD</td>
						</tr>
						<tr>
							<td>Rank:</td>
							<td>Officer I</td>
						</tr>
						<tr>
							<td>Separation Status: </td>
							<td>N/A</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="option-box">
			<p class="title">Grant Started</p>
			<p class="description">August 31, 2013</p>
		</div>
		<div class="option-box">
			<p class="title">Subscription Price</p>
			<p class="description">Php 6.00 </p>
		</div>
		<div class="option-box">
			<p class="title">No. of Shares Availed</p>
			<p class="description">130,144</p>
		</div>
		<div class="option-box">
			<p class="title">Value of Shares Availed</p>
			<p class="description">Php 324,058.56 </p>
		</div>

		<h2 class="margin-top-50">Vesting Rights</h2>
		
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>No.</th>
						<th>Year</th>
						<th>Percentage</th>
						<th>No. of Shares</th>
						<th>Value of Shares</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Sept. 01, 2013</td>
						<td>20%</td>
						<td>26,028</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content">					
						<td colspan="2"></td>
						<td class="combine"><span class="total-text">Total:</span> 100%</td>
						<td>130,144</td>
						<td>Php 324,058.56 </td>
					</tr>
			</table>
		</tbody>

		<h2 class="margin-top-50">Summary</h2>

		<div class="long-panel border-10px">
			<p class="first-text">Amount Shares Taken 130,144 | @ 2.40</p>
			<p class="second-text margin-right-100">Php 324, 058.56 </p>
			<div class="clear"></div>
		</div>

		<div class="tbl-runded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>No.</th>
						<th>Payment Details</th>
						<th>Shares</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>06</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>-</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="4" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>

		<h2 class="margin-top-50">Payment Application</h2>

		<div class="long-panel border-10px">
			<p class="first-text">Year 2</p>
			<p class="first-text margin-left-30" ;="">July 28, 2015</p>
			<p class="second-text margin-right-80">With Vesting Rights</p>
			<div class="clear"></div>
		</div>

		<div class="long-panel border-10px margin-top-20">
			<p class="first-text">Amount Shares Taken 130,144 | @ 2.40</p>
			<p class="second-text margin-right-80">Php 324,058.56</p>
			<div class="clear"></div>
		</div>

		<div class="tbl-runded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>No.</th>
						<th>Payment Details</th>
						<th>Shares</th>
						<th>Date Paid</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Gratuity</td>
						<td>06</td>
						<td>October 2, 2013</td>
						<td>18,399.36 Php</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Profit Share</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>10,590.72 Php</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Cash</td>
						<td>-</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Credit Card</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Check</td>
						<td>12</td>
						<td>October 2, 2013</td>
						<td>64,809.72 Php</td>
					</tr>
					<tr class="last-content ">
						<td colspan="4" class="text-left">
							<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
							<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
						</td>
						<td>
							<p class="font-15 padding-bottom-5">39,001.35 Php</p>
							<p class="font-15">285,057.21 Php</p>
						</td>					
					</tr>
				</tbody>
			</table>
		</div>

		
		<div class="emp-claim-cont margin-top-40">
			<div class="emp-form ">
				<p class="title-form">Employee Claim Form</p>
				<table class="emp-table">
					<tbody>
						<tr>
							<td>ESOP Name:</td>
							<td>ESOP 1</td>
							<td>Number of Matured Stocks:</td>
							<td><strong>26,500 Stocks</strong></td>
						</tr>
					</tbody>
				</table>
				<div class="numbers">
					<p class="f-left">Number of Stocks to be claimed:</p>
					<p class="f-right">26,500 Shares</p>
					<div class="clear"></div>
				</div>
				<p>Furthermore, this is to request for the issuance of the aboce number of stock certificates under my name.</p>
				<div class="complex-info">
					<div>
						<table>
							<tbody>
								<tr>
									<td>
										<p class="white-color">xx</p>
										<div class="line"></div>
										<p>Employee's Name and Signature</p>
									</td>
									<td>
										<p>September 21, 2014</p>
										<div class="line"></div>
										<p>Date</p>
									</td>
								</tr>
							</tbody>
						</table>					
					</div>
				</div>

				<div class="divider"></div>
				<p class="authorized">Authorization of Claim</p>
				<p class="long-text">This is to accept and validate the Mr. / Ms. <strong>Joselito Salazar</strong> has execised his / 
					her stock option vested rights, over <strong>26,500</strong> number of shares under <strong><abbr title="
					Employee Stock Option Plan">ESOP</abbr> 1</strong>.
					In this regard, the Stock Transfer Agent shall issue the Stocks Certificate as declared on this portion.</p>

				<p class="approved">Approved by:</p>
				<div class="incharge">
					<p><abbr title="Employee Stock Option Plan">ESOP</abbr> Administrator</p>
				</div>
			</div>
		</div>
	<div>
</section>

<!-- send claim  -->
<div class="modal-container" modal-id="send-claim">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SEND CLAIM TO ESOP ADMIN</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">	
			<div class="error">Amount Paid is Invalid. <br />Please type numbers only in the textbox.</div>	
	
			<table class="send-claim margin-top-10 ">
				<tbody>
					<tr>
						<td class="width-200px">
							<p>ESOP NAME</p>
						</td>
						<td>
							<p>ESOP 1</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>Vesting Rights</p>
						</td>
						<td>
							<p class="">Year 1</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>OR Number:</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-300px" />
						</td>
					</tr>	
					<tr>
						<td>Amount Paid:</td>
						<td>
							<input type="text" class="small add-border-radius-5px" />
							<div class="select width-100px add-radius">
								<select>
									<option value="Php">PHP</option>
									<option value="usd">USD</option>
									<option value="cad">CAD</option>
									<option value="hkd">HKD</option>
									<option value="inr">INR</option>
								</select>
							</div> 
						</td>
					</tr>			
					<tr>
						<td>
							<p>Date of OR:</p>
						</td>
						<td>
							<div class="date-picker display-inline-mid add-radius ">
								<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>					
				</tbody>
			</table>		

			<p class="margin-top-20 font-bold">PLEASE INDICATE THE FILE NAME OF YOUR ATTACHMENT:</p>
			<p class="margin-bottom-20 margin-top-15 display-inline-mid">File Name: <span><em>No file uploaded yet</em></span></p>
			<p class="display-inline-mid"><a href="#" class=" margin-left-30">Upload File</a></p>
			<div class="bg-modal padding-20px margin-top-20">
				<i class="fa fa-file-pdf-o fa-3x display-inline-mid"></i>
				<p class="display-inline-mid margin-left-10">ESOP Payment OR.pdf</p>
				<a href="#" class="red-color f-right margin-top-10">Remove File</a>
				<div class="clear"></div>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal">Add Claim</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>