<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Custom Reports</h1>
			
			<div class="clear"></div>
		</div>
		
		<div>
			<div class="display-inline-mid">
				<p class="white-color margin-bottom-5">Name Type:</p>

				<div class="display-inline-mid">
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="Employee Name">Employee Name</option>
							<option value="ESOP Name">ESOP Name</option>
							
						</select>
					</div>				
				</div>
			</div>
			<div class="display-inline-mid ">
				<p class="white-color margin-bottom-5 margin-left-20">Please Indicate Date Range</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10 ">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Generate Report</button>
				</div>
			</div>
		</div>			

	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">
		<div class="check-cont f-left">
			<p class="font-20 white-color ">Please Indicate Reports Column</p>
			<p class="white-color margin-top-10"><em>ESOP Details: </em></p>
			
			<table class="report-claim-tbl">
				<tbody>
					<tr>
						<td>							
							<label>
								<input type="checkbox" name="stocks">
									<span >Grant Started</span>
							</label>												
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Subsription Price</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Total Gratuity Grandted</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Average Gratuity Granted</span>
							</label>	
						</td>
					</tr>
					<tr>
						<td>							
							<label>
								<input type="checkbox" name="stocks">
									<span >Value of Shares Availed</span>
							</label>												
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >No. of Shares Offered</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >No. of Shares Re-offered</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >No. of Shares Claimed</span>
							</label>	
						</td>
					</tr>
					<tr>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Employees who Availed</span>
							</label>
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Employees who did not Avail</span>
							</label>
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Expiry Date of Offer</span>
							</label>
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Vesting Years</span>
							</label>
						</td>
					</tr>
					<tr>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Date Gratuity Added</span>
							</label>
						</td>
					</tr>
				</tbody>
			</table>

			<p class="white-color"><em>Employee Details:</em></p>

			<div class="display-inline-mid  margin-left-10 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span class="margin-left-10">Employee Number</span>
				</label>												
			</div>

			<div class="display-inline-mid  margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span class="margin-left-10">Email</span>
				</label>												
			</div>
			
			<div class="display-inline-mid  margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span >Username</span>
				</label>												
			</div>
			
			<div class="display-inline-mid  margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span >Company</span>
				</label>												
			</div>
			
			<div class="display-inline-mid margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span >Rank</span>
				</label>												
			</div>
		
		</div>

		<div class="list-cont f-left">
			<p class="font-20 white-color ">Please Indicate Reports Column</p>
			<div class="margin-top-10">
				<div class="f-right">
					<i class="fa fa-arrow-up font-20 margin-right-5 hover-icon"></i>
					<i class="fa fa-arrow-down font-20  hover-icon"></i>
				</div>
				<div class="clear"></div>
				<div class="item-limit-container">
					<ul>
						<li>1. Date Gratuity Added</li>
						<li>2. ESOP Name</li>
						<li>3. Subscription Price</li>
						<li>4. Total Gratuity Granted</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>	

		<div class="text-right-line margin-top-10">
			<div class="line"></div>
		</div>

		<div class="header-effect margin-top-50">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="Date Added">Date Gratuity Added</option>
							<option value="Esop Name">ESOP Name</option>
							<option value="Subscription Price">Subscription Price</option>
							<option value="Total Gratuity">Total Gratuity Granted</option>
						</select>
					</div>

					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px">
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px">
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

				</div>
			</div>

			<div class="display-inline-mid added-grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Date Gratuity Added</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>


			<div class="display-inline-mid total-grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Total Gratuity Granted</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<p class="white-color margin-bottom-5 margin-left-20">Price</p>
				
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
		<div class="f-right margin-top-50 margin-right-10">
			<i class="fa fa-print fa-2x hover-icon"></i>
		</div>
		<div class="clear"></div>
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>Date Gratuity Report</th>
						<th>ESOP Name</th>
						<th>Subscription Price</th>
						<th>Total Gratuity Granted</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>September 10, 2015</td>
						<td>ESOP 1</td>
						<td>0.60 Php per Share</td>
						<td>1,300,215.15 Php</td>
					</tr>
					<tr>
						<td>September 10, 2015</td>
						<td>ESOP 1</td>
						<td>0.60 Php per Share</td>
						<td>1,300,215.15 Php</td>
					</tr>
					
					<tr class="last-content ">						
						<td colspan="4" class="text-right">										
							<p class="display-inline-mid">Total Payments:</p>							
							<p class="font-15 display-inline-mid margin-left-30 margin-right-50">2,600,430.3 Php</p>																			
													
						</td>					
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>