<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left">Add User</h1>
			<div class="f-right">
				<a href="SETTINGS-user-list.php">
					<button type="button" class="display-inline-mid btn-cancel color-cancel">Cancel</button>
				</a>
				
				<button type="button" class="display-inline-mid btn-normal margin-left-10">Add User</button>			
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="upload-photo-ver2 ">
			<i class="fa fa-camera"></i>
			<p>Upload Photo</p>
		</div>

		<table class="add-user-table margin-top-50">
			<tbody>
				<tr>
					<td>EMPLOYEE CODE:</td>
					<td><input type="text" class="normal width-300px add-border-radius-5px" /></td>
					<td>FIRST NAME:</td>
					<td><input type="text" class="normal width-300px add-border-radius-5px" /></td>
				</tr>
				<tr>
					<td>COMPANY</td>
					<td>
						<div class="select width-300px add-radius">
							<select>
								<option value="op1">Company A</option>
								<option value="op2">Company B</option>
							</select>
						</div>
					</td>
					<td>LAST NAME:</td>
					<td><input type="text" class="normal width-300px add-border-radius-5px" /></td>
				</tr>
				<tr>
					<td>DEPARTMENT:</td>
					<td>
						<div class="select width-300px add-radius">
							<select>
								<option value="dept1">Department A</option>
								<option value="dept2">Department B</option>
							</select>
						</div>
					</td>
					<td>PASSWORD:</td>
					<td>
						<div class="password-input">
							<input type="password" class="normal width-300px add-border-radius-5px" />
							<i class="fa fa-eye black-color"></i>
						</div>
					</td>
				</tr>
				<tr>
					<td>RANK:</td>
					<td>
						<div class="select width-300px add-radius">
							<select>
								<option value="ranka">Rank A</option>
								<option value="rankb">Rank B</option>
							</select>
						</div>
					</td>
					<td>CONTACT NO:</td>
					<td>
						<div class="select width-100px add-radius">
							<select>
								<option value="op1">Mobile</option>
								<option value="op2">House</option>
								<option value="op3">Company</option>
							</select>
						</div>
						<input type="text" class="normal display-inline-mid width-197px add-border-radius-5px" />
					</td>
				</tr>
				<tr>
					<td>USER ROLE:</td>
					<td>
						<div class="select width-300px add-radius">
							<select>
								<option value="usera">User Role A</option>
								<option value="userb">User Role B</option>
							</select>
						</div>
					</td>
					<td>EMAIL ADDRESS:</td>
					<td><input type="text" class="normal width-300px add-border-radius-5px" />
						
					</td>
				</tr>
			</tbody>
		</table>

	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>