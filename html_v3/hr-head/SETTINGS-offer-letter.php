<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left ">Payment Method List</h1>
			<h2 class="f-left hidden">ESOP View</h2>

			<div class="f-right">
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="add-letter">Add Letter</button>
				<a href="SETTINGS-letter-archives.php">
					<button class="btn-normal ">Letter Archives</button>
				</a>
			</div>
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10 add-radius margin-top-5">
							<select>
								<option value="op1">Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid add-border-radius-5px margin-top-5" />
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-5">Search</button>
					</td>
				</tr>
			<tbody>
		</table>		
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="text-right-line margin-bottom-50">
			<div class="line"></div>
		</div>
		
		<div class="clear"></div>
	

		<div class="panel-group text-left margin-top-30">

			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Offer Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>	
					<p class="f-right white-color font-16">4 Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">		

					<div class="panel-body padding-15px">
						
						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

					</div>		
				</div>
			</div>


			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Acceptance Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>	
					<p class="white-color font-16 f-right">10 Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
					<div class="panel-body padding-15px">
						
						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="SETTINGS-view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

					</div>	
				</div>
			</div>
		</div>
	<div>

	


</section>


<!-- add letter  -->
<div class="modal-container edit-letter" modal-id="add-letter">
	<div class="modal-body max-width-1200 width-1200px ">
		<div class="modal-head ">
			<h4 class="text-left">ADD LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content ">	

			<div class="head">
				<div class="display-inline-mid width-80percent">
					<p class="margin-bottom-5">Title</p>
					<input type="text" class="normal width-100percent add-border-radius-5px" value="Offer Letter v.1 for ROXOL" />
				</div>
				<div class="display-inline-mid margin-left-20">
					<p class="margin-bottom-5">Letter Type:</p>
					<div class="select add-radius">
						<select>
							<option value="op1">Offer Letter A</option>
							<option value="op2">Offer Letter B</option>
							<option value="op3">Offer Letter B</option>
						</select>
					</div>
				</div>
			</div>

			<div class="big-header">
				<p class="f-left margin-left-10 white-color">Date Created: October 30, 2015</p>
				<p class="f-right margin-right-20 white-color">Created By: Joselito Salazar</p>
				<div class="clear"></div>
			</div>

			<div class="big-body">

				<div class="function">
					<!-- left - side -->
					<div class="format f-left  ">				
						<div class="text-files">
							<textarea name="editor1" id="editor1" rows="10" cols="80"></textarea>
						</div>
					</div>							
				</div>

				<div class="letter-nav ">
					<!-- right side  -->

					<div class="tag  ">
						<p class="text-center">TAGS</p>
					</div>					
					
					<div class="menu hover-letter">
						<ul>
							<li>ESOP Name</li>
							<li>Offer Shares</li>
							<li>Price / Share</li>
							<li>Total Value of Share</li>
							<li>Vesting Years</li>
							<li>Date of Letter</li>
							<li>Sender Name</li>	
							<li>Sender Rank</li>						
							<li>Sender Department</li>
							<li>Recipient Name</li>
							<li>Recipient Rank</li>
							<li>Recipient Department</li>								
							<li></li>								
						</ul>
					</div>

					<div class="clear"></div>

				</div>

			</div>
				
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			
			<button type="button" class="display-inline-mid btn-normal alert-btn">Submit</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>