<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="STOCK-OFFER.php">Stock Offers</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>				
			</div>			
			<div class="clear"></div>
		</div>
		<div class="f-right margin-top-10 margin-bottom-10">
			<button class="btn-normal margin-right-5">Print Offer</button>
			<button class="btn-normal modal-trigger" modal-target="accept-offer">Accept Offer</button>
		</div>
		<div class="clear"></div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content mother-container">

		<div class="main-container">

			<div class="sub-container">			
				<div class="head-center">
					<i class="fa fa-angle-left"></i>
					<p>Page</p>
					<p class="black-color">1</p>
					<p class="white-color">/</p>
					<a href="STOCK-OFFER-view-offer1.php">
						<p>2</p>
					</a>
					<a href="STOCK-OFFER-view-offer1.php">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>				
				<div class="head-right">
					<i class="fa fa-search-plus"></i>
					<i class="fa fa-search-minus"></i>
				</div>			
				<div class="clear"></div>
			</div>
			<div class="sub-container1">
				<div class="inside-sub big-paper">						
					<p class="f-right">September 10, 2015</p>
					<div class="clear"></div>
					<div>
						<p>Jessie Rex Caringal</p>
						<p>Auditor</p>
						<p>Internal Audit</p>
					</div>
					<p class="margin-top-10">Dear Joselito Salazar,</p>

					<p class="margin-top-10">In accordance with the terms of the Employee Stock Option Plan 
					(ESOP) of Roxas Holdins, Inc. (RHI), which was approved by the Securities and Exchange 
					Commision (SEC) on April 30, 2014, the company offers you an option to subscribe and 
					purchase 133,000 shares of RHI which have been allocation to you over the exercise period
					 of Five (5) years commencing from the date of this letter offer under the following conditions:
					</p>

					<p class="margin-top-10 font-bold">1. Subscription Price &amp; Terms.</p>
					<p class="margin-left-20 margin-top-10">The subscription price is based on the average price of the RHI shares in the Philippine 
					Stock Exchange for thirty (30) trading days prior to the date of this option letter 
					discounted at the rate of fifteen percent (15%) and payable within a period of Five (5) 
					years without interest. In view thereof, the subscription price is Php 6.00 / Share.</p>

					<p class="margin-top-10 font-bold">2. Vesting of Option</p>
					<p class="margin-left-20 margin-top-10">The option will vest as follows:</p>
					
					<table class="table-offer margin-top-10">
						<thead>
							<tr>
								<th>Vesting Date</th>
								<th>Vesting Option for</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>At the end of (1) year from the date of the option offer letter</td>
								<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
							</tr>
							<tr>
								<td>At the end of (2) year from the date of the option offer letter</td>
								<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
							</tr>
							<tr>
								<td>At the end of (3) year from the date of the option offer letter</td>
								<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
							</tr>
							<tr>
								<td>At the end of (4) year from the date of the option offer letter</td>
								<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
							</tr>

							<tr>
								<td>At the end of (5) year from the date of the option offer letter</td>
								<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
							</tr>
						</tbody>
					</table>
					
					<p class="margin-top-10 font-bold">3. Exercise of the Option.</p>
					<p class="margin-left-20 margin-top-10">You may exercise, in whole or in part, the option 
					for any given year at any time within the five (5) year term of the Plan.</p>
					<p>

					<p class="margin-top-10 font-bold">4. Option-Linked Annual Stock Purchase Gratuity.</p>
					<p class="margin-left-20 margin-top-10">During the five (5) year period commencing from the date 
					of this offer and, as an additional incentive to you for accepting the offer, the company shall 
					grant you an Annual Stock Purchase Gratuity equal to the amount of cash dividends that would 
					have been paid on the unvested and exercised portion of your Option shares accruing from the
					 date of your acceptance of the Option. The Annual Purchase Gratuity shall be automatically 
					 applied to the subscription price on the exercised Option as partial payment.
					</p>

					<p class="margin-top-10 font-bold">5. Other Payment Terms</p>
					<p class="margin-left-20 margin-top-10">The subscription price may be paid in accordance with the Rules and Regulations as the 
					Administrator of the Plan may promulgate. Moreover, should the company declare the payment 
					of a profit share, a portion of your profit share, if any, but not to exceed ten (10%) thereof 
					will be applied as partial payment</p>
					
					<div class="address-paper">
						<p>6/F Cacho-Gonzales Bldg., 101 Aguirre Street, Legaspi Village, Makati City 1229 Philippines</p>
						<p>Trunk Lines: (632)810 8901 to 06 Fax No.: (632) 8171875</p>
					</div>
					
				</div>
			</div>
		</div>

	</div>

</section>

<!-- Accept Offer -->
<div class="modal-container" modal-id="accept-offer">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ACCEPT OFFER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<div class="">

				<div class="error margin-bottom-10">Quantity in Invalid. <br /> Please type number only in the textbox</div>
				<p class="font-18 margin-bottom-10"	>I will accept the offer and take:</p>

				<div class="margin-left-30">
					<input type="radio" name="offer" id="all" />
					<label for="all" class="black-color txt-normal font-15 margin-left-10">All of the Shares Offered</label>				
				</div>

				<div class="margin-left-30 margin-top-5">
					<input type="radio" name="offer" id="only" />
					<label for="only" class="black-color txt-normal font-15 margin-left-10">
						Only <input type="text" class="small width-100px margin-right-5 margin-left-5 add-border-radius-5px" />Shares from Shares Offered
					</label>				
				</div>													
			</div>	
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			
			<button type="button" class="display-inline-mid btn-normal">View Acceptance Letter</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>