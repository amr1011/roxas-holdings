<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left ">Payment Method List</h1>
			<h2 class="f-left hidden">ESOP View</h2>
			<button class="btn-normal f-right modal-trigger" modal-target="add-payment">Add Payment Method</button>
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10 add-radius margin-top-5">
							<select>
								<option value="op1">Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid add-border-radius-5px margin-top-5" />
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-5">Search</button>
					</td>
				</tr>
			<tbody>
		</table>		
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="text-right-line margin-bottom-50">
			<div class="line"></div>
		</div>
		
		<div class="clear"></div>
		<div class="tbl-rounded">
			<table class="table-roxas margin-top-20 tbl-display">
				<thead>
					<tr>
						<th>Payment Method Name</th>
						<th>Date Added</th>
						<th>Action</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Cash</td>
						<td>October 12, 2014</td>
						<td>
							<a href="#" class="modal-trigger" modal-target="edit-payment">Edit</a><span class="margin-left-20 margin-right-20">|</span><a href="#" class="red-color">Delete</a>
						</td>					
					</tr>
					<tr>
						<td>Check</td>
						<td>October 12, 2014</td>
						<td>
							<a href="#" class="modal-trigger" modal-target="edit-payment">Edit</a><span class="margin-left-20 margin-right-20">|</span><a href="#" class="red-color">Delete</a>
						</td>
					</tr>
					<tr>
						<td>Credit Card</td>
						<td>October 12, 2014</td>
						<td>
							<a href="#" class="modal-trigger" modal-target="edit-payment">Edit</a><span class="margin-left-20 margin-right-20">|</span><a href="#" class="red-color">Delete</a>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>

		<div class="text-right-line margin-bottom-80 margin-top-30">
			<div class="line"></div>
		</div>
	<div>

	

	<div class="panel-group text-left margin-top-30">
		<div class="accordion_custom">
			<div class="panel-heading border-10px">
				<a href="#">
					<h4 class="panel-title white-color active">							
						Audit Logs
						<i class="change-font fa fa-caret-right font-left"></i>
						<i class="fa fa-caret-down font-right"></i>							
					</h4>
				</a>																	
				<div class="clear"></div>					
			</div>					
			<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
				<div class="panel-body ">

					<table class="table-roxas">
						<thead>
							<tr>
								<th>Date of Activiy</th>
								<th>User</th>
								<th>Activity Description</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>September 10, 2015</td>
								<td>ROXAS, PEDRO OLGADO</td>
								<td>Created Payment Method <strong>Cash</strong></td>
							</tr>
							<tr>
								<td>September 10, 2015</td>
								<td>VALENCIA, RENATO CRUZ</td>
								<td>Updated <strong> "COD" </span>to <strong> "Check" </strong></td>
							</tr>
						
						</tbody>
					</table>

				</div>			
			</div>
		</div>
	</div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-payment">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD PAYMENT METHOD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<p class="display-inline-mid margin-right-10">Payment Name:</p>
			<input type="text" class="normal display-inline-mid width-300px add-border-radius-5px" />
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal">Add Payment Method</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add ESOP -->
<div class="modal-container" modal-id="edit-payment">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT PAYMENT METHOD</h4>
			<div class="modal-close close-me"></div>
		</div>
		
		<!-- content -->
		<div class="modal-content">
			<p class="display-inline-mid margin-right-10">Payment Name:</p>
			<input type="text" class="normal display-inline-mid width-300px add-border-radius-5px" value="CASH"/>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>