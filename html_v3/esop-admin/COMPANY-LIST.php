<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">Company List</h1>
			<button class="btn-normal f-right modal-trigger" modal-target="add-company">Add Company</button>
			<div class="clear"></div>
		</div>
		
		<div>
			<p class="white-color margin-bottom-5 margin-left-5">Search</p>
			<div class="select add-radius">
				<select>
					<option value="search1">Company Name</option>
					<option value="search2">Company No.</option>
					<option value="search3">No. of Department</option>					
				</select>
			</div>
			<input type="text" class="small add-border-radius-5px margin-left-10">
			<button class="btn-normal display-inline-mid margin-left-10">Seach</button>
		</div>
	
			
		<div class="text-right-line margin-top-10">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">No. of Departments</a></p>					
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th></th>
						<th>Company Code</th>
						<th>Company Name</th>
						<th>No. of Departments</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td><img src="../assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td><img src="../assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr class="last-content">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-company">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD COMPANY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content  padding-left-0 padding-right-0">
			<div class="upload-photo upload-photo-modal">
				<i class="fa fa-camera fa-3x"></i>
				<p class="margin-top-10 ">Upload Photo</p>
			</div>
			<table class="width-100percent margin-top-10">
				<tbody>
					<tr>
						<td class="text-right padding-right-10">Company Code</td>
						<td class="text-center"><input type="text" class="normal " disabled value="1010" /></td>
					</tr>
					<tr>
						<td class="text-right padding-right-10">Company Name</td>
						<td class="text-center">
							<input type="text" class="normal  " />
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			
			<button type="button" class="display-inline-mid btn-normal">Add Company</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>