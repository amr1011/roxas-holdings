<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">ESOP </a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Joselito Salazar</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">View Letter</a>
				
			</div>			
			<div class="clear"></div>
		</div>

		<h1 class="f-left">Acceptance Letter</h1>
		
		<button class="btn-normal f-right">Print Letter</button>
		<a href="stock-offers.php"><button class="btn-cancel white-color f-right margin-right-20 ">Cancel</button></a>
		
		<div class="clear"></div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content">
		<div class="offer-letter">
			<p class="f-right">September 10, 2015</p>
			<div class="clear"></div>
			<div>
				<p>Mr. Ramon M. De Leon</p>
				<p>SVP, Human Resources &amp; <abbr title="Employee Stock Option Plan">ESOP</abbr> Administrator</p>
				<p>Roxas Holdings</p>
				<p>7th Floor, Cacho-Gonzales Building</p>
				<p>101 Aguirre St., Legaspic Village</p>
				<p>Makati City, Metro Manila</p>

			</div>
			
			<p class="margin-top-30 margin-bottom-20">Dear Sir,</p>
			<p class="text-content">This refers to the Option offer which was the subject of your leter dated
				30 April 2014. In this, please be informed that I am accepting the Option offer under the terms
				and conditions of the Plan.</p>
			<p class="text-content margin-top-10"> Furthermore, I will be taking <span class="text-underline">26,000</span> shares out of the total shares of 
				133,000 shares.</p>
			<div>			
		</div>
	</div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>