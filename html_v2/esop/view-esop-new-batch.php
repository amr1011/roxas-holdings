<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<div class="clear"></div>
		</div>

		<div class="clear"></div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-0">
		<div class="f-right margin-bottom-30">
			<button type="button" class="btn-normal display-inline-mid margin-right-10">Print Analytics Reports</button>
			<button type="button" class="btn-normal display-inline-mid margin-right-10" disabled>Upload Payment Records</button>			
			<button type="button" class="btn-normal display-inline-mid modal-trigger" modal-target="esop-batch">Edit ESOP</button>
		</div>		
		<div class="clear"></div>
		<div class="margin-bottom-30">
			<div class="chart-container">
				<h2 class="black-color">Share Distribution</h2>
				<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
				<div class="data-container">
					<span data-value="500">Number of Shares Taken</span>
					<span data-value="0">Number of Shares Untaken</span>
					<span data-value="100">Number of Employee Offered</span>
				</div>
			</div>
		</div>

		<div class="margin-top-20 margin-bottom-20">
			<div class="select modified-select">
				<select>
					<option value="op1">ESOP 1</option>
					<option value="op2">ESOP 1 - Batch</option>
				</select>
			</div>
			<h2 class="f-right">Granted Sept. 10, 2015</h2>
			
		
			<!-- <a href="esop-view.php"><button class="f-right btn-cancel white-color">Cancel</button></a> -->
			<div class="clear"></div>
		</div>

		<div class="option-box trio">
			<p class="title">Total Share Quantity</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">Php 6.00</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>


		<div class="margin-top-10 f-right margin-right-5 margin-bottom-10">
			<button type="button" class="btn-normal">Upload Share Distribution Template</button>
			<button type="button" class="btn-normal margin-left-10">Distribute Shares</button>
		</div>
		<div class="clear"></div>

		<div class="option-box trio">
			<p class="title">Number of Employee Offered</p>
			<p class="description">0 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Taken</p>
			<p class="description">0 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Untaken</p>
			<p class="description">0 Shares</p>
		</div>

		<div class="text-right-line margin-top-30 padding-bottom-30">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Shares Distributed</a></p>
			</div>
		</div>

		
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading">
					<a href="#" class="f-left">
						<h4 class="panel-title active white-color margin-top-5">							
							CAPDI.HO
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right "></i>							
						</h4>
					</a>
					<div class="f-right">
						<p class="display-inline-mid font-15 white-color margin-right-10">100,000 Shares</p>						
					</div>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body">
							
					
						<!-- Secondary Accordion -->
						<div class="panel-group text-left">
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title active white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">100,000 Shares</p>
									</div>															
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse in">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>													
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>													
												</tr>																						
											</tbody>
										</table>

									</div>			
								</div>
							</div>
							<div class="accordion_custom margin-top-20">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title white-color">							
											VP and AVP
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>																	
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">25,000 Shares</p>
									</div>
									<div class="clear"></div>	

								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
							<div class="accordion_custom margin-top-20">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title white-color">							
											Senior Manager
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>			
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">25,000 Shares</p>
									</div>														
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						</div>

					</div>			
				</div>
			</div>	


			<div class="accordion_custom margin-top-10">
				<div class="panel-heading">
					<a href="#" class="f-left">
						<h4 class="panel-title  white-color margin-top-5">							
							CACI
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right "></i>							
						</h4>
					</a>
					<div class="f-right">
						<p class="display-inline-mid font-15 white-color margin-right-10">200,000 Shares</p>						
					</div>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse ">								
					<div class="panel-body">
							
						<div class="bg-accordion padding-20px">							
							<div class="display-inline-mid">
								<p class="display-inline-mid text-right font-bold padding-right-20">Offer Letters:</p>
								<p class="display-inline-mid">Offer Version 5 Oct 5, 2015</p>
								
							</div>
							<div class="display-inline-mid text-right">
								<p class="display-inline-mid text-right padding-right-20 margin-left-50 font-bold">Acceptance Letter:</p>
								<p class="display-inline-mid">Acceptance Version 5 Oct 5, 2015</p>
							</div>
							<div class="display-inline-mid text-right">
								<p class="display-inline-mid text-right padding-right-20 margin-left-50 font-bold">Offer Expiry:</p>
								<p class="display-inline-mid">Oct 10, 2015</p>
							</div>
						</div>	


						<!-- Secondary Accordion -->
						<div class="panel-group text-left">
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title active white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">100,000 Shares</p>
									</div>															
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse in">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
													<th>Shares Taken</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>30,000 Shares</td>
													<td>30,000 Shares</td>
													<td><a href="#">View Letter</a> <span class="margin-left-10 margin-right-10">|</span> <a href="#">View Account</a></td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>President &amp; CEO</td>
													<td>30,000 Shares</td>
													<td>N/A (Rejected)</td>
													<td><a href="#" class="not-active">View Letter</a> <span class="margin-left-10 margin-right-10">|</span> <a href="#" class="not-active">View Account</a></td>
												</tr>																						
											</tbody>
										</table>

									</div>			
								</div>
							</div>
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title white-color">							
											VP and AVP
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>																	
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">25,000 Shares</p>
									</div>
									<div class="clear"></div>	

								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title white-color">							
											Senior Manager
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>			
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">25,000 Shares</p>
									</div>														
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						</div>

					</div>			
				</div>
			</div>


			<div class="accordion_custom margin-top-10">
				<div class="panel-heading">
					<a href="#" class="f-left">
						<h4 class="panel-title  white-color margin-top-5">							
							ROXOL
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right "></i>							
						</h4>
					</a>
					<div class="f-right">
						<p class="display-inline-mid font-15 white-color margin-right-10">200,000 Shares</p>						
					</div>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse ">								
					<div class="panel-body">
							
						<div class="bg-accordion padding-20px">							
							<div class="display-inline-mid">
								<p class="display-inline-mid text-right font-bold padding-right-20">Offer Letters:</p>
								<p class="display-inline-mid">Offer Version 5 Oct 5, 2015</p>
								
							</div>
							<div class="display-inline-mid text-right">
								<p class="display-inline-mid text-right padding-right-20 margin-left-50 font-bold">Acceptance Letter:</p>
								<p class="display-inline-mid">Acceptance Version 5 Oct 5, 2015</p>
							</div>
							<div class="display-inline-mid text-right">
								<p class="display-inline-mid text-right padding-right-20 margin-left-50 font-bold">Offer Expiry:</p>
								<p class="display-inline-mid">Oct 10, 2015</p>
							</div>
						</div>	


						<!-- Secondary Accordion -->
						<div class="panel-group text-left">
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title active white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">100,000 Shares</p>
									</div>															
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse in">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
													<th>Shares Taken</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>30,000 Shares</td>
													<td>30,000 Shares</td>
													<td><a href="#">View Letter</a> <span class="margin-left-10 margin-right-10">|</span> <a href="#">View Account</a></td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>President &amp; CEO</td>
													<td>30,000 Shares</td>
													<td>N/A (Rejected)</td>
													<td><a href="#" class="not-active">View Letter</a> <span class="margin-left-10 margin-right-10">|</span> <a href="#" class="not-active">View Account</a></td>
												</tr>																						
											</tbody>
										</table>

									</div>			
								</div>
							</div>
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title white-color">							
											VP and AVP
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>																	
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">25,000 Shares</p>
									</div>
									<div class="clear"></div>	

								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title white-color">							
											Senior Manager
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>			
									<div class="f-right">	
										<p class=" font-15 white-color margin-right-10">25,000 Shares</p>
									</div>														
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						</div>

					</div>			
				</div>
			</div>
		</div>

		<div class="margin-top-20 margin-bottom-20">
		<!-- 	<button class="f-right btn-normal margin-left-15">Finalize Distribution</button>
			<button class="f-right btn-normal margin-left-15 ">Save as Draft</button>
			<a href="esop-view.php"><button class="f-right btn-cancel white-color">Cancel</button></a> -->
			<div class="clear"></div>
		</div>

	<div>

	<div class="panel-group text-left margin-top-30">
		<div class="accordion_custom">
			<div class="panel-heading bg-accordion">
				<a href="#">
					<h4 class="panel-title active white-color">							
						AUDIT LOGS
						<i class="change-font fa fa-caret-right font-left"></i>
						<i class="fa fa-caret-down font-right"></i>							
					</h4>
				</a>																	
				<div class="clear"></div>					
			</div>					
			<div class="panel-collapse in">								
				<div class="panel-body bg-accordion padding-15px">
					<p>Lorem Ipsum</p>
				</div>			
			</div>
		</div>	
	</div>
</section>


<!-- set OFFER -->
<div class="modal-container" modal-id="add-grat">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SET OFFER LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<p>Are you sure you want to send the offer letters of this ESOP to the employees?</p>
			<div>
					<table>
						<tbody>
							<tr>
								<td>Esop Name:</td>
								<td class="padding-left-20 font-bold">ESOP 1</td>
							</tr>
							<tr>
								<td>Dividend Price per Share:</td>
								<td>
									<input type="text" class="normal display-inline-mid width-100px margin-left-20" />
									<div class="select display-inline-mid width-100px margin-left-10">
										<select>
											<option type="money1">PHP</option>
										</select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>


			</div>

			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Add Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- ESOP Batch -->
<div class="modal-container" modal-id="esop-batch">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CREATE ESOP BATCH</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
		
			<div>
				<table>
					<tbody>
						<tr>
							<td>ESOP Name</td>
							<td><input type="text" class="normal margin-left-20" /></td>
						</tr>
						<tr>
							<td>Dividend Price per Share</td>
							<td class="font-bold padding-left-20">Php 6.00 / Share</td>
						</tr>
					</tbody>
				</table>
			</div>		
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Create Batch</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>