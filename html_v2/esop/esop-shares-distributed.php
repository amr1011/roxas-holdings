<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<div class="clear"></div>
		</div>
		<button class="btn-normal f-right modal-trigger" modal-target="edit-esop">Edit ESOP</button>
		<div class="clear"></div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class=" margin-bottom-30">
			<div class="chart-container">
				<h2 class="black-color">Share Distribution</h2>
				<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
				<div class="data-container">
					<span data-value="500">Number of Shares Taken</span>
					<span data-value="0">Number of Shares Untaken</span>
					<span data-value="100">Number of Employee Offered</span>
				</div>
			</div>
		</div>

		<div class="margin-top-20 margin-bottom-20">
			<h2 class="f-left">Share Distribution</h2>
			<button class="f-right btn-normal margin-left-15 modal-trigger" modal-target="set-offer">Set Offer Expiration</button>
			<button class="f-right btn-normal margin-left-15 modal-trigger" modal-target="distribution">Upload Shares Distribution Template</button>
			<!-- <a href="esop-view.php"><button class="f-right btn-cancel white-color">Cancel</button></a> -->
			<div class="clear"></div>
		</div>

		<div class="option-box trio">
			<p class="title">Number of Employees Offered</p>
			<p class="description">0 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Taken</p>
			<p class="description">0 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Untaken</p>
			<p class="description">0 Shares</p>
		</div>

		<h2 class="f-left">ESOP 1</h2>
		<h2 class="f-right">Granted Sept. 10, 2015</h2>
		<div class="clear"></div>

		<div class="option-box trio">
			<p class="title">Total Share Quantity</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">6.00 PHP</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>

		<div class="text-right-line margin-top-30">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Shares Distributed</a></p>
			</div>
		</div>
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-40 padding-top-40">
			<div class="accordion_custom">
				<div class="panel-heading">
					<a href="#" class="f-left">
						<h4 class="panel-title active white-color margin-top-5">							
							CAPDI.HO
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right padding-top-5"></i>							
						</h4>
					</a>
					<div class="f-right">
						<p class="display-inline-mid font-15 white-color margin-right-10">100,000 Shares</p>
						<i class="fa fa-pencil-square-o display-inline-mid  fa-2x hover-icon"></i>
					</div>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body">
							
						<div class="bg-accordion padding-20px">							
							<div class="display-inline-mid">
								<p class="display-inline-mid text-right font-bold padding-right-20">Offer Letters:</p>
								<p class="display-inline-mid">Offer Version 5 Oct 5, 2015</p>
								<i class="display-inline-mid fa fa-pencil-square-o fa-2x margin-left-10"></i>
							</div>
							<div class="display-inline-mid text-right">
								<p class="display-inline-mid text-right padding-right-20 margin-left-50 font-bold">Acceptance Letter:</p>
								<div class="select display-inline-mid ">
									<select>
										<option value="Sample 1">Sample 1</option>
										<option value="Sample 2">Sample 2</option>
										<option value="Sample 3">Sample 3</option>
										<option value="Sample 4">Sample 4</option>
										<option value="Sample 5">Sample 5</option>
										<option value="Sample 6">Sample 6</option>
									</select>
								</div>
								<button type="button" class="btn-normal">Save</button>
							</div>
						</div>	

						<!-- Secondary Accordion -->
						<div class="panel-group text-left">
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left margin-top-5">
										<h4 class="panel-title active white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-5"></i>							
										</h4>
									</a>	
									<div class="f-right">
										<p class="display-inline-mid font-15 white-color margin-right-10">100,000 Shares</p>
										<i class="fa fa-pencil-square-o display-inline-mid  fa-2x hover-icon"></i>
									</div>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse in">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>30,00 Shares</td>
													<td><a href="#">Edit Shares Offered</a></td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>President &amp; CEO</td>
													<td>20,00 Shares</td>
													<td><a href="#">Edit Shares Offered</a></td>
												</tr>
												
											</tbody>
										</table>

									</div>			
								</div>
							</div>
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#" class="f-left margin-top-5">
										<h4 class="panel-title white-color">							
											VP and AVP
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-10"></i>							
										</h4>
									</a>	
									<div class="f-right">
										<input type="text" class="small display-inline-mid" />
										<p class="white-color font-15 display-inline-mid margin-left-10">Shares</p>
										<button type="button" class="display-inline-mid btn-cancel white-color">Cancel</button>
										<button type="button" class="btn-normal display-inline-mid">Save Changes</button>
										
									</div>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
							<div class="accordion_custom margin-top-15">
								<div class="panel-heading ">
									<a href="#" class="f-left">
										<h4 class="panel-title white-color ">							
											Senior Manager
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right padding-top-10"></i>							
										</h4>
									</a>				
									<div class="f-right">
										<p class="display-inline-mid font-15 white-color margin-right-10">25,000 Shares</p>
										<i class="fa fa-pencil-square-o display-inline-mid  fa-2x hover-icon"></i>
									</div>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						</div>

					</div>			
				</div>
			</div>	
		</div>

		<div class="margin-top-20 margin-bottom-20">

			<button class="f-right btn-normal margin-left-15 modal-trigger" modal-target="set-offer">Set Offer Expiration</button>
			<button class="f-right btn-normal margin-left-15 modal-trigger" modal-target="distribution">Upload Shares Distribution Template</button>

		<!-- 	<button class="f-right btn-normal margin-left-15">Finalize Distribution</button>
			<button class="f-right btn-normal margin-left-15 ">Save as Draft</button>
			<a href="esop-view.php"><button class="f-right btn-cancel white-color">Cancel</button></a> -->
			<div class="clear"></div>
		</div>

	<div>
</section>

<!-- edit ESOP -->
<div class="modal-container" modal-id="edit-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">Please fill up form</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- set OFFER -->
<div class="modal-container" modal-id="set-offer">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SET OFFER EXPIRATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<p>Please set the date of offer expiration</p>
			<div class="margin-top-10">
				<p class="display-inline-mid">Expiry Date: </p>
				<div class="date-picker margin-left-10 display-inline-mid">
					<input type="text" data-date-format="MM/DD/YYYY">
					<span class="fa fa-calendar text-center"></span>
				</div>
			</div>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Set Offer Expiration</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- distribution templates -->
<div class="modal-container" modal-id="distribution">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATES</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<p>Please indicate the file name of the template:</p>
			<div class="margin-top-10">
				<p class="display-inline-mid">File Name: </p>
				<em class="display-inline-mid marign-left-10">No file uploaded yet</em>
				<button type="button" class="btn-normal margin-left-30">Browse File</button>
			</div>
		</div>

		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>