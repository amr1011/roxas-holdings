<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<h2 class="f-left">ESOP 1</h2>
		<h2 class="f-right">Granted Sept. 10, 2015</h2>
		<div class="clear"></div>

		<div class="option-box trio">
			<p class="title">Total Share Quantity</p>
			<p class="description">500.000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">6.00 PHP</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>

		<div class="margin-top-20 margin-bottom-20">
			<h2 class="f-left">Share Distribution</h2>
			<button class="f-right btn-normal margin-left-15">Finalize Distribution</button>
			<button class="f-right btn-normal margin-left-15 ">Save as Draft</button>
			<a href="esop-view.php"><button class="f-right btn-cancel white-color">Cancel</button></a>
			<div class="clear"></div>
		</div>

		<div class="option-box trio">
			<p class="title">Number of Employees Offered</p>
			<p class="description">0 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Taken</p>
			<p class="description">0 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Untaken</p>
			<p class="description">0 Shares</p>
		</div>

		<div class="text-right-line margin-top-40 padding-bottom-30">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Shares Distributed</a></p>
			</div>
		</div>

		<div class="error-msg margin-top-30 ">Please Indicate Company Shares First</div>
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading">
					<a href="#" class="f-left">
						<h4 class="panel-title active white-color margin-top-15">							
							CAPDI.HO
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>
					<div class="f-right">
						<input type="text" class="normal display-inline-mid"/>
						<p class="display-inline-mid margin-left-10 white-color">Shares</p>
					</div>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body">
							
						<div class="bg-accordion padding-20px">
							<p><strong>Please select the letter Templates to be sent to the employees:</strong></p>
							<div class="margin-top-20">
								<p class="display-inline-mid text-right  padding-right-20">Offer Letters:</p>
								<div class="select display-inline-mid width-25per">
									<select>
										<option value="Sample 1">Sample 1</option>
										<option value="Sample 2">Sample 2</option>
										<option value="Sample 3">Sample 3</option>
										<option value="Sample 4">Sample 4</option>
										<option value="Sample 5">Sample 5</option>
										<option value="Sample 6">Sample 6</option>
									</select>
								</div>
								<p class="display-inline-mid text-right padding-right-20 margin-left-50">Acceptance Letter:</p>
								<div class="select display-inline-mid width-25per">
									<select>
										<option value="Sample 1">Sample 1</option>
										<option value="Sample 2">Sample 2</option>
										<option value="Sample 3">Sample 3</option>
										<option value="Sample 4">Sample 4</option>
										<option value="Sample 5">Sample 5</option>
										<option value="Sample 6">Sample 6</option>
									</select>
								</div>
							</div>
						</div>	

						<!-- Secondary Accordion -->
						<div class="panel-group text-left">
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#">
										<h4 class="panel-title active white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse in">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small error-form tool-tip" tt-html="<p><strong>Remaining Shares:</strong></p><p>500,000 Shares</p>"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small tool-tip" tt-html="<p><strong>Remaining Shares:</strong></p><p>500,000 Shares</p>"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small tool-tip" tt-html="<p><strong>Remaining Shares:</strong></p><p>500,000 Shares</p>"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small tool-tip" tt-html="<p><strong>Remaining Shares:</strong></p><p>500,000 Shares</p>"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small tool-tip" tt-html="<p><strong>Remaining Shares:</strong></p><p>500,000 Shares</p>"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#">
										<h4 class="panel-title white-color">							
											VP and AVP
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#">
										<h4 class="panel-title white-color">							
											Senior Manager
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td><input type="text" class="small"/> Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						</div>

					</div>			
				</div>
			</div>	
		</div>

		<div class="margin-top-20 margin-bottom-20">
			<button class="f-right btn-normal margin-left-15">Finalize Distribution</button>
			<button class="f-right btn-normal margin-left-15 ">Save as Draft</button>
			<a href="esop-view.php"><button class="f-right btn-cancel white-color">Cancel</button></a>
			<div class="clear"></div>
		</div>

	<div>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>