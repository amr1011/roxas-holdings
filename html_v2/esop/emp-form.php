<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">Personal Stocks</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Statement of Account</a>
			</div>			
			<div class="clear"></div>
		</div>
		<button class="btn-normal f-right">Print Claim Form</button>
		<div class="clear"></div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content">
		<div class="emp-form">
			<p class="title-form">Employee Form</p>
			<table class="emp-table">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td>ESOP 1</td>
						<td>Number of Matured Stocks:</td>
						<td><strong>26,500 Stocks</strong></td>
					</tr>
				</tbody>
			</table>
			<div class="numbers">
				<p class="f-left">Number of Stocks to be claimed:</p>
				<p class="f-right">26,500 Shares</p>
				<div class="clear"></div>
			</div>
			<p>Furthermore, this is to request for the issuance of the above number of stock certificates under my name.</p>
			<div class="complex-info">
				<div>
					<table>
						<tbody>
							<tr>
								<td>
									<p class="white-color">xx</p>
									<div class="line"></div>
									<p>Employee's Name and Signature</p>
								</td>
								<td>
									<p>September 21, 2014</p>
									<div class="line"></div>
									<p>Date</p>
								</td>
							</tr>
						</tbody>
					</table>					
				</div>
			</div>

			<div class="divider"></div>
			<p class="authorized">Authorization of Claim</p>
			<p class="long-text">This is to accept and validate the Mr. / Ms. <strong>Joselito Salazar</strong> has execised his / 
				her stock option vested rights, over <strong>26,500</strong> number of shares under <strong><abbr title="Employee Stock Option Plan">ESOP</abbr> 1</strong>.
				In this regard, the Stock Transfer Agent shall issue the Stocks Certificate as declared on this portion.</p>

			<p class="approved">Approved by:</p>
			<div class="incharge">
				<p><abbr title="Employee Stock Option Plan">ESOP</abbr> Administrator</p>
			</div>
		</div>
	</div>
</section>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>