<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">Stock Offers</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
				
			</div>			
			<div class="clear"></div>
		</div>

		<h1 class="f-left">Offer Letter</h1>
		<button class="btn-normal f-right modal-trigger" modal-target="accept-offer">Accept Offer</button>
		<!-- <button class="btn-normal f-right margin-right-20" >Reject Offer</button> -->
		
		<div class="clear"></div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content">
		<div class="offer-letter">
			<p class="f-right">September 10, 2015</p>
			<div class="clear"></div>
			<div >
				<p>Jessie Rex Caringa</p>
				<p>Auditor</p>
				<p>Internal Audit</p>
			</div>
			
			<p class="margin-top-30 margin-bottom-20">Dear Joselito Salazar,</p>
			<p class="text-content">In accordance with the terms of the Employee Stock Option Plan 
				<abbr title="Employee Stock Option Plan">(ESOP)</abbr> of Roxas Holdings, Inc. (RHI),
				which was approved by the Securities and Exchange Commission (SEC) on April 30, 2014, the company
				offers you an option to subscribe and purchase 133,000 shares of RHI which have been allocation 
				to you over the exercise period of Five (5) years commencing from the date of this letter offer 
				under the following conditions:</p>
			<div>
			<ol class="margin-top-30">
				<li >Subscription Price &amp; Terms.</li>
				<p class="inside-list">The subsription price is based on the average price of the RHI shares in the Philippine Stock
					Exchange for thirty (30) trading days prior to the date of this option letter discounted
					at the rate of fifteen percent (15%) and payable within a period of Five (5) years without
					interest. In view thereof, the subscription price is Php 6.00 / Share.</p>
				<li>Vesting of Option.</li>
				<p class="inside-list">Option will vest as follows:</p>
				<table class="table-offer">
					<thead>
						<tr>
							<th>Vesting Date</th>
							<th>Vesting Option for</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>At the end of one (1) year from the date of the option offer letter</td>
							<td>One - fifth (1/5) of the aggregate number of Option Shares</td>
						</tr>
						<tr>
							<td>At the end of two (2) years from the date of the option offer letter</td>
							<td>One - fifth (1/5) of the aggregate number of Option Shares</td>
						</tr>
						<tr>
							<td>At the end of three (3) years from the date of the option offer letter</td>
							<td>One - fifth (1/5) of the aggregate number of Option Shares</td>
						</tr>
					</tbody>
				</table>
			</ol>
		</div>
	</div>
</section>


<!-- add ESOP -->
<div class="modal-container" modal-id="accept-offer">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ACCEPT OFFER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please indicate Amount of Shares to Accept</p>
			<div class="margin-top-20">
				<p class="display-inline-mid margin-right-10">Shares to Accept:</p>				
				<input type="text" class="normal display-inline-mid margin-right-10" />				
				<p class="display-inline-mid">Shares</p>
			</div>

			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<a href="accept-offer.php"><button type="button" class="display-inline-mid btn-normal">View Acceptance Letter</button></a>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>