<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">Personal Stocks</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>			
			<div class="clear"></div>
		</div>
		<h1 class="f-left">View Employee <abbr title="Employee Stock Option Plan">ESOP</abbr></h1>
		<div class="f-right">
			<button class="btn-normal display-inline-block margin-right-10 modal-trigger" modal-target="upload-record">Upload Payment Record Template</button>
			<button class="btn-normal display-inline-block modal-trigger" modal-target="payment-method"> Add Payment Method</button>
		</div>
		<div class="clear"></div>

		<div class="user-profile margin-top-50">
			<div class="user-image">
				<img src="../assets/images/profile/profile.jpg" alt="user-picture" />
			</div>
			<div class="user-name">
				<p class="name">Joselito Salazar</p>
				<p class="position">Employee No: <span>132</span></p>
			</div>
			<div class="user-info">
				<table>
					<tbody>
						<tr>
							<td>Company</td>
							<td>ROXOL</td>
						</tr>
						<tr>
							<td>Department</td>
							<td>ISD</td>
						</tr>
						<tr>
							<td>Rank:</td>
							<td>Officer I</td>
						</tr>
						<tr>
							<td>Separation Status: </td>
							<td>N/A</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content">

		<div class="option-box">
			<p class="title">Grant Started</p>
			<p class="description">August 31, 2013</p>
		</div>
		<div class="option-box">
			<p class="title">Subscription Price</p>
			<p class="description">Php 6.00 </p>
		</div>
		<div class="option-box">
			<p class="title">No. of Shares Availed</p>
			<p class="description">130,144</p>
		</div>
		<div class="option-box">
			<p class="title">Value of Shares Availed</p>
			<p class="description">Php 324,058.56 </p>
		</div>

		<h2 class="margin-top-50">Vesting Rights</h2>
		<table class="table-roxas">
			<thead>
				<tr>
					<th>No.</th>
					<th>Year</th>
					<th>Percentage</th>
					<th>No. of Shares</th>
					<th>Value of Shares</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>5</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr class="last-content">					
					<td colspan="2"></td>
					<td class="combine"><span class="total-text">Total:</span> 100%</td>
					<td>130,144</td>
					<td>Php 324,058.56</td>
				</tr>
		</table>

		<h2 class="margin-top-50">Summary</h2>

		<div class="long-panel">
			<p class="first-text">Amount Shares Taken: 130,144 | @ 2.40</p>
			<p class="second-text margin-right-50">Php 324,058.56</p>
			<div class="clear"></div>
		</div>

		<table class="table-roxas margin-top-20">
			<thead>
				<tr>
					<th class="width-250px">No.</th>
					<th>Payment Details</th>
					<th>Date Paid</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Gratuity</td>
					<td>October 2, 2013</td>
					<td>18,399.36 Php</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Profit Share</td>
					<td>October 2, 2013</td>
					<td>10,590.72 Php</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Cash</td>
					<td>October 2, 2013</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Credit Card</td>
					<td>October 2, 2013</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>5</td>
					<td>Check</td>
					<td>October 2, 2013</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr class="last-content ">
					<td colspan="4" class="text-right">
						<div class="display-inline-mid text-center margin-right-20">
							<p class="font-14">Total Payments:</p>
							<p class="font-15">39,001.35 Php</p>
						</div>
						<div class="display-inline-mid text-center margin-right-50">
							<p class="font-12 width-200px">Outstanding Balance as of November 24, 2014</p>
							<p class="font-15">285,057.21 Php</p>
						</div>
					</td>					
				</tr>
			</tbody>
		</table>

		<h2 class="margin-top-50">Payment Application</h2>

		<table class="table-roxas">
			<thead>
				<tr>
					<th colspan="5">
						<p class="f-left  margin-left-10 font-20">YEAR 1 - SEPTEMBER 01, 2013</p>
						<div class="f-right">
							<p class="display-inline-mid margin-right-10 font-20">With Vesting Rights</p>
						
						</div>
						<div class="clear"></div>
					</th>
				</tr>
			</thead>
			<tbody>

				<tr>
					<td colspan="5">
						<p class="f-left margin-left-10 font-15">Amount Shares Taken <span class="margin-left-50">26,028</span></p>
						<p class="f-right margin-right-100 font-18">64,809.72 Php</p>
						<div class="clear"></div>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<p class="text-left margin-left-20 font-bold">Less Payment</p>
					</td>
				</tr>
				<tr>
					<td class="width-150px">						
						<p>No</p>
					</td>
					<td class="">Payment Date</td>
					<td class="">Payment Mode</td>
					<td class="">Amount</td>
					<td class=" width-400px">Payment Details</td>
				</tr>

				<tr>
					<td>1</td>
					<td>October 2, 2013</td>
					<td>Gratuity</td>
					<td>7,808.64 Php</td>
					<td>Gratuity @.6 / Share</td>
				</tr>

				<tr>
					<td colspan="4"></td>
					<td >
						<p class="display-inline-mid ">Total Payments:</p>
						<p class="display-inline-mid margin-left-10">Php 7,808.64 </p>
					</td>
				</tr>			
			</tbody>
		</table>
		<div class="bg-last-content padding-10px text-right">
			<div class="display-inline-mid white-color text-left margin-right-100">
				<p class="font-bold">Outstanding Balance:<span class="margin-left-10 font-normal">For Year 1</span></p>
				
			</div>
			<p class="display-inline-mid white-color margin-right-100">Php 18,399.36 </p>
		</div>

		<table class="table-roxas margin-top-50">
			<thead>
				<tr>
					<th colspan="5">
						<p class="f-left  margin-left-10 font-20">YEAR 2 - SEPTEMBER 01, 2013</p>
						<div class="f-right">
							<p class="display-inline-mid margin-right-10 font-20">Without Vesting Rights</p>							
						</div>
						<div class="clear"></div>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="5">
						<p class="f-left margin-left-10 font-15">Amount Shares Taken <span class="margin-left-50">26,028</span></p>
						<p class="f-right margin-right-100 font-18">64,809.72 Php</p>
						<div class="clear"></div>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<p class="text-left margin-left-20 font-bold">Less Payment</p>
					</td>
				</tr>
				<tr>
					<td class="width-150px">						
						<p>No</p>
					</td>
					<td class="">Payment Date</td>
					<td class="">Payment Mode</td>
					<td class="">Amount</td>
					<td class=" width-400px">Payment Details</td>
				</tr>

				<tr>
					<td>1</td>
					<td>October 2, 2013</td>
					<td>Gratuity</td>
					<td>7,808.64 Php</td>
					<td>Gratuity @.6 / Share</td>
				</tr>
				<tr>
					<td>2</td>
					<td>December 9, 2013</td>
					<td>Gratuity</td>
					<td>2,602.88 Php</td>
					<td>Gratuity @.6 / Share</td>
				</tr>
				<tr>
					<td>3</td>
					<td>February 15, 2013</td>
					<td>Gratuity</td>
					<td>7,808.64 Php</td>
					<td>Gratuity @.6 / Share</td>
				</tr>
				<tr>
					<td colspan="4"></td>
					<td >
						<p class="display-inline-mid ">Total Payments:</p>
						<p class="display-inline-mid margin-left-10">Php 15,617.28 </p>
					</td>
				</tr>			
			</tbody>
		</table>
		<div class="bg-last-content padding-10px text-right">
			<div class="display-inline-mid white-color text-left margin-right-100">
				<p class="font-bold">Outstanding Balance: <span class="font-normal margin-left-10">For Year 2</span></p>
				
			</div>
			<p class="display-inline-mid white-color margin-right-100">Php 10,590.72 </p>
		</div>

	<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color">							
							AUDIT LOGS
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body bg-accordion padding-15px">
						<p>Lorem Ipsum</p>
					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- payment method -->
<div class="modal-container" modal-id="payment-method">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD PAYMENT RECORD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<table class="width-100percent">
				<tbody>
					<tr>
						<td class="width-150px">ESOP Name</td>
						<td>ESOP 1</td>
					</tr>
					<tr>
						<td>Payment Method</td>
						<td><div class="select width-100percent">
								<select>
									<option value="op1">Cash</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Payment Value:</td>
						<td><input type="text" class="normal display-inline-mid width-150px"> 
							<div class="select display-inline-mid width-150px margin-left-5">
								<select >
									<option value="cur">PHP</option>
								</select>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Add Payment</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- upload record -->
<div class="modal-container" modal-id="upload-record">
	<div class="modal-body">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD PAYMENT RECORD TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please indicate the file name of the template:</p>
			<div class="margin-top-20">
				<p class="display-inline-mid padding-right-10">File Name:</p>
				<p class="display-inline-mid padding-left-10"><em>No file uploaded yet</em></p>
				<button type="button" class="btn-normal display-inline-mid margin-left-20">Browse File</button>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>