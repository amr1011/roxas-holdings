<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
	
		<h1 class="text-center">Analytics Report</h1>
				
	</div>
</section>

<section section-style="content-panel">

	<div class="content">
		<div>
			<h2 class="f-left">ESOP 1</h2>
			<h2 class="f-right">Granted Sept. 10, 2015</h2>
			<div class="clear"></div>
		</div>
		<div class="option-box trio">
			<p class="title">Total Share Quantity</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">Php 6.00 </p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>
		<h2>Share Distribution</h2>
		<div class="option-box trio">
			<p class="title">Number of Employees Offered</p>
			<p class="description">1,000 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Share Taken</p>
			<p class="description">0 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Untaken</p>
			<p class="description">0 Shares</p>
		</div>


		<div class=" margin-bottom-30 margin-top-30">
			<div class="chart-container">
				<h2 class="black-color">Share Distribution</h2>
				<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
				<div class="data-container">
					<span data-value="500">Number of Shares Taken</span>
					<span data-value="0">Number of Shares Untaken</span>
					<span data-value="100">Number of Employee Offered</span>
				</div>
			</div>
		</div>

		<h2 class="margin-top-50">Employee List</h2>
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#" class="f-left">
						<h4 class="panel-title active white-color">							
							CAPDI. HO
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>			
					<p class="f-right white-color">100,000 Shares</p>														
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body bg-accordion padding-15px">
						<p>Lorem Ipsum</p>
					</div>			
				</div>
			</div>	
		</div>

		<table class="table-roxas">
			<thead>
				<tr>
					<th>Employee Name</th>
					<th>Department</th>
					<th>Rank</th>
					<th>Shares Offered</th>
					<th>Shares Taken</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>ROXAS, PEDRO OLGADO</td>
					<td>Office of the President</td>
					<td>CHAIRMAN</td>
					<td>30,000 Shares</td>
					<td>30,000 Shares</td>
				</tr>
				<tr>
					<td>VALENCIA, RENATO CRUZ</td>
					<td>Office of teh President</td>
					<td>PRESIDENT &amp; CEO</td>
					<td>20,000 Shares</td>
					<td>N/A (Rejected)</td>
				</tr>
			</tbody>
		</table>

	

	

	<!-- place accordion here  -->
		

	<div>
</section>

<!-- payment method -->
<div class="modal-container" modal-id="payment-method">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD PAYMENT RECORD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<table class="width-100percent">
				<tbody>
					<tr>
						<td class="width-150px">ESOP Name</td>
						<td>ESOP 1</td>
					</tr>
					<tr>
						<td>Payment Method</td>
						<td><div class="select width-100percent">
								<select>
									<option value="op1">Cash</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Payment Value:</td>
						<td><input type="text" class="normal display-inline-mid width-150px"> 
							<div class="select display-inline-mid width-150px margin-left-5">
								<select >
									<option value="cur">PHP</option>
								</select>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Add Payment</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- upload record -->
<div class="modal-container" modal-id="upload-record">
	<div class="modal-body">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD PAYMENT RECORD TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please indicate the file name of the template:</p>
			<div class="margin-top-20">
				<p class="display-inline-mid padding-right-10">File Name:</p>
				<p class="display-inline-mid padding-left-10"><em>No file uploaded yet</em></p>
				<button type="button" class="btn-normal display-inline-mid margin-left-20">Browse File</button>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>