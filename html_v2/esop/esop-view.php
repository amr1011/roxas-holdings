<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<button class="btn-normal f-right modal-trigger" modal-target="edit-esop">Edit ESOP</button>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<h2 class="f-left">ESOP 1</h2>
		<h2 class="f-right">Granted Sept. 10, 2015</h2>
		<div class="clear"></div>

		<div class="option-box trio">
			<p class="title">Total Share Quantity</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">6.00 PHP</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>

		<div class="margin-top-20 margin-bottom-20">
			<h2 class="f-left">Share Distribution</h2>
			<a href="esop-view-distribute.php"><button class="f-right btn-normal margin-left-15">Distribute Shares</button></a>
			<button class="f-right btn-normal modal-trigger" modal-target="share-distribution-template">Upload Share Distribution Template</button>
			<div class="clear"></div>
		</div>

		<div class="option-box trio">
			<p class="title">Number of Employees Offered</p>
			<p class="description">0 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Taken</p>
			<p class="description">0 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Number of Shares Untaken</p>
			<p class="description">0 Shares</p>
		</div>

		<div class="text-right-line margin-top-30">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Shares Distributed</a></p>
			</div>
		</div>
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-50 padding-top-30">
			<div class="accordion_custom">
				<div class="panel-heading  ">
					<a href="#">
						<h4 class="panel-title active white-color">							
							CAPDI.HO
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body">
						<!-- Secondary Accordion -->
						<div class="panel-group text-left">
							<div class="accordion_custom">
								<div class="panel-heading ">
									<a href="#">
										<h4 class="panel-title active white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse in">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>
							<div class="accordion_custom ">
								<div class="panel-heading ">
									<a href="#">
										<h4 class="panel-title white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-20px">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
							<div class="accordion_custom margin-top-15">
								<div class="panel-heading ">
									<a href="#">
										<h4 class="panel-title white-color">							
											Office of the President
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse">								
									<div class="panel-body padding-all-20">

										<table class="table-roxas margin-top-20 margin-bottom-20">
											<thead>
												<tr>
													<th>Employee Name</th>
													<th>Rank</th>
													<th>Shares Offered</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
												<tr>
													<td>Cruz, Juan Dela</td>
													<td>Chairman</td>
													<td>0 Shares</td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						</div>

					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Share Distribution Template</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<p>Please indicate the file name of the template:</p>
			<div class="margin-top-30">
				<p class="display-inline-mid margin-right-20">File Name:</p>
				<p class="display-inline-mid margin-left-10 margin-right-20"><i>No file uploaded yet</i></p>
				<button class="display-inline-mid btn-normal">Browse File</button>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add ESOP -->
<div class="modal-container" modal-id="edit-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Edit ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">Please fill up form</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>