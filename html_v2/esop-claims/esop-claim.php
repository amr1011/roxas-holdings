<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">ESOP Claims</h1>
			<h2 class="hidden">ESOP Claims</h2>
			<!-- <button class="btn-normal f-right modal-trigger" modal-target="add-esop">ADD ESOP</button> -->
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>
					
					<td class="padding-left-20 white-color">Grant Date</td>
					<td class="padding-left-20 white-color">Price per Share</td>
				</tr>
				<tr>
					<td>
						<label class="display-inline-mid margin-left-20">From</label>
						<div class="date-picker display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY">
							<span class="fa fa-calendar text-center"></span>
						</div>
						<label class="display-inline-mid margin-left-10">To</label>
						<div class="date-picker display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY">
							<span class="fa fa-calendar text-center"></span>
						</div>
					</td>
					<td>
						<div class="price xsmall display-inline-mid margin-left-20">
							<input type="text">
						</div>
					</td>
					<td><button class="btn-normal display-inline-mid margin-left-10">Filter List</button></td>
				</tr>				
			<tbody>
		</table>
			
		<div class="text-right-line margin-top-30">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">ESOP Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Status</a></p>					
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content padding-top-40">


		<!-- pending  -->
		<div class="data-box data-two divide-by-2">
			<table class="width-100per">
				<tbody>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
						<td colspan="2" class="text-right color-pending font-15 font-bold padding-right-20">PENDING</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
						<td>ESOP 1</td>
						<td class="text-right">Vesting Years:</td>
						<td class="text-right">Year 1</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
						<td>25,000 Shares</td>
						<td class="text-right">Price per Share:</td>
						<td class="text-right">Php 6.00 </td>
					</tr>
				</tbody>
			</table>
			<div class="shapes-pending triangle"></div>
			<div class="data-hover text-center">
				<a href="view-claim.php">
					<button class="btn-normal">View Claim</button>
				</a>
			</div>
		</div>

		<div class="data-box data-two divide-by-2">
			<table class="width-100per">
				<tbody>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
						<td colspan="2" class="text-right color-pending font-15 font-bold padding-right-20">PENDING</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
						<td>ESOP 1</td>
						<td class="text-right">Vesting Years:</td>
						<td class="text-right">Year 1</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
						<td>25,000 Shares</td>
						<td class="text-right">Price per Share:</td>
						<td class="text-right">Php 6.00 </td>
					</tr>
				</tbody>
			</table>
			<div class="shapes-pending triangle"></div>
			<div class="data-hover text-center">
				<a href="view-claim.php">
					<button class="btn-normal">View Claim</button>
				</a>
			</div>
		</div>

		<!-- approved  -->
		<div class="data-box data-two divide-by-2">
			<table class="width-100per">
				<tbody>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
						<td colspan="2" class="text-right color-approved font-15 font-bold padding-right-20">APPROVED</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
						<td>ESOP 1</td>
						<td class="text-right">Vesting Years:</td>
						<td class="text-right">Year 1</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
						<td>25,000 Shares</td>
						<td class="text-right">Price per Share:</td>
						<td class="text-right">Php 6.00 </td>
					</tr>
				</tbody>
			</table>
			<div class="shapes-approved triangle"></div>
			<div class="data-hover text-center">
				<a href="view-claim.php">
					<button class="btn-normal">View Claim</button>
				</a>
			</div>
		</div>

		<div class="data-box data-two divide-by-2">
			<table class="width-100per">
			<tbody>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
						<td colspan="2" class="text-right color-approved font-15 font-bold padding-right-20">APPROVED</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
						<td>ESOP 1</td>
						<td class="text-right">Vesting Years:</td>
						<td class="text-right">Year 1</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
						<td>25,000 Shares</td>
						<td class="text-right">Price per Share:</td>
						<td class="text-right">Php 6.00 </td>
					</tr>
				</tbody>
			</table>
			<div class="shapes-approved triangle"></div>
			<div class="data-hover text-center">
				<a href="view-claim.php">
					<button class="btn-normal">View Claim</button>
				</a>
			</div>
		</div>

		<!-- rejecet -->
		<div class="data-box data-two divide-by-2">
			<table class="width-100per">
				<tbody>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
						<td colspan="2" class="text-right color-reject font-15 font-bold padding-right-20">REJECTED</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
						<td>ESOP 1</td>
						<td class="text-right">Vesting Years:</td>
						<td class="text-right">Year 1</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
						<td>25,000 Shares</td>
						<td class="text-right">Price per Share:</td>
						<td class="text-right">Php 6.00 </td>
					</tr>
				</tbody>
			</table>
			<div class="shapes-reject triangle"></div>
			<div class="data-hover text-center">
				<a href="view-claim.php">
					<button class="btn-normal">View Claim</button>
				</a>
			</div>
		</div>

		<div class="data-box data-two divide-by-2">
			<table class="width-100per">
				<tbody>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">12345 - Jose Protacio</h3></td>
						<td colspan="2" class="text-right color-reject font-15 font-bold padding-right-20">REJECTED</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
						<td>ESOP 1</td>
						<td class="text-right">Vesting Years:</td>
						<td class="text-right">Year 1</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
						<td>25,000 Shares</td>
						<td class="text-right">Price per Share:</td>
						<td class="text-right">Php 6.00 </td>
					</tr>
				</tbody>
			</table>
			<div class="shapes-reject triangle"></div>
			<div class="data-hover text-center">
				<a href="view-claim.php">
					<button class="btn-normal">View Claim</button>
				</a>
			</div>
		</div>

	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">PLEASE FILL UP FORM</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>