<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<div class="breadcrumbs margin-bottom-20">
				<a href="esop-check.php">ESOP Claim</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Jose Protacio</a>				
			</div>			
			<div class="clear"></div>
		</div>
		<h1 class="f-left">View Employee Claim</h1>
		<div class="f-right">
			<button class="btn-normal display-inline-mid margin-right-10 modal-trigger" modal-target="reject-claim">Reject Claim</button>
			<button class="btn-normal display-inline-mid modal-trigger" modal-target="approve-claim">Approve Claim</button>
		</div>
		<div class="clear"></div>
	</div>
</section>

<section section-style="content-panel">	
	

	<div class="content print-statement">

		<div class="big-box">
			<table>
				<tr>
					<td class="text-left width-200px">ESOP Name</td>
					<td class="font-bold text-left">ESOP 1</td>
					<td class="text-left width-200px">Vested Right:</td>
					<td class="font-bold text-left">Year 1</td>
					<td class="width-200px"></td>
					<td></td>
				</tr>
				<tr>
					<td class="text-left">OR Number:</td>
					<td class="font-bold text-left">123456</td>
					<td class="text-left">Date of OR:</td>
					<td class="font-bold text-left">October 1, 2015</td>
					<td class="text-left">Amount Paid:</td>
					<td class="font-bold text-left">30,000.00 PHP</td>
				</tr>
			</table>
		</div>

		<div class="big-box margin-top-30 padding-20px">
			<p class="font-20 margin-left-30 margin-bottom-30">Attachment</p>			
			<table class="border-all-ssmall">
				<tbody>
					<tr>
						<td class="text-left width-300px padding-left-50">
							<i class="fa fa-file-pdf-o fa-3x display-inline-mid margin-right-30"></i>
							<p class="display-inline-mid">ESOP Payment OR.pdf</p>
						</td>
						<td>
							<p class="display-inline-mid margin-right-10">Date Attached: </p>
							<p class="display-inline-mid font-bold">October 1, 2015</p>
						</td>
						<td>
							<a href="#" class="font-15">Download File</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="user-profile margin-bottom-30">
			<div class="margin-bottom-20 margin-top-50">
				<h2><abbr title="Employee Stock Option Plan">ESOP</abbr> 1 Statement of Account</h2>
			</div>
			<div class="user-name">
				<p class="name">Joselito Salazar</p>
				<p class="position">Employee No: <span>132</span></p>
			</div>
			<div class="user-info">
				<table>
					<tbody>
						<tr>
							<td>Company</td>
							<td>ROXOL</td>
						</tr>
						<tr>
							<td>Department</td>
							<td>ISD</td>
						</tr>
						<tr>
							<td>Rank:</td>
							<td>Officer I</td>
						</tr>
						<tr>
							<td>Separation Status: </td>
							<td>N/A</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="option-box">
			<p class="title">Grant Started</p>
			<p class="description">August 31, 2013</p>
		</div>
		<div class="option-box">
			<p class="title">Subscription Price</p>
			<p class="description">Php 6.00 </p>
		</div>
		<div class="option-box">
			<p class="title">No. of Shares Availed</p>
			<p class="description">130,144</p>
		</div>
		<div class="option-box">
			<p class="title">Value of Shares Availed</p>
			<p class="description">Php 324,058.56 </p>
		</div>

		<h2 class="margin-top-50">Vesting Rights</h2>

		<table class="table-roxas">
			<thead>
				<tr>
					<th>No.</th>
					<th>Year</th>
					<th>Percentage</th>
					<th>No. of Shares</th>
					<th>Value of Shares</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>5</td>
					<td>Sept. 01, 2013</td>
					<td>20%</td>
					<td>26,028</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr class="last-content">					
					<td colspan="2"></td>
					<td class="combine"><span class="total-text">Total:</span> 100%</td>
					<td>130,144</td>
					<td>Php 324,058.56 </td>
				</tr>
		</table>

		<h2 class="margin-top-50">Summary</h2>

		<div class="long-panel">
			<p class="first-text">Amount Shares Taken 130,144 | @ 2.40</p>
			<p class="second-text margin-right-100">Php 324, 058.56 </p>
			<div class="clear"></div>
		</div>

		<table class="table-roxas margin-top-20">
			<thead>
				<tr>
					<th class="width-250px">No.</th>
					<th>Payment Details</th>
					<th>Date Paid</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Gratuity</td>
					<td>October 2, 2013</td>
					<td>18,399.36 Php</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Profit Share</td>
					<td>October 2, 2013</td>
					<td>10,590.72 Php</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Cash</td>
					<td>October 2, 2013</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Credit Card</td>
					<td>October 2, 2013</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr>
					<td>5</td>
					<td>Check</td>
					<td>October 2, 2013</td>
					<td>64,809.72 Php</td>
				</tr>
				<tr class="last-content ">
					<td colspan="4" class="text-right">
						<div class="display-inline-mid text-center margin-right-20">
							<p class="font-14">Total Payments:</p>
							<p class="font-15">39,001.35 Php</p>
						</div>
						<div class="display-inline-mid text-center margin-right-50">
							<p class="font-12 width-200px">Outstanding Balance as of November 24, 2014</p>
							<p class="font-15">285,057.21 Php</p>
						</div>
					</td>					
				</tr>
			</tbody>
		</table>

		<h2 class="margin-top-50">Payment Application</h2>

		<table class="table-roxas">
			<thead>
				<tr>
					<th colspan="5">
						<p class="f-left  margin-left-10 font-20">YEAR 1 - SEPTEMBER 01, 2013</p>
						
						<div class="clear"></div>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="5">
						<p class="f-left margin-left-10 font-15">Amount Shares Taken: <span class="margin-left-20">26,028</span></p>
						<p class="f-right margin-right-50 font-18">Php 64,809.72 </p>
						<div class="clear"></div>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<p class="text-left font-bold margin-left-20">Less Payment</p>
					</td>
				</tr>
				<tr>
					<td class="width-150px">						
						<p>No</p>
					</td>
					<td class="">Payment Date</td>
					<td class="">Payment Mode</td>
					<td class="">Amount</td>
					<td class=" width-400px">Payment Details</td>
				</tr>

				<tr>
					<td>1</td>
					<td>October 2, 2013</td>
					<td>Gratuity</td>
					<td>7,808.64 Php</td>
					<td>Gratuity @.6 / Share</td>
				</tr>

				<tr>
					<td colspan="4"></td>
					<td >
						<p class="display-inline-mid ">Total Payments:</p>
						<p class="display-inline-mid margin-left-10">7,808.64 Php</p>
					</td>
				</tr>			
			</tbody>
		</table>

		<div class="bg-last-content padding-10px text-right">
			<div class="display-inline-mid white-color text-left margin-right-100">
				<p class="font-bold">Outstanding Balance:<span class="font-normal margin-left-10">For Year 1</span></p>
				
			</div>
			<p class="display-inline-mid white-color margin-right-100">Php 18,399.36 </p>
		</div>

		<div class="emp-form margin-top-40">
			<p class="title-form">Employee Claim Form</p>
			<table class="emp-table">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td>ESOP 1</td>
						<td>Number of Matured Stocks:</td>
						<td><strong>26,500 Stocks</strong></td>
					</tr>
				</tbody>
			</table>
			<div class="numbers">
				<p class="f-left">Number of Stocks to be claimed:</p>
				<p class="f-right">26,500 Shares</p>
				<div class="clear"></div>
			</div>
			<p>Furthermore, this is to request for the issuance of the aboce number of stock certificates under my name.</p>
			<div class="complex-info">
				<div>
					<table>
						<tbody>
							<tr>
								<td>
									<p class="white-color">xx</p>
									<div class="line"></div>
									<p>Employee's Name and Signature</p>
								</td>
								<td>
									<p>September 21, 2014</p>
									<div class="line"></div>
									<p>Date</p>
								</td>
							</tr>
						</tbody>
					</table>					
				</div>
			</div>

			<div class="divider"></div>
			<p class="authorized">Authorization of Claim</p>
			<p class="long-text">This is to accept and validate the Mr. / Ms. <strong>Joselito Salazar</strong> has execised his / 
				her stock option vested rights, over <strong>26,500</strong> number of shares under <strong><abbr title="
				Employee Stock Option Plan">ESOP</abbr> 1</strong>.
				In this regard, the Stock Transfer Agent shall issue the Stocks Certificate as declared on this portion.</p>

			<p class="approved">Approved by:</p>
			<div class="incharge">
				<p><abbr title="Employee Stock Option Plan">ESOP</abbr> Administrator</p>
			</div>
		</div>

	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="approve-claim">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">APPROVE CLAIM</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Are you sure you want to approve this <abbr title="Employee Stock Option Plan">ESOP</abbr> Claim? </p>				
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Approve Claim</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="modal-container" modal-id="reject-claim">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">REJECT CLAIM</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please state reason for Rejecting: </p>				
			<textarea class="black-color margin-top-20 padding-10px"></textarea>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Reject Claim</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>