<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">Personal Stocks</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Stock Claim Form</a>
			</div>			
			<div class="clear"></div>
		</div>
		<button class="btn-normal f-right">Print Claim Form</button>
		<div class="clear"></div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content mother-container">

		<div class="main-container">

			<div class="sub-container">			
				<div class="head-center">
					<i class="fa fa-angle-left"></i>
					<p>Page</p>
					<p>1</p>
					<p class="white-color">/</p>
					<p>2</p>
					<i class="fa fa-angle-right"></i>
				</div>				
				<div class="head-right">
					<i class="fa fa-search-plus"></i>
					<i class="fa fa-search-minus"></i>
				</div>			
				<div class="clear"></div>
			</div>
			<div class="sub-container1">
				<div class="inside-sub">						
					<table class="line-tbl">
						<tbody>
							<tr>
								<td><p class="text-center">PART 1 - To be filled out by the employee</p></td>
							</tr>
							<tr>
								<td>
									<div>
										<p class="display-inline-mid">1. OPTION PLAN TYPE</p>
										<div class="checkbox-esop display-inline-mid">
											<label class="black-color"><input type="checkbox" name="option">ESOP 1</label>
											<label class="black-color"><input type="checkbox" name="option">ESOP 2</label>
											<label class="black-color"><input type="checkbox" name="option">ESOP 2 2nd Round</label>									
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<p>2. NUMBER OF MATURE STOCKS: <span class="margin-left-30">43,468.00</span></p>

								</td>
							</tr>
							<tr>
								<td>
									<div>
										<p>3. NUMBER OF STOCKS TO BE CLAIMED</p>
										<p class="margin-left-30">(Check the box of the desired claim)</p>

										<div class="checkbox-rate">
											<label class="black-color"><input type="checkbox" name="stocks"><span>Partial:</span> No. of Shares _________ Amount for Payment _________ </label>
											<label class="black-color"><input type="checkbox" name="stocks"><span>Full:</span> No of Shares <spam class="txt-under"> 43,468.00</spam> Amounth of Payment <span class="txt-under"> P 108,235.32 </span></label>
										</div>
										<div class="text-center margin-top-30 margin-bottom-30">	
											<p>Furthermore, this is to request for the issuance of the above number of stock certificates under my name.</p>
										</div>
										<div class="profile-line-tbl">
											<div class="profile1">
												<p>Marcelino C. Bundoc</p>
												<p>_________________________</p>
												<p>Employee's Name and Signature</p>
											</div>
											<div class="profile2">
												
												<p>_________________________</p>
												<p>Date</p>
											</div>
										</div>
									</div>	
								</td>
							</tr>
						</tbody>
					</table>
					

					<h2 class="black-color margin-top-30">Payment Application</h2>
					<div class="long-panel border-10px margin-top-10">
						<p class="first-text">Year 2</p>
						<p class="first-text margin-left-30">July 28, 2016</p>
						<p class="second-text margin-right-50">With Vesting Rights</p>
						<div class="clear"></div>
					</div>
					<div class="long-panel border-10px margin-top-10">
						<p class="first-text">Amount Shares Taken: 130,144 | @ 2.40</p>
						<p class="second-text margin-right-50">Php 324, 058.56 </p>
						<div class="clear"></div>
					</div>
					
					<div class="tbl-rounded">
						<table class="table-roxas tbl-display margin-top-20">
							<thead>
								<tr>
									<th class="width-250px">No.</th>
									<th>Payment Details</th>
									<th>Date Paid</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Gratuity</td>
									<td>October 2, 2013</td>
									<td>18,399.36 Php</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Profit Share</td>
									<td>October 2, 2013</td>
									<td>10,590.72 Php</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Cash</td>
									<td>October 2, 2013</td>
									<td>64,809.72 Php</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Credit Card</td>
									<td>October 2, 2013</td>
									<td>64,809.72 Php</td>
								</tr>
								<tr>
									<td>5</td>
									<td>Check</td>
									<td>October 2, 2013</td>
									<td>64,809.72 Php</td>
								</tr>
								<tr class="last-content ">
									<td colspan="3" class="text-left">
										<p class="margin-left-30 padding-bottom-5">Total Payments:</p>
										<p class="margin-left-30">Outstanding Balance (as of Tuesday, December 29, 2016)</p>
									</td>
									<td  class="">										
										<p class="font-15 padding-bottom-5">39,001.35 Php</p>																			
										<p class="font-15">285,057.21 Php</p>									
									</td>					
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>

</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CLAIM VESTING RIGHTS</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please indicate Amounth of Shares to Claim</p>
			<div class="margin-top-20">
				<p class="display-inline-mid margin-right-10">Share to Claim:</p>
				<div class="tool-tip display-inline-mid" tt-html="<p>Claimable Shares:</p><p><small>26,208 Shares</small></p>">	
					<input type="text" class="normal display-inline-mid margin-right-10" />
				</div>								
				<p class="display-inline-mid">Shares</p>
			</div>

			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>