<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">Company List</h1>
			<button class="btn-normal f-right modal-trigger" modal-target="add-company">Add Company</button>
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>					
					<td class="padding-left-20 white-color">Search</td>					
				</tr>
				<tr>
					<td>
						<div class="select margin-right-20">
							<select>
								<option value="op1">Company Name</option>
								<option value="op2">Company Name</option>
								<option value="op3">Company Name</option>
							</select>
						</div>
						
					</td>
					<td>						
						<input type="text" class="normal">					
					</td>
					<td><button class="btn-normal display-inline-mid margin-left-10">Search</button></td>
				</tr>				
			<tbody>
		</table>
			
		<div class="text-right-line margin-top-5">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">No. of Departments</a></p>					
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<table class="table-roxas">
			<thead>
				<tr>
					<th></th>
					<th>Company Code</th>
					<th>Company Name</th>
					<th>No. of Departments</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
					<td>1010</td>
					<td>Cr8v Web Solutions, Inc.</td>
					<td>10</td>
					<td><a href="view-company.php">View Company</a></td>
				</tr>
				<tr>
					<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
					<td>1010</td>
					<td>Cr8v Web Solutions, Inc.</td>
					<td>10</td>
					<td><a href="view-company.php">View Company</a></td>
				</tr>
				<tr>
					<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
					<td>1010</td>
					<td>Cr8v Web Solutions, Inc.</td>
					<td>10</td>
					<td><a href="view-company.php">View Company</a></td>
				</tr>
				<tr>
					<td class="padding-0px"><img src="../assets/images/mountain.png"></td>
					<td>1010</td>
					<td>Cr8v Web Solutions, Inc.</td>
					<td>10</td>
					<td><a href="view-company.php">View Company</a></td>
				</tr>
				<tr>
					<td><img src="../assets/images/mountain.png"></td>
					<td>1010</td>
					<td>Cr8v Web Solutions, Inc.</td>
					<td>10</td>
					<td><a href="view-company.php">View Company</a></td>
				</tr>
				<tr>
					<td><img src="../assets/images/mountain.png"></td>
					<td>1010</td>
					<td>Cr8v Web Solutions, Inc.</td>
					<td>10</td>
					<td><a href="view-company.php">View Company</a></td>
				</tr>
				<tr class="last-content">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>

	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-company">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD COMPANY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content  padding-left-0 padding-right-0">
			<div class="upload-photo">
				<i class="fa fa-camera fa-3x "></i>
				<p class="margin-top-10 ">Upload Photo</p>
			</div>
			<table class="width-100percent ">
				<tbody>
					<tr>
						<td class="text-right padding-right-10">Company Code</td>
						<td class="text-center"><input type="text" class="normal " /></td>
					</tr>
					<tr>
						<td class="text-right padding-right-10">Company Name</td>
						<td class="text-center">
							<input type="text" class="normal  " />
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Add Company</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>