<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">View Department Information</h1>
			<div class="f-right">
				<button class="btn-normal display-inline-mid margin-right-10 modal-trigger" modal-target="add-rank">Add Rank</button>
				<a href="edit-rank.php"><button class="btn-normal display-inline-mid margin-right-10 " >Edit Rank</button></a>
				<button class="btn-normal display-inline-mid modal-trigger"  modal-target="edit-rank">Edit Department</button>				
			</div>
			<div class="clear"></div>
		</div>
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="margin-top-20 white-color">
			<table class="width-100percent">
				<tbody>
					<tr>
						<td>Department Code:</td>
						<td>Department Name:</td>
					</tr>
					<tr>
						<td class="padding-top-10">101001</td>
						<td class="padding-top-10">Office of the President</td>
					</tr>
				</tbody>
			</table>
			
			<p class="margin-top-30">Description:</p>
			<p class="margin-top-10">Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​,
			 morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra
			  adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The 
			  voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. 
			  Nescio an Undead zombies. Sicut malus movie horror.</p>
		</div>
		<div class="text-right-line margin-top-5">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Rank <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Rank No.</a></p>					
			</div>
		</div>
	
		<h2 class="margin-top-40">Rank List</h2>
		<table class="table-roxas comp-info">
			<thead>
				<tr>
					<th class="width-200px">No</th>
					<th>Rank</th>
					<th class="width-150px">No. of Employee</th>					
				</tr>
			</thead>		
			<tbody>
				<tr>
					<td>1st</td>
					<td>Chairman</td>
					<td>10</td>
				</tr>
				<tr>
					<td>2nd</td>
					<td>President &amp; CEO</td>
					<td>05</td>
				</tr>
			</tbody>
		</table>
	

		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color">							
							AUDIO LOGS
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body bg-accordion padding-15px">
						<p>Lorem Ipsum</p>
					</div>			
				</div>
			</div>	
		</div>
	<div>
</section>

<!-- edit RANK -->
<div class="modal-container" modal-id="edit-rank">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT DEPARTMENT INFORMATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<table class="">
				<tbody>
					<tr>
						<td>Department Code:</td>
						<td><input type="text" class="normal margin-left-10" value="101001 " /></td>
					</tr>
					<tr>
						<td>Department Name:</td>
						<td><input type="text" class="normal margin-left-10" value="Office of the President " /></td>
					</tr>
					<tr>
						<td class="v-top">Description:</td>
						<td>
							<textarea class="margin-left-10 black-color width-200px" placeholder="Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead."></textarea>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Edit Department</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add RANK -->
<div class="modal-container" modal-id="add-rank">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD RANK</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<table class="width-100ppercent center-margin">
				<tbody>
					<tr>
						<td class="padding-top-10">No. :</td>
						<td><input type="text" class="normal margin-left-20 width-300px"/></td>
					</tr>
					<tr>
						<td class="v-top padding-top-15">Rank Name:</td>
						<td>
							<div class="select margin-left-20 width-300px">
								<select>
									<option value="op1">Value 1</option>
									<option value="op2">Value 2</option>
									<option value="op3">Value 3</option>
								</select>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Add Rank</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>