<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">View Company Information</h1>
			<div class="f-right">
				<button class="btn-normal display-inline-mid modal-trigger margin-right-10" modal-target="add-company">Add Department</button>
				<button class="btn-normal display-inline-mid modal-trigger" modal-target="edit-company">Edit Company</button>				
			</div>
			<div class="clear"></div>
		</div>

		<div class="company-logo">
			<img src="../assets/images/mountain.png" alt="company-img" class=" width-400px display-inline-mid" />
			<div class="display-inline-mid">
				<h2 class="">Cr8v Web Solution, Inc.</h2>
				<h3 class="white-color text-left font-normal">Company Code: 1010</h3>
			</div>
		</div>
	
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="text-right-line margin-top-5">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">No. of Departments</a></p>					
			</div>
		</div>

		<h2 class="margin-top-40">Department List</h2>
		<table class="table-roxas comp-info">
			<thead>
				<tr>
					<th class="width-150px">Code</th>
					<th class="width-200px">Name</th>
					<th>Company Name</th>
					<th class="width-200px">No. of Ranks</th>					
				</tr>
			</thead>		
		</table>
		<div class="data-box width-100percent margin-0px fake-table ">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-department.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>
		<div class="data-box width-100percent margin-0px fake-table">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-department.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>
		<div class="data-box width-100percent margin-0px fake-table">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-department.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>
		<div class="data-box width-100percent margin-0px fake-table">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-department.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>
		<div class="data-box width-100percent margin-0px fake-table">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-department.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>
		<table class="table-roxas comp-info">
			<thead>
				<tr class="height-40px">
					<th></th>
					<th></th>
					<th></th>					
				</tr>
			</thead>		
		</table>


		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color">							
							AUDIO LOGS
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body bg-accordion padding-15px">
						<p>Lorem Ipsum</p>
					</div>			
				</div>
			</div>	
		</div>
	<div>
</section>

<!-- edit ESOP -->
<div class="modal-container" modal-id="edit-company">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT COMPANY INFORMATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="upload-photo">
				<i class="fa fa-camera fa-3x"></i>
				<p class="margin-top-10">Upload Photo</p>
			</div>
			<table class="width-100percent">
				<tbody>
					<tr>
						<td class="text-right">Company Code:</td>
						<td class="text-center"><input type="text" class="normal" value="1010"/></td>
					</tr>
					<tr>
						<td class="text-right">Company Name:</td>
						<td class="text-center"><input type="text" class="normal display-inline-mid" value="Cr8v Web Solutions" /></td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-company">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD DEPARTMENT</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<table class="center-margin">
				<tr>
					<td>Department Code</td>
					<td><input type="text" class="normal margin-left-20" /></td>
				</tr>
				<tr>
					<td>Department Name</td>
					<td><input type="text" class="normal margin-left-20" /></td>
				</tr>
				<tr>
					<td class="v-top">Description</td>
					<td><textarea class="margin-left-20 black-color width-200px"></textarea></td>
				</tr>
			</table>			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Add Department</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>