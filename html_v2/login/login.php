
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" href="assets/ico/favicon.ico">

        <title>Roxas Holings Login</title>

        <!-- Bootstrap core CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        <link href=../"assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">

        <!-- Core CSS -->
    
    
        <!-- Custom CSS -->
        <link href="../assets/css/global/commons.css" rel="stylesheet">
        <link href="../assets/css/global/ui-widgets.css" rel="stylesheet">
        <link href="../assets/css/global/login.css" rel="stylesheet">
    </head>
    <body>
        <section class="login-parent">
            <div class="login-form">
                <form>
                    <div class="image-content margin-bottom-20">
                        <img src="../assets/images/roxas-holdings-logo.jpg" alt="roxas logo">
                    </div>
                    <p class="black-color font-bold text-center margin-bottom-10 font-20 ">ESOP Management System</p>  
                    <p>Please enter your email for Password Reset:</p>
                    <div class="error">Username and Password do not match. Please try again.</div>                  
                    <input type="text" class="normal width-100per margin-top-30 add-border-radius-5px " placeholder="Username">
                    <input type="password" class="normal margin-top-20 width-100per add-border-radius-5px" placeholder="Password">
                
                   
                    <p class="forgot"><a href="">Can't Remember Password?</a></p>
                    <button class="btn-normal margin-top-30 width-200px">Login</button>
                </form>

                <div class="email">
                    
                </div>        
            </div> 

        </section>
    </body>
</html>