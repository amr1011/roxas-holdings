<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Gratuity Report</h1>			
			<div class="clear"></div>
		</div>
		<table>
			<tbody>
				<tr>
					<td class="white-color">Please indicate the Date Range</td>					
				</tr>
				<tr>					
					<td>
						<label class="display-inline-mid"> FROM</label>
						<div class="date-picker display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY">
							<span class="fa fa-calendar text-center"></span>
						</div>
						<label class="display-inline-mid margin-left-10"> TO</label>
						<div class="date-picker display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY">
							<span class="fa fa-calendar text-center"></span>
						</div>
					</td>				
					<td>
						<a href="generated-report.php">
							<button class="btn-normal display-inline-mid margin-left-10">Generate Report</button>
						</a>
					</td>
				</tr>
			<tbody>
		</table>			
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		
		<div class="clear"></div>
		<div class="calendar-icon">
			<i class="fa fa-calendar"></i>
			<h2 class="margin-top-50">Please Supply the Date Range to Generate the Report</h2>
		</div>
	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>
		<!-- content -->
		<div class="modal-content">
			<div class="error">PLEASE FILL UP FORM</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>