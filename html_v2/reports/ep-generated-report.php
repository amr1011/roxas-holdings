<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left">Employee Payment Report</h1>
			<!-- <button class="btn-normal f-right modal-trigger" modal-target="add-esop">ADD ESOP</button> -->
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>
					<td class="white-color">Please indicate the Date Range</td>					
				</tr>
				<tr>					
					<td>
						<label class="display-inline-mid ">From</label>
						<div class="date-picker display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY">
							<span class="fa fa-calendar text-center"></span>
						</div>
						<label class="display-inline-mid margin-left-10">To</label>
						<div class="date-picker display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY">
							<span class="fa fa-calendar text-center"></span>
						</div>
					</td>
					
					<td><button class="btn-normal display-inline-mid margin-left-10">Generate Report</button></td>
				</tr>
			<tbody>
		</table>

		<table class="margin-top-20">
			<tbody>
				<tr>
					<td class="white-color">Search</td>
					<td class="padding-left-20 white-color">Company</td>
					<td class="padding-left-20 white-color">Price per Share</td>
				</tr>
				<tr>
					<td>
						<div class="select display-inline-mid">
							<select>
								<option value="ESOP Name">ESOP Name</option>
								<option value="Vesting Years">Vesting Years</option>
								<option value="Grant Date">Grant Date</option>
								<option value="Share QTY">Share QTY</option>
							</select>
						</div>

						<input type="text" class="search normal display-inline-mid margin-left-10"/>

						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</td>
					<td>
						<div class="select margin-left-20">
							<select>
								<option value="coma">Company a</option>
								<option value="comb">Company b</option>
								<option value="comc">Company c</option>
							</select>
						</div>

					</td>
					<td>
						<div class="price xsmall display-inline-mid margin-left-20">
							<input type="text">
						</div>
						<div class="price xsmall display-inline-mid margin-left-20">
							<input type="text">
						</div>
					</td>
					<td><button class="btn-normal display-inline-mid margin-left-10">Filter List</button></td>
				</tr>
			<tbody>
		</table>
			
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="gen-report">
			<i class="fa fa-file-excel-o fa-2x margin-right-10"></i>
			<i class="fa fa-file-pdf-o fa-2x margin-right-10"></i>
			<i class="fa fa-print fa-2x"></i>
		</div>
		<div class="clear"></div>
		<table class="table-roxas margin-top-20">
			<thead>
				<tr>
					<th>Date Added</th>
					<th>Employee Name</th>
					<th>ESOP Name</th>
					<th>Vesting Year</th>
					<th>Payment Type</th>
					<th>Payment Value</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>September 10, 2015</td>
					<td>Joselito Salazar</td>
					<td>ESOP 1</td>
					<td>Year 1</td>
					<td>Cash</td>
					<td>100,000 Php</td>
				</tr>
				<tr>
					<td>September 10, 2015</td>
					<td>Aaron Paul Labing-Lima</td>
					<td>StockOptionPlan 1</td>
					<td>Year 2</td>
					<td>Credit Card</td>
					<td>50,000 Php</td>
				</tr>
				<tr class="last-content">
					<td colspan="6" class="text-right">
						<span class="font-bold ">Total</span>
						<span class="margin-left-20 margin-right-50">150,000 Php</span>
					</td>
				</tr>
			</tbody>
		</table>
	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">PLEASE FILL UP FORM</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>