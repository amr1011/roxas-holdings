<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
				
			<div class="clear"></div>
		</div>
		
		<div class="f-right">
			<a href="view-archieved-letter.php">
				<button class="btn-normal display-inline-block margin-right-10">Archive This Letter</button>
			</a>
			<button class="btn-normal display-inline-block modal-trigger " modal-target="edit-letter" id="edit-letter-click">Edit Letter</button>
		</div>
		<div class="clear"></div>

	</div>

</section>

<section section-style="content-panel">

	<div class="content">

		<p class="font-20 white-color">Offer Letter</p>
		<h2>Offer Letter v.1 for ROXOL</h2>

			
		<div class="letter-head"> 
			<p class="f-left">Date Created: October 30, 2015</p>
			<p class="f-right">Created By: Joselito Salazar</p>
			<div class="clear"></div>
		</div>
		<div class="letter-content">
			<p class="f-right font-bold">&#60;Date of Letter></p>
			<div class="f-left margin-top-20">
				<p class="font-bold"> &#60;Sender Name></p>
				<p class="font-bold"> &#60;Sender Rank></p>
				<p class="font-bold"> &#60;Sender Department></p>
			</div>
			<div class="clear"></div>

			<p class="margin-top-20">Dear <span class="font-bold">&#60;Recepient's Name>,</span></p>
			<p class="margin-top-10">In accordance with the terms of the Employee Stock Option Plan (<abbr title="
			Employee Stock Option Plan">ESOP</abbr>) of Roxas Holdings,
			Inc. (<abbr title="Roxas Holdings Inc.">RHI</abbr>), which was approved by the Securities and Exchange Commission 
			(<abbr title="Securities and Exchange Commission">SEC</abbr>) on April 30, 2014, 
			the company offers you an option to subscribe and purchange <span class="font-bold"> &#60;Offer Shares> </span>
			shares of RHI which has been allocated to you over the exercise period of <span class="font-bold">
			&#60;Vesting Years> </span> years commencing from the date of this letter offer under the following conditions:
			</p>

			<ol class="margin-top-20">
				<li class="font-bold">Subscription Price &amp; Terms.</li>
				<p>This subscription price is based on the average price of the RHI shares in the 
				Philippine Stock Exchange for thirty(30) trading days prior to the date of this 
				subscription price is <strong>&#60;Price per Share></strong>.</p>
				<li class="font-bold margin-top-20">Vesting of Option.</li>
				<p>The Option will vest as follows:</p>
			</ol>
			<table class="table-offer">
				<thead>
					<tr>
						<th>Vesting Date</th>
						<th>Vesting Option for</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>At the end of one (1) year from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of two (2) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of three (3) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
				</tbody>
			</table>


		</div>

		<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color">							
							AUDIT LOGS
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body bg-accordion padding-15px">
						<p>Lorem Ipsum</p>
					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- edit letter -->
<div class="modal-container edit-letter" modal-id="edit-letter">
	<div class="modal-body max-width-1200 width-1200px ">
		<div class="modal-head ">
			<h4 class="text-left">EDIT LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content ">	

			<div class="head">
				<div class="display-inline-mid width-80percent">
					<p class="margin-bottom-5">Title</p>
					<input type="text" class="normal width-100percent" value="Offer Letter v.1 for ROXOL" />
				</div>
				<div class="display-inline-mid margin-left-20">
					<p class="margin-bottom-5">Letter Type:</p>
					<div class="select">
						<select>
							<option value="op1">Offer Letter A</option>
							<option value="op2">Offer Letter B</option>
							<option value="op3">Offer Letter B</option>
						</select>
					</div>
				</div>
			</div>

			<div class="big-header">
				<p class="f-left margin-left-10">Date Created: October 30, 2015</p>
				<p class="f-right margin-right-20">Created By: Joselito Salazar</p>
				<div class="clear"></div>
			</div>

			<div class="big-body">

				<div class="function">
					<!-- left - side -->
					<div class="format f-left  ">

					

						<div class="text-files">
							<textarea name="editor1" id="editor1" rows="10" cols="80"></textarea>
						</div>
					</div>							
				</div>

				<div class="letter-nav ">
					<!-- right side  -->

					<div class="tag  ">
						<p class="text-center">TAGS</p>
					</div>					
					
					<div class="menu ">
						<ul>
							<li>ESOP Name</li>
							<li>Offer Shares</li>
							<li>Price / Share</li>
							<li>Total Value of Share</li>
							<li>Vesting Years</li>
							<li>Date of Letter</li>
							<li>Sender Name</li>
							<li>Sender Department</li>
							<li>Recipient Name</li>
							<li>Recipient Rank</li>
							<li>Recipient Department</li>								
							<li></li>								
						</ul>
					</div>

					<div class="clear"></div>

				</div>

			</div>
				
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			
			<button type="button" class="display-inline-mid btn-normal alert-btn">Submit</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>