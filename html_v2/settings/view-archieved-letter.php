<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
				
			<div class="clear"></div>
		</div>
		
		<div class="f-right">			
			<button class="btn-normal display-inline-block">Activate Letter</button>
		</div>
		<div class="clear"></div>

	</div>

</section>

<section section-style="content-panel">

	<div class="content">

		<p class="font-20 white-color">Offer Letter</p>
		<h2>Offer Letter v.1 for ROXOL</h2>
			
		<div class="letter-head"> 
			<p class="f-left">Date Created: October 30, 2015</p>
			<p class="f-right">Created By: Joselito Salazar</p>
			<div class="clear"></div>
		</div>
		<div class="letter-content">
			<p class="f-right font-bold">&#60;Date of Letter></p>
			<div class="f-left margin-top-20">
				<p class="font-bold"> &#60;Sender Name></p>
				<p class="font-bold"> &#60;Sender Rank></p>
				<p class="font-bold"> &#60;Sender Department></p>
			</div>
			<div class="clear"></div>

			<p class="margin-top-20">Dear <span class="font-bold">&#60;Recepient's Name>,</span></p>
			<p class="margin-top-10">In accordance with the terms of teh Employee Stock Option Plan (<abbr title="Employee Stock Option Plan">ESOP</abbr>
			) of Roxas Holdings, Inc. (<abbr title="Roxas Holdings, Inc.">RHI</abbr>), which was approved by the Securities and Exchange Commission (
			<abbr title="Securities and Exchange Commission">SEC</abbr>) on April
			30, 2014, the company offers you an option to subscribe and purchange <span class="font-bold">
			&#60;Offer Shares> </span>
			shares of RHI which has been allocated to you over the exercise period of <span class="font-bold">
			&#60;Vesting Years> </span> years commencing from the date of this letter offer under the following conditions:
			</p>

			<ol class="margin-top-20">
				<li class="font-bold">Subscription Price &amp; Terms.</li>
				<p>This subscription price is based on the average price of the RHI shares in the 
				Philippine Stock Exchange for thirty(30) trading days prior to the date of this 
				subscription price is <strong>&#60;Price per Share></strong>.</p>
				<li class="font-bold margin-top-20">Vesting of Option.</li>
				<p>The Option will vest as follows:</p>
			</ol>
			<table class="table-offer">
				<thead>
					<tr>
						<th>Vesting Date</th>
						<th>Vesting Option for</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>At the end of one (1) year from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of two (2) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of three (3) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
				</tbody>
			</table>


		</div>

		<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color">							
							AUDIO LOGS
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body bg-accordion padding-15px">
						<p>Lorem Ipsum</p>
					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CLAIM VESTING RIGHTS</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please indicate Amounth of Shares to Claim</p>
			<div class="margin-top-20">
				<p class="display-inline-mid margin-right-10">Share to Claim:</p>
				<div class="tool-tip display-inline-mid" tt-html="<p>Claimable Shares:</p><p><small>26,208 Shares</small></p>">	
					<input type="text" class="normal display-inline-mid margin-right-10" />
				</div>								
				<p class="display-inline-mid">Shares</p>
			</div>			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>