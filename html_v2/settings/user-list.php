<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left">User List</h1>
			<h2 class="f-left hidden">ESOP View</h2>
			<div class="f-right">
				<button class="btn-normal margin-right-10">Download Templates for ESOP Distribution</button>
				<a href="add-user.php">
					<button class="btn-normal ">Add User</button>
				</a>
			</div>
			
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>
					<td class="white-color">Search</td>
					<td class="padding-left-20 white-color">Company</td>
					<td class="padding-left-20 white-color">User Role:</td>
				</tr>
				<tr>
					<td>
						<div class="select display-inline-mid">
							<select>
								<option value="ESOP Name">ESOP Name</option>
								<option value="Vesting Years">Vesting Years</option>
								<option value="Grant Date">Grant Date</option>
								<option value="Share QTY">Share QTY</option>
							</select>
						</div>

						<input type="text" class="search normal display-inline-mid margin-left-10"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</td>
					<td>
						<div class="select margin-left-20 width-165px">
							<select>
								<option value="op1">Company A</option>
								<option value="op2">Company B</option>
								<option value="op3">Company C</option>
							</select>
						</div>

						<div class="select margin-left-10 width-165px">
							<select>
								<option value="op1">Company A</option>
								<option value="op2">Company B</option>
								<option value="op3">Company C</option>
							</select>
						</div>															
					</td>
					<td>
						<div class="select margin-left-10 width-165px">
							<select>
								<option value="user1">User A</option>
								<option value="user2">User B</option>
								<option value="user3">User C</option>
							</select>
						</div>							
					</td>
					<td><button class="btn-normal display-inline-mid margin-left-10">Filter List</button></td>
				</tr>
			<tbody>
		</table>
			
		<div class="text-right-line margin-top-30">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Company</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Username</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Employee No.</a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content padding-top-30">

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>
					<tr>
						<td rowspan="4">
							<div class="img-user">
								<img src="../assets/images/profile/profile.jpg" alt="user profile" />
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
						<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Username:</td>
						<td>DGonzagA</td>
						<td class="text-left">Company:</td>
						<td class="text-left">ROXOL</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">User Role:</td>
						<td>HR Officer</td>
						<td class="text-left">Department:</td>
						<td class="text-left">HR Department</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-user.php">
					<button class="btn-normal">View User</button>
				</a>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>
					<tr>
						<td rowspan="4">
							<div class="img-user">
								<img src="../assets/images/profile/profile.jpg" alt="user profile" />
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
						<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Username:</td>
						<td>DGonzagA</td>
						<td class="text-left">Company:</td>
						<td class="text-left">ROXOL</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">User Role:</td>
						<td>HR Officer</td>
						<td class="text-left">Department:</td>
						<td class="text-left">HR Department</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-user.php">
					<button class="btn-normal">View User</button>
				</a>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>
					<tr>
						<td rowspan="4">
							<div class="img-user">
								<img src="../assets/images/profile/profile.jpg" alt="user profile" />
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
						<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Username:</td>
						<td>DGonzagA</td>
						<td class="text-left">Company:</td>
						<td class="text-left">ROXOL</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">User Role:</td>
						<td>HR Officer</td>
						<td class="text-left">Department:</td>
						<td class="text-left">HR Department</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-user.php">
					<button class="btn-normal">View User</button>
				</a>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>
					<tr>
						<td rowspan="4">
							<div class="img-user">
								<img src="../assets/images/profile/profile.jpg" alt="user profile" />
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
						<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Username:</td>
						<td>DGonzagA</td>
						<td class="text-left">Company:</td>
						<td class="text-left">ROXOL</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">User Role:</td>
						<td>HR Officer</td>
						<td class="text-left">Department:</td>
						<td class="text-left">HR Department</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-user.php">
					<button class="btn-normal">View User</button>
				</a>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>
					<tr>
						<td rowspan="4">
							<div class="img-user">
								<img src="../assets/images/profile/profile.jpg" alt="user profile" />
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
						<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Username:</td>
						<td>DGonzagA</td>
						<td class="text-left">Company:</td>
						<td class="text-left">ROXOL</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">User Role:</td>
						<td>HR Officer</td>
						<td class="text-left">Department:</td>
						<td class="text-left">HR Department</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-user.php">
					<button class="btn-normal">View User</button>
				</a>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>
					<tr>
						<td rowspan="4">
							<div class="img-user">
								<img src="../assets/images/profile/profile.jpg" alt="user profile" />
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
						<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Username:</td>
						<td>DGonzagA</td>
						<td class="text-left">Company:</td>
						<td class="text-left">ROXOL</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">User Role:</td>
						<td>HR Officer</td>
						<td class="text-left">Department:</td>
						<td class="text-left">HR Department</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center">
				<a href="view-user.php">
					<button class="btn-normal">View User</button>
				</a>
			</div>
		</div>

	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>