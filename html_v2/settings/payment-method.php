<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left ">Payment Method List</h1>
			<h2 class="f-left hidden">ESOP View</h2>
			<button class="btn-normal f-right modal-trigger" modal-target="add-payment">Add Payment Method</button>
			<div class="clear"></div>
		</div>

		<table>
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10">
							<select>
								<option value="op1">Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid" />
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</td>
				</tr>
			<tbody>
		</table>

			
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		
		<div class="clear"></div>
		<table class="table-roxas margin-top-20">
			<thead>
				<tr>
					<th>Payment Method Name</th>
					<th>Date Added</th>
					<th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Cash</td>
					<td>October 12, 2014</td>
					<td>
						<a href="#" class="modal-trigger" modal-target="edit-payment">Edit</a><span class="margin-left-20 margin-right-20">|</span><a href="#" class="red-color">Delete</a>
					</td>					
				</tr>
				<tr>
					<td>Check</td>
					<td>October 12, 2014</td>
					<td>
						<a href="#" class="modal-trigger" modal-target="edit-payment">Edit</a><span class="margin-left-20 margin-right-20">|</span><a href="#" class="red-color">Delete</a>
					</td>
				</tr>
				<tr>
					<td>Credit Card</td>
					<td>October 12, 2014</td>
					<td>
						<a href="#" class="modal-trigger" modal-target="edit-payment">Edit</a><span class="margin-left-20 margin-right-20">|</span><a href="#" class="red-color">Delete</a>
					</td>
				</tr>
				
			</tbody>
		</table>
	<div>
	<div class="panel-group text-left margin-top-30">
		<div class="accordion_custom">
			<div class="panel-heading bg-accordion">
				<a href="#">
					<h4 class="panel-title active white-color">							
						AUDIO LOGS
						<i class="change-font fa fa-caret-right font-left"></i>
						<i class="fa fa-caret-down font-right"></i>							
					</h4>
				</a>																	
				<div class="clear"></div>					
			</div>					
			<div class="panel-collapse in">								
				<div class="panel-body bg-accordion padding-15px">
					<p>Lorem Ipsum</p>
				</div>			
			</div>
		</div>	
	</div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-payment">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD PAYMENT METHOD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<p class="display-inline-mid margin-right-10">Payment Name:</p>
			<input type="text" class="normal display-inline-mid width-300px" />
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Add Payment Method</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add ESOP -->
<div class="modal-container" modal-id="edit-payment">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT PAYMENT METHOD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<p class="display-inline-mid margin-right-10">Payment Name:</p>
			<input type="text" class="normal display-inline-mid " value="CASH"/>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>