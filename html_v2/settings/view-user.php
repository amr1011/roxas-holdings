<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		
		<h1 class="f-left">View User</h1>
		<h2 class="f-left hidden">View User</h2>
		<a href="edit-user.php">
			<button class="btn-normal f-right">Edit User</button>
		</a>
		<div class="clear"></div>

		<div class="user-profile">
			<div class=" display-inline-mid upload-view">
				<i class="fa fa-camera  white-color" ></i>
				<p class="white-color">Upload Photo</p>	
				
			</div>
			<table class="display-inline-mid text-left white-color view-user-table">
				<tbody>
					<tr>
						<td colspan="2" class="font-20 width-200px">Joselito Salazar</td>
						<td colspan="2" >User Role:</td>
					</tr>
					<tr>
						<td>Employee No:</td>
						<td>1337</td>
						<td>Username:</td>
						<td>joselito.salazar</td>
					</tr>
					<tr>
						<td>Company:</td>
						<td>ROXOL</td>
						<td>Password:</td>
						<td><p class="bullets-like">*************</p></td>
					</tr>
					<tr>
						<td>Department:</td>
						<td>ISD</td>
						<td>Contact No.:</td>
						<td>+639234567894</td>
					</tr>
					<tr>
						<td>Rank:</td>
						<td>ISD</td>
						<td>Email Address:</td>
						<td>Joselito.Salazar@gmail.com</td>
					</tr>
				</tbody>
			</table>
			
		</div>
	</div>

</section>

<section section-style="content-panel">

	<div class="content">

		<div class="option-box divide-by-3">
			<p class="title">Total Shares Offered</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box divide-by-3">
			<p class="title">Total Shares Taken</p>
			<p class="description">300,000 Shares</p>
		</div>
		<div class="option-box divide-by-3">
			<p class="title">Total Shares Untaken</p>
			<p class="description">200,000 Shares</p>
		</div>
		<div class="text-right-line margin-top-20 margin-bottom-20 margin-right-10">
			<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">ESOP Name <i class="fa fa-chevron-down"></i></a></p>
			<span class="margin-left-10 margin-right-10 white-color">|</span>
			<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Grant Date</a></p>
			
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">ESOP 1</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">ESOP 2</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">ESOP 3</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">E-ESOP</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		
		<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color">							
							AUDIT LOGS
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body bg-accordion padding-15px">
						<p>Lorem Ipsum</p>
					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CLAIM VESTING RIGHTS</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please indicate Amounth of Shares to Claim</p>
			<div class="margin-top-20">
				<p class="display-inline-mid margin-right-10">Share to Claim:</p>
				<div class="tool-tip display-inline-mid" tt-html="<p>Claimable Shares:</p><p><small>26,208 Shares</small></p>">	
					<input type="text" class="normal display-inline-mid margin-right-10" />
				</div>								
				<p class="display-inline-mid">Shares</p>
			</div>			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-normal">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>