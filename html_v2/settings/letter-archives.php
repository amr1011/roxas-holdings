<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content padding-bottom-0">
		
		<div>
			<h1 class="f-left">Letter Archives</h1>
			<h2 class="f-left hidden">Letter Archives</h2>
			<div class="f-right">
				<a href="offer-letters.php">		
					<button class="btn-normal display-inline-mid">Back to Active List</button>
				</a>
			</div>
			<div class="clear"></div>			 
		</div>


		<table>
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10">
							<select>
								<option value="op1">Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid" />
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</td>
				</tr>
			<tbody>
		</table>
		<div class="text-right-line  padding-top-30">	
			<div class="line margin-top-30"></div>
			<div class="content-text">		
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Company Name <i class="fa fa-chevron-down"></i></a></p>						
			</div>
		</div>				
	</div>
</section>

<section section-style="content-panel">
	<div class="content padding-top-30">

		<div class="panel-group text-left margin-top-30">

			<div class="accordion_custom">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color f-left">							
							OFFER LETTER
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
						<p class="f-right white-color">04 LETTERS</p>
						<div class="clear"></div>					
					</a>	
					
						
					
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body padding-15px">
						
						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-archieved-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-letter.php">
									<button class="btn-normal">VIEW LETTER</button>
								</a>
							</div>
						</div>

					</div>			
				</div>
			</div>	


			<div class="accordion_custom margin-top-30">
				<div class="panel-heading bg-accordion">
					<a href="#">
						<h4 class="panel-title active white-color f-left">							
							ACCEPTANCE LETTER
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
						<p class="f-right white-color">04 LETTERS</p>
						<div class="clear"></div>					
					</a>															
				</div>					
				<div class="panel-collapse in">								
					<div class="panel-body padding-15px">
						
						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>

						<div class="data-box divide-by-2">

							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color">Application</h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left">October 20, 2015</td>
										<td class="text-right">Created By:</td>
										<td class="text-center">Aaron Paul P. Cruz</td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="view-letter.php">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>
					</div>			
				</div>
			</div>	
		</div>
	</div>
</section>


<!-- edit letter -->
<div class="modal-container edit-letter" modal-id="add-letter">
	<div class="modal-body max-width-1200 width-1200px ">
		<div class="modal-head ">
			<h4 class="text-left">ADD LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content ">	

			<div class="head">
				<div class="display-inline-mid width-80percent">
					<p class="margin-bottom-5">Title</p>
					<input type="text" class="normal width-100percent" />
				</div>
				<div class="display-inline-mid margin-left-20">
					<p class="margin-bottom-5">Letter Type:</p>
					<div class="select">
						<select>
							<option value="op1">Offer Letter A</option>
							<option value="op2">Offer Letter B</option>
							<option value="op3">Offer Letter B</option>
						</select>
					</div>
				</div>
			</div>

			<div class="big-header">
				<p class="f-left margin-left-10">Date Created: October 30, 2015</p>
				<p class="f-right margin-right-20">Created By: Joselito Salazar</p>
				<div class="clear"></div>
			</div>

			<div class="big-body">

				<div class="function">
					<!-- left - side -->
					<div class="format f-left  ">

					

						<div class="text-files">
							<textarea name="editor1" id="editor1" rows="10" cols="80"></textarea>
						</div>
					</div>							
				</div>

				<div class="letter-nav ">
					<!-- right side  -->

					<div class="tag  ">
						<p class="text-center">TAGS</p>
					</div>					
					
					<div class="menu ">
						<ul>
							<li>ESOP Name</li>
							<li>Offer Shares</li>
							<li>Price / Share</li>
							<li>Total Value of Share</li>
							<li>Vesting Years</li>
							<li>Date of Letter</li>
							<li>Sender Name</li>
							<li>Sender Department</li>
							<li>Recipient Name</li>
							<li>Recipient Rank</li>
							<li>Recipient Department</li>								
							<li></li>								
						</ul>
					</div>

					<div class="clear"></div>

				</div>

			</div>
				
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			
			<button type="button" class="display-inline-mid btn-normal alert-btn">Submit</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>