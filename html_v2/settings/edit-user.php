<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left">Edit User</h1>
			<div class="f-right">
				<a href="view-user.php">
					<button type="button" class="display-inline-mid btn-cancel white-color">Cancel</button>
				</a>
				
				<button type="button" class="display-inline-mid btn-normal margin-left-10 ">Save Changes</button>			
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="upload-photo-ver2 ">
			<i class="fa fa-camera"></i>
			<p>Upload Photo</p>
		</div>

		<table class="add-user-table margin-top-50">
			<tbody>
				<tr>
					<td>EMPLOYEE CODE:</td>
					<td><input type="text" class="normal width-300px" value="1234" /></td>
					<td>FIRST NAME:</td>
					<td><input type="text" class="normal width-300px" value="Aaron Paul"/></td>
				</tr>
				<tr>
					<td>COMPANY</td>
					<td>
						<div class="select width-300px">
							<select>
								<option value="op1" selected>ROXOL</option>
								<option value="op2">Cr8v</option>
							</select>
						</div>
					</td>
					<td>LAST NAME:</td>
					<td><input type="text" class="normal width-300px" value="Labing-Lima" /></td>
				</tr>
				<tr>
					<td>DEPARTMENT:</td>
					<td>
						<div class="select width-300px">
							<select>
								<option value="dept1" selected>ISD</option>
								<option value="dept2">IDS</option>
							</select>
						</div>
					</td>
					<td>PASSWORD:</td>
					<td>
						<div class="password-input">
							<input type="password" class="normal width-300px" value="Random" />
							<i class="fa fa-eye black-color"></i>
						</div>
					</td>
				</tr>
				<tr>
					<td>RANK:</td>
					<td>
						<div class="select width-300px">
							<select>
								<option value="ranka">ISD</option>
								<option value="rankb">IDS</option>
							</select>
						</div>
					</td>
					<td>CONTACT NO:</td>
					<td>
						<div class="select width-100px">
							<select>
								<option value="op1">Mobile</option>
								<option value="op2">House</option>
								<option value="op3">Company</option>
							</select>
						</div>
						<input type="text" class="normal display-inline-mid width-197px" value="+63912 123 4567" />
					</td>
				</tr>
				<tr>
					<td>USER ROLE:</td>
					<td>
						<div class="select width-300px">
							<select>
								<option value="usera">User Role A</option>
								<option value="userb">User Role B</option>
							</select>
						</div>
					</td>
					<td>EMAIL ADDRESS:</td>
					<td><input type="text" class="normal width-300px" value="joselito.salazar@gmail.com" />
						
					</td>
				</tr>
			</tbody>
		</table>

	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>
		<!-- content -->
		<div class="modal-content">
			<div class="error">PLEASE FILL UP FORM</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>