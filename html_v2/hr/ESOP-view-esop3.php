<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<div class="f-right">
				
				<button class="btn-normal modal-trigger" modal-target="send-letter">Send Offer Letter</button>				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<h2 class="f-left">ESOP 1</h2>
		
		<div class="clear"></div>

		<p class="font-20 white-color margin-bottom-10">CACI</p>


		<div class="option-box trio">
			<p class="title">Total Alloted Shares</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">2.49</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>

		<div class="option-box trio">
			<p class="title">No. of Employee Accepted</p>
			<p class="description">22 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Availed</p>
			<p class="description">125,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Unavailed</p>
			<p class="description">125,000 Shares</p>
		</div>

		
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-50 padding-top-30">
			<div class="accordion_custom ">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Employee
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
					<div class="panel-body">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Company Code</th>
									<th>Employee Code</th>
									<th>Department Name</th>
									<th>Employee Name</th>
									<th>Rank / Level</th>
									<th>Alloted Shares</th>
									<th>Total Shares Availed</th>
									<th>Total Amount Availed</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
				
			<div class="text-right-line  margin-bottom-80">				
				<div class="line"></div>								
			</div>


			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Shares Offered</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>VALENCIA, RENATO CRUZ</td>
									<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
								</tr>
							
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- send offer letter -->
<div class="modal-container" modal-id="send-letter">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SEND OFFER LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">			
			<div class="">
				<p class="font-15 font-bold">Are you sure you want to send the offer letters of this ESOP to the employees?</p>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Send Offer Letter</button>
		</div>
		<div class="clear"></div>
	</div>
</div>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>