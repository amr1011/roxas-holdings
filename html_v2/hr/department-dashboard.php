<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">

	
</section>

<section section-style="content-panel">	
	

	<div class="content ">
			
		<div class="margin-bottom-20 margin-top-20">
			<h2>Welcome, <span>Joselito Salazar</span></h2>
			<p class="font-20 white-color">CAPDI. HO</p>
			
			<div class="f-left width-100per">
				<table class="width-100per">
					<tbody>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Price Per Share</p>
									<p class="description">2.49</p>
								</div>
							</td>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Alloted Shares</p>
									<p class="description">15,000.00 Shares</p>
								</div>
							</td>
							<td rowspan="2" class="width-400px">
								<div class="with-txt">
									<div class="svg-cont chart-container">
										<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

										
										<div class="data-container">
											<span data-value="70%">Total Shares Availed</span>										
											<span data-value="30%">Total Shares Unavailed</span>										
										</div>		
										<div class="graph-txt">
											<p class="lower">30 %</p>
											<p class="higher">70 %</p>
										</div>													
									</div>
													
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shared Availed</p>
									<p class="description">12,500,000.00 Shares</p>
								</div>
							</td>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shares Unavailed</p>
									<p class="description">50,000,000 Shares</p>
								</div>
							</td>
						</tr>
					</tbody>


				</table>
				
			</div>
			
			<div class="clear"></div>

			
			
		
			
			

		</div>

		<div class="tab-panel">
			
			<input type="radio" name="tabs" id="toggle-tab1" checked="checked" />
			<label for="toggle-tab1">ESOP 1</label>

			<input type="radio" name="tabs" id="toggle-tab2" />
			<label for="toggle-tab2">ESOP 2</label>

			<input type="radio" name="tabs" id="toggle-tab3" />
			<label for="toggle-tab3">ESOP 3</label>


			<div id="tab1" class="tab">

				<h2 class="margin-top-50 white-color">Total of ESOP 1</h2>
				
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Price per Share</th>
								<th>Total Alloted Shares</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Unavailed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>2.49</td>
								<td>30,000,000 Shares</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tbl-rounded margin-top-30">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>Employee Code</th>
								<th>Employee Name</th>
								<th>Rank / Level</th>
								<th>Total Shares Availed</th>
								<th>Total Amount Availed</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0001</td>
								<td>Joselito Salazar</td>
								<td>Chairman</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0002</td>
								<td>Aaron Paul Labing-Lima</td>
								<td>President</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0003</td>
								<td>Eric Nilo</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>5,000,000</td>
							</tr>
							<tr>
								<td>0004</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							<tr>
								<td>0005</td>
								<td>Adrian Cedrick Lim</td>
								<td>Secretary</td>
								<td>25,000,000</td>
								<td>25,000,000</td>
							</tr>
							
							
						</tbody>
					</table>
				</div>

			
				

			</div>

			<div id="tab2" class="tab">
			
				<h2 class="margin-top-50 white-color">Vesting Rights</h2>
	
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>No.</th>
								<th>Year</th>
								<th>Percentage</th>
								<th>No. of Shares</th>
								<th>Value of Shares</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content">					
								<td colspan="2"></td>
								<td class="combine"><span class="total-text">Total:</span> 100%</td>
								<td>130,144</td>
								<td>Php 324,058.56 </td>
							</tr>
						</tbody>
					</table>
				</div>

		
			</div>

			<div id="tab3" class="tab">

			</div>

		</div>

	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>