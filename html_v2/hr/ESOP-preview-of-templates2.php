<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Share Distribution Template</a>
			</div>
			<div class="f-right">
				<button class="btn-normal margin-right-10">Cancel Upload</button>
				<button class="btn-normal margin-right-10">Upload Another Template</button>
				<button class="btn-normal">Confirm Upload</button>				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	
	<div class="content">
	
		<h2 class="f-left">Preview of ESOP_Template.xls</h2>
		<h2 class="f-right">Granted July 29, 2015</h2>
		<div class="clear"></div>

		<div class="big-tbl-cont">
			<div class="tbl-rounded ">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>No.</th>
						<th>Company Code</th>
						<th>Department Code</th>						
						<th>Department Name</th>						
						<th>Employee Code</th>						
						<th>Employee Name</th>						
						<th>Alloted Shares</th>												
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>						
					</tr>
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
					</tr>			
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
					</tr>		
				</tbody>
			</table>
		</div>
		</div>

		<h2>Error on the Document</h2>
		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Row Number</th>
						<th>Column Name</th>
						<th>Issue</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Employee Name, Employee Code</td>
						<td>Employee Name does not match code</td>						
					</tr>
					<tr>
						<td>1</td>
						<td>Department Name, Employee Code</td>
						<td>Department Name does not match code</td>
					</tr>					
				</tbody>
			</table>
		</div>

		

	<div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">			
			<div class="">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<button class="display-inline-mid btn-normal">Upload File</button>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add ESOP -->
<div class="modal-container" modal-id="edit-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Edit ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">Please fill up form</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>