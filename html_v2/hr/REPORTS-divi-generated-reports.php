<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Employee's Taken Share Report</h1>			
			<div class="clear"></div>

		</div>
		<p class=" white-color margin-bottom-10 margin-top-20">Please Indicate ESOP Name</p>
		<div class="select add-radius width-200px">
			<select>				
				<option value="op1">ESOP 1</option>
				<option value="op2">ESOP 2</option>
				<option value="op3">ESOP 3</option>
				<option value="op4">E-ESOP</option>
			</select>
		</div>
		<button class="btn-normal display-inline-mid margin-left-10">Generate Report</button>

	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div>
			<p class="white-color margin-bottom-5 margin-left-5">Search</p>
			<div class="select add-radius">
				<select>
					<option value="search1">Employee Name</option>
					<option value="search2">Total Shares Offered</option>
					<option value="search3">Total Shares Availed</option>
					<option value="search4">Total Shares Unavailed</option>
				</select>
			</div>
			<input type="text" class="small add-border-radius-5px margin-left-10" />
			<button class="btn-normal display-inline-mid margin-left-10">Seach</button>
		</div>
		<div class="text-right-line margin-top-30">
			<div class="line"></div>			
		</div>
		<div class="margin-top-50 f-right">
			<i class="fa fa-file-pdf-o fa-2x hover-icon margin-right-10"></i>
			<i class="fa fa-print fa-2x hover-icon margin-right-10"></i>
		</div>
		<div class="clear"></div>
		<div class="tbl-rounded margin-top-20">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee Name</th>
						<th>Total Share Offered</th>
						<th>Total Shares Availed</th>
						<th>Total Share Unavailed</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Joselito Salazar</td>
						<td>500,000 Shares</td>
						<td>250,000 Shares</td>
						<td>250,000 Shares</td>
					</tr>
					<tr>
						<td>Joselito Salazar</td>
						<td>500,000 Shares</td>
						<td>250,000 Shares</td>
						<td>250,000 Shares</td>
					</tr>
					
					<tr class="last-content ">						
						<td colspan="4" class="text-right">										
							<p class="display-inline-mid"></p>							
							<p class="font-15 display-inline-mid"></p>																			
													
						</td>					
					</tr>
				</tbody>
			</table>
		</div>
		
		
	<div>
</section>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>