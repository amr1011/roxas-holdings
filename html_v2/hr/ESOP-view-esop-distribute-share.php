<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Distribute Shares</a>
			</div>			
			<div class="clear"></div>
		</div>
		<div class="f-right">
			<button class="btn-cancel margin-right-10">Cancel</button>
			<button class="btn-normal">Finalise Distribution</button>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		
		<div class="error-out margin-top-30">Share's of CACI Employees have exceeded the Total Alloted Shares. 
			Please re-distribute the shares accordingly
		</div>
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content print-statement">

		<div>
			<p class="f-left font-20 white-color">CACI</p>
			<div class="f-right">
				<input type="text" class="large border-10px display-inline-mid" />
				<p class="display-inline-mid font-20 white-color margin-left-10">Allotted Shares</p>
			</div>
			<div class="clear"></div>
		</div>
		
		<div class="long-panel border-10px margin-top-30 bg-other-color">
			<div class="f-left">
				<p class="first-text margin-right-30">Offer Letter</p>
				<div class="select add-radius width-200px">
					<select>
						<option value="letter1">Offer Letter 1</option>
						<option value="letter2">Offer Letter 2</option>
						<option value="letter3">Offer Letter 3</option>
						<option value="letter4">Offer Letter 4</option>
						<option value="letter5">Offer Letter 5</option>
					</select>
				</div>
			</div>
			<div class="f-right">
				<p class="second-text f-left margin-right-50">Acceptance Letter:</p>
				<div class="select add-radius f-left drop-change-color width-200px">
					<select>
						<option value="acc1">Acceptance Letter 1</option>
						<option value="acc2">Acceptance Letter 2</option>
						<option value="acc3">Acceptance Letter 3</option>
						<option value="acc4">Acceptance Letter 4</option>
						<option value="acc5">Acceptance Letter 5</option>
					</select>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>


		<div class="width-100per display-block margin-top-30 bg-last-content border-10px">
			<p class="font-20 white-color padding-top-10 padding-left-30 padding-bottom-10">Employee</p>
		</div>

		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Company Code</th>
						<th>Employee Code</th>
						<th>Department Name</th>
						<th>Employee Name</th>
						<th>Rank / Level</th>
						<th>Alloted Shares</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>0001</td>
						<td>0001</td>
						<td>Office of the President</td>
						<td>Juan Dela Cruz</td>
						<td>Executive</td>
						<td><input type="text" class="small border-10px " /></td>
					</tr>
					<tr>
						<td>0002</td>
						<td>01214</td>
						<td>Corporate Accounting</td>
						<td>Marcelino Cabingao</td>
						<td>Executive</td>
						<td><input type="text" class="small border-10px " /></td>
					</tr>					
				</tbody>
			</table>
		</div>
	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>