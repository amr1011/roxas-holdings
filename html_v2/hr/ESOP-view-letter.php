<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>				
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Joselito Salazar</a>				
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">View Letter</a>				
			</div>			
			<div class="clear"></div>
		</div>
		<h2>Acceptance Offer</h2>
		<div class="f-right">			
			<button class="btn-normal ">Print Letter</button>
		</div>
		<div class="clear"></div>	

	
	</div>
</section>

<section section-style="content-panel">

	<div class="content mother-container">

		<div class="main-container">

			<div class="sub-container">			
				<div class="head-center">
					<i class="fa fa-angle-left"></i>
					<p>Page</p>
					<p class="black-color">1</p>
					<p class="white-color font-20">/</p>
					<p>5</p>
					<i class="fa fa-angle-right"></i>
				</div>				
				<div class="head-right">
					<i class="fa fa-print"></i>
					<i class="fa fa-search-plus"></i>
					<i class="fa fa-search-minus"></i>
				</div>			
				<div class="clear"></div>
			</div>
			<div class="sub-container1">
				<div class="inside-sub big-paper">						
					<p class="f-right">January 05, 2016</p>
					<div class="clear"></div>
					<div>
						<p>Mr. Roman M. De Leon</p>
						<p>SVP, Human Resources &amp; ESOP Administrator</p>
						<p>Roxas Holdings, Inc.</p>
						<p>7th Floor, Cacho-Gonzalez Building</p>
						<p>101 Aguirre St., Legazpi Village</p>
						<p>Makati City, Metro Manila</p>
					</div>
					
					<p class="margin-top-30 text-center">Subject: 
						<span class="font-bold">EMPLOYEE STOCK OPTION PLAN (ESOP)</span>
					</p>

					<p class="margin-top-20">Dear Sir:</p>
					<p class="margin-left-50 margin-top-30">This Refers to the Option offer whick was the subject of your letter 
						dated <span class="txt-under">January 05, 2016</span>
					</p>
					<p class="margin-left-50 margin-top-10">In this regard, Please be informed that I am accepting the Option offer
					 under the terms and conditions of plan. Further, I am exercising my option to: </p>
					<p class="margin-left-50 margin-top-10">(Please check appropriate box)</p>

					<div class="checkbox-rate margin-left-50 margin-top-20">
						<label class="black-color font-15"><input type="checkbox" name="stocks" class="margin-right-10">ALL of my allocated shares totalling <span class="txt-under"> 964,000 </span> shares. </label>
						<br />
						<label class="black-color font-15 margin-top-20"><input type="checkbox" name="stocks" class="margin-right-10">Only ___________ shares from my allocated shares totalling ______________ shares.</label>
					</div>
					
					<p class="margin-left-50 margin-top-10">I am authorizing the company to deduct from my salary tha amount of ten pesos (P 10.00) as acceptance fee. </p>

					<div class="f-right margin-right-50 margin-top-30">

						<p class="margin-bottom-30">Very truly yours,</p>
						<p>______________________________</p>
						<p class="text-center margin-bottom-30">(Printed Name &amp; Signature)
						<p>______________________________</p>
						<p class="text-center margin-bottom-30">(Position)</p>
						<p>______________________________</p>
						<p class="text-center">(Company)</p>
					</div>
					<div class="clear"></div>
					<p class="margin-top-50 margin-left-50">Note: Please submit thru HR Department</p>

					
				</div>
			</div>
		</div>

	</div>

</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>