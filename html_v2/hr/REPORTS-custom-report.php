<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Custom Reports</h1>
			
			<div class="clear"></div>
		</div>
		
		<div>
			<div class="display-inline-mid">
				<p class="white-color margin-bottom-5">Name Type:</p>

				<div class="display-inline-mid">
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="Employee Name">Employee Name</option>
							<option value="ESOP Name">ESOP Name</option>
							
						</select>
					</div>				
				</div>
			</div>
			<div class="display-inline-mid ">
				<p class="white-color margin-bottom-5 margin-left-20">Please Indicate Date Range</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10 ">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Generate Report</button>
				</div>
			</div>
		</div>			

	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">
		<div class="check-cont f-left">
			<p class="font-20 white-color ">Please Indicate Reports Column</p>
			<p class="white-color margin-top-10"><em>ESOP Details: </em></p>

			<table class="report-claim-tbl">
				<tbody>
					<tr>
						<td>							
							<label>
								<input type="checkbox" name="stocks">
									<span >Grant Started</span>
							</label>												
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Subsription Price</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Total Gratuity Grandted</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Average Gratuity Granted</span>
							</label>	
						</td>
					</tr>
					<tr>
						<td>							
							<label>
								<input type="checkbox" name="stocks">
									<span >Value of Shares Availed</span>
							</label>												
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >No. of Shares Offered</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >No. of Shares Re-offered</span>
							</label>	
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >No. of Shares Claimed</span>
							</label>	
						</td>
					</tr>
					<tr>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Employees who Availed</span>
							</label>
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Employees who did not Avail</span>
							</label>
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Expiry Date of Offer</span>
							</label>
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Vesting Years</span>
							</label>
						</td>
					</tr>
					<tr>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span >Date Gratuity Added</span>
							</label>
						</td>
					</tr>
				</tbody>
			</table>

			<p class="white-color"><em>Employee Details:</em></p>

			<div class="display-inline-mid  margin-left-10 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span class="margin-left-10">Employee Number</span>
				</label>												
			</div>

			<div class="display-inline-mid  margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span class="margin-left-10">Email</span>
				</label>												
			</div>
			
			<div class="display-inline-mid  margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span >Username</span>
				</label>												
			</div>
			
			<div class="display-inline-mid  margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span >Company</span>
				</label>												
			</div>
			
			<div class="display-inline-mid margin-left-30 margin-top-20">
				<label>
					<input type="checkbox" name="stocks">
						<span >Rank</span>
				</label>												
			</div>
		
		</div>

		<div class="list-cont f-left">
			<p class="font-20 white-color ">Please Indicate Reports Column</p>
			<div class="margin-top-10">
				<div class="f-right">
					<i class="fa fa-arrow-up font-20 margin-right-5 default-cursor"></i>
					<i class="fa fa-arrow-down font-20 default-cursor"></i>
				</div>
				<div class="clear"></div>
				<div class="item-limit-container">
					<ul>
						<li>1. Date Gratuity Added</li>
						<li>2. ESOP Name</li>
						<li>3. Subscription Price</li>
						<li>4. Total Gratuity Granted</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>		
		<div class="text-right-line margin-top-10">
			<div class="line"></div>
		</div>

		<div class="calendar-icon margin-top-80">
			<i class="fa fa-calendar"></i>
			<h2 class="margin-top-20">Please Supply the Date Range to Generate the Report</h2>
		</div>
	</div>



</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Add ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">Please fill up form</div>
			<table class="width-100percent">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>