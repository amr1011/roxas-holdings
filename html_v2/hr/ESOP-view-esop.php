<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<div class="f-right">
				<button class="btn-normal margin-right-10">Distribute Shares</button>
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Share Distribution Template</button>
				<button class="btn-normal">Edit ESOP</button>				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<h2 class="f-left">ESOP 1</h2>
		<h2 class="f-right">Granted July 29, 2015</h2>
		<div class="clear"></div>

		<p class="font-20 white-color margin-bottom-10">CACI</p>


		<div class="option-box trio">
			<p class="title">Total Alloted Shares</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">2.49</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>

		<div class="option-box trio">
			<p class="title">No. of Employee Accepted</p>
			<p class="description">22 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Availed</p>
			<p class="description">125,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Unavailed</p>
			<p class="description">125,000 Shares</p>
		</div>

		
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-50 padding-top-30">
			<div class="accordion_custom ">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Employee
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Company Code</th>
									<th>Employee Code</th>
									<th>Department Name</th>
									<th>Employee Name</th>
									<th>Rank / Level</th>
									<th>Alloted Shares</th>
									<th>Total Shares Availed</th>
									<th>Total Amount Availed</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
				
			<div class="text-right-line  margin-bottom-80">				
				<div class="line"></div>								
			</div>


			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Shares Offered</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name "ESOP 1"</td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name "ESOP 1"</td>
								</tr>
							
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
		</div>

	<div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>			
			<div class=" margin-top-20">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add ESOP -->
<div class="modal-container" modal-id="edit-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Edit ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">Please fill up form</div>
			<table class="width-100per">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>