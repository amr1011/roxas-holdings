<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<div class="f-right">
				
				<button class="btn-normal margin-right-10">Print Analytics Report</button>				
				<button class="btn-normal modal-trigger" modal-target="upload-payment">Upload Payment Records</button>				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">	
	

	<div class="content ">
		
		<div class="margin-bottom-20 margin-top-20">
			
			
			<div class="f-left width-100per">
				<table class="width-100per">
					<tbody>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Price Per Share</p>
									<p class="description">2.49</p>
								</div>
							</td>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Alloted Shares</p>
									<p class="description">15,000.00 Shares</p>
								</div>
							</td>
							<td rowspan="2" class="width-400px">
								<div class="with-txt">
									<div class="svg-cont chart-container">
										<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

										
										<div class="data-container">
											<span data-value="50%" class="">Total Shares Availed</span>										
											<span data-value="50%" class="margin-top-30">Total Shares Unavailed</span>										
										</div>		
										<div class="graph-txt">
											<p class="lower">50 %</p>
											<p class="higher">50 %</p>
										</div>													
									</div>												
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shared Availed</p>
									<p class="description">12,500,000.00 Shares</p>
								</div>
							</td>						
						</tr>
					</tbody>
				</table>
				
			</div>
			
			<div class="clear"></div>
		</div>

		<div class="text-right-line margin-bottom-60">				
			<div class="line"></div>								
		</div>
		
		<div class="tab-panel">
			
			<input type="radio" name="tabs" id="toggle-tab1" checked="checked" />
			<label for="toggle-tab1">ESOP 1</label>

			<input type="radio" name="tabs" id="toggle-tab2" />
			<label for="toggle-tab2">ESOP 2</label>

			<input type="radio" name="tabs" id="toggle-tab3" />
			<label for="toggle-tab3">ESOP 3</label>


			<div id="tab1" class="tab">

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">2.49</p>
				</div>
				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>
				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>
				

				<div class="text-right-line margin-bottom-60 margin-top-30">				
					<div class="line"></div>								
				</div>
				
				<div class="margin-bottom-20 margin-top-20">
					<p class="font-20  f-left white-color">CACI</p>
					<p class="font-20  f-right white-color">0.00 Alloted Shares</p>
					<div class="clear"></div>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">2.49</p>
				</div>
				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>
				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>

				
				<div class="panel-group text-left margin-top-50 ">
					<div class="accordion_custom ">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">Edit</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">Edit</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">Edit</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">Edit</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">Edit</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
				
					<div class="text-right-line  margin-bottom-80">				
						<div class="line"></div>								
					</div>


					<div class="accordion_custom">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Audit Logs
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
							<div class="panel-body ">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Date of Activiy</th>
											<th>User</th>
											<th>Shares Offered</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>September 10, 2015</td>
											<td>ROXAS, PEDRO OLGADO</td>
											<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
										</tr>
										<tr>
											<td>September 10, 2015</td>
											<td>VALENCIA, RENATO CRUZ</td>
											<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
										</tr>
									
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
				</div>


			</div>

			<div id="tab2" class="tab">
			
				<h2 class="margin-top-50 black-color">Vesting Rights</h2>
	
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>No.</th>
								<th>Year</th>
								<th>Percentage</th>
								<th>No. of Shares</th>
								<th>Value of Shares</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content">					
								<td colspan="2"></td>
								<td class="combine"><span class="total-text">Total:</span> 100%</td>
								<td>130,144</td>
								<td>Php 324,058.56 </td>
							</tr>
						</tbody>
					</table>
				</div>

		
			</div>

			<div id="tab3" class="tab">

			</div>

		</div>

	<div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="upload-payment">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD PAYMENT RECORD TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>			
			<div class=" margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid ">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>