<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP Scoreboard Report</h1>			
			<div class="clear"></div>

		</div>
		<p class=" white-color margin-bottom-10 margin-top-20">Search</p>
		<div class="select add-radius width-200px">
			<select>				
				<option value="op1">ESOP Name</option>
				<option value="op2">ESOP 2</option>
				<option value="op3">ESOP 3</option>
				<option value="op4">E-ESOP</option>
			</select>
		</div>
		<button class="btn-normal display-inline-mid margin-left-10">Search</button>

	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="text-right-line ">
			<div class="line"></div>			
		</div>
		<div class="margin-top-50 f-right">
			<i class="fa fa-file-pdf-o fa-2x hover-icon margin-right-10"></i>
			<i class="fa fa-print fa-2x hover-icon margin-right-10"></i>
		</div>
		<div class="clear"></div>
		<div class="tbl-rounded margin-top-20">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>ESOP Name</th>
						<th>Total Share Quantity</th>
						<th>Price per Share</th>
						<th>Vesting Years</th>
						<th>No. of Employee Offered</th>
						<th>Total Shares Taken</th>
						<th>Total Share Untaken</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>ESOP 1</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td>10,000 Employees</td>
						<td>250,000 Shares</td>
						<td>250,000 Shares</td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td>10,000 Employees</td>
						<td>250,000 Shares</td>
						<td>250,000 Shares</td>
					</tr>
					
					<tr class="last-content ">						
						<td colspan="7" class="text-right">										
							<p class="display-inline-mid"></p>							
							<p class="font-15 display-inline-mid"></p>																			
													
						</td>					
					</tr>
				</tbody>
			</table>
		</div>
		
		
	<div>
</section>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>