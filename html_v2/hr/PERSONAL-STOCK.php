<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Personal Stocks</h1>
			
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Grant Date</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Price per Share</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">ESOP Name</a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Vesting Years <i class="fa fa-chevron-down"></i></a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">

		<div class="grid-content">

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 2</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 3</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>E-ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ROXAS ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>PESO ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 15</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="esop-view.php"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price per Share</th>
						<th>Vesting</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="#">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="#">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="#">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="#">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="#">View ESOP</a></td>
					</tr>
				</tbody>
			</table>



		</div>


	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Add ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error">Please fill up form</div>
			<table class="width-100percent">
				<tbody>
					<tr>
						<td>ESOP Name:</td>
						<td><input type="text" class="normal"/></td>
					</tr>
					<tr>
						<td class="padding-top-10">Grant Date:</td>
						<td>
							<div class="date-picker display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Total Share Quantity:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Shares</p>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Price per Share:</td>
						<td>
							<input type="text" class="normal"/>
							<div class="select xsmall display-inline-mid margin-left-10">
							<select>
								<option value="PHP">PHP</option>
								<option value="USD">USD</option>
								<option value="CAD">CAD</option>
								<option value="HKD">HKD</option>
								<option value="INR">INR</option>
							</select>
						</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Vesting Years:</td>
						<td>
							<input type="text" class="normal"/>
							<p class="display-inline-mid margin-left-10">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me">Cancel</button>
			<span class="display-inline-mid margin-left-10 margin-right-10 font-20">|</span>
			<button type="button" class="display-inline-mid btn-dark">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>