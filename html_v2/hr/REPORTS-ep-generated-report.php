<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Employee Payment Report</h1>			
			<div class="clear"></div>

		</div>
		<div>
			<p class="white-color margin-bottom-5 margin-top-20 ">Date Range</p>
			<div>
				<label class="display-inline-mid ">From</label>
				<div class="date-picker add-radius display-inline-mid margin-left-10">
					<input type="text" data-date-format="MM/DD/YYYY">
					<span class="fa fa-calendar text-center"></span>
				</div>
				<label class="display-inline-mid margin-left-10">To</label>
				<div class="date-picker add-radius display-inline-mid margin-left-10">
					<input type="text" data-date-format="MM/DD/YYYY">
					<span class="fa fa-calendar text-center"></span>
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>
			</div>
		</div>

		<div class="header-effect margin-top-20">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>

					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option> <!-- search me -->
							<option value="Date Added">Date Added</option> <!-- done -->
							<option value="Employee Name">Employee Name</option> 							
							<option value="Vesting Years">Vesting Years</option>  <!-- vesting year -->
							<option value="Payment Type">Payment Type</option>  <!-- payment type -->
							<option value="Payment Value">Payment Value</option> <!-- payment value -->
						</select>
					</div>

					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px">
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

					<div class="display-inline-mid employee-name">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px">
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

					<div class="display-inline-mid vesting-years"> 
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px">
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>

					<div class="display-inline-mid payment-type margin-left-10">
						<div class="select add-radius">
							<select>
								<option value="cash">Cash</option>
								<option value="card">Credit Card</option>
								<option value="check">Check</option>
							</select>
						</div>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
				
				</div>
			</div>
			
			

			<div class="display-inline-mid date-added">
				<p class="white-color margin-bottom-5 margin-left-20">Date Added</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid payment-value">
				<p class="white-color margin-left-20 margin-bottom-5">Price</p>
				
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>


			
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="text-right-line ">
			<div class="line"></div>			
		</div>
		<div class="margin-top-50 f-right">
			<i class="fa fa-file-excel-o fa-2x hover-icon margin-right-10"></i>
			<i class="fa fa-file-pdf-o fa-2x hover-icon margin-right-10"></i>
			<i class="fa fa-print fa-2x hover-icon margin-right-10"></i>
		</div>
		<div class="clear"></div>

		<div class="tbl-rounded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>Date Added</th>
						<th>Employee Name </th>
						<th>ESOP Name</th>
						<th>Vesting Year</th>
						<th>Payment Type</th>
						<th>Payment Value</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>September 10, 2015</td>
						<td>Joselito Salazar</td>
						<td>ESOP 1</td>
						<td>Year 1</td>
						<td>Cash</td>
						<td>100,000 Php</td>
					</tr>
					<tr>
						<td>September 10, 2015</td>
						<td>Aaron Paul Labing-Lima</td>
						<td>StockOptionPlan 1</td>
						<td>Year 2</td>
						<td>Credit Card</td>
						<td>50,000 Php</td>
					</tr>
					
					<tr class="last-content ">						
						<td colspan="6" class="text-right">										
							<p class="display-inline-mid">Total Payments:</p>							
							<p class="font-15 display-inline-mid margin-left-30 margin-right-50">2,600,430.3 Php</p>																			
													
						</td>					
					</tr>
				</tbody>
			</table>
		</div>
		
		
	<div>
</section>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>