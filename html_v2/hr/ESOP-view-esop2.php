<?php include "../construct/header.php"; ?>
<?php include "../construct/top-navi.php"; ?>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="esop-check.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">ESOP 1</a>
			</div>
			<div class="f-right">
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="set-offer-expiration">Set Offer Expiration</button>
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Share Distribution Template</button>
				<button class="btn-normal">Edit Distribute Shares</button>				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<h2 class="f-left">ESOP 1</h2>
		<h2 class="f-right">Granted July 29, 2015</h2>
		<div class="clear"></div>

		<p class="font-20 white-color margin-bottom-10">CACI</p>


		<div class="option-box trio">
			<p class="title">Total Alloted Shares</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Price per Share</p>
			<p class="description">2.49</p>
		</div>
		<div class="option-box trio">
			<p class="title">Vesting Years</p>
			<p class="description">5 Years</p>
		</div>

		<div class="option-box trio">
			<p class="title">No. of Employee Accepted</p>
			<p class="description">22 Employees</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Availed</p>
			<p class="description">125,000 Shares</p>
		</div>
		<div class="option-box trio">
			<p class="title">Total Shared Unavailed</p>
			<p class="description">125,000 Shares</p>
		</div>

		
		<!-- Primary Accordion -->
		<div class="panel-group text-left margin-top-50 padding-top-30">
			<div class="accordion_custom ">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color">							
							Employee
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Company Code</th>
									<th>Employee Code</th>
									<th>Department Name</th>
									<th>Employee Name</th>
									<th>Rank / Level</th>
									<th>Alloted Shares</th>
									<th>Total Shares Availed</th>
									<th>Total Amount Availed</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
								<tr>
									<td>0001</td>
									<td>0001</td>
									<td>Office of the President</td>
									<td>Juan Dela Cruz</td>
									<td>Executive</td>
									<td>10,000</td>
									<td>0.00</td>
									<td>Php 125,000.00</td>
									<td><a href="#">Edit</a></td>
								</tr>
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
				
			<div class="text-right-line  margin-bottom-80">				
				<div class="line"></div>								
			</div>


			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color ">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Shares Offered</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name "ESOP 1"</td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name "ESOP 1"</td>
								</tr>
							
							</tbody>
						</table>

					</div>			
				</div>
			</div>	
		</div>			
	<div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">	
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>		
			<div class="margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<button class="display-inline-mid btn-normal">Upload File</button>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- set offer expiration  -->
<!-- share distribution template -->
<div class="modal-container" modal-id="set-offer-expiration">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SET OFFER EXPIRATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">			
			<div class="error">Date below has already passed. <br />Please choose another date.</div>
			<div class="margin-top-30">
				<p class="f-left margin-right-30">Expiry Date: </p>
				<div class="date-picker add-radius f-right margin-left-10">
					<input type="text" data-date-format="MM/DD/YYYY" class="width-300px">
					<span class="fa fa-calendar text-center"></span>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Set Offer Expiration</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>