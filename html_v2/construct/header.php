<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<link rel="shortcut icon" href="assets/ico/favicon.ico">

		<title>Roxas Holding</title>

		<!-- <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> -->
		
		<!-- Bootstrap core CSS -->
		<link href="../assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="../assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">

		<!-- Core CSS -->
		<link href="../assets/css/global/commons.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="../assets/css/global/header.css" rel="stylesheet">
		<link href="../assets/css/global/section-top-panel.css" rel="stylesheet">
		<link href="../assets/css/global/section-content-panel.css" rel="stylesheet">
		<link href="../assets/css/global/ui-widgets.css" rel="stylesheet">
		<link href="../assets/css/global/unique-widget-style.css" rel="stylesheet">
		<link href="../assets/css/global/extra.css" rel="stylesheet">
		<link href="../assets/css/global/plugin-style.css" rel="stylesheet">
	</head>