<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		
		<h1 class="f-left">View User</h1>
		<h2 class="f-left hidden">View User</h2>
		<a href="SETTINGS-edit-user.php">
			<button class="btn-normal f-right">Edit User</button>
		</a>
		<div class="clear"></div>

		<div class="user-profile">
			<div class=" display-inline-mid upload-view">
				<i class="fa fa-camera  white-color" ></i>
				<p class="white-color">Upload Photo</p>	
				
			</div>
			<table class="display-inline-mid text-left white-color view-user-table">
				<tbody>
					<tr>
						<td colspan="2" class="font-20 width-200px">Joselito Salazar</td>
						<td colspan="2" >User Role:</td>
					</tr>
					<tr>
						<td><strong>Employee No:</strong></td>
						<td>1337</td>
						<td><strong>Username:</strong></td>
						<td>joselito.salazar</td>
					</tr>
					<tr>
						<td><strong>Company:</strong></td>
						<td>ROXOL</td>
						<td><strong>Password:</strong></td>
						<td><p class="bullets-like">*************</p></td>
					</tr>
					<tr>
						<td><strong>Department:</strong></td>
						<td>ISD</td>
						<td><strong>Contact No.:</strong></td>
						<td>+639234567894</td>
					</tr>
					<tr>
						<td><strong>Rank:</strong></td>
						<td>ISD</td>
						<td><strong>Email Address:</strong></td>
						<td>Joselito.Salazar@gmail.com</td>
					</tr>
				</tbody>
			</table>
			
		</div>
	</div>

</section>

<section section-style="content-panel">

	<div class="content">

		<div class="option-box divide-by-3">
			<p class="title">Total Shares Offered</p>
			<p class="description">500,000 Shares</p>
		</div>
		<div class="option-box divide-by-3">
			<p class="title">Total Shares Taken</p>
			<p class="description">300,000 Shares</p>
		</div>
		<div class="option-box divide-by-3">
			<p class="title">Total Shares Untaken</p>
			<p class="description">200,000 Shares</p>
		</div>

		<div class="text-right-line margin-top-20 margin-bottom-70">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">ESOP Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Grant Date</a></p>
			</div>
		</div>
	

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">ESOP 1</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">ESOP 2</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">ESOP 3</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		<div class="data-box divide-by-2">

			<table class="width-100per">
				<tbody>					
					<tr>
						<td colspan="2"><h3 class="font-bold black-color">E-ESOP</h3></td>
						<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
					</tr>
					<tr>
						<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
						<td>500,000 Shares</td>
						<td class="text-right">Total Value of Shares:</td>
						<td class="text-right">500,000 Php</td>
					</tr>				
				</tbody>
			</table>
			<div class="data-hover text-center">
				<button class="btn-normal">View ESOP</button>
			</div>
		</div>

		
		<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Activity Description</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Created User<span class="font-bold">"Joselito Salazar"</span></td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>VALENCIA, RENATO CRUZ</td>
									<td>Update User Role from <span class="font-bold">"HR Officer"</span> to <span class="font-bold">"ESOp Admin"</span></td>
								</tr>							
							</tbody>						
						</table>
					</div>			
				</div>
			</div>
		</div>
	<div>
</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>