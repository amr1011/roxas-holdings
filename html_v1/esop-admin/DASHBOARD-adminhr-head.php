<?php include "../construct/header.php"; ?>


<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>




<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left ">Welcome, Maria Cruz</h1>
			<div class="clear"></div>
		</div>
	</div>
</section>



<section section-style="content-panel">	
	
	<div class="content ">		

		<div class="margin-bottom-20 ">	

			<div class="f-left width-100per">

				<table class="width-100per">
					<tbody>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Alloted Shares</p>
									<p class="description">100,000,000 Shares</p>
								</div>
							</td>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shares Availed</p>
									<p class="description">50,000,000 Shares</p>
								</div>
							</td>
							<td rowspan="2" class="width-400px">
								<div class="with-txt">
									<div class="svg-cont chart-container " >
										<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

										
										<div class="data-container">
											<span data-value="50%" class="">Total Shares Availed</span>										
											<span data-value="50%" class="margin-top-30">Total Shares Unavailed</span>										
										</div>		
										<div class="graph-txt">
											<p class="lower">50 %</p>
											<p class="higher">50 %</p>
										</div>													
									</div>												
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shared Unavailed</p>
									<p class="description">50,000,000.00 Shares</p>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				
			</div>

			
			<div class="clear"></div>
		</div>

		<div class="text-right-line">
			<div class="view-by">				
				<p>View By: 
					<i class="fa fa-th-large grid" ></i>
					<i class="fa fa-bars list" ></i>
				</p>
			</div>
			<div class="line"></div>					
		</div>
		

		<div class="dash-grid margin-top-70">

			<div class="tab-panel">	

				<input type="radio" name="tabs" id="toggle-tab1" checked="checked" />
				<label for="toggle-tab1">ESOP 1</label>

				<input type="radio" name="tabs" id="toggle-tab2" />
				<label for="toggle-tab2">ESOP 2</label>

				<input type="radio" name="tabs" id="toggle-tab3" />
				<label for="toggle-tab3">ESOP 3</label>


				<div id="tab1" class="tab">

					<p class="display-inline-mid margin-right-10">Filter</p>
					<div class="select add-radius margin-left-5 display-inline-mid">
						<select>
							<option value="filter1">All</option>
							<option value="filter2">Per Subsidiaries</option>
						</select>
					</div>
					<h2 class="black-color margin-top-35">Grand Total</h2>
										
					<table class="width-100per">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										
											</div>		

											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>		
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					
							</tr>

						</tbody>
					</table>
					
				

					<div class="text-right-line margin-bottom-75 margin-top-30">				
						<div class="line"></div>								
					</div>

					<h2 class="black-color margin-top-35">CAPDI. HO</h2>
					
					
					<table class="width-100per">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

											</div>		
											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>		
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					
							</tr>

						</tbody>
					</table>

					<div class="text-right-line margin-bottom-70 margin-top-30">				
						<div class="line"></div>								
					</div>

					
					<h2 class="black-color margin-top-30">CACI</h2>
									
					<table class="width-100per margin-bottom-50">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>

								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

											</div>		
											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>

							</tr>

							<tr>

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>	

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					

							</tr>
						</tbody>
					</table>	

					<div class="margin-bottom-30 margin-top-50 color-hide">xx</div>
					
				</div>

				<div id="tab2" class="tab">
				
						<p class="display-inline-mid margin-right-10">Filter</p>
					<div class="select add-radius margin-left-5 display-inline-mid">
						<select>
							<option value="filter1">All</option>
							<option value="filter2">Per Subsidiaries</option>
						</select>
					</div>
					<h2 class="black-color margin-top-35">Grand Total</h2>
										
					<table class="width-100per">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										
											</div>		

											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>		
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					
							</tr>

						</tbody>
					</table>
					
				

					<div class="text-right-line margin-bottom-75 margin-top-30">				
						<div class="line"></div>								
					</div>

					<h2 class="black-color margin-top-35">CAPDI. HO</h2>
					
					
					<table class="width-100per">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

											</div>		
											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>		
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					
							</tr>

						</tbody>
					</table>

					<div class="text-right-line margin-bottom-70 margin-top-30">				
						<div class="line"></div>								
					</div>

					
					<h2 class="black-color margin-top-30">CACI</h2>
									
					<table class="width-100per margin-bottom-50">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>

								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

											</div>		
											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>

							</tr>

							<tr>

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>	

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					

							</tr>
						</tbody>
					</table>	

					<div class="margin-bottom-30 margin-top-50 color-hide">xx</div>
			
				</div>

				<div id="tab3" class="tab">

						<p class="display-inline-mid margin-right-10">Filter</p>
					<div class="select add-radius margin-left-5 display-inline-mid">
						<select>
							<option value="filter1">All</option>
							<option value="filter2">Per Subsidiaries</option>
						</select>
					</div>
					<h2 class="black-color margin-top-35">Grand Total</h2>
										
					<table class="width-100per">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										
											</div>		

											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>		
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					
							</tr>

						</tbody>
					</table>
					
				

					<div class="text-right-line margin-bottom-75 margin-top-30">				
						<div class="line"></div>								
					</div>

					<h2 class="black-color margin-top-35">CAPDI. HO</h2>
					
					
					<table class="width-100per">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

											</div>		
											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>		
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					
							</tr>

						</tbody>
					</table>

					<div class="text-right-line margin-bottom-70 margin-top-30">				
						<div class="line"></div>								
					</div>

					
					<h2 class="black-color margin-top-30">CACI</h2>
									
					<table class="width-100per margin-bottom-50">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description">Php 2,190,000 Shares</p>
									</div>
								</td>

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description">30,000,000 Shares</p>
									</div>
								</td>

								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container graph-redesign margin-left-5">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
											
											<div class="data-container">
												<span data-value="70%" class="">Total Shares Availed</span>										
												<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

											</div>		
											<div class="graph-txt">
												<p class="lower">30 %</p>
												<p class="higher">70 %</p>
											</div>													
										</div>												
									</div>
								</td>

							</tr>

							<tr>

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description">25,000,000.00 Shares</p>
									</div>
								</td>	

								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description">5,000,000.00 Shares</p>
									</div>
								</td>					

							</tr>
						</tbody>
					</table>	

					<div class="margin-bottom-30 margin-top-50 color-hide">xx</div>

				</div>

			</div>
		</div>

		<div class="dash-table margin-top-70">
			
			<div class="margin-bottom-20">
				<p class="display-inline-mid margin-right-10 white-color">Filter</p>
				<div class="select add-radius display-inline-mid">
					<select>
						<option value="all-filter">All</option>
						<option value="per-filter">Per Subsidiaries</option>
					</select>
				</div>
			</div>

			<div class="tab-panel1">
				<input type="radio" name="list" id="toggle-list1" checked="checked" />
				<label for="toggle-list1">ESOP 1</label>

				<input type="radio" name="list" id="toggle-list2" />
				<label for="toggle-list2">ESOP 2</label>

				<input type="radio" name="list" id="toggle-list3" />
				<label for="toggle-list3">ESOP 3</label>
				
				<div id="list1" class="tab">

					<h2 class=" black-color ">Total of ESOP 1</h2>

					<div class="tbl-rounded margin-top-25">
						<table class="table-roxas tbl-display">
							<thead>
								<tr>
									<th>Price Per Share</th>
									<th>Total Alloted Shares</th>
									<th>Total Shares Availed</th>
									<th>Total Shares Unavailed</th>									
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>2,190,000 Shares</td>
									<td>30,000,000 Shares</td>
									<td>25,000,000</td>
									<td>5,000,000</td>								
								</tr>								
							</tbody>
						</table>
					</div>

					<div class="tbl-rounded margin-top-30">
						<table class="table-roxas tbl-display">
							<thead>
								<tr>
									<th>Subsidiaries</th>
									<th>Department</th>
									<th>ESOP Name</th>
									<th>Price / Share</th>									
									<th>Total Alloted Shares</th>
									<th>Total Shares Availed</th>
									<th>Unavailed</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>CAPDI HO.</td>
									<td>Office of the President</td>
									<td>ESOP 1</td>
									<td>2.00</td>								
									<td>30,000,000</td>								
									<td>25,000,000</td>								
									<td>5,000,000</td>								
								</tr>		
								<tr>
									<td>CAPDI HO.</td>
									<td>VP and AVP</td>
									<td>ESOP 2</td>
									<td>2.49</td>								
									<td>50,000,000</td>								
									<td>25,000,000</td>								
									<td>25,000,000</td>								
								</tr>	
								<tr>
									<td>CACI.</td>
									<td>Office of the President</td>
									<td>ESOP 1</td>
									<td>2.00</td>								
									<td>30,000,000</td>								
									<td>25,000,000</td>								
									<td>5,000,000</td>								
								</tr>		
								<tr>
									<td>CACI</td>
									<td>VP and AVP</td>
									<td>ESOP 2</td>
									<td>2.49</td>								
									<td>50,000,000</td>								
									<td>25,000,000</td>								
									<td>5,000,000</td>								
								</tr>									
							</tbody>
						</table>
					</div>
				</div>
					
				<div id="list2" class="tab">

				</div>

				<div id="list3" class="tab">

				</div>
			</div>
		</div>


	</div>

</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>