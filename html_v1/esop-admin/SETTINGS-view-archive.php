<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
				
			<div class="clear"></div>
		</div>
		
		<div class="f-right">			
			<button class="btn-normal display-inline-block margin-right-10 ">Activate Letter</button>						
		</div>
		<div class="clear"></div>

	</div>

</section>

<section section-style="content-panel">

	<div class="content">

		<p class="font-20 white-color">Offer Letter</p>
		<h2 class="margin-bottom-30">Offer Letter v.1 for ROXOL</h2>

			
		<div class="letter-head border-tl-10px border-tr-10px"> 
			<p class="f-left">Date Created: October 30, 2015</p>
			<p class="f-right">Created By: Joselito Salazar</p>
			<div class="clear"></div>
		</div>

		<div class="letter-content border-bl-10px border-br-10px">
			<p class="f-right font-bold">&#60;Date of Letter></p>
			<div class="f-left margin-top-20">
				<p class="font-bold"> &#60;Sender Name></p>
				<p class="font-bold"> &#60;Sender Rank></p>
				<p class="font-bold"> &#60;Sender Department></p>
			</div>
			<div class="clear"></div>

			<p class="margin-top-20">Dear <span class="font-bold">&#60;Recepient's Name>,</span></p>
			<p class="margin-top-10">In accordance with the terms of the Employee Stock Option Plan (<abbr title="
			Employee Stock Option Plan">ESOP</abbr>) of Roxas Holdings,
			Inc. (<abbr title="Roxas Holdings Inc.">RHI</abbr>), which was approved by the Securities and Exchange Commission 
			(<abbr title="Securities and Exchange Commission">SEC</abbr>) on April 30, 2014, 
			the company offers you an option to subscribe and purchange <span class="font-bold"> &#60;Offer Shares> </span>
			shares of RHI which has been allocated to you over the exercise period of <span class="font-bold">
			&#60;Vesting Years> </span> years commencing from the date of this letter offer under the following conditions:
			</p>

			<ol class="margin-top-20">
				<li class="font-bold">Subscription Price &amp; Terms.</li>
				<p>This subscription price is based on the average price of the RHI shares in the 
				Philippine Stock Exchange for thirty(30) trading days prior to the date of this 
				subscription price is <strong>&#60;Price per Share></strong>.</p>
				<li class="font-bold margin-top-20">Vesting of Option.</li>
				<p>The Option will vest as follows:</p>
			</ol>

			<table class="table-offer">
				<thead>
					<tr>
						<th>Vesting Date</th>
						<th>Vesting Option for</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>At the end of one (1) year from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of two (2) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of three (3) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
				</tbody>
			</table>


		</div>

		<!-- place accordion here  -->
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom margin-top-30">

				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color active">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>		

				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activiy</th>
									<th>User</th>
									<th>Shares Offered</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>September 10, 2015</td>
									<td>ROXAS, PEDRO OLGADO</td>
									<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
								</tr>
								<tr>
									<td>September 10, 2015</td>
									<td>VALENCIA, RENATO CRUZ</td>
									<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
								</tr>
							
							</tbody>
						</table>

					</div>			
				</div>
			</div>
		</div>

	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>