<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>


<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">View Company Information</h1>
			
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="margin-top-20 white-color">
			<p>Department Name:</p>
			<p class="margin-top-10">Office of the President</p>
			<p class="margin-top-10">Description:</p>
			<p class="margin-top-10">Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​,
			 morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra
			  adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The 
			  voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. 
			  Nescio an Undead zombies. Sicut malus movie horror.</p>
		</div>
		<div class="text-right-line margin-top-5">
			<div class="line"></div>
			<div class="content-text">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color">Rank <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Rank No.</a></p>					
			</div>
		</div>


		<h2 class="f-left margin-top-50">Rank List</h2>		
		<div class="f-right margin-top-50">
			<a href="COMPANY-LIST-view-dept.php">
				<button type="button" class="display-inline-mid btn-cancel close-me color-cancel">Cancel</button>			
			</a>
			<button type="button" class="display-inline-mid btn-normal margin-left-10">Save Changes</button>
		</div>
		<div class="clear"></div>
		
		<div class="tbl-rounded">
			<table class="table-roxas comp-info margin-top-30 tbl-display">
				<thead>
					<tr>
						<th class="width-200px">No</th>
						<th>Rank</th>
						<th class="width-150px">No. of Employee</th>					
					</tr>
				</thead>		
				<tbody>
					<tr>
						<td>1st</td>
						<td><input type="text" class="normal text-center width-500px" value="Chairman" /></td>
						<td><input type="text" class="normal text-center" value="10" /></td>
					</tr>
					<tr>
						<td>2nd</td>
						<td><input type="text" class="normal text-center width-500px" value="President &amp; CEO" /></td>
						<td><input type="text" class="normal text-center" value="05" /></td>
					</tr>
				</tbody>
			</table>
		</div>
	
		
	
	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>