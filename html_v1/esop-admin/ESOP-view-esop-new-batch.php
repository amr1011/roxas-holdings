<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../esop-admin/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../esop-admin/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../esop-admin/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../esop-admin/ESOP-CLAIM-claim.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../esop-admin/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a ><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../esop-admin/REPORTS-dividends-report.php">Gratuity</a></li>
						<li><a href="../esop-admin/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../esop-admin/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../esop-admin/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../esop-admin/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../esop-admin/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../esop-admin/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../esop-admin/SETTINGS-offer-letter.php">Offer Letter</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="../esop-admin/PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>



<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="ESOp-esop.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>ESOP 1</a>
			</div>
			<div class="f-right">
				
				<button class="btn-normal margin-right-10">Print Analytics Report</button>				
				<button class="btn-normal modal-trigger margin-right-10" modal-target="upload-payment">Upload Payment Records</button>	
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="esop-batch">Create ESOP Batch</button>
				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">	
	

	<div class="content ">
		
		<div class="margin-bottom-20 margin-top-20">
			
			
			<div class="f-left width-100per">
				<table class="width-100per">
					<tbody>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Price Per Share</p>
									<p class="description">2.49</p>
								</div>
							</td>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Alloted Shares</p>
									<p class="description">15,000.00 Shares</p>
								</div>
							</td>
							<td rowspan="2" class="width-400px">
								<div class="with-txt">
									<div class="svg-cont chart-container">
										<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

										
										<div class="data-container">
											<span data-value="50%" class="">Total Shares Availed</span>										
											<span data-value="50%" class="margin-top-30">Total Shares Unavailed</span>										
										</div>		
										<div class="graph-txt">
											<p class="lower">50 %</p>
											<p class="higher">50 %</p>
										</div>													
									</div>												
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="option-box width-tbl">
									<p class="title">Total Shared Availed</p>
									<p class="description">12,500,000.00 Shares</p>
								</div>
							</td>						
						</tr>
					</tbody>
				</table>
				
			</div>
			
			<div class="clear"></div>
		</div>

		<div class="text-right-line margin-bottom-55">				
			<div class="line"></div>								
		</div>
			
		<h2 class="f-left  white-color">ESOP 1</h2>
		<h2 class="f-right  white-color">Granted: September 10, 2015</h2>
		<div class="clear"></div>

		<div class="tab-panel margin-top-20">
			
			<input type="radio" name="tabs" id="toggle-tab1" checked="checked" />
			<label for="toggle-tab1">ESOP 1</label>

			<input type="radio" name="tabs" id="toggle-tab2" />
			<label for="toggle-tab2">ESOP 2</label>

			<input type="radio" name="tabs" id="toggle-tab3" />
			<label for="toggle-tab3">ESOP 3</label>


			<div id="tab1" class="tab">

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">2.49</p>
				</div>
				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>
				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>
				

				<div class="text-right-line margin-bottom-50 margin-top-25">				
					<div class="line"></div>								
				</div>
				
				<div class=" margin-top-20">
					<h2 class="f-left black-color">CACI</h2>					
					<div class="clear"></div>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">2.49</p>
				</div>
				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>
				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>

				
				<div class="panel-group text-left margin-top-50 ">
					
					<div class="accordion_custom ">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="ESOP-view-employee.php">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
				
					<div class="text-right-line  margin-bottom-55">				
						<div class="line"></div>								
					</div>
					
					<h2 class="black-color black-color">CAPDI.HO</h2>

					<div class="option-box trio margin-top-10">
						<p class="title">Total Alloted Shares</p>
						<p class="description">500,000 Shares</p>
					</div>
					
					<div class="option-box trio margin-top-10">
						<p class="title">Price Per Share</p>
						<p class="description">2.49</p>
					</div>
					<div class="option-box trio margin-top-10">
						<p class="title">Vesting Years</p>
						<p class="description">2 Years</p>
					</div>
					<div class="option-box trio">
						<p class="title">No. of Employees Accepted</p>
						<p class="description">53 Employees</p>
					</div>
					<div class="option-box trio">
						<p class="title">Total Shares Availed</p>
						<p class="description">250,000 Shares</p>
					</div>
					
					<div class="option-box trio">
						<p class="title">Total Shares Unavailed</p>
						<p class="description">250,000 Shares</p>
					</div>
						
					<div class="accordion_custom margin-top-30">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
			
					<div class="text-right-line margin-bottom-60 margin-top-25">
						<div class="line"></div>
					</div>

					<div class="margin-bottom-20">
						<h2 class=" black-color f-left margin-top-0 margin-bottom-0">Gratuity List</h2>
						<button type="button" class="btn-normal f-right" > Add Gratuity</button>
						<div class="clear"></div>
					</div>

					<div class="tbl-rounded">
						<table class="table-roxas tbl-display margin-top-20">
							<thead>
								<tr>
									<th>Date Added</th>
									<th>Price per Share</th>
									<th>Total Value of Gratuity Given</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>October 10, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p>Pending Approval</p></td>
									<td>
										<a class="modal-trigger default-cursor" modal-target="approved-grat">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a class="red-color modal-trigger default-cursor" modal-target="reject-grant">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Approved</p></td>
									<td>
										<a href="#">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a href="#" class="red-color">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="red-color">Rejected</p></td>
									<td>
										<a class="modal-trigger" modal-target="reject-reason">View Rejected Reason</a>
									</td>
								</tr>
								<tr>
									<td>September 20, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Gratuity Sent</p></td>
									<td>
									
									</td>
								</tr>
								
								
							</tbody>
						</table>
					</div>

					<div class="text-right-line margin-top-25 margin-bottom-70" >
						<div class="line"></div>
					</div>

					<div class="accordion_custom margin-top-30">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Audit Logs
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
							<div class="panel-body ">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Date of Activiy</th>
											<th>User</th>
											<th>Shares Offered</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>September 10, 2015</td>
											<td>ROXAS, PEDRO OLGADO</td>
											<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
										</tr>
										<tr>
											<td>September 10, 2015</td>
											<td>VALENCIA, RENATO CRUZ</td>
											<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
										</tr>
									
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
				</div>
				


			</div>

			<div id="tab2" class="tab">
			
				<h2 class="margin-top-50 black-color">Vesting Rights</h2>
	
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>No.</th>
								<th>Year</th>
								<th>Percentage</th>
								<th>No. of Shares</th>
								<th>Value of Shares</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content">					
								<td colspan="2"></td>
								<td class="combine"><span class="total-text">Total:</span> 100%</td>
								<td>130,144</td>
								<td>Php 324,058.56 </td>
							</tr>
						</tbody>
					</table>
				</div>

		
			</div>

			<div id="tab3" class="tab">

			</div>

		</div>

	<div>
</section>

<!-- share distribution template -->
<div class="modal-container" modal-id="upload-payment">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD PAYMENT RECORD TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>			
			<div class=" margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid ">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>	
			<a href="ESOP-view-esop-3-direct-preview-template.php">		
				<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
			</a>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- reject gratuity  -->
<div class="modal-container" modal-id="reject-grant">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">REJECT GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<p>Please indicate reason for rejection:</p>
			<textarea class="margin-top-10 border-10px"></textarea>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Reject Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<!-- reject reason  -->
<div class="modal-container" modal-id="reject-reason">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">VIEW REJECTION REASON</h4>
			<div class="modal-close close-me"></div>
		</div>
		
		<div class="modal-content">
			<table>
				<tbody>
					<tr>
						<td class="width-200px">ESOP Name:</td>
						<td>ESOP 1</td>
					</tr>
					<tr>
						<td>Dividend Price per Share</td>
						<td>Php 0.60 / Share</td>
					</tr>
					<tr>
						<td>Date Applied:</td>
						<td>September 30, 2015</td>
					</tr>
					<tr>
						<td>Reason:</td>
					</tr>
				</tbody>
			</table>
			<p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum 
			cerebro. De carne lumbering animata corpora quaeritis. Summus brains
			 sit​​, morbo vel maleficia? De apocalypsi gorger omero undead 
			 survivor dictum mauris. 
			 </p>
		</div>				
	</div>
</div>

<!-- create esop batch  -->
<!-- approved gratuity  -->
<div class="modal-container" modal-id="esop-batch">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CREATE ESOP BATCH</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">					
			<table>
				<tbody>
					<tr>
						<td class="width-200px"><p>ESOP Name:</p></td>
						<td><input type="text" class="small add-border-radius-5px"/></td>
					</tr>
					<tr>
						<td><p>Divident Price per Share:</p></td>
						<td class="font-bold">Php 0.60 / Share</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Create Batch</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<!-- approved gratuity  -->
<div class="modal-container" modal-id="approved-grat">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">APPROVED GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<p>Are you sure you want to approve this gratuity application?</p>
			<table>
				<tbody>
					<tr>
						<td class="width-200px"><p>ESOP Name:</p></td>
						<td><p class="font-bold">ESOP 1</p></td>
					</tr>
					<tr>
						<td><p>Divident Price per Share:</p></td>
						<td class="font-bold">Php 0.60 / Share</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Appoved Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add employee -->
<div class="modal-container" modal-id="add-employee">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">ADD EMPLOYEE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error">Price per Share is invalid. Please type number only in the textbox.</div>			
			<table>
				<tbody>
					<tr>
						<td>
							<p>Department</p>
						</td>
						<td>
							<div class="select add-radius width-250px">
								<select >
									<option value="dept1">CACI</option>
									<option value="dept2">CAPDI</option>
									<option value="dept3">CAPDI.HT</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Employee Name:</p>
						</td>
						<td>
							<div class="select add-radius width-250px">
								<select>
									<option value="emp1">Joselito Salazar</option>
									<option value="emp1">Aaron Paul Labing-Isa</option>
								</select>							
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-250px">
								<select>
									<option value="eop1">ESOP 1</option>
									<option value="eop2">ESOP 1.2</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Total Share Unavailed:</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-250px display-inline-mid" />
							<p class="display-inline-mid margin-left-5">Shares</p>
						</td>
					</tr>
					<tr>
						<td>
							<p>Share you want to Availed</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-250px display-inline-mid" />
							<p class="display-inline-mid margin-left-5">Shares</p>
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<label>
								<input type="checkbox" name="stocks">
									<span class="black-color txt-normal font-15">Copy Initial Grant Date, Price per Share and Vesting Years</span></label>
							</label>
						</td>
					</tr>
					<tr>
						<td><p>Grant Date:</p></td>
						<td>
							<div class="date-picker add-radius display-inline-mid margin-left-10">
								<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Price per Share</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-150px">
							<div class="select add-radius width-100px">
								<select>
									<option value="price1">PHP</option>
									<option value="price2">USD</option>
									<option value="price3">CAD</option>
									<option value="price4">HKD</option>
									<option value="price5">INR</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Vesting Years</p>
						</td>
						<td>
							<input type="text" class="small add-border-radius-5px display-inline-mid width-250px">
							<p class="display-inline-mid">Years</p>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="margin-top-20">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Add Employee</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>