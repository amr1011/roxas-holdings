<?php include "../construct/header.php"; ?>

<!-- for navigation  -->
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>						
						<li><a href="../employee/personal-stocks.php">Personal Stocks</a></li>
						<li><a href="../employee/stock-offer.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT	
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="profile-page.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">

	
</section>

<section section-style="content-panel">	

	<div class="content ">			
		<div class="margin-bottom-20 margin-top-20">
			<h2 class="font-400">Welcome, <span>Maria Cruz</span></h2>
		</div>

		<div class="tab-panel">
			
			<input type="radio" name="tabs" id="toggle-tab1" checked="checked" />
			<label for="toggle-tab1">ESOP 1</label>

			<input type="radio" name="tabs" id="toggle-tab2" />
			<label for="toggle-tab2">ESOP 2</label>

			<input type="radio" name="tabs" id="toggle-tab3" />
			<label for="toggle-tab3">ESOP 3</label>


			<div id="tab1" class="tab">

			
				<div class="option-box margin-top-15">
					<p class="title">Grant Started</p>
					<p class="description">August 31, 2013</p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">Subscription Price</p>
					<p class="description">Php 6.00 </p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">No. of Shares Availed</p>
					<p class="description">130,144</p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">Value of Shares Availed</p>
					<p class="description">Php 324,058.56 </p>
				</div>

				<h2 class="margin-top-35 black-color font-400">Vesting Rights</h2>
	
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>No.</th>
								<th>Year</th>
								<th>Percentage</th>
								<th>No. of Shares</th>
								<th>Value of Shares</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content">					
								<td colspan="2"></td>
								<td class="combine"><span class="total-text">Total:</span> 100%</td>
								<td>130,144</td>
								<td>Php 324,058.56 </td>
							</tr>
						</tbody>
					</table>
				</div>

				<h2 class="margin-top-35 black-color font-400">Payment Summary</h2>

				<div class="long-panel border-10px bg-white">
					<p class="first-text">Amount Shares Taken 130,144 | @ 2.40</p>
					<p class="second-text margin-right-100">Php 324, 058.56 </p>
					<div class="clear"></div>
				</div>
				
				<div class="tbl-rounded">
					<table class="table-roxas margin-top-20 tbl-display">
						<thead>
							<tr>
								<th class="width-250px">No.</th>
								<th>Payment Details</th>
								<th>Date Paid</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Gratuity</td>
								<td>October 2, 2013</td>
								<td>18,399.36 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Profit Share</td>
								<td>October 2, 2013</td>
								<td>10,590.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Cash</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Credit Card</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Check</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content ">
								<td colspan="3" class="text-left">
									<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
									<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
								</td>
								<td>
									<p class="font-15 padding-bottom-5">39,001.35 Php</p>
									<p class="font-15">285,057.21 Php</p>
								</td>					
							</tr>
						</tbody>
					</table>
				</div>
				

			</div>

			<div id="tab2" class="tab">

				<div class="option-box margin-top-15">
					<p class="title">Grant Started</p>
					<p class="description">August 31, 2013</p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">Subscription Price</p>
					<p class="description">Php 6.00 </p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">No. of Shares Availed</p>
					<p class="description">130,144</p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">Value of Shares Availed</p>
					<p class="description">Php 324,058.56 </p>
				</div>

				<h2 class="margin-top-35 black-color font-400">Vesting Rights</h2>
	
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>No.</th>
								<th>Year</th>
								<th>Percentage</th>
								<th>No. of Shares</th>
								<th>Value of Shares</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content">					
								<td colspan="2"></td>
								<td class="combine"><span class="total-text">Total:</span> 100%</td>
								<td>130,144</td>
								<td>Php 324,058.56 </td>
							</tr>
						</tbody>
					</table>
				</div>

				<h2 class="margin-top-35 black-color font-400">Payment Summary</h2>

				<div class="long-panel border-10px bg-white">
					<p class="first-text">Amount Shares Taken 130,144 | @ 2.40</p>
					<p class="second-text margin-right-100">Php 324, 058.56 </p>
					<div class="clear"></div>
				</div>
				
				<div class="tbl-rounded">
					<table class="table-roxas margin-top-20 tbl-display">
						<thead>
							<tr>
								<th class="width-250px">No.</th>
								<th>Payment Details</th>
								<th>Date Paid</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Gratuity</td>
								<td>October 2, 2013</td>
								<td>18,399.36 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Profit Share</td>
								<td>October 2, 2013</td>
								<td>10,590.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Cash</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Credit Card</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Check</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content ">
								<td colspan="3" class="text-left">
									<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
									<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
								</td>
								<td>
									<p class="font-15 padding-bottom-5">39,001.35 Php</p>
									<p class="font-15">285,057.21 Php</p>
								</td>					
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>

			<div id="tab3" class="tab">
				<div class="option-box margin-top-15">
					<p class="title">Grant Started</p>
					<p class="description">August 31, 2013</p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">Subscription Price</p>
					<p class="description">Php 6.00 </p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">No. of Shares Availed</p>
					<p class="description">130,144</p>
				</div>

				<div class="option-box margin-top-15">
					<p class="title">Value of Shares Availed</p>
					<p class="description">Php 324,058.56 </p>
				</div>

				<h2 class="margin-top-35 black-color font-400">Vesting Rights</h2>
	
				<div class="tbl-rounded">
					<table class="table-roxas tbl-display">
						<thead>
							<tr>
								<th>No.</th>
								<th>Year</th>
								<th>Percentage</th>
								<th>No. of Shares</th>
								<th>Value of Shares</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Sept. 01, 2013</td>
								<td>20%</td>
								<td>26,028</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content">					
								<td colspan="2"></td>
								<td class="combine"><span class="total-text">Total:</span> 100%</td>
								<td>130,144</td>
								<td>Php 324,058.56 </td>
							</tr>
						</tbody>
					</table>
				</div>

				<h2 class="margin-top-35 black-color font-400">Payment Summary</h2>

				<div class="long-panel border-10px bg-white">
					<p class="first-text">Amount Shares Taken 130,144 | @ 2.40</p>
					<p class="second-text margin-right-100">Php 324, 058.56 </p>
					<div class="clear"></div>
				</div>
				
				<div class="tbl-rounded">
					<table class="table-roxas margin-top-20 tbl-display">
						<thead>
							<tr>
								<th class="width-250px">No.</th>
								<th>Payment Details</th>
								<th>Date Paid</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Gratuity</td>
								<td>October 2, 2013</td>
								<td>18,399.36 Php</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Profit Share</td>
								<td>October 2, 2013</td>
								<td>10,590.72 Php</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Cash</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Credit Card</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Check</td>
								<td>October 2, 2013</td>
								<td>64,809.72 Php</td>
							</tr>
							<tr class="last-content ">
								<td colspan="3" class="text-left">
									<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
									<p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>							
								</td>
								<td>
									<p class="font-15 padding-bottom-5">39,001.35 Php</p>
									<p class="font-15">285,057.21 Php</p>
								</td>					
							</tr>
						</tbody>
					</table>
				</div>

			</div>

		</div>

	<div>
</section>


<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>