<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="PERSONAL-STOCK.php">Personal Stock</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="PERSONAL-STOCK-view.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>Stock Claim Form</a>
			</div>			
			<div class="clear"></div>
		</div>
		<button class="btn-normal f-right">Print Claim Form</button>
		<div class="clear"></div>
		
		
	</div>
</section>

<section section-style="content-panel">

	<div class="content mother-container">

		<div class="main-container">

			<div class="sub-container">			
				<div class="head-center">
					<i class="fa fa-angle-left"></i>
					<p>Page</p>
					<p>1</p>
					<p class="white-color">/</p>
					<p>2</p>
					<i class="fa fa-angle-right"></i>
				</div>				
				<div class="head-right">
					<i class="fa fa-search-plus"></i>
					<i class="fa fa-search-minus"></i>
				</div>			
				<div class="clear"></div>
			</div>
			<div class="sub-container1">
				<div class="inside-sub">						
					<table class="line-tbl">
						<tbody>
							<tr>
								<td><p class="text-center">PART 1 - To be filled out by the employee</p></td>
							</tr>
							<tr>
								<td>
									<div>
										<p class="display-inline-mid">1. OPTION PLAN TYPE</p>
										<div class="checkbox-esop display-inline-mid">
											<label class="black-color"><input type="checkbox" name="option">ESOP 1</label>
											<label class="black-color"><input type="checkbox" name="option">ESOP 2</label>
											<label class="black-color"><input type="checkbox" name="option">ESOP 2 2nd Round</label>									
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<p>2. NUMBER OF MATURE STOCKS: <span class="margin-left-30">43,468.00</span></p>

								</td>
							</tr>
							<tr>
								<td>
									<div>
										<p>3. NUMBER OF STOCKS TO BE CLAIMED</p>
										<p class="margin-left-30">(Check the box of the desired claim)</p>

										<div class="checkbox-rate">
											<label class="black-color"><input type="checkbox" name="stocks"><span>Partial:</span> No. of Shares _________ Amount for Payment _________ </label>
											<label class="black-color"><input type="checkbox" name="stocks"><span>Full:</span> No of Shares <spam class="txt-under"> 43,468.00</spam> Amounth of Payment <span class="txt-under"> P 108,235.32 </span></label>
										</div>
										<div class="text-center margin-top-30 margin-bottom-30">	
											<p>Furthermore, this is to request for the issuance of the above number of stock certificates under my name.</p>
										</div>
										<div class="profile-line-tbl">
											<div class="profile1">
												<p>Marcelino C. Bundoc</p>
												<p>_________________________</p>
												<p>Employee's Name and Signature</p>
											</div>
											<div class="profile2">
												
												<p>_________________________</p>
												<p>Date</p>
											</div>
										</div>
									</div>	
								</td>
							</tr>
						</tbody>
					</table>
					

					<h2 class="black-color margin-top-30">Payment Application</h2>
					<div class="long-panel border-10px margin-top-10">
						<p class="first-text">Year 2</p>
						<p class="first-text margin-left-30">July 28, 2016</p>
						<p class="second-text margin-right-50">With Vesting Rights</p>
						<div class="clear"></div>
					</div>
					<div class="long-panel border-10px margin-top-10">
						<p class="first-text">Amount Shares Taken: 130,144 | @ 2.40</p>
						<p class="second-text margin-right-50">Php 324, 058.56 </p>
						<div class="clear"></div>
					</div>
					
					<div class="tbl-rounded">
						<table class="table-roxas tbl-display margin-top-20">
							<thead>
								<tr>
									<th class="width-250px">No.</th>
									<th>Payment Details</th>
									<th>Date Paid</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Gratuity</td>
									<td>October 2, 2013</td>
									<td>18,399.36 Php</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Profit Share</td>
									<td>October 2, 2013</td>
									<td>10,590.72 Php</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Cash</td>
									<td>October 2, 2013</td>
									<td>64,809.72 Php</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Credit Card</td>
									<td>October 2, 2013</td>
									<td>64,809.72 Php</td>
								</tr>
								<tr>
									<td>5</td>
									<td>Check</td>
									<td>October 2, 2013</td>
									<td>64,809.72 Php</td>
								</tr>
								<tr class="last-content ">
									<td colspan="3" class="text-left">
										<p class="margin-left-30 padding-bottom-5">Total Payments:</p>
										<p class="margin-left-30">Outstanding Balance (as of Tuesday, December 29, 2016)</p>
									</td>
									<td  class="">										
										<p class="font-15 padding-bottom-5">39,001.35 Php</p>																			
										<p class="font-15">285,057.21 Php</p>									
									</td>					
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>

</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>