<?php include "../construct/header.php"; ?>

<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-head/ESOP-esop.php">ESOP list</a></li>
						<li><a href="../hr-head/PERSONAL-STOCK.php">Personal Stocks</a></li>
						<li><a href="../hr-head/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-head/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			<li>
				<a href="../hr-head/COMPANY-LIST.php"><img src="../assets/images/ui/companies-btn.svg"></a>
				<div class="sub-nav">
					<p>Companies</p>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-head/REPORTS-dividend-report.php">Gratuity</a></li>
						<li><a href="../hr-head/REPORTS-employee-payment.php">Employee Payment Record</a></li>
						<li><a href="../hr-head/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-head/REPORTS-esop-scoreboard.php">ESOP Scoreboard</a></li>
						<li><a href="../hr-head/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a><img src="../assets/images/ui/settings-btn.svg"></a>
				<div class="sub-nav">
					<p>Settings</p>
					<ul>
						<li><a href="../hr-head/SETTINGS-user-list.php">User List</a></li>
						<li><a href="../hr-head/SETTINGS-payment-method.php">Payment Method List</a></li>
						<li><a href="../hr-head/SETTINGS-offer-letter.php">Offer Letters</a></li>
					</ul>
				</div>
			</li>
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Gratuity Report</h1>
			
			<div class="clear"></div>
		</div>
		
		<div>			
			<div class="display-inline-mid ">
				<p class="white-color margin-bottom-5">Please Indicate Date Range</p>
				<div>
					<label class="display-inline-mid ">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10 ">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Generate Report</button>
				</div>
			</div>
		</div>	

		<div class="header-effect margin-top-20">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Date Added">Date Added</option>							
							<option value="Share QTY">Price per Share</option>
							<option value="Total Value">Total Value of Gratuity Given</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px">
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px">
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
				</div>
			</div>

			<div class="display-inline-mid date-added" >
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<p class="white-color  margin-bottom-5 margin-left-20">Price per Share</p>
				
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>

			<div class="display-inline-mid total-value">
				<p class="white-color  margin-bottom-5 margin-left-20">Total Value of Gratuity Given</p>
				
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>	
	</div>
</section>

<section section-style="content-panel">
	<div class="content">		
		
		<div class="text-right-line ">
			<div class="line"></div>			
		</div>
		<div class="margin-top-50 f-right">
			<i class="fa fa-file-pdf-o fa-2x hover-icon margin-right-10"></i>
			<i class="fa fa-print fa-2x hover-icon margin-right-10"></i>
		</div>
		<div class="clear"></div>
		<div class="tbl-rounded margin-top-20">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Date Added</th>
						<th>ESOP Name</th>
						<th>Price per Share</th>
						<th>Total Value of Gratuity Given</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>September 10, 2015</td>
						<td>ESOP 1</td>
						<td>0.60 Php / Share</td>
						<td>1,300,215.15 Php</td>
					</tr>
					<tr>
						<td>September 10, 2015</td>
						<td>StockOptionPlan 1</td>
						<td>1.00 Php / Share</td>
						<td>1,300,215.15 Php</td>
					</tr>
					
					<tr class="last-content ">						
						<td colspan="4" class="text-right">										
							<p class="display-inline-mid margin-right-10">Total</p>							
							<p class="font-15 display-inline-mid margin-right-100">2,600,430.3 Php</p>																			
													
						</td>					
					</tr>
				</tbody>
			</table>
		</div>
		
		
	<div>
</section>



<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>