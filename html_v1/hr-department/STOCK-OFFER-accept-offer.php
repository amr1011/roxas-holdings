<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="stock-offer.php">Stock Offers</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a>ESOP 1</a>				
			</div>			
			<div class="clear"></div>
		</div>
		<div class="margin-top-30 margin-bottom-15">
			<h2 class="f-left margin-top-0 margin-bottom-0">Accept Offer</h2>
			<div class="f-right">
				<a href="STOCK-OFFER-view-offer1.php">
					<button class="btn-cancel margin-right-5 color-cancel">Cancel</button>
				</a>
				<button class="btn-normal ">Send Letter</button>
			</div>
			<div class="clear"></div>				
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content mother-container margin-bottom-30">

		<div class="main-container">

			<div class="sub-container">			
				<div class="head-center">
					<i class="fa fa-angle-left"></i>
					<p>Page</p>
					<p class="black-color">1</p>
					<p class="white-color font-20">/</p>
					<p>5</p>
					<i class="fa fa-angle-right"></i>
				</div>				
				<div class="head-right">
					<i class="fa fa-print"></i>
					<i class="fa fa-search-plus"></i>
					<i class="fa fa-search-minus"></i>
				</div>			
				<div class="clear"></div>
			</div>
			<div class="sub-container1">
				<div class="inside-sub big-paper">						
					<p class="f-right">January 05, 2016</p>
					<div class="clear"></div>
					<div>
						<p>Mr. Roman M. De Leon</p>
						<p>SVP, Human Resources &amp; ESOP Administrator</p>
						<p>Roxas Holdings, Inc.</p>
						<p>7th Floor, Cacho-Gonzalez Building</p>
						<p>101 Aguirre St., Legazpi Village</p>
						<p>Makati City, Metro Manila</p>
					</div>
					
					<p class="margin-top-30 text-center">Subject: 
						<span class="font-bold">EMPLOYEE STOCK OPTION PLAN (ESOP)</span>
					</p>

					<p class="margin-top-20">Dear Sir:</p>
					<p class="margin-left-50 margin-top-30">This Refers to the Option offer whick was the subject of your letter 
						dated <span class="txt-under">January 05, 2016</span>
					</p>
					<p class="margin-left-50 margin-top-10">In this regard, Please be informed that I am accepting the Option offer
					 under the terms and conditions of plan. Further, I am exercising my option to: </p>
					<p class="margin-left-50 margin-top-10">(Please check appropriate box)</p>

					<div class="checkbox-rate margin-left-50 margin-top-20">
						<label class="black-color font-15"><input type="checkbox" name="stocks" class="margin-right-10">ALL of my allocated shares totalling <span class="txt-under"> 964,000 </span> shares. </label>
						<br />
						<label class="black-color font-15 margin-top-20"><input type="checkbox" name="stocks" class="margin-right-10">Only ___________ shares from my allocated shares totalling ______________ shares.</label>
					</div>
					
					<p class="margin-left-50 margin-top-10">I am authorizing the company to deduct from my salary tha amount of ten pesos (P 10.00) as acceptance fee. </p>

					<div class="f-right margin-right-50 margin-top-30">

						<p class="margin-bottom-30">Very truly yours,</p>
						<p>______________________________</p>
						<p class="text-center margin-bottom-30">(Printed Name &amp; Signature)
						<p>______________________________</p>
						<p class="text-center margin-bottom-30">(Position)</p>
						<p>______________________________</p>
						<p class="text-center">(Company)</p>
					</div>
					<div class="clear"></div>
					<p class="margin-top-50 margin-left-50">Note: Please submit thru HR Department</p>

					
				</div>
			</div>
		</div>

	</div>

</section>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>