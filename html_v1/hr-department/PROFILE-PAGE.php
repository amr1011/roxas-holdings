<?php include "../construct/header.php"; ?>
<header custom-style="header">
	<img class="w-logo" src="../assets/images/roxas-holdings-logo.png">
	<nav>
		<ul>
			<li>
				<a><img src="../assets/images/ui/esop-btn.svg"></a>
				<div class="sub-nav">
					<p>ESOP</p>
					<ul>
						<li><a href="../hr-department/ESOP-esop-list.php">ESOP list</a></li>
						<li><a href="../hr-department/PERSONAL-STOCK.php">Pesronal Stocks</a></li>
						<li><a href="../hr-department/STOCK-OFFER.php">Stock Offers</a></li>
					</ul>
				</div>
			</li>
			<li>	
				<a href="../hr-department/ESOP-CLAIM-hr.php"><img src="../assets/images/ui/claims-btn.svg"></a>
				<div class="sub-nav">
					<p>Claims</p>
				</div>
			</li>
			
			<li>
				<a href="../reports/dividends-report.php"><img src="../assets/images/ui/reports-btn.svg"></a>
				<div class="sub-nav">
					<p>Reports</p>
					<ul>
						<li><a href="../hr-department/REPORTS-employee-taken-share.php">Employee Taken Share</a></li>
						<li><a href="../hr-department/REPORTS-esop-scoreboard-report.php">ESOP Scoreboard</a></li>						
						<li><a href="../hr-department/REPORTS-employee-payment-report.php">Employee Payment</a></li>
						<li><a href="../hr-department/REPORTS-custom-report.php">Custom Report</a></li>
					</ul>
				</div>
			</li>
			
		</ul>
	</nav>
	<a href="#" class="log">
		LOG OUT
	</a>
	<a href="#" class="bell">
		<i class="fa fa-bell"></i>
	</a>	
	<a href="PROFILE-PAGE.php" class="profile">
		<img src="../assets/images/profile/profile.jpg" class="img-circle"/>
		<p>Maria Cruz</p>		
		<i class="fa fa-caret-down white-color fa-2x"></i>
	</a>

	
	<div class="clear"></div>
</header>

<section section-style="top-panel">
	<div class="content">
		
		<h1 class="f-left margin-top-10">Profile Page</h1>
		<div class="f-right">			
			<button class="btn-normal modal-trigger" modal-target="change-pass">Change Password</button>
		</div>
		<div class="clear"></div>

		<div class="user-profile">
			<div class="text-center margin-top-30">
				<img src="../assets/images/profile/profile.jpg" alt="user-picture" class="add-border-radius-half width-150px" />
			</div>
			<br />
			<div class="text-center white-color">
				<p class="font-20">Maria Cruz</p>
				<p class="font-15">Employee No: <span>132</span></p>
			</div>
			
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content">
		<div class="option-box width-100per padding-20px bg-profile-cont text-left font-15">
			<table class="width-100per">
				<thead>
					<tr >
						<th colspan="4" class="font-20 padding-bottom-20">Company Details</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="width-150px padding-bottom-10">User Role:</td>
						<td class="width-400px padding-bottom-10">ISD</td>
						<td class="width-150px padding-bottom-10">Department:</td>
						<td class="padding-bottom-10">CACI</td>
					</tr>
					<tr>
						<td>Company:</td>
						<td>Roxas</td>
						<td>Rank:</td>
						<td>Executive</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="option-box width-100per padding-20px bg-profile-cont text-left font-15">
			<table class="width-100per">
				<thead>
					<tr >
						<th colspan="4" class="font-20 padding-bottom-20">Account Details</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="width-150px padding-bottom-10">Contact Number:</td>
						<td class="width-400px padding-bottom-10">+639234567894</td>
						<td class="width-150px padding-bottom-10">Username:</td>
						<td class="padding-bottom-10">joselito.salazar</td>
					</tr>
					<tr>
						<td>Email Address: </td>
						<td>joselito.salazar@gmail.com</td>
						<td>Password</td>
						<td>*************** <a href="#">Show</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	
	
	
		
	
	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="change-pass">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CHANGE PASSWORD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error margin-bottom-10">Current Password is not correct. <br /> Please type your current password.</div>
			<div class="success margin-bottom-10">Password Change has been successful</div>
			
			<div class="margin-top-20">
				<p class="f-left margin-top-5 margin-left-10 font-bold">Current Password</p>
				<div class="f-right position-rel">
					<input type="password" class="small add-border-radius-5px width-200px padding-right-40" />
					<i class="fa fa-eye font-20 position-abs top-5 right-10 default-cursor"></i>
				</div>
				<div class="clear"></div>
			</div>
			<div class="margin-top-20">
				<p class="f-left margin-top-5 margin-left-10 font-bold">New Password</p>
				<div class="f-right position-rel">
					<input type="password" class="small add-border-radius-5px width-200px padding-right-40" />
					<i class="fa fa-eye font-20 position-abs top-5 right-10 default-cursor"></i>
				</div>
				<div class="clear"></div>
			</div>
			<div class="margin-top-20">
				<p class="f-left margin-top-5 margin-left-10 font-bold">Confirm New Password</p>
				<div class="f-right position-rel">
					<input type="password" class="small add-border-radius-5px width-200px padding-right-40" />
					<i class="fa fa-eye font-20 position-abs top-5 right-10 default-cursor"></i>
				</div>
				<div class="clear"></div>
			</div>
			

			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal margin-right-10">Change Password</button>
			<button type="button" class="display-inline-mid btn-normal close-me">Close</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<?php include "../construct/bottom-navi.php"; ?>
<?php include "../construct/footer.php"; ?>
