
    <script src="<?php echo base_url(); ?>/assets/js/plugins/piechart.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/commons/main.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/customdropdown.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/bootstrap-datetimepicker.ru.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugins/notifications.js"></script>

    <script>

        if($('.text-files').length > 0){
            CKEDITOR.replace( 'editor1');
            //CKEDITOR.replace( 'editor2');
        }
        
        $("select:not([multiple])").transformDD()
        
        cr8v.stock_offer_notify();

        moment.lang('en');

    </script>
