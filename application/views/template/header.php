
<!-- TO CR8V TEAM 
    THANKS GUYS!!!
    
    #SULSOLORD
    #MAMBAOUT
    #RANDALL BONDOC
 -->


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/ico/favicon.ico">

    <title>{title}</title>

    {meta}
    <meta name="{name}" content="{content}" />
    {/meta}

    {style}
    <link rel="stylesheet" type="text/css" href="{href}" media="{media}">
    {/style}

    {javascript}
    <script src="{src}" type="text/javascript"></script>
    {/javascript}

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>/assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">
    <!-- Core CSS -->
    <link href="<?php echo base_url() ?>/assets/css/global/commons.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>/assets/css/global/header.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/section-top-panel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/section-content-panel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/ui-widgets.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/unique-widget-style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/extra.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/plugin-style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/notification-custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/animate.css" rel="stylesheet">

    <!--JS Constants-->
    <script>
        var oObject = {};

        var iUserID = '<?php echo $this->session->userdata('user_id'); ?>';
        var sUserName = '<?php echo $this->session->userdata('user_name'); ?>';
        var sFullName = '<?php echo $this->session->userdata('full_name'); ?>';
        var iUserRole = '<?php echo $this->session->userdata('user_role'); ?>';
        var iEmployeeCode = '<?php echo $this->session->userdata('employee_code'); ?>';
        var sFirstName = '<?php echo $this->session->userdata('first_name'); ?>';
        var sMiddleName = '<?php echo $this->session->userdata('middle_name'); ?>';
        var sLastName = '<?php echo $this->session->userdata('last_name'); ?>';
        var sEmail = '<?php echo $this->session->userdata('email'); ?>';
        var iIsDeleted = '<?php echo $this->session->userdata('is_deleted'); ?>';
        var sContactNumber = '<?php echo $this->session->userdata('contact_number'); ?>';
        var iContactNumberType = '<?php echo $this->session->userdata('contact_number_type'); ?>';
        var iCompanyID = '<?php echo $this->session->userdata('company_id'); ?>';
        var iDepartmentID = '<?php echo $this->session->userdata('department_id'); ?>';
        var iRankID = '<?php echo $this->session->userdata('rank_id'); ?>';
        var sCompanyName = '<?php echo $this->session->userdata('company_name'); ?>';
        var sDepartmentName = '<?php echo $this->session->userdata('department_name'); ?>';
        var sRankName = '<?php echo $this->session->userdata('rank_name'); ?>';
        var sUserRoleName = '<?php echo $this->session->userdata('user_role_name'); ?>';
        var sImage = '<?php echo $this->session->userdata('img'); ?>';
        var sCurrentDate = '<?php echo  date("Y-m-d H:i:s"); ?>';
        var sOfferLetterType ;
        var arrLogIDS = [] ;
        var iNotificationOffset = 0 ;

        var oUserInfo = {
            "id" : iUserID,
            "user_name" : sUserName,
            "full_name" : sFullName,
            "user_role" : iUserRole,
            "employee_code" : iEmployeeCode,
            "first_name" : sFirstName,
            "middle_name" : sMiddleName,
            "last_name" : sLastName,
            "email" : sEmail,
            "is_deleted" : iIsDeleted,
            "contact_number" : sContactNumber,
            "contact_number_type" : iContactNumberType,
            "company_id" : iCompanyID,
            "department_id" : iDepartmentID,
            "rank_id" : iRankID,
            "company_name" : sCompanyName,
            "department_name" : sDepartmentName,
            "rank_name" : sRankName,
            "user_role_name" : sUserRoleName,
            "img" : sImage,
        }
    </script>

</head>
<body>
<?php $user_role = $this->session->userdata('user_role'); ?>

<?php if($this->session->userdata('user_id') != '') { ?>
    <header custom-style="header">
        <a href="<?php echo base_url(); ?>esop/dashboard">
            <img class="w-logo" src="<?php echo base_url(); ?>/assets/images/roxas-holdings-logo.png">
        </a>
        <nav>
            <ul>
                <li>
                    <a><div class="stock-offer-notify hidden">0</div><img src="<?php echo base_url(); ?>/assets/images/ui/esop-btn.svg"></a>
                    <div class="sub-nav">
                        <p>ESOP</p>
                        <ul>
                            <?php if($user_role == 2 || $user_role == 3 || $user_role == 4) { ?>
                                <li><a href="<?php echo base_url(); ?>esop/esop_list">ESOP list</a></li>
                                <li><a href="<?php echo base_url(); ?>esop/group_wide_stocks">Group Wide Stocks</a></li>
                                <li><a href="<?php echo base_url(); ?>esop/payment_records">Payment Records</a></li>
                                <li><a href="<?php echo base_url(); ?>esop/esop_gratuity_list">Gratuity List</a></li>
                            <?php } ?>
                            <li><a href="<?php echo base_url(); ?>esop/personal_stocks">Personal Stocks</a></li><!-- <?php echo base_url(); ?>esop/personal_stocks -->
                            <li><a href="<?php echo base_url(); ?>esop/stock_offers">Stock Offers<div class="li-stock-offer-notify hidden">0</div></a></li>
                        </ul>
                    </div>

                </li>
                <li>
                <?php if($user_role == 1) { ?>
                    <a><div class="stock-offer-notify hidden">0</div><img src="<?php echo base_url(); ?>/assets/images/ui/claims-btn.svg"></a>
                    <div class="sub-nav">
                        <ul>
                            
                            <li><a href="<?php echo base_url(); ?>claims/employee_claims">Claims</a></li>
                           
                        </ul>
                    </div>
                    
                </li>
                 <?php } ?>
                <?php if($user_role == 2 || $user_role == 3 || $user_role == 4) { ?>
                    <li>    
                        <a><img src="<?php echo base_url(); ?>/assets/images/ui/claims-btn.svg"></a><!-- <?php echo base_url(); ?>claims -->
                        <div class="sub-nav">
                            <a href="<?php echo base_url(); ?>claims/index"><p>Claims</p></a>
                        </div>
                    </li>
                    <?php if($user_role == 3 || $user_role == 4) { ?>
                        <li>
                            <a><img src="<?php echo base_url(); ?>/assets/images/ui/companies-btn.svg"></a>
                            <div class="sub-nav">
                                <a href="<?php echo base_url(); ?>companies/company_list"><p>Companies</p></a>
                            </div>
                        </li>
                    <?php } ?>
                    <li>
                        <a><img src="<?php echo base_url(); ?>/assets/images/ui/reports-btn.svg"></a>
                        <div class="sub-nav">
                            <p>Reports</p>
                            <ul>
                                <?php if($user_role == 3 || $user_role == 4) { ?>
                                    <li><a href="<?php echo base_url(); ?>reports/gratuity">Gratuity</a></li><!-- <?php echo base_url(); ?>reports/gratuity -->
                                <?php } ?>
                                <li><a href="<?php echo base_url(); ?>reports/employee_payment_report">Employee Payment</a></li><!-- <?php echo base_url(); ?>reports/employee_payment_record -->
                                <li><a href="<?php echo base_url(); ?>reports/employee_taken_share">Employee Taken Share</a></li><!-- <?php echo base_url(); ?>reports/employee_taken_share -->
                                <li><a href="<?php echo base_url(); ?>reports/esop_scoreboard">ESOP Scoreboard</a></li><!-- <?php echo base_url(); ?>reports/esop_scoreboard -->
                                <li><a href="<?php echo base_url(); ?>reports/downloadable_reports">Custom Report</a></li><!-- <?php echo base_url(); ?>reports/custom_report -->
                            </ul>
                        </div>
                    </li>
                    <?php if($user_role == 3 || $user_role == 4) { ?>
                        <li>
                            <a><img src="<?php echo base_url(); ?>/assets/images/ui/settings-btn.svg"></a>
                            <div class="sub-nav">
                                <p>Settings</p>
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>users/user_list">User List</a></li>
                                    <li><a href="<?php echo base_url(); ?>settings/payment_method_list">Payment Method List</a></li><!-- <?php echo base_url(); ?>settings/payment_method_list -->
                                    <li><a href="<?php echo base_url(); ?>settings/offer_letter">Offer Letter</a></li><!-- <?php echo base_url(); ?>settings/offer_letter -->
                                </ul>
                            </div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </nav>
        <a href="<?php echo base_url(); ?>auth/logout" class="log">
            LOG OUT
        </a>

        <!-- Start For Notification -->
        <div href="#" class="bell" id="display-notificaitons" style='position:relative; cursor: pointer;'>
            <i class="fa fa-bell"></i>
            <i class="fa fa-circle notif-counts fa-fa2x" aria-hidden="true"><span class="notif-numbers">15</span></i>
            <div class='pop-up notifications-holder'>

                <div class='arrow-up'></div>

                <div class="top-container">
                    <img class="icon" src="https://cr8v.zenhours.com/assets/images/ui/announce_mic.svg">
                    <h4>Notifications</h4>
                </div>

                <div class='notifications display-notif-records'>
                  
                    <div class='contents display-notif-items notif-items'>
                        <a class="notication-view-modal" style="width: 100%;">
                            <img class="profile img-responsive display-notification-user-image" src="">
                            <p  style="width: 70%;">
                                <strong class="display-notification-user">Gale Ann Tenedero</strong> posted 
                                <strong class="display-notification-note">APE</strong>: <span class="display-notification-description">Dear All,Please be informed that all employees of…</span>                                        
                            </p>
                            <p class="time-of-post display-notification-date-created">Wednesday, May 18, 2016 at 06:57 PM</p> 
                        </a>
                    </div>

                </div>
                <div class="see-more" style="">
                    <a href="javascript:void(0)" id="display-notif-view-more" style="display: none; height: 20px;">View More</a>
                </div>        
            </div>
        </div>  
         <!-- Start For Notification -->

        <!--Start Notification Modal -->
        <div class="modal-container" id="notication-view-container-modal">
            <div class="modal-body" id="notication-view-container-modal-body">
               

                    <div class="modal-head">
                        <h4 class="text-left" id="display-selected-notif-note">For Title</h4>
                        <div class="modal-close close-me"></div>
                    </div>

                    <div class="modal-content padding-40px">
                       <div class="notif-image">
                            <img src="<?php echo base_url(); echo $this->session->userdata('img');?>" class="img-circle" id="display-selected-notif-user-img" style="width: 90px;height: 90px;">
                       </div>
                       <div class="notif-user-name">
                            <p id="display-selected-notif-user">For User Name</p>
                       </div>
                       <div class="notif-date-created">
                            <p id="display-selected-notif-date-created">Wednesday, May 18, 2016 at 06:57 PM</p>
                        </div>

                        <div class="notif-description">
                            <p id="display-selected-notif-description">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                                It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                                and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </div>


                    </div>

                    

                    <div class="f-right margin-right-20 margin-bottom-10">
                        <button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Close</button>            
                    </div>
                    <div class="clear"></div>

            </div>
        </div>
        <!-- End Of Notification Modal -->


        <a href="<?php echo base_url(); ?>users/user_profile" class="profile"><!-- <?php echo base_url(); ?>user_profile -->
            <?php if($this->session->userdata('img') != ''){ ?>
                <img src="<?php echo base_url(); echo $this->session->userdata('img');?>" class="img-circle">
            <?php } else { ?>
                <img src="<?php echo base_url(); ?>/assets/images/profile/default_user_image.jpg" class="img-circle">
            <?php } ?>
            <p><?php echo $this->session->userdata('first_name'); ?></p>       
            <!-- <i class="fa fa-caret-down white-color fa-2x"></i> -->
        </a>

        
        <div class="clear"></div>
    </header>

    <style type="text/css" media="screen">
.pop-up > .arrow-up
{
    position: relative;
    margin-top: 5px;
    float: right;
    width: 20px;
    height: 15px;
    background-image: url("<?php echo base_url()?>/assets/images/ui/up-arrow.svg");
    z-index: 1;
}
    </style>
<?php } ?>

<!-- Loading Modal -->
<div class="modal-container" id="show-loading-data-shares">
    <div class="modal-body small" style="width: 100px; height: 100px;">
        <div class="modal-close close-me" style="display: none; "></div>
        <div style="padding-top: 10px;">
            <i class="fa fa-spinner fa-pulse fa-5x fa-fw" style="font-size: 80px;"></i>
        </div>
    </div>
</div>

<!-- end of Loading Modal  -->



