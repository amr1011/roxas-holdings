<?php if ( !defined( 'BASEPATH' ) )
{
    exit( 'No direct script access allowed' );
}


//
class MY_Controller extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
		// get user ip
		// get whitelisted ip
		// compare
		// redirect if not 
    }

    public function get_navigation()
    {

    }
}


class Authenticated_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        /*user authentication can be put here*/

        $this->load->library( 'template' );

        /*load title*/
        $this->template->set_title( 'Roxas Holdings, Inc.' );

        /*load css*/
        // $this->template->add_style( assets_url() . '/css/font-awesome/font-awesome.min.css' );

        /*load scripts*/
        // $this->template->add_script( assets_url() . 'js/plugins/jsPDF-master/dist/jspdf.debug.js' );
        $this->template->add_script( assets_url() . '/js/extras/php.full.min.js' );
        $this->template->add_script( assets_url() . '/js/extras/websocket_events.js' );

        $this->template->add_script( assets_url() . '/js/jQueries/jquery-1.11.1.js' );
        $this->template->add_script( assets_url() . '/js/jQueries/jquery-te-1.4.0.min.js' );
        $this->template->add_script( assets_url() . '/js/jQueries/jquery.tinysort.min.js' );
        $this->template->add_script( assets_url() . '/js/jQueries/jquery.cookie.js' );

        $this->template->add_script( assets_url() . '/js/bootstrap/bootstrap.min.js' );

        $this->template->add_script( assets_url() . '/js/core/platform.js' );
        $this->template->add_script( assets_url() . '/js/core/config.js' );
        $this->template->add_script( assets_url() . '/js/core/memory_storage_engine.js' );
        $this->template->add_script( assets_url() . '/js/core/ajax.js' );
        $this->template->add_script( assets_url() . '/js/core/ui.js' );
        $this->template->add_script( assets_url() . '/js/core/memory_storage_engine.js' );
        $this->template->add_script( assets_url() . '/js/core/errors.js' );

        $this->template->add_script( assets_url() . '/js/plugins/ajaxq.js' );
        $this->template->add_script( assets_url() . '/js/plugins/CconnectionDetector.js' );
        $this->template->add_script( assets_url() . '/js/plugins/jquery.dataTables.min.js' );
        $this->template->add_script( assets_url() . '/js/plugins/jquery.filtertable.min.js' );
        $this->template->add_script( assets_url() . '/js/plugins/integr8formvalidation.js', true );
        $this->template->add_script( assets_url() . '/js/plugins/jquery.twbsPagination.js', true );
        $this->template->add_script( assets_url() . '/js/plugins/jquery.table2excel.js', true );

        $this->template->set_header( 'template/header' );
        $this->template->set_template( 'template/index' );
        $this->template->set_footer( 'template/footer' );
    }

    /*for checking of user roles. if user role has the allowed methods, it wont redirect to esop page.*/
    public function check_user_role(){
        $return = FALSE;

        $user_role = $this->session->userdata('user_role');

        if(isset($user_role) AND strlen($user_role) AND $user_role > 0)
        {
            /*declare the methods to be allowed per user role*/
            if($user_role == 1) // employee
            {
                $user_role_allowed_methods = array(
                    /*claims controller methods*/
                    'claims/view',
                    
                    /*companies controller methods*/
                    /*'companies/company_list',*/ /*'companies/get',*/ /*'companies/add',*/ /*'companies/edit',*/ /*'companies/ajax_upload_image',*/ /*'companies/_upload_image',*/

                    /*deparments controller methods*/
                    /*'departments/department_list',*/ /*'departments/get',*/ /*'departments/add',*/ /*'departments/edit',*/

                    /*esop controller methods*/
                    /*'esop/esop_list',*/
                    'claims/employee_claims','esop/personal_stocks', 'esop/stock_offers', /*'esop/view_esop',*/ /*'esop/add_distribute_shares',*/ /*'esop/edit_distribute_shares',*/ 
                    'esop/get_currency', 'esop/get', 'esop/add', 'esop/edit', 'esop/add_distribute_share', 'esop/edit_distribute_share', 'esop/get_time',
                    'esop/view_stock_offer', 'esop/get_user_stock_offer', 'esop/accept_offer', 'esop/check_offer_expiration', 'esop/add_vesting_rights', 'esop/personal_stock_view',
                    'esop/download_offer_letter', 'esop/download_acceptance_letter', 'esop/claim_form', 'esop/upload_share_distribution_csv', 'esop/open_csv', 'esop/validate_upload_share_distribution',
                    'esop/insert_upload_share_distribution', 'esop/download_share_distribution_template', 'esop/upload_share_distribution', 'esop/update_stock_offer_status', 'esop/view_esop_batch',   
                    'esop/batch_add_distribute_shares', 'esop/batch_edit_distribute_shares', 'esop/batch_upload_share_distribution', 'esop/open_batch_csv', 'esop/add_new_employee', 'esop/get_special',  
                    'esop/dashboard', 'esop/upload_payment_record', 'esop/open_payment_csv', 'esop/download_payment_record_template', 
                    'esop/download_claim_form',

                    /*ranks controller methods*/
                    /*'ranks/rank_list',*/ /*'ranks/get',*/ /*'ranks/add',*/ /*'ranks/edit',*/

                    /*reports controller methods*/
                    /*'reports/gratuity',*/ /*'reports/employee_payment_report',*/ /*'reports/employee_taken_share',*/ /*'reports/esop_scoreboard',*/ /*'reports/custom_report',*/

                    /*settings controller methods*/
                    /*'settings/payment_method_list', 'settings/offer_letter','settings/view_offer_letter',*/ /*'settings/add_offer_letter',*/ /*'settings/update_offer_letter',*/ /*'settings/archive_offer_letter',*/ /*'settings/archive_letter',*/
                    'settings/get_offer_letters',

                    /*users controller methods*/
                    'users/user_profile', /*'users/user_list',*/ /*'users/add_user',*/ /*'users/user_info',*/ /*'users/edit_user',*/ /*'users/get',*/ /*'users/get',*/ /*'users/add',*/ /*'users/edit',*/
                    /*'users/ajax_upload_image',*/ /*'users/_upload_image',*/ 'users/change_password', /*'users/upload_user_list_csv',*/ /* 'users/upload_user_list',*/ /*'users/validate_upload_user_list',*/
                    /*'users/insert_upload_user_list',*/ /*'users/get_users_grouped_by_company',*/ /*'users/open_csv',*/ /*'users/download_user_list_template',*/ /*'users/delete_file',*/ /*'users/delete_all_except',*/   


                );
            }
            else if($user_role == 2) // hr department
            {
                $user_role_allowed_methods = array(
                    /*claims controller methods*/
                    'claims/index','claims/get','claims/view','claims/send_claim_esop_to_admin','claims/ajax_upload_attachment','claims/reject_esop_claim','claims/approve_esop_claim','claims/mark_claim_as_completed',
                    'esop/esop_gratuity_list','esop/payment_records',
                    /*companies controller methods*/
                    /*'companies/company_list',*/ 'companies/get', /*'companies/add',*/ /*'companies/edit',*/ /*'companies/ajax_upload_image',*/ /*'companies/_upload_image',*/

                    /*deparments controller methods*/
                    /*'departments/department_list',*/ 'departments/get', /*'departments/add',*/ /*'departments/edit',*/

                    /*esop controller methods*/
                    'esop/accept_multiple_offer','esop/download_letters','esop/view_group_wide_stock_offers','esop/group_wide_stocks','esop/esop_list', 'esop/personal_stocks', 'esop/stock_offers', 'esop/view_esop', 'esop/add_distribute_shares', 'esop/edit_distribute_shares', 
                    'esop/get_currency', 'esop/get', 'esop/add', 'esop/edit', 'esop/add_distribute_share', 'esop/edit_distribute_share', 'esop/get_time',
                    'esop/view_stock_offer', 'esop/get_user_stock_offer', 'esop/accept_offer', 'esop/check_offer_expiration', 'esop/add_vesting_rights',  'esop/personal_stock_view',
                    'esop/download_offer_letter', 'esop/download_acceptance_letter', 'esop/claim_form', 'esop/upload_share_distribution_csv', 'esop/open_csv', 'esop/validate_upload_share_distribution',
                    'esop/insert_upload_share_distribution', 'esop/download_share_distribution_template', 'esop/upload_share_distribution', 'esop/update_stock_offer_status', 'esop/view_esop_batch', 'esop/view_employee_by_esop_batch', 'esop/add_payment_record',     
                    'esop/batch_add_distribute_shares', 'esop/batch_edit_distribute_shares', 'esop/batch_upload_share_distribution', 'esop/open_batch_csv', 'esop/add_new_employee', 'esop/get_special',  
                    'esop/dashboard', 'esop/upload_payment_record', 'esop/open_payment_csv', 'esop/download_payment_record_template', 
                    'esop/download_claim_form',

                    /*ranks controller methods*/
                    /*'ranks/rank_list',*/ 'ranks/get', /*'ranks/add',*/ /*'ranks/edit',*/

                    /*reports controller methods*/
                    /*'reports/gratuity',*/ 'reports/employee_payment_report', 'reports/employee_taken_share', 'reports/esop_scoreboard', 'reports/custom_report',

                    /*settings controller methods*/
                    /*'settings/payment_method_list', 'settings/offer_letter','settings/view_offer_letter',*/ /*'settings/add_offer_letter',*/ /*'settings/update_offer_letter',*/ /*'settings/archive_offer_letter',*/ /*'settings/archive_letter',*/
                    'settings/get_offer_letters',

                    /*users controller methods*/
                    'users/user_profile', /*'users/user_list',*/ /*'users/add_user',*/ /*'users/user_info',*/ /*'users/edit_user',*/ /*'users/get',*/ /*'users/add',*/ /*'users/edit',*/
                    /*'users/ajax_upload_image',*/ /*'users/_upload_image',*/ 'users/change_password', /*'users/upload_user_list_csv',*/  /*'users/upload_user_list',*/ /*'users/validate_upload_user_list',*/
                    /*'users/insert_upload_user_list',*/ 'users/get_users_grouped_by_company', /*'users/open_csv',*/ /*'users/download_user_list_template',*/ 'users/delete_file', /*'users/delete_all_except',*/   
                    
                    /* downloadable reports*/
                    'reports/generate_esop_summary', 'reports/generate_share_summary', 'reports/generate_subscription_summary', 'reports/generate_claim_summary'
                );
            }
            else if($user_role == 3) // hr head
            {
                $user_role_allowed_methods = array(
                    /*claims controller methods*/
                    'claims/index','claims/get','claims/view','claims/send_claim_esop_to_admin','claims/ajax_upload_attachment','claims/reject_esop_claim','claims/approve_esop_claim','claims/mark_claim_as_completed',
                    'esop/esop_gratuity_list','esop/payment_records',
                    /*companies controller methods*/
                    'companies/company_list', 'companies/get', 'companies/add', 'companies/edit', 'companies/ajax_upload_image', 'companies/_upload_image',

                    /*deparments controller methods*/
                    'departments/department_list', 'departments/get', 'departments/add', 'departments/edit',

                    /*esop controller methods*/
                    'esop/accept_multiple_offer','esop/download_letters','esop/view_group_wide_stock_offers','esop/group_wide_stocks','esop/esop_list', 'esop/personal_stocks', 'esop/stock_offers', 'esop/view_esop', 'esop/add_distribute_shares', 'esop/edit_distribute_shares', 
                    'esop/get_currency', 'esop/get', 'esop/add', 'esop/edit', 'esop/add_distribute_share', 'esop/edit_distribute_share', 'esop/get_time',
                    'esop/view_stock_offer', 'esop/get_user_stock_offer', 'esop/accept_offer', 'esop/check_offer_expiration', 'esop/add_vesting_rights',  'esop/personal_stock_view',
                    'esop/download_offer_letter', 'esop/download_acceptance_letter', 'esop/claim_form', 'esop/upload_share_distribution_csv', 'esop/open_csv', 'esop/validate_upload_share_distribution',
                    'esop/insert_upload_share_distribution', 'esop/download_share_distribution_template', 'esop/upload_share_distribution', 'esop/update_stock_offer_status', 'esop/view_esop_batch', 'esop/view_employee_by_esop_batch', 'esop/add_payment_record',     
                    'esop/batch_add_distribute_shares', 'esop/batch_edit_distribute_shares', 'esop/batch_upload_share_distribution', 'esop/open_batch_csv', 'esop/add_new_employee', 'esop/get_special',  
                    'esop/dashboard', 'esop/upload_payment_record', 'esop/open_payment_csv', 'esop/download_payment_record_template', 
                    'esop/download_claim_form',

                    /*ranks controller methods*/
                    'ranks/rank_list', 'ranks/get', 'ranks/add', 'ranks/edit',

                    /*reports controller methods*/
                    'reports/gratuity', 'reports/employee_payment_report', 'reports/employee_taken_share', 'reports/esop_scoreboard', 'reports/custom_report',

                    /*settings controller methods*/
                    'settings/payment_method_list', 'settings/offer_letter','settings/view_offer_letter','settings/add_offer_letter','settings/update_offer_letter','settings/archive_offer_letter','settings/archive_letter',
                    'settings/get_offer_letters','settings/add_payment_method','settings/update_payment_method','settings/delete_payment_method',

                    /*users controller methods*/
                    'users/user_profile', 'users/user_list', 'users/add_user', 'users/user_info', 'users/edit_user', 'users/get', 'users/get', 'users/add', 'users/edit',
                    'users/ajax_upload_image', 'users/_upload_image', 'users/change_password', 'users/upload_user_list_csv', 'users/upload_user_list',
                    'users/validate_upload_user_list', 'users/insert_upload_user_list', 'users/get_users_grouped_by_company', 'users/open_csv', 'users/download_user_list_template', 'users/delete_file', /*'users/delete_all_except',*/  
                
                    /* downloadable reports*/
                    'reports/generate_esop_summary'
                );
            }
            else if($user_role == 4) // esop admin
            {
                $user_role_allowed_methods = array(
                    /*claims controller methods*/
                    'claims/index','claims/get','claims/view',
                    'esop/esop_gratuity_list','esop/payment_records',
                    /*companies controller methods*/
                    'companies/company_list', 'companies/get', 'companies/add', 'companies/edit', 'companies/ajax_upload_image', 'companies/_upload_image',

                    /*deparments controller methods*/
                    'departments/department_list', 'departments/get', 'departments/add', 'departments/edit',

                    /*esop controller methods*/
                    'esop/accept_multiple_offer','esop/download_letters','esop/view_group_wide_stock_offers','esop/group_wide_stocks','esop/esop_list', 'esop/personal_stocks', 'esop/stock_offers', 'esop/view_esop', 'esop/add_distribute_shares', 'esop/edit_distribute_shares', 
                    'esop/get_currency', 'esop/get', 'esop/add', 'esop/edit', 'esop/add_distribute_share', 'esop/edit_distribute_share', 'esop/get_time',
                    'esop/view_stock_offer', 'esop/get_user_stock_offer', 'esop/accept_offer', 'esop/check_offer_expiration', 'esop/add_vesting_rights',  'esop/personal_stock_view',
                    'esop/download_offer_letter', 'esop/download_acceptance_letter', 'esop/claim_form', 'esop/upload_share_distribution_csv', 'esop/open_csv', 'esop/validate_upload_share_distribution',
                    'esop/insert_upload_share_distribution', 'esop/download_share_distribution_template', 'esop/upload_share_distribution', 'esop/update_stock_offer_status', 'esop/view_esop_batch', 'esop/view_employee_by_esop_batch', 'esop/add_payment_record',   
                    'esop/batch_add_distribute_shares', 'esop/batch_edit_distribute_shares', 'esop/batch_upload_share_distribution', 'esop/open_batch_csv', 'esop/add_new_employee', 'esop/get_special',  
                    'esop/dashboard', 'esop/upload_payment_record', 'esop/open_payment_csv', 'esop/download_payment_record_template', 
                    'esop/download_claim_form',

                    /*ranks controller methods*/
                    'ranks/rank_list', 'ranks/get', 'ranks/add', 'ranks/edit',

                    /*reports controller methods*/
                    'reports/gratuity', 'reports/employee_payment_report', 'reports/employee_taken_share', 'reports/esop_scoreboard', 'reports/custom_report',

                    /*settings controller methods*/
                    'settings/payment_method_list', 'settings/offer_letter','settings/view_offer_letter','settings/add_offer_letter','settings/update_offer_letter','settings/archive_offer_letter','settings/archive_letter',
                    'settings/get_offer_letters','settings/add_payment_method','settings/update_payment_method','settings/delete_payment_method',

                    /*users controller methods*/
                    'users/user_profile', 'users/user_list', 'users/add_user', 'users/user_info', 'users/edit_user', 'users/get', 'users/get', 'users/add', 'users/edit',
                    'users/ajax_upload_image', 'users/_upload_image', 'users/change_password', 'users/upload_user_list_csv', 'users/upload_user_list',
                    'users/validate_upload_user_list', 'users/insert_upload_user_list', 'users/get_users_grouped_by_company', 'users/open_csv', 'users/download_user_list_template', 'users/delete_file', /*'users/delete_all_except',*/  

                    /* downloadable reports*/
                    'reports/generate_esop_summary'
                );
            }

            $current_url = $this->uri->segment(1) ."/". $this->uri->segment(2);

            // foreach ($user_role_allowed_methods as $i => $user_role_allowed_method) {
            //     if(strpos($_SERVER['REQUEST_URI'], $user_role_allowed_method) > 0)
            //     {
            //         $return = TRUE;
            //     }
            // }

            if(in_array($current_url, $user_role_allowed_methods))
            {
                $return = TRUE;
            }
        }

        return $return;
    }
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */