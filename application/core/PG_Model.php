<?php

class PG_Model extends CI_Model
{
	public function __construct()
	{
		parent :: __construct();
	}
	
	/**
	 *	The method that will be used to execute manually the pgqueries
	 *	This will be an override to codeigniter's query method
	 *
	 *	@param: SQL {String}{Required} - The SQL Command that will be executed
	 *
	 *	@return: {stdClass object}
	 *	@structure: 
	 *				{
	 *					'row_array' : Returns the first row of the result set in associative array format
	 *					'result_array' : Returns the entire result set of the query in associative array format,
	 *					'row' : Returns first row of the result set in standard class object format,
	 *					'result' : Returns the entire result set of the query in standard class object format,
	 *					'num_rows' : Returns the number of rows returned by the query
	 *				}
	 *
	 *	@return type: StdClass
	*/
	protected function dbquery ($sql = "")
	{
		/* 
			initialization of the return object
			NOTE: we will be returning stdClass data type to lessen the required changes in the code in our model
		*/
		$return = new StdClass;
		$return->result_array = array();
		$return->row = array();
		$return->row_array = array();
		$return->num_rows = 0;
		
		
		/* 
			validate query. SQL is required.
		*/
		if (strlen($sql) > 0)
		{
			/* 
				Initialization of temporary variables
			*/
			$num_rows = 0; 
			$row_ar = array();
			$result = array();
			
			$q = pg_query($sql); // execute postgresql query
			
			/* 
				result maniputation.
				we'll prepare the the result set
			*/
			while ($row = pg_fetch_assoc($q))
			{	
				$num_rows ++;
				// get row
				if ($num_rows == 1)
				{
					$row_ar = array_push($row_ar, $row); // we'll insert the first value of the result set
				}
				array_push($result,$row); // insert to result
			}
			
			$return->row = (object)$row_ar;    // typecast row_ar to be able to convert it to stdClass
			$return->result = (object)$result; // typecast result array to be able to convert it to stdClass
			$return->result_array = $result;   
			$return->row_array = $row_ar; 
			$return->num_rows = $num_rows;
			
		}
		return $return;
	}
}