<?php

		
	/**
	* get the column name (A,B,C,...)
	* @param string|int $num 	An associative string of initialization num
	* @return string $letter
	*/
	function get_excel_column_name_by_row_index($num = '') 
	{	
	    $numeric = $num % 26;
	    $letter = chr(65 + $numeric);
	    $num2 = intval($num / 26);

	    if ($num2 > 0) 
	    {
	       return get_excel_column_name_by_row_index($num2 - 1) . $letter;
	    } 

	    return $letter;
	}

	/**
	* getting all the columns that has data on the excel
	* @param string $end_column			An associative string of initialization end_column
	* @param string $first_letters		An associative string of initialization first_letters
	* @return array $columns
	*/
	function create_columns_array($end_column ='', $first_letters = '')
	 {
		 $columns = array();
		 $length = strlen($end_column);
		 $letters = range('A', 'Z');

		 // Iterate over 26 letters.
		 foreach ($letters as $letter) 
		 {
			 // Paste the $first_letters before the next.
			 $column = $first_letters . $letter;

			 // Add the column to the final array.
			 $columns[] = $column;

			 // If it was the end column that was added, return the columns.
			 if ($column == $end_column)
			 return $columns;
		 }

		 // Add the column children.
		 foreach ($columns as $column) 
		 {
			 // Don't itterate if the $end_column was already set in a previous itteration.
			 // Stop iterating if you've reached the maximum character length.
			 if (!in_array($end_column, $columns) && strlen($column) < $length) 
			 {
				 $new_columns = create_columns_array($end_column, $column);
				 // Merge the new columns which were created with the final columns array.
				 $columns = array_merge($columns, $new_columns);
			 }
		 }

		 return $columns;
	 }

	/**
	* generates an excel file
	* in using this helper, the param $data should be a two dimensional array, E.g.
	* $data['array_name_1'] => array (
	*	['array_name 2'] => array (
	*			['data_1'] => '',
	*			['data_2'] => ''
	*		)
	*	)
	* styles on excel is already inside the function, just add bold, underline, upperline, etc. on the key of the array_name_2 in example above. E.g. ['bold_underline_array_name_2']
	* there are special function below for specific reports for manipulation of rows and columns or other styles.
	* You can add more styles and special functions if you want to, read comments below for more instruction
	* E.g of $tmp_folder = FCPATH . 'tmp/csv_download/' . $data['company_info']['slug'] . '/';  		->this is where the data will be saved
	* E.g of $filename = "Batch_Deduction_Report" . strtotime(date("y-m-d H:i:s")) . ".xls";			->this is the name of the file
	* E.g of $file = base_url() . 'tmp/csv_download/' . $data['company_info']['slug'] . '/' .$filename;	->this will be the data that should be open once saving is done
	*
	* @param array 	$data 			An associative array of initialization data
	* @param string $tmp_folder 	An associative string of initialization tmp_folder
	* @param string $filename 		An associative string of initialization filename
	* @param string $file 			An associative string of initialization file
	* @return array $return 
	*/
	function excel_generate($data = array(), $tmp_folder ='', $filename ='', $file ='')
	{


		$CI =& get_instance();
				
		$CI->load->library('excel');

		//information of the excel
		$CI->excel->setActiveSheetIndex(0);
		$CI->excel->getProperties() ->setCreator("ZenHours")
									->setLastModifiedBy("ZenHours")
									->setTitle(empty($data['report_type'][0]) ? '(None)' : $data['report_type'][0])
									->setSubject(empty($data['report_type'][0]) ? '(None)' : $data['report_type'][0]); //the parameter of data should have atleast $data['report_type'] with value inorder to have title and subject of the excel
												 
		$CI->excel->getActiveSheet()->setTitle(empty($data['report_type'][0]) ? '(None)' : $data['report_type'][0]);
		
		$counter_merge_top	= 0; //for merging top for the title

		//get data
		if(array_check($data))
		{
			$x 					= 1; //for the rows
			$counter_merge_top	= 0; //for merging top for the title
			$counter_merge_top1	= 0; //for merging top for the title
			$counter_A_loi 		= 0; //for loi function
			$counter_signature  = 0;

			foreach ($data as $key => $value) 
			{	

				$retain_x = 1;	//if value becomes 0, the row will not move	

				//finding values on the key of the 2 dimensional array, if not boolean then configure the design of the specific cell	
				
				//text fonts and style and format
				$find_bold							= strpos($key, 'bold');
				$find_underline						= strpos($key, 'underline');
				$find_upperline						= strpos($key, 'upperline');
				$find_double_underline				= strpos($key, 'doubledline');
				$find_double_upperline				= strpos($key, 'doubleuline');
				$find_center						= strpos($key, 'center');
				$find_left_align					= strpos($key, 'left_align');
				$find_right_align					= strpos($key, 'right_align');
				$find_justify						= strpos($key, 'justify');
				$find_money							= strpos($key, 'money'); //change format of the money
				
				//merging columns (you can add more depending ion how many columns you are need to merge)
				$find_merge_2 						= strpos($key, 'merge2'); //merge two(2) columns
				$find_merge_4						= strpos($key, 'merge4'); //merge two(2) columns
				$find_merge_6						= strpos($key, 'merge6'); //merge two(2) columns
				$find_merge_8						= strpos($key, 'merge8'); //merge two(2) columns
				$find_merge_top						= strpos($key, '#$%#!'); //considered as special function that the 3 rows above will be at the center
				$find_merge_top2					= strpos($key, 'mergetop2'); //considered as special function that the 3 rows above will be at the center
				$find_merge_top1					= strpos($key, 'mergetop1'); //considered as special function that the 5 rows above will be at the center

				//plus the numner of $k then minus to the number coresponds to the key
				$find_plus_one	 					= strpos($key, 'plus_one');
				$find_plus_one_only	 				= strpos($key, 'plus_one_only');
				$find_plus_two	 					= strpos($key, 'plus_two');
				$find_plus_two_only	 				= strpos($key, 'plus_two_only');
				$find_plus_three		 			= strpos($key, 'plus_three'); //with retain x
				$find_plus_three_only				= strpos($key, 'plus_three_only'); //does not retain x
				$find_plus_four_only 				= strpos($key, 'plus_four_only'); //does not retain x
				$find_plus_four 					= strpos($key, 'plus_four');
				$find_plus_five	 					= strpos($key, 'plus_five');
				$find_plus_six	 					= strpos($key, 'plus_six');
				$find_plus_six_only	 				= strpos($key, 'plus_six_only');
				
				//rearanging columns (you can add for other format for column and row manipulation of generating the excel)
				$find_employee_records 				= strpos($key, 'employee_records');
				$find_employee_info 				= strpos($key, 'employee_info');
				$find_total_lates 					= strpos($key, 'total_hours');
				$find_cash_batch_deduct				= strpos($key, 'cash_batch_deduct');
				$find_batch_deduct_total			= strpos($key, 'batch_deduct_total');
				$find_paragraph						= strpos($key, 'paragraph');
				$find_loi_attsubj 					= strpos($key, 'loi_attsubj');
				$find_loi_date_payroll_schedule 	= strpos($key, 'loi_date_payroll_schedule');
				$find_loi_date_credit_date			= strpos($key, 'loi_date_credit_date');
				$find_loi_total						= strpos($key, 'loi_total');
				$find_loi_plus_two_cash				= strpos($key, 'loi_plus_two_cash');
				$find_loi_plus_one_cash				= strpos($key, 'loi_plus_one_cash');
				$find_PSR 							= strpos($key, 'PSR');
				$find_payroll_record				= strpos($key, 'payroll_record');
				$find_footer						= strpos($key, 'footer');
				$find_signature						= strpos($key, 'signature');
				$find_employee_string				= strpos($key, 'employee_string');
				$find_TAR 							= strpos($key, 'TAR');
				$find_not_first						= strpos($key, 'not_first');
				$find_merge_6_plus_one				= strpos($key, 'merge_6_plusone'); 

				$find_aderans_signature1 			= strpos($key, 'aderans_signature1'); 
				$find_aderans_absences_summary 		= strpos($key, 'aderans_absences_summary'); 

				$find_ss_title						= strpos($key, 'ss_title');
				$find_ss_header						= strpos($key, 'ss_header');
				$find_ss_body						= strpos($key, 'ss_body');

				$find_right_border					= strpos($key, 'right_border');

				if(array_check($value))
				{
					foreach ($value as $k => $v) 
					{

						if(!is_bool($find_ss_title))
						{
							$retain_x = 0;
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+1).$x .':' .get_excel_column_name_by_row_index($k+7).$x);
							$styleArray = array(
								    'font' => array(
								        'bold' => true,
								        'underline' => true
								    )
							);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+1).$x)->applyFromArray($styleArray);
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+1).$x, $v);
						}

						if(!is_bool($find_ss_header))
						{
							$retain_x = 0;
							$styleArray = array(
								  'borders' => array(
									    'allborders' => array(
									      'style' => PHPExcel_Style_Border::BORDER_THIN
									    ),
									),
								   'font' => array(
									   'bold' => true
									),
								   'alignment' => array(
							            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							        ),
								    'fill' => array(
							            'type' => PHPExcel_Style_Fill::FILL_SOLID,
							            'color' => array('rgb' => 'FDE9D9')
							        )
								
							);

							if($k == 0)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 1)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+3).$x .':' .get_excel_column_name_by_row_index($k+5).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+3).$x .':' .get_excel_column_name_by_row_index($k+5).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+3).$x, $v);
							}
							elseif($k == 2)
							{

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+5).$x .':' .get_excel_column_name_by_row_index($k+13).$x);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+5).$x .':' .get_excel_column_name_by_row_index($k+13).$x)->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+5).$x, $v);

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+5).($x+1) .':' .get_excel_column_name_by_row_index($k+7).($x+1));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+5).($x+1) .':' .get_excel_column_name_by_row_index($k+7).($x+1))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+5).($x+1), 'CLAIMED/PURCHASED');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+8).($x+1) .':' .get_excel_column_name_by_row_index($k+10).($x+1));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+8).($x+1) .':' .get_excel_column_name_by_row_index($k+10).($x+1))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+8).($x+1), 'UNCLAIMED');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+11).($x+1) .':' .get_excel_column_name_by_row_index($k+13).($x+1));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+11).($x+1) .':' .get_excel_column_name_by_row_index($k+13).($x+1))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+11).($x+1), 'TOTAL');


								//======== CLAIMED/PURCHASED ========//
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+5).($x+2) .':' .get_excel_column_name_by_row_index($k+5).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+5).($x+2) .':' .get_excel_column_name_by_row_index($k+5).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+5))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+5).($x+2), 'Active');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+6).($x+2) .':' .get_excel_column_name_by_row_index($k+6).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+6).($x+2) .':' .get_excel_column_name_by_row_index($k+6).($x+2))->applyFromArray($styleArray);
									$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+6))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+6).($x+2), 'Separated');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+7).($x+2) .':' .get_excel_column_name_by_row_index($k+7).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+7).($x+2) .':' .get_excel_column_name_by_row_index($k+7).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+7))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+7).($x+2), 'Sub-Total');

								//======== UNCLAIMED ========//
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+8).($x+2) .':' .get_excel_column_name_by_row_index($k+8).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+8).($x+2) .':' .get_excel_column_name_by_row_index($k+8).($x+2))->applyFromArray($styleArray);
									$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+8))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+8).($x+2), 'Active');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+9).($x+2) .':' .get_excel_column_name_by_row_index($k+9).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+9).($x+2) .':' .get_excel_column_name_by_row_index($k+9).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+9))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+9).($x+2), 'Separated');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+10).($x+2) .':' .get_excel_column_name_by_row_index($k+10).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+10).($x+2) .':' .get_excel_column_name_by_row_index($k+10).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+10))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+10).($x+2), 'Sub-Total');

								//======== TOTAL ========//
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+11).($x+2) .':' .get_excel_column_name_by_row_index($k+11).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+11).($x+2) .':' .get_excel_column_name_by_row_index($k+11).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+11))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+11).($x+2), 'Active');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+12).($x+2) .':' .get_excel_column_name_by_row_index($k+12).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+12).($x+2) .':' .get_excel_column_name_by_row_index($k+12).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+12))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+12).($x+2), 'Separated');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+13).($x+2) .':' .get_excel_column_name_by_row_index($k+13).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+13).($x+2) .':' .get_excel_column_name_by_row_index($k+13).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+13))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+13).($x+2), 'Sub-Total');
							}
							elseif($k == 3)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+13).$x .':' .get_excel_column_name_by_row_index($k+15).($x+1));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+13).$x .':' .get_excel_column_name_by_row_index($k+15).($x+1))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+13).$x, $v);

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+13).($x+2) .':' .get_excel_column_name_by_row_index($k+13).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+13).($x+2) .':' .get_excel_column_name_by_row_index($k+13).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+13))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+13).($x+2), 'Active');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+14).($x+2) .':' .get_excel_column_name_by_row_index($k+14).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+14).($x+2) .':' .get_excel_column_name_by_row_index($k+14).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+14))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+14).($x+2), 'Separated');

								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+15).($x+2) .':' .get_excel_column_name_by_row_index($k+15).($x+2));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+15).($x+2) .':' .get_excel_column_name_by_row_index($k+15).($x+2))->applyFromArray($styleArray);
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k+15))->setWidth('12');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+15).($x+2), 'Sub-Total');
								$retain_x = 1;
								$x += 2;
							}
							
						}

						if(!is_bool($find_ss_body))
						{
							$retain_x = 0;
							$styleArray = array(
								  'borders' => array(
									    'allborders' => array(
									      'style' => PHPExcel_Style_Border::BORDER_THIN
									    ),
									),
								   'font' => array(
									   'bold' => true
									),
								   'alignment' => array(
							            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
							            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							        ),
								
							);

							$rankStyleArray = array(
								
							);

							if($k == 0)
							{
								$CI->excel->getActiveSheet()->getColumnDimension(get_excel_column_name_by_row_index($k))->setWidth('3');
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 1)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+2).($x));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+2).($x))->applyFromArray($rankStyleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 2)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+2).$x .':' .get_excel_column_name_by_row_index($k+4).($x));
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+2).$x .':' .get_excel_column_name_by_row_index($k+4).($x))->applyFromArray($rankStyleArray);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+2).$x, $v);
							}
							else
							{
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+4).$x, $v);
							}

							if($k > 13)
							{
								$x++;
							}
						}


						if(!is_bool($find_right_border))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}

						//if the key finds a bold, the text will be bold on the excel
						if(!is_bool($find_bold))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getFont()->setBold(true);
						}	

						//if the key finds a underline, the text will have an underline on the excel
						if(!is_bool($find_underline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}	

						//if the key finds a upperline, the text will have an upperline on the excel
						if(!is_bool($find_upperline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}	

						//if the key finds a doubledline, the text will have double underline on the excel
						if(!is_bool($find_double_underline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
						}	

						//if the key finds a doubleuline, the text will have double upperline on the excel
						if(!is_bool($find_double_upperline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
						}

						//if the key finds a center, the text will be aligned in center 
						if(!is_bool($find_center))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						//if the key finds a left_align, the text will be aligned in left 
						if(!is_bool($find_left_align))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						}

						//if the key finds a right_align, the text will be aligned in right 
						if(!is_bool($find_right_align))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						}

						//if the key finds a justify, the text will be aligned justify
						if(!is_bool($find_justify))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
						}

						//if the key finds a doubleuline, the text will have double upperline on the excel
						if(!is_bool($find_money))
						{
							if(!is_bool($find_not_first) AND $k != 0)
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).($x))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
							}elseif(is_bool($find_not_first)){
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).($x))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
							}
						}	

						//merge 2 columns into one
						if (!is_bool($find_merge_2)) 
						{
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						//merge 4 columns into one
						if (!is_bool($find_merge_4)) 
						{
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						//merge 6 columns into one
						if (!is_bool($find_merge_6)) 
						{
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+5).$x );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x );
						}

						//merge 8 columns into one
						if (!is_bool($find_merge_8)) 
						{
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+7).$x );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x );
						}

						

						//merge top
						if (!is_bool($find_merge_top)) 
						{
							$counter_merge_top = 1;
						}

						//merge top
						if (!is_bool($find_merge_top2)) 
						{
							$counter_merge_top = 1;
						}

						//merge top1
						if (!is_bool($find_merge_top1)) 
						{
							$counter_merge_top1 = 1;
						}

						//merge paragraph in loi
						if (!is_bool($find_paragraph)) 
						{
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
						}

						//special function: batch deduct summary
						if (!is_bool($find_cash_batch_deduct)) 
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+2).($x-1))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
						}
						if (!is_bool($find_batch_deduct_total)) 
						{
							if($k != 0)
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).($x))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
							}
						}


						//special function: LOI
						if (!is_bool($find_loi_attsubj)) 
						{
							if(!empty($v))
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+1).($x-1))->getFont()->setBold(true);
							}
						}
						elseif (!is_bool($find_loi_date_payroll_schedule)) 
						{
							if(empty($v))
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+2).$x)->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+2).$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							}
						}
						elseif(!is_bool($find_loi_date_credit_date))
						{
							if(empty($v))
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+2).$x)->getFont()->setBold(true);
							}
						}
						elseif(!is_bool($find_loi_total))
						{
							if(!empty($v))
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).($x))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).($x))->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
							}
						}
						elseif (!is_bool($find_loi_plus_two_cash))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+2).($x-1))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
						}
						elseif (!is_bool($find_loi_plus_one_cash))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+1).($x-1))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
						}

						//special function : if the cell is on the $key employee_info, for underline
						if(!is_bool($find_employee_info))
						{
							if($k == 1 || $k == 4)
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							}
						}

						//special function : payroll record
						if(!is_bool($find_payroll_record))
						{
							if($k == 0){
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getFont()->setBold(true);
							}else{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).($x))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
							}

						}

						//special function : export employee information
						if(!is_bool($find_employee_string))
						{
							if($k == 0)
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).($x))->getNumberFormat()->setFormatCode('000');
							}

						}


						//special function : Time and Attendance Report
						if(!is_bool($find_TAR))
						{	
							if($k == 6)
							{	
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 8)
							{
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							}
							elseif($k == 9)
							{
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							}
							elseif($k == 10)
							{	
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 14)
							{	
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+7).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+7).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+7).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+7).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+7).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 22)
							{	
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+15).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+15).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+15).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+15).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+15).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 38)
							{	
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 42)
							{	
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).($x+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).($x+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
						}
						
						//for data to be put in the excel
						if (!is_bool($find_plus_one))
						{	
							if(is_bool($find_plus_one_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+1).($x-1), $v);
						}
						else if (!is_bool($find_plus_two))
						{	
							if(is_bool($find_plus_two_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+2).($x-1), $v);
						}
						else if (!is_bool($find_plus_three))
						{	
							if(is_bool($find_plus_three_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+3).($x-1), $v);
						}else if (!is_bool($find_aderans_absences_summary))
						{	
							
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+1).($x-1), $v);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+1).($x-1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}

						else if (!is_bool($find_plus_four))
						{	
							if(is_bool($find_plus_four_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+4).($x-1), $v);
						}
						else if (!is_bool($find_plus_five))
						{	
							$retain_x = 0;
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+5).($x-1), $v);
						}
						else if (!is_bool($find_plus_six))
						{	
							if(is_bool($find_plus_six_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+6).($x-1), $v);
						}
						else if (!is_bool($find_merge_6_plus_one)) 
						{
							$retain_x = 0;
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+5).($x-1) .':' .get_excel_column_name_by_row_index($k+9).($x-1) );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+5).($x-1) .':' .get_excel_column_name_by_row_index($k+9).($x-1) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+5).($x-1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+6).($x-1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+7).($x-1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+8).($x-1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+9).($x-1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+5).($x-1), $v);

						}else if (!is_bool($find_aderans_signature1)) 
						{
							
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+3).($x) .':' .get_excel_column_name_by_row_index($k+4).($x) );
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+3).($x+1) .':' .get_excel_column_name_by_row_index($k+4).($x+1) );
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+3).($x+2) .':' .get_excel_column_name_by_row_index($k+4).($x+2) );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+3).($x) .':' .get_excel_column_name_by_row_index($k+4).($x) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+3).($x+1) .':' .get_excel_column_name_by_row_index($k+4).($x+1) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+3).($x+2) .':' .get_excel_column_name_by_row_index($k+4).($x+2) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+3).($x) .':' .get_excel_column_name_by_row_index($k+4).($x) )->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+6).($x) .':' .get_excel_column_name_by_row_index($k+7).($x) );
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+6).($x+1) .':' .get_excel_column_name_by_row_index($k+7).($x+1) );
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+6).($x+2) .':' .get_excel_column_name_by_row_index($k+7).($x+2) );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+6).($x) .':' .get_excel_column_name_by_row_index($k+7).($x) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+6).($x+1) .':' .get_excel_column_name_by_row_index($k+7).($x+1) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+6).($x+2) .':' .get_excel_column_name_by_row_index($k+7).($x+2) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+6).($x) .':' .get_excel_column_name_by_row_index($k+7).($x) )->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+9).($x) .':' .get_excel_column_name_by_row_index($k+10).($x) );
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+9).($x+1) .':' .get_excel_column_name_by_row_index($k+10).($x+1) );
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+9).($x+2) .':' .get_excel_column_name_by_row_index($k+10).($x+2) );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+9).($x) .':' .get_excel_column_name_by_row_index($k+10).($x) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+9).($x+1) .':' .get_excel_column_name_by_row_index($k+10).($x+1) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+9).($x+2) .':' .get_excel_column_name_by_row_index($k+10).($x+2) )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+9).($x) .':' .get_excel_column_name_by_row_index($k+10).($x) )->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							
							

						}
						else if(!is_bool($find_total_lates))
						{
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+9).($x-1), $v);
						}
						else if(!is_bool($find_employee_records))
						{	
							
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+1).($x-1), $v);
						}
						elseif(!is_bool($find_PSR))
						{
							if($k == 0)
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							elseif($k == 1)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+($value[4]-1)).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+($value[4]-1)).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+($value[4]-1)).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							}
							if($k == 2)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+($value[4]-1)).$x .':' .get_excel_column_name_by_row_index($k+($value[5]-2)).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+($value[4]-1)).$x .':' .get_excel_column_name_by_row_index($k+($value[5]-2)).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+($value[4]-1)).$x .':' .get_excel_column_name_by_row_index($k+($value[5]-2)).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+($value[4]-1)).$x, $v);
							}
							if($k == 3)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+($value[5]-2)).$x .':' .get_excel_column_name_by_row_index($k+($value[6]-3)).$x );
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+($value[5]-2)).$x .':' .get_excel_column_name_by_row_index($k+($value[6]-3)).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+($value[5]-2)).$x .':' .get_excel_column_name_by_row_index($k+($value[6]-3)).$x )->getFont()->setBold(true);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+($value[5]-2)).$x, $v);
							}
						}
						elseif(!is_bool($find_footer))
						{
							$end_column = $CI->excel->setActiveSheetIndex(0)->getHighestColumn();
							$num = ord($end_column) - 1;

							if($k == 0){
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
								
							}elseif($k == 1){
								$CI->excel->setActiveSheetIndex(0)->mergeCells(chr($num).$x .':' .$end_column.$x);
								$CI->excel->setActiveSheetIndex(0)->getStyle(chr($num).$x .':' .$end_column.$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->getActiveSheet()->setCellValue(chr($num).$x, $v);
															
							}
						}
						elseif(!is_bool($find_signature))
						{
							if($k == 0 || $k == 3 || $k == 6)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+$counter_signature).$x.':' .get_excel_column_name_by_row_index($k+$counter_signature).($x+1));
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+$counter_signature).$x.':' .get_excel_column_name_by_row_index($k+$counter_signature).($x+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+$counter_signature).$x.':' .get_excel_column_name_by_row_index($k+$counter_signature).($x+1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+$counter_signature).$x, $v);
							}
							elseif($k == 1 || $k == 4 || $k == 7)
							{
								$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k+$counter_signature).$x.':' .get_excel_column_name_by_row_index($k+$counter_signature+2).($x+1));
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+$counter_signature).$x.':' .get_excel_column_name_by_row_index($k+$counter_signature+2).($x+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
								$CI->excel->setActiveSheetIndex(0)->getStyle(get_excel_column_name_by_row_index($k+$counter_signature).$x.':' .get_excel_column_name_by_row_index($k+$counter_signature+2).($x+1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+$counter_signature).$x, $v);
								$counter_signature += 2;
							}
							else
							{
								$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+$counter_signature).$x, $v);
							}
						}
						else
						{
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
						}
					}
				}

				if($retain_x == 1)
				{
					$x++;
				}				
			}
		}
		//getting the highest column render on the excel
		$highest_column = $CI->excel->setActiveSheetIndex(0)->getHighestColumn();

		//merging top rows
		if($counter_merge_top == 1)
		{	
			//first row, usually company name
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A1:' .$highest_column.'1');
			$CI->excel->getActiveSheet()->getStyle('A1:' .$highest_column.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			//second row, usually report type
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A2:' .$highest_column.'2');
			$CI->excel->getActiveSheet()->getStyle('A2:' .$highest_column.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			//third row, usually date or period covered
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A3:' .$highest_column.'3');
			$CI->excel->getActiveSheet()->getStyle('A3:' .$highest_column.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		}
		// //special function : merging top rows for Time and Attendance DTR
		// if($counter_merge_top1 == 1)
		// {	
		// 	//first row, usually company name
		// 	$CI->excel->setActiveSheetIndex(0)->mergeCells('A1:' .$highest_column.'1');
		// 	$CI->excel->getActiveSheet()->getStyle('A1:' .$highest_column.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		// 	//second row, usually report type
		// 	$CI->excel->setActiveSheetIndex(0)->mergeCells('A2:' .$highest_column.'2');
		// 	$CI->excel->getActiveSheet()->getStyle('A2:' .$highest_column.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		// 	//third row, usually date or period covered
		// 	$CI->excel->setActiveSheetIndex(0)->mergeCells('A3:' .$highest_column.'3');
		// 	$CI->excel->getActiveSheet()->getStyle('A3:' .$highest_column.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// 	//fourth row, date created
		// 	$CI->excel->setActiveSheetIndex(0)->mergeCells('A4:' .$highest_column.'4');
		// 	$CI->excel->getActiveSheet()->getStyle('A4:' .$highest_column.'4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// 	//fifth row, creator
		// 	$CI->excel->setActiveSheetIndex(0)->mergeCells('A5:' .$highest_column.'5');
		// 	$CI->excel->getActiveSheet()->getStyle('A5:' .$highest_column.'5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// }

		//autosizing the cell of the excel
		$var = create_columns_array($highest_column);

		foreach($var as $columnID)
		{
			$CI->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(false);
		}

		//make a new directory if the $tmp_folder is not yet existing
		if(!file_exists($tmp_folder)){
			mkdir($tmp_folder,0777,true);
		}

		$tmp_folder = $tmp_folder.$filename;

		//write the excel on the directory
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');  
		$objWriter->save($tmp_folder);


		$return = array('url' => $file,'file'=>$filename);

		//return $return
		return $return;
	}

?>