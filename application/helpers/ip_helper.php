<?php

if ( ! function_exists( 'ip_check' ) )
{
    /**
     * Server Security Helper
     *
     *
     *
     * @access	public
     * @param	string
     * @return	none
     */

    function ip_check()
    {
        $CI =& get_instance();
        $CI->load->model("default_model");

        $access = array(
            'passed' => false,
            'message' => ""
        );
        $ip_whitelist = $CI->default_model->get_whitelist_ip();
        $ip_whitelist_list = array_column($ip_whitelist, 'ip_address');

        $ip_address = $_SERVER['REMOTE_ADDR'];
        if(in_array($ip_address, $ip_whitelist_list))
        {
            $access['passed'] = TRUE;
        }
        else{
            $access['message'] = 'Your IP Address is not whitelisted nor Username included in whitelisted users';
        }

        return $access;

    }
}