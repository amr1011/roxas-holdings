<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists( 'parse_letter' ) )
{
    /**
     * Parses/replaces an html content 
     *
     * @author RandallB
     *
     * @param string $html - pass the html with the tags that should be parsed
     *
     * @return string $parsed_letter
     */
    function parse_letter( $html = '' , $data = array(), $url= '' )
    {

        // if($data['esop_created_company_logo'] == "" || $data['esop_created_company_logo'] === null )
        // {
        //     $user_stock_offers[$i]["esop_created_company_logo"] = "/assets/images/roxas-holdings-logo.png";
        // }



        $parsed_letter = '';
        if( strlen($html) > 0 && count($data) > 0 )
        {

            $tags = array(
                    "[esop_name]"                => isset($data["esop_name"]) ? $data["esop_name"] : '',
                    "[offer_shares]"             => isset($data["stock_amount"]) ? number_format($data["stock_amount"], 2) : '',
                    "[price_per_share]"          => isset($data["esop_price_per_share"]) ? number_format($data["esop_price_per_share"], 2) : '',
                    "[total_value_of_share]"     => isset($data["esop_total_share_qty"]) ? number_format($data["esop_total_share_qty"], 2) : '',
                    "[vesting_years]"            => isset($data["esop_vesting_years"]) ? $data["esop_vesting_years"] : '',
                    "[accepted_shares]"          => isset($data["accepted"]) ? number_format($data["accepted"], 2) : '[accepted_shares]',
                    "[value_of_accepted_shares]" => (isset($data["accepted"]) && isset($data["esop_price_per_share"])) ? number_format(($data["accepted"] * $data["esop_price_per_share"]), 2) : '[value_of_accepted_shares]',
                    "[date_of_letter]"           => date("F d, Y"),
                    "[sender_name]"              => isset($data["esop_created_by_name"]) ? $data["esop_created_by_name"] : '',
                    "[sender_rank]"              => isset($data["esop_created_by_rank_name"]) ? $data["esop_created_by_rank_name"] : '',
                    "[sender_department]"        => isset($data["esop_created_by_department_name"]) ? $data["esop_created_by_department_name"] : '',
                    "[recipient_name]"           => isset($data["full_name"]) ? $data["full_name"] : '',
                    "[recipient_rank]"           => isset($data["rank_name"]) ? $data["rank_name"] : '',
                    "[recipient_department]"     => isset($data["department_name"]) ? $data["department_name"] : '',
                    "[offer_letter_date]"     => isset($data["date_sent_formatted"]) ? $data["date_sent_formatted"] : '',
                    "[company_logo]"             => isset($data["esop_created_company_logo"]) ? '<img src="'.$url.$data["esop_created_company_logo"].'" height="80px" width="190px">'  : '',
                    "<table>"                    => "<table class='table-offer'>"
                );

// '<img src="http://localhost/roxas/assets/images/roxas-holdings-logo.png" height="80px" width="190px">'
// /assets/images/companies/1576263d738f2c.jpg

// isset($data["date_sent"]) ? date('F d, Y', strtotime($data["date_sent"])) : ''

            foreach ($tags as $tag_key => $tag_value) {
                $html = str_replace($tag_key, $tag_value, $html);
            }

            $parsed_letter = $html;
        }

        return $parsed_letter;
    }
}