<?php

/**
 *	This class will be used to manage authentication token for user’s requests. 
 *	This will generate, authenticate, and regenerate token based on the request scenario. 
 *	The auth token that will be generated is composed of 20-bit random character and a timestamp to ensure uniqueness of the token.
 *	The concatenated string will be encrypted using SHA1 PHP method.
 *
 *	@author: J. Alejandro
 */
class authToken {
		
	/**
	 *  The method that will be used to generate the authentication token by creating 20-bit random alpha-numeric (with upper and lower case) character + current timestamp.
	 *	The encryption method that will be used will be sha1.
	 *  @param: NA
	 *	@return: string
	 *
	*/
	public function generate() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 20; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $current_timestamp = time();
        $randomString .= $current_timestamp;
        $randomString = sha1($randomString);

        return $randomString;
    }
	
	
	
}