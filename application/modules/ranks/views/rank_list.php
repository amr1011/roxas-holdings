<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<!-- <h1 class="f-left">View Department Information</h1> -->
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="<?php echo base_url(); ?>companies/company_list">Company List</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="javascript:void(0)" class="back_to_previous_company"></a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a data-label="department_name"></a>
			</div>
			<div class="f-right">
				<button class="btn-normal display-inline-mid margin-right-10 modal-trigger" modal-target="add-rank">Add Rank</button>
				<!-- <a href="javascript:void(0)"><button class="btn-normal display-inline-mid margin-right-10 " >Edit Rank</button></a> -->
				<button class="btn-normal display-inline-mid modal-trigger"  modal-target="edit-department">Edit Department</button>				
			</div>
			<div class="clear"></div>
		</div>
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="margin-top-20 white-color" data-container="department_information">
			<table class="width-100percent">
				<tbody>
					<tr>
						<td class="font-20">Department Code:</td>
						<td class="font-20">Department Name:</td>
					</tr>
					<tr>
						<td data-label="department_code" class="padding-top-10 font-30"><!-- 101001 --></td>
						<td data-label="department_name" class="padding-top-10 font-30"><!-- Office of the President --></td>
					</tr>
				</tbody>
			</table>
			
			<p class="margin-top-30 font-20">Description:</p>
			<p class="margin-top-10" data-label="department_description"><!-- Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​,
			 morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra
			  adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The 
			  voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. 
			  Nescio an Undead zombies. Sicut malus movie horror. --></p>
		</div>
		<div class="text-right-line margin-top-5">
			<div class="line"></div>
			<div class="content-text" id="sort_rank">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-rank-name">Rank Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-rank-employee-count">No. of Employees <i class="fa fa-chevron-down"></i></a></p>					
			</div>
		</div>
	
		<h2 class="margin-top-40">Rank List</h2>
		<!-- commented table -->
		<!-- <div class="tbl-rounded">
			<table class="table-roxas tbl-display comp-info ">
				<thead>
					<tr>
						<th class="width-200px">No</th>
						<th>Rank</th>
						<th class="width-150px">No. of Employee</th>					
					</tr>
				</thead>		
				<tbody data-container="rank_list_container">
					<tr class="rank template hidden data-box">
						<td data-label="rank_number">10100101</td>
						<td data-label="rank_name">Chairman</td>
						<td data-label="rank_employee_count">0</td>
					</tr>
				</tbody>
			</table>
		</div> -->
		<!-- new table -->
		<table class="table-roxas comp-info">
			<thead>
				<tr>
					<th class="width-200px text-center">No</th>
					<th class="text-center">Rank</th>
					<th class="width-150px text-center">No. of Employee</th>
				</tr>
			</thead>		
		</table>
		<div data-container="rank_list_container">
			<div class="data-box width-100percent margin-0px fake-table no-border rank template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td data-label="rank_number" class="text-center width-200px">10100101</td>
							<td data-label="rank_name" class="text-center">Chairman</td>
							<td data-label="rank_employee_count" class="text-center width-150px">0</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center no-border">
					<a href="javascript:void(0)" class="edit_rank">
						<button class="btn-normal">Edit Rank</button>
					</a>
				</div>
			</div>

			<div class="data-box width-100percent margin-0px fake-table no-border no_results_found template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="3" class="text-center">No Results found</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<table class="table-roxas comp-info1">
			<thead>
				<tr class="height-40px">
					<th></th>
					<th></th>
					<th></th>					
				</tr>
			</thead>		
		</table>
		
		<div data-container="audit_logs" class="hidden">
			<div class="panel-group text-left margin-top-30">
				<div class="accordion_custom">
					<div class="panel-heading border-10px">
						<a href="#">
							<h4 class="panel-title white-color">							
								Audit Logs
								<i class="change-font fa fa-caret-right font-left"></i>
								<i class="fa fa-caret-down font-right"></i>							
							</h4>
						</a>																	
						<div class="clear"></div>					
					</div>					
					<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
						<div class="panel-body ">

							<table class="table-roxas">
								<thead>
									<tr>
										<th>Date of Activity</th>
										<th>User</th>
										<th>Activity Description</th>
									</tr>
								</thead>
								<tbody data-container="logs">
									<tr class="logs template hidden">
										<td data-label="date_created">September 10, 2015</td>
										<td data-label="created_by">ROXAS, PEDRO OLGADO</td>
										<td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
									</tr>
									<!-- <tr>
										<td>September 10, 2015</td>
										<td>VALENCIA, RENATO CRUZ</td>
										<td>Created Department  <span class="font-bold">"HR Department"</span></td>
									</tr> -->
								</tbody>
							</table>
						</div>			
					</div>
				</div>
			</div>
		</div>
	<div>
</section>

<!-- edit RANK -->
<div class="modal-container" modal-id="edit-department">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT DEPARTMENT INFORMATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error edit_department_error_message hidden margin-bottom-10">
			</div>
			<div class="success edit_department_success_message hidden margin-bottom-10">
			</div>

			<form id="edit_department_form">
				<table class="">
					<tbody>
						<tr>
							<td>Department Code: <span class="red-color">*</span></td>
							<td><input name="department_code" datavalid="required" labelinput="Department Code" type="text" class="normal margin-left-10 add-border-radius-5px width-300px" value="Office of the President " /></td>
						</tr>
						<tr>
							<td>Department Name: <span class="red-color">*</span></td>
							<td><input name="department_name" datavalid="required" labelinput="Department Name" type="text" class="normal margin-left-10 add-border-radius-5px width-300px" value="101001 " /></td>
						</tr>
						<tr>
							<td class="v-top">Description: <span class="red-color">*</span></td>
							<td>
								<textarea name="department_description" datavalid="required" labelinput="Department Description" class="margin-left-10 black-color width-300px add-border-radius-5px" placeholder="Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead."></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal" id="edit_department_btn">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add RANK -->
<div class="modal-container" modal-id="add-rank">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD RANK</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error add_rank_error_message hidden margin-bottom-10">
			</div>
			<div class="success add_rank_success_message hidden margin-bottom-10">
			</div>

			<form id="add_rank_form">
				<table class="width-100ppercent center-margin">
					<tbody>
						<tr>
							<td class="padding-top-10">Rank Code: <span class="red-color">*</span></td>
							<td><input name="rank_number" datavalid="required" labelinput="Rank Number" type="text" class="normal margin-left-20 width-300px add-border-radius-5px code-generated"/></td>
						</tr>
						<tr>
							<td class="v-top padding-top-15">Rank Name: <span class="red-color">*</span></td>
							<td>
								<input name="rank_name" datavalid="required" labelinput="Rank Name" type="text" class="normal margin-left-20 width-300px add-border-radius-5px"/>
								<!-- <div class="select margin-left-20 width-300px add-radius">
									<select>
										<option value="op1">Value 1</option>
										<option value="op2">Value 2</option>
										<option value="op3">Value 3</option>
									</select>
								</div> -->
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal" id="add_rank_btn">Add Rank</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- edit RANK -->
<div class="modal-container" modal-id="edit-rank">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT RANK</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error edit_rank_error_message hidden margin-bottom-10">
			</div>
			<div class="success edit_rank_success_message hidden margin-bottom-10">
			</div>

			<form id="edit_rank_form">
				<table class="width-100ppercent center-margin">
					<tbody>
						<tr>
							<td class="padding-top-10">Rank Code:</td>
							<td><input name="rank_number" datavalid="required" labelinput="Rank Number" type="text" class="normal margin-left-20 width-300px add-border-radius-5px"/></td>
						</tr>
						<tr>
							<td class="v-top padding-top-15">Rank Name:</td>
							<td>
								<input name="rank_name" datavalid="required" labelinput="Rank Name" type="text" class="normal margin-left-20 width-300px add-border-radius-5px"/>
								<!-- <div class="select margin-left-20 width-300px add-radius">
									<select>
										<option value="op1">Value 1</option>
										<option value="op2">Value 2</option>
										<option value="op3">Value 3</option>
									</select>
								</div> -->
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal" id="edit_rank_btn">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>