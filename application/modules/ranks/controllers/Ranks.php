<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ranks extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }

        $this->load->model('Ranks/Ranks_model');

        $allowed = $this->check_user_role();

        if(!$allowed)
        {
            // header('Location: '.base_url().'esop');
            header('Location: '.base_url().'auth/forbidden_403');
        }
    }

    public function index ()
    {
        header('Location: '.base_url().'ranks/rank_list');
    }
    
    
    public function rank_list ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/departments.js' );
        $this->template->add_script( assets_url() . '/js/libraries/ranks.js' );
        $this->template->add_content( $this->load->view( 'rank_list', $data, TRUE ) );
        $this->template->draw();
    }

    public function get ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'name' => 'r.name',
                    'number' => 'r.number',
                    'department_id' => 'r.department_id',
                    'employee_count' => 'employee_count',
                    'date_added' => 'r.date_added',
                    'id' => 'r.id',
                    'is_deleted' => 'r.is_deleted',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "r.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );
            $response['qualifiers'] = $qualifiers;
            $ranks = $this->Ranks_model->get_rank_data($qualifiers);

            if(count($ranks) > 0)
            {
                $response['total_rows'] = $ranks['total_rows'];
                unset($ranks['total_rows']);
                $response['data'] = $ranks;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function add ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $rank_information = $this->input->post();

        if(isset($rank_information) && count($rank_information) > 0)
        {
            /*validate department code*/
            if(isset($rank_information['name']) AND strlen($rank_information['name']) > 0)
            {
                if(strlen($rank_information['name']) > 1 AND preg_match("/[a-z||A-Z]/", trim($rank_information['name'])) )
                {
                    if(preg_match("/^[a-zA-Z0-9\-\.\&\,\/ ]+$/", trim($rank_information['name'])) )
                    {
                        $rank_information_input['name'] = $rank_information['name'];
                    }
                    else
                    {
                        $response['message'][] = "Invalid rank name or is too short";
                        $error++; 
                    }
                }
                else
                {
                    $response['message'][] = "Invalid rank name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Rank name is required.";
                $error++; 
            }

            /*validate department name*/
            if(isset($rank_information['number']) AND strlen($rank_information['number']) > 0)
            {
                if(strlen($rank_information['number']) > 0 AND preg_match("/^[0-9]+$/", trim($rank_information['number'])) )
                {
                    $rank_information_input['number'] = $rank_information['number'];
                }
                else
                {
                    $response['message'][] = "Invalid rank number or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Rank number is required.";
                $error++; 
            }

            /*validate department company id*/
            if(isset($rank_information['department_id']) AND strlen($rank_information['department_id']) > 0)
            {
                if(strlen($rank_information['department_id']) > 0 AND preg_match("/^[0-9]+$/", trim($rank_information['department_id'])) )
                {
                    $rank_information_input['department_id'] = $rank_information['department_id'];
                }
                else
                {
                    $response['message'][] = "Invalid department id or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Department id is required.";
                $error++; 
            }

            $rank_information_input['is_deleted'] = 0;
            $rank_information_input['date_added'] = date('Y-m-d H:i:s');
            
            if($error == 0)
            {
                /*validate rank number if existing*/
                $check_existing_qualifiers = array(
                    'where' => array(
                        array(
                            'field' => 'number',
                            'operator' => '=',
                            'value' => "'".$rank_information['number']."'",
                        )
                    )
                );
                $existing_rank_number = $this->Ranks_model->check_existing_data($check_existing_qualifiers, 'rank');

                if($existing_rank_number > 0)
                {
                    $response['message'][] = "Rank number is already existing.";
                    $error++;
                }

                if($existing_rank_number == 0 && $error == 0)
                {
                    /*insert if all inputs are okay*/
                    $added_rank_information_id = $this->Ranks_model->insert_rank_information($rank_information_input);

                    if($added_rank_information_id > 0)
                    {
                        $response['message'][] = "Rank added successfully.";
                        $response['status'] = TRUE;
                        $response['data'] = $rank_information_input;
                        $response['data']['id'] = $added_rank_information_id;

                        /*for logs*/
                        $_POST['params'] = $response['data'];
                        $_POST['status'] = TRUE;
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function edit ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $rank_information = $this->input->post();

        if(isset($rank_information) && count($rank_information) > 0)
        {
            if(isset($rank_information['data']) AND is_array($rank_information['data']) AND count($rank_information['data']) > 0 AND isset($rank_information['id']) AND $rank_information['id'] > 0)
            {
                foreach ( $rank_information['data'][0] as $i => $update_data )
                {
                    if ( strlen( $i ) > 0 )
                    {
                        /*validate fields*/

                        /*rank name validation*/
                        if ($i == 'name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1 AND preg_match("/[a-z||A-Z]/", trim($update_data)))
                                {
                                    if(preg_match("/^[a-zA-Z0-9\-\.\&\,\/ ]+$/", trim($update_data)))
                                    {
                                        $rank_information_input['name'] = $update_data;
                                    }
                                    else
                                    {
                                        $response['message'][] = "Rank name invalid or is too short";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Rank name invalid or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Rank name is required";
                                $error ++;
                            }
                        }

                        /*rank code validation*/
                        if ($i == 'number')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    /*validate rank number if existing*/
                                    $check_existing_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'number',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            ),
                                            array(
                                                'field' => 'id',
                                                'operator' => '!=',
                                                'value' => "'".$rank_information['id']."'",
                                            ),
                                        )
                                    );

                                    $existing_rank_number = $this->Ranks_model->check_existing_data($check_existing_qualifiers, 'rank');

                                    if($existing_rank_number > 0)
                                    {
                                        $response['message'][] = "Rank number is already existing.";
                                        $error++;
                                    }
                                    else
                                    {
                                        $rank_information_input['number'] = $update_data;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Rank number invalid or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Rank number is required";
                                $error ++;
                            }
                        }
                    }
                }

                if($error == 0)
                {
                    /*update if all inputs are okay*/
                    if($error == 0)
                    {
                        $logs_from = $this->Ranks_model->get_rank_data(
                                array(
                                    'where' => array(
                                        array(
                                            'field' => 'r.id',
                                            'operator' => '=',
                                            'value' => $rank_information['id'],
                                        )
                                    )
                                )
                            );
                        $updated_rank_information_id = $this->Ranks_model->update_rank_information(
                            array(
                                array(
                                'field' => 'r.id',
                                'value' => $rank_information['id'],
                                'operator' => '='
                                )
                            ), 
                            $rank_information['data'][0]);

                        if($updated_rank_information_id > 0)
                        {
                            $response['message'][] = "Rank updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $rank_information_input;
                            $response['data']['id'] = $rank_information['id'];

                            /*for logs*/
                            $_POST['params']['id'] = $rank_information['id'];
                            $_POST['params']['to'] = $response['data'];
                            $_POST['params']['from'] = $logs_from[0];
                            $_POST['params']['diff'] = array_diff_assoc($_POST['params']['to'], $_POST['params']['from']);
                            $_POST['status'] = TRUE;
                        }
                        else
                        {
                            // $response['message'][] = "Nothing is changed.";
                            $response['message'][] = "Rank updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $rank_information_input;
                            $response['data']['id'] = $rank_information['id'];
                        }
                        $response['data']['affected_rows'] = $updated_rank_information_id;
                    }
                }
            }
        }

        echo json_encode($response);
    }

}

/* End of file Departments.php */
/* Location: ./application/modules/departments/controllers/Departments.php */