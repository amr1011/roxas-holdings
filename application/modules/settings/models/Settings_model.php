<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model {

    public function insert_offer_letter($data)
    {
        $this->db->insert('offer_letter',$data);
        return $this->db->insert_id();
    }

    public function get_offer_letter_data($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT 
                    ol.*,
                    u.first_name,
                    u.middle_name,
                    u.last_name,
                    CONCAT(u.first_name, \' \', u.middle_name, \' \', u.last_name) as user_name,
                    (SELECT
                        CASE
                        WHEN ol.type = 1 
                        THEN \'Offer Letter\'
                        WHEN ol.type = 2  
                        THEN \'Acceptance Letter\'
                        WHEN ol.type = 3
                        THEN \'Reject Letter\'
                        END) as type_name

                FROM "offer_letter" ol
                LEFT JOIN "user" u ON ol.user_id = u.id
                WHERE TRUE ';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "ol.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY ol.id,u.first_name,u.middle_name,u.last_name";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." OFFSET ".$qualifiers['offset']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function update_offer_letter($id,$data)
    {
        $query = $this->db
        ->where('offer_letter.id',$id)
        ->update('offer_letter',$data);
        //print_r($query);exit;
        return $this->db->affected_rows();
    }

    public function get_payment_methods_data($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT 
                    pm.*
                FROM "payment_methods" pm
                WHERE TRUE ';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "pm.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY pm.id";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." OFFSET ".$qualifiers['offset']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function insert_payment_method($data)
    {
        $this->db->insert('payment_methods',$data);
        return $this->db->insert_id();
    }

    public function update_payment_method($id,$data)
    {
        $query = $this->db
        ->where('payment_methods.id',$id)
        ->update('payment_methods',$data);
        //print_r($query);exit;
        return $this->db->affected_rows();
    }

    
}