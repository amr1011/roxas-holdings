<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');


        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }

        $this->load->model('Settings/Settings_model');

        $allowed = $this->check_user_role();

        if(!$allowed)
        {
            // header('Location: '.base_url().'esop');
            header('Location: '.base_url().'auth/forbidden_403');
        }
    }


    public function index ()
    {
        header('Location: '.base_url().'settings/payment_method_list');
    }

    public function payment_method_list ()
    {
        $qualifiers = array(
            'where' => isset($params['where']) ? $params['where'] : array() ,
            'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
            'search_field' => isset($search_field) ? $search_field : '' ,
            'offset' => isset($params['offset']) ? $params['offset'] : 0,
            'limit' => isset($params['limit']) ? $params['limit'] : 30,
            'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "pm.id",
            'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
        );

        $qualifiers['where'] = array(
                    array(
                    'field' => 'pm.status',
                    'value' => '1'
                )
            );

        $data = array();
        $payment_methods_data = json_decode(json_encode($this->Settings_model->get_payment_methods_data($qualifiers)));
        unset($payment_methods_data->total_rows);
        $data['payment_methods'] = $payment_methods_data;

        $this->template->add_script( assets_url() . '/js/libraries/settings.js' );
        $this->template->add_content( $this->load->view( 'payment_method_list', $data, TRUE ) );
        $this->template->draw();
    }

    public function add_payment_method ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['name']) AND strlen(trim($params['name'])) > 0)
            {

            }
            else
            {
                $response['message'][] = "Name is required.";
                $error++; 
            }

            if($error == 0)
            {
                /*insert if all inputs are okay*/
                $params['date_added'] = date('Y-m-d H:i:s');
                $insert_id = $this->Settings_model->insert_payment_method($params);

                if($insert_id > 0)
                {
                    $response['message'][] = "Payment Method added successfully.";
                    $response['status'] = TRUE;
                    $response['data'] = $params;
                    $response['data']['id'] = $insert_id;

                    /*for logs*/
                    $_POST['params']['id'] = $insert_id;
                    $_POST['params']['name'] = $params['name'];
                    $_POST['status'] = TRUE;
                }
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function update_payment_method ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['name']) AND strlen(trim($params['name'])) > 0)
            {
                if(isset($params['id']) AND strlen(trim($params['id'])) > 0)
                {
                    
                    
                }
                else
                {
                    $response['message'][] = "ID is required.";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Name is required.";
                $error++; 
            }

            if($error == 0)
            {
                $logs_from = $this->Settings_model->get_payment_methods_data(
                        array(
                            'where' => array(
                                array(
                                    'field' => 'id',
                                    'operator' => '=',
                                    'value' => $params['id'],
                                )
                            )
                        )
                    );
                /*update if all inputs are okay*/
                $is_updated = $this->Settings_model->update_payment_method($params['id'],$params);

                $response['message'][] = "Payment Method updated successfully.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                $response['data']['id'] = $params['id'];

                if($is_updated > 0)
                {
                    /*for logs*/
                    $_POST['params']['id'] = $params['id'];
                    $_POST['params']['name'] = $params['name'];
                    $_POST['params']['to'] = $response['data'];
                    $_POST['params']['from'] = $logs_from[0];
                    $_POST['params']['diff'] = array_diff_assoc($_POST['params']['to'], $_POST['params']['from']);
                    $_POST['status'] = TRUE;
                }
                
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function delete_payment_method ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['id']) AND strlen(trim($params['id'])) > 0)
            {
                
                
            }
            else
            {
                $response['message'][] = "ID is required.";
                $error++;
            }

            if($error == 0)
            {
                $name = $params['name'];
                if(isset($params['name'])) { unset($params['name']); }

                /*update if all inputs are okay*/
                $is_updated = $this->Settings_model->update_payment_method($params['id'],$params);

                $response['message'][] = "Payment Method deleted successfully.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                $response['data']['id'] = $params['id'];
                
                if($is_updated > 0)
                {
                    /*for logs*/
                    $_POST['params']['id'] = $params['id'];
                    $_POST['params']['name'] = $name;
                    $_POST['status'] = TRUE;
                }
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }


    public function offer_letter ()
    {
        $qualifiers = array(
            'where' => isset($params['where']) ? $params['where'] : array() ,
            'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
            'search_field' => isset($search_field) ? $search_field : '' ,
            'offset' => isset($params['offset']) ? $params['offset'] : 0,
            'limit' => isset($params['limit']) ? $params['limit'] : 30,
            'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "ol.id",
            'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
        );

        $qualifiers['where'] = array(
                    array(
                    'field' => 'ol.status',
                    'value' => '1'
                )
            );

        $data = array();
        $offer_letters_data = json_decode(json_encode($this->Settings_model->get_offer_letter_data($qualifiers)));
        unset($offer_letters_data->total_rows);
        $data['offer_letters'] = $offer_letters_data;
        //print_r($data);exit;
        $this->template->add_script( assets_url() . '/js/libraries/settings.js' );
        $this->template->add_content( $this->load->view( 'offer_letter', $data, TRUE ) );
        $this->template->draw();
    }

    public function view_offer_letter ($id)
    {
        $qualifiers = array(
            'where' => isset($params['where']) ? $params['where'] : array() ,
            'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
            'search_field' => isset($search_field) ? $search_field : '' ,
            'offset' => isset($params['offset']) ? $params['offset'] : 0,
            'limit' => isset($params['limit']) ? $params['limit'] : 30,
            'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "ol.id",
            'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
        );

        $qualifiers['where'] = array(
                    array(
                    'field' => 'ol.id',
                    'value' => $id
                )
            );

        $data = array();
        $offer_letters_data = json_decode(json_encode($this->Settings_model->get_offer_letter_data($qualifiers)));
        unset($offer_letters_data->total_rows);
        $data['offer_letters'] = $offer_letters_data;
        //print_r($data);exit;
        $this->template->add_script( assets_url() . '/js/libraries/settings.js' );
        //print_r($data);exit;
        $this->template->add_content( $this->load->view( 'view_offer_letter', $data, TRUE ) );
        $this->template->draw();
    }

    public function add_offer_letter ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['name']) AND strlen(trim($params['name'])) > 0)
            {
                if(isset($params['content']) AND strlen(trim($params['content'])) > 0)
                {
                    if(isset($params['type']) AND strlen(trim($params['type'])) > 0)
                    {
                        if(isset($params['user_id']) AND strlen(trim($params['user_id'])) > 0)
                        {
                            
                        }
                        else
                        {
                            $response['message'][] = "User id is required.";
                            $error++;
                        }

                    }
                    else
                    {
                        $response['message'][] = "Type is required.";
                        $error++;
                    }

                }
                else
                {
                    $response['message'][] = "Content is required.";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Title is required.";
                $error++; 
            }

            if($error == 0)
            {
                /*insert if all inputs are okay*/
                $params['date_created'] = date('Y-m-d H:i:s');
                $insert_id = $this->Settings_model->insert_offer_letter($params);

                if($insert_id > 0)
                {
                    $response['message'][] = "Letter added successfully.";
                    $response['status'] = TRUE;
                    $response['data'] = $params;
                    $response['data']['id'] = $insert_id;

                    /*for logs*/
                    $_POST['params']['id'] = $insert_id;
                    $_POST['params']['name'] = $params['name'];
                    $_POST['status'] = TRUE;
                }
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function update_offer_letter ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['name']) AND strlen(trim($params['name'])) > 0)
            {
                if(isset($params['content']) AND strlen(trim($params['content'])) > 0)
                {
                    if(isset($params['type']) AND strlen(trim($params['type'])) > 0)
                    {
                        if(isset($params['user_id']) AND strlen(trim($params['user_id'])) > 0)
                        {

                            if(isset($params['id']) AND strlen(trim($params['id'])) > 0)
                            {

                            }
                            else
                            {
                               $response['message'][] = "ID is required.";
                                $error++;
                            }
                            
                        }
                        else
                        {
                            $response['message'][] = "User id is required.";
                            $error++;
                        }

                    }
                    else
                    {
                        $response['message'][] = "Type is required.";
                        $error++;
                    }

                }
                else
                {
                    $response['message'][] = "Content is required.";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Title is required.";
                $error++; 
            }

            if($error == 0)
            {
                $logs_from = $this->Settings_model->get_offer_letter_data(
                        array(
                            'where' => array(
                                array(
                                    'field' => 'ol.id',
                                    'operator' => '=',
                                    'value' => $params['id'],
                                )
                            )
                        )
                    );
                $user_name = $params['user_name'];
                $type_name = $params['type_name'];

                if(isset($params['user_name'])) { unset($params['user_name']); }
                if(isset($params['type_name'])) { unset($params['type_name']); }

                /*update if all inputs are okay*/
                $is_updated = $this->Settings_model->update_offer_letter($params['id'],$params);

                $response['message'][] = "Letter updated successfully.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                $response['data']['id'] = $params['id'];

                if($is_updated > 0)
                {
                    /*for logs*/
                    $response['data']['user_name'] = $user_name;
                    $response['data']['type_name'] = $type_name;

                    $_POST['params']['id'] = $params['id'];
                    $_POST['params']['to'] = $response['data'];
                    $_POST['params']['from'] = $logs_from[0];
                    $_POST['params']['diff'] = array_diff_assoc($_POST['params']['to'], $_POST['params']['from']);
                    $_POST['status'] = TRUE;
                }
                
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function archive_letter ()
    {
        $qualifiers = array(
            'where' => isset($params['where']) ? $params['where'] : array() ,
            'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
            'search_field' => isset($search_field) ? $search_field : '' ,
            'offset' => isset($params['offset']) ? $params['offset'] : 0,
            'limit' => isset($params['limit']) ? $params['limit'] : 30,
            'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "ol.id",
            'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
        );

        $qualifiers['where'] = array(
                    array(
                    'field' => 'ol.status',
                    'value' => '0'
                )
            );

        $data = array();
        $offer_letters_data = json_decode(json_encode($this->Settings_model->get_offer_letter_data($qualifiers)));
        unset($offer_letters_data->total_rows);
        $data['offer_letters'] = $offer_letters_data;
        //print_r($data);exit;
        $this->template->add_script( assets_url() . '/js/libraries/settings.js' );
        $this->template->add_content( $this->load->view( 'archive_letter', $data, TRUE ) );
        $this->template->draw();
    }

    public function archive_offer_letter ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();


            if(isset($params['id']) AND strlen(trim($params['id'])) > 0)
            {

            }
            else
            {
               $response['message'][] = "ID is required.";
                $error++;
            }
                            

            if($error == 0)
            {
                $name = $params['name'];
                if(isset($params['name'])) { unset($params['name']); }

                /*update if all inputs are okay*/
                //$params['date_created'] = date('Y-m-d H:i:s');
                $is_updated = $this->Settings_model->update_offer_letter($params['id'],$params);

                $response['message'][] = "Letter archived successfully.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                $response['data']['id'] = $params['id'];

                if($is_updated > 0)
                {
                    $_POST['params']['id'] = $params['id'];
                    $_POST['params']['name'] = $name;
                    $_POST['status'] = TRUE;
                }
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function get_offer_letters ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'name' => 'ol.name',
                    'content' => 'ol.content',
                    'ol.date_created' => 'ol.date_created',
                    'status' => 'ol.status',
                    'type' => 'ol.type',
                    'user_id' => 'ol.user_id',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "ol.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;
            $offer_letters = $this->Settings_model->get_offer_letter_data($qualifiers);
            $total_rows = $offer_letters['total_rows'];
            unset($offer_letters['total_rows']);

            if(count($offer_letters) > 0)
            {
                $response['total_rows'] = $total_rows;
                $response['data'] = $offer_letters;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    


}

/* End of file settings.php */
/* Location: ./application/modules/settings/controllers/settings.php */