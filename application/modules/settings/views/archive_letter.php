<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left ">Archive Letter List</h1>
			<h2 class="f-left hidden">ESOP View</h2>
			<div class="f-right">
				<a href="<?php echo base_url()?>settings/offer_letter">
					<button class="btn-normal ">Back to Active List</button>
				</a>
			</div>
			
			<div class="clear"></div>
		</div>

		<table class="search_letter_container">
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10 add-radius margin-top-5">
							<select>
								<option value="Name">Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid add-border-radius-5px margin-top-5" name="search" />
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-5 btn-search">Search</button>
					</td>
				</tr>
			<tbody>
		</table>		
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="text-right-line margin-bottom-50">
			<div class="line"></div>
		</div>
		
		<div class="clear"></div>
	

		<div class="panel-group text-left margin-top-30">

			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Offer Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>
					<?php 
					$offer_letters_count = 0;
					if(count($offer_letters)>0){	
						foreach ($offer_letters as $offer_letter) {
							if($offer_letter->type==1){
								$offer_letters_count++;
							}
						}
					}
					?>
					<p class="f-right white-color font-16"><?php echo $offer_letters_count;?> Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">		

					<div class="panel-body padding-15px offer_letter_list">
						<?php 
						if(count($offer_letters)>0){
							foreach ($offer_letters as $offer_letter) {
							if($offer_letter->type==1){
						?>
						<div class="data-box divide-by-2" data-name="<?php echo $offer_letter->name?>">
							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color"><?php echo $offer_letter->name?></h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">ID:</td>
										<td class="text-left"><?php echo $offer_letter->id?></td>
										<td class="text-right"></td>
										<td class="text-center"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left"><?php echo date("F j, Y",strtotime($offer_letter->date_created))?></td>
										<td class="text-right">Created By:</td>
										<td class="text-center"><?php echo $offer_letter->first_name ." ". $offer_letter->last_name?></td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="<?php echo base_url()?>settings/view_offer_letter/<?php echo $offer_letter->id?>">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>
								<?php }?>
							<?php } ?>
						<?php } ?>

					</div>		
				</div>
			</div>


			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Acceptance Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>	
					<?php 
					$accept_letters_count = 0;
					if(count($offer_letters)>0){
						foreach ($offer_letters as $offer_letter) {
							if($offer_letter->type==2){
								$accept_letters_count++;
							}
						}
					}
					?>
					<p class="white-color font-16 f-right"><?php echo $accept_letters_count;?> Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
					<div class="panel-body padding-15px offer_letter_list">
						
						<?php 
						if(count($offer_letters)>0){
							foreach ($offer_letters as $offer_letter) {
								if($offer_letter->type==2){
						?>
						<div class="data-box divide-by-2"  data-name="<?php echo $offer_letter->name?>">
							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color"><?php echo $offer_letter->name?></h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">ID:</td>
										<td class="text-left"><?php echo $offer_letter->id?></td>
										<td class="text-right"></td>
										<td class="text-center"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left"><?php echo date("F j, Y",strtotime($offer_letter->date_created))?></td>
										<td class="text-right">Created By:</td>
										<td class="text-center"><?php echo $offer_letter->first_name ." ". $offer_letter->last_name?></td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="<?php echo base_url()?>settings/view_offer_letter/<?php echo $offer_letter->id?>">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>

					</div>	
				</div>
			</div>
		</div>
	<div>
</section>
