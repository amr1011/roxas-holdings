<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left ">Letter List</h1>
			<h2 class="f-left hidden">ESOP View</h2>
			<div class="f-right">
				<button class="btn-normal modal-trigger margin-right-10" modal-target="add-letter">Add Letter</button>
				<a href="<?php echo base_url()?>settings/archive_letter">
					<button class="btn-normal ">Letter Archives</button>
				</a>
			</div>
			
			<div class="clear"></div>
		</div>

		<table class="search_letter_container">
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10 add-radius margin-top-5">
							<select>
								<option value="Letter Name">Letter Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid add-border-radius-5px margin-top-5" name="search" />
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-5 btn-search">Search</button>
					</td>
				</tr>
			<tbody>
		</table>		
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="text-right-line margin-bottom-50">
			<div class="line"></div>
		</div>
		
		<div class="clear"></div>
	

		<div class="panel-group text-left margin-top-30">

			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Offer Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>
					<?php 
					$offer_letters_count = 0;
					if(count($offer_letters)>0){	
						foreach ($offer_letters as $offer_letter) {
							if($offer_letter->type==1){
								$offer_letters_count++;
							}
						}
					}
					?>
					<p class="f-right white-color font-16"><?php echo $offer_letters_count;?> Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">		

					<div class="panel-body padding-15px offer_letter_list">
						<?php 
						if(count($offer_letters)>0){
							foreach ($offer_letters as $offer_letter) {
							if($offer_letter->type==1){
						?>
						<div class="data-box divide-by-2" data-name="<?php echo $offer_letter->name?>">
							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color"><?php echo $offer_letter->name?></h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">ID:</td>
										<td class="text-left"><?php echo $offer_letter->id?></td>
										<td class="text-right"></td>
										<td class="text-center"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left"><?php echo date("F j, Y",strtotime($offer_letter->date_created))?></td>
										<td class="text-right">Created By:</td>
										<td class="text-center"><?php echo $offer_letter->first_name ." ". $offer_letter->last_name?></td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="<?php echo base_url()?>settings/view_offer_letter/<?php echo $offer_letter->id?>">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>
								<?php }?>
							<?php } ?>
						<?php } ?>

					</div>		
				</div>
			</div>


			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Acceptance Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>	
					<?php 
					$accept_letters_count = 0;
					if(count($offer_letters)>0){
						foreach ($offer_letters as $offer_letter) {
							if($offer_letter->type==2){
								$accept_letters_count++;
							}
						}
					}
					?>
					<p class="white-color font-16 f-right"><?php echo $accept_letters_count;?> Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
					<div class="panel-body padding-15px offer_letter_list">
						
						<?php 
						if(count($offer_letters)>0){
							foreach ($offer_letters as $offer_letter) {
								if($offer_letter->type==2){
						?>
						<div class="data-box divide-by-2" data-name="<?php echo $offer_letter->name?>">
							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color"><?php echo $offer_letter->name?></h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">ID:</td>
										<td class="text-left"><?php echo $offer_letter->id?></td>
										<td class="text-right"></td>
										<td class="text-center"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left"><?php echo date("F j, Y",strtotime($offer_letter->date_created))?></td>
										<td class="text-right">Created By:</td>
										<td class="text-center"><?php echo $offer_letter->first_name ." ". $offer_letter->last_name?></td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="<?php echo base_url()?>settings/view_offer_letter/<?php echo $offer_letter->id?>">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>

					</div>	
				</div>
			</div>

			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#" class="f-left">
						<h4 class="panel-title white-color active">							
							Reject Letter
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>	
					<?php 
					$reject_letters_count = 0;
					if(count($offer_letters)>0){
						foreach ($offer_letters as $offer_letter) {
							if($offer_letter->type==3){
								$reject_letters_count++;
							}
						}
					}
					?>
					<p class="white-color font-16 f-right"><?php echo $reject_letters_count;?> Letters</p>																
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
					<div class="panel-body padding-15px offer_letter_list">
						
						<?php 
						if(count($offer_letters)>0){
							foreach ($offer_letters as $offer_letter) {
								if($offer_letter->type==3){
						?>
						<div class="data-box divide-by-2" data-name="<?php echo $offer_letter->name?>">
							<table class="width-100per">
								<tbody>					
									<tr>
										<td colspan="2"><h3 class="font-bold black-color"><?php echo $offer_letter->name?></h3></td>
										<td colspan="2" class="text-right font-bold"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">ID:</td>
										<td class="text-left"><?php echo $offer_letter->id?></td>
										<td class="text-right"></td>
										<td class="text-center"></td>
									</tr>
									<tr>
										<td class="padding-top-10 padding-bottom-10">Date Created:</td>
										<td class="text-left"><?php echo date("F j, Y",strtotime($offer_letter->date_created))?></td>
										<td class="text-right">Created By:</td>
										<td class="text-center"><?php echo $offer_letter->first_name ." ". $offer_letter->last_name?></td>
									</tr>				
								</tbody>
							</table>
							<div class="data-hover text-center">
								<a href="<?php echo base_url()?>settings/view_offer_letter/<?php echo $offer_letter->id?>">
									<button class="btn-normal">View Letter</button>
								</a>
							</div>
						</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>

					</div>	
				</div>
			</div>


		</div>
	<div>
</section>

<!-- add letter  -->
<div class="modal-container" modal-id="add-letter">
	<div class="modal-body max-width-1200 width-1200px">
		<div class="modal-head ">
			<h4 class="text-left">ADD LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content ">	
			<div class="success add_letter_success_message margin-bottom-10 hidden"></div>
			<div class="error add_letter_error_message margin-bottom-10 hidden"></div>
			<div class="head">
				<div class="display-inline-mid width-80percent">
					<p class="margin-bottom-5">Title</p>
					<input type="text" class="normal width-100percent add-border-radius-5px" value="" name="name"/>
				</div>
				<div class="display-inline-mid margin-left-20">
					<p class="margin-bottom-5">Letter Type:</p>
					<div class="select add-radius">
						<div class="frm-custom-dropdown font-0">
							<div class="frm-custom-dropdown-txt">
							<input type="text" class="dd-txt" name="type" autocomplete="off"></div><div class="frm-custom-icon"></div>
							<div class="frm-custom-dropdown-option" style="display: none;">
								<div class="option" data-value="1">Offer Letter</div>
								<div class="option" data-value="2">Acceptance Letter</div>
								<div class="option" data-value="3">Reject Letter</div>
							</div>
						</div>
						<select class="frm-custom-dropdown-origin" style="display: none;">
							<option value="1">Offer Letter</option>
							<option value="2">Acceptance Letter</option>
							<option value="3">Reject Letter</option>
						</select>

					</div>
				</div>
			</div>

			<div class="big-header">
				<p class="f-left margin-left-10 white-color">Date Created: October 30, 2015</p>
				<p class="f-right margin-right-20 white-color">Created By: Joselito Salazar</p>
				<div class="clear"></div>
			</div>

			<div class="big-body">

				<div class="function">
					<!-- left - side -->
					<div class="format f-left  ">				
						<div class="text-files">
							<textarea name="editor1" id="editor1" rows="10" cols="80"></textarea>
						</div>
					</div>							
				</div>

				<div class="letter-nav ">
					<!-- right side  -->

					<div class="tag  ">
						<p class="text-center">TAGS</p>
					</div>					
					
					<div class="menu hover-letter">
						<ul class="tags">
							<li data-value="[esop_name]">ESOP Name</li>
							<li data-value="[offer_shares]">Offered Shares</li>
							<li data-value="[price_per_share]">Price / Share</li>
							<li data-value="[total_value_of_share]">Total Value of Share</li>
							<li data-value="[vesting_years]">Vesting Years</li>
							<li data-value="[accepted_shares]">Accepted Share</li>
							<li data-value="[value_of_accepted_shares]">Value of Accepted Shares</li>
							<li data-value="[date_of_letter]">Date of Letter</li>
							<li data-value="[sender_name]">Sender Name</li>	
							<li data-value="[sender_rank]">Sender Rank</li>						
							<li data-value="[sender_department]">Sender Department</li>
							<li data-value="[recipient_name]">Recipient Name</li>
							<li data-value="[recipient_rank]">Recipient Rank</li>
							<li data-value="[recipient_department]">Recipient Department</li>		
							<li data-value="[company_logo]">Company Logo</li>
							<li data-value="[offer_letter_date]">Offer Letter Date</li>										
							<li ></li>								
						</ul>
					</div>

					<div class="clear"></div>

				</div>

			</div>
				
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			
			<button type="button" class="display-inline-mid btn-normal alert-btn submit-letter">Submit</button>
		</div>
		<div class="clear"></div>
	</div>
</div>