<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left ">Payment Method List</h1>
			<h2 class="f-left hidden">ESOP View</h2>
			<button class="btn-normal f-right modal-trigger" modal-target="add-payment">Add Payment Method</button>
			<div class="clear"></div>
		</div>

		<table class="search_payment_method_container">
			<tbody>
				<tr>
					<td class="white-color">Search</td>					
				</tr>
				<tr>					
					<td>
						<div class="select display-inline-mid margin-right-10 add-radius margin-top-5">
							<select>
								<option value="Payment Name">Payment Name</option>
							</select>
						</div>				
						<input  type="text" class="normal display-inline-mid add-border-radius-5px margin-top-5" name="search" />
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-5 btn-search">Search</button>
					</td>
				</tr>
			<tbody>
		</table>		
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">

		<div class="text-right-line margin-bottom-50">
			<div class="line"></div>
		</div>
		
		<div class="clear"></div>
		<div class="tbl-rounded">
			<table class="table-roxas margin-top-20 tbl-display" id="payment_method_list">
				<thead>
					<tr>
						<th>ID</th>
						<th>Payment Method Name</th>
						<th>Date Added</th>
						<th>Action</th>
						
					</tr>
				</thead>
				<tbody class="payment_method_list_body">
					<?php 
						if(count($payment_methods)>0){
							foreach ($payment_methods as $payment_method) {
						?>
					<tr data-name="<?php echo $payment_method->name?>">
						<td><?php echo $payment_method->id?></td>
						<td><?php echo $payment_method->name?></td>
						<td><?php echo date("F j, Y",strtotime($payment_method->date_added))?></td>
						<td>
							<a href="#" class="modal-trigger edit_payment" modal-target="edit-payment" data-id="<?php echo $payment_method->id?>" data-name="<?php echo $payment_method->name?>">Edit</a>
							<span class="margin-left-20 margin-right-20">|</span>
							<a href="#" class="modal-trigger red-color delete_payment" modal-target="delete-payment" data-id="<?php echo $payment_method->id?>" data-name="<?php echo $payment_method->name?>">Delete</a>
						</td>					
					</tr>
						<?php } ?>
					<?php } ?>
					
				</tbody>
			</table>
		</div>

		<!-- <div class="text-right-line margin-bottom-80 margin-top-30">
			<div class="line"></div>
		</div> -->
	<div>	

	<div data-container="audit_logs" class="hidden">
		<div class="panel-group text-left margin-top-30">
			<div class="accordion_custom">
				<div class="panel-heading border-10px">
					<a href="#">
						<h4 class="panel-title white-color">							
							Audit Logs
							<i class="change-font fa fa-caret-right font-left"></i>
							<i class="fa fa-caret-down font-right"></i>							
						</h4>
					</a>																	
					<div class="clear"></div>					
				</div>					
				<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
					<div class="panel-body ">

						<table class="table-roxas">
							<thead>
								<tr>
									<th>Date of Activity</th>
									<th>User</th>
									<th>Activity Description</th>
								</tr>
							</thead>
							<tbody data-container="logs">
								<tr class="logs template hidden">
									<td data-label="date_created">September 10, 2015</td>
									<td data-label="created_by">ROXAS, PEDRO OLGADO</td>
									<td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
								</tr>
								<!-- <tr>
									<td>September 10, 2015</td>
									<td>VALENCIA, RENATO CRUZ</td>
									<td>Created Department  <span class="font-bold">"HR Department"</span></td>
								</tr> -->
							</tbody>
						</table>
					</div>			
				</div>
			</div>
		</div>
	</div>
</section>

<!-- add payment -->
<div class="modal-container" modal-id="add-payment">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD PAYMENT METHOD</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="success add_payment_method_success_message margin-bottom-10 hidden"></div>
			<div class="error add_payment_method_error_message margin-bottom-10 hidden"></div>

			<p class="display-inline-mid margin-right-10">Payment Name:</p>
			<input type="text" class="normal display-inline-mid width-300px add-border-radius-5px" name="name"/>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal add_payment">Add Payment Method</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- edit payment -->
<div class="modal-container" modal-id="edit-payment">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT PAYMENT METHOD</h4>
			<div class="modal-close close-me"></div>
		</div>
		
		<!-- content -->
		<div class="modal-content">
			<div class="success update_payment_method_success_message margin-bottom-10 hidden"></div>
			<div class="error update_payment_method_error_message margin-bottom-10 hidden"></div>
			<p class="display-inline-mid margin-right-10">Payment Name:</p>
			<input type="text" class="normal display-inline-mid width-300px add-border-radius-5px" value="" name="name"/>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal update_payment">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- delete payment -->
<div class="modal-container" modal-id="delete-payment">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">DELETE PAYMENT METHOD</h4>
			<div class="modal-close close-me"></div>
		</div>
		
		<!-- content -->
		<div class="modal-content">
			<div class="success delete_payment_method_success_message margin-bottom-10 hidden"></div>
			<div class="error delete_payment_method_error_message margin-bottom-10 hidden"></div>
			<strong> Are you sure you want to delete this?</strong>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal remove_payment">Yes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script type="text/javascript">
       var oGetLogsParams = {
           "where" : [
               // {
               //     "field" : "ref_id",
               //     "operator" : "IN",
               //     "value" : "("+arrLogIDS.join()+")"
               // },
               {
                   "field" : "type",
                   "operator" : "IN",
                   "value" : "('payment_method')"
               }
           ]
       };
        cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
</script>