<?php 
if(count($offer_letters)>0){
	foreach ($offer_letters as $offer_letter) {
?>
<script type="text/javascript">
	sOfferLetterType = '<?php echo $offer_letter->type; ?>';
	arrLogIDS.push(<?php echo $offer_letter->id?>);
</script>
<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
				
			<div class="clear"></div>
		</div>
		
		<?php if($offer_letter->status != 0){?>
		<div class="f-right">			
			<button class="btn-normal display-inline-block margin-right-10 modal-trigger" modal-target="archive-letter">Archive this Letter</button>			
			<button class="btn-normal display-inline-block modal-trigger " modal-target="edit-letter" id="edit-letter-click">Edit Letter</button>
		</div>
		<div class="clear"></div>
		<?php }?>
	</div>

</section>

<section section-style="content-panel">

	<div class="content">

		<p class="font-20 white-color">Offer Letter</p>
		<h2 class="margin-bottom-30"><?php echo $offer_letter->name?></h2>

			
		<div class="letter-head border-tl-10px border-tr-10px"> 
			<p class="f-left">Date Created: <?php echo date("F j, Y",strtotime($offer_letter->date_created))?></p>
			<p class="f-right">Created By: <?php echo $offer_letter->first_name ." ". $offer_letter->last_name?></p>
			<div class="clear"></div>
		</div>

		<div class="letter-content border-bl-10px border-br-10px">
			<?php echo $offer_letter->content?>
			<!-- <p class="f-right font-bold">&#60;Date of Letter></p>
			<div class="f-left margin-top-20">
				<p class="font-bold"> &#60;Sender Name></p>
				<p class="font-bold"> &#60;Sender Rank></p>
				<p class="font-bold"> &#60;Sender Department></p>
			</div>
			<div class="clear"></div>

			<p class="margin-top-20">Dear <span class="font-bold">&#60;Recepient's Name>,</span></p>
			<p class="margin-top-10">In accordance with the terms of the Employee Stock Option Plan (<abbr title="
			Employee Stock Option Plan">ESOP</abbr>) of Roxas Holdings,
			Inc. (<abbr title="Roxas Holdings Inc.">RHI</abbr>), which was approved by the Securities and Exchange Commission 
			(<abbr title="Securities and Exchange Commission">SEC</abbr>) on April 30, 2014, 
			the company offers you an option to subscribe and purchange <span class="font-bold"> &#60;Offer Shares> </span>
			shares of RHI which has been allocated to you over the exercise period of <span class="font-bold">
			&#60;Vesting Years> </span> years commencing from the date of this letter offer under the following conditions:
			</p>

			<ol class="margin-top-20">
				<li class="font-bold">Subscription Price &amp; Terms.</li>
				<p>This subscription price is based on the average price of the RHI shares in the 
				Philippine Stock Exchange for thirty(30) trading days prior to the date of this 
				subscription price is <strong>&#60;Price per Share></strong>.</p>
				<li class="font-bold margin-top-20">Vesting of Option.</li>
				<p>The Option will vest as follows:</p>
			</ol>

			<table class="table-offer">
				<thead>
					<tr>
						<th>Vesting Date</th>
						<th>Vesting Option for</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>At the end of one (1) year from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of two (2) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
					<tr>
						<td>At the end of three (3) years from the date of the option offer letter</td>
						<td>One-fifth (1/5) of the aggregate number of Option Shares</td>
					</tr>
				</tbody>
			</table> -->

		</div>

		<!-- place accordion here  -->
		<div data-container="audit_logs" class="hidden">
			<div class="panel-group text-left margin-top-30">
				<div class="accordion_custom">
					<div class="panel-heading border-10px">
						<a href="#">
							<h4 class="panel-title white-color">							
								Audit Logs
								<i class="change-font fa fa-caret-right font-left"></i>
								<i class="fa fa-caret-down font-right"></i>							
							</h4>
						</a>																	
						<div class="clear"></div>					
					</div>					
					<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
						<div class="panel-body ">

							<table class="table-roxas">
								<thead>
									<tr>
										<th>Date of Activity</th>
										<th>User</th>
										<th>Activity Description</th>
									</tr>
								</thead>
								<tbody data-container="logs">
									<tr class="logs template hidden">
										<td data-label="date_created">September 10, 2015</td>
										<td data-label="created_by">ROXAS, PEDRO OLGADO</td>
										<td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
									</tr>
									<!-- <tr>
										<td>September 10, 2015</td>
										<td>VALENCIA, RENATO CRUZ</td>
										<td>Created Department  <span class="font-bold">"HR Department"</span></td>
									</tr> -->
								</tbody>
							</table>
						</div>			
					</div>
				</div>
			</div>
		</div>

	<div>
</section>

<!-- edit letter -->
<div class="modal-container edit-letter" modal-id="edit-letter">
	<div class="modal-body max-width-1200 width-1200px ">
		<div class="modal-head ">
			<h4 class="text-left">EDIT LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content ">	
			<div class="head">
				<div class="success update_letter_success_message margin-bottom-10 hidden"></div>
				<div class="error update_letter_error_message margin-bottom-10 hidden"></div>
				<div class="display-inline-mid width-80percent">
					<p class="margin-bottom-5">Title</p>
					<input type="text" class="normal width-100percent add-border-radius-5px" name="name" value="<?php echo $offer_letter->name?>" />
				</div>
				<div class="display-inline-mid margin-left-20">
					<p class="margin-bottom-5">Letter Type:</p>
					<div class="select add-radius">
						<select>
							<option value="1">Offer Letter</option>
							<option value="2">Acceptance Letter</option>
							<option value="3">Reject Letter</option>
						</select>

					</div>
				</div>
			</div>

			<div class="big-header">
				<p class="f-left margin-left-10 white-color">Date Created: <?php echo date("F j, Y",strtotime($offer_letter->date_created))?></p>
				<p class="f-right margin-right-20 white-color">Created By: <?php echo $offer_letter->first_name ." ". $offer_letter->last_name;?></p>
				<div class="clear"></div>
			</div>

			<div class="big-body">

				<div class="function">
					<!-- left - side -->
					<div class="format f-left  ">			
						<div class="text-files">
							<textarea name="editor1" id="editor1" rows="10" cols="80"><?php echo $offer_letter->content;?></textarea>
						</div>
					</div>							
				</div>

				<div class="letter-nav ">
					<!-- right side  -->

					<div class="tag  ">
						<p class="text-center">TAGS</p>
					</div>					
					
					<div class="menu hover-letter">
						<ul class="tags">
							<li data-value="[esop_name]">ESOP Name</li>
							<li data-value="[offer_shares]">Offered Shares</li>
							<li data-value="[price_per_share]">Price / Share</li>
							<li data-value="[total_value_of_share]">Total Value of Share</li>
							<li data-value="[vesting_years]">Vesting Years</li>
							<li data-value="[accepted_shares]">Accepted Share</li>
							<li data-value="[value_of_accepted_shares]">Value of Accepted Shares</li>
							<li data-value="[date_of_letter]">Date of Letter</li>
							<li data-value="[sender_name]">Sender Name</li>	
							<li data-value="[sender_rank]">Sender Rank</li>						
							<li data-value="[sender_department]">Sender Department</li>
							<li data-value="[recipient_name]">Recipient Name</li>
							<li data-value="[recipient_rank]">Recipient Rank</li>
							<li data-value="[recipient_department]">Recipient Department</li>	
							<li data-value="[company_logo]">Company Logo</li>
							<li data-value="[offer_letter_date]">Offer Letter Date</li>								
							<li ></li>								
						</ul>
					</div>

					<div class="clear"></div>

				</div>

			</div>
				
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			
			<button type="button" class="display-inline-mid btn-normal alert-btn update-letter" data-id="<?php echo $offer_letter->id?>" data-name="<?php echo $offer_letter->name?>">Submit</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- archive letter -->
<div class="modal-container archive-letter" modal-id="archive-letter">
	<div class="modal-body max-width-1200 width-500px ">
		<div class="modal-head ">
			<h4 class="text-left">ARCHIVE LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content ">	
			<div class="head">
				<div class="success archive_letter_success_message margin-bottom-10 hidden"></div>
				<div class="error archive_letter_error_message margin-bottom-10 hidden"></div>
			</div>
			<strong> Are you sure you really want to archive this letter?</strong>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			
			<button type="button" class="display-inline-mid btn-normal alert-btn archive-letter" data-id="<?php echo $offer_letter->id?>" data-name="<?php echo $offer_letter->name?>">Confirm</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

	<?php } ?>
<?php } ?>

<script type="text/javascript">
	var oGetLogsParams = {
	   "where" : [
	       {
	           "field" : "ref_id",
	           "operator" : "IN",
	           "value" : "("+arrLogIDS.join()+")"
	       },
	       {
	           "field" : "type",
	           "operator" : "IN",
	           "value" : "('offer_letter')"
	       }
	   ]
	};
	cr8v.get_audit_logs(oGetLogsParams, $('[section-style="content-panel"]'));
</script>