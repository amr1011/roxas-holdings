<section section-style="top-panel">
	<div class="content" data-container="company_information">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<!-- <h1 class="f-left">View Company Information</h1> -->
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="<?php echo base_url(); ?>companies/company_list">Company List</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a data-label="company_name"></a>
			</div>
			<div class="f-right">
				<button class="btn-normal display-inline-mid modal-trigger margin-right-10" modal-target="add-department">Add Department</button>
				<button class="btn-normal display-inline-mid modal-trigger" modal-target="edit-company">Edit Company</button>				
			</div>
			<div class="clear"></div>
		</div>

		<div class="company-logo" data-label="company_image">
			<img src="<?php echo base_url(); ?>/assets/images/mountain.png" alt="company-img" class=" width-200px height-200px display-inline-mid" />
			<div class="padding-left-20 display-inline-mid">
				<h2 data-label="company_name"><!-- Cr8v Web Solution, Inc. --></h2>
				
			</div>
		</div>
	
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="text-right-line margin-top-5">
			<div class="line"></div>
			<div class="content-text" id="sort_department">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-department-name">Department Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-department-rank-count">No. of Ranks <i class="fa fa-chevron-down"></i></a></p>					
			</div>
		</div>

		<h2 class="margin-top-40">Department List</h2>
		
		<table class="table-roxas comp-info">
			<thead>
				<tr>
					<th class="width-200px">Code</th>
					<th class="width-200px">Name</th>
					<th>Description</th>
					<th class="width-200px">No. of Ranks</th>					
				</tr>
			</thead>		
		</table>
		<div data-container="department_list_container">
			<div class="data-box width-100percent margin-0px fake-table no-border department template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td data-label="department_code" class="width-200px text-center"><!-- 101001 --></td>
							<td data-label="department_name" class="width-200px text-center"><!-- HR Department --></td>
							<td data-label="department_description" class="padding-left-20 padding-right-30"><!-- Zombies reversus ab inferno, nam malum cerebro --></td>
							<td data-label="department_rank_count" class="width-200px text-center">0</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center no-border">
					<a href="javascript:void(0)" class="view_ranks">
						<button class="btn-normal">View Department</button>
					</a>
				</div>
			</div>

			<div class="data-box width-100percent margin-0px fake-table no-border no_results_found template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="4" class="text-center">No Results found</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- <div class="data-box width-100percent margin-0px fake-table no-border">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center no-border">
				<a href="COMPANY-LIST-view-dept.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>

		<div class="data-box width-100percent margin-0px fake-table no-border">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center no-border">
				<a href="COMPANY-LIST-view-dept.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>

		<div class="data-box width-100percent margin-0px fake-table no-border">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center no-border">
				<a href="COMPANY-LIST-view-dept.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div>

		<div class="data-box width-100percent margin-0px fake-table no-border">
			<table>
				<tbody>
					<tr>
						<td class="width-150px text-center">101001</td>
						<td class="width-150px text-center">HR Department</td>
						<td>Zombies reversus ab inferno, nam malum cerebro. De carne animata corpora quaeritis. Summus sit​​, morbo vel maleficia? De Apocalypsi undead dictum mauris. Hi mortuis soulless creaturas, imo monstra adventus vultus comedat cerebella viventium. Qui offenderit rapto, terribilem incessu. The voodoo sacerdos suscitat mortuos comedere carnem. Search for solum oculi eorum defunctis cerebro. Nescio an Undead zombies. Sicut malus movie horror.</td>
						<td class="width-150px text-center">10</td>
					</tr>
				</tbody>
			</table>
			<div class="data-hover text-center no-border">
				<a href="COMPANY-LIST-view-dept.php">
					<button class="btn-normal">View Department</button>
				</a>
			</div>
		</div> -->

		<table class="table-roxas comp-info1">
			<thead>
				<tr class="height-40px">
					<th></th>
					<th></th>
					<th></th>					
				</tr>
			</thead>		
		</table>

		<div data-container="audit_logs" class="hidden">
			<div class="panel-group text-left margin-top-30">
				<div class="accordion_custom">
					<div class="panel-heading border-10px">
						<a href="#">
							<h4 class="panel-title white-color">							
								Audit Logs
								<i class="change-font fa fa-caret-right font-left"></i>
								<i class="fa fa-caret-down font-right"></i>							
							</h4>
						</a>																	
						<div class="clear"></div>					
					</div>					
					<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
						<div class="panel-body ">

							<table class="table-roxas">
								<thead>
									<tr>
										<th>Date of Activity</th>
										<th>User</th>
										<th>Activity Description</th>
									</tr>
								</thead>
								<tbody data-container="logs">
									<tr class="logs template hidden">
										<td data-label="date_created">September 10, 2015</td>
										<td data-label="created_by">ROXAS, PEDRO OLGADO</td>
										<td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
									</tr>
									<!-- <tr>
										<td>September 10, 2015</td>
										<td>VALENCIA, RENATO CRUZ</td>
										<td>Created Department  <span class="font-bold">"HR Department"</span></td>
									</tr> -->
								</tbody>
							</table>
						</div>			
					</div>
				</div>
			</div>
		</div>
	<div>
</section>

<!-- edit ESOP -->
<div class="modal-container" modal-id="edit-company">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EDIT COMPANY INFORMATION</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content  padding-left-0 padding-right-0">
			<div class="error edit_company_error_message hidden margin-bottom-10">
			</div>
			<div class="success edit_company_success_message hidden margin-bottom-10">
			</div>

			<div class="upload-photo upload-photo-modal position-rel width-150px height-150px" data-container="image_container">
				<div class="width-100percent">
					<i class="fa fa-camera fa-3x"></i>
					<p class="margin-top-10 ">Upload Photo</p>
				</div>
				<img class="width-100percent position-abs height-100percent top-0 left-0">
			</div>
			<label>
	            <input type="file" name="company_image" class="hidden" />
			</label>

			<form id="edit_company_form">
				<table class="width-100percent margin-top-10">
					<tbody>
						<tr>
							<td class="text-right padding-right-10">Company Code: <span class="red-color">*</span></td>
							<td class="text-center"><input type="text" class="normal " value="" name="company_code" datavalid="required" labelinput="Company Code"/></td>
						</tr>
						<tr>
							<td class="text-right padding-right-10">Company Name: <span class="red-color">*</span></td>
							<td class="text-center">
								<input type="text" class="normal" name="company_name" datavalid="required" labelinput="Company Name"/>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			
			<button type="button" class="display-inline-mid btn-normal" id="edit_company_btn">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-department">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD DEPARTMENT</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<div class="error add_department_error_message hidden margin-bottom-10">
			</div>
			<div class="success add_department_success_message hidden margin-bottom-10">
			</div>
			<form id="add_department_form">
				<table class="">
					<tr>
						<td>Department Code: <span class="red-color">*</span></td> 
						<td><input type="text" class="normal margin-left-20 add-border-radius-5px  width-300px code-generated"  name="department_code" datavalid="required" labelinput="Department Code" /></td>
					</tr>
					<tr>
						<td>Department Name: <span class="red-color">*</span></td>
						<td><input type="text" class="normal margin-left-20 width-300px add-border-radius-5px"  name="department_name" datavalid="required" labelinput="Department Name" /></td>
					</tr>
					<tr>
						<td class="v-top">Description: <span class="red-color">*</span></td>
						<td><textarea class="margin-left-20 black-color width-300px add-border-radius-5px"  name="department_description" datavalid="required" labelinput="Department Description" ></textarea></td>
					</tr>
				</table>
			</form>			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal" id="add_department_btn">Add Department</button>
			
		</div>
		<div class="clear"></div>
	</div>
</div>