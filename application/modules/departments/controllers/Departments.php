<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departments extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }

        $this->load->model('Departments/Departments_model');

        $allowed = $this->check_user_role();

        if(!$allowed)
        {
            // header('Location: '.base_url().'esop');
            header('Location: '.base_url().'auth/forbidden_403');
        }
    }

    public function index ()
    {
        header('Location: '.base_url().'deparments/deparmtent_list');
    }
    
    
    public function department_list ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/companies.js' );
        $this->template->add_script( assets_url() . '/js/libraries/departments.js' );
        $this->template->add_content( $this->load->view( 'department_list', $data, TRUE ) );
        $this->template->draw();
    }

    public function get ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'name' => 'd.name',
                    'department_code' => 'd.department_code',
                    'description' => 'd.description',
                    'company_id' => 'd.company_id',
                    'rank_count' => 'rank_count',
                    'date_added' => 'd.date_added',
                    'id' => 'd.id',
                    'is_deleted' => 'd.is_deleted',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "d.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;
            $departments = $this->Departments_model->get_department_data($qualifiers);

            if(count($departments) > 0)
            {
                $response['total_rows'] = $departments['total_rows'];
                unset($departments['total_rows']);
                $response['data'] = $departments;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function add ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $department_information = $this->input->post();

        if(isset($department_information) && count($department_information) > 0)
        {
            /*validate department code*/
            if(isset($department_information['department_code']) AND strlen($department_information['department_code']) > 0)
            {
                if(strlen($department_information['department_code']) > 0 AND preg_match("/^[0-9]+$/", trim($department_information['department_code'])) )
                {
                    $department_information_input['department_code'] = $department_information['department_code'];
                }
                else
                {
                    $response['message'][] = "Invalid department code or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Department Code is required.";
                $error++; 
            }

            /*validate department name*/
            if(isset($department_information['name']) AND strlen($department_information['name']) > 0)
            {
                if(strlen($department_information['name']) > 1 AND preg_match("/[a-z||A-Z]/", trim($department_information['name'])) )
                {
                    if(preg_match("/^[a-zA-Z0-9\-\.\&\,\/ ]+$/", trim($department_information['name'])))
                    {
                        $department_information_input['name'] = $department_information['name'];
                    }
                    else
                    {
                        $response['message'][] = "Invalid department name or is too short";
                        $error++;
                    }
                }
                else
                {
                    $response['message'][] = "Invalid department name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Department Name is required.";
                $error++; 
            }

            /*validate department description*/
            if(isset($department_information['description']) AND strlen($department_information['description']) > 0)
            {
                if(strlen(trim($department_information['description'])) > 1 AND preg_match("/[a-z||A-Z|| ]/", trim($department_information['description'])) )
                {
                    if(strlen(trim($department_information['description'])) > 1)
                    {
                        $department_information_input['description'] = $department_information['description'];
                    }
                    else
                    {
                        $response['message'][] = "Invalid department description or is too short";
                        $error++;
                    }
                }
                else
                {
                    $response['message'][] = "Invalid department description or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Department description is required.";
                $error++; 
            }

            /*validate department company id*/
            if(isset($department_information['company_id']) AND strlen($department_information['company_id']) > 0)
            {
                if(strlen($department_information['company_id']) > 0 AND preg_match("/^[0-9]+$/", trim($department_information['company_id'])) )
                {
                    $department_information_input['company_id'] = $department_information['company_id'];
                }
                else
                {
                    $response['message'][] = "Invalid company id or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Company id is required.";
                $error++; 
            }

            $department_information_input['is_deleted'] = 0;
            $department_information_input['date_added'] = date('Y-m-d H:i:s');
            
            if($error == 0)
            {
                /*validate company code if existing*/
                $check_existing_qualifiers = array(
                    'where' => array(
                        array(
                            'field' => 'department_code',
                            'operator' => '=',
                            'value' => "'".$department_information['department_code']."'",
                        )
                    )
                );
                $existing_department_code = $this->Departments_model->check_existing_data($check_existing_qualifiers, 'department');

                if($existing_department_code > 0)
                {
                    $response['message'][] = "Department Code is already existing.";
                    $error++;
                }

                /*insert if all inputs are okay*/
                if($existing_department_code == 0 && $error == 0)
                {
                    $added_department_information_id = $this->Departments_model->insert_department_information($department_information_input);

                    if($added_department_information_id > 0)
                    {
                        $response['message'][] = "Department added successfully.";
                        $response['status'] = TRUE;
                        $response['data'] = $department_information_input;
                        $response['data']['id'] = $added_department_information_id;

                        /*for logs*/
                        $_POST['params'] = $response['data'];
                        $_POST['status'] = TRUE;
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function edit ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $department_information = $this->input->post();

        if(isset($department_information) && count($department_information) > 0)
        {
            if(isset($department_information['data']) AND is_array($department_information['data']) AND count($department_information['data']) > 0 AND isset($department_information['id']) AND $department_information['id'] > 0)
            {
                foreach ( $department_information['data'][0] as $i => $update_data )
                {
                    if ( strlen( $i ) > 0 )
                    {
                        /*validate fields*/

                        /*department name validation*/
                        if ($i == 'name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1 AND preg_match("/[a-z||A-Z]/", trim($update_data)))
                                {
                                    if(preg_match("/^[a-zA-Z0-9\-\.\&\,\/ ]+$/", trim($update_data)))
                                    {
                                        $department_information_input['name'] = $update_data;
                                    }
                                    else
                                    {
                                        $response['message'][] = "Department name invalid or is too short";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Department name invalid or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Department name is required";
                                $error ++;
                            }
                        }

                        /*department code validation*/
                        if ($i == 'department_code')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    /*validate department code if existing*/
                                    $check_existing_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'department_code',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            ),
                                            array(
                                                'field' => 'id',
                                                'operator' => '!=',
                                                'value' => "'".$department_information['id']."'",
                                            ),
                                        )
                                    );
                                    $existing_department_code = $this->Departments_model->check_existing_data($check_existing_qualifiers, 'department');

                                    if($existing_department_code > 0)
                                    {
                                        $response['message'][] = "Department Code is already existing.";
                                        $error++;
                                    }
                                    else
                                    {
                                        $department_information_input['department_code'] = $update_data;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Department code invalid or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Department code is required";
                                $error ++;
                            }
                        }

                        /*department description validation*/
                        if ($i == 'description')
                        {
                            if(strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 1 AND preg_match("/[a-z||A-Z|| ]/", trim($update_data)))
                                {
                                    if(strlen(trim($update_data)) > 1)
                                    {
                                        $department_information_input['description'] = $update_data;
                                    }
                                    else
                                    {
                                        $response['message'][] = "Department description invalid or is too short";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Department description invalid or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Department description is required";
                                $error ++;
                            }
                        }
                    }
                }

                if($error == 0)
                {
                    /*update if all inputs are okay*/
                    if($error == 0)
                    {
                        $logs_from = $this->Departments_model->get_department_data(
                                array(
                                    'where' => array(
                                        array(
                                            'field' => 'd.id',
                                            'operator' => '=',
                                            'value' => $department_information['id'],
                                        )
                                    )
                                )
                            );
                        $updated_department_information_id = $this->Departments_model->update_department_information(
                            array(
                                array(
                                'field' => 'd.id',
                                'value' => $department_information['id'],
                                'operator' => '='
                                )
                            ), 
                            $department_information['data'][0]);

                        if($updated_department_information_id > 0)
                        {
                            $response['message'][] = "Department updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $department_information_input;
                            $response['data']['id'] = $department_information['id'];

                            /*for logs*/
                            $_POST['params']['id'] = $department_information['id'];
                            $_POST['params']['to'] = $response['data'];
                            $_POST['params']['from'] = $logs_from[0];
                            $_POST['params']['diff'] = array_diff_assoc($_POST['params']['to'], $_POST['params']['from']);
                            $_POST['status'] = TRUE;
                        }
                        else
                        {
                            // $response['message'][] = "Nothing is changed.";
                            $response['message'][] = "Department updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $department_information_input;
                            $response['data']['id'] = $department_information['id'];
                        }
                        $response['data']['affected_rows'] = $updated_department_information_id;
                    }
                }
            }
        }

        echo json_encode($response);
    }

}

/* End of file Departments.php */
/* Location: ./application/modules/departments/controllers/Departments.php */