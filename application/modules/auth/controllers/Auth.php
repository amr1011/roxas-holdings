<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        if( $this->session->userdata('user_id') != '' ) //if session is found load main page
        {
           // header('Location: '.base_url().'esop');
        }

        $this->load->model('Auth/Auth_model');

         $this->load->library('emailer');
         $this->load->model('users/Users_model');

    }

    public function index ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/auth.js' );
        $this->template->add_content( $this->load->view( 'login', $data, TRUE ) );
        $this->template->draw();
    } 

    public function reset_password ($rs)
    {
        if($this->Users_model->is_temp_pass_valid($rs))
        {
        // $data = array();
        $data["confirm_key"] = $rs;

        $this->template->add_script( assets_url() . '/js/libraries/auth.js' );
        $this->template->add_content( $this->load->view( 'confirm_view', $data, TRUE ) );
        $this->template->draw();

        }
        else
        {

        redirect(base_url()."auth");
        
        }
        

    }

    public function change_password ()
    {

        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        $error = 0;

        if(!isset($params['password']) AND strlen($params['password']) == 0)
        {
            $response['message'][] = 'Password is required.';
            $error++;
        }

        // if(!isset($params['user_role']) AND strlen($params['user_role']) == 0 AND number_greater_than_zero($params['user_role']))
        // {
        //     $response['message'][] = 'Please specify user role.';
        //     $error++;
        // }

        if($error == 0)
        {

            $key = $params['rs'];

            $get_user_qualifiers = array(
                    "where" => 
                        array(
                            array(
                                "field" => 'u.rs',
                                "value" => $key
                            )/*,
                            array(
                                "field" => 'u.user_role',
                                "value" => 'asd'
                            )*/
                        )
                );

            $user_information = $this->Auth_model->get_user_data($get_user_qualifiers);

            if(isset($user_information) AND is_array($user_information) AND count($user_information) > 0)
            {
                if(isset($user_information['is_deleted']) AND $user_information['is_deleted'] == 0)
                {
                    if (isset($user_information['rs']) AND $user_information['rs'] == $key)
                    {
                        $this->load->library("password");
                        $data = array(
                                       'password' => $this->password->encrypt(xss_clean($params['password'])),
                                        'rs'      => ''
                                    );
                        $this->db->where('rs', $key);
                        $this->db->update('user', $data);


                        $response['data'] = $user_information;
                        $response['message'][] = "Password reset complete.";
                        $response['status'] = TRUE;

                            // echo $try_print;

                            // print_r($try_print);
                            // exit();

                            // end Email Notification



                    } else {
                        $response['message'][] = "Incorrect reset";
                    }
                }
                else
                {
                    $response['message'][] = "Unregistered email account already deactivated.";
                }
            }
            else
            {
                $response['message'][] = "Unregistered email account.";
            }
        }

        echo json_encode($response);
    }


    public function forbidden_403 ()
    {
        $this->load->view('403');
    }    

    public function authenticate()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        $error = 0;

        if(!isset($params['user_name']) AND strlen($params['user_name']) == 0)
        {
            $response['message'][] = 'User Name is required.';
            $error++;
        }

        if(!isset($params['password']) AND strlen($params['password']) == 0)
        {
            $response['message'][] = 'Password is required.';
            $error++;
        }

        // if(!isset($params['user_role']) AND strlen($params['user_role']) == 0 AND number_greater_than_zero($params['user_role']))
        // {
        //     $response['message'][] = 'Please specify user role.';
        //     $error++;
        // }

        if($error == 0)
        {

            $username = $params['user_name'];
            $password = $params['password'] ;
            // $userrole = $params['user_role'];

            $get_user_qualifiers = array(
                    "where" => 
                        array(
                            array(
                                "field" => 'u.user_name',
                                "value" => $username
                            )/*,
                            array(
                                "field" => 'u.user_role',
                                "value" => 'asd'
                            )*/
                        )
                );

            $user_information = $this->Auth_model->get_user_data($get_user_qualifiers);

            if(isset($user_information) AND is_array($user_information) AND count($user_information) > 0)
            {
                if(isset($user_information['is_deleted']) AND $user_information['is_deleted'] == 0)
                {
                    $this->load->library("password");
                    if (isset($user_information['password']) && $this->password->match($password, $user_information['password']))
                    {
                        unset($user_information['password']);

                        $response['data'] = $user_information;
                        $response['message'][] = "Login Succesful.";
                        $response['status'] = TRUE;


                         // For Notification

                       
                        $get_user_stock_offer = $this->Auth_model->get_user_stock_offer($user_information['id']);
                        

                            if(count($get_user_stock_offer) > 0)
                            {
                                foreach ($get_user_stock_offer as $user_key => $user_value) {

                                   
                                    $check_10_days = $this->Auth_model->check_10_days_notif($user_value['esop_id']);

                                        if(count($check_10_days) > 0)
                                        {
                                            

                                            foreach ($check_10_days as $key => $value) {
                                                $count_parent =  $this->Auth_model->check_count_parent_notif($value["notif_id"], $user_information['id']);
                                                $count_expiration_parent = $this->Auth_model->check_count_expiration_parent_notif($value["notif_id"], $user_information['id']);

                                                 // for 10 days Remaining
                                                if($count_parent == 0)
                                                {

                                                    // For 10 days to second = 864000
                                                    $date_timestamp = strtotime($value["date_created"]);
                                                    $date_now = date("Y-m-d H:i:s");
                                                    $date_now = strtotime($date_now);
                                                    $total = $date_timestamp - $date_now;

                                                    if(864000 > $total)
                                                    {
                                                        $arr_build_10_remaining = array(
                                                            "user_id"=>$value["user_id"],
                                                            "user_role"=>"1,2,3,4",
                                                            "date_created"=>date("Y-m-d H:i:s"),
                                                            "value"=>'Your offer in '.$value["esop_name"].' will expire in 10 Days. Please go to the Offers page to check it out before it’s gone!',
                                                            "action_note"=>"10 Days Remaining",
                                                            "ref_id"=>$user_information['id'],
                                                            "parent_id"=>$value["notif_id"]
                                                        );

                                                        $this->Auth_model->save_remaining_days_notif($arr_build_10_remaining);
                                                    }
                                                     
 
                                                }
                                                //  end for 10 days Remaining

                                                // for Expiration Date
                                                if($count_expiration_parent == 0)
                                                {
                                                    
                                                    $date_offer_expire = strtotime($value["offer_expiration"]);
                                                    $date_expire_now = date("Y-m-d");
                                                    $date_expire_now = strtotime($date_expire_now);

                                                    if(  $date_expire_now >= $date_offer_expire )
                                                    {
                                                        $arr_build_expiration = array(
                                                            "user_id"=>$value["user_id"],
                                                            "user_role"=>"1,2,3,4",
                                                            "date_created"=>date("Y-m-d H:i:s"),
                                                            "value"=>'Your offer in '.$value["esop_name"].' has now expired.',
                                                            "action_note"=>"Offer has expired",
                                                            "ref_id"=>$user_information['id'],
                                                            "parent_id"=>$value["notif_id"]
                                                        );

                                                        $this->Auth_model->save_expiration_date_notif($arr_build_expiration);
                                                    }
                                                    
                                                }
                                                // end for Expiration Date

                                            }
                                        }
                                   

                                   
                                    // $check_expiration = $this->Auth_model->check_expiration_notif($user_value['esop_id']);





                                    

                                }
                            }
                           





                        // end For Notification



                    } else {
                        $response['message'][] = "Incorrect user password.";
                    }
                }
                else
                {
                    $response['message'][] = "Unregistered user account already deactivated.";
                }
            }
            else
            {
                $response['message'][] = "Unregistered user account.";
            }
        }

        echo json_encode($response);



    }

    public function authenticate_email()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        $error = 0;

        if(!isset($params['email']) AND strlen($params['email']) == 0)
        {
            $response['message'][] = 'Email is required.';
            $error++;
        }

        // if(!isset($params['user_role']) AND strlen($params['user_role']) == 0 AND number_greater_than_zero($params['user_role']))
        // {
        //     $response['message'][] = 'Please specify user role.';
        //     $error++;
        // }

        if($error == 0)
        {

            $email = $params['email'];

            $get_user_qualifiers = array(
                    "where" => 
                        array(
                            array(
                                "field" => 'email',
                                "value" => $email
                            )/*,
                            array(
                                "field" => 'u.user_role',
                                "value" => 'asd'
                            )*/
                        )
                );

            $user_information = $this->Auth_model->get_user_data($get_user_qualifiers);

            if(isset($user_information) AND is_array($user_information) AND count($user_information) > 0)
            {
                if(isset($user_information['is_deleted']) AND $user_information['is_deleted'] == 0)
                {
                    if (isset($user_information['email']) AND $user_information['email'] == $email)
                    {

                        $this->load->helper('string');
                        $rs = random_string('alnum', 30);

                        $data = array(
                                       'rs' => $rs
                                    );
                        $this->db->where('email', $email);
                        $this->db->update('user', $data);

                         // For Email Notification


                            $password_notif = array(
                                    "to"=>$email,
                                    "user_name"=>$user_information['name'],
                                    "subject" => "Forgot Password",
                                    "message"=>"<p>We've received a request from you to reset your password. If you didn't make the request, just ignore this email. Otherwise, you can reset your password by clicking this link:</p>
                                        <button class='btn-normal display-inline-mid width-300px'>
                                        <a href='http://localhost/roxas/auth/reset_password/".$rs."'>
                                        Reset Password</a></button>
                                        <p class='margin-bottom-20'>If you have anymore concerns, kindly contact your HR Officer.</p>
                                        <p>Thank You,<br> RHI ESOP Team</p>
                                    "
                                );

                            
                            $this->emailer->send_mail($password_notif);

                            $response['data'] = $user_information;
                            $response['message'][] = "An email has been sent to your email for password reset.";
                            $response['status'] = TRUE;



                    } else {
                        $response['message'][] = "Incorrect user email";
                    }
                }
                else
                {
                    $response['message'][] = "Unregistered email account already deactivated.";
                }
            }
            else
            {
                $response['message'][] = "Unregistered email account.";
            }
        }

        echo json_encode($response);
    }

    
    public function save_session()
    {
        $return = array();
        $data = array();
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        $user_info = $params;

        $newdata = array(
            'user_id'               => $user_info['id'],
            'user_name'             => $user_info['user_name'],
            'full_name'             => $user_info['name'],
            'user_role'             => $user_info['user_role'],
            'employee_code'         => $user_info['employee_code'],
            'first_name'            => $user_info['first_name'],
            'middle_name'           => $user_info['middle_name'],
            'last_name'             => $user_info['last_name'],
            'email'                 => $user_info['email'],
            'is_deleted'            => $user_info['is_deleted'],
            'contact_number'        => $user_info['contact_number'],
            'contact_number_type'   => $user_info['contact_number_type'],
            'company_id'            => $user_info['company_id'],
            'department_id'         => $user_info['department_id'],
            'rank_id'               => $user_info['rank_id'],
            'company_name'          => $user_info['company_name'],
            'department_name'       => $user_info['department_name'],
            'rank_name'             => $user_info['rank_name'],
            'user_role_name'        => $user_info['user_role_name'],
            'img'                   => $user_info['img'],
            'logged_in'             => TRUE
        );

        $this->session->set_userdata($newdata);
        $response['status'] = TRUE;
        echo json_encode($response);
    }

    public function logout()
    {
        $newdata = array(
            'user_id'               => $this->session->userdata('user_id'),
            'user_name'             => $this->session->userdata('user_name'),
            'full_name'             => $this->session->userdata('name'),
            'user_role'             => $this->session->userdata('user_role'),
            'employee_code'         => $this->session->userdata('employee_code'),
            'first_name'            => $this->session->userdata('first_name'),
            'middle_name'           => $this->session->userdata('middle_name'),
            'last_name'             => $this->session->userdata('last_name'),
            'email'                 => $this->session->userdata('email'),
            'is_deleted'            => $this->session->userdata('is_deleted'),
            'contact_number'        => $this->session->userdata('contact_number'),
            'contact_number_type'   => $this->session->userdata('contact_number_type'),
            'company_id'            => $this->session->userdata('company_id'),
            'department_id'         => $this->session->userdata('department_id'),
            'rank_id'               => $this->session->userdata('rank_id'),
            'company_name'          => $this->session->userdata('company_name'),
            'department_name'       => $this->session->userdata('department_name'),
            'rank_name'             => $this->session->userdata('rank_name'),
            'user_role_name'        => $this->session->userdata('user_role_name'),
            'img'                   => $this->session->userdata('img'),
            'logged_in'             => FALSE
        );

        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        header('Location: '.base_url().'auth');
    }

    public function get_audit_logs(){
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if($this->input->is_ajax_request())
        {
            $params = $this->input->get();
            if(isset($params))
            {
                $qualifiers = array(
                        'where' => isset($params['where']) ? $params['where'] : array() ,
                        'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                        'search_field' => isset($search_field) ? $search_field : '' ,
                        'offset' => isset($params['offset']) ? $params['offset'] : 0,
                        'limit' => isset($params['limit']) ? $params['limit'] : 9999999,
                        'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "l.log_id",
                        'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                    );

                $response['qualifiers'] = $qualifiers;
                $audit_logs = $this->Auth_model->get_audit_logs($qualifiers);

                if(count($audit_logs) > 0)
                {
                    $response['status'] = TRUE;
                    $response['data'] = $audit_logs;
                }
            }
        }

        echo json_encode($response);
    }

    public function get_user_notif()
    {
        $user_id = $this->session->userdata('user_id');

        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );


         $user_role_id = $this->session->userdata('user_role');








        // if()
        // first loop get all like

        // select * where user_role = '1,2,3,4' AND ref_id = user_id


        // second get ref id if()user_role = 1,2,3,45

        // select * where user_role like '%user_role%' AND user_role != '1,2,3,4'
         $arr_store_data = array();
         $get_notif = "";
         $icount = 0;
         $icountAllrecords = 0;

        // $offset = 0;
        // $limit = 5;

        //  // if all notification
        //  $get_all_notif = $this->Auth_model->get_all_notif_employee();
        //  $$get_per_employee = "";

        //  foreach ($get_all_notif as $key => $value) {
        //      if($value["user_role"] == "1,2,3,4")
        //      {
        //         $get_per_employee = $this->Auth_model->get_per_employee_notif($user_id);

        //      }else{
        //          $get_per_employee = $this->Auth_model->get_per_heads_notif($user_role_id);
        //      }

        //      foreach ($get_per_employee as $perkey => $pervalue) {
        //             $new_data_notif = array(
        //                     "notif_id"=>$pervalue["notif_id"],
        //                     "user_id"=>$pervalue["user_id"],
        //                     "user_fullname"=>$pervalue["user_fullname"],
        //                     "image"=>$pervalue["image"],
        //                     "date_created"=>$pervalue["date_created"],
        //                     "value"=>$pervalue["value"],
        //                     "action_note"=>$pervalue["action_note"],

        //                 );
        //      }


        //  }









        // // First Query
        if($user_role_id == "1")
        {
            $offset = 0;
            $limit = 5;

             $get_notif = $this->Auth_model->get_user_notif_employee($user_id, $limit, $offset);

             $icountAllrecords = $this->count_user_notif($user_id);
             $icount = $this->count_user_notif_not_viewed($user_id);
             // $get_10_days = $this->Auth_model->get_user_notif_employee($user_id);


             if(count($get_notif) > 0)
             {
                foreach ($get_notif as $key => $value) {


                   $get_if_view =  $this->Auth_model->get_if_view_notif($user_id, $value["notif_id"]);
                   $get_notif[$key]["is_viewed"] = $get_if_view;

                   // if($get_if_view == 0)
                   // {
                   //      $icount++;
                   // }

                   if($value["image"] == "")
                   {
                     $get_notif[$key]["image"] = "/assets/images/profile/default_user_image.jpg";
                   }

                   array_push($arr_store_data, $get_notif[$key]);
                 }
             }           
             
        }else{


      

            $get_notif = $this->Auth_model->get_user_notif_heads($user_role_id);

            
            $icountAllrecords = $this->count_head_notif($user_role_id);
            $icount = $this->count_head_notif_not_viewed($user_id ,$user_role_id);




             if(count($get_notif) > 0)
             {
                $countArr = 0;

                 

                foreach ($get_notif as $key => $value) {

                   
                    


                    if(array_key_exists($key, $get_notif)){


                        if($value["user_role"] == '1,2,3,4')
                        {
                            $new_head_data_notif = $this->Auth_model->new_head_data_notif_all($value["notif_id"], $user_id);
                        }else{
                            $new_head_data_notif = $this->Auth_model->new_head_data_notif_heads($value["notif_id"], $user_id);
                        }


                        // echo "<pre>";
                        // print_r($new_head_data_notif);
                        // echo "</pre>";
                        // exit();

                        $get_if_view =  $this->Auth_model->get_if_view_notif($user_id, $value["notif_id"]);
                       $get_notif[$key]["is_viewed"] = $get_if_view;



                        if(count($new_head_data_notif ) > 0)
                        {
                            $new_array_data = array(
                                "notif_id"=>$new_head_data_notif[0]["notif_id"],
                                "user_id"=>$new_head_data_notif[0]["user_id"],
                               "user_fullname"=>$new_head_data_notif[0]["user_fullname"],
                               "image"=>$new_head_data_notif[0]["image"],
                               "date_created"=>$new_head_data_notif[0]["date_created"],
                               "value"=>$new_head_data_notif[0]["value"],
                               "action_note"=>$new_head_data_notif[0]["action_note"],
                               "is_viewed"=>$get_if_view

                            );

                            if($new_array_data["image"] == "")
                            {
                             $new_array_data["image"] = "/assets/images/profile/default_user_image.jpg";
                            }

                            array_push($arr_store_data, $new_array_data);



                            if($countArr == 4)
                            {
                                break;
                            }

                            $countArr++;

                        }                 

                    }
                 }
                
             }

        }

        // echo "<pre>";
        // print_r($arr_store_data);
        // echo "</pre>";
        // exit();



        $arr_gather_all = array(
                "notif_count"=>$icount,
                "count_all_records"=>$icountAllrecords,
                "notif_info"=>$arr_store_data
            );

  
        $response["data"] = $arr_gather_all;

         echo json_encode($response);
    }

    public function save_user_notif_ids()
    {
        $notif_id = $this->input->post('notif_id');
        $user_id = $this->session->userdata('user_id');

        if($notif_id != "" AND $user_id != "")
        {
            $arr_notif_data = array(
                    "notification_id"=>$notif_id,
                    "user_id"=>$user_id,
                );

            $result = $this->Auth_model->save_user_notif_ids($arr_notif_data);

            $response = array(
                "status" => FALSE,
                "message" => "Save User Notification",
                "data" =>$result
            );
        }else{
            $response = array(
                "status" => FALSE,
                "message" => "Save User Notification",
                "data" =>"Save User Notification Failed"
            );
        }

        
    }

    public function count_user_notif($user_id)
    {
        $result = $this->Auth_model->count_user_notif($user_id);
        return $result;
    }

    public function count_user_notif_not_viewed($user_id)
    {
        $get_notif = $this->Auth_model->count_user_notif_not_viewed($user_id);
        $icount = 0;

         foreach ($get_notif as $key => $value) {
           $get_if_view =  $this->Auth_model->get_if_view_notif($user_id, $value["notif_id"]);

               if($get_if_view == 0)
               {
                    $icount++;
               }

         }


        return $icount;
    }

    public function count_head_notif($user_role_id)
    {
        // $get_notif = $this->Auth_model->get_user_notif_heads($user_role_id, $limit, $offset);

        $get_count_all_head = $this->Auth_model->get_count_all_head($user_role_id);

        $result = 0;

        foreach($get_count_all_head as $key => $value) {
           
           if($value["user_role"] == '1,2,3,4')
           {
                $result += $this->Auth_model->count_head_notif_all($value["notif_id"]);
           }else{
                $result += $this->Auth_model->count_head_notif($value["notif_id"]);
           }
        }


        

         return $result;
    }

    public function count_head_notif_not_viewed($user_id, $user_role_id)
    {
        $get_count_all_head = $this->Auth_model->get_count_all_head($user_role_id);
        $icount = 0;

        if(count($get_count_all_head) > 0)
        {
            foreach ($get_count_all_head as $key => $value) {

                if($value["user_role"] == '1,2,3,4')
                {
                    $get_head_all_notif = $this->Auth_model->get_head_all_notif($user_id, $value["notif_id"]);

                }else{
                    $get_head_all_notif = $this->Auth_model->get_head_all_notif_selected($user_id, $value["notif_id"]);
                }

                
                if(count($get_head_all_notif) > 0)
                {
                    $get_if_view =  $this->Auth_model->get_if_view_notif($user_id, $get_head_all_notif[0]["id"]);

                   if($get_if_view == 0)
                   {
                        $icount++;
                   }
                }
                
                   
                
            }
            
        }


        return $icount;
    }

    public function add_more_notif()
    {
        $limit = $this->input->post('limit');
        $offset = $this->input->post('offset');

        if($limit != 0 AND $offset !=0)
        {
            $limit_head = $limit+5;
            $user_id = $this->session->userdata('user_id');

            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );


             $user_role_id = $this->session->userdata('user_role');

             $arr_store_data = array();
             $get_notif = "";
             $icount = 0;
             $icountAllrecords = 0;





            // // First Query
            if($user_role_id == "1")
            {


                 $get_notif = $this->Auth_model->get_user_notif_employee($user_id, $limit, $offset);

                 $icountAllrecords = $this->count_user_notif($user_id);


                 if(count($get_notif) > 0)
                 {
                    foreach ($get_notif as $key => $value) {
                       $get_if_view =  $this->Auth_model->get_if_view_notif($user_id, $value["notif_id"]);

                       $get_remaining_days = $this->Auth_model->get_if_view_notif($user_id, $value["notif_id"]);

                       

                       $get_notif[$key]["is_viewed"] = $get_if_view;

                       if($get_if_view == 0)
                       {
                            $icount++;
                       }

                       if($value["image"] == "")
                       {
                         $get_notif[$key]["image"] = "/assets/images/profile/default_user_image.jpg";
                       }

                       array_push($arr_store_data, $get_notif[$key]);
                     }
                 }           
                 
            }else{

                // kprint($arr_store_data); exit;


               


            $get_notif = $this->Auth_model->get_user_notif_heads($user_role_id);

            
            $icountAllrecords = $this->count_head_notif($user_role_id);


             if(count($get_notif) > 0)
             {
                $countArr = 0;
               

                foreach ($get_notif as $key => $value) {

                   
                    

                    if(array_key_exists($key, $get_notif)){


                        if($value["user_role"] == '1,2,3,4')
                        {
                            $new_head_data_notif = $this->Auth_model->new_head_data_notif_all($value["notif_id"], $user_id);
                        }else{
                            $new_head_data_notif = $this->Auth_model->new_head_data_notif_heads($value["notif_id"], $user_id);
                        }


                        // echo "<pre>";
                        // print_r($new_head_data_notif);
                        // echo "</pre>";
                        // exit();

                        $get_if_view =  $this->Auth_model->get_if_view_notif($user_id, $value["notif_id"]);
                       $get_notif[$key]["is_viewed"] = $get_if_view;


                        if(count($new_head_data_notif ) > 0)
                        {
                            if($limit <= $countArr && $limit_head>$countArr)
                            {
                                $new_array_data = array(
                                    "notif_id"=>$new_head_data_notif[0]["notif_id"],
                                    "user_id"=>$new_head_data_notif[0]["user_id"],
                                   "user_fullname"=>$new_head_data_notif[0]["user_fullname"],
                                   "image"=>$new_head_data_notif[0]["image"],
                                   "date_created"=>$new_head_data_notif[0]["date_created"],
                                   "value"=>$new_head_data_notif[0]["value"],
                                   "action_note"=>$new_head_data_notif[0]["action_note"],
                                   "is_viewed"=>$get_if_view

                                );

                                if($new_array_data["image"] == "")
                                {
                                 $new_array_data["image"] = "/assets/images/profile/default_user_image.jpg";
                                }

                                array_push($arr_store_data, $new_array_data);


                                

                                
                            }
                            $countArr++;

                            if($countArr == $limit_head)
                            {
                                break;
                            }
                            

                        }                 

                    }
                 }
                
             }

            }



            $arr_gather_all = array(
                    "notif_count"=>$icount,
                    "count_all_records"=>$icountAllrecords,
                    "notif_info"=>$arr_store_data
                );

            // print_r( $arr_gather_all);
            // exit();

      
            $response["data"] = $arr_gather_all;

             echo json_encode($response);
        }
    }

}

/* End of file auth.php */
/* Location: ./application/modules/auth/controllers/auth.php */