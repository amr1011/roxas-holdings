<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends PG_Model {

    public function get_user_data ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT u.*,
                CONCAT(u.first_name, \', \', u.last_name, \' \', u.middle_name) as name,
                c.name as company_name,
                d.name as department_name,
                r.name as rank_name,
                ur.description as user_role_name
            FROM "'.'user" u
            LEFT JOIN 
                "'.'rank" r ON u.rank_id = r.id
            LEFT JOIN 
                "'.'department" d ON u.department_id = d.id
            LEFT JOIN 
                "'.'company" c ON u.company_id = c.id
            LEFT JOIN 
                "'.'user_role" ur ON u.user_role = ur.id';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " WHERE ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " WHERE ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'u.user_name')
            {
                $sql .= " WHERE ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'c.company_name')
            {
                $sql .= " WHERE ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'name')
            {
                $sql .= " WHERE (u.first_name LIKE '%".$qualifiers['keyword']."%' OR u.middle_name LIKE '%".$qualifiers['keyword']."%' OR u.last_name LIKE '%".$qualifiers['keyword']."%' 
                            OR CONCAT(u.first_name, ' ', u.middle_name, ' ', u.last_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.first_name, ' ', u.last_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.last_name, ' ',u.middle_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.last_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.last_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.last_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.first_name, ' ',u.last_name) LIKE '%".$qualifiers['keyword']."%' ) ";
            }
            else if($qualifiers['search_field'] == 'ur.description')
            {
                $sql .= " WHERE ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else
            {
                $sql .= " WHERE ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
            
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "u.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY u.id,c.name,d.name,r.name,ur.description";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['offset'].", ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->row_array()) > 0)
        {
            $result = $query->row_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_user_email ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT *
            FROM user
            WHERE TRUE ';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }


        $query = $this->db->query($sql);
        if(count($query->row_array()) > 0)
        {
            $result = $query->row_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_audit_logs($qualifiers = array()) {

        $result = array();
        $sql = 'SELECT l.*, CONCAT(u.last_name, \', \', u.first_name, \' \', u.middle_name) as created_by_name 
            FROM "logs" l 
            LEFT JOIN "user" u ON u.id = l.user_id 
            WHERE TRUE ';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        if($where['operator'] == 'IN')
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                        }
                        else
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                        }
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "l.log_id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY l.log_id,created_by_name,l.user_id,l.type,l.date_created,l.value,l.action_note,l.ref_id ";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    // public function get_user_notif($qualifiers = array()) {

    //     $result = array();
    //     $sql = "SELECT 
    //             SQL_CALC_FOUND_ROWS
    //             n.*,
    //             CONCAT(u.last_name, ', ', u.first_name, ' ', u.middle_name) as created_by_name
    //         FROM logs l
    //         LEFT JOIN
    //             user u ON u.id =n.user_id
    //         WHERE 1 ";
            
    //     if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
    //     {
    //         foreach($qualifiers['where'] as $key => $where)
    //         {
    //             if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
    //             {
    //                 if(isset($where['operator']) AND strlen($where['operator']) > 0)
    //                 {
    //                     if($where['operator'] == 'IN')
    //                     {
    //                         $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
    //                     }
    //                     else
    //                     {
    //                         $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
    //                     }
    //                 }
    //                 else
    //                 {
    //                     $sql .= " AND ".$where['field']." = '".$where['value']."' ";
    //                 }
    //             }
    //         }
    //     }
    //     if(isset($qualifiers['custom']))
    //     {
    //         foreach ($variable as $key => $value) {
    //             # code...
    //         }
    //     }
    //     if(!isset($qualifiers['order_by']))
    //     {
    //         $order_by = "l.log_id";
    //     }
    //     else
    //     {
    //         $order_by = $qualifiers['order_by'];
    //     }

    //     $sql .= " GROUP BY l.log_id";


    //     if(isset($qualifiers['sorting']))
    //     {
    //         $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
    //     }

    //     if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
    //     {
    //         $sql .= " LIMIT ".$qualifiers['offset'].", ".$qualifiers['limit']." ";
    //     }

    //     $query = $this->db->query($sql);
    //     if(count($query->result_array()) > 0)
    //     {
    //         $result = $query->result_array();
    //     }

    //     // $sql2 = "SELECT FOUND_ROWS() as total";

    //     // $total_rows = $this->db->query($sql2)->row()->total;
    //     // $result["total_rows"] = $total_rows;

    //     return $result;
    // }

    // start for new

    public function get_all_notif_employee()
    {
        $sql = 'SELECT n.user_role
                FROM "notification" n
                LEFT JOIN "user" u ON u.id=n.user_id
                ORDER BY n.id
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_per_employee_notif($user_id)
    {
        $sql = 'SELECT n.id as notif_id, n.user_id,  CONCAT(u.last_name, \', \', u.first_name, \' \', u.middle_name) as user_fullname, u.img as image,
                to_char(n.date_created, \'Day Month DD,YYYY HH12:MI:SS AM\') as date_created, n.value, n.action_note
                FROM "notification" n
                LEFT JOIN "user" u ON u.id=n.user_id
                WHERE n.user_role=\'1,2,3,4\' AND n.ref_id =\''.$user_id.'\'
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_per_heads_notif()
    {
        $sql = 'SELECT n.id as notif_id, n.user_id,  CONCAT(u.last_name, \', \', u.first_name, \' \', u.middle_name) as user_fullname, u.img as image,
                to_char(n.date_created, \'Day Month DD,YYYY HH12:MI:SS AM\') as date_created, n.value, n.action_note
                FROM "notification" n
                LEFT JOIN "user" u ON u.id=n.user_id
                WHERE n.user_role LIKE \'%'.$user_role_id.'%\' AND n.user_role <> \'1,2,3,4\' 
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    // end of start new

    public function get_user_notif_employee($user_id, $limit, $offset)
    {
            $sql = 'SELECT n.id as notif_id, n.user_id, 
                CONCAT(u.last_name, \', \', u.first_name, \' \', u.middle_name) as user_fullname, 
                u.img as image, 
                to_char(n.date_created, \'Day Month DD,YYYY HH12:MI:SS AM\') as date_created, 
                n.value, n.action_note 
                FROM "'.'notification" n 
                LEFT JOIN "'.'user" u ON u.id=n.user_id 
                WHERE n.user_role=\'1,2,3,4\' AND n.ref_id =\''.$user_id.'\' ORDER BY n.id DESC LIMIT '.$limit.' OFFSET '.$offset;

        /*$sql = "SELECT n.id as notif_id, n.user_id,  CONCAT(u.last_name, ', ', u.first_name, ' ', u.middle_name) as user_fullname, u.img as image,
                DATE_FORMAT(n.date_created, '%W, %M %d, %Y %r') as date_created, n.value, n.action_note
                FROM notification n
                LEFT JOIN user u ON u.id=n.user_id
                WHERE n.user_role='1,2,3,4' AND n.ref_id ='{$user_id}' ORDER BY n.id DESC LIMIT $offset, $limit 
                ";*/
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_if_view_notif($user_id, $notif_id)
    {
        $sql = 'SELECT notification_id FROM "notification_map" WHERE notification_id=\''.$notif_id.'\' AND user_id=\''.$user_id.'\' ';
        /*$sql = "SELECT notification_id FROM notification_map WHERE notification_id='{$notif_id}' AND user_id='$user_id' ";*/
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function get_user_notif_heads($user_role_id)
    {
        $sql = 'SELECT n.id as notif_id, n.user_role
                FROM "notification" n
                WHERE n.user_role LIKE \'%'.$user_role_id.'%\' ORDER BY n.id DESC
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function new_head_data_notif_all($notif_id, $user_id)
    {
        $sql = 'SELECT n.id as notif_id, n.user_id,  CONCAT(u.last_name, \', \', u.first_name, \' \', u.middle_name) as user_fullname, u.img as image,
                to_char(n.date_created, \'Day Month DD,YYYY HH12:MI:SS AM\') as date_created, n.value, n.action_note, n.user_role
                FROM "notification" n
                LEFT JOIN "user" u ON u.id=n.user_id
                WHERE n.user_role=\'1,2,3,4\' AND n.id=\''.$notif_id.'\' AND n.ref_id=\''.$user_id.'\'
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function new_head_data_notif_heads($notif_id, $user_id)
    {
         $sql = 'SELECT n.id as notif_id, n.user_id,  CONCAT(u.last_name, \', \', u.first_name, \' \', u.middle_name) as user_fullname, u.img as image,
                to_char(n.date_created, \'Day Month DD,YYYY HH12:MI:SS AM\') as date_created, n.value, n.action_note, n.user_role
                FROM "notification" n
                LEFT JOIN "user" u ON u.id=n.user_id
                WHERE n.user_role<>\'1,2,3,4\' AND n.id=\''.$notif_id.'\'
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function save_user_notif_ids($arr_notif_data)
    {
        $this->db->insert("notification_map", $arr_notif_data);
        return $this->db->insert_id();
    }

    public function count_user_notif($user_id)
    {
        $sql = 'SELECT id FROM "notification" WHERE user_role=\'1,2,3,4\' AND ref_id =\''.$user_id.'\' ';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function get_head_all_notif($user_id, $notif_id)
    {
         $sql = 'SELECT id FROM "notification" WHERE user_role=\'1,2,3,4\' AND ref_id =\''.$user_id.'\' AND  id=\''.$notif_id.'\' ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

     public function get_head_all_notif_selected($user_id, $notif_id)
    {
         $sql = 'SELECT id FROM "notification" WHERE user_role<>\'1,2,3,4\' AND  id=\''.$notif_id.'\' ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_count_all_head($user_role_id)
    {
        $sql = 'SELECT id as notif_id, user_role FROM "notification" WHERE user_role LIKE \'%'.$user_role_id.'%\' ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function count_head_notif_all($notif_id)
    {
        $sql = 'SELECT id  FROM "notification" WHERE user_role=\'1,2,3,4\' AND id=\''.$notif_id.'\' ';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function get_head_notif_all($notif_id)
    {
         $sql = 'SELECT id  FROM "notification" WHERE user_role=\'1,2,3,4\' AND id=\''.$notif_id.'\' ';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function count_head_notif($notif_id)
    {
        $sql = 'SELECT id FROM "notification" WHERE user_role <> \'1,2,3,4\' AND id=\''.$notif_id.'\' ';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function count_user_notif_not_viewed($user_id)
    {
         $sql = 'SELECT n.id as notif_id, n.user_id
                FROM "notification" n
                WHERE n.user_role=\'1,2,3,4\' AND n.ref_id =\''.$user_id.'\'
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function count_head_notif_not_viewed($user_role_id)
    {
         $sql = 'SELECT n.id as notif_id, n.user_id
                FROM "notification" n
                WHERE n.user_role LIKE \'%'.$user_role_id.'%\' AND n.user_role <> \'1,2,3,4\' 
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_user_stock_offer($user_id)
    {
         $sql = 'SELECT esop_id
                FROM "user_stock_offer"
                WHERE user_id=\''.$user_id.'\' AND status=1
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function check_10_days_notif($esop_id)
    {
        $sql = 'SELECT n.action_note, n.date_created, n.user_id, n.id as notif_id, e.name as esop_name, e.offer_expiration, n.ref_id, n.user_role
                FROM "notification" n
                LEFT JOIN "esop" e ON e.id=n.ref_id 
                WHERE action_note=\'Offer Expiration Set\' AND n.ref_id =\''.$esop_id.'\'
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

   

    public function check_count_parent_notif($notif_id, $user_id)
    {
        $sql = 'SELECT id 
                FROM "notification"
                WHERE action_note=\'10 Days Remaining\' AND parent_id=\''.$notif_id.'\' AND ref_id=\''.$user_id.'\'
                ';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function save_remaining_days_notif($arr_build_10_remaining)
    {
        $this->db->insert("notification", $arr_build_10_remaining);
        return $this->db->insert_id();
    }

    public function check_count_expiration_parent_notif($notif_id, $user_id)
    {
        $sql = 'SELECT id 
                FROM "notification"
                WHERE action_note=\'Offer has expired\' AND parent_id=\''.$notif_id.'\' AND ref_id=\''.$user_id.'\'
                ';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function save_expiration_date_notif($arr_build_expiration)
    {
        $this->db->insert("notification", $arr_build_expiration);
        return $this->db->insert_id();
    }



    


    

} 