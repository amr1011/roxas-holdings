
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" href="assets/ico/favicon.ico">

        <title>Roxas Holings Login</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">

        <!-- Core CSS -->
    
    
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/global/commons.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/global/ui-widgets.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/global/login.css" rel="stylesheet">
    </head>
    <body>
        <section class="login-parent">


           <!--reset password-->
        <div class="modal-container showed" id="reset-password-modal">
            <div class="modal-body small bg-white">
                <!-- content -->
                <form id="confirm_form">
                <div class="modal-content text-center"> 
                        <div class="text-center">
                            <img class="w-logo" src="<?php echo base_url(); ?>/assets/images/roxas-holdings-logo.png" class="">
                        </div>
                        <p class="font-25 font-bold">ESOP Management System</p>
                        <input type="hidden" class="small add-border-radius-5px width-250px display-inline-mid" name="res_key" value="<?php echo $confirm_key;?>">
                        <div class="error change_password_error_message margin-bottom-10 hidden"></div>
                        <div class="success change_password_success_message margin-bottom-10 hidden"></div>
                        <div class="success margin-bottom-20 margin-top-20">You have successfully reset your password. Please indicate a new password to prevent anyone in accessing your account.</div>
                        <div class="margin-bottom-20 margin-top-20 text-left margin-left-20">
                            <p class="display-inline-mid padding-right-34">New Password: </p>
                            <input type="password" class="small add-border-radius-5px width-250px display-inline-mid" name="password" datavalid="required" labelinput="New Password">
                        </div>
                        <div class="margin-bottom-20 margin-top-20 text-left margin-left-20">
                            <p class="display-inline-mid padding-right-10">Confirm Password: </p>
                            <input type="password" class="small add-border-radius-5px width-250px display-inline-mid" name="passwordconf" datavalid="required" labelinput="Confirm Password">
                        </div>
                        <div class="margin-bottom-10 text-center">
                            <button class="btn-normal display-inline-mid margin-left-10 width-150px" id="confirm_reset">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--reset password-->

        <!--password complete-->
        <div class="modal-container" id="password-complete-modal">
            <div class="modal-body small bg-white">
                <!-- content -->
                <div class="modal-content width-90percent margin-auto text-center"> 
                    <div class="text-center">
                        <img class="w-logo" src="<?php echo base_url(); ?>/assets/images/roxas-holdings-logo.png" class="">
                    </div>
                    <p class="font-25 font-bold">ESOP Management System</p>
                    <div class="success margin-bottom-20 margin-top-20">You password has been reset. Click the button below to proceed.</div>
                    <div class="margin-bottom-10 text-center">
                        <button class="btn-normal display-inline-mid margin-left-10 modal-trigger width-150px close-me" id="btn_done">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!--password complete-->


        </section>

    </body>
</html>