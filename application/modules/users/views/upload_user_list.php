<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="ESOP-esop-list.php">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="ESOP-view-esop.php">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Share Distribution Template</a>
			</div> -->
			<div class="f-right">
				<!-- <a href="<?php echo base_url(); ?>users/user_list"> -->
					<button class="btn-normal margin-right-10" id="cancel_upload_btn">Cancel Upload</button>
				<!-- </a> -->
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="upload-user-csv">Upload Another Template</button>
				<!-- <a href="ESOP-view-esop2.php"> -->
					<button class="btn-normal" id="confirm_upload_btn" disabled="disabled">Confirm Upload</button>				
				<!-- </a> -->
			</div>
			<div class="clear"></div>
		</div>

		<div class="error-user-add insert_upload_list_error_message hidden margin-bottom-10 margin-top-10">
		</div>
		<div class="success-user-add insert_upload_list_success_message hidden margin-bottom-10 margin-top-10">
		</div>
	</div>
</section>

<section section-style="content-panel">
	
	<div class="content">
	
		<h2 class="f-left">Preview of <span data-label="upload_user_list_file_name"></span></h2>
		<div class="clear"></div>

		<div class="big-tbl-cont-user margin-top-10">
			<div class="tbl-rounded ">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>No.</th>
						<th>Employee Code</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Company Code</th>
						<th>Department Code</th>
						<th>Rank Code</th>
						<th>User Role</th>
						<th>User Name</th>
						<th>Password</th>
						<th>Contact Type</th>
						<th>Contact Number</th>
						<th>Email Address</th>
					</tr>
				</thead>
				<tbody data-container="upload_user_list_container">
					<tr class="upload_user_list template hidden">
						<td data-label="row">1</td>
						<td data-error="Employee Code" data-label="employee_code"></td>
						<td data-error="First Name" data-label="first_name"></td>
						<td data-error="Last Name" data-label="last_name"></td>
						<td data-error="Company Code" data-label="company_code"></td>
						<td data-error="Department Code" data-label="department_code"></td>
						<td data-error="Rank Code" data-label="rank_code"></td>
						<td data-error="User Role" data-label="user_role_name"></td>
						<td data-error="User Name" data-label="user_name"></td>
						<td data-error="Password" data-label="password"></td>
						<td data-error="Contact Type" data-label="contact_number_type_name"></td>
						<td data-error="Contact Number" data-label="contact_number"></td>
						<td data-error="Email" data-label="email"></td>		
					</tr>
					<!-- <tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
					</tr>			
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
					</tr> -->		
				</tbody>
			</table>
		</div>
		</div>

		<h2 class="margin-top-30">Error on the Document</h2>
		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Row Number</th>
						<th>Column Name</th>
						<th>Issue</th>						
					</tr>
				</thead>
				<tbody data-container="upload_user_list_error_container">
					<tr class="upload_user_list_error template hidden">
						<td data-label="row_error">1</td>
						<td data-label="row_name_error"></td>
						<td data-label="row_message_error"></td>
					</tr>
					<tr class="no_upload_user_list_error template hidden">
						<td colspan="3">No Errors Found.</td>
					</tr>
					<!-- <tr>
						<td>1</td>
						<td>Department Name, Employee Code</td>
						<td>Department Name does not match code</td>
					</tr> -->					
				</tbody>
			</table>
		</div>

	<div>
</section>

<div class="modal-container" modal-id="upload-user-csv">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD USER LIST</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<!-- <div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	 -->
			<div class="error upload_user_list_error_message hidden margin-bottom-15">
			</div>
			<div class="success upload_user_list_success_message hidden margin-bottom-15">
			</div>
			<div class="margin-top-5">
				<p class="display-inline-mid margin-right-30">Upload user list:</p>
				<p class="display-inline-mid margin-right-30" id="file_name"><i>No file uploaded yet</i></p>
				<a href="javascript:void(0)" class="display-inline-mid" id="trigger_upload_file">Upload File</a>
				<label><input type="file" name="upload_user_list" class="hidden"></label>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<!-- <a href="<?php echo base_url(); ?>esop/template_preview"> -->
				<button type="button" class="display-inline-mid btn-dark btn-normal" id="upload_list" disabled="true">Upload List</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>