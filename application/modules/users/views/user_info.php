<section section-style="top-panel">
	<div class="content"  data-container="user_info_container">
		
		<!-- <h1 class="f-left">View User</h1> -->
		<!-- <h2 class="f-left hidden">View User</h2> -->
		<div class="breadcrumbs margin-bottom-20 border-10px">
			<a href="<?php echo base_url(); ?>users/user_list">Users List</a>
			<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
			<a data-label="full_name"></a>
		</div>
		<a href="<?php echo base_url(); ?>users/edit_user">
			<button class="btn-normal f-right">Edit User</button>
		</a>
		<div class="clear"></div>

		<div class="user-profile">
			<!-- <div class=" display-inline-mid upload-view">
				<i class="fa fa-camera  white-color" ></i>
				<p class="white-color">Upload Photo</p>	
				
			</div> -->
			<div class="display-inline-mid upload-view position-rel width-150px height-150px" data-container="image_container">
				<i class="fa fa-camera  white-color" ></i>
				<p class="white-color">Upload Photo</p>	
				<img src="<?php echo base_url(); ?>assets/images/profile/default_user_image.jpg" class="width-100percent position-abs height-100percent top-0 left-0" style="border-radius:50%;">
			</div>

			<table class="display-inline-mid text-left white-color view-user-table">
				<tbody>
					<tr>
						<td colspan="2" class="font-20 width-200px" data-label="full_name">Joselito Salazar</td>
						<td colspan="2" ><strong>User Role: </strong> <span data-label="user_role_name"></span></td>
					</tr>
					<tr>
						<td><strong>Employee No:</strong></td>
						<td data-label="employee_code">1337</td>
						<td><strong>Username:</strong></td>
						<td data-label="user_name">joselito.salazar</td>
					</tr>
					<tr>
						<td><strong>Company:</strong></td>
						<td data-label="company_name">ROXOL</td>
						<td><strong>Password:</strong></td>
						<td><p class="bullets-like">*************</p></td>
					</tr>
					<tr>
						<td><strong>Department:</strong></td>
						<td data-label="department_name">ISD</td>
						<td><strong>Contact No.:</strong></td>
						<td data-label="contact_number">+639234567894</td>
					</tr>
					<tr>
						<td><strong>Rank:</strong></td>
						<td data-label="rank_name">ISD</td>
						<td><strong>Email Address:</strong></td>
						<td data-label="email">Joselito.Salazar@gmail.com</td>
					</tr>
				</tbody>
			</table>
			
		</div>
	</div>

</section>

<section section-style="content-panel">

	<div class="content">
		<div data-container="total_shares_container" class="">
			<div class="option-box divide-by-3">
				<p class="title">Total Shares Offered</p>
				<p class="description"><span data-label="total_shares_offered">0</span> Shares</p>
			</div>
			<div class="option-box divide-by-3">
				<p class="title">Total Shares Taken</p>
				<p class="description"><span data-label="total_shares_taken">0</span> Shares</p>
			</div>
			<div class="option-box divide-by-3">
				<p class="title">Total Shares Untaken</p>
				<p class="description"><span data-label="total_shares_untaken">0</span> Shares</p>
			</div>
		</div>

		<div data-container="esop_sort_container" class="">
			<div class="text-right-line margin-top-20 margin-bottom-70">
				<div class="line"></div>
				<div class="content-text" id="sort_personal_stocks">				
					<p class="font-15 white-color display-inline-mid">Sort By: <a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-esop-name">ESOP Name <i class="fa fa-chevron-down"></i></a></p>
					<span class="margin-left-10 margin-right-10 white-color">|</span>
					<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-esop-grant-date">Grant Date <i class="fa fa-chevron-down"></i></a></p>
				</div>
			</div>
		</div>
	
		<div data-container="personal_stocks_container" class="padding-top-40">
			<div class="no_results_found template hidden">
				<h2 class="text-center">NO ESOP Found</h2>
			</div>
			<div class="data-box divide-by-2 hidden template personal_stocks">

				<table class="width-100per">
					<tbody>					
						<tr>
							<td colspan="2"><h3 class="font-bold black-color" data-label="esop_name">ESOP 1</h3></td>
							<td colspan="2" class="text-right font-bold">Granted <span data-label="grant_date">Sept. 10, 2015</span></td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
							<td><span data-label="total_share_qty">0</span> Shares</td>
							<td class="text-right">Total Value of Shares:</td>
							<td class="text-right"><span data-label="accepted">0</span></td>
						</tr>				
					</tbody>
				</table>
				<div class="data-hover text-center view_personal_stock">
					<button class="btn-normal">View ESOP</button>
				</div>
			</div>

			<!-- <div class="data-box divide-by-2 hidden">

				<table class="width-100per">
					<tbody>					
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">ESOP 2</h3></td>
							<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Total Value of Shares:</td>
							<td class="text-right">500,000 Php</td>
						</tr>				
					</tbody>
				</table>
				<div class="data-hover text-center">
					<button class="btn-normal">View ESOP</button>
				</div>
			</div>

			<div class="data-box divide-by-2 hidden">

				<table class="width-100per">
					<tbody>					
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">ESOP 3</h3></td>
							<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Total Value of Shares:</td>
							<td class="text-right">500,000 Php</td>
						</tr>				
					</tbody>
				</table>
				<div class="data-hover text-center">
					<button class="btn-normal">View ESOP</button>
				</div>
			</div>

			<div class="data-box divide-by-2 hidden">

				<table class="width-100per">
					<tbody>					
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">E-ESOP</h3></td>
							<td colspan="2" class="text-right font-bold">Granted Sept. 10, 2015</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Total Share Quantity:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Total Value of Shares:</td>
							<td class="text-right">500,000 Php</td>
						</tr>				
					</tbody>
				</table>
				<div class="data-hover text-center">
					<button class="btn-normal">View ESOP</button>
				</div>
			</div> -->
		</div>

		
		<!-- place accordion here  -->
		<div data-container="audit_logs" class="hidden">
			<div class="panel-group text-left margin-top-30">
				<div class="accordion_custom">
					<div class="panel-heading border-10px">
						<a href="#">
							<h4 class="panel-title white-color">							
								Audit Logs
								<i class="change-font fa fa-caret-right font-left"></i>
								<i class="fa fa-caret-down font-right"></i>							
							</h4>
						</a>																	
						<div class="clear"></div>					
					</div>					
					<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
						<div class="panel-body ">

							<table class="table-roxas">
								<thead>
									<tr>
										<th>Date of Activity</th>
										<th>User</th>
										<th>Activity Description</th>
									</tr>
								</thead>
								<tbody data-container="logs">
									<tr class="logs template hidden">
										<td data-label="date_created">September 10, 2015</td>
										<td data-label="created_by">ROXAS, PEDRO OLGADO</td>
										<td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
									</tr>
									<!-- <tr>
										<td>September 10, 2015</td>
										<td>VALENCIA, RENATO CRUZ</td>
										<td>Created Department  <span class="font-bold">"HR Department"</span></td>
									</tr> -->
								</tbody>
							</table>
						</div>			
					</div>
				</div>
			</div>
		</div>
	<div>
</section>