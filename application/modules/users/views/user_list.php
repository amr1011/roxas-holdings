<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">User List</h1>
			<div class="f-right">
				<a href="<?php echo base_url(); ?>users/download_user_list_template">
					<button class="btn-normal margin-top-20 margin-right-10" >Download User List Template</button>
				</a>
				<!-- <a href="<?php echo base_url(); ?>users/upload_user_list"> -->
					<button class="btn-normal margin-top-20 margin-right-10 modal-trigger" modal-target="upload-user-csv">Upload User List</button>
				<!-- </a> -->
				<a href="<?php echo base_url(); ?>users/add_user">
					<button class="btn-normal margin-top-20">Add User</button>
				</a>
			</div>
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid" id="search_user_dropdown">
						<select>
							<option value="Username">Username</option>
							<option value="Name">Name</option>
							<option value="Company">Company</option>
							<option value="User Role">User Role</option>
						</select>
					</div>
					<div class="display-inline-mid username">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px search_user_name_input"/>
						<button class="btn-normal display-inline-mid margin-left-10 search_user_btn">Search</button>
					</div>
					<div class="display-inline-mid name">
						<input type="text" class="search width-200px display-inline-mid margin-left-10 add-border-radius-5px search_user_full_name_input"/>
						<button class="btn-normal display-inline-mid margin-left-10 search_user_btn">Search</button>
					</div>
				</div>
			</div>

			<div class="display-inline-mid margin-left-10 company">
				<p class="white-color margin-bottom-5 ">Company</p>
				<div>
					<div class="select add-radius margin-right-10" id="company_dropdown">
						<select>
							<option value="roxol">ROXOL</option>
							<option value="capdi">CAPDI</option>
							<option value="caci">CACI</option>
						</select>
					</div>
					<div class="select add-radius margin-left-10" id="company_user_role_dropdown">
						<select>
							<option value="All">All</option>
							<option value="ESOP Admin">ESOP Admin</option>
							<option value="HR Head">HR Head</option>
							<option value="HR Department">HR Department</option>
							<option value="Employee">Employee</option>
						</select>
					</div>
					
					<button class="btn-normal display-inline-mid margin-left-10 search_user_btn">Search</button>
				</div>
			</div>

			<div class="display-inline-mid user-role margin-left-10">
				<p class="white-color margin-bottom-5 ">User Role
				</p>				
				<div class="select add-radius" id="user_role_search_dropdown">
					<select>
						<option value="ESOP Admin">ESOP Admin</option>
						<option value="HR Head">HR Head</option>
						<option value="HR Department">HR Department</option>
						<option value="Employee">Employee</option>
					</select>
				</div>
				<button class="btn-normal display-inline-mid margin-left-10 search_user_btn">Search</button>				
			</div>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text" id="sort_user">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-user-full-name">Employee Name <i class="fa fa-chevron-down"></i></a></p>
				<!-- <span class="margin-left-10 margin-right-10 white-color">|</span> -->
				<!-- <p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Company</a></p> -->
				<!-- <span class="margin-left-10 margin-right-10 white-color">|</span> -->
				<!-- <p class="font-15 white-color display-inline-mid"><a href="#" class="white-color">Username</a></p> -->
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-user-employee-code">Employee No. <i class="fa fa-chevron-down"></i></a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">

		<div class="grid-content" data-container="user_view_by_grid">

			<div class="data-box divide-by-2 user_grid template hidden">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user" data-container="image_container">
									<img src="<?php echo base_url(); ?>assets/images/profile/default_user_image.jpg" alt="user profile" data-label="user_image">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color" data-label="full_name">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. <span data-label="employee_code"></span></td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td data-label="user_name">DGonzagA</td>
							<td class="text-left">Company:</td>
							<td data-label="company_name" class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td data-label="user_role_name">HR Officer</td>
							<td class="text-left">Department:</td>
							<td data-label="department_name" class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="javascript:void(0)">
						<button class="btn-normal view_user_info">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box width-100per padding-20-30px no_results_found template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="text-center">No Results found</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- <div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div>

			<div class="data-box divide-by-2">

				<table class="width-100per">
					<tbody>
						<tr>
							<td rowspan="4">
								<div class="img-user">
									<img src="../assets/images/profile/profile.jpg" alt="user profile">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><h3 class="font-bold black-color">Derek Gonzaga</h3></td>
							<td colspan="2" class="text-right font-bold font-20">Emp No. 1248</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Username:</td>
							<td>DGonzagA</td>
							<td class="text-left">Company:</td>
							<td class="text-left">ROXOL</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">User Role:</td>
							<td>HR Officer</td>
							<td class="text-left">Department:</td>
							<td class="text-left">HR Department</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="SETTINGS-view-user.php">
						<button class="btn-normal">View User</button>
					</a>
				</div>
			</div> -->

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Employee Name</th>
						<th>Employee No.</th>
						<th>Username</th>
						<th>User Role</th>
						<th>Company</th>
						<th>Department</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody data-container="user_view_by_list">
					<tr class="user_list template hidden">
						<td data-label="full_name">Jose Protacio</td>
						<td data-label="employee_code">12345</td>
						<td data-label="user_name">JSalazar</td>
						<td data-label="user_role_name">HR Officer</td>
						<td data-label="company_name">ROXOL</td>
						<td data-label="department_name">HR Department</td>
						<td><a href="javascript:void(0)" class="view_user_info">View User</a></td>
					</tr>
					<tr class="no_results_found template hidden">
						<td colspan="7">No Results found</td>
					</tr>
					<!-- <tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr>
					<tr>
						<td>12345</td>
						<td>Jose Protacio</td>
						<td>JSalazar</td>
						<td>HR Officer</td>
						<td>ROXOL</td>
						<td>HR Department</td>
						<td><a href="SETTINGS-view-user.php">View User</a></td>
					</tr> -->
					<tr class="last-content">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
</section>

<div class="modal-container" modal-id="upload-user-csv">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD USER LIST</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<!-- <div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	 -->
			<div class="error upload_user_list_error_message hidden margin-bottom-15">
			</div>
			<div class="success upload_user_list_success_message hidden margin-bottom-15">
			</div>
			<div class="margin-top-5">
				<p class="display-inline-mid margin-right-30">Upload user list:</p>
				<p class="display-inline-mid margin-right-30" id="file_name"><i>No file uploaded yet</i></p>
				<a href="javascript:void(0)" class="display-inline-mid" id="trigger_upload_file">Upload File</a>
				<label><input type="file" name="upload_user_list" class="hidden"></label>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<!-- <a href="<?php echo base_url(); ?>esop/template_preview"> -->
				<button type="button" class="display-inline-mid btn-dark btn-normal" id="upload_list" disabled="true">Upload List</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>