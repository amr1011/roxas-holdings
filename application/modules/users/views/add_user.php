<section section-style="top-panel">
	<div class="content">
		
		<div>
			<h1 class="f-left">Add User</h1>
			<div class="f-right">
				<a href="<?php echo base_url(); ?>users/user_list">
					<button type="button" class="display-inline-mid btn-cancel color-cancel">Cancel</button>
				</a>
				
				<button type="button" class="display-inline-mid btn-normal margin-left-10" id="add_user_btn">Add User</button>			
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<form id="add_user_form">
			<div class="error-user-add add_user_error_message hidden margin-bottom-10">
				<p class="font-15 error_message">error</p>
			</div>
			<div class="success-user-add add_user_success_message hidden margin-bottom-10">
				<p class="font-15 success_message">success</p>
			</div>

			<div class="upload-photo-ver2 position-rel width-150px height-150px" data-container="image_container">
				<i class="fa fa-camera"></i>
				<p>Upload Photo</p>
				<img class="width-100percent position-abs height-100percent top-0 left-0 hidden" style="border-radius:50%;">
			</div>
			<label>
	            <input type="file" name="user_image" class="hidden" /> 
			</label>
			<!-- <div class="upload-photo upload-photo-modal position-rel width-150px height-150px" >
				<div class="width-100percent">
					<i class="fa fa-camera fa-3x"></i>
					<p class="margin-top-10 ">Upload Photo</p>
				</div>
				<img class="width-100percent position-abs height-100percent top-0 left-0">
			</div> -->

			<table class="add-user-table margin-top-50">
				<tbody>
					<tr>
						<td>EMPLOYEE CODE: <span class="red-color">*</span></td>
						<td><input type="text" class="normal width-300px add-border-radius-5px" name="employee_code" datavalid="required" maxlength="5" labelinput="Employee Code"/></td>
						<td>FIRST NAME: <span class="red-color">*</span></td>
						<td><input type="text" class="normal width-300px add-border-radius-5px capitalize" name="first_name" datavalid="required" labelinput="First Name"/></td>
					</tr>
					<tr>
						<td>COMPANY: <span class="red-color">*</span></td>
						<td>
							<div class="select width-300px add-radius" id="company_dropdown">
								<select>
									<option value="Select Company">Select Company</option>
									<!-- <option value="op2">Company B</option> -->
								</select>
							</div>
						</td>
						<td>MIDDLE NAME:</td>
						<td><input type="text" class="normal width-300px add-border-radius-5px capitalize" name="middle_name"/></td>
					</tr>
					<tr>
						<td>DEPARTMENT: <span class="red-color">*</span></td>
						<td>
							<div class="select width-300px add-radius" id="department_dropdown">
								<select>
									<option value=""></option>
									<!-- <option value="dept2">Department B</option> -->
								</select>
							</div>
						</td>
						<td>LAST NAME: <span class="red-color">*</span></td>
						<td><input type="text" class="normal width-300px add-border-radius-5px capitalize" name="last_name" datavalid="required" labelinput="Last Name"/></td>
					</tr>
					<tr>
						<td>RANK: <span class="red-color">*</span></td>
						<td>
							<div class="select width-300px add-radius" id="rank_dropdown">
								<select>
									<option value=""></option>
									<!-- <option value="rankb">Rank B</option> -->
								</select>
							</div>
						</td>
						<td>CONTACT NO: <span class="red-color">*</span></td>
						<td>
							<div class="select width-100px add-radius" id="contact_dropdown">
								<select>
									<option value="Mobile">Mobile</option>
									<option value="House">House</option>
									<option value="Company">Company</option>
								</select>
							</div>
							<input type="text" class="normal display-inline-mid width-197px add-border-radius-5px" name="contact_number" datavalid="required" labelinput="Contact Number"/>
						</td>
					</tr>
					<tr>
						<td>USER ROLE: <span class="red-color">*</span></td>
						<td>
							<div class="select width-300px add-radius" id="user_role_dropdown">
								<select>
									<option value="Employee">Employee</option>
									<option value="ESOP Admin">ESOP Admin</option>
									<option value="HR Head">HR Head</option>
									<option value="HR Department">HR Department</option>
								</select>
							</div>
						</td>
						<td>EMAIL ADDRESS: <span class="red-color">*</span></td>
						<td><input type="text" class="normal width-300px add-border-radius-5px" name="email"  datavalid="email" labelinput="Email Address"/>
							
						</td>
					</tr>

					<tr>
						<td>USER NAME: <span class="red-color">*</span></td>
						<td>
							<input type="text" class="normal width-300px add-border-radius-5px" name="user_name" datavalid="required" labelinput="User Name"/>
						</td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>PASSWORD: <span class="red-color">*</span></td>
						<td>
							<div class="password-input">
								<input type="password" class="normal width-300px add-border-radius-5px" name="password" datavalid="required" labelinput="Password"/>
								<i class="fa fa-eye default-cursor black-color show_password"></i>
							</div>
						</td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>CONFIRM PASSWORD: <span class="red-color">*</span></td>
						<td>
							<div class="password-input">
								<input type="password" class="normal width-300px add-border-radius-5px" name="confirm_password" datavalid="required" labelinput="Confirm Password"/>
								<i class="fa fa-eye default-cursor black-color show_confirm_password"></i>
							</div>
						</td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</form>
					
	<div>
</section>