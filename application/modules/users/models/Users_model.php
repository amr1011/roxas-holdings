<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

    // public function get_user_info_data()
    // {
    //     $sql = "SELECT 
    //             FROM
    //             WHERE
    //             ";
    // }

    public function get_user_data ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT 
                    u.id, u.first_name, u.middle_name, u.last_name, 
                    CONCAT(u.first_name, \' \', u.middle_name, \' \', u.last_name) as name, 
                    u.user_name, u.user_role, u.email, u.contact_number, u.contact_number_type, 
                    u.company_id, u.department_id, u.rank_id, u.employee_code, u.is_deleted, u.date_created, 
                    u.employment_status, u.img, c.name as company_name, d.name as department_name, r.name as rank_name, 
                    c.company_code as company_code, d.department_code as department_code, r.number as rank_code, 
                    ur.description as user_role_name, 
                    (SELECT CASE WHEN u.contact_number_type = 1 THEN \'Mobile\' WHEN u.contact_number_type = 2 THEN \'House\' WHEN u.contact_number_type = 3 THEN \'Company\' END) as contact_number_type_name 
                FROM "user" u 
                LEFT JOIN "rank" r ON u.rank_id = r.id 
                LEFT JOIN "department" d ON u.department_id = d.id 
                LEFT JOIN "company" c ON u.company_id = c.id 
                LEFT JOIN "user_role" ur ON u.user_role = ur.id 
                WHERE TRUE AND u.is_deleted = 0';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        if($where['operator'] == 'IN')
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                        }
                        else if($where['operator'] == 'LIKE')
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." '%".$where['value']."%' ";
                        }
                        else
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                        }
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }


            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'u.user_name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'c.company_name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'name')
            {
                $sql .= " AND (u.first_name LIKE '%".$qualifiers['keyword']."%' OR u.middle_name LIKE '%".$qualifiers['keyword']."%' OR u.last_name LIKE '%".$qualifiers['keyword']."%' 
                            OR CONCAT(u.first_name, ' ', u.middle_name, ' ', u.last_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.first_name, ' ', u.last_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.last_name, ' ',u.middle_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.last_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.last_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.last_name, ' ',u.first_name) LIKE '%".$qualifiers['keyword']."%'
                            OR CONCAT(u.middle_name, ' ',u.first_name, ' ',u.last_name) LIKE '%".$qualifiers['keyword']."%' ) ";
            }
            else if($qualifiers['search_field'] == 'ur.description')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
            
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "u.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY u.id,c.name,d.name,r.name,c.company_code,d.department_code,r.number,ur.description";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." OFFSET ".$qualifiers['offset']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function insert_user_information($user = array())
    {
        if(is_array($user) AND count($user) > 0)
        {
            $this->db->insert("user", $user);
            return $this->db->insert_id();
        }
    }

    public function update_user_information($qualifier = array(), $data = array())
    {
        if(count($qualifier) > 0 AND count($data) > 0)
        {
            $sql = "UPDATE user u SET ";
            foreach($data as $i => $update)
            {
                if(isset($i) AND strlen($i) > 0 AND isset($update) AND strlen($update) > 0 )
                {
                    $length = count($data) - 1;
                    $sql .= " ".$i." = '".$update."' ";

                    if($i != $length )
                    {
                        $sql .= ",";
                    }
                }
            }
            $sql = rtrim($sql, ",");
            $sql .= " WHERE 1 ";

            foreach($qualifier as $i => $where)
            {
                if(isset($where['field']) AND strlen($where['value']) > 0 AND isset($where['field']) AND strlen($where['value']) > 0  AND isset($where['operator']))
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";

                }
            }
        }

        $query = $this->db->query($sql);

        return $this->db->affected_rows();
    }

    public function check_existing_data($qualifiers = array(), $table = '')
    {
        if(count($qualifiers) > 0 AND strlen($table) >0)
        {
            $sql = 'SELECT
                    *
                    FROM "'.$table.'"
                    WHERE TRUE';

            if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
            {
                foreach($qualifiers['where'] as $key => $where)
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                }
            }

            $query = $this->db->query($sql);
            return $query->num_rows();
        }
        else
        {
            return 0;
        }
    }

    public function get_user_email($user_id)
    {
        $sql = 'SELECT email,  CONCAT(first_name, \' \', middle_name, \' \', last_name) as name
                FROM "user" WHERE id= '.$user_id.'';
        $query = $this->db->query($sql);

       return $query->result_array();
       


    }

    public function is_temp_pass_valid($rs)
    {
        $this->db->where('rs', $rs);
        $query = $this->db->get('user');
        
        if($query->num_rows() == 1)
        {
            return TRUE;
        }
        else
        {
          return FALSE;  
        } 
    }

}