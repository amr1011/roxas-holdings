<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('session');

        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }

        $this->load->model('Users/Users_model');

        $allowed = $this->check_user_role();

        if(!$allowed)
        {
            // header('Location: '.base_url().'esop');
            header('Location: '.base_url().'auth/forbidden_403');
        }
    }

    public function index ()
    {
        header('Location: '.base_url().'users/user_list');
    }

    public function user_profile ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/users.js' );
        $this->template->add_content( $this->load->view( 'user_profile', $data, TRUE ) );
        $this->template->draw();
    }

    public function user_list ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/users.js' );
        $this->template->add_content( $this->load->view( 'user_list', $data, TRUE ) );
        $this->template->draw();
    }
    
    public function add_user ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/users.js' );
        $this->template->add_content( $this->load->view( 'add_user', $data, TRUE ) );
        $this->template->draw();
    }
    
    public function user_info ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/users.js' );
        $this->template->add_content( $this->load->view( 'user_info', $data, TRUE ) );
        $this->template->draw();
    }

    public function edit_user ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/users.js' );
        $this->template->add_content( $this->load->view( 'edit_user', $data, TRUE ) );
        $this->template->draw();
    }

    public function upload_user_list ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/users.js' );
        $this->template->add_content( $this->load->view( 'upload_user_list', $data, TRUE ) );
        $this->template->draw();
    }

    public function get ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'u.id',
                    'name' => 'name',
                    'first_name' => 'u.first_name',
                    'middle_name' => 'u.middle_name',
                    'last_name' => 'u.last_name',
                    'user_name' => 'u.user_name',
                    'user_role' => 'ur.description',
                    'email' => 'u.email',
                    'contact_number' => 'u.contact_number',
                    'company_id' => 'u.company_id',
                    'department_id' => 'u.department_id',
                    'rank_id' => 'u.rank_id',
                    'employee_code' => 'u.employee_code',
                    'date_created' => 'u.date_created',
                    'is_deleted' => 'u.is_deleted',
                    'company_name' => 'c.company_name',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }
            
            $total_rows = 0;

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "u.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );
            $response['qualifiers'] = $qualifiers;
            $users = $this->Users_model->get_user_data($qualifiers);

            $total_rows = $users['total_rows'];
            unset($users['total_rows']);

            foreach ($users as $i => $user) {

                $users[$i]['user_stock_offers'] = array();

                if(isset($params['esop_id']) AND $params['esop_id'] > 0)
                {
                    $get_user_stock_offer_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "esop_id", 
                                "value" => $params['esop_id']
                            ),
                            array(
                                "field" => "u.id", 
                                "value" => $user['id']
                            )
                        ) 
                    );
                }
                else
                {
                    $get_user_stock_offer_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "u.id", 
                                "value" => $user['id']
                            )
                        ) 
                    );
                }

                $esop_controller = $this->load->module('esop/Esop');
                $user_stock_offers = $esop_controller->Esop_model->get_user_stock_offer_data($get_user_stock_offer_qualifiers);
                unset($user_stock_offers['total_rows']);

                if(count($user_stock_offers) > 0)
                {
                    $users[$i]['user_stock_offers'] = $user_stock_offers;
                }
            }


            if(count($users) > 0)
            {
                $response['total_rows'] = $total_rows;
                $response['data'] = $users;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function get_users_grouped_by_company ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'u.id',
                    'name' => 'name',
                    'first_name' => 'u.first_name',
                    'middle_name' => 'u.middle_name',
                    'last_name' => 'u.last_name',
                    'user_name' => 'u.user_name',
                    'user_role' => 'ur.description',
                    'email' => 'u.email',
                    'contact_number' => 'u.contact_number',
                    'company_id' => 'u.company_id',
                    'department_id' => 'u.department_id',
                    'rank_id' => 'u.rank_id',
                    'employee_code' => 'u.employee_code',
                    'date_created' => 'u.date_created',
                    'is_deleted' => 'u.is_deleted',
                    'company_name' => 'c.company_name',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }
            
            $total_rows = 0;

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "u.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );
            $response['qualifiers'] = $qualifiers;
            $users = $this->Users_model->get_user_data($qualifiers);

            $total_rows = $users['total_rows'];
            unset($users['total_rows']);

            $users_by_company = array();

            foreach ($users as $i => $user) {
                $users_by_company[$user['company_id']]['company_name'] = $user['company_name'];
                $users_by_company[$user['company_id']]['company_id'] = $user['company_id'];
                $users_by_company[$user['company_id']]['name'] = $user['company_name'];
                $users_by_company[$user['company_id']]['id'] = $user['company_id'];
                
                $user['user_stock_offers'] = array();
               
                $user_accepted = FALSE;

                if(isset($params['esop_id']) AND $params['esop_id'] > 0 AND !isset($params['esop_batch_parent_id']))
                {
                    $get_user_stock_offer_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "esop_id", 
                                "value" => $params['esop_id']
                            ),
                            array(
                                "field" => "u.id", 
                                "value" => $user['id']
                            )
                        ) 
                    );
                }
                else
                {
                    $get_user_stock_offer_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "u.id", 
                                "value" => $user['id']
                            )
                        ) 
                    );
                }

                $esop_controller = $this->load->module('esop/Esop');
                $user_stock_offers = $esop_controller->Esop_model->get_user_stock_offer_data($get_user_stock_offer_qualifiers);
                unset($user_stock_offers['total_rows']);

                if(count($user_stock_offers) > 0)
                {
                    foreach ($user_stock_offers as $key => $user_stock_offer) {
                        $users_by_company[$user['company_id']]['company_allotment'] = $user_stock_offer['company_allotment'];
                        $users_by_company[$user['company_id']]['offer_letter_id'] = $user_stock_offer['offer_letter_id'];
                        $users_by_company[$user['company_id']]['acceptance_letter_id'] = $user_stock_offer['acceptance_letter_id'];

                        if(isset($params['esop_batch_parent_id']))
                        {
                            $exploded = explode(',', $params['esop_batch_parent_id']);
                            if(in_array($user_stock_offer['esop_id'], $exploded))
                            {
                                if($user_stock_offer['status'] == 2 AND $user_stock_offer['accepted'] != null)
                                {
                                    $user_accepted = TRUE;
                                }
                            }
                        }
                    }

                    $user['user_stock_offers'] = $user_stock_offers;
                }
                
                $user['user_id'] = $user['id'];
                
                if(!isset($users_by_company[$user['company_id']]['employees']))
                {
                    $users_by_company[$user['company_id']]['employees'] = array();

                    if($user_accepted == FALSE)
                    {
                        $users_by_company[$user['company_id']]['employees'][] = $user;                
                    }
                }
                else
                {
                    if($user_accepted == FALSE)
                    {
                        $users_by_company[$user['company_id']]['employees'][] = $user;                
                    }
                }
            }

            foreach ($users_by_company as $key => $value) {
                if(count($value['employees']) == 0)
                {
                    unset($users_by_company[$key]);
                }
            }

            if(count($users_by_company) > 0)
            {
                $response['total_rows'] = $total_rows;
                $response['data'] = $users_by_company;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function add ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $user_information = $this->input->post();

        if(isset($user_information) && count($user_information) > 0)
        {
            /*validate first name*/
            if(isset($user_information['first_name']) AND strlen($user_information['first_name']) > 0)
            {
                if(strlen($user_information['first_name']) > 1)
                {
                    $user_information_input['first_name'] = utf8_encode(ucwords($user_information['first_name']));
                }
                else
                {
                    $response['message'][] = "Invalid first name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "First name is required.";
                $error++; 
            }

            /*validate middle name*/
            if(isset($user_information['middle_name']) AND strlen($user_information['middle_name']) > 0)
            {
                if(strlen($user_information['middle_name']) > 0)
                {
                    $user_information_input['middle_name'] = utf8_encode(ucwords($user_information['middle_name']));
                }
                else
                {
                    // $response['message'][] = "Invalid rank middle name or is too short";
                    // $error++;
                }
            }
            else
            {
                // $response['message'][] = "Middle name is required.";
                // $error++; 
            }

            /*validate last name*/
            if(isset($user_information['last_name']) AND strlen($user_information['last_name']) > 0)
            {
                if(strlen($user_information['last_name']) > 1)
                {
                    $user_information_input['last_name'] = utf8_encode(ucwords($user_information['last_name']));
                }
                else
                {
                    $response['message'][] = "Invalid last name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Last name is required.";
                $error++; 
            }

            /*validate epmployee code*/
            if(isset($user_information['employee_code']) AND strlen($user_information['employee_code']) > 0)
            {
                if(strlen($user_information['employee_code']) > 0)
                {
                    $user_information_input['employee_code'] = $user_information['employee_code'];
                }
                else
                {
                    $response['message'][] = "Invalid employee code or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Employee code is required.";
                $error++; 
            }

            /*validate company id*/
            if(isset($user_information['company_id']) AND strlen($user_information['company_id']) > 0)
            {
                if(strlen($user_information['company_id']) > 0 AND preg_match("/^[0-9]+$/", trim($user_information['company_id'])) )
                {
                    $user_information_input['company_id'] = $user_information['company_id'];
                }
                else
                {
                    $response['message'][] = "Invalid Company ID or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Company ID is required.";
                $error++; 
            }

            /*validate department id*/
            if(isset($user_information['department_id']) AND strlen($user_information['department_id']) > 0)
            {
                if(strlen($user_information['department_id']) > 0 AND preg_match("/^[0-9]+$/", trim($user_information['department_id'])) )
                {
                    $user_information_input['department_id'] = $user_information['department_id'];
                }
                else
                {
                    $response['message'][] = "Invalid Department ID or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Department ID is required.";
                $error++; 
            }

            /*validate rank id*/
            if(isset($user_information['rank_id']) AND strlen($user_information['rank_id']) > 0)
            {
                if(strlen($user_information['rank_id']) > 0 AND preg_match("/^[0-9]+$/", trim($user_information['rank_id'])) )
                {
                    $user_information_input['rank_id'] = $user_information['rank_id'];
                }
                else
                {
                    $response['message'][] = "Invalid Rank ID or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Rank ID is required.";
                $error++; 
            }

            /*validate user role*/
            if(isset($user_information['user_role']) AND strlen($user_information['user_role']) > 0)
            {
                if(strlen($user_information['user_role']) > 0 AND preg_match("/^[0-9]+$/", trim($user_information['user_role'])) )
                {
                    $user_information_input['user_role'] = $user_information['user_role'];
                }
                else
                {
                    $response['message'][] = "Invalid User Role or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "User Role is required.";
                $error++; 
            }

            /*validate user name*/
            if(isset($user_information['user_name']) AND strlen($user_information['user_name']) > 0)
            {
                if(strlen($user_information['user_name']) > 1)
                {
                    $user_information_input['user_name'] = $user_information['user_name'];
                }
                else
                {
                    $response['message'][] = "Invalid User name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "User name is required.";
                $error++; 
            }

            /*validate password*/
            if(isset($user_information['password']) AND strlen($user_information['password']) > 0)
            {
                if(strlen($user_information['password']) > 1)
                {
                    $this->load->library('password');
                    $user_information_input['password'] = $this->password->encrypt(xss_clean($user_information['password']));
                }
                else
                {
                    $response['message'][] = "Invalid password or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Password is required.";
                $error++; 
            }

            /*validate contact number*/
            if(isset($user_information['contact_number']) AND strlen($user_information['contact_number']) > 0)
            {
                if(strlen($user_information['contact_number']) > 0 AND preg_match("/^[0-9]+$/", trim($user_information['contact_number'])) )
                {
                    $user_information_input['contact_number'] = $user_information['contact_number'];
                }
                else
                {
                    $response['message'][] = "Invalid Contact number or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Contact number is required.";
                $error++; 
            }

            /*validate contact number type*/
            if(isset($user_information['contact_number_type']) AND strlen($user_information['contact_number_type']) > 0)
            {
                if(strlen($user_information['contact_number_type']) > 0 AND preg_match("/^[0-9]+$/", trim($user_information['contact_number_type'])) )
                {
                    $user_information_input['contact_number_type'] = $user_information['contact_number_type'];
                }
                else
                {
                    $response['message'][] = "Invalid Contact number type or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Contact number type is required.";
                $error++; 
            }

            /*validate email*/
            if(isset($user_information['email']) AND strlen($user_information['email']) > 0)
            {
                if(strlen($user_information['email']) > 1 AND preg_match( "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/", $user_information['email'] ) )
                {
                    $user_information_input['email'] = $user_information['email'];
                }
                else
                {
                    $response['message'][] = "Invalid Email or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Email is required.";
                $error++; 
            }

            $user_information_input['img'] = isset($user_information['img']) ? $user_information['img'] : '' ;
            $user_information_input['is_deleted'] = 0;
            $user_information_input['date_created'] = date('Y-m-d H:i:s');
            
            if($error == 0)
            {
                /*validate email if existing*/
                $check_existing_email_qualifiers = array(
                    'where' => array(
                        array(
                            'field' => 'email',
                            'operator' => '=',
                            'value' => "'".$user_information['email']."'",
                        )
                    )
                );
                $existing_email = $this->Users_model->check_existing_data($check_existing_email_qualifiers, 'user');

                if($existing_email > 0)
                {
                    $response['message'][] = "Email Address is already existing.";
                    $error++;
                }

                /*validate employee code if existing*/
                $check_existing_code_qualifiers = array(
                    'where' => array(
                        array(
                            'field' => 'employee_code',
                            'operator' => '=',
                            'value' => "'".$user_information['employee_code']."'",
                        )
                    )
                );
                $existing_employee_code = $this->Users_model->check_existing_data($check_existing_code_qualifiers, 'user');

                if($existing_employee_code > 0)
                {
                    $response['message'][] = "Employee Code is already existing.";
                    $error++;
                }

                /*validate user name if existing*/
                $check_existing_user_name_qualifiers = array(
                    'where' => array(
                        array(
                            'field' => 'user_name',
                            'operator' => '=',
                            'value' => "'".$user_information['user_name']."'",
                        )
                    )
                );
                $existing_user_name = $this->Users_model->check_existing_data($check_existing_user_name_qualifiers, 'user');

                if($existing_user_name > 0)
                {
                    $response['message'][] = "User name is already existing.";
                    $error++;
                }

                if($existing_email == 0 && $existing_employee_code == 0 && $existing_user_name == 0 && $error == 0)
                {
                    /*insert if all inputs are okay*/
                    $added_user_information_id = $this->Users_model->insert_user_information($user_information_input);

                    if($added_user_information_id > 0)
                    {
                        $response['message'][] = "User added successfully.";
                        $response['status'] = TRUE;
                        $response['data'] = $user_information_input;
                        $response['data']['id'] = $added_user_information_id;

                        $first_name = (isset($user_information_input['first_name']) ? $user_information_input['first_name'].' ' : '' );
                        $middle_name = (isset($user_information_input['middle_name']) ? $user_information_input['middle_name'].' ' : '' );
                        $last_name = (isset($user_information_input['last_name']) ? $user_information_input['last_name'] : '' );
                        /*for logs*/
                        $_POST['params'] = $response['data'];
                        $_POST['params']['name'] = ucwords($first_name.''.$middle_name.''.$last_name);
                        $_POST['status'] = TRUE;
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function edit ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $user_information = $this->input->post();

        if(isset($user_information) && count($user_information) > 0)
        {
            if(isset($user_information['data']) AND is_array($user_information['data']) AND count($user_information['data']) > 0 AND isset($user_information['id']) AND $user_information['id'] > 0)
            {
                foreach ( $user_information['data'][0] as $i => $update_data )
                {
                    if ( strlen( $i ) > 0 )
                    {
                        /*validate fields*/

                        /*validate first name*/
                        if ($i == 'first_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $user_information_input['first_name'] = utf8_encode(ucwords($update_data));
                                }
                                else
                                {
                                    $response['message'][] = "Invalid first name or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "First name is required";
                                $error ++;
                            }
                        }

                        /*validate middle name*/
                        if ($i == 'middle_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $user_information_input['middle_name'] = utf8_encode(ucwords($update_data));
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid first name or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "First name is required";
                                // $error ++;
                            }
                        }

                        /*validate last name*/
                        if ($i == 'last_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $user_information_input['last_name'] = utf8_encode(ucwords($update_data));
                                }
                                else
                                {
                                    $response['message'][] = "Invalid last name or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Last name is required";
                                $error ++;
                            }
                        }

                        /*validate epmployee code*/
                        if ($i == 'employee_code')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    /*validate employee code if existing*/
                                    $check_existing_code_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'employee_code',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            ),
                                            array(
                                                'field' => 'id',
                                                'operator' => '!=',
                                                'value' => "'".$user_information['id']."'",
                                            ),
                                        )
                                    );
                                    $existing_employee_code = $this->Users_model->check_existing_data($check_existing_code_qualifiers, 'user');

                                    if($existing_employee_code > 0)
                                    {
                                        $response['message'][] = "Employee Code is already existing.";
                                        $error++;
                                    }
                                    else
                                    {
                                        $user_information_input['employee_code'] = $update_data;
                                    }
                                    
                                }
                                else
                                {
                                    $response['message'][] = "Invalid Employee code or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Employee code is required";
                                $error ++;
                            }
                        }

                        /*validate company id*/
                        if ($i == 'company_id')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $user_information_input['company_id'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid Company ID or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Company ID is required";
                                $error ++;
                            }
                        }

                        /*validate company name for logs*/
                        if ($i == 'company_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $user_information_input['company_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Company ID or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Company ID is required";
                                // $error ++;
                            }
                        }

                        /*validate department id*/
                        if ($i == 'department_id')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $user_information_input['department_id'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid Department ID or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Department ID is required";
                                $error ++;
                            }
                        }

                        /*validate department name for logs*/
                        if ($i == 'department_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $user_information_input['department_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid department ID or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Department ID is required";
                                // $error ++;
                            }
                        }

                        /*validate rank id*/
                        if ($i == 'rank_id')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $user_information_input['rank_id'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid Rank ID or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Rank ID is required";
                                $error ++;
                            }
                        }

                        /*validate rank name for logs*/
                        if ($i == 'rank_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $user_information_input['rank_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid rank name or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Rank name is required";
                                // $error ++;
                            }
                        }

                        /*validate user role*/
                        if ($i == 'user_role')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $user_information_input['user_role'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid User Role or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "User Role is required";
                                $error ++;
                            }
                        }

                        /*validate user role name for logs*/
                        if ($i == 'user_role_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $user_information_input['user_role_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid user role name or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "User role name is required";
                                // $error ++;
                            }
                        }

                        /*validate user name*/
                        if ($i == 'user_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    /*validate user name if existing*/
                                    $check_existing_user_name_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'user_name',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            ),
                                            array(
                                                'field' => 'id',
                                                'operator' => '!=',
                                                'value' => "'".$user_information['id']."'",
                                            ),
                                        )
                                    );
                                    $existing_user_name = $this->Users_model->check_existing_data($check_existing_user_name_qualifiers, 'user');

                                    if($existing_user_name > 0)
                                    {
                                        $response['message'][] = "User name is already existing.";
                                        $error++;
                                    }
                                    else
                                    {
                                        $user_information_input['user_name'] = $update_data;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Invalid User name or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "User name is required";
                                $error ++;
                            }
                        }

                        /*validate password*/
                        if ($i == 'password')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $this->load->library('password');
                                    $user_information['data'][0]['password'] = $this->password->encrypt(xss_clean($update_data));
                                    $user_information_input['password'] = $this->password->encrypt(xss_clean($update_data));
                                }
                                else
                                {
                                    $response['message'][] = "Invalid password or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Password is required";
                                // $error ++;
                            }
                        }

                        /*validate contact number*/
                        if ($i == 'contact_number')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $user_information_input['contact_number'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid Contact number or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Contact number is required";
                                $error ++;
                            }
                        }

                        /*validate contact number type*/
                        if ($i == 'contact_number_type')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $user_information_input['contact_number_type'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid Contact number type or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Contact number type is required";
                                $error ++;
                            }
                        }

                        /*validate contact number type name for logs*/
                        if ($i == 'contact_number_type_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $user_information_input['contact_number_type_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid contact number type name or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Contact number type name is required";
                                // $error ++;
                            }
                        }

                        /*validate email*/
                        if ($i == 'email')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match( "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/", $update_data ))
                                {
                                    /*validate email if existing*/
                                    $check_existing_email_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'email',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            ),
                                            array(
                                                'field' => 'id',
                                                'operator' => '!=',
                                                'value' => "'".$user_information['id']."'",
                                            ),
                                        )
                                    );
                                    $existing_email = $this->Users_model->check_existing_data($check_existing_email_qualifiers, 'user');

                                    if($existing_email > 0)
                                    {
                                        $response['message'][] = "Email Address is already existing.";
                                        $error++;
                                    }
                                    else
                                    {
                                        $user_information_input['email'] = $update_data;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Invalid Email or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Email is required";
                                $error ++;
                            }
                        }

                        /*validate image for logs*/
                        if ($i == 'img')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $user_information_input['img'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid image or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Image is required";
                                // $error ++;
                            }
                        }
                    }
                }

                if($error == 0)
                {
                    /*update if all inputs are okay*/
                    if($error == 0)
                    {
                        $logs_from = $this->Users_model->get_user_data(
                                array(
                                    'where' => array(
                                        array(
                                            'field' => 'u.id',
                                            'operator' => '=',
                                            'value' => $user_information['id'],
                                        )
                                    )
                                )
                            );

                        if(isset($user_information['data'][0]['company_name'])) { unset($user_information['data'][0]['company_name']); }
                        if(isset($user_information['data'][0]['department_name'])) { unset($user_information['data'][0]['department_name']); }
                        if(isset($user_information['data'][0]['rank_name'])) { unset($user_information['data'][0]['rank_name']); }
                        if(isset($user_information['data'][0]['user_role_name'])) { unset($user_information['data'][0]['user_role_name']); }
                        if(isset($user_information['data'][0]['contact_number_type_name'])) { unset($user_information['data'][0]['contact_number_type_name']); }

                        $updated_user_information_id = $this->Users_model->update_user_information(
                            array(
                                array(
                                'field' => 'u.id',
                                'value' => $user_information['id'],
                                'operator' => '='
                                )
                            ), 
                            $user_information['data'][0]);

                        $password = isset($user_information_input['password']) ? $user_information_input['password'] : NULL ;
                        unset($user_information_input['password']);

                        if($updated_user_information_id > 0)
                        {
                            $response['message'][] = "User updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $user_information_input;
                            $response['data']['id'] = $user_information['id'];

                            /*for logs*/
                            $_POST['params']['id'] = $user_information['id'];
                            $_POST['params']['to'] = $response['data'];

                            if(isset($password))
                            {
                                $_POST['params']['to']['password'] = $password;
                            }

                            $_POST['params']['from'] = $logs_from[0];
                            $_POST['params']['diff'] = array_diff_assoc($_POST['params']['to'], $_POST['params']['from']);
                            $_POST['status'] = TRUE;
                        }
                        else
                        {
                            // $response['message'][] = "Nothing is changed.";
                            $response['message'][] = "User updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $user_information_input;
                            $response['data']['id'] = $user_information['id'];
                        }
                        $response['data']['affected_rows'] = $updated_user_information_id;
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function ajax_upload_image()
    {
        $return = array();

        $path = './assets/images/users';

        if(!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path,0777,TRUE);
        }
        // else
        // {
        //     echo json_encode(array('status'=>FALSE, 'message'=>'Error updating image, please try again later.', 'data'=>array()));
        //     exit();
        // }

        echo json_encode($this->_upload_image($_FILES));
    }

    private function _upload_image($file)
    {
        $_FILES = $file;

        $file_name = uniqid( TRUE );

        if ( ! isset( $_FILES['client_img']['name'] ) )
        {
            return array( 'status' => FALSE );
        }

        $file       = pathinfo( $_FILES['client_img']['name'] );
        $extensions = array( 'jpg', 'jpeg', 'gif', 'png' );
        // kprint($file); exit;
        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ) )
        {

            //get width and height of selected image
            list( $width, $height ) = getimagesize( $_FILES['client_img']['tmp_name'] );

            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.' . $file['extension'];

            if(move_uploaded_file($_FILES['client_img']['tmp_name'], './assets/images/users/' . $file_name))
            {
                $data['image_height'] = $height;
                $data['image_width'] = $width;
                $data['image_name'] = $file_name;
                $data['image_path'] = /*base_url() . */'/assets/images/users/' . $file_name;

                return array('status'=>TRUE, 'message'=>'Image file successfully updated!', 'data'=>$data);
            }
            else
            {
                return array('status'=>FALSE, 'message'=>'Error adding image file, please try again later.', 'data'=>array());
            }

        }
        else
        {
            return array('status'=>FALSE, 'message'=>'Invalid image file, please try again.', 'data'=>array());
        }
    }

    public function upload_user_list_csv ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $path = './assets/uploads/users/csv';

        if(!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path,0777,TRUE);
        }
        // else
        // {
        //     $response['message'][] = 'Error uploading file, please try again later.';
        //     echo json_encode($response);
        //     exit();
        // }

        $file_name = uniqid( TRUE );

        if ( ! isset( $_FILES['csv_file']['name'] ) )
        {
            echo json_encode($response);
            exit();
        }

        $file       = pathinfo( $_FILES['csv_file']['name'] );
        $extensions = array( 'csv'/*, 'xls', 'xlsx'*/ );

        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ) )
        {
            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.csv' /*. $file['extension']*/;

            if(move_uploaded_file($_FILES['csv_file']['tmp_name'], './assets/uploads/users/csv/' . $file_name))
            {
                $data['original_file_name'] = $_FILES['csv_file']['name'];
                $data['file_name'] = $file_name;
                $data['file_path'] = base_url() . '/assets/uploads/users/csv/' . $file_name;
             
                $csv_file = fopen($data['file_path'], 'r');
                while (!feof($csv_file) ) {
                    $user_list_info[] = fgetcsv($csv_file, 1024, ',');
                }
                fclose($csv_file);

                $user_list_info = array_filter($user_list_info);

                /*check if array has more than 2 columns. the first column / $user_list_info[0] is the header, the second column / $user_list_info[1] is false if it has no other data*/
                if(count($user_list_info) > 1 AND isset($user_list_info[1]) AND $user_list_info[1] != FALSE)
                {
                    // if(
                    //     (isset($user_list_info[3]) AND isset($user_list_info[3][0]) AND strtolower($user_list_info[3][0]) == strtolower("NOTES:"))
                    //     || (isset($user_list_info[4]) AND isset($user_list_info[4][0]) AND strtolower($user_list_info[4][0]) == strtolower("- User Role should be Esop Admin or HR Head or HR Department or Employee only."))
                    //     || (isset($user_list_info[5]) AND isset($user_list_info[5][0]) AND strtolower($user_list_info[5][0]) == strtolower("- Contact Type should be Mobile or House or Company only."))
                    //     || (isset($user_list_info[6]) AND isset($user_list_info[6][0]) AND strtolower($user_list_info[6][0]) == strtolower("- Company Code, Department Code and Rank Code should match the system's codes"))
                    //     || (isset($user_list_info[7]) AND isset($user_list_info[7][0]) AND strtolower($user_list_info[7][0]) == strtolower("- DELETE this note when uploading the file"))
                    // )
                    // {
                    //     unlink('./assets/uploads/users/csv/' . $file_name);
                    //     $response['message'][] = 'File uploaded has experienced some error in the content. Please remove the notes in the content.';
                    // }
                    // else
                    // {
                        $response['status'] = TRUE;
                        $response['data'] = $data;
                        $response['message'][] = 'File successfully uploaded!';
                    // }
                }
                else
                {
                    unlink('./assets/uploads/users/csv/' . $file_name);
                    $response['message'][] = 'File uploaded has no content. Please upload file with content.';
                }
            }
            else
            {
                $response['message'][] = 'Error adding file, please try again later.';
            }
        }
        else
        {
            $response['message'][] = 'Invalid file, please try again.';
        }
        
        echo json_encode($response);
    }

    public function open_csv()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $user_list_info = array();

        $file = $this->input->post();

        if(isset($file['file_path']) && strlen($file['file_path']) > 0)
        {
            $file_path = str_replace(base_url(), '.', $file['file_path']);

            if(file_exists($file_path))
            {
                $csv_file = fopen($file['file_path'], 'r');
                while (!feof($csv_file) ) {
                    $user_list_info[] = fgetcsv($csv_file, 1024, ',');
                }
                fclose($csv_file);
        
                $user_list_info = array_filter($user_list_info);
                // array_walk($user_list_info, function(&$value, $key){ 
                //     $all_inputs_valid++;
                //     $value['valid'] = 0;
                //     $value['error_message']['Alloted Shares'] = "Shares have exceeded the Total Alloted Shares. Please re-distribute the shares accordingly"; 
                // });
                if(count($user_list_info) > 1 AND isset($user_list_info[1]) AND $user_list_info[1] != FALSE)
                {
                    // if(
                    //     (isset($user_list_info[3]) AND isset($user_list_info[3][0]) AND strtolower($user_list_info[3][0]) == strtolower("NOTES:""))
                    //     || (isset($user_list_info[4]) AND isset($user_list_info[4][0]) AND strtolower($user_list_info[4][0]) == strtolower("- User Role should be Esop Admin or HR Head or HR Department or Employee only."))
                    //     || (isset($user_list_info[5]) AND isset($user_list_info[5][0]) AND strtolower($user_list_info[5][0]) == strtolower("- Contact Type should be Mobile or House or Company only."))
                    //     || (isset($user_list_info[6]) AND isset($user_list_info[6][0]) AND strtolower($user_list_info[6][0]) == strtolower("- Company Code, Department Code and Rank Code should match the system's codes"))
                    //     || (isset($user_list_info[7]) AND isset($user_list_info[7][0]) AND strtolower($user_list_info[7][0]) == strtolower("- DELETE this note when uploading the file"))
                    // )
                    // {
                    //     $response['message'][] = 'File uploaded has experienced some error in the content. Please remove the notes in the content.';
                    // }
                    // else
                    // {
                        $validate = $this->validate_upload_user_list($user_list_info);
                        if(isset($validate['data']) AND count($validate['data']) > 0)
                        {
                            $response['status'] = TRUE;
                            $response['data'] = $validate['data'];
                        }
                    // }
                }
                else
                {
                    $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
                }
            }
            else
            {
                $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
            }
        }

        echo json_encode($response);
    }

    public function validate_upload_user_list($user_list_info = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $all_inputs_valid = 0;
        $user_list_upload_info = array();

        $emails = array();
        $user_names = array();
        $employee_codes = array();

        $companies_controller = $this->load->module('companies/Companies');
        $companies_get = $companies_controller->Companies_model->get_company_data(array("limit" => 999999));
        unset($companies_get['total_rows']);
        $companies = $companies_get;

        $departments_controller = $this->load->module('departments/Departments');
        $departments_get = $departments_controller->Departments_model->get_department_data(array("limit" => 999999));
        unset($departments_get['total_rows']);
        $departments = $departments_get;

        $ranks_controller = $this->load->module('ranks/Ranks');
        $ranks_get = $ranks_controller->Ranks_model->get_rank_data(array("limit" => 999999));
        unset($ranks_get['total_rows']);
        $ranks = $ranks_get;

        // print_r($companies);
        // print_r($departments);
        // print_r($ranks);
        // exit;
        $user_list_info = array_filter($user_list_info);

        if(isset($user_list_info) AND count($user_list_info) > 0)
        {
            foreach ($user_list_info as $key => $user_info) {

                if($key > 0)
                {
                    $error = 0;
                    $user_information_input = array(
                            "employee_code" => '',
                            "first_name" => '',
                            "last_name" => '',
                            "company_id" => '',
                            "department_id" => '',
                            "rank_id" => '',
                            "user_role" => '',
                            "user_name" => '',
                            "password" => '',
                            "contact_number_type" => '',
                            "contact_number" => '',
                            "email" => '',
                            "valid" => 0,
                            "error_message" => array()
                        );

                    foreach ($user_info as $i => $update_data) {
                        $filtered = '';
                        $match = array();
                        $update_data = utf8_encode($update_data);
                        /*validate epmployee code*/
                        if ($i == 0)
                        {
                            $user_information_input['employee_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    /*validate employee code if existing*/
                                    $check_existing_code_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'employee_code',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            )
                                        )
                                    );
                                    $existing_employee_code = $this->Users_model->check_existing_data($check_existing_code_qualifiers, 'user');

                                    if($existing_employee_code > 0 OR in_array($update_data, $employee_codes))
                                    {
                                        if(in_array($update_data, $employee_codes))
                                        {
                                            array_walk($user_list_upload_info, function(&$value, $key) use ($update_data) { 
                                                if($value['employee_code'] == $update_data)
                                                {
                                                    $value['error_message']['Employee Code'] = "Employee Code is already existing in the template";
                                                }
                                            });

                                            // $response['message'][] = "Employee Code is already existing in the template";
                                            $user_information_input['error_message']['Employee Code'] = "Employee Code is already existing in the template";
                                            $error++;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Employee Code is already existing";
                                            $user_information_input['error_message']['Employee Code'] = "Employee Code is already existing";
                                            $error++;
                                        }
                                    }
                                    else
                                    {
                                        array_push($employee_codes, $update_data);
                                        $user_information_input['employee_code'] = $update_data;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Employee code";
                                    $user_information_input['error_message']['Employee Code'] = "Invalid Employee code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Employee code is required";
                                $user_information_input['error_message']['Employee Code'] = "Employee code is required";
                                $error ++;
                            }
                        }

                        /*validate first name*/
                        if ($i == 1)
                        {
                            $user_information_input['first_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $user_information_input['first_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid first name";
                                    $user_information_input['error_message']['First Name'] = "Invalid first name";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "First name is required";
                                $user_information_input['error_message']['First Name'] = "First name is required";
                                $error ++;
                            }
                        }
                        
                        /*validate last name*/
                        if ($i == 2)
                        {
                            $user_information_input['last_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $user_information_input['last_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid last name";
                                    $user_information_input['error_message']['Last Name'] = "Invalid last name";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Last name is required";
                                $user_information_input['error_message']['Last Name'] = "Last name is required";
                                $error ++;
                            }
                        }

                        /*validate company id*/
                        if ($i == 3)
                        {
                            $user_information_input['company_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9\-]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($companies, function($filter_key) use ($update_data) {
                                        return $filter_key['company_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $company_id = $match['id'];
                                        $user_information_input['company_id'] = $company_id;
                                        $user_information_input['company_code'] = $update_data;
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Company code is not existing";
                                        $user_information_input['error_message']['Company Code'] = "Company code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Company code";
                                    $user_information_input['error_message']['Company Code'] = "Invalid Company code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Company code is required";
                                $user_information_input['error_message']['Company Code'] = "Company code is required";
                                $error ++;
                            }
                        }

                        /*validate department id*/
                        if ($i == 4)
                        {
                            $user_information_input['department_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9\-]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($departments, function($filter_key) use ($update_data) {
                                        return $filter_key['department_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }
  
                                        if($user_information_input['company_id'] == $match['company_id'])
                                        {
                                            $department_id = $match['id'];
                                            $user_information_input['department_id'] = $department_id;
                                            $user_information_input['department_code'] = $update_data;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Department code is not existing in the company provided";
                                            $user_information_input['error_message']['Department Code'] = "Department code is not existing in the company provided";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Department code is not existing";
                                        $user_information_input['error_message']['Department Code'] = "Department code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Department ID";
                                    $user_information_input['error_message']['Department Code'] = "Invalid Department ID";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Department ID is required";
                                $user_information_input['error_message']['Department Code'] = "Department ID is required";
                                $error ++;
                            }
                        }

                        /*validate rank id*/
                        if ($i == 5)
                        {
                            $user_information_input['rank_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9\-]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($ranks, function($filter_key) use ($update_data) {
                                        return $filter_key['number'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        if($user_information_input['department_id'] == $match['department_id'])
                                        {
                                            $rank_id = $match['id'];
                                            $user_information_input['rank_id'] = $rank_id;
                                            $user_information_input['rank_code'] = $update_data;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Rank code is not existing in the department provided";
                                            $user_information_input['error_message']['Rank Code'] = "Rank code is not existing in the department provided";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Rank code is not existing";
                                        $user_information_input['error_message']['Rank Code'] = "Rank code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Rank ID";
                                    $user_information_input['error_message']['Rank Code'] = "Invalid Rank ID";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Rank ID is required";
                                $user_information_input['error_message']['Rank Code'] = "Rank ID is required";
                                $error ++;
                            }
                        }

                        /*validate user role*/
                        if ($i == 6)
                        {
                            $user_information_input['user_role_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[a-zA-Z0-9 ]+$/", trim($update_data)))
                                {
                                    $user_roles = array(
                                            "esop admin" => 4,
                                            "hr head" => 3,
                                            "hr department" => 2,
                                            "employee" => 1
                                        );

                                    if(array_key_exists(strtolower($update_data), $user_roles))
                                    {
                                        $user_information_input['user_role'] = $user_roles[strtolower($update_data)];
                                        $user_information_input['user_role_name'] = $update_data;
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Invalid User Role";
                                        $user_information_input['error_message']['User Role'] = "Invalid User Role";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid User Role";
                                    $user_information_input['error_message']['User Role'] = "Invalid User Role";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "User Role is required";
                                $user_information_input['error_message']['User Role'] = "User Role is required";
                                $error ++;
                            }
                        }

                        /*validate user name*/
                        if ($i == 7)
                        {
                            $user_information_input['user_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    /*validate user name if existing*/
                                    $check_existing_user_name_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'user_name',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            )
                                        )
                                    );
                                    $existing_user_name = $this->Users_model->check_existing_data($check_existing_user_name_qualifiers, 'user');

                                    if($existing_user_name > 0 OR in_array($update_data, $user_names))
                                    {
                                        if(in_array($update_data, $user_names))
                                        {
                                            array_walk($user_list_upload_info, function(&$value, $key) use ($update_data) { 
                                                if($value['user_name'] == $update_data)
                                                {
                                                    $value['error_message']['User Name'] = "User name is already existing in the template";
                                                }
                                            });

                                            // $response['message'][] = "User name is already existing in the template";
                                            $user_information_input['error_message']['User Name'] = "User name is already existing in the template";
                                            $error++;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "User name is already existing";
                                            $user_information_input['error_message']['User Name'] = "User name is already existing";
                                            $error++;
                                        }
                                    }
                                    else
                                    {
                                        array_push($user_names, $update_data);
                                        $user_information_input['user_name'] = $update_data;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid User name";
                                    $user_information_input['error_message']['User Name'] = "Invalid User name";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "User name is required";
                                $user_information_input['error_message']['User Name'] = "User name is required";
                                $error ++;
                            }
                        }

                        /*validate password*/
                        if ($i == 8)
                        {
                            $user_information_input['password'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $this->load->library('password');
                                    $user_information_input['password_encrypted'] = $this->password->encrypt(xss_clean($update_data));
                                    $user_information_input['password'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid password";
                                    $user_information_input['error_message']['Password'] = "Invalid password";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Password is required";
                                $user_information_input['error_message']['Password'] = "Password is required";
                                $error ++;
                            }
                        }

                        /*validate contact number type*/
                        if ($i == 9)
                        {
                            $user_information_input['contact_number_type_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[a-zA-Z]+$/", trim($update_data)))
                                {
                                    $mobile_types = array(
                                            "mobile" => 1,
                                            "house" => 2,
                                            "company" => 3
                                        );

                                    if(array_key_exists(strtolower($update_data), $mobile_types))
                                    {
                                        $user_information_input['contact_number_type'] = $mobile_types[strtolower($update_data)];
                                        $user_information_input['contact_number_type_name'] = $update_data;
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Invalid Contact type";
                                        $user_information_input['error_message']['Contact Type'] = "Invalid Contact type";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Contact type";
                                    $user_information_input['error_message']['Contact Type'] = "Invalid Contact type";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Contact type is required";
                                $user_information_input['error_message']['Contact Type'] = "Contact type is required";
                                $error ++;
                            }
                        }

                        /*validate contact number*/
                        if ($i == 10)
                        {
                            $user_information_input['contact_number'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $user_information_input['contact_number'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Contact number";
                                    $user_information_input['error_message']['Contact Number'] = "Invalid Contact number";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Contact number is required";
                                $user_information_input['error_message']['Contact Number'] = "Contact number is required";
                                $error ++;
                            }
                        }

                        /*validate email*/
                        if ($i == 11)
                        {
                            $user_information_input['email'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match( "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/", $update_data ))
                                {
                                    /*validate email if existing*/
                                    $check_existing_email_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'email',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            )
                                        )
                                    );
                                    $existing_email = $this->Users_model->check_existing_data($check_existing_email_qualifiers, 'user');

                                    if($existing_email > 0 OR in_array($update_data, $emails))
                                    {
                                        if(in_array($update_data, $emails))
                                        {
                                            array_walk($user_list_upload_info, function(&$value, $key) use ($update_data) { 
                                                if($value['email'] == $update_data)
                                                {
                                                    $value['error_message']['Email'] = "Email Address is already existing in the template";
                                                }
                                            });

                                            // $response['message'][] = "Email Address is already existing in the template";
                                            $user_information_input['error_message']['Email'] = "Email Address is already existing in the template";
                                            $error++;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Email Address is already existing";
                                            $user_information_input['error_message']['Email'] = "Email Address is already existing";
                                            $error++;
                                        }
                                    }
                                    else
                                    {
                                        array_push($emails, $update_data);
                                        $user_information_input['email'] = $update_data;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Email";
                                    $user_information_input['error_message']['Email'] = "Invalid Email";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Email is required";
                                $user_information_input['error_message']['Email'] = "Email is required";
                                $error ++;
                            }
                        }

                    }
                    
                    if($error == 0)
                    {
                        $user_information_input['valid'] = 1;
                    }
                    else
                    {
                        $all_inputs_valid++;
                    }
                    
                    $user_list_upload_info[] = $user_information_input;
                }
            }

            // print_r($emails);
            // print_r($user_names);
            // print_r($employee_codes);
            // print_r($user_list_upload_info);
            // print_r($response);
            if($all_inputs_valid == 0)
            {
                $response['status'] = TRUE;
            }

            $response['data'] = $user_list_upload_info;
        }
        return $response;
    }

    public function insert_upload_user_list()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $params = $this->input->post();
        $user_list = $params;

        if(isset($user_list['data']) && count($user_list['data']) > 0)
        {
            $this->load->library('password');
            foreach ($user_list['data'] as $i => $user_information) {
                $user_information_input = array();
                $user_information_input['first_name'] = isset($user_information['first_name']) ? ucwords($user_information['first_name']) : '' ;
                $user_information_input['last_name'] = isset($user_information['last_name']) ? ucwords($user_information['last_name']) : '' ;
                $user_information_input['user_name'] = isset($user_information['user_name']) ? $user_information['user_name'] : '' ;
                $user_information_input['user_role'] = isset($user_information['user_role']) ? $user_information['user_role'] : '' ;
                $user_information_input['email'] = isset($user_information['email']) ? $user_information['email'] : '' ;
                $user_information_input['contact_number'] = isset($user_information['contact_number']) ? $user_information['contact_number'] : '' ;
                $user_information_input['contact_number_type'] = isset($user_information['contact_number_type']) ? $user_information['contact_number_type'] : '' ;
                $user_information_input['password'] = isset($user_information['password']) ? $this->password->encrypt(xss_clean($user_information['password'])) : '' ;
                $user_information_input['company_id'] = isset($user_information['company_id']) ? $user_information['company_id'] : '' ;
                $user_information_input['department_id'] = isset($user_information['department_id']) ? $user_information['department_id'] : '' ;
                $user_information_input['rank_id'] = isset($user_information['rank_id']) ? $user_information['rank_id'] : '' ;
                $user_information_input['employee_code'] = isset($user_information['employee_code']) ? $user_information['employee_code'] : '' ;
                $user_information_input['img'] = isset($user_information['img']) ? $user_information['img'] : '' ;
                $user_information_input['is_deleted'] = 0;
                $user_information_input['date_created'] = date('Y-m-d H:i:s');

                /*insert if all inputs are okay*/
                $added_user_information_id = $this->Users_model->insert_user_information($user_information_input);

                if($added_user_information_id > 0)
                {
                    $response['message'][] = "User added successfully.";
                    $response['status'] = TRUE;
                    $user_information_input['id'] = $added_user_information_id;
                    $response['data'][] = $user_information_input;

                    $first_name = (isset($user_information_input['first_name']) ? $user_information_input['first_name'].' ' : '' );
                    $middle_name = (isset($user_information_input['middle_name']) ? $user_information_input['middle_name'].' ' : '' );
                    $last_name = (isset($user_information_input['last_name']) ? $user_information_input['last_name'] : '' );
                    /*for logs*/
                    $added['params'] = $user_information_input;
                    $added['params']['id'] = $added_user_information_id;
                    $added['params']['name'] = ucwords($first_name.''.$middle_name.''.$last_name);
                    $_POST['status'] = TRUE;
                    $_POST['params']['added'][] = $added;
                }
            }
        }

        echo json_encode($response);
    }

    public function change_password()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $params = $this->input->post();

        if(isset($params) && count($params) > 0)
        {
            if(isset($params['data']) AND is_array($params['data']) AND count($params['data']) > 0 
                AND isset($params['id']) AND $params['id'] > 0 
                AND isset($params['current_password']) AND strlen($params['current_password']) > 0)
            {
                $get_user_qualifiers = array(
                        "where" => 
                            array(
                                array(
                                    "field" => 'u.id',
                                    "value" => $params['id']
                                )
                            )
                    );

                $auth_controller = $this->load->module('auth/Auth');
                $user_information = $auth_controller->Auth_model->get_user_data($get_user_qualifiers);


                if(isset($user_information) AND is_array($user_information) AND count($user_information) > 0)
                {
                    if(isset($user_information['is_deleted']) AND $user_information['is_deleted'] == 0)
                    {
                        $this->load->library("password");
                        if (isset($user_information['password']) && $this->password->match($params['current_password'], $user_information['password']))
                        {
                            unset($user_information['password']);

                            foreach ( $params['data'][0] as $i => $update_data )
                            {
                                if ( strlen( $i ) > 0 )
                                {
                                    /*validate password*/
                                    if ($i == 'password')
                                    {
                                        if(strlen($update_data) > 0)
                                        {
                                            if(strlen($update_data) > 1)
                                            {
                                                $this->load->library('password');
                                                $params['data'][0]['password'] = $this->password->encrypt(xss_clean($update_data));
                                            }
                                        }
                                    }
                                }
                            }

                            $updated_user_information_id = $this->Users_model->update_user_information(
                                array(
                                    array(
                                    'field' => 'u.id',
                                    'value' => $params['id'],
                                    'operator' => '='
                                    )
                                ), 
                                $params['data'][0]);

                            if($updated_user_information_id > 0)
                            {
                                $response['message'][] = "Password changed successfully.";
                                $response['status'] = TRUE;
                                $response['data'] = $user_information;
                            }
                            else
                            {
                                // $response['message'][] = "Nothing is changed.";
                                $response['message'][] = "Password changed successfully.";
                                $response['status'] = TRUE;
                                $response['data'] = $user_information;
                            }
                            $response['data']['affected_rows'] = $updated_user_information_id;

                        } else {
                            $response['message'][] = "Incorrect current password.";
                        }
                    }
                    else
                    {
                        $response['message'][] = "Unregistered user account already deactivated.";
                    }
                }
                else
                {
                    $response['message'][] = "Unregistered user account.";
                }
            }
            else
            {   
                $response['message'][] = "Params error.";
            }
        }
        echo json_encode($response);
    }

    public function download_user_list_template()
    {
        $file = './assets/templates/user_list_template.csv';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function delete_file()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();

        if(isset($params['file_path']))
        {
            if(strlen($params['file_path']) > 0)
            {
                $file_path = $params['file_path'];

                if(file_exists($file_path))
                {
                    unlink($file_path);

                    $response['status'] = TRUE;
                    $response['message'][] = 'File successfully forgotten.';
                }
            }
        }

        echo json_encode($response);
    }

    public function delete_all_except()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();

        if(isset($params['directory']) AND strlen($params['directory']) > 0)
        {

            $dir = $params['directory'];
            $leave_files = (isset($params['exception_files']) AND count($params['exception_files']) > 0) ? $params['exception_files'] : array() ;
            
            if(is_dir($dir))
            {
                $deleted_files = array();
                foreach( glob($dir."/*") as $file ) {
                    if( !in_array(basename($file), $leave_files) )
                    {
                        $deleted_files[] = $file;
                        unlink($file);
                    }
                }

                if(count($deleted_files) > 0)
                {
                    $response['status'] = TRUE;
                    $response['deleted_files'] = $deleted_files;
                }
            }
        }

        echo json_encode($response);
    }

}

/* End of file users.php */
/* Location: ./application/modules/users/controllers/users.php */