<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model {

    function get_gratuity($qualifiers = array())
    {
        $sql = 'SELECT g.grant_date as gratuity_date, e.name as gratuity_esop, g.price_per_share as gratuity_price, g.total_value_of_gratuity_given as gratuity_value 
                from gratuity g 
                LEFT JOIN esop e ON g.esop_id = e.id 
                WHERE TRUE';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                }
            }
        }

        if(isset($qualifiers['group_by']) && strlen($qualifiers['group_by']) > 0)
        {
            $sql .= " GROUP BY ".$qualifiers['group_by']." ";
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    function getEsopList(){

       /* $sql = "SELECT name
        FROM esop WHERE status = 1";*/
         // $sql = "SELECT e.name, e.id            
         //         FROM user_stock_offer uo   
         //         INNER JOIN esop e            
         //         ON uo.esop_id = e.id"; 
        $sql = "SELECT e.name, e.id            
                FROM esop e"; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }

     public function getShareSeparatedUnClaimUserWOV($esop_id, $company_id, $rank_id, $dept_id){

        $sql = ' SELECT (SUM(vr.value_of_shares) * (SELECT (vesting_years) - (date_part(\'year\',age(now(), grant_date))) FROM esop WHERE id=\''.$esop_id.'\')) as total_active
                 FROM "user_esop" ue
                 LEFT JOIN 
                    "user" u ON u.id = ue.user_id
                LEFT JOIN 
                    "vesting_rights" vr ON vr.esop_map_id = ue.id
                 LEFT JOIN
                    "user_claim" uc ON uc.user_id = ue.user_id AND uc.esop_id = ue.esop_id
                 WHERE uc.status <> \'2\' AND ue.esop_id = \''.$esop_id.'\' AND u.company_id = \''.$company_id.'\' AND u.department_id= \''.$dept_id.'\' AND u.rank_id = \''.$rank_id.'\' AND u.employment_status = \'2\' AND vr.status=\'0\' ';

            $query = $this->db->query($sql);
            return $query->result_array();
    }

    public function getShareActiveUnClaimUserWOV($esop_id, $company_id, $rank_id, $dept_id){

        $sql = ' SELECT (SUM(vr.value_of_shares) * (SELECT (vesting_years) - (date_part(\'year\',age(now(), grant_date))) FROM esop WHERE id=\''.$esop_id.'\')) as total_active
                 FROM "user_esop" ue
                 LEFT JOIN 
                    "user" u ON u.id = ue.user_id
                LEFT JOIN 
                    "vesting_rights" vr ON vr.esop_map_id = ue.id
                 LEFT JOIN
                    "user_claim" uc ON uc.user_id = ue.user_id AND uc.esop_id = ue.esop_id
                 WHERE uc.status <> \'2\' AND ue.esop_id = \''.$esop_id.'\' AND u.company_id = \''.$company_id.'\' AND u.department_id= \''.$dept_id.'\' AND u.rank_id = \''.$rank_id.'\' AND u.employment_status = \'1\' AND vr.status=\'0\' ';

            $query = $this->db->query($sql);
            return $query->result_array();
    }

    public function getShareSummary($esop_id,$company_id){

        $sql = 'SELECT
                r.name as rank_name,c.name as company_name,MAX(c.id) as company_id,MAX(r.id) as rank_id, MAX(d.id) as dept_id,SUM(ue.stock) as subscribed,MAX(e.vesting_years) as vesting_years
            FROM "user_esop" ue
            LEFT JOIN 
                "user" u ON u.id = ue.user_id
            LEFT JOIN 
                 "rank" r ON r.id = u.rank_id
            LEFT JOIN 
                "department" d ON d.id = u.department_id
            LEFT JOIN
                "company" c ON c.id = u.company_id
            LEFT JOIN
                "esop" e ON e.id = ue.esop_id
            WHERE ue.esop_id = \''.$esop_id.'\' AND u.company_id = \''.$company_id.'\'
            GROUP BY r.name, c.name, c.id';


            $query = $this->db->query($sql);
            return $query->result_array();

    }


    public function getShareActiveClaimUser($esop_id, $company_id, $rank_id, $dept_id){

        $sql = ' SELECT (SUM(vr.value_of_shares) * (SELECT (date_part(\'year\',age(now(), grant_date))) FROM esop WHERE id=\''.$esop_id.'\')) as total_active
                 FROM "user_esop" ue
                 LEFT JOIN 
                    "user" u ON u.id = ue.user_id
                LEFT JOIN 
                    "vesting_rights" vr ON vr.esop_map_id = ue.id
                 LEFT JOIN
                    "user_claim" uc ON uc.user_id = ue.user_id AND uc.esop_id = ue.esop_id
                 WHERE uc.status = \'2\' AND ue.esop_id = \''.$esop_id.'\' AND u.company_id = \''.$company_id.'\' AND u.department_id= \''.$dept_id.'\' AND u.rank_id = \''.$rank_id.'\' AND u.employment_status = \'1\' AND vr.status=\'1\' ';

            $query = $this->db->query($sql);
            return $query->result_array();
    }

    public function getShareSeparatedClaimUser($esop_id, $company_id, $rank_id, $dept_id){

        $sql = ' SELECT (SUM(vr.value_of_shares) * (SELECT (date_part(\'year\',age(now(), grant_date))) FROM esop WHERE id=\''.$esop_id.'\')) as total_active
                 FROM "user_esop" ue
                 LEFT JOIN 
                    "user" u ON u.id = ue.user_id
                LEFT JOIN 
                    "vesting_rights" vr ON vr.esop_map_id = ue.id
                 LEFT JOIN
                    "user_claim" uc ON uc.user_id = ue.user_id AND uc.esop_id = ue.esop_id
                 WHERE uc.status = \'2\' AND ue.esop_id = \''.$esop_id.'\' AND u.company_id = \''.$company_id.'\' AND u.department_id= \''.$dept_id.'\' AND u.rank_id = \''.$rank_id.'\' AND u.employment_status = \'2\' AND vr.status=\'1\' ';

            $query = $this->db->query($sql);
            return $query->result_array();
    }

    public function getShareActiveUnClaimUser($esop_id, $company_id, $rank_id, $dept_id){

        $sql = ' SELECT (SUM(vr.value_of_shares) * (SELECT (date_part(\'year\',age(now(), grant_date))) FROM esop WHERE id=\''.$esop_id.'\')) as total_active
                 FROM "user_esop" ue
                 LEFT JOIN 
                    "user" u ON u.id = ue.user_id
                LEFT JOIN 
                    "vesting_rights" vr ON vr.esop_map_id = ue.id
                 LEFT JOIN
                    "user_claim" uc ON uc.user_id = ue.user_id AND uc.esop_id = ue.esop_id
                 WHERE uc.status <> \'2\' AND ue.esop_id = \''.$esop_id.'\' AND u.company_id = \''.$company_id.'\' AND u.department_id= \''.$dept_id.'\' AND u.rank_id = \''.$rank_id.'\' AND u.employment_status = \'1\' AND vr.status=\'0\' ';

            $query = $this->db->query($sql);
            return $query->result_array();
    }


    public function getShareSeparatedUnClaimUser($esop_id, $company_id, $rank_id, $dept_id){

        $sql = ' SELECT (SUM(vr.value_of_shares) * (SELECT (date_part(\'year\',age(now(), grant_date))) FROM esop WHERE id=\''.$esop_id.'\')) as total_active
                 FROM "user_esop" ue
                 LEFT JOIN 
                    "user" u ON u.id = ue.user_id
                LEFT JOIN 
                    "vesting_rights" vr ON vr.esop_map_id = ue.id
                 LEFT JOIN
                    "user_claim" uc ON uc.user_id = ue.user_id AND uc.esop_id = ue.esop_id
                 WHERE uc.status <> \'2\' AND ue.esop_id = \''.$esop_id.'\' AND u.company_id = \''.$company_id.'\' AND u.department_id= \''.$dept_id.'\' AND u.rank_id = \''.$rank_id.'\' AND u.employment_status = \'2\' AND vr.status=\'0\' ';

            $query = $this->db->query($sql);
            return $query->result_array();
    }

    

    public function getSubscriptionSummary($id){

        $sql = 'SELECT (SELECT date_part(\'year\',age(now(), grant_date)) FROM esop WHERE id=\''.$id.'\') as e_grant_date, 
                e.id as esop_id,e.name as esop_name,r.name as rank_name, 
                SUM(ue.stock) as no_of_shares_subscribed, 
                (SUM(ue.stock) * e.price_per_share) as amount_subscribed, 
                (SUM(ue.stock) / 5) * (SELECT date_part(\'year\',age(now(), grant_date)) FROM esop WHERE id=\''.$id.'\') as no_of_shares_wvr, 
                ((SUM(ue.stock) / 5) * (SELECT date_part(\'year\',age(now(), grant_date)) FROM esop WHERE id=\''.$id.'\')) * (e.price_per_share) as amount_wvr, 
                (SUM(ue.stock)) - ((SUM(ue.stock) / 5) * (SELECT date_part(\'year\',age(now(), grant_date)) FROM esop WHERE id=\''.$id.'\')) as no_of_shares_wovr, 
                ((SUM(ue.stock) * e.price_per_share)) - (((SUM(ue.stock) / 5) * (SELECT date_part(\'year\',age(now(), grant_date)) FROM esop WHERE id=\''.$id.'\')) * (e.price_per_share)) as amount_wovr, e.vesting_years,e.price_per_share 
                FROM "user_esop" ue 
                LEFT JOIN "user" u ON ue.user_id = u.id 
                LEFT JOIN "rank" r ON u.rank_id = r.id 
                LEFT JOIN "vesting_rights" v ON ue.esop_id = v.esop_map_id 
                LEFT JOIN "esop" e ON ue.esop_id = e.id 
                WHERE e.id = \''.$id.'\' 
                GROUP By r.name,e.id';

                $query = $this->db->query($sql);
                return $query->result_array();
    }

    function getByEsopName($qualifiers = array()){


         $sql = 'SELECT e.name as esop_name,u.first_name AS first_name,sto.stock_amount AS total_share_offered,ue.stock AS total_shares_availed,(e.total_share_qty - ue.stock) AS total_share_unavailed 
                FROM "user_stock_offer" sto 
                LEFT JOIN "user" u ON sto.user_id = u.id 
                LEFT JOIN "user_esop" ue ON u.id = ue.user_id 
                LEFT JOIN "esop" e ON sto.esop_id = e.id 
                where TRUE';

            if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
            {
                foreach($qualifiers['where'] as $key => $where)
                {
                    if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                    }
                }
            }

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    

    function get_employee_payment($qualifiers = array())
    {
        $sql = 'SELECT upm.date_added as date_added, CONCAT(u.first_name,\' \',u.last_name) as name, e.name as esop_name, v.year_no as vesting_years, pt.name as payment_type, upm.amount_paid as payment_value 
            FROM "user_payment" upm 
            LEFT JOIN "user" u ON upm.user_id = u.id 
            LEFT JOIN "vesting_rights" v ON upm.vesting_rights_id = v.id 
            LEFT JOIN "payment_methods" pt ON upm.payment_type_id = pt.id 
            LEFT JOIN "user_esop" ue ON ue.id = v.esop_map_id 
            LEFT JOIN "esop" e ON ue.esop_id = e.id ';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                }
            }
        }

        if(isset($qualifiers['group_by']) && strlen($qualifiers['group_by']) > 0)
        {
            $sql .= " GROUP BY ".$qualifiers['group_by']." ";
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_esop_scoreboard($qualifiers = array())
    {
        $sql = 'SELECT * FROM 
                        (SELECT e.name as esopname, 
                                e.total_share_qty as esopquantity, 
                                e.price_per_share as price_per_share, 
                                e.vesting_years as vesting_years, 
                                (SELECT COUNT(uso.id) from user_stock_offer uso 
                                    WHERE uso.esop_id = e.id ) as employee_count, 
                                (SELECT SUM(ue.stock) from user_esop ue WHERE ue.esop_id = e.id) as total_share_accepted, 
                                (e.total_share_qty - ((SELECT SUM(ue.stock) 
                                from user_esop ue WHERE ue.esop_id = e.id))) as total_share_untaken 
                        FROM esop e) sb 
                        WHERE TRUE';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                }
            }
        }

        if(isset($qualifiers['group_by']) && strlen($qualifiers['group_by']) > 0)
        {
            $sql .= " GROUP BY ".$qualifiers['group_by']." ";
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

     public function get_payment_methods_data($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT pm.* 
                FROM payment_methods pm 
                WHERE TRUE';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "pm.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY pm.id";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }


    public function get_esop_data ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT 
                e.*,
                c.name as currency_name,
                CONCAT(u.first_name, \' \', u.middle_name, \' \', u.last_name) as created_by_name,
                (SELECT COUNT(uso.id) from user_stock_offer uso WHERE uso.esop_id = e.id ) as employee_count
            FROM "esop" e
            LEFT JOIN
                "currency" c ON e.currency = c.id
            LEFT JOIN
                "user" u ON e.created_by = u.id
            LEFT JOIN
                "user_stock_offer" uso ON e.id = uso.esop_id
            LEFT JOIN
                "user_esop" ue ON e.id = ue.esop_id
            LEFT JOIN
                "department_distribution" dd ON e.id = dd.esop_id
            LEFT JOIN
                "gratuity" g ON e.id = g.esop_id
            WHERE TRUE 
            AND e.is_deleted = 0';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'e.name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'e.price_per_share')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if ($qualifiers['search_field'] == 'e.grant_date')
            {
                $sql .= " AND DATE(".$qualifiers['search_field'].") BETWEEN ".$qualifiers['keyword']." ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "e.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY e.id,c.name,created_by_name";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['offset'].", ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_user_stock_offer_data ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT 
                uso.id as id,
                uso.stock_amount as employee_alloted_shares,
                uso.stock_amount as stock_amount,
                uso.esop_id as esop_id,
                uso.offer_letter_id as offer_letter_id,
                uso.acceptance_letter_id as acceptance_letter_id,
                uso.date_sent as date_sent,
                uso.status as status,
                c.id as company_id,
                c.name as company_name,
                d.name as department_name,
                r.name as rank_name,
                c.company_code as company_code,
                d.department_code as department_code,
                r.number as rank_code,
                ur.description as user_role_name,
                CONCAT(u.first_name, \' \', u.middle_name, \' \', u.last_name) as full_name,
                u.employee_code as employee_code,
                u.id as user_id,
                (SELECT allotment from offer_allotment oa WHERE u.company_id = oa.company_id AND uso.esop_id = oa.esop_id) as company_allotment,
                e.name as esop_name,
                e.grant_date as esop_grant_date,
                e.total_share_qty as esop_total_share_qty,
                e.currency as esop_currency,
                e.vesting_years as esop_vesting_years,
                e.price_per_share as esop_price_per_share,
                e.offer_expiration as esop_offer_expiration,
                e.created_by as esop_created_by,
                e.parent_id as esop_parent_id,
                (SELECT CONCAT(us.first_name, \' \', us.middle_name, \' \', us.last_name) from user us WHERE e.created_by = us.id) as esop_created_by_name,
                (SELECT rank_id from user us WHERE e.created_by = us.id) as esop_created_by_rank_id,
                (SELECT name from rank ra WHERE ra.id = (SELECT rank_id from user us WHERE e.created_by = us.id)) as esop_created_by_rank_name,
                (SELECT department_id from user us WHERE e.created_by = us.id) as esop_created_by_department_id,
                (SELECT name from department de WHERE de.id = (SELECT department_id from user us WHERE e.created_by = us.id)) as esop_created_by_department_name,
                (SELECT content from offer_letter ol WHERE uso.offer_letter_id = ol.id) as offer_letter_content,
                (SELECT content from offer_letter ol WHERE uso.acceptance_letter_id = ol.id) as acceptance_letter_content,
                cu.name as currency_name,
                cu.name as esop_currency_name,
                (SELECT stock from user_esop WHERE uso.user_id = user_id AND uso.esop_id = esop_id) as accepted

            FROM "user_stock_offer" uso
            LEFT JOIN 
                "esop" e ON e.id = uso.esop_id
            LEFT JOIN
                "user" u ON u.id = uso.user_id
            LEFT JOIN 
                "company" c ON u.company_id = c.id
            LEFT JOIN 
                "department" d ON u.department_id = d.id
            LEFT JOIN 
                "rank" r ON u.rank_id = r.id
            LEFT JOIN 
                "user_role" ur ON u.user_role = ur.id
            LEFT JOIN
                "currency" cu ON e.currency = cu.id
            WHERE TRUE ';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        if($where['operator'] == 'IN')
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                        }
                        else
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                        }
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'e.name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'e.price_per_share')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if ($qualifiers['search_field'] == 'e.grant_date')
            {
                $sql .= " AND DATE(".$qualifiers['search_field'].") BETWEEN ".$qualifiers['keyword']." ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "uso.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY uso.id,c.id,d.name,r.name,d.department_code,r.number,ur.description,full_name,u.employee_code,u.id,e.name,e.grant_date,e.total_share_qty,e.currency,e.vesting_years,e.price_per_share,e.offer_expiration,e.created_by,e.parent_id,cu.name";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['offset'].", ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_rank_data ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT 
                r.*,
                (SELECT COUNT(u.id) from user u WHERE u.rank_id = r.id ) as employee_count
            FROM "rank" r
            LEFT JOIN 
                "user" u ON u.rank_id = r.id
            LEFT JOIN 
                "department" d ON d.id = r.department_id
            LEFT JOIN
                "company" c ON d.company_id = c.id
            WHERE TRUE 
            AND r.is_deleted = 0 ';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'employee_count')
            {
                $sql .= " AND (SELECT COUNT(u.id) from user u WHERE u.rank_id = r.id) = '".$qualifiers['keyword']."' ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "r.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY r.id";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['offset'].", ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_rank_data_group_by_name ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT 
                r.*,
                (SELECT COUNT(u.id) from user u WHERE u.rank_id = r.id ) as employee_count
            FROM "rank" r
            LEFT JOIN 
                "user" u ON u.rank_id = r.id
            LEFT JOIN 
                "department" d ON d.id = r.department_id
            LEFT JOIN
                "company" c ON d.company_id = c.id
            WHERE TRUE 
            AND r.is_deleted = 0 ';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'employee_count')
            {
                $sql .= " AND (SELECT COUNT(u.id) from user u WHERE u.rank_id = r.id) = '".$qualifiers['keyword']."' ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "r.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY r.id";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['offset'].", ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }


    public function get_user_claimed_info($esop_id, $company_id)
    {
        $sql = 'SELECT ue.id as user_esop_id,ue.user_id, u.employee_code, u.first_name, u.middle_name, u.last_name, u.employment_status, d.name as department_name,
                r.name as rank_name, uc.status as claim_status
                FROM "user_esop" ue
                LEFT JOIN "user_claim" uc ON uc.user_id=ue.user_id AND uc.esop_id=ue.esop_id
                LEFT JOIN "user" u ON u.id=ue.user_id
                LEFT JOIN "company" c ON c.id=u.company_id
                LEFT JOIN "department" d ON d.id=u.department_id
                LEFT JOIN "rank" r ON r.id=u.rank_id
                WHERE u.company_id=\''.$company_id.'\' AND ue.esop_id=\''.$esop_id.'\' ORDER BY r.name ASC
                ';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_user_vesting_info($user_esop_id)
    {
        $sql = "SELECT year, no_of_shares
                FROM vesting_rights
                WHERE esop_map_id='{$user_esop_id}'
                ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    
}