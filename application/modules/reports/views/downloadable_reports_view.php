<section section-style="top-panel">
	<div class="content">
		<div class="breadcrumbs margin-bottom-20 border-10px">
			<a href="javascript:void(0)">Reports</a>
			<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
			<a href="ESOP-view-esop.php">Downloadable Reports</a>
		</div>

		<div class="header-effect display-none">

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
	
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30 ">

		<div class="grid-content">

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>Share Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="share-summary">Download Report</button>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>Subscription Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="subscription-summary">Download Report</button>
				</div>
			</div>
			
			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="esop-summary">Download Report</button>
				</div>
			</div>
			
			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>Claim Summary</h3></td>
						</tr>
				</table>
				<div class="text-center">
					<button class="btn-normal modal-trigger" modal-target="claim-summary">Download Report</button>
				</div>
			</div>

		</div>

		<div class="tbl-rounded margin-top-20 table-content display-none">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price per Share</th>
						<th>Vesting Years</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="ESOP-view-esop.php">View ESOP</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>

<!-- Share Summary -->
<div class="modal-container" modal-id="share-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Share Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error add_esop_error_message margin-bottom-10 display-none">
				<!-- <p class="font-15 error_message"> 
					Esop Name is required.
				</p> -->
			</div>
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>

						<td>
							<div class="select add-radius width-100percent margin-left-10">
								<select class="select-esop-name"></select>
							</div>
						</td>
						<!-- <td>
							<div class="display-inline-mid member-selection">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-esop-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid font-14 esop-search-input">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div sharesummary-esop-list-container">
										<div class="thumb_list_view small text-size-0 template esop-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
									</div>										
								</div>
							</div>
						</td> -->
					</tr>
					<tr>
						<td>
							<p>Filter by Company:</p>
						</td>

						<!-- <td>
							<div class="select add-radius width-100percent margin-left-10">
								<select class="select-company-name"></select>
							</div>
						</td> -->
						<td>

							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-company-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0 ">
										<input type="text" class="width-90percent display-inline-mid font-14 input-company-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div company-list-container">
										<div class="thumb_list_view small text-size-0 template company-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>
										</div>
									</div>										
								</div>
							</div>

						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-download-share-summary">Generate Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- Share Summary Table -->
<div class="share_summary_table" style="display: none;">
		
	<table border="1" bordercolor="#eee" class="esop">
        <thead class="font-10">
                <tr>
                    <th class="font-14" rowspan="3" width="150"><p class="text-center">Classification / Rank</p></th>
                    <th class="font-14" rowspan="3" width="150"><p class="text-center">Subscribed</p></th>
                    <th class="text-center font-14" colspan="9" width="650">W/ Vesting Rights</th>
                    <th class="text-center font-14" colspan="3" width="230">W/O Vesting Rights</th>                               
                </tr>   
                <tr>
                    <th class="text-center" colspan="3">Claimed / Purchased</th>
                    <th class="text-center" colspan="3">Unclaimed</th>
                    <th class="text-center" colspan="3">Total</th>
                </tr>                      
                <tr>
                    <th class="text-center bggray-darkred">Active</th>
                    <th class="text-center bggray-brightred">Separate</th>
                    <th class="text-center bggray-darkred">Sub-total</th>

                    <th class="text-center bggray-darkred">Active</th>
                    <th class="text-center bggray-brightred">Separate</th>
                    <th class="text-center bggray-darkred">Sub-total</th>

                    <th class="text-center bggray-darkred">Active</th>
                    <th class="text-center bggray-brightred">Separate</th>
                    <th class="text-center bggray-darkred">Sub-total</th>

                    <th class="text-center bggray-darkred">Active</th>
                    <th class="text-center bggray-brightred">Separate</th>
                    <th class="text-center bggray-darkred">Sub-total</th>
                </tr>
        </thead>

        <tbody class="font-14">
        	<tr class="" data-id="">
	            <td data-label-for="">1 Board</td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
	            <td data-label-for="" class="text-center"></td>
        	</tr>

        	<tr class="" data-id="">
	            <td data-label-for="">RHI & CADPI</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
        	</tr>

        	<tr class="" data-id="">
	            <td data-label-for="">CACI</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
        	</tr>

        	<tr class="" data-id="">
	            <td data-label-for="">ROXOL</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
	            <td data-label-for="" class="text-center">9999</td>
        	</tr>
		</tbody>
	</table>

	<button class="share_summary_populate">Pakshet</button>
</div>

<!-- Suscription Summary -->
<div class="modal-container" modal-id="subscription-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Subscription Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error sucscription_error_message margin-bottom-10 display-none">
				<!-- <p class="font-15 error_message"> 
					Esop Name is required.
				</p> -->
			</div>
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-esop-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid font-14 esop-search-input">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div subscription-esop-list-container">
										<div class="thumb_list_view small text-size-0 template esop-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Filter by Company:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-company-names"> </p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid font-14 input-company-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div company-list-container">
										<div class="thumb_list_view small text-size-0 template company-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<!-- <img class="img-responsive" src="assets/images/profile/aaron.png"> -->
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"> </p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
										
									</div>										
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-generate-subscription">Generate Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- ESOP Summary -->
<div class="modal-container" modal-id="esop-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">ESOP Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error add_esop_error_message margin-bottom-10 display-none">
				<!-- <p class="font-15 error_message"> 
					Esop Name is required.
				</p> -->
			</div>
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-100percent margin-left-10">
								<select class="select-esop-name"></select>
							</div>
						</td>
<!-- 						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-esop-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0 ">
										<input type="text" class="width-90percent display-inline-mid font-14 input-esop-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div esop-list-container">
										<div class="thumb_list_view small text-size-0 template esop-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>
										</div>
									</div>										
								</div>
							</div>
						</td> -->
					</tr>
					<tr>
						<td>
							<p>Company:</p>
						</td>
						<td>
							<div class="select add-radius width-100percent margin-left-10">
								<select class="select-company-name"></select>
							</div>
						</td>
						<!-- <td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-company-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0 ">
										<input type="text" class="width-90percent display-inline-mid font-14 input-company-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div company-list-container">
										<div class="thumb_list_view small text-size-0 template company-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>
										</div>
									</div>										
								</div>
							</div>
						</td> -->
					</tr>
					<tr>
						<td>
							<p>Department:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-department-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid font-14 input-department-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div department-list-container">
										<div class="thumb_list_view small text-size-0 template department-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Rank:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-rank-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid font-14 input-rank-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div rank-list-container">
										<div class="thumb_list_view small text-size-0 template rank-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
									</div>										
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-download-esop-summary">Download Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- Claim Summary -->
<div class="modal-container" modal-id="claim-summary">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Claim Summary</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="error add_esop_error_message margin-bottom-10 display-none">
				<!-- <p class="font-15 error_message"> 
					Esop Name is required.
				</p> -->
			</div>
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
						<div class="select add-radius width-100percent margin-left-10">
								<select class="select-esop-name"></select>
						</div>
						<!-- <td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-esop-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0 ">
										<input type="text" class="width-90percent display-inline-mid font-14 input-esop-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div esop-list-container">
										<div class="thumb_list_view small text-size-0 template esop-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>
										</div>
									</div>										
								</div>
							</div>
						</td> -->
						</td>
					</tr>
					<tr>
						<td>
							<p>Company:</p>
						</td>
						<td>
							<div class="select add-radius width-100percent margin-left-10">
								<select class="select-company-name"></select>
							</div>
						</td>
						<!-- <td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-company-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0 ">
										<input type="text" class="width-90percent display-inline-mid font-14 input-company-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div company-list-container">
										<div class="thumb_list_view small text-size-0 template company-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>
										</div>
									</div>										
								</div>
							</div>
						</td> -->
					</tr>
					<tr>
						<td>
							<p>Department:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-department-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid font-14 input-department-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div department-list-container">
										<div class="thumb_list_view small text-size-0 template department-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
									</div>										
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Rank:</p>
						</td>
						<td>
							<div class="display-inline-mid member-selection margin-left-10">
								<div class="for-selection">
									<div class="member-container display-inline-mid">
										<p class="display-selected-rank-names"></p>
										<div class="angle-down">
											<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="popup_person_list">
									<div class="display-inline-mid width-100percent text-left font-0">
										<input type="text" class="width-90percent display-inline-mid font-14 input-rank-name">
										<div class="sSearch display-inline-mid width-10percent">
											<i class="fa fa-search font-18" aria-hidden="true"></i>
										</div>
									</div>
									<div class="popup_person_list_div rank-list-container">
										<div class="thumb_list_view small text-size-0 template rank-item">
											<div class="thumb_list_inner display-inline-mid width-90percent">
												<div class="profile display-inline-mid">
													<p class="profile_name font-15 padding-left-10 name"></p>														
												</div>
											</div>
											<div class="check width-10percent">
												<i class="fa fa-check font-18" aria-hidden="true"></i>
											</div>

										</div>
									</div>										
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-download-esop-summary">Download Report</button>
		</div>
		<div class="clear"></div>
	</div>
</div>