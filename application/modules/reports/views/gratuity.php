<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Gratuity Report</h1>
			
			<div class="clear"></div>
		</div>
		
		<div>			
			<div class="display-inline-mid ">
				<p class="white-color margin-bottom-5">Please Indicate Date Range</p>
				<div>
					<label class="display-inline-mid ">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10 ">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" id="gratuity_from">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" id="gratuity_to">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<!--//<a href="#">-->
						<button class="btn-normal display-inline-mid margin-left-10 generate-gratuity">Generate Report</button>
					<!--</a>-->
				</div>
			</div>
		</div>

        <div class="header-effect margin-top-20 hidden" id="gratuity_search_params">

            <div class="display-inline-mid default">
                <p class="white-color margin-bottom-5">Search</p>
                <div>
                    <div class="select add-radius display-inline-mid gratselect">
                        <select>
                            <option id="ss" value="ESOP Name">ESOP Name</option>
                            <option value="Date Added">Date Added</option>
                            <option value="Share QTY">Price per Share</option>
                            <option value="Total Value">Total Value of Gratuity Given</option>
                        </select>
                    </div>
                    <div class="display-inline-mid search-me">
                        <input type="text" id="esop_name" name="esop_name" class="esop_name search normal display-inline-mid margin-left-10 add-border-radius-5px">
                        <button class="btn-normal display-inline-mid margin-left-10 esop_name_search">Search</button>
                    </div>
                    <div class="display-inline-mid vesting-years">
                        <input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px">
                        <button class="btn-normal display-inline-mid margin-left-10">Search</button>
                    </div>
                </div>
            </div>
           
            
            <div class="display-inline-mid date-added" >
                <p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
                <div>
                    <label class="display-inline-mid margin-left-20">From</label>
                    <div class="date-picker add-radius display-inline-mid margin-left-10">
                        <input type="text" data-date-format="MM/DD/YYYY" name="grant_from" id="grant_from" class="grant_from">
                        <span class="fa fa-calendar text-center"></span>
                    </div>
                    <label class="display-inline-mid margin-left-10">To</label>
                    <div class="date-picker add-radius display-inline-mid margin-left-10">
                        <input type="text" data-date-format="MM/DD/YYYY" name="grant_to" id="grant_to" class="grant_to">
                        <span class="fa fa-calendar text-center"></span>
                    </div>
                    <button class="btn-normal display-inline-mid margin-left-10 grant_date_search">Search</button>
                </div>
            </div>

            <div class="display-inline-mid price-share">
                <p class="white-color  margin-bottom-5 margin-left-20">Price per Share</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="price_share" name="price_share" class="price_share search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 price_search">Search</button>
            </div>

            <div class="display-inline-mid total-value">
                <p class="white-color  margin-bottom-5 margin-left-20">Total Value of Gratuity Given</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="total_value" name="total_value" class="total_value search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 total_value_search">Search</button>
            </div>

        </div>



    </div>
</section>

<section section-style="content-panel">

	<div class="content ">
        <div class="margin-bottom-30 margin-right-10">
            <i class="btn_export pull-right default-cursor margin-bottom-20 hidden" title="Export Excel File"><span class="fa fa-file-excel-o fa-2x hover-icon margin-right-10 text-center"></span></i>
        </div>
            
		<div class="text-right-line ">

			<div class="line"></div>
		</div>

		<div class="calendar-icon margin-top-80 empty">
			<i class="fa fa-calendar"></i>
			<h2 class="margin-top-20">Please Supply the Date Range to Generate the Report</h2>
		</div>

        <div class="tbl-rounded margin-top-20 hidden gratuity">
        
            <table id="gratuity" class="table-roxas tbl-display">
                <thead class="template">
                <tr>
                    <th>Gratuity Report</th>
                </tr>
                <tr>
                    <th>Generated By: <?php echo $this->session->userdata('first_name'); ?></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th>Date Added</th>
                    <th>ESOP Name</th>
                    <th>Price per Share</th>
                    <th>Total Value of Gratuity Given</th>
                </tr>
                </thead>
                <tbody data-container="gratuity" class="gratuity">
<!--                <tr>-->
<!--                    <td>September 10, 2015</td>-->
<!--                    <td>ESOP 1</td>-->
<!--                    <td>0.60 Php / Share</td>-->
<!--                    <td>1,300,215.15 Php</td>-->
<!--                </tr>-->
                <tr class="template gratuity">
                    <td data-label="gratuity_date">September 10, 2015</td>
                    <td data-label="gratuity_esop">StockOptionPlan 1</td>
                    <td data-label="gratuity_price">1.00 Php / Share</td>
                    <td data-label="gratuity_value">1,300,215.15 Php</td>
                </tr>

<!--                <tr class="last-content">-->
<!--                    <td colspan="4" class="text-right">-->
<!--                        <p class="display-inline-mid margin-right-10">Total</p>-->
<!--                        <p class="font-15 display-inline-mid margin-right-100">2,600,430.3 Php</p>-->
<!---->
<!--                    </td>-->
<!--                </tr>-->
                </tbody>
            </table>
        </div>
	</div>



</section>
