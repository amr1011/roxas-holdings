<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Employee Payment Report</h1>
			
			<div class="clear"></div>
		</div>
		
		<div>			
			<div class="display-inline-mid ">
				<p class="white-color margin-bottom-5">Please Indicate Date Range</p>
				<div>
					<label class="display-inline-mid ">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10 ">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" id="emplpayment_from">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" id="emplpayment_to">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<!--//<a href="#">-->
						<button class="btn-normal display-inline-mid margin-left-10 generate-emplpayment">Generate Report</button>
					<!--</a>-->
				</div>
			</div>
		</div>

        <div class="header-effect margin-top-20 hidden" id="emplpayment_search_params">

            <div class="display-inline-mid default">
                <p class="white-color margin-bottom-5">Search</p>
                <div>
                    <div class="select add-radius display-inline-mid emplselect">
                        <select>
                            <option id="ss" value="Employee Name">Employee Name</option>
                            <option value="Date Added">Date Added</option>
                            <option value="ESOP Name">ESOP Name</option>
                            <option value="Vesting Year">Vesting Years</option>
                            <option value="Payment Type">Payment Type</option>
                            <option value="Payment Value">Payment Value</option>
                        </select>
                    </div>
                    
                    <div class="display-inline-mid employee-name">
                        <input type="text" id="employee_name" name="employee_name" class="employee_name search normal display-inline-mid margin-left-10 add-border-radius-5px">
                        <button class="btn-normal display-inline-mid margin-left-10 employee_name_search">Search</button>
                    </div>
                </div>
            </div>

            <div class="display-inline-mid date-added" >
                <p class="white-color margin-bottom-5 margin-left-20">Date Added</p>
                <div>
                    <label class="display-inline-mid margin-left-20">From</label>
                    <div class="date-picker add-radius display-inline-mid margin-left-10">
                        <input type="text" data-date-format="MM/DD/YYYY" name="empl_from" id="empl_from" class="empl_from">
                        <span class="fa fa-calendar text-center"></span>
                    </div>
                    <label class="display-inline-mid margin-left-10">To</label>
                    <div class="date-picker add-radius display-inline-mid margin-left-10">
                        <input type="text" data-date-format="MM/DD/YYYY" name="empl_to" id="empl_to" class="empl_to">
                        <span class="fa fa-calendar text-center"></span>
                    </div>
                    <button class="btn-normal display-inline-mid margin-left-10 emplpayment_added_search">Search</button>
                </div>
            </div>

            <div class="display-inline-mid vesting-years">
                <p class="white-color  margin-bottom-5 margin-left-20">Vesting Years</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="vesting_years" name="vesting_years" class="vesting_years search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 vesting_years_search">Search</button>
            </div>

            <div class="display-inline-mid esop-name">
                <p class="white-color  margin-bottom-5 margin-left-20">ESOP Name</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="esop_name" name="esop_name" class="esop_name search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 esop_name_search">Search</button>
            </div>


            <div class="display-inline-mid payment-type">
                <p class="white-color  margin-bottom-5 margin-left-20">Payment Type</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <div class="select add-radius width-200px test" id="payment_method_dropdown" >
                    <select id="payment">
                        <option value="Select Payment" >Select Payment Method</option>
                        <!-- <option value="op2">Payment X</option>
                        <option value="op3">Payment k</option> -->               
                       
                    </select>
                </div>
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 payment_type_search">Search</button>
            </div>

            <div class="display-inline-mid payment-value">
                <p class="white-color  margin-bottom-5 margin-left-20">Payment Value</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="payment_value" name="payment_value" class="payment_value search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 payment_value_search">Search</button>
            </div>

        </div>



    </div>
</section>

<section section-style="content-panel">

	<div class="content ">
        <div class="margin-bottom-30 margin-right-10">
            <i class="btn_export pull-right default-cursor margin-bottom-20 hidden" title="Export Excel File"><span class="fa fa-file-excel-o fa-2x hover-icon margin-right-10 text-center"></span></i>
        </div>
            
		<div class="text-right-line ">

			<div class="line"></div>
		</div>

		<div class="calendar-icon margin-top-80 empty">
			<i class="fa fa-calendar"></i>
			<h2 class="margin-top-20">Please Supply the Date Range to Generate the Report</h2>
		</div>

        <div class="tbl-rounded margin-top-20 hidden emplpayment">
        
            <table id="emplpayment" class="table-roxas tbl-display">
                <thead class="template">
                <tr>
                    <th>Employee Payment Report</th>
                </tr>
                <tr>
                    <th>Generated By: <?php echo $this->session->userdata('first_name'); ?></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th>Date Added</th>
                    <th>Employee Name</th>
                    <th>ESOP Name</th>
                    <th>Vesting Year</th>
                    <th>Payment Type</th>
                    <th>Payment Value</th>
                </tr>
                </thead>
                <tbody data-container="emplpayment" class="emplpayment">
<!--                <tr>-->
<!--                    <td>September 10, 2015</td>-->
<!--                    <td>ESOP 1</td>-->
<!--                    <td>0.60 Php / Share</td>-->
<!--                    <td>1,300,215.15 Php</td>-->
<!--                </tr>-->
                <tr class="template emplpayment">
                    <td data-label="date_added">September 10, 2015</td>
                    <td data-label="name">StockOptionPlan 1</td>
                    <td data-label="esop_name">1.00 Php / Share</td>
                    <td data-label="vesting_years">1,300,215.15 Php</td>
                    <td data-label="payment_type">1,300,215.15 Php</td>
                    <td data-label="payment_value">1,300,215.15 Php</td>
                </tr>

<!--                <tr class="last-content">-->
<!--                    <td colspan="4" class="text-right">-->
<!--                        <p class="display-inline-mid margin-right-10">Total</p>-->
<!--                        <p class="font-15 display-inline-mid margin-right-100">2,600,430.3 Php</p>-->
<!---->
<!--                    </td>-->
<!--                </tr>-->
                </tbody>
            </table>
        </div>
	</div>



</section>
