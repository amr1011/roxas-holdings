<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP Scoreboard Report</h1>			
			<div class="clear"></div>

		</div>
		<!--  <p class=" white-color margin-bottom-10 margin-top-20">Search</p>
                <div class="select add-radius width-200px esopselect" id="esop_dropdown">
                    <select>                
                        <option value="">ESOP Name</option>
                    </select>
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 search-esop">Search</button> -->
        <div class="header-effect margin-top-20" id="esopscoreboard_search_params">

            <div class="display-inline-mid default">
                <p class="white-color margin-bottom-5">Search</p>
                <div>
                    <div class="select add-radius display-inline-mid gratselect">
                       <!-- <select>
                           <option id="ss" value="ESOP Name">ESOP Name</option>
                           <option value="Date Added">Date Added</option>
                           <option value="Share QTY">Price per Share</option>
                           <option value="Total Value">Total Value of Gratuity Given</option>
                       </select> -->

                        <select>
                            <option id="ss" value="ESOP Name">ESOP Name</option>
                            <option value="Total Share Quantity">Total Share Quantity</option>
                            <option value="Price per Share">Price per Share</option>
                            <option value="Vesting Years">Vesting Years</option>
                            <option value="No. of Employee Offered">No. of Employee Offered</option>
                            <option value="Total Shares Taken">Total Shares Taken</option>
                            <option value="Total Shares Untaken">Total Shares Untaken</option>
                        </select>
                    </div>
                    <div class="display-inline-mid search-me">
                        <input type="text" id="esop_name" name="esop_name" class="esop_name search normal display-inline-mid margin-left-10 add-border-radius-5px">
                        <button class="btn-normal display-inline-mid margin-left-10 search-esop">Search</button>
                    </div>
                    <div class="display-inline-mid vesting-years">
                        <input type="text" id="vesting_years" name="vesting_years" class="vesting_years search width-150px display-inline-mid margin-left-10 add-border-radius-5px">
                        <button class="btn-normal display-inline-mid margin-left-10 vesting_years_search">Search</button>
                    </div>
                </div>
            </div>
           
            
             <div class="display-inline-mid total-share-qty">
                <p class="white-color  margin-bottom-5 margin-left-20">Total Share Quantity</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="total_share_qty" name="total_share_qty" class="total_share_qty search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 total_share_qty_search">Search</button>
            </div>

            <div class="display-inline-mid no-of-employee">
                <p class="white-color  margin-bottom-5 margin-left-20">No. of Employee Offered</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="no_of_employee" name="no_of_employee" class="no_of_employee search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 no_of_employee_search">Search</button>
            </div>

            <div class="display-inline-mid price-share">
                <p class="white-color  margin-bottom-5 margin-left-20">Price per Share</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="price_share" name="price_share" class="price_share search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 price_search">Search</button>
            </div>

            <div class="display-inline-mid total-shares-taken">
                <p class="white-color  margin-bottom-5 margin-left-20">Total Shares Taken</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="total_shares_taken" name="total_shares_taken" class="total_shares_taken search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 total_shares_taken_search">Search</button>
            </div>

            <div class="display-inline-mid total-shares-untaken">
                <p class="white-color  margin-bottom-5 margin-left-20">Total Shares Untaken</p>

                <div class=" xsmall display-inline-mid margin-left-20">
                    <input type="text" id="total_shares_untaken" name="total_shares_untaken" class="total_shares_untaken search normal display-inline-mid margin-left-10 add-border-radius-5px">
                </div>
                <button class="btn-normal display-inline-mid margin-left-10 total_shares_untaken_search">Search</button>
            </div>

        </div>

	</div>
</section>

<section section-style="content-panel">
	<div class="content">

	<div class="margin-bottom-30 margin-right-10">
           
        </div>
		<div class="text-right-line ">
			<div class="line"></div>			
		</div>
		<div class="margin-top-50 f-right">
			<i class="btn_export fa fa-file-excel-o fa-2x hover-icon margin-right-10 default-cursor hidden" title="Export Excel File"></i>
		</div>
		<div class="clear"></div>
		<div class="tbl-rounded margin-top-20 hidden esop">
		
			<table id="esop" class="table-roxas tbl-display esop">
                <thead class="template">
                <tr>
                    <th>Esop Scoreboard Report</th>
                </tr>
                <tr>
                    <th>Generated By: <?php echo $this->session->userdata('first_name'); ?></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th>ESOP Name</th>
                    <th>Total Share Quantity</th>
                    <th>Price per Share</th>
                    <th>Vesting Years</th>
                    <th>No. of Employee Offered</th>
                    <th>Total Shares Taken</th>
                    <th>Total Shares Untaken</th>
                </tr>
                </thead>
                <tbody data-container="esop" class="esop">
<!--                <tr>-->
<!--                    <td>September 10, 2015</td>-->
<!--                    <td>ESOP 1</td>-->
<!--                    <td>0.60 Php / Share</td>-->
<!--                    <td>1,300,215.15 Php</td>-->
<!--                </tr>-->
                <tr class="template esop">
                    <td data-label="esopname">September 10, 2015</td>
                    <td data-label="esopquantity">StockOptionPlan 1</td>
                    <td data-label="price_per_share">1.00 Php / Share</td>
                    <td data-label="vesting_years">1,300,215.15 Php</td>
                    <td data-label="employee_count">1,300,215.15 Php</td>
                    <td data-label="total_share_accepted">1,300,215.15 Php</td>
                    <td data-label="total_share_untaken">1,300,215.15 Php</td>
                </tr>

<!--                <tr class="last-content">-->
<!--                    <td colspan="4" class="text-right">-->
<!--                        <p class="display-inline-mid margin-right-10">Total</p>-->
<!--                        <p class="font-15 display-inline-mid margin-right-100">2,600,430.3 Php</p>-->
<!---->
<!--                    </td>-->
<!--                </tr>-->
                </tbody>
            </table>
		</div>
		
		
	<div>
</section>