<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->library('excel');
        $this->load->model('Reports_model');

        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }

        $allowed = $this->check_user_role();

       /* if(!$allowed && $this->input->is_ajax_request() == false)
        {
            // header('Location: '.base_url().'esop');
            header('Location: '.base_url().'auth/forbidden_403');
        }*/
    }


    public function index ()
    {
        $user_role = $this->session->userdata('user_role');
        if($user_role == 1)
        {
            /*employee*/

            // header('Location: '.base_url().'esop');
        }
        else if($user_role == 2)
        {
            /*hr department*/

            header('Location: '.base_url().'reports/employee_payment_report');
        }
        else if($user_role == 3)
        {
            /*hr head*/

            header('Location: '.base_url().'reports/gratuity');
        }
        else if($user_role == 4)
        {
            /*esop admin*/

            header('Location: '.base_url().'reports/gratuity');
        }
    }

    public function gratuity ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/reports.js' );
        $this->template->add_script( assets_url() . '/js/plugins/jquery.dataTables.min.js' );
        $this->template->add_content( $this->load->view( 'gratuity', $data, TRUE ) );
        $this->template->draw();
    }

    public function employee_payment_report ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_script( assets_url() . '/js/libraries/employee_payment_report.js' );
        $this->template->add_content( $this->load->view( 'employee_payment_report', $data, TRUE ) );
        $this->template->draw();
    }
    
    public function employee_taken_share ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/employee_taken_share.js' );
        $this->template->add_content( $this->load->view( 'employee_taken_share', $data, TRUE ) );
        $this->template->draw();
    }

    public function esop_scoreboard ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/employee_taken_share.js' );
        $this->template->add_script( assets_url() . '/js/commons/main.js' );
        $this->template->add_script( assets_url() . '/js/libraries/esop_scoreboard.js' );
        
        $this->template->add_content( $this->load->view( 'esop_scoreboard', $data, TRUE ) );
        $this->template->draw();
    }

    public function custom_report ()
    {
        $data = array();
        $this->template->add_content( $this->load->view( 'custom_report', $data, TRUE ) );
        $this->template->draw();
    }

    public function downloadable_reports()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/downloadable_reports.js' );
        
        $this->template->add_content( $this->load->view( 'downloadable_reports_view', $data, TRUE ) );
        $this->template->draw();
    }

    public function get_gratuity($qualifiers = array())
    {
        if(isset($qualifiers) && count($qualifiers) > 0)
        {
            $qualifiers = $qualifiers;
        }
        else{
            $qualifiers = $this->input->post();
        }

        $gratuities = $this->Reports_model->get_gratuity($qualifiers);

        if(isset($qualifiers) && count($qualifiers) > 0)
        {
            return $gratuities;
        }
        else{
            echo json_encode($gratuities);
        }
    }

    public function get_employee_payment($qualifiers = array())
    {
        if(isset($qualifiers) && count($qualifiers) > 0)
        {
            $qualifiers = $qualifiers;
        }
        else{
            $qualifiers = $this->input->post();
        }

        $payments = $this->Reports_model->get_employee_payment($qualifiers);

        if(isset($qualifiers) && count($qualifiers) > 0)
        {
            return $payments;
        }
        else{
            echo json_encode($payments);
        }
    }

    public function get_esop_scoreboard($qualifiers = array())
    {
        if(isset($qualifiers) && count($qualifiers) > 0)
        {
            $qualifiers = $qualifiers;
        }
        else{
            $qualifiers = $this->input->post();
        }

        $payments = $this->Reports_model->get_esop_scoreboard($qualifiers);

        if(isset($qualifiers) && count($qualifiers) > 0)
        {
            return $payments;
        }
        else{
            echo json_encode($payments);
        }
    }

    public function get_payment_method()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $qualifiers = array(
            'where' => isset($params['where']) ? $params['where'] : array() ,
            'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
            'search_field' => isset($search_field) ? $search_field : '' ,
            'offset' => isset($params['offset']) ? $params['offset'] : 0,
            'limit' => isset($params['limit']) ? $params['limit'] : 30,
            'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "pm.id",
            'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
        );

        $qualifiers['where'] = array(
                    array(
                    'field' => 'pm.status',
                    'value' => '1'
                )
            );

        $payment_method = $this->Reports_model->get_payment_methods_data($qualifiers);
        unset($payment_method['total_rows']);
        if(count($payment_method) > 0)
        {
            $response['status'] = TRUE;
            $response['data'] = $payment_method;
        }

        echo json_encode($response);
    }

    public function generate_gratuity() {

        if($this->input->is_ajax_request())
        {
            $grants = array();
            $params = $this->input->post();
            if(count($params) > 0)
            {
                $qualifiers = array(
                    'where' => array(),
                    'limit' => 99999
                );

                if(isset($params['date_from']) && isset($params['date_to']))
                {

                    $qualifiers['where'][] = array(
                        'field' => "DATE("."g.grant_date".")",
                        'operator' => '>=',
                        'value' => "DATE('".date('Y-m-d' , strtotime($params['date_from']))."')"
                    );

                    $qualifiers['where'][] = array(
                        'field' => "DATE("."g.grant_date".")",
                        'operator' => '<=',
                        'value' => "DATE('".date('Y-m-d' , strtotime($params['date_to']))."')"
                    );

                    // print_r($qualifiers);
                    // exit();

                    $grants = $this->get_gratuity($qualifiers);

                    if(count($grants) > 0 && is_array($grants))
                    {
                        foreach($grants as $i => $g)
                        {
                            $grants[$i]['gratuity_date']  = date('F d, Y' , strtotime($grants[$i]['gratuity_date']));
                            $grants[$i]['gratuity_price'] = number_format($grants[$i]['gratuity_price'], 2);
                            $grants[$i]['gratuity_value'] = number_format($grants[$i]['gratuity_value'], 2);
                        }
                    }

                }
            }

            echo json_encode($grants);

        }

    }


    public function get_esop_list()
    {
        $this->load->model("Reports_model");
        $esop = array();

        $esop = $this->Reports_model->getEsopList();
       
        $response = array(
            "status" => true,
            "message" => "",
            "data" => $esop
        );

        header("Content-Type:application/json;");
        echo json_encode($response);

    }

    public function get_by_esop()
    {

       
        $this->load->model("Reports_model");
        $params = $this->input->post();
            
        $esop = array();

        $esop = $this->Reports_model->getByEsopName($params);
       
        $response = array(
            "status" => true,
            "message" => "",
            "data" => $esop
        );

        header("Content-Type:application/json;");
        echo json_encode($response);
    }
    public function generate_employee_payment() {

        if($this->input->is_ajax_request())
        {
            $grants = array();
            $params = $this->input->post();
            if(count($params) > 0)
            {
                $qualifiers = array(
                    'where' => array(),
                    'limit' => 99999
                );

                if(isset($params['date_from']) && isset($params['date_to']))
                {
                    $qualifiers['where'][] = array(
                        'field' => "DATE("."upm.date_added".")",
                        'operator' => '>=',
                        'value' => "DATE('".date('Y-m-d' , strtotime($params['date_from']))."')"
                    );

                    $qualifiers['where'][] = array(
                        'field' => "DATE("."upm.date_added".")",
                        'operator' => '<=',
                        'value' => "DATE('".date('Y-m-d' , strtotime($params['date_to']))."')"
                    );

                    $grants = $this->Reports_model->get_employee_payment($qualifiers);

                    if(count($grants) > 0 && is_array($grants))
                    {
                        foreach($grants as $i => $g)
                        {
                            $grants[$i]['date_added']  = date('F d, Y' , strtotime($grants[$i]['date_added']));
                            $grants[$i]['payment_value'] = number_format($grants[$i]['payment_value'], 2);
                        }
                    }

                }
            }

            echo json_encode($grants);

        }


    }

    public function generate_esop_scoreboard() {

        if($this->input->is_ajax_request())
        {
            $grants = array();
            $params = $this->input->post();
            if(count($params) > 0)
            {
                   

                $qualifiers = array(
                    'where' => $params['where'],
                    'limit' => 99999
                );


                   

                    $grants = $this->Reports_model->get_esop_scoreboard($qualifiers);

                    if(count($grants) > 0 && is_array($grants))
                    {
                        foreach($grants as $i => $g)
                        {
                            $grants[$i]['esopquantity'] = number_format($grants[$i]['esopquantity'], 2);
                            $grants[$i]['price_per_share'] = number_format($grants[$i]['price_per_share'], 2);
                            $grants[$i]['total_share_accepted'] = number_format($grants[$i]['total_share_accepted'], 2);
                            $grants[$i]['total_share_untaken'] = number_format($grants[$i]['total_share_untaken'], 2);
                        }
                    }
            }

            echo json_encode($grants);

        }

    }

    public function generate_share_summary()
    {
        if($this->input->is_ajax_request())
        { 
            $params = $this->input->post();

            if(count($params) > 0)
            {
                $return_assemble_data = $this->_assemble_share_summary_report($params);

                $tmp_folder = FCPATH . 'downloads/';
                $filename = "share_summary_report_" . strtotime(date("y-m-d H:i:s")) . ".xls";
                $file = base_url() . 'downloads/'.$filename;

                $return = excel_generate($return_assemble_data, $tmp_folder, $filename, $file);
            }
            

            echo json_encode($return);
        }
    }

    private function _assemble_share_summary_report($params)
    {

        $assemble_data = array();
        $esop_title = array();
        $data = array();
        $ectr = 1;

        $data['esop_id'] = json_decode($params['esop_id'], TRUE);
        $data['company_id'] = json_decode($params['company_id'], TRUE);

        //===== GET ESOP =====//
        foreach ($data['esop_id'] as $e) {
            $esop_qualifiers = array(
                "limit" => 1, 
                "where" => array(
                    array(
                        "id" => "e.id", 
                        "value" => $params['esop_id']
                    )
                ) 
            );
            $esop[] = $this->get_esop($esop_qualifiers);
        }
       
    
        //===== GET RANKS =====//
        foreach ($data['company_id'] as $c) {
            $rank_data_qualifiers = array(
                "where" => array(
                    array(
                        "field" => "c.id", 
                        "value" => $c['company_id']
                    )
                ) 
            );
            $rank_data = $this->Reports_model->get_rank_data_group_by_name($rank_data_qualifiers);
            $rank_list[$c['company_id']] = $rank_data;
        }

        $ranks_name = array();
        foreach($rank_list as $rk => $rv)
        {
            foreach ($rv as $rkv => $rvv) {
                if(isset($rvv['name']) AND !in_array($rvv['name'], $ranks_name))
                {
                    array_push($ranks_name, $rvv['name']);
                }      
            }
        }


        //===== GET ALL ESOP =====//
        foreach ($esop as $ed_key => $ed_val) {
            foreach ($ed_val['data'] as $key => $value) {
                array_push($esop_title, $value['name']);
            }
        }

        //===== HEADER =====//
        $assemble_data['bold_merge6_title'] = array('GROUPWIDE SUMMARY OF '.implode(",", $esop_title));
        $assemble_data['merge6_current_date'] = array(date('F j, Y'));
        $assemble_data['clear2'] = array('');
        $assemble_data['clear3'] = array('');
        $assemble_data['clear4'] = array('');

        if(count($esop) > 0)
        {

            foreach($esop as $ed_key => $ed_val) {

                foreach ($ed_val['data'] as $dkey => $dval) {


                            $assemble_data['ss_title_'.$ectr] = array($ectr.'. '.$dval['name'].' @ '.$dval['price_per_share'].' with '.$dval['vesting_years'].' years vesting rights (in terms of shares)');
                            $assemble_data['clear5'] = array(''); 
                            $assemble_data['clear6']  = array('');        
                            $assemble_data['ss_header_'.$ectr] = array('CLASSIFICATION/RANK','SUBSCRIBED','W/ VESTING RIGHTS','W/O VESTING RIGHTS');

                            $rctr = 1;
                            $assemble_data['clear7'] = array('');
                            foreach ($ranks_name as $rkey => $rval) {

                                if(isset($rval))
                                {
                                    $assemble_data['ss_body_'.$rctr] = array($rctr,$rval,'','','','','','','','','','','','','');

                                    if(count($dval['distribution']) > 0)
                                    {

                                        //===== GET DISTRIBUTION COMPANY DATA =====//
                                        foreach($dval['distribution'] as $dskey => $dsval)
                                        {

                                            //===== GET RANKS COMPANY DATA =====//
                                            foreach ($dsval['ranks'] as $drkey => $drval) {
                                               
                                                if(isset($drval['name']) AND in_array($drval['name'], $ranks_name))
                                                {
                                                    $assemble_data['company_ss_body_'.$rctr] = array('',$dsval['company_name'],'','','','','','','','','','','','','');
                                                }

                                            }

                                            $assemble_data['company_ss_body_subtotal'.$rctr] = array('','Sub-total','','','','','','','','','','','','','');

                                            $assemble_data['comapany_clear_'.$rctr] = array('');
                                        }

                                    }

                                }
                                
                                $rctr++;
                            }

                            $assemble_data['ss_body_grand_total_'.$rctr] = array($rctr,'GRAND TOTAL','','','','','','','','','','','','','');

                            $gtctr = 0;
                            foreach($dval['distribution'] as $dskey => $dsval)
                            {
                                $assemble_data['ss_body_'.$dsval['company_name'].'_'.$gtctr] = array('',$dsval['company_name'],'','','','','','','','','','','','','');
                                $gtctr++;
                            }

                            $ectr++;
                        }


                }
               
            
        }

        return $assemble_data;

    }

    public function get_esop($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'e.id',
                    'name' => 'e.name',
                    'grant_date' => 'e.grant_date',
                    'total_share_qty' => 'total_share_qty',
                    'price_per_share' => 'e.price_per_share',
                    'currency' => 'e.currency',
                    'vesting_years' => 'e.vesting_years',
                    'is_deleted' => 'e.is_deleted',
                    'created_by' => 'e.created_by',
                    'status' => 'e.status',
                    'parent_id' => 'e.parent_id',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $total_rows = 0;

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "e.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;
            $esops = $this->Reports_model->get_esop_data($qualifiers);
            $total_rows = $esops['total_rows'];
            unset($esops['total_rows']);


            if(count($esops) > 0)
            {
                $esop_data = array();
                foreach ($esops as $i => $esop) {
                    $total_shares_availed_esop = 0;
                    $total_shares_unavailed_esop = 0;
             
                    $get_user_stock_offer_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "uso.esop_id", 
                                "value" => $esop['id']
                            )
                        ) 
                    );

                    $user_stock_offers = $this->Reports_model->get_user_stock_offer_data($get_user_stock_offer_qualifiers);
                    unset($user_stock_offers['total_rows']);

                    $esop_data[$i] = $esop;
                    $esop_data[$i]['distribution'] = array();

                    $total_shares_availed_company = 0;
                    $total_shares_unavailed_company = 0;
                    foreach ($user_stock_offers as $key => $user_stock_offer) {
                        // $company_allotment = $this->Reports_model->get_company_allotment();

                        $esop_data[$i]['letter_sent'] = $user_stock_offer['status']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['id'] = $user_stock_offer['company_id']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['company_id'] = $user_stock_offer['company_id']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['company_name'] = $user_stock_offer['company_name']; 

                        $rank_data_qualifiers = array(
                            "where" => array(
                                array(
                                    "field" => "c.id", 
                                    "value" => $user_stock_offer['company_id']
                                )
                            ) 
                        );
                        $ranks_by_company = $this->Reports_model->get_rank_data($rank_data_qualifiers);
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['ranks'] = $ranks_by_company;

                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['employees'][] = $user_stock_offer; 


                        $total_shares_availed_company += $user_stock_offer['employee_alloted_shares'];
                        $total_shares_availed_esop += $user_stock_offer['employee_alloted_shares'];
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['total_shares_availed'] = $total_shares_availed_company;

                        $total_shares_unavailed_company = $user_stock_offer['company_allotment'] - $total_shares_availed_company;
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['total_shares_unavailed'] = $total_shares_unavailed_company; 

                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['total_alloted_shares'] = $user_stock_offer['company_allotment']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['company_allotment'] = $user_stock_offer['company_allotment'];
                    }

                    $esop_data[$i]['total_shares_availed'] = $total_shares_availed_esop;
                    $esop_data[$i]['total_shares_unavailed'] = $esop['total_share_qty'] - $total_shares_availed_esop;

                    $get_esop_batch_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "e.parent_id", 
                                "value" => $esop['id']
                            ),
                            array(
                                "field" => "e.is_special", 
                                "value" => 0
                            )
                        ) 
                    );

                    $esop_batches = $this->Reports_model->get_esop_data($get_esop_batch_qualifiers);
                    unset($esop_batches['total_rows']);

                    $esop_data[$i]['esop_batches'] = array();

                    if(isset($esop_batches) AND count($esop_batches) > 0)
                    {
                        $esop_data[$i]['esop_batches'] = $esop_batches;
                        foreach ($esop_batches as $x => $esop_batch) {
                            $total_shares_availed_esop = 0;
                            $total_shares_unavailed_esop = 0;

                            $get_user_stock_offer_qualifiers = array(
                                "limit" => 999999, 
                                "where" => array(
                                    array(
                                        "field" => "uso.esop_id", 
                                        "value" => $esop_batch['id']
                                    )
                                ) 
                            );

                            $user_stock_offers = $this->Reports_model->get_user_stock_offer_data($get_user_stock_offer_qualifiers);
                            unset($user_stock_offers['total_rows']);

                            $esop_data[$i]['esop_batches'][$x] = $esop_batch;
                            $esop_data[$i]['esop_batches'][$x]['distribution'] = array();

                            $total_shares_availed_company = 0;
                            $total_shares_unavailed_company = 0;
                            foreach ($user_stock_offers as $key => $user_stock_offer) {
                                $esop_data[$i]['esop_batches'][$x]['letter_sent'] = $user_stock_offer['status']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['id'] = $user_stock_offer['company_id']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['company_id'] = $user_stock_offer['company_id']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['company_name'] = $user_stock_offer['company_name']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['employees'][] = $user_stock_offer; 

                                $total_shares_availed_company += $user_stock_offer['employee_alloted_shares'];
                                $total_shares_availed_esop += $user_stock_offer['employee_alloted_shares'];
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['total_shares_availed'] = $total_shares_availed_company;

                                $total_shares_unavailed_company = $user_stock_offer['company_allotment'] - $total_shares_availed_company;
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['total_shares_unavailed'] = $total_shares_unavailed_company; 

                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['total_alloted_shares'] = $user_stock_offer['company_allotment']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_name']]['company_allotment'] = $user_stock_offer['company_allotment'];
                            }
                            $esop_data[$i]['esop_batches'][$x]['total_shares_availed'] = $total_shares_availed_esop;
                            $esop_data[$i]['esop_batches'][$x]['total_shares_unavailed'] = $esop_batch['total_share_qty'] - $total_shares_availed_esop;
                        }
                    }
                }

                $esops = $esop_data;
                $response['total_rows'] = $total_rows;
                $response['data'] = $esops;
                $response['status'] = TRUE;
            }
        }

        return $response;
    }

    public function generate_random_numbers($len = 15){

        $char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234568790abcdefghijklmnopqrstuvwxyz";
        $response = "";
        for($i = 0; $i <= $len; $i++) {
            $response .= $char[rand(0, strlen($char) - 1)];
        }

        return $response;
    }

    public function generate_esop_summary(){
        $company = $this->input->post('company');
        $department = $this->input->post('department');
        $rank = $this->input->post('rank');
        $esop_id = $this->input->post('esop_id');

        $basepath = BASEPATH;
        $path = str_replace('system/', '', $basepath);
        $upload_path = $path . 'downloads/';

        $dateNow = date("F d,Y l h.i A");
        $file_name = 'ESOP Summary Report '.$dateNow.'.xlsx';
        $uploaded_path = $upload_path.$file_name;

        $arr_company = json_decode($company, TRUE);
        $arr_department = json_decode($department, TRUE);
        $arr_rank = json_decode($rank, TRUE);
        $string_where_rank = "";


        $response = array(
            "status" => FALSE,
            "message" => '',
            "data" => array()
        );

        $this->load->model('departments/Departments_model');
        $this->load->model('companies/Companies_model');
        $this->load->model('ranks/Ranks_model');
        $this->load->model('esop/Esop_model');
        $this->load->model('users/Users_model');

        $company_list = array();
        $department_list = array();
        $rank_list = array();

        foreach ($arr_company as $c) {

            if(count($c) > 0)
            {
                $comp_data = $this->Companies_model->get_company_data (array("limit" => 1, "where" => array(
                    array(
                        "field" => "c.id",
                        "operator" => "=",
                        "value" => $c['company_id']
                    )
                )));
                $company_list[] = $comp_data[0];
            }
                
            
        }

        foreach ($arr_department as $c) {
            if(count($c) > 0)
            {

                $dept_data = $this->Departments_model->get_department_data(array("limit" => 1, 
                    "where" => array(
                        array(
                            "field" => "d.id", 
                            "value" => $c['department_id']
                        )
                    )));
                $department_list[] = $dept_data[0];
            }
        }

        foreach ($arr_rank as $c) {
            if(count($c) > 0)
            {

                $rank_data = $this->Ranks_model->get_rank_data(array("limit" => 1, 
                    "where" => array(
                        array(
                            "field" => "r.id", 
                            "value" => $c['rank_id']
                        )
                    )));
                $rank_list[] = $rank_data[0];
            }
        }

        $error = 0;


        if(!isset($company)){
            $error++;
        }

        if(!isset($department)){
            $error++;
        }

        if(!isset($rank)){
            $error++;
        }else{
            if(count($arr_rank) > 0){
                foreach ($arr_rank as $i => $r) {
                    if( (count($arr_rank) - 1) == (int)$i){

                    }else{
                        $string_where_rank .= "";
                    }
                }
            }
        }

        if(!isset($esop_id)){
            $error++;
        }

        if($error == 0){

            $data = array();
 
            $esop_qualifiers = array(
                "limit" => 1, 
                "where" => array(
                    array(
                        "id" => "e.id", 
                        "value" => $esop_id
                    )
                ) 
            );

            $esop = $this->Esop_model->get_esop_data($esop_qualifiers);
            $users = $this->Esop_model->get_user_esop($esop_id);


            foreach ($users as &$user) {
                $user_data = $this->Users_model->get_user_data(
                    array("where" => array(
                            array( "field" => "u.id", "operator" => "=", "value" => $user["user_id"] )
                        )
                    )
                );
                $user = $user_data[0];
            }


            $filtered_users = array();

            /*FOR FILTER*/
            foreach ($users as &$user) {
                $bool_add = false;
                $bool_in_company = true;
                $bool_in_department = true;
                $bool_in_rank = true;


                /* FILTER BY COMPANY */
                /* Check if array of companies' id are requested */
                if(count($arr_company) > 0){
                    $is_in_comp = false;
                    foreach ($arr_company as $c) {
                        if( $c['company_id'] == $user['company_id']){
                            $is_in_comp = true;
                        }
                    }
                    $bool_in_company = $is_in_comp;
                }
                /* END FILTER BY COMPANY */

                /* FILTER BY DEPARTMENT */
                /* Check if array of departments' id are requested */
                if(count($arr_department) > 0){
                    $is_in_dep = false;
                    foreach ($arr_department as $c) {
                        if( $c['department_id'] == $user['department_id']){
                            $is_in_dep = true;
                        }
                    }
                    $bool_in_department = $is_in_dep;
                }
                /* END FILTER BY DEPARTMENT */

                /* FILTER BY RANK */
                /* Check if array of ranks' id are requested */
                if(count($arr_rank) > 0){
                    $is_in_rank = false;
                    foreach ($arr_rank as $c) {
                        if( $c['rank_id'] == $user['rank_id']){
                            $is_in_rank = true;
                        }
                    }
                    $bool_in_rank = $is_in_rank;
                }
                /* END FILTER BY RANK */

                if($bool_in_company === true AND $bool_in_department === true AND $bool_in_rank === true){
                    $bool_add = true;
                }

                if($bool_add === true){
                    $filtered_users[] = $user;
                }
            }

            $users_by_rank = array();
            foreach($rank_list as $rank) {
                foreach ($users as &$user) {
                    if($rank['id'] == $user['rank_id']){
                        if(array_key_exists($rank['id'], $users_by_rank)){
                            $users_by_rank[$rank['id']][] = $user;
                        }else{
                            $users_by_rank[$rank['id']] = array();
                            $users_by_rank[$rank['id']][] = $user;
                        }
                    }
                }
            }


            $data = array(
                "esop" => $esop[0],
                "uploaded_path" => $uploaded_path,
                "file_name" => $file_name,
                "rank_list" => $rank_list,
                "company_list" => $company_list,
                "department_list" => $department_list,
                "users" => $filtered_users,
                "users_by_rank" => $users_by_rank
            );


            $file_url = $this->build_esop_summary_excel($data);

            $data['file_url'] = $file_url;
            $response['status'] = true;
            $response['data'] = $data;
            echo json_encode($response);
        }else{
            $response['message'] = 'Invalid parameters';
            echo json_encode($response);
        }
    }

    public function build_esop_summary_excel($data){

        $this->load->library('excel');
        $spreadsheet = new Excel();

        $company_names = "";
        $company_list = $data['company_list'];
        $esop = $data["esop"];
        $users_by_rank = $data["users_by_rank"];
        $rank_list = $data["rank_list"];
        $uploaded_path = $data['uploaded_path'];

        foreach ($company_list as $i => $c) {
            if(count($company_list) - 1 == $i){
                $company_names .= strtoupper($c['name']);
            }else{
                $company_names .= strtoupper($c['name']).",";
            }
        }

        $styleBigBold = array('font'  => array('bold'  => true));
        $styleBigBoldCenterBorder = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER ),
            'font'  => array('bold'  => true),
            'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN ) )
        );
        $styleCenterBold = array(
            'font' => array('bold'  => true),
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleCenter = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleLeft = array(
            'alignment' => array("horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT )
        );


        $spreadsheet->setActiveSheetIndex(0);
        
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->SetCellValueByColumnAndRow(0, 1, $company_names);
        $spreadsheet->getActiveSheet()->getStyle("A1")->applyFromArray($styleBigBold);

        $name_share = $esop["name"]." @ P".$esop["price_per_share"]."/SHARE";
        $worksheet->SetCellValueByColumnAndRow(0, 2, $name_share);
        $spreadsheet->getActiveSheet()->getStyle("A2")->applyFromArray($styleBigBold);

        $grant_date = "DATE GRANTED:" . strtoupper(date('M d, Y', strtotime($esop["grant_date"])));
        $worksheet->SetCellValueByColumnAndRow(0, 3, $grant_date);
        $spreadsheet->getActiveSheet()->getStyle("A3")->applyFromArray($styleBigBold);

        $subscribed = "SUBSCRIBED";
        $worksheet->SetCellValueByColumnAndRow(6, 6, $subscribed);
        $spreadsheet->getActiveSheet()->getStyle("G6:H6")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->mergeCells("G6:H6");

        $worksheet->SetCellValueByColumnAndRow(0, 7, "");
        $spreadsheet->getActiveSheet()->getStyle("A7")->applyFromArray($styleBigBoldCenterBorder);

        $worksheet->SetCellValueByColumnAndRow(1, 7, "ECODE");
        $spreadsheet->getActiveSheet()->getStyle("B7")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(15);

        $worksheet->SetCellValueByColumnAndRow(2, 7, "LAST NAME");
        $spreadsheet->getActiveSheet()->getStyle("C7")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);

        $worksheet->SetCellValueByColumnAndRow(3, 7, "FIRST NAME");
        $spreadsheet->getActiveSheet()->getStyle("D7")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);

        $worksheet->SetCellValueByColumnAndRow(4, 7, "MIDDLE NAME");
        $spreadsheet->getActiveSheet()->getStyle("E7")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);

        $worksheet->SetCellValueByColumnAndRow(5, 7, "DEPARTMENT");
        $spreadsheet->getActiveSheet()->getStyle("F7")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

        $worksheet->SetCellValueByColumnAndRow(6, 7, "NO. OF SHARES");
        $spreadsheet->getActiveSheet()->getStyle("G7")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getStyle("G7")->getAlignment()->setWrapText(true);

        $worksheet->SetCellValueByColumnAndRow(7, 7, "AMOUNT (@ P".$esop['price_per_share'].")");
        $spreadsheet->getActiveSheet()->getStyle("H7")->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getStyle("H7")->getAlignment()->setWrapText(true);

        $row = 9;
        $column = 0;
        $counter_rank = 1;
        $counter_user = 1;

        foreach ($rank_list as $rank) {
            $id = intval($rank["id"]);

            if( array_key_exists($id, $users_by_rank) ){
                $users_rank = $users_by_rank[$id];
                $roman_number_rank = $this->romanic_number($counter_rank);
                $worksheet->SetCellValueByColumnAndRow($column, $row,  $roman_number_rank);
                $number_to_letter = $this->number_to_letter($column);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleCenterBold);
                $column++;

                $worksheet->SetCellValueByColumnAndRow($column, $row,  strtoupper($rank["name"]));
                $number_to_letter = $this->number_to_letter($column);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleCenterBold);
                $column = $column + 2;

                $total_number_of_user = count($users_rank);
                $worksheet->SetCellValueByColumnAndRow($column, $row,  $total_number_of_user);
                $number_to_letter = $this->number_to_letter($column);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleCenterBold);

                $row = $row + 2;
                $column = 0;
                foreach ($users_rank as $u) {
                
                    $worksheet->SetCellValueByColumnAndRow($column, $row, $counter_user);
                    $number_to_letter = $this->number_to_letter($column);

                    // $spreadsheet->getActiveSheet()->setCellValue($number_to_letter.$row, 'Hello');
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleCenter);
                    $column++;
                    $worksheet->SetCellValueByColumnAndRow($column, $row, strtoupper($u["employee_code"]));
                    $number_to_letter = $this->number_to_letter($column);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleCenter);
                    $column++;
                    $worksheet->SetCellValueByColumnAndRow($column, $row,  strtoupper($u["last_name"]));
                    $column++;
                    $worksheet->SetCellValueByColumnAndRow($column, $row,  strtoupper($u["first_name"]));
                    $column++;
                    $worksheet->SetCellValueByColumnAndRow($column, $row,  strtoupper($u["middle_name"]));
                    $column++;
                    $worksheet->SetCellValueByColumnAndRow($column, $row,  strtoupper($u["department_name"]));
                    $column++;

                    $row++;
                    $column = 0;

                    $worksheet->SetCellValueByColumnAndRow(($column + 2), $row,  strtoupper("SUB_TOTAL"));
                    $number_to_letter = $this->number_to_letter(($column + 2));
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                    $counter_user++;  
                }

                $counter_rank++;
            }
        }

        $writer = new PHPExcel_Writer_Excel2007($spreadsheet);
        $writer->save($uploaded_path);

        $file_name = $data['file_name'];
        $file_url = base_url() ."downloads/".$file_name;
        
        return $file_url;
    }

    public function generate_subscription_summary(){
        $company = $this->input->post('company');
        $esop = $this->input->post('esop');

        $basepath = BASEPATH;
        $path = str_replace('system/', '', $basepath);
        $upload_path = $path . 'downloads/';

        //$random_string = $this->generate_random_numbers(20);
        $dateNow = date("F d,Y l h.i A");
        $file_name = 'Subscription Summary Report '.$dateNow.'.xlsx';
        $uploaded_path = $upload_path.$file_name;

        $arr_company = json_decode($company, TRUE);
        $arr_esop = $esop;
        $summary = array();
        foreach ($arr_esop as $id => $e) {
           $summary[$e['esop_id']] = $this->Reports_model->getSubscriptionSummary($e['esop_id']);
        }
        if(count($arr_esop) == 0){
            $response = array(
                "status" => FALSE,
                "message" => '',
                "data" => array()
            );

            $response['message'] = 'Invalid parameters';
            echo json_encode($response);
        }else{
            $this->load->model('departments/Departments_model');
            $this->load->model('companies/Companies_model');
            $this->load->model('ranks/Ranks_model');
            $this->load->model('esop/Esop_model');
            $this->load->model('users/Users_model');
            $this->load->model('reports/Reports_model');

            $company_list = array();
            $esop_list = array();
            $rank_list = $this->Ranks_model->get_rank_data(array("limit" => 9999, "where" => array()));
            $report_list = $summary;
            $data = array();

            foreach ($arr_company as $c) {
                $comp_data = $this->Companies_model->get_company_data (array("limit" => 1, "where" => array(
                        array(
                            "field" => "c.id",
                            "operator" => "=",
                            "value" => $c['company_id']
                        )
                    )));
                $company_list[] = $comp_data[0];
            }

            foreach ($arr_esop as $c) {
                $esop_data = $this->Esop_model->get_esop_data (array("limit" => 1, "where" => array(
                        array(
                            "field" => "e.id",
                            "operator" => "=",
                            "value" => $c['esop_id']
                        )
                    )));
                
                $data_esop = array(
                    "esop_data" => $esop_data[0]
                );

                $esop_list[] = $data_esop;
            }

            unset($rank_list["total_rows"]);

            $data['esop_list'] = $esop_list;
            $data['company_list'] = $company_list;
            $data['uploaded_path'] = $uploaded_path;
            $data['file_name'] = $file_name;
            $data['rank_list'] = $rank_list;
            $data['report_list'] = $report_list;

            $file_url = $this->build_subscription_summary_excel($data);
            $data['file_url'] = $file_url;

            $response = array(
                "status" => TRUE,
                "message" => '',
                "data" => $data
            );
            echo json_encode($response);

        }
    }

    public function build_subscription_summary_excel($data){

        $this->load->library('excel');
        $spreadsheet = new Excel();

        $company_names = "GROUPWIDE SUMMARY OF ";
        $company_list = $data['company_list'];
        $esop_list = $data["esop_list"];
        $rank_list = $data["rank_list"];
        $report_list = $data["report_list"];
        $file_name = $data["file_name"];

        $uploaded_path = $data['uploaded_path'];
        foreach ($esop_list as $i => $c) {
            $esop_data = $c['esop_data'];
            if(count($esop_list) - 1 == $i){
                $company_names .= strtoupper($esop_data['name']);
            }else{
                $company_names .= strtoupper($esop_data['name']).",";
            }
        }

        $company_names .= " SUBSCRIPTION (Shares & Amount)";

        $styleBigBold = array('font'  => array('bold'  => true));
        $styleBoldUnderline = array(
            'font' => array('underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE, 'bold'  => true),
        );
        $styleBigBoldCenterBorder = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER ),
            'font'  => array('bold'  => true),
            'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN ) )
        );
        $styleCenterBold = array(
            'font' => array('bold'  => true),
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleCenter = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleLeft = array(
            'alignment' => array("horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT )
        );

        $date = date('F d, Y');

        $spreadsheet->setActiveSheetIndex(0);
        
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->SetCellValueByColumnAndRow(0, 1, $company_names);
        $spreadsheet->getActiveSheet()->getStyle("A1")->applyFromArray($styleBigBold);

        $worksheet->SetCellValueByColumnAndRow(0, 2, $date);

        $row = 5;
        $column = 0;
        $counter_esop = 1;

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);

        foreach ($esop_list as $e) {
            $esop_data = $e['esop_data'];
            
            $roman_number_esop = $this->romanic_number($counter_esop);
            $worksheet->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
            $column++;

            $year_years = "year";
            if(intval($esop_data["vesting_years"]) > 1){
                $year_years = "years";
            }
            $countdate="";
            foreach ($report_list as $r => $report) {
                if($esop_data['id'] == $r AND count($report)>0)
                {
                    $countdate = $report[0]['e_grant_date'];
                }
            }
            $esop_name_price_vesting = strtoupper($esop_data['name'])." @ P".$esop_data["price_per_share"] . " (with ".$countdate." ".$year_years." vesting rights)";
            $worksheet->SetCellValueByColumnAndRow($column, $row,  $esop_name_price_vesting);
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBoldUnderline);
            $column++;

            $row++;
            $row++;
            $column = 0;
            $countdate = 0;

            $number_to_letter1 = $this->number_to_letter($column);
            $merge_start = $number_to_letter1.$row;
            $number_to_letter2 = $this->number_to_letter($column + 1);
            $merge_end = $number_to_letter2.($row + 1);
            $spreadsheet->getActiveSheet()->mergeCells($merge_start.":".$merge_end);

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "CLASSIFICATION/RANK");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
            $column += 2;

            $number_to_letter_subscribed1 = $this->number_to_letter($column);
            $merge_start_subscribed = $number_to_letter_subscribed1.$row;
            $number_to_letter_subscribed2 = $this->number_to_letter($column + 1);
            $merge_end_subscribed = $number_to_letter_subscribed2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_subscribed.":".$merge_end_subscribed);

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "SUBSCRIBED");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $row++;

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "No. of shares");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $column++;

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "Amount");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $column++;
            $row--;

            $number_to_letter_w_vesting1 = $this->number_to_letter($column);
            $merge_end_w_vesting_start = $number_to_letter_w_vesting1.$row;
            $number_to_letter_w_vesting2 = $this->number_to_letter($column + 1);
            $merge_end_w_vesting_end = $number_to_letter_w_vesting2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_end_w_vesting_start.":".$merge_end_w_vesting_end);

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "W/VESTING RIGHTS");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $row++;

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "No. of shares");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $column++;

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "Amount");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $column++;
            $row--;

            $number_to_letter_wo_vesting1 = $this->number_to_letter($column);
            $merge_end_wo_vesting_start = $number_to_letter_wo_vesting1.$row;
            $number_to_letter_wo_vesting2 = $this->number_to_letter($column + 1);
            $merge_end_wo_vesting_end = $number_to_letter_wo_vesting2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_end_wo_vesting_start.":".$merge_end_wo_vesting_end);

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "W/O VESTING RIGHTS");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $row++;

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "No. of shares");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $column++;

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "Amount");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
            $row++;

            /* USER RANK */
            $rank_counter = 1;
            $column = 0;

            $row++;
            $rank_length = count($rank_list);

            foreach ($report_list as $r => $report) {
                if($esop_data['id'] == $r)
                {
                    $total1= $total2= $total3= $total4= $total5= $total6 =0;

                    foreach ($report as $r1 => $report1) {
                        $worksheet->SetCellValueByColumnAndRow($column, $row,  $rank_counter);
                        $column++;
                        $worksheet->SetCellValueByColumnAndRow($column, $row, $report1["rank_name"]);
                        $worksheet->SetCellValueByColumnAndRow(($column+1), $row, number_format($report1["no_of_shares_subscribed"],2));
                        $worksheet->SetCellValueByColumnAndRow(($column+2), $row, number_format($report1["amount_subscribed"],2));
                        $worksheet->SetCellValueByColumnAndRow(($column+3), $row, number_format($report1["no_of_shares_wvr"],2));
                        $worksheet->SetCellValueByColumnAndRow(($column+4), $row, number_format($report1["amount_wvr"],2));
                        $worksheet->SetCellValueByColumnAndRow(($column+5), $row, number_format($report1["no_of_shares_wovr"],2));
                        $worksheet->SetCellValueByColumnAndRow(($column+6), $row, number_format($report1["amount_wovr"],2));

                        $total1 += $report1["no_of_shares_subscribed"];
                        $total2 += $report1["amount_subscribed"];
                        $total3 += $report1["no_of_shares_wvr"];
                        $total4 += $report1["amount_wvr"];
                        $total5 += $report1["no_of_shares_wovr"];
                        $total6 += $report1["amount_wovr"];
                        $column = 0;
                        $row += 2;
                        $rank_counter++;
                    }
                    //$totalbold =  $worksheet->SetCellValueByColumnAndRow(($column+1), $row, 'Total');
                    $worksheet->SetCellValueByColumnAndRow(($column+1), $row,  'TOTAL');
                    $number_to_letter = $this->number_to_letter($column+1);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                    $worksheet->SetCellValueByColumnAndRow(($column+2), $row, number_format($total1,2));
                    $number_to_letter = $this->number_to_letter($column+2);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

                    $worksheet->SetCellValueByColumnAndRow(($column+3), $row, number_format($total2,2));
                    $number_to_letter = $this->number_to_letter($column+3);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

                    $worksheet->SetCellValueByColumnAndRow(($column+4), $row, number_format($total3,2));
                    $number_to_letter = $this->number_to_letter($column+4);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

                    $worksheet->SetCellValueByColumnAndRow(($column+5), $row, number_format($total4,2));
                    $number_to_letter = $this->number_to_letter($column+5);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

                    $worksheet->SetCellValueByColumnAndRow(($column+6), $row, number_format($total5,2));
                    $number_to_letter = $this->number_to_letter($column+6);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

                    $worksheet->SetCellValueByColumnAndRow(($column+7), $row, number_format($total6,2));
                    $number_to_letter = $this->number_to_letter($column+7);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

                } 
            }

            /* END USER RANK */



            $counter_esop++;
            $column = 0;
            $row += 3;
        }


        $writer = new PHPExcel_Writer_Excel2007($spreadsheet);
        $writer->save($uploaded_path);

        $file_name = $data['file_name'];
        $file_url = base_url() ."downloads/".$file_name;
        
        return $file_url;
    }

    public function generate_claim_summary(){
        $esop_id = $this->input->post('esop_id');
        $company_id = $this->input->post('company_id');
        $department = $this->input->post('department');
        $rank = $this->input->post('rank');

        $basepath = BASEPATH;
        $path = str_replace('system/', '', $basepath);
        $upload_path = $path . 'downloads/';

        $dateNow = date("F d,Y l h.i A");
        $file_name = 'Claim Summary Report '.$dateNow.'.xlsx';
        $uploaded_path = $upload_path.$file_name;

      
        $arr_department = json_decode($department, TRUE);
        $arr_rank = json_decode($rank, TRUE);
        $string_where_rank = "";


        $response = array(
            "status" => FALSE,
            "message" => '',
            "data" => array()
        );

        if($esop_id != ""){

            $this->load->model('departments/Departments_model');
            $this->load->model('companies/Companies_model');
            $this->load->model('ranks/Ranks_model');
            $this->load->model('esop/Esop_model');
            $this->load->model('users/Users_model');

        

            $get_esop_name = $this->Esop_model->get_esop_name_info($esop_id);
            // $get_user_esop_info = $this->Esop_model->get_user_esop_info($esop_id, $company_id);
            $get_user_claimed = $this->Reports_model->get_user_claimed_info($esop_id, $company_id);

            $get_company_info = $this->Companies_model->get_company_names($company_id);

            foreach ($get_user_claimed as $key => $value) {
                // $get_user_esop_info[$key]["total_share"] = floatval($value["price_per_share"]) * floatval($value["num_shares"]);
                $arr_vesting_years = array();

                $get_user_vesting = $this->Reports_model->get_user_vesting_info($value["user_esop_id"]);

                $total_sub_amount = 0;
                $total_sub_shares = 0;
                foreach ($get_user_vesting as $vestingkey => $vestingvalue) {
                    $date_now = date("Y-m-d");
                    $this_date = strtotime($date_now);
                    $value_date = strtotime($vestingvalue["year"]);



                    if($this_date >= $value_date)
                    {
                        $get_user_vesting[$vestingkey]["total_amount"] = floatval($vestingvalue["no_of_shares"]) * floatval($get_esop_name[0]["price_per_share"]); 
                        array_push($arr_vesting_years, $get_user_vesting[$vestingkey]);

                        // $total_sub_shares += floatval($vestingvalue["no_of_shares"]);
                        // $total_sub_amount += floatval( $get_user_vesting[$vestingkey]["total_amount"]);
                    }


                }

                // $arr_vesting_years["total_shares"] = $total_sub_shares;
                // $arr_vesting_years["total_amount"] = $total_sub_amount;
                $get_user_claimed[$key]["vesting_info"] = $arr_vesting_years;

                $arr_vesting_years = array();

            }

            $build_active_user_not_claimed = array();
            $build_active_user_claimed = array();
            $build_not_active_user_not_claimed = array();
            $build_not_active_user_claimed = array();

            foreach ($get_user_claimed as $key => $value) {
                if($value["employment_status"] == "1" AND $value["claim_status"] != "2")
                {
                    array_push($build_active_user_not_claimed, $get_user_claimed[$key]);
                }else if($value["employment_status"] == "1" AND $value["claim_status"] == "2"){
                    array_push($build_active_user_claimed, $get_user_claimed[$key]);
                }else if($value["employment_status"] != "1" AND $value["claim_status"] != "2"){
                    array_push($build_not_active_user_not_claimed, $get_user_claimed[$key]);
                }else if($value["employment_status"] != "1" AND $value["claim_status"] == "2"){
                    array_push($build_not_active_user_claimed, $get_user_claimed[$key]);
                }
            }



            $arr_stored_claims_data = array(
                    "active_user_not_claimed"=> $build_active_user_not_claimed,
                    "active_user_claimed"=>$build_active_user_claimed,
                    "not_active_user_not_claimed"=>$build_not_active_user_not_claimed,
                    "not_active_user_claimed"=>$build_not_active_user_claimed
                );


            $data = array(
            "esop_id"=>$esop_id,
            "esop_name"=>$get_esop_name[0]["esop_name"],
            "price_per_share"=>$get_esop_name[0]["price_per_share"],
            "grant_date"=>$get_esop_name[0]["grant_date_formatted"],
            "company_info"=>$get_company_info[0]["name"],
            "esop_user_info"=>$arr_stored_claims_data,
            "file_name" => $file_name,
            "uploaded_path"=>$uploaded_path

            );


            $file_url = $this->extract_claim_summary_excel($data);

            $data['file_url'] = $file_url;
            $response['status'] = true;
            $response['data'] = $data;
            echo json_encode($response);
        }else{
            $response['message'] = 'Invalid parameters';
            echo json_encode($response);
        }
    }

    public function romanic_number($integer, $upcase = true) 
    { 
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
        $return = ''; 
        while($integer > 0) 
        { 
            foreach($table as $rom=>$arb) 
            { 
                if($integer >= $arb) 
                { 
                    $integer -= $arb; 
                    $return .= $rom; 
                    break; 
                } 
            } 
        } 

        return $return; 
    }

    public function number_to_letter($int = 0) {
        $alphabet = range('A', 'Z');
        return $alphabet[$int];
    }

    public function extract_esop_summary()
    {
        $esop_id = $this->input->post('esop_id');
        $company_id = $this->input->post('company_id');
        $department = $this->input->post('department');
        $rank = $this->input->post('rank');

        $basepath = BASEPATH;
        $path = str_replace('system/', '', $basepath);
        $upload_path = $path . 'downloads/';

        $dateNow = date("F d,Y l h.i A");
        $file_name = 'ESOP Summary Report '.$dateNow.'.xlsx';
        $uploaded_path = $upload_path.$file_name;


        $arr_department = json_decode($department, TRUE);
        $arr_rank = json_decode($rank, TRUE);
        $string_where_rank = "";


        $response = array(
            "status" => FALSE,
            "message" => '',
            "data" => array()
        );

        $this->load->model('departments/Departments_model');
        $this->load->model('companies/Companies_model');
        $this->load->model('ranks/Ranks_model');
        $this->load->model('esop/Esop_model');
        $this->load->model('users/Users_model');

    

        $get_esop_name = $this->Esop_model->get_esop_name_info($esop_id);
        $get_user_esop_info = $this->Esop_model->get_user_esop_info($esop_id, $company_id);
        $get_company_info = $this->Companies_model->get_company_names($company_id);

        foreach ($get_user_esop_info as $key => $value) {
            $get_user_esop_info[$key]["total_share"] = floatval($value["price_per_share"]) * floatval($value["num_shares"]);

        }


        $data = array(
            "esop_id"=>$esop_id,
            "esop_name"=>$get_esop_name[0]["esop_name"],
            "price_per_share"=>$get_esop_name[0]["price_per_share"],
            "grant_date"=>$get_esop_name[0]["grant_date_formatted"],
            "company_info"=>$get_company_info[0]["name"],
            "esop_user_info"=>$get_user_esop_info,
            "file_name" => $file_name,
            "uploaded_path"=>$uploaded_path

            );


        $file_url = $this->extract_esop_summary_excel($data);

        $data['file_url'] = $file_url;
        $response['status'] = true;
        $response['data'] = $data;
        echo json_encode($response);

    }

    public function extract_share_summary()
    {
        $esop_id = $this->input->post('esop_id');
        $company_info = json_decode($this->input->post('company_id'),TRUE);

        $basepath = BASEPATH;
        $path = str_replace('system/', '', $basepath);
        $upload_path = $path . 'downloads/';

        $dateNow = date("F d,Y l h.i A");
        $file_name = 'Share Summary Report '.$dateNow.'.xlsx';
        $uploaded_path = $upload_path.$file_name;
        $build_company_info = array();


        $response = array(
            "status" => FALSE,
            "message" => '',
            "data" => array()
        );

        $this->load->model('departments/Departments_model');
        $this->load->model('companies/Companies_model');
        $this->load->model('ranks/Ranks_model');
        $this->load->model('esop/Esop_model');
        $this->load->model('users/Users_model');

        $get_esop_name_info = $this->Esop_model->get_esop_info_claims($esop_id);

        foreach ($company_info as $k => $v) {
            $getCompany = $this->Reports_model->getShareSummary($esop_id,$v['company_id']);
            array_push($build_company_info,$getCompany);
        }
      
        $icount = 0;
        $createRank = array();
        $icount_records = 0;
        
        $ranks_array = array();
        $grand_total = array();
        $grand_total_claim = array();
        $build_temp_comp_name = array();
        $wvr_total = 0;

        foreach ($build_company_info as $info) {
            foreach ($info as $i) {
                if(!array_key_exists($i['rank_name'], $ranks_array)){
                    $ranks_array[$i['rank_name']] = array();
                }
            }
        }


         
        foreach ($build_company_info as $info) {
            foreach ($info as $i) {
                $ranks_array[$i['rank_name']][] = $i;
            }
        }

        //FOR GRAND TOTAL
        foreach ($build_company_info as $key => $value) {
            $itotal = 0;
            foreach ($build_company_info[$key] as $i) {
                if(!array_key_exists($i['company_name'], $grand_total)){
                    $grand_total[$i['company_name']]= floatval($i['subscribed']);
                }
                else{
                     $grand_total[$i['company_name']]+= floatval($i['subscribed']);
                }
            } 
            
        }

        // FOR ACTIVE UNCLAIMED W/O VESTING RIGHTS
        foreach ($ranks_array as $key => $value) {
            foreach ($ranks_array[$key] as $k => $v) {
                $get_user_info = $this->Reports_model->getShareActiveUnClaimUserWOV($esop_id, $v["company_id"],$v["rank_id"],$v["dept_id"]);
                $ranks_array[$key][$k]["active_unclaimed_wov"] = $get_user_info[0]["total_active"];
                $wvr_total+= floatval($get_user_info[0]["total_active"]);
            }
        }
        $grand_total_claim['active_unclaimed_wov'] = $wvr_total;
        $wvr_total = 0;

        // FOR SEPARATED UNCLAIMED W/O VESTING RIGHTS
        foreach ($ranks_array as $key => $value) {
            foreach ($ranks_array[$key] as $k => $v) {
                $get_user_info = $this->Reports_model->getShareSeparatedUnClaimUserWOV($esop_id, $v["company_id"],$v["rank_id"],$v["dept_id"]);
                $ranks_array[$key][$k]["separated_unclaimed_wov"] = $get_user_info[0]["total_active"];
                $wvr_total+= floatval($get_user_info[0]["total_active"]);
            }
        }
        $grand_total_claim['separated_unclaimed_wov'] = $wvr_total;
        $wvr_total = 0;

        // FOR ACTIVE CLAIMED WITH VESTING RIGHTS
        foreach ($ranks_array as $key => $value) {
            foreach ($ranks_array[$key] as $k => $v) {
                $get_user_info = $this->Reports_model->getShareActiveClaimUser($esop_id, $v["company_id"],$v["rank_id"],$v["dept_id"]);
                $ranks_array[$key][$k]["active_claimed"] = $get_user_info[0]["total_active"];
                $wvr_total+= floatval($get_user_info[0]["total_active"]);
            }
        }
        $grand_total_claim['active_claimed'] = $wvr_total;
        $wvr_total = 0;

     

        // FOR SEPARATED CLAIMED WITH VESTING RIGHTS
        foreach ($ranks_array as $key => $value) {
            foreach ($ranks_array[$key] as $k => $v) {
                $get_user_info = $this->Reports_model->getShareSeparatedClaimUser($esop_id, $v["company_id"],$v["rank_id"],$v["dept_id"]);
                $ranks_array[$key][$k]["separated_claimed"] = $get_user_info[0]["total_active"];
                $wvr_total+= floatval($get_user_info[0]["total_active"]);
            }
        }
        $grand_total_claim['separated_claimed'] = $wvr_total;
        $wvr_total = 0;


        // FOR ACTIVE UNCLAIMED WITH VESTING RIGHTS
        foreach ($ranks_array as $key => $value) {
            foreach ($ranks_array[$key] as $k => $v) {
                $get_user_info = $this->Reports_model->getShareActiveUnClaimUser($esop_id, $v["company_id"],$v["rank_id"],$v["dept_id"]);
                $ranks_array[$key][$k]["active_unclaimed"] = $get_user_info[0]["total_active"];
                $wvr_total+= floatval($get_user_info[0]["total_active"]);
            }
        }
        $grand_total_claim['active_unclaimed'] = $wvr_total;
        $wvr_total = 0;

        // FOR SEPARATED UNCLAIMED WITH VESTING RIGHTS
        foreach ($ranks_array as $key => $value) {
            foreach ($ranks_array[$key] as $k => $v) {
                $get_user_info = $this->Reports_model->getShareSeparatedUnClaimUser($esop_id, $v["company_id"],$v["rank_id"],$v["dept_id"]);
                $ranks_array[$key][$k]["separated_unclaimed"] = $get_user_info[0]["total_active"];
                $wvr_total+= floatval($get_user_info[0]["total_active"]);
            }
        }
        $grand_total_claim['separated_unclaimed'] = $wvr_total;
        $wvr_total = 0;

        //FOR GETTING ACTIVE GRAND TOTAL BY COMPANY
        $arr_comp_name = array();
        foreach ($ranks_array as $info) {
            foreach ($info as $i) {
                if(!array_key_exists($i['company_name'], $arr_comp_name)){
                    $arr_comp_name[$i['company_name']] = 0;
                }

                if(!array_key_exists($i['company_name'], $build_temp_comp_name)){
                    $build_temp_comp_name[$i['company_name']] = 0;
                }
            }
        }
  

        foreach ($arr_comp_name as $trykey => $tryvalue) {
           $arr_comp_name[$trykey] = array("active_claimed"=>0, "separated_claimed"=>0, "active_unclaimed"=>0, "separated_unclaimed"=>0, "active_unclaimed_wov"=>0, "separated_unclaimed_wov"=>0);
        }

  
        foreach ($ranks_array as $info) {
            foreach ($info as $i) {
                $count_item = 0;
                foreach ($arr_comp_name as $arrcompkey => $arrcompvalue) {
 
                    if($arrcompkey==$i['company_name']){



                        $total_active_claimed = $arr_comp_name[$arrcompkey]["active_claimed"];
                        $total_separated_claimed = $arr_comp_name[$arrcompkey]["separated_claimed"];
                        $total_active_unclaimed = $arr_comp_name[$arrcompkey]["active_unclaimed"];
                        $total_separated_unclaimed = $arr_comp_name[$arrcompkey]["separated_unclaimed"];
                        $total_active_unclaimed_wov = $arr_comp_name[$arrcompkey]["active_unclaimed_wov"];
                        $total_separated_unclaimed_wov = $arr_comp_name[$arrcompkey]["separated_unclaimed_wov"];


                        $total_active_claimed += floatval($i['active_claimed']);

                        $total_separated_claimed += floatval($i['separated_claimed']);

                        $total_active_unclaimed += floatval($i['active_unclaimed']);

                        $total_separated_unclaimed += floatval($i['separated_unclaimed']);

                        $total_active_unclaimed_wov += floatval($i['active_unclaimed_wov']);

                        $total_separated_unclaimed_wov += floatval($i['separated_unclaimed_wov']);


                        $arr_comp_name[$arrcompkey]["active_claimed"] = $total_active_claimed;
                        $arr_comp_name[$arrcompkey]["separated_claimed"] = $total_separated_claimed;
                        $arr_comp_name[$arrcompkey]["active_unclaimed"] = $total_active_unclaimed;
                        $arr_comp_name[$arrcompkey]["separated_unclaimed"] = $total_separated_unclaimed;
                        $arr_comp_name[$arrcompkey]["active_unclaimed_wov"] = $total_active_unclaimed_wov;
                        $arr_comp_name[$arrcompkey]["separated_unclaimed_wov"] = $total_separated_unclaimed_wov;
                        
 
                    } 


                }
            }
        }

  
        $data = array(
            "esop_id"=>$esop_id,
            "esop_name"=>$get_esop_name_info[0]["name"],
            "price_per_share"=>$get_esop_name_info[0]["price_per_share"],
            "vesting_years"=>$get_esop_name_info[0]["e_vesting_date"],
            "date_downloaded"=>date("F d, Y"),
            "esop_user_info"=>$ranks_array,
            "grand_total_wvr"=>$arr_comp_name,
            "grand_total_subscribed"=>$grand_total,
            "grand_total_of_total"=>$grand_total_claim,
            "comp_name_temp"=>$build_temp_comp_name,
            "file_name" => $file_name,
            "uploaded_path"=>$uploaded_path

            );

        $file_url = $this->extract_share_summary_excel($data);

        $data['file_url'] = $file_url;
        $response['status'] = true;
        $response['data'] = $data;
        echo json_encode($response);

    }

    public function extract_share_summary_excel($data)
    {

        
        // load excel
        $this->load->library('excel');
        $spreadsheet = new Excel();
        //load excel end

         $uploaded_path = $data['uploaded_path'];



        // build styles

        $styleBigBold = array('font'  => array('bold'  => true));
        $styleBigBoldCenterBorder = array(
                                  'borders' => array(
                                        'allborders' => array(
                                          'style' => PHPExcel_Style_Border::BORDER_THIN
                                        ),
                                    ),
                                   'font' => array(
                                       'bold' => true
                                    ),
                                   'alignment' => array(
                                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    ),
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'FDE9D9')
                                    )
                                
                            );
        $styleBigCenterBorder = array(
                                  'borders' => array(
                                        'allborders' => array(
                                          'style' => PHPExcel_Style_Border::BORDER_THIN
                                        ),
                                    ),
                                   'font' => array(
                                       'bold' => true
                                    ),
                                   'alignment' => array(
                                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    )
                                
                            );
        $styleCenterBold = array(
            'font' => array('bold'  => true),
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleCenter = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleLeft = array(
            'alignment' => array("horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT )
        );
        $styleBoldUnderline = array(
            'font' => array('underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE, 'bold'  => true),
        );


         //end build styles


        // Build Execl Content


        //For Title and Header

            $esop_title = "GROUPWIDE SUMMARY OF ".$data['esop_name'];


            $esop_title .= " (Shares)";

            $spreadsheet->setActiveSheetIndex(0);
            
            $worksheet = $spreadsheet->getActiveSheet();
            $worksheet->SetCellValueByColumnAndRow(0, 1, $esop_title);
            $spreadsheet->getActiveSheet()->getStyle("A1")->applyFromArray($styleBigBold);

            $worksheet->SetCellValueByColumnAndRow(0, 2, $data["date_downloaded"]);

            $row = 5;
            $column = 0;
            $counter_esop = 1;

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(20);

            
            $roman_number_esop = $this->romanic_number($counter_esop);
            $worksheet->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

            $year_years = "year";
            if(intval($data["vesting_years"]) > 1){
                $year_years = "years";
            }

            $esop_name_price_vesting = strtoupper($data['esop_name'])." @ P".$data["price_per_share"] . " (with ".$data["vesting_years"]." ".$year_years." vesting rights)";

            $worksheet->SetCellValueByColumnAndRow($column, $row,  $esop_name_price_vesting);
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBoldUnderline);
        

            $row++;
            $row++;
            $column = 0;

            $number_to_letter1 = $this->number_to_letter($column);
            $merge_start = $number_to_letter1.$row;
            $number_to_letter2 = $this->number_to_letter($column + 1);
            $merge_end = $number_to_letter2.($row + 2);
            $spreadsheet->getActiveSheet()->mergeCells($merge_start.":".$merge_end);

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "CLASSIFICATION/RANK");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
            $column += 2;

            $number_to_letter_subscribed1 = $this->number_to_letter($column);
            $merge_start_subscribed = $number_to_letter_subscribed1.$row;
            $number_to_letter_subscribed2 = $this->number_to_letter($column);
            $merge_end_subscribed = $number_to_letter_subscribed2.($row + 2);
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_subscribed.":".$merge_end_subscribed);

            $worksheet->SetCellValueByColumnAndRow($column, $row,  "SUBSCRIBED");
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_wvr = $this->number_to_letter($column+1);
            $merge_start_wvr = $number_to_letter_wvr.$row;
            $number_to_letter_wvr2 = $this->number_to_letter($column + 9);
            $merge_end_wvr = $number_to_letter_wvr2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_wvr.":".$merge_end_wvr);

            $worksheet->SetCellValueByColumnAndRow($column+1, $row,  "W/ VESTING RIGHTS");
            $number_to_letter = $this->number_to_letter($column+1);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_wovr = $this->number_to_letter($column+10);
            $merge_start_wovr = $number_to_letter_wovr.$row;
            $number_to_letter_wovr2 = $this->number_to_letter($column + 12);
            $merge_end_wovr = $number_to_letter_wovr2.($row + 1);
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_wovr.":".$merge_end_wovr);

            $worksheet->SetCellValueByColumnAndRow($column+10, $row,  "W/O VESTING RIGHTS");
            $number_to_letter = $this->number_to_letter($column+10);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $row++;

            $number_to_letter_claimed = $this->number_to_letter($column+1);
            $merge_start_claimed = $number_to_letter_claimed.$row;
            $number_to_letter_claimed2 = $this->number_to_letter($column + 3);
            $merge_end_claimed = $number_to_letter_claimed2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_claimed.":".$merge_end_claimed);

            $worksheet->SetCellValueByColumnAndRow($column+1, $row,  "CLAIMED/PURCHASED");
            $number_to_letter = $this->number_to_letter($column+1);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_unclaimed = $this->number_to_letter($column+4);
            $merge_start_unclaimed = $number_to_letter_unclaimed.$row;
            $number_to_letter_unclaimed2 = $this->number_to_letter($column + 6);
            $merge_end_unclaimed = $number_to_letter_unclaimed2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_unclaimed.":".$merge_end_unclaimed);

            $worksheet->SetCellValueByColumnAndRow($column+4, $row,  "UNCLAIMED");
            $number_to_letter = $this->number_to_letter($column+4);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_total = $this->number_to_letter($column+7);
            $merge_start_total = $number_to_letter_total.$row;
            $number_to_letter_total2 = $this->number_to_letter($column + 9);
            $merge_end_total = $number_to_letter_total2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_total.":".$merge_end_total);

            $worksheet->SetCellValueByColumnAndRow($column+7, $row,  "TOTAL");
            $number_to_letter = $this->number_to_letter($column+7);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $row++;


            $number_to_letter_active_wovr = $this->number_to_letter($column+1);
            $merge_start_active_wovr = $number_to_letter_active_wovr.$row;
            $number_to_letter_active_wovr2 = $this->number_to_letter($column + 1);
            $merge_end_active_wovr = $number_to_letter_active_wovr2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_active_wovr.":".$merge_end_active_wovr);

            $worksheet->SetCellValueByColumnAndRow($column+10, $row,  "ACTIVE");
            $number_to_letter = $this->number_to_letter($column+10);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_separated_wovr = $this->number_to_letter($column+1);
            $merge_start_separated_wovr = $number_to_letter_separated_wovr.$row;
            $number_to_letter_separated_wovr2 = $this->number_to_letter($column + 1);
            $merge_end_separated_wovr = $number_to_letter_separated_wovr2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_separated_wovr.":".$merge_end_separated_wovr);

            $worksheet->SetCellValueByColumnAndRow($column+11, $row,  "SEPARATED");
            $number_to_letter = $this->number_to_letter($column+11);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_subtotal_wovr = $this->number_to_letter($column+1);
            $merge_start_subtotal_wovr = $number_to_letter_subtotal_wovr.$row;
            $number_to_letter_subtotal_wovr2 = $this->number_to_letter($column + 1);
            $merge_end_subtotal_wovr = $number_to_letter_subtotal_wovr2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_subtotal_wovr.":".$merge_end_subtotal_wovr);

            $worksheet->SetCellValueByColumnAndRow($column+12, $row,  "SUB-TOTAL");
            $number_to_letter = $this->number_to_letter($column+12);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_total_active = $this->number_to_letter($column+1);
            $merge_start_total_active = $number_to_letter_total_active.$row;
            $number_to_letter_total_active2 = $this->number_to_letter($column + 1);
            $merge_end_total_active = $number_to_letter_total_active2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_total_active.":".$merge_end_total_active);

            $worksheet->SetCellValueByColumnAndRow($column+7, $row,  "ACTIVE");
            $number_to_letter = $this->number_to_letter($column+7);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_total_separated = $this->number_to_letter($column+1);
            $merge_start_total_separated = $number_to_letter_total_separated.$row;
            $number_to_letter_total_separated2 = $this->number_to_letter($column + 1);
            $merge_end_total_separated = $number_to_letter_total_separated2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_total_separated.":".$merge_end_total_separated);

            $worksheet->SetCellValueByColumnAndRow($column+8, $row,  "SEPARATED");
            $number_to_letter = $this->number_to_letter($column+8);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_total_subtotal = $this->number_to_letter($column+1);
            $merge_start_total_subtotal = $number_to_letter_total_subtotal.$row;
            $number_to_letter_total_subtotal2 = $this->number_to_letter($column + 1);
            $merge_end_total_subtotal = $number_to_letter_total_subtotal2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_total_subtotal.":".$merge_end_total_subtotal);

            $worksheet->SetCellValueByColumnAndRow($column+9, $row,  "SUB-TOTAL");
            $number_to_letter = $this->number_to_letter($column+9);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_unclaimed_active = $this->number_to_letter($column+1);
            $merge_start_unclaimed_active = $number_to_letter_unclaimed_active.$row;
            $number_to_letter_unclaimed_active2 = $this->number_to_letter($column + 1);
            $merge_end_unclaimed_active = $number_to_letter_unclaimed_active2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_unclaimed_active.":".$merge_end_unclaimed_active);

            $worksheet->SetCellValueByColumnAndRow($column+4, $row,  "ACTIVE");
            $number_to_letter = $this->number_to_letter($column+4);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_unclaimed_separated = $this->number_to_letter($column+1);
            $merge_start_unclaimed_separated = $number_to_letter_unclaimed_separated.$row;
            $number_to_letter_unclaimed_separated2 = $this->number_to_letter($column + 1);
            $merge_end_unclaimed_separated = $number_to_letter_unclaimed_separated2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_unclaimed_separated.":".$merge_end_unclaimed_separated);

            $worksheet->SetCellValueByColumnAndRow($column+5, $row,  "SEPARATED");
            $number_to_letter = $this->number_to_letter($column+5);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_unclaimed_subtotal = $this->number_to_letter($column+1);
            $merge_start_unclaimed_subtotal = $number_to_letter_unclaimed_subtotal.$row;
            $number_to_letter_unclaimed_subtotal2 = $this->number_to_letter($column + 1);
            $merge_end_unclaimed_subtotal = $number_to_letter_unclaimed_subtotal2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_unclaimed_subtotal.":".$merge_end_unclaimed_subtotal);

            $worksheet->SetCellValueByColumnAndRow($column+6, $row,  "SUB-TOTAL");
            $number_to_letter = $this->number_to_letter($column+6);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_claimed_active = $this->number_to_letter($column+1);
            $merge_start_claimed_active = $number_to_letter_claimed_active.$row;
            $number_to_letter_claimed_active2 = $this->number_to_letter($column + 1);
            $merge_end_claimed_active = $number_to_letter_claimed_active2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_claimed_active.":".$merge_end_claimed_active);

            $worksheet->SetCellValueByColumnAndRow($column+1, $row,  "ACTIVE");
            $number_to_letter = $this->number_to_letter($column+1);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_claimed_separated = $this->number_to_letter($column+1);
            $merge_start_claimed_separated = $number_to_letter_claimed_separated.$row;
            $number_to_letter_claimed_separated2 = $this->number_to_letter($column + 1);
            $merge_end_claimed_separated = $number_to_letter_claimed_separated2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_claimed_separated.":".$merge_end_claimed_separated);

            $worksheet->SetCellValueByColumnAndRow($column+2, $row,  "SEPARATED");
            $number_to_letter = $this->number_to_letter($column+2);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);

            $number_to_letter_claimed_subtotal = $this->number_to_letter($column+1);
            $merge_start_claimed_subtotal = $number_to_letter_claimed_subtotal.$row;
            $number_to_letter_claimed_subtotal2 = $this->number_to_letter($column + 1);
            $merge_end_claimed_subtotal = $number_to_letter_claimed_subtotal2.$row;
            $spreadsheet->getActiveSheet()->mergeCells($merge_start_claimed_subtotal.":".$merge_end_claimed_subtotal);

            $worksheet->SetCellValueByColumnAndRow($column+3, $row,  "SUB-TOTAL");
            $number_to_letter = $this->number_to_letter($column+3);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);


        //End For the label

        $numeric_number = 1;
        $record_number = 1;
        $row += 2;
        $row_count = 1;
        $icount_records = 0;
        $counted_rows = 0;
        $rank_name = "";
        $total_per_share = 0;
        $total_amount = 0;
        $subtotal_subscribed = 0;
        $subtotal_active_claimed_wvr = 0;
        $subtotal_separated_claimed_wvr = 0;
        $subtotalright_claimed_wvr = 0;
        $subtotalbottom_claimed_wvr = 0;
        $totalbottom_claimed_wvr = 0;
        $subtotalright_unclaimed_wvr = 0;
        $totalbottom_unclaimed_wvr = 0;
        $subtotal_active_unclaimed_wvr = 0;
        $subtotal_separated_unclaimed_wvr = 0;
        $total_of_total_active_right = 0;
        $total_of_total_separated_right = 0;
        $total_of_total_active_separated = 0;
        $total_of_total_active_bottom = 0;
        $total_of_total_separated_bottom = 0;
        $total_of_total_active_separated_bottom = 0;
        $subtotal_active_wovr = 0;
        $subtotal_separated_wovr = 0;
        $subtotal_active_separated_right_wovr = 0;
        $subtotal_active_separated_bottom_wovr = 0;
        $set_company_name = "";
        $arr_get_total_of_total = array();

        // Start Data
        foreach ($data["esop_user_info"] as $key => $value) {
            foreach ($data["esop_user_info"][$key] as $k => $v) {
             
            if($row_count == 1)
            {
                $icount_records = 1;

                $counted_rows = $row;
                $rank_name = $data["esop_user_info"][$key][$k]["rank_name"];
                $roman_number_esop = $this->romanic_number($numeric_number);
                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-2, $row,  $record_number.".");
                $number_to_letter = $this->number_to_letter($column-2);


                $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-1, $row,  $data["esop_user_info"][$key][$k]["rank_name"]);
                $number_to_letter = $this->number_to_letter($column-1);

                $numeric_number++;

                $row += 2;


            }else{

                if($rank_name != $data["esop_user_info"][$key][$k]["rank_name"])
                {
                    $subtotal_subscribed = 0;
                    $subtotal_active_claimed_wvr = 0;
                    $subtotal_separated_claimed_wvr = 0;
                    $subtotalright_claimed_wvr = 0;
                    $subtotalright_unclaimed_wvr = 0;
                    $subtotalbottom_claimed_wvr = 0;
                    $totalbottom_claimed_wvr = 0;
                    $totalbottom_unclaimed_wvr = 0;
                    $subtotal_active_unclaimed_wvr = 0;
                    $subtotal_separated_unclaimed_wvr = 0;
                    $total_of_total_active_right = 0;
                    $total_of_total_separated_right = 0;
                    $total_of_total_active_separated = 0;
                    $total_of_total_active_bottom = 0;
                    $total_of_total_separated_bottom = 0;
                    $total_of_total_active_separated_bottom = 0;
                    $subtotal_active_wovr = 0;
                    $subtotal_separated_wovr = 0;
                    $subtotal_active_separated_right_wovr = 0;
                    $subtotal_active_separated_bottom_wovr = 0;
                    $row += 2;
                    $counted_rows = $row;

                    $rank_name = $data["esop_user_info"][$key][$k]["rank_name"];
                    $roman_number_esop = $this->romanic_number($numeric_number);
                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-2, $row,  $record_number.".");
                    $number_to_letter = $this->number_to_letter($column-2);


                    $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-1, $row,  $data["esop_user_info"][$key][$k]["rank_name"]);
                    $number_to_letter = $this->number_to_letter($column-1);

                   

                    $row += 2;

                    $icount_records = 0;
                }else{
                    $icount_records++;
                }


            }

            $compute_total_active = 0;

            $set_company_name = $data["esop_user_info"][$key][$k]["company_name"];
            $arr_get_total_of_total[$set_company_name] = $compute_total_active;

           
            foreach ($data["comp_name_temp"] as $tempCompkey => $tempCompvalue) {
                if($tempCompkey == $set_company_name)
                {
                    $total_active = $data["comp_name_temp"][$tempCompkey]["active_total"];
                    $total_separated = $data["comp_name_temp"][$tempCompkey]["separated_total"];

                    $total_active += floatval($data["esop_user_info"][$key][$k]["active_claimed"] + $data["esop_user_info"][$key][$k]["active_unclaimed"]);
                    $total_separated += floatval($data["esop_user_info"][$key][$k]["separated_claimed"] + $data["esop_user_info"][$key][$k]["separated_unclaimed"]);

                   $data["comp_name_temp"][$tempCompkey] = array("active_total"=>$total_active, "separated_total"=>$total_separated);
                   
                }
            }
                        // $total_active_claimed = $arr_comp_name[$arrcompkey]["active_claimed"];
                        // $total_separated_claimed = $arr_comp_name[$arrcompkey]["separated_claimed"];
                        // $total_active_unclaimed = $arr_comp_name[$arrcompkey]["active_unclaimed"];
                        // $total_separated_unclaimed = $arr_comp_name[$arrcompkey]["separated_unclaimed"];
                        // $total_active_unclaimed_wov = $arr_comp_name[$arrcompkey]["active_unclaimed_wov"];
                        // $total_separated_unclaimed_wov = $arr_comp_name[$arrcompkey]["separated_unclaimed_wov"];


                        // $total_active_claimed += floatval($i['active_claimed']);

                        // $total_separated_claimed += floatval($i['separated_claimed']);

                        // $total_active_unclaimed += floatval($i['active_unclaimed']);

                        // $total_separated_unclaimed += floatval($i['separated_unclaimed']);

                        // $total_active_unclaimed_wov += floatval($i['active_unclaimed_wov']);

                        // $total_separated_unclaimed_wov += floatval($i['separated_unclaimed_wov']);


                        // $arr_comp_name[$arrcompkey]["active_claimed"] = $total_active_claimed;
                        // $arr_comp_name[$arrcompkey]["separated_claimed"] = $total_separated_claimed;
                        // $arr_comp_name[$arrcompkey]["active_unclaimed"] = $total_active_unclaimed;
                        // $arr_comp_name[$arrcompkey]["separated_unclaimed"] = $total_separated_unclaimed;
                        // $arr_comp_name[$arrcompkey]["active_unclaimed_wov"] = $total_active_unclaimed_wov;
                        // $arr_comp_name[$arrcompkey]["separated_unclaimed_wov"] = $total_separated_unclaimed_wov;

            
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-1, $row,  $data["esop_user_info"][$key][$k]["company_name"]);
            $number_to_letter = $this->number_to_letter($column-1);

            

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-1, $row+1,  "Sub-total");

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  number_format($data["esop_user_info"][$key][$k]["subscribed"], 2));

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+10, $row,  number_format($data["esop_user_info"][$key][$k]["active_unclaimed_wov"], 2));

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+11, $row,  number_format($data["esop_user_info"][$key][$k]["separated_unclaimed_wov"], 2));

            //SUB-TOTAL SUBSCRIBED
            $subtotal_subscribed += $data["esop_user_info"][$key][$k]["subscribed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row+1,  number_format($subtotal_subscribed, 2));


             //SUB-TOTAL ACTIVE CLAIMED W/ VESTING RIGHTS
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  number_format($data["esop_user_info"][$key][$k]["active_claimed"], 2));
            $number_to_letter = $this->number_to_letter($column+1);
            $subtotal_active_claimed_wvr += $data["esop_user_info"][$key][$k]["active_claimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row+1,  number_format($subtotal_active_claimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+1);

             //SUB-TOTAL SEPARATED CLAIMED W/ VESTING RIGHTS
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $row,  number_format($data["esop_user_info"][$key][$k]["separated_claimed"], 2));
            $number_to_letter = $this->number_to_letter($column+2);
            $subtotal_separated_claimed_wvr += $data["esop_user_info"][$key][$k]["separated_claimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $row+1,  number_format($subtotal_separated_claimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+2);

            //SUB-TOTAL(RIGHT) CLAIMED W/ VESTING RIGHTS
            $subtotalright_claimed_wvr = $data["esop_user_info"][$key][$k]["active_claimed"] + $data["esop_user_info"][$key][$k]["separated_claimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $row,  number_format($subtotalright_claimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+3);

            //SUB-TOTAL(BOTTOM) CLAIMED W/ VESTING RIGHTS
            $totalbottom_claimed_wvr += $subtotalright_claimed_wvr;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $row+1,  number_format($totalbottom_claimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+3);

            //SUB-TOTAL ACTIVE UNCLAIMED W/ VESTING RIGHTS
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $row,  number_format($data["esop_user_info"][$key][$k]["active_unclaimed"], 2));
            $number_to_letter = $this->number_to_letter($column+4);
            $subtotal_active_unclaimed_wvr += $data["esop_user_info"][$key][$k]["active_unclaimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $row+1,  number_format($subtotal_active_unclaimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+4);

            //SUB-TOTAL SEPARATED UNCLAIMED W/ VESTING RIGHTS
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row,  number_format($data["esop_user_info"][$key][$k]["separated_unclaimed"], 2));
            $number_to_letter = $this->number_to_letter($column+5);
            $subtotal_separated_unclaimed_wvr += $data["esop_user_info"][$key][$k]["separated_unclaimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row+1,  number_format($subtotal_separated_unclaimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+5);

            //SUB-TOTAL SEPARATED UNCLAIMED W/ VESTING RIGHTS
            $number_to_letter = $this->number_to_letter($column+5);
            $subtotal_separated_unclaimed_wvr += $data["esop_user_info"][$key][$k]["separated_unclaimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row+1,  number_format($subtotal_separated_unclaimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+5);

            //SUB-TOTAL(RIGHT) UNCLAIMED W/ VESTING RIGHTS
            $subtotalright_unclaimed_wvr = $data["esop_user_info"][$key][$k]["active_unclaimed"] + $data["esop_user_info"][$key][$k]["separated_unclaimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+6, $row,  number_format($subtotalright_unclaimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+6);

            //SUB-TOTAL(BOTTOM) UNCLAIMED W/ VESTING RIGHTS
            $totalbottom_unclaimed_wvr += $subtotalright_unclaimed_wvr;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+6, $row+1,  number_format($totalbottom_unclaimed_wvr, 2));
            $number_to_letter = $this->number_to_letter($column+6);

            //TOTAL(RIGHT) ACTIVE W/ VESTING RIGHTS
            $total_of_total_active_right = $data["esop_user_info"][$key][$k]["active_claimed"] + $data["esop_user_info"][$key][$k]["active_unclaimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+7, $row,  number_format($total_of_total_active_right, 2));
            $number_to_letter = $this->number_to_letter($column+7);

            //TOTAL(RIGHT) SEPARATED W/ VESTING RIGHTS
            $total_of_total_separated_right = $data["esop_user_info"][$key][$k]["separated_claimed"] + $data["esop_user_info"][$key][$k]["separated_unclaimed"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+8, $row,  number_format($total_of_total_separated_right, 2));
            $number_to_letter = $this->number_to_letter($column+8);

            //TOTAL(RIGHT) ACTIVE+SEPARATED W/ VESTING RIGHTS
            $total_of_total_active_separated = $total_of_total_active_right + $total_of_total_separated_right;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+9, $row,  number_format($total_of_total_active_separated, 2));
            $number_to_letter = $this->number_to_letter($column+9);

            //TOTAL(BOTTOM) ACTIVE W/ VESTING RIGHTS
            $total_of_total_active_bottom += $total_of_total_active_right;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+7, $row+1,  number_format($total_of_total_active_bottom, 2));
            $number_to_letter = $this->number_to_letter($column+7);

            //TOTAL(BOTTOM) SEPARATED W/ VESTING RIGHTS
            $total_of_total_separated_bottom += $total_of_total_separated_right;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+8, $row+1,  number_format($total_of_total_separated_bottom, 2));
            $number_to_letter = $this->number_to_letter($column+8);

            //TOTAL(BOTTOM) ACTIVE+SEPARATED W/ VESTING RIGHTS
            $total_of_total_active_separated_bottom += $total_of_total_active_separated;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+9, $row+1,  number_format($total_of_total_active_separated_bottom, 2));
            $number_to_letter = $this->number_to_letter($column+9);

            //TOTAL ACTIVE W/O VESTING RIGHTS
            $subtotal_active_wovr += $data["esop_user_info"][$key][$k]["active_unclaimed_wov"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+10, $row+1,  number_format($subtotal_active_wovr, 2));
            $number_to_letter = $this->number_to_letter($column+10);

            //TOTAL SEPARATED W/O VESTING RIGHTS
            $subtotal_separated_wovr += $data["esop_user_info"][$key][$k]["separated_unclaimed_wov"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+11, $row+1,  number_format($subtotal_separated_wovr, 2));
            $number_to_letter = $this->number_to_letter($column+11);

            //TOTAL(RIGHT) ACTIVE+SEPARATED W/O VESTING RIGHTS
            $subtotal_active_separated_right_wovr = $data["esop_user_info"][$key][$k]["active_unclaimed_wov"]+$data["esop_user_info"][$key][$k]["separated_unclaimed_wov"];
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+12, $row,  number_format($subtotal_active_separated_right_wovr, 2));
            $number_to_letter = $this->number_to_letter($column+12);

            //TOTAL(BOTTOM) ACTIVE+SEPARATED W/O VESTING RIGHTS
            $subtotal_active_separated_bottom_wovr += $subtotal_active_separated_right_wovr;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+12, $row+1,  number_format($subtotal_active_separated_bottom_wovr, 2));
            $number_to_letter = $this->number_to_letter($column+12);

            $row_count++;
            
           
            $row += 1;
        }
        $record_number++;
        }
        $row += 1;


        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-2, $row+1,  $record_number.".");
        $number_to_letter = $this->number_to_letter($column-2);
        $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column-2).$row+1)->applyFromArray($styleBigBold);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-1, $row+1,  "GRAND TOTAL");
        $number_to_letter = $this->number_to_letter($column-1);
        $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column-1).$row+1)->applyFromArray($styleBigBold);
        $row+=1;

        $display_key = array_keys($data['grand_total_subscribed']);
        sort($display_key);

         
        $row+=1;
        $display_row_key = $row;
        $display_row_value = $row;
        $total_of_grand_total = 0;
        foreach ($display_key as $grandkey => $grandvalue) {

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-1, $display_row_key, $display_key[$grandkey]);
            $number_to_letter = $this->number_to_letter($column-1);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row+1)->applyFromArray($styleBigBold);
            
            $display_row_key+=1;
        }
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column-1, $display_row_key, "TOTAL");
        $number_to_letter = $this->number_to_letter($column-1);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$display_row_key)->applyFromArray($styleBigBold);
        ksort($data['grand_total_subscribed']);
       
        foreach ($data['grand_total_subscribed'] as $grandkey2 => $grandvalue2) {

        $total_of_grand_total+= $data['grand_total_subscribed'][$grandkey2];
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $display_row_key, number_format($total_of_grand_total,2));
        $number_to_letter = $this->number_to_letter($column);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$display_row_key)->applyFromArray($styleBigCenterBorder);

        }

        foreach ($data['grand_total_subscribed'] as $grandkey => $grandvalue) {

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $display_row_value, number_format($data['grand_total_subscribed'][$grandkey], 2));
            $number_to_letter = $this->number_to_letter($column);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row+1)->applyFromArray($styleBigBold);
            
             $display_row_value+=1;
        } 
        
        ksort($data['grand_total_wvr']);
        ksort($data["comp_name_temp"]);
        $column_count = $column+1;
        $row_count = $row;
        $total_claimed_active_separated_right = 0;
        $total_unclaimed_active_separated_right = 0;
        $grand_total_of_total_bottom_claimed = 0;
        $grand_total_of_total_bottom_unclaimed = 0;
        $grand_total_of_total_active_wovr = 0;
        $grand_total_of_total_separated_wovr = 0;
        $grand_total_of_total_wovr_right = 0;
        $grand_total_of_total_wovr_bottom = 0;
        $grand_total_of_total_active_bottom = 0;
        $grand_total_of_total_separated_bottom = 0;
        $sub_total_of_total_active_separated_right = 0;
        $sub_total_of_total_active_separated_bottom = 0;

        $total_counted = count($data['grand_total_wvr']);
        $count_rows = 1;

        foreach ($data['grand_total_wvr'] as $gtwkey => $gtwvalue) {


            $total_claimed_active_separated_right = floatval($gtwvalue['active_claimed']) + floatval($gtwvalue['separated_claimed']);
            $total_unclaimed_active_separated_right = floatval($gtwvalue['active_unclaimed']) + floatval($gtwvalue['separated_unclaimed']);

            $grand_total_of_total_bottom_claimed += $total_claimed_active_separated_right;
            $grand_total_of_total_bottom_unclaimed += $total_unclaimed_active_separated_right;

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count, $row_count, number_format($gtwvalue['active_claimed'], 2));

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+1, $row_count, number_format($gtwvalue['separated_claimed'], 2));

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+2, $row_count, number_format($total_claimed_active_separated_right, 2));
            
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+3, $row_count, number_format($gtwvalue['active_unclaimed'], 2));
             
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+4, $row_count, number_format($gtwvalue['separated_unclaimed'], 2));

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+5, $row_count, number_format($total_unclaimed_active_separated_right, 2));

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+9, $row_count, number_format($gtwvalue['active_unclaimed_wov'], 2));

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+10, $row_count, number_format($gtwvalue['separated_unclaimed_wov'], 2));


            $grand_total_of_total_wovr_right = $gtwvalue['active_unclaimed_wov'] + $gtwvalue['separated_unclaimed_wov'];

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+11, $row_count, number_format($grand_total_of_total_wovr_right, 2));

            $grand_total_of_total_wovr_bottom += $grand_total_of_total_wovr_right;

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+11, $row_count+1, number_format($grand_total_of_total_wovr_bottom, 2));
            if ($total_counted == $count_rows) {
               $number_to_letter = $this->number_to_letter($column_count+11);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.($row_count+1))->applyFromArray($styleBigCenterBorder);
            }

            $grand_total_of_total_active_wovr += $gtwvalue['active_unclaimed_wov'];

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+9, $row_count+1, number_format($grand_total_of_total_active_wovr, 2));
            if ($total_counted == $count_rows) {
               $number_to_letter = $this->number_to_letter($column_count+9);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.($row_count+1))->applyFromArray($styleBigCenterBorder);
            }

            $grand_total_of_total_separated_wovr += $gtwvalue['separated_unclaimed_wov'];

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column_count+10, $row_count+1, number_format($grand_total_of_total_separated_wovr, 2));
            if ($total_counted == $count_rows) {
               $number_to_letter = $this->number_to_letter($column_count+10);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.($row_count+1))->applyFromArray($styleBigCenterBorder);
            }

            $row_count++;
            $count_rows++;

            
        }

        $new_total_row =  $row_count;




            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $new_total_row, number_format($data['grand_total_of_total']['active_claimed'],2));
            $number_to_letter = $this->number_to_letter($column+1);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$new_total_row)->applyFromArray($styleBigCenterBorder);
                
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $new_total_row, number_format($data['grand_total_of_total']['separated_claimed'],2));
            $number_to_letter = $this->number_to_letter($column+2);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$new_total_row)->applyFromArray($styleBigCenterBorder);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $new_total_row, number_format($data['grand_total_of_total']['active_unclaimed'],2));
            $number_to_letter = $this->number_to_letter($column+4);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$new_total_row)->applyFromArray($styleBigCenterBorder);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $new_total_row, number_format($data['grand_total_of_total']['separated_unclaimed'],2));
            $number_to_letter = $this->number_to_letter($column+5);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$new_total_row)->applyFromArray($styleBigCenterBorder);  

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $new_total_row, number_format($grand_total_of_total_bottom_claimed,2));
            $number_to_letter = $this->number_to_letter($column+3);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$new_total_row)->applyFromArray($styleBigCenterBorder);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+6, $new_total_row, number_format($grand_total_of_total_bottom_unclaimed,2));
            $number_to_letter = $this->number_to_letter($column+6);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$new_total_row)->applyFromArray($styleBigCenterBorder); 

            $temprow = $row;
            
            $total_countedtemp = count($data['comp_name_temp']);
            $count_rows_temp = 0;
            foreach ($data["comp_name_temp"] as $totalkey => $totalvalue) {
            
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+7, $temprow, number_format($totalvalue['active_total'],2));
           
            $grand_total_of_total_active_bottom += $totalvalue['active_total'];

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+7, $temprow+1, number_format($grand_total_of_total_active_bottom,2));
           

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+8, $temprow, number_format($totalvalue['separated_total'],2));

            $grand_total_of_total_separated_bottom += $totalvalue['separated_total'];

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+8, $temprow+1, number_format($grand_total_of_total_separated_bottom,2));

            $sub_total_of_total_active_separated_right = $totalvalue['active_total'] + $totalvalue['separated_total'];

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+9, $temprow, number_format($sub_total_of_total_active_separated_right,2));

            $sub_total_of_total_active_separated_bottom += $sub_total_of_total_active_separated_right;

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+9, $temprow+1, number_format($sub_total_of_total_active_separated_bottom,2));

            
           $temprow++; 
            $count_rows_temp++;         
            }

            if ($total_countedtemp == $count_rows_temp) {

               $number_to_letter = $this->number_to_letter($column+7);
               $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$temprow)->applyFromArray($styleBigCenterBorder);

               $number_to_letter = $this->number_to_letter($column+8);
               $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$temprow)->applyFromArray($styleBigCenterBorder);

               $number_to_letter = $this->number_to_letter($column+9);
               $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$temprow)->applyFromArray($styleBigCenterBorder);

            }
        // End Data

        // end Build Execl Content



        // return or download excel
         $writer = new PHPExcel_Writer_Excel2007($spreadsheet);
        $writer->save($uploaded_path);

        $file_name = $data['file_name'];
        $file_url = base_url() ."downloads/".$file_name;
        
        return $file_url;
        

        // end return or download excel
    }

    public function extract_claim_summary_excel($data)
    {
        // load excel
        $this->load->library('excel');
        $spreadsheet = new Excel();
        //load excel end

         $uploaded_path = $data['uploaded_path'];



        // build styles

        $styleBigBold = array('font'  => array('bold'  => true));
        $styleBigBoldCenterBorder = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER ),
            'font'  => array('bold'  => true),
            'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN ) )
        );
        $styleCenterBold = array(
            'font' => array('bold'  => true),
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleCenter = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleLeft = array(
            'alignment' => array("horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT )
        );

         //end build styles


        // Build Execl Content
        $column = 0;
        $row = 1;
        $spreadsheet->setActiveSheetIndex(0);
        $number_to_letter = $this->number_to_letter($column);

        $spreadsheet->getActiveSheet()->mergeCells($number_to_letter.$row.':'.$this->nlconvert($column+1).$row);
        $spreadsheet->getActiveSheet()->setCellValue($number_to_letter.$row, "Company Name: ".$data["company_info"]);
        $spreadsheet->getActiveSheet()->getStyle()->applyFromArray($styleBigBold);

        $row++;
        $spreadsheet->getActiveSheet()->mergeCells($number_to_letter.$row.':'.$this->nlconvert($column+2).$row);
        $spreadsheet->getActiveSheet()->setCellValue($number_to_letter.$row, "Esop Name: ".$data["esop_name"]." @ P".$data["price_per_share"]."/Share");
        $spreadsheet->getActiveSheet()->getStyle()->applyFromArray($styleBigBold);

        $row++;

        $spreadsheet->getActiveSheet()->mergeCells($number_to_letter.$row.':'.$this->nlconvert($column+3).$row);
        $spreadsheet->getActiveSheet()->setCellValue($number_to_letter.$row, "DATE GRANTED: ".$data["grant_date"]);
        $spreadsheet->getActiveSheet()->getStyle()->applyFromArray($styleBigBold);

        $row+=3;


        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  '');
        $number_to_letter = $this->number_to_letter($column);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);



        // For the label
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+1), $row,  'ECODE');
        $number_to_letter = $this->number_to_letter($column+1);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);


        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+2), $row,  'LAST NAME');
        $number_to_letter = $this->number_to_letter($column+2);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('c')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row,  'FIRST NAME');
        $number_to_letter = $this->number_to_letter($column+3);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+4), $row,  'MIDDLE NAME');
        $number_to_letter = $this->number_to_letter($column+4);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'DEPARTMENT');
        $number_to_letter = $this->number_to_letter($column+5);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

        

        //End For the label

        $numeric_number = 1;
        $row += 2;
        $row_count = 1;
        $icount_records = 0;
        $counted_rows = 0;
        $rank_name = "";
        $total_per_share = 0;
        $total_amount = 0;
        $emp_count = 1;

        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+3).$row);
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+1), $row,  'CLAIMED/PURCHASED BY ACTIVE EMPLOYEES:');
        $number_to_letter = $this->number_to_letter($column+1);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

        // Start Data
        if(count($data["esop_user_info"]["active_user_claimed"]) > 0)
        {
            $rank_name = $data["esop_user_info"]["active_user_claimed"][0]['rank_name'];

            $arr_gather_all = array();
            foreach ($data["esop_user_info"]["active_user_claimed"] as $key => $value) {
                    $row +=1;

                    if($icount_records == 0)
                    {
                        $row +=1;

                        $roman_number_esop = $this->romanic_number($numeric_number);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                        $number_to_letter = $this->number_to_letter($column);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                        $number_to_letter = $this->number_to_letter($column+1);
                        $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row, $emp_count);
                        $number_to_letter = $this->number_to_letter($column+3);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                        $row +=2;

                        $numeric_number++;

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                        $number_to_letter = $this->number_to_letter($column);

                    }else{

                        if($rank_name != $value["rank_name"])
                        {
                            $emp_count = 1;
                            $row +=1;
                            $rank_name = $value["rank_name"];

                            $roman_number_esop = $this->romanic_number($numeric_number);
                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                            $number_to_letter = $this->number_to_letter($column);
                            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                            $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                            $number_to_letter = $this->number_to_letter($column+1);
                            $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);


                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row, $emp_count);
                            $number_to_letter = $this->number_to_letter($column+3);
                            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                            $row +=2;

                            $numeric_number++;

                            $row_count = 1;

                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                            $number_to_letter = $this->number_to_letter($column);
                        }

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                        $number_to_letter = $this->number_to_letter($column);


                    }

              
                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["employee_code"]);
                    $number_to_letter = $this->number_to_letter($column+1);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $row,  $value["last_name"]);
                    $number_to_letter = $this->number_to_letter($column+2);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $row,  $value["first_name"]);
                    $number_to_letter = $this->number_to_letter($column+3);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $row,  $value["middle_name"]);
                    $number_to_letter = $this->number_to_letter($column+4);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row,  $value["department_name"]);
                    $number_to_letter = $this->number_to_letter($column+5);

                    $new_col_shares = $column+6;
                    $new_col_amount = $column+7;
                    $vesting_year = 1;
                    $count_item = 0;

                   
                    foreach ($value["vesting_info"] as $vestingkey => $vestingvalue) {
                       

                        $rows_for_heads_year = 5;
                        $rows_for_amounts = 6;
                        // Header for share and amount
                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($new_col_shares).$rows_for_heads_year.':'.$this->nlconvert($new_col_amount).$rows_for_heads_year);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($new_col_shares), $rows_for_heads_year,  'YEAR '.$vesting_year);
                        $number_to_letter_one = $this->number_to_letter($new_col_shares);
                         $number_to_letter = $this->number_to_letter($new_col_shares+1);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter_one.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);


                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_shares, $rows_for_amounts,  'NO. OF SHARES');
                        $number_to_letter = $this->number_to_letter($new_col_shares);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_amount, $rows_for_amounts,  'AMOUNT (@ P'.$data["price_per_share"].')');
                        $number_to_letter = $this->number_to_letter($new_col_amount);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        //end Header for share and amount

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_shares, $row,  number_format($vestingvalue["no_of_shares"], 2));
                        $number_to_letter = $this->number_to_letter($new_col_shares);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_amount, $row,  number_format($vestingvalue["total_amount"], 2));
                        $number_to_letter = $this->number_to_letter($new_col_amount);

                        $new_col_shares+=2;
                        $new_col_amount+=2;
                        $vesting_year++;


                        if(count($arr_gather_all) > 0)
                        {
                            $icount = 0;
                            foreach ($arr_gather_all as $sumkey => $sumvalue) {

                                if($sumkey == $vestingkey)
                                {
                                    $icount++;
                                    $arr_gather_all[$vestingkey]["no_of_shares"] = floatval($sumvalue["no_of_shares"]) + floatval($vestingvalue["no_of_shares"]);
                                    $arr_gather_all[$vestingkey]["total_amount"] = floatval($sumvalue["total_amount"]) + floatval($vestingvalue["total_amount"]);
                                }

                            }
                            if($icount==0)
                            {
                                $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                                $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                            }
                        }else{
                            $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                            $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                        }




                    }


                    
                $icount_records++;
                $emp_count++;
                $row_count++;
            }



            

            $row += 2;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'SUB-TOTAL');
            $number_to_letter = $this->number_to_letter($column+5);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

            $shares_col = $column+6;
            $amout_col = $column+7;
            foreach ($arr_gather_all as $shares_amount_key => $shares_amount_value) {

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($shares_col, $row,  number_format($shares_amount_value["no_of_shares"], 2));
                $number_to_letter = $this->number_to_letter($shares_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $shares_col+=2;

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($amout_col, $row,  number_format($shares_amount_value["total_amount"], 2));
                $number_to_letter = $this->number_to_letter($amout_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $amout_col+=2;
            }

            // foreach ($arr_amount as $amountkey => $amountvalue) {

                
            // }
        }


         //**************************** FOR ClAIMED SEPARATED ********************************//
   

        $numeric_number = 1;
        $row += 2;
        $row_count = 1;
        $icount_records = 0;
        $counted_rows = 0;
        $rank_name = "";
        $total_per_share = 0;
        $total_amount = 0;
        $emp_count = 1;

        $arr_gather_all = array();

        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+3).$row);
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+1), $row,  'CLAIMED/PURCHASED BY SEPARATED EMPLOYEES:');
        $number_to_letter = $this->number_to_letter($column+1);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

        // Start Data
        if(count($data["esop_user_info"]["not_active_user_claimed"]) > 0)
        {
            $rank_name = $data["esop_user_info"]["not_active_user_claimed"][0]['rank_name'];
            
            foreach ($data["esop_user_info"]["not_active_user_claimed"] as $key => $value) {
                    $row +=1;

                    if($icount_records == 0)
                    {
                        $row +=1;

                        $roman_number_esop = $this->romanic_number($numeric_number);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                        $number_to_letter = $this->number_to_letter($column);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                        $number_to_letter = $this->number_to_letter($column+1);
                        $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row, $emp_count);
                        $number_to_letter = $this->number_to_letter($column+3);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                        $row +=2;

                        $numeric_number++;

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                        $number_to_letter = $this->number_to_letter($column);

                    }else{

                        if($rank_name != $value["rank_name"])
                        {
                            $emp_count = 1;
                            $row +=1;
                            $rank_name = $value["rank_name"];

                            $roman_number_esop = $this->romanic_number($numeric_number);
                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                            $number_to_letter = $this->number_to_letter($column);
                            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                            $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                            $number_to_letter = $this->number_to_letter($column+1);
                            $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);


                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row, $emp_count);
                            $number_to_letter = $this->number_to_letter($column+3);
                            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                            $row +=2;

                            $numeric_number++;

                            $row_count = 1;

                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                            $number_to_letter = $this->number_to_letter($column);
                        }

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                        $number_to_letter = $this->number_to_letter($column);


                    }

                    

                   

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["employee_code"]);
                    $number_to_letter = $this->number_to_letter($column+1);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $row,  $value["last_name"]);
                    $number_to_letter = $this->number_to_letter($column+2);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $row,  $value["first_name"]);
                    $number_to_letter = $this->number_to_letter($column+3);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $row,  $value["middle_name"]);
                    $number_to_letter = $this->number_to_letter($column+4);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row,  $value["department_name"]);
                    $number_to_letter = $this->number_to_letter($column+5);

                    $new_col_shares = $column+6;
                    $new_col_amount = $column+7;
                    $vesting_year = 1;
                    $count_item = 0;

                   
                    foreach ($value["vesting_info"] as $vestingkey => $vestingvalue) {
                       

                        $rows_for_heads_year = 5;
                        $rows_for_amounts = 6;
                        // Header for share and amount
                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($new_col_shares).$rows_for_heads_year.':'.$this->nlconvert($new_col_amount).$rows_for_heads_year);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($new_col_shares), $rows_for_heads_year,  'YEAR '.$vesting_year);
                        $number_to_letter_one = $this->number_to_letter($new_col_shares);
                         $number_to_letter = $this->number_to_letter($new_col_shares+1);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter_one.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);


                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_shares, $rows_for_amounts,  'NO. OF SHARES');
                        $number_to_letter = $this->number_to_letter($new_col_shares);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_amount, $rows_for_amounts,  'AMOUNT (@ P'.$data["price_per_share"].')');
                        $number_to_letter = $this->number_to_letter($new_col_amount);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        //end Header for share and amount

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_shares, $row,  number_format($vestingvalue["no_of_shares"], 2));
                        $number_to_letter = $this->number_to_letter($new_col_shares);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_amount, $row,  number_format($vestingvalue["total_amount"], 2));
                        $number_to_letter = $this->number_to_letter($new_col_amount);

                        $new_col_shares+=2;
                        $new_col_amount+=2;
                        $vesting_year++;


                        if(count($arr_gather_all) > 0)
                        {
                            $icount = 0;
                            foreach ($arr_gather_all as $sumkey => $sumvalue) {

                                if($sumkey == $vestingkey)
                                {
                                    $icount++;
                                    $arr_gather_all[$vestingkey]["no_of_shares"] = floatval($sumvalue["no_of_shares"]) + floatval($vestingvalue["no_of_shares"]);
                                    $arr_gather_all[$vestingkey]["total_amount"] = floatval($sumvalue["total_amount"]) + floatval($vestingvalue["total_amount"]);
                                }

                            }
                            if($icount==0)
                            {
                                $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                                $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                            }
                        }else{
                            $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                            $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                        }




                    }


                    
                $icount_records++;
                $emp_count++;
                $row_count++;
            }


        }

        $row += 2;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'SUB-TOTAL');
            $number_to_letter = $this->number_to_letter($column+5);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

            $shares_col = $column+6;
            $amout_col = $column+7;
            foreach ($arr_gather_all as $shares_amount_key => $shares_amount_value) {

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($shares_col, $row,  number_format($shares_amount_value["no_of_shares"], 2));
                $number_to_letter = $this->number_to_letter($shares_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $shares_col+=2;

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($amout_col, $row,  number_format($shares_amount_value["total_amount"], 2));
                $number_to_letter = $this->number_to_letter($amout_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $amout_col+=2;
            }





            //**************************** END FOR ClAIMED SEPARATED ********************************//



        //**************************** FOR NOT ClAIMED AND SEPARATED ********************************//
   

        $numeric_number = 1;
        $row += 2;
        $row_count = 1;
        $icount_records = 0;
        $counted_rows = 0;
        $rank_name = "";
        $total_per_share = 0;
        $total_amount = 0;
        $emp_count = 1;

        $arr_gather_all = array();

        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+3).$row);
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+1), $row,  'AVAILABLE SHARES (BACK TO POOL)');
        $number_to_letter = $this->number_to_letter($column+1);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

        // Start Data
        if(count($data["esop_user_info"]["not_active_user_not_claimed"]) > 0)
        {
            $rank_name = $data["esop_user_info"]["not_active_user_not_claimed"][0]['rank_name'];
            
            foreach ($data["esop_user_info"]["not_active_user_not_claimed"] as $key => $value) {
                    $row +=1;

                    if($icount_records == 0)
                    {
                        $row +=1;

                        $roman_number_esop = $this->romanic_number($numeric_number);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                        $number_to_letter = $this->number_to_letter($column);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                        $number_to_letter = $this->number_to_letter($column+1);
                        $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row, $emp_count);
                        $number_to_letter = $this->number_to_letter($column+3);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                        $row +=2;

                        $numeric_number++;

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                        $number_to_letter = $this->number_to_letter($column);

                    }else{

                        if($rank_name != $value["rank_name"])
                        {
                            $emp_count = 1;
                            $row +=1;
                            $rank_name = $value["rank_name"];

                            $roman_number_esop = $this->romanic_number($numeric_number);
                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                            $number_to_letter = $this->number_to_letter($column);
                            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                            $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                            $number_to_letter = $this->number_to_letter($column+1);
                            $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);


                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row, $emp_count);
                            $number_to_letter = $this->number_to_letter($column+3);
                            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                            $row +=2;

                            $numeric_number++;

                            $row_count = 1;

                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                            $number_to_letter = $this->number_to_letter($column);
                        }

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
                        $number_to_letter = $this->number_to_letter($column);


                    }

                    

                   

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["employee_code"]);
                    $number_to_letter = $this->number_to_letter($column+1);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $row,  $value["last_name"]);
                    $number_to_letter = $this->number_to_letter($column+2);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $row,  $value["first_name"]);
                    $number_to_letter = $this->number_to_letter($column+3);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $row,  $value["middle_name"]);
                    $number_to_letter = $this->number_to_letter($column+4);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row,  $value["department_name"]);
                    $number_to_letter = $this->number_to_letter($column+5);

                    $new_col_shares = $column+6;
                    $new_col_amount = $column+7;
                    $vesting_year = 1;
                    $count_item = 0;

                   
                    foreach ($value["vesting_info"] as $vestingkey => $vestingvalue) {
                       

                        $rows_for_heads_year = 5;
                        $rows_for_amounts = 6;
                        // Header for share and amount
                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($new_col_shares).$rows_for_heads_year.':'.$this->nlconvert($new_col_amount).$rows_for_heads_year);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($new_col_shares), $rows_for_heads_year,  'YEAR '.$vesting_year);
                        $number_to_letter_one = $this->number_to_letter($new_col_shares);
                         $number_to_letter = $this->number_to_letter($new_col_shares+1);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter_one.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);


                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_shares, $rows_for_amounts,  'NO. OF SHARES');
                        $number_to_letter = $this->number_to_letter($new_col_shares);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_amount, $rows_for_amounts,  'AMOUNT (@ P'.$data["price_per_share"].')');
                        $number_to_letter = $this->number_to_letter($new_col_amount);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        //end Header for share and amount

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_shares, $row,  number_format($vestingvalue["no_of_shares"], 2));
                        $number_to_letter = $this->number_to_letter($new_col_shares);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_amount, $row,  number_format($vestingvalue["total_amount"], 2));
                        $number_to_letter = $this->number_to_letter($new_col_amount);

                        $new_col_shares+=2;
                        $new_col_amount+=2;
                        $vesting_year++;


                        if(count($arr_gather_all) > 0)
                        {
                            $icount = 0;
                            foreach ($arr_gather_all as $sumkey => $sumvalue) {

                                if($sumkey == $vestingkey)
                                {
                                    $icount++;
                                    $arr_gather_all[$vestingkey]["no_of_shares"] = floatval($sumvalue["no_of_shares"]) + floatval($vestingvalue["no_of_shares"]);
                                    $arr_gather_all[$vestingkey]["total_amount"] = floatval($sumvalue["total_amount"]) + floatval($vestingvalue["total_amount"]);
                                }

                            }
                            if($icount==0)
                            {
                                $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                                $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                            }
                        }else{
                            $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                            $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                        }




                    }


                    
                $icount_records++;
                $emp_count++;
                $row_count++;
            }


        }

        $row += 2;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'SUB-TOTAL');
            $number_to_letter = $this->number_to_letter($column+5);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

            $shares_col = $column+6;
            $amout_col = $column+7;
            foreach ($arr_gather_all as $shares_amount_key => $shares_amount_value) {

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($shares_col, $row,  number_format($shares_amount_value["no_of_shares"], 2));
                $number_to_letter = $this->number_to_letter($shares_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $shares_col+=2;

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($amout_col, $row,  number_format($shares_amount_value["total_amount"], 2));
                $number_to_letter = $this->number_to_letter($amout_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $amout_col+=2;
            }





            //**************************** END FOR ClAIMED SEPARATED ********************************//


        //**************************** FOR ACTIVE BUT NOT YET CLAIMED ********************************//
   

        $numeric_number = 1;
        $row += 2;
        $row_count = 1;
        $icount_records = 0;
        $counted_rows = 0;
        $rank_name = "";
        $rank_name_again = "";
        $total_per_share = 0;
        $total_amount = 0;
        $emp_count = 1;

        $arr_gather_all = array();
        $arr_build_amount_shares = array();

        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+3).$row);
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+1), $row,  'UNCLAIMED SHARES OF ACTIVE EMPLOYEES');
        $number_to_letter = $this->number_to_letter($column+1);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

        // Start Data
        if(count($data["esop_user_info"]["active_user_not_claimed"]) > 0)
        {
            $rank_name = $data["esop_user_info"]["active_user_not_claimed"][0]['rank_name'];
            
            foreach ($data["esop_user_info"]["active_user_not_claimed"] as $key => $value) {
                    

                    if($icount_records == 0)
                    {
                        $row +=1;

                       
                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                        $number_to_letter = $this->number_to_letter($column+1);

                        $numeric_number++;

                    
                    }else{

                        if($rank_name != $value["rank_name"])
                        {
                            $emp_count = 1;
                            $row +=1;
                            $rank_name = $value["rank_name"];

                            $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $rank_name);
                            $number_to_letter = $this->number_to_letter($column+1);
                            
                            $numeric_number++;

                            $row_count = 1;

                        }else{
                            
                        }

                        
                    }



                    $new_col_shares = $column+6;
                    $new_col_amount = $column+7;
                    $vesting_year = 1;
                    $count_item = 0;
                    $shares_sum = 0;
                    $amount_sum = 0;


                   
                    foreach ($value["vesting_info"] as $vestingkey => $vestingvalue) {
                       

                        $rows_for_heads_year = 5;
                        $rows_for_amounts = 6;
                        // Header for share and amount
                        $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($new_col_shares).$rows_for_heads_year.':'.$this->nlconvert($new_col_amount).$rows_for_heads_year);
                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($new_col_shares), $rows_for_heads_year,  'YEAR '.$vesting_year);
                        $number_to_letter_one = $this->number_to_letter($new_col_shares);
                         $number_to_letter = $this->number_to_letter($new_col_shares+1);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter_one.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_heads_year)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);


                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_shares, $rows_for_amounts,  'NO. OF SHARES');
                        $number_to_letter = $this->number_to_letter($new_col_shares);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($new_col_amount, $rows_for_amounts,  'AMOUNT (@ P'.$data["price_per_share"].')');
                        $number_to_letter = $this->number_to_letter($new_col_amount);
                        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$rows_for_amounts)->applyFromArray($styleBigBoldCenterBorder);
                        $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                        //end Header for share and amount


                        $this_col_shares_not_yet_claim = $new_col_shares;
                        $this_col_amount_not_yet_claim = $new_col_amount;



                        if($icount_records == 0)
                        {
                           
                            $arr_build_amount_shares[$vestingkey]["per_share"] = $vestingvalue["no_of_shares"];
                            $arr_build_amount_shares[$vestingkey]["per_amount"] = $vestingvalue["total_amount"];

                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($this_col_shares_not_yet_claim, $row,  number_format($vestingvalue["no_of_shares"], 2));
                            $number_to_letter = $this->number_to_letter($this_col_shares_not_yet_claim);

                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($this_col_amount_not_yet_claim, $row,  number_format($vestingvalue["total_amount"],2));
                            $number_to_letter = $this->number_to_letter($this_col_amount_not_yet_claim);

                        }else{


                            if($rank_name_again != $value["rank_name"])
                            {
                               
                                $shares_sum = floatval($vestingvalue["no_of_shares"]);
                                $amount_sum = floatval($vestingvalue["total_amount"]);

                                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($this_col_shares_not_yet_claim, $row,  number_format($shares_sum, 2));
                                $number_to_letter = $this->number_to_letter($this_col_shares_not_yet_claim);

                                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($this_col_amount_not_yet_claim, $row,  number_format($amount_sum, 2));
                                $number_to_letter = $this->number_to_letter($this_col_amount_not_yet_claim);


                            }else if($rank_name == $value["rank_name"]){

                                if(count($arr_build_amount_shares) > 0)
                                {
                                    foreach ($arr_build_amount_shares as $computekey => $computevalue) {
                                       if($vestingkey == $computekey)
                                       {
                                            $arr_build_amount_shares[$computekey]["per_share"] = floatval($computevalue["per_share"]) + floatval($vestingvalue["no_of_shares"]);
                                            $arr_build_amount_shares[$computekey]["per_amount"] = floatval($computevalue["per_amount"]) + floatval($vestingvalue["total_amount"]);

                                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($this_col_shares_not_yet_claim, $row,  number_format($arr_build_amount_shares[$computekey]["per_share"], 2));
                                            $number_to_letter = $this->number_to_letter($this_col_shares_not_yet_claim);

                                            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($this_col_amount_not_yet_claim, $row,  number_format($arr_build_amount_shares[$computekey]["per_amount"], 2));
                                            $number_to_letter = $this->number_to_letter($this_col_amount_not_yet_claim);
                                       }
                                    }
                                }

                            }

                        }


                        

                        

                        $new_col_shares+=2;
                        $new_col_amount+=2;
                        $vesting_year++;


                        if(count($arr_gather_all) > 0)
                        {
                            $icount = 0;
                            foreach ($arr_gather_all as $sumkey => $sumvalue) {

                                if($sumkey == $vestingkey)
                                {
                                    $icount++;
                                    $arr_gather_all[$vestingkey]["no_of_shares"] = floatval($sumvalue["no_of_shares"]) + floatval($vestingvalue["no_of_shares"]);
                                    $arr_gather_all[$vestingkey]["total_amount"] = floatval($sumvalue["total_amount"]) + floatval($vestingvalue["total_amount"]);
                                }

                            }
                            if($icount==0)
                            {
                                $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                                $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                            }
                        }else{
                            $arr_gather_all[$vestingkey]["no_of_shares"] = $vestingvalue["no_of_shares"];
                            $arr_gather_all[$vestingkey]["total_amount"] = $vestingvalue["total_amount"];
                        }




                    }

                    $rank_name_again = $rank_name;
                    
                $icount_records++;
                $emp_count++;
                $row_count++;
            }


        }

        $row += 2;
            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'SUB-TOTAL');
            $number_to_letter = $this->number_to_letter($column+5);
            $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

            $shares_col = $column+6;
            $amout_col = $column+7;
            foreach ($arr_gather_all as $shares_amount_key => $shares_amount_value) {

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($shares_col, $row,  number_format($shares_amount_value["no_of_shares"], 2));
                $number_to_letter = $this->number_to_letter($shares_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $shares_col+=2;

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($amout_col, $row,  number_format($shares_amount_value["total_amount"], 2));
                $number_to_letter = $this->number_to_letter($amout_col);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
                $spreadsheet->getActiveSheet()->getColumnDimension($number_to_letter)->setWidth(20);

                $amout_col+=2;
            }





            //**************************** END FOR ACTIVE BUT NOT YET CLAIMED ********************************//




      

        //     if($row_count == 1)
        //     {
        //         $icount_records = 1;

        //         $counted_rows = $row;
        //         $rank_name = $value["rank_name"];
        //         $roman_number_esop = $this->romanic_number($numeric_number);
        //         $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
        //         $number_to_letter = $this->number_to_letter($column);
        //         $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


        //         $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
        //         $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["rank_name"]);
        //         $number_to_letter = $this->number_to_letter($column+1);
        //         $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);

        //         $numeric_number++;

        //         $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $counted_rows,  $icount_records);
        //         $number_to_letter = $this->number_to_letter($column+3);
        //         $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

        //         $row += 2;

        //     }else{

        //         if($rank_name != $value["rank_name"])
        //         {
                    

        //             $row += 2;
        //             $counted_rows = $row;

        //             $rank_name = $value["rank_name"];
        //             $roman_number_esop = $this->romanic_number($numeric_number);
        //             $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
        //             $number_to_letter = $this->number_to_letter($column);
        //             $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


        //             $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
        //             $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["rank_name"]);
        //             $number_to_letter = $this->number_to_letter($column+1);
        //             $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);

        //             $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $counted_rows,  $icount_records);
        //             $number_to_letter = $this->number_to_letter($column+3);
        //             $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

        //             $row += 2;

        //             $icount_records = 0;
        //         }else{
        //             $icount_records++;
        //         }


        //     }

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
        //     $number_to_letter = $this->number_to_letter($column);

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["employee_code"]);
        //     $number_to_letter = $this->number_to_letter($column+1);

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $row,  $value["last_name"]);
        //     $number_to_letter = $this->number_to_letter($column+2);

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $row,  $value["first_name"]);
        //     $number_to_letter = $this->number_to_letter($column+3);

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $row,  $value["middle_name"]);
        //     $number_to_letter = $this->number_to_letter($column+4);

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row,  $value["department_name"]);
        //     $number_to_letter = $this->number_to_letter($column+5);

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+6, $row,  number_format($value["num_shares"], 2));
        //     $number_to_letter = $this->number_to_letter($column+6);

        //     $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+7, $row,  number_format($value["total_share"], 2));
        //     $number_to_letter = $this->number_to_letter($column+7);

        //     $total_per_share += $value["num_shares"];
        //     $total_amount += $value["total_share"];


        //     $row_count++;
           
        //     $row += 1;

        // }
        // $row += 1;
        // $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'SUB-TOTAL');
        // $number_to_letter = $this->number_to_letter($column+5);
        // $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
        // $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

        // $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+6), $row,  number_format($total_per_share, 2));
        // $number_to_letter = $this->number_to_letter($column+6);
        // $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        // $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);

        // $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+7), $row,  number_format($total_amount, 2));
        // $number_to_letter = $this->number_to_letter($column+7);
        // $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        // $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);



         

        // End Data



        // end Build Execl Content



        // return or download excel
         $writer = new PHPExcel_Writer_Excel2007($spreadsheet);
        $writer->save($uploaded_path);

        $file_name = $data['file_name'];
        $file_url = base_url() ."downloads/".$file_name;
        
        return $file_url;
        

        // end return or download excel
    }

    public function extract_esop_summary_excel($data)
    {
        // load excel
        $this->load->library('excel');
        $spreadsheet = new Excel();
        //load excel end

         $uploaded_path = $data['uploaded_path'];



        // build styles

        $styleBigBold = array('font'  => array('bold'  => true));
        $styleBigBoldCenterBorder = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER ),
            'font'  => array('bold'  => true),
            'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN ) )
        );
        $styleCenterBold = array(
            'font' => array('bold'  => true),
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleCenter = array(
            'alignment' => array( "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER )
        );
        $styleLeft = array(
            'alignment' => array("horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT )
        );

         //end build styles


        // Build Execl Content
        $column = 0;
        $row = 1;
        $spreadsheet->setActiveSheetIndex(0);
        $number_to_letter = $this->number_to_letter($column);

        $spreadsheet->getActiveSheet()->mergeCells($number_to_letter.$row.':'.$this->nlconvert($column+1).$row);
        $spreadsheet->getActiveSheet()->setCellValue($number_to_letter.$row, $data["company_info"]);
        $spreadsheet->getActiveSheet()->getStyle()->applyFromArray($styleBigBold);

        $row++;
        $spreadsheet->getActiveSheet()->mergeCells($number_to_letter.$row.':'.$this->nlconvert($column+2).$row);
        $spreadsheet->getActiveSheet()->setCellValue($number_to_letter.$row, $data["esop_name"]." @ P".$data["price_per_share"]."/Share");
        $spreadsheet->getActiveSheet()->getStyle()->applyFromArray($styleBigBold);

        $row++;

        $spreadsheet->getActiveSheet()->mergeCells($number_to_letter.$row.':'.$this->nlconvert($column+3).$row);
        $spreadsheet->getActiveSheet()->setCellValue($number_to_letter.$row, "DATE GRANTED: ".$data["grant_date"]);
        $spreadsheet->getActiveSheet()->getStyle()->applyFromArray($styleBigBold);

        $row+=3;


        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  '');
        $number_to_letter = $this->number_to_letter($column);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);



        // For the label
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+1), $row,  'ECODE');
        $number_to_letter = $this->number_to_letter($column+1);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);


        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+2), $row,  'LAST NAME');
        $number_to_letter = $this->number_to_letter($column+2);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('c')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $row,  'FIRST NAME');
        $number_to_letter = $this->number_to_letter($column+3);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+4), $row,  'MIDDLE NAME');
        $number_to_letter = $this->number_to_letter($column+4);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'DEPARTMENT');
        $number_to_letter = $this->number_to_letter($column+5);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+6), $row,  'NO. OF SHARES');
        $number_to_letter = $this->number_to_letter($column+6);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+7), $row,  'AMOUNT (@ P'.$data["price_per_share"].')');
        $number_to_letter = $this->number_to_letter($column+7);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        //End For the label

        $numeric_number = 1;
        $row += 2;
        $row_count = 1;
        $icount_records = 0;
        $counted_rows = 0;
        $rank_name = "";
        $total_per_share = 0;
        $total_amount = 0;


            // $roman_number_esop = $this->romanic_number($numeric_number);
            // $worksheet->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
            // $number_to_letter = $this->number_to_letter($column);
            // $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
            // $column++;
        // Start Data
        foreach ($data["esop_user_info"] as $key => $value) {

            if($row_count == 1)
            {
                $icount_records = 1;

                $counted_rows = $row;
                $rank_name = $value["rank_name"];
                $roman_number_esop = $this->romanic_number($numeric_number);
                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                $number_to_letter = $this->number_to_letter($column);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["rank_name"]);
                $number_to_letter = $this->number_to_letter($column+1);
                $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);

                $numeric_number++;

                $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $counted_rows,  $icount_records);
                $number_to_letter = $this->number_to_letter($column+3);
                $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                $row += 2;

            }else{

                if($rank_name != $value["rank_name"])
                {
                    

                    $row += 2;
                    $counted_rows = $row;

                    $rank_name = $value["rank_name"];
                    $roman_number_esop = $this->romanic_number($numeric_number);
                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $roman_number_esop.".");
                    $number_to_letter = $this->number_to_letter($column);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);


                    $spreadsheet->getActiveSheet()->mergeCells($this->nlconvert($column+1).$row.':'.$this->nlconvert($column+2).$row);
                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["rank_name"]);
                    $number_to_letter = $this->number_to_letter($column+1);
                    $spreadsheet->getActiveSheet()->getStyle($this->nlconvert($column+1).$row)->applyFromArray($styleBigBold);

                    $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+3), $counted_rows,  $icount_records);
                    $number_to_letter = $this->number_to_letter($column+3);
                    $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);

                    $row += 2;

                    $icount_records = 0;
                }else{
                    $icount_records++;
                }


            }

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column, $row,  $row_count);
            $number_to_letter = $this->number_to_letter($column);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+1, $row,  $value["employee_code"]);
            $number_to_letter = $this->number_to_letter($column+1);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+2, $row,  $value["last_name"]);
            $number_to_letter = $this->number_to_letter($column+2);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+3, $row,  $value["first_name"]);
            $number_to_letter = $this->number_to_letter($column+3);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+4, $row,  $value["middle_name"]);
            $number_to_letter = $this->number_to_letter($column+4);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+5, $row,  $value["department_name"]);
            $number_to_letter = $this->number_to_letter($column+5);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+6, $row,  number_format($value["num_shares"], 2));
            $number_to_letter = $this->number_to_letter($column+6);

            $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow($column+7, $row,  number_format($value["total_share"], 2));
            $number_to_letter = $this->number_to_letter($column+7);

            $total_per_share += $value["num_shares"];
            $total_amount += $value["total_share"];


            $row_count++;
           
            $row += 1;

        }
        $row += 1;
        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+5), $row,  'SUB-TOTAL');
        $number_to_letter = $this->number_to_letter($column+5);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBold);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+6), $row,  number_format($total_per_share, 2));
        $number_to_letter = $this->number_to_letter($column+6);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);

        $spreadsheet->getActiveSheet()->SetCellValueByColumnAndRow(($column+7), $row,  number_format($total_amount, 2));
        $number_to_letter = $this->number_to_letter($column+7);
        $spreadsheet->getActiveSheet()->getStyle($number_to_letter.$row)->applyFromArray($styleBigBoldCenterBorder);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);



         

        // End Data



        // end Build Execl Content



        // return or download excel
         $writer = new PHPExcel_Writer_Excel2007($spreadsheet);
        $writer->save($uploaded_path);

        $file_name = $data['file_name'];
        $file_url = base_url() ."downloads/".$file_name;
        
        return $file_url;
        

        // end return or download excel
    }



    public function nlconvert($col)
    {
        $num_to_let =  $this->number_to_letter($col);
         return $num_to_let;
    }

    public function getShareSummary(){

        $data = $this->Reports_model->getShareSummary();

            foreach ($data as $k => $v) {
                $getTotalAmount = $this->Reports_model->getShareSummary();
                $data[$k]['total_amount_active'] = $getTotalAmount[0]['total_active'];

            }
            foreach ($data as $k => $v) {
                $getTotalAmount = $this->Reports_model->getShareSummary();
                $data[$k]['total_amount_separated'] = $getTotalAmount[0]['total_active'];

            }
    }



}

/* End of file reports.php */
/* Location: ./application/modules/reports/controllers/reports.php */