
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:400,100,400italic,300,500,700);
@font-face{ 
	font-family: 'Roboto', sans-serif;
	src:url(https://fonts.googleapis.com/css?family=Roboto:400,100,400italic,300,500,700);}



</style>


<section section-style="">	

	<div class="pnl-parent text-center  " style="text-align:center;width:900px;margin:0 auto">
		<div  style="text-align:left; width: 750px;padding:30px;background-color:#fff;min-height:250px;margin:0 auto;display:inline-block;vertical-align:middle;border:solid #2b70c0 25px">
			<img src="http://moonstone.cr8vwebsolutions.net/rhi-esop/assets/images/roxas-holdings-logo.png" alt="">
			<h1 class="font-family:'Roboto',sans-serif;font-weight:400;font-size:30px;margin-bottom:30px;margin-top:30px;text-indent:20px;color:#545454">
				Hi <span><?php echo $fullname;?></span>,
			</h1>
			<h2  style="padding-left:50px;font-family:'Roboto',sans-serif;font-weight:400;font-size:23px;width:700px;color:#333;margin:0 auto">
				<?php echo $message;?>
			</h2>
		</div>
	</div>
</section>
