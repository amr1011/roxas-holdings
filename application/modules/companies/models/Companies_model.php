<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies_model extends CI_Model {

    public function get_company_data ($qualifiers = array())
    {
        $result = array();
        $sql = "SELECT c.*, 
            (SELECT COUNT(d.id) from department d WHERE c.id = d.company_id ) as department_count 
            FROM company c 
            LEFT JOIN department d ON c.id = d.company_id 
            WHERE c.is_deleted = 0 ";
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'department_count')
            {
                $sql .= " AND (SELECT COUNT(d.id) from department d WHERE c.id = d.company_id ) = '".$qualifiers['keyword']."' ";
            }
            else if($qualifiers['search_field'] == 'c.name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "c.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY c.id,department_count ";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function insert_company_information($company = array())
    {
        if(is_array($company) AND count($company) > 0)
        {
            $this->db->insert("company", $company);
            return $this->db->insert_id();
        }
    }

    public function update_company_information($qualifier = array(), $data = array())
    {
        if(count($qualifier) > 0 AND count($data) > 0)
        {
            $sql = "UPDATE company c SET ";
            foreach($data as $i => $update)
            {
                if(isset($i) AND strlen($i) > 0 AND isset($update) AND strlen($update) > 0 )
                {
                    $length = count($data) - 1;
                    $sql .= " ".$i." = '".$update."' ";

                    if($i != $length )
                    {
                        $sql .= ",";
                    }
                }
            }
            $sql = rtrim($sql, ",");
            $sql .= " WHERE 1 ";

            foreach($qualifier as $i => $where)
            {
                if(isset($where['field']) AND strlen($where['value']) > 0 AND isset($where['field']) AND strlen($where['value']) > 0  AND isset($where['operator']))
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";

                }
            }
        }

        $query = $this->db->query($sql);

        return $this->db->affected_rows();
    }

    public function check_existing_data($qualifiers = array(), $table = '')
    {
        if(count($qualifiers) > 0 AND strlen($table) >0)
        {
            $sql = "SELECT
                    *
                    FROM {$table}
                    WHERE TRUE";

            if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
            {
                foreach($qualifiers['where'] as $key => $where)
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                }
            }

            $query = $this->db->query($sql);
            return $query->num_rows();
        }
        else
        {
            return 0;
        }
    }

    public function get_company_names($company_id)
    {
        $sql = "SELECT id, name
                FROM company
                WHERE id='{$company_id}'
                ";
        $query = $this->db->query($sql);
        return $query->result_array();

    }


}