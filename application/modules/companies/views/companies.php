<section section-style="top-panel">
	<div class="content">
		<div>
			<!-- <h1 class="f-left hidden">ESOP View</h1> -->
			<h1 class="f-left">Company List</h1>
			<button class="btn-normal f-right modal-trigger" modal-target="add-company">Add Company</button>
			<div class="clear"></div>
		</div>
		
		<div>
			<p class="white-color margin-bottom-5 margin-left-5">Search</p>
			<div class="select add-radius"  id="search_field_dropdown">
				<select>
					<option value="Company Name">Company Name</option>
					<option value="Company Code">Company Code</option>
					<option value="No. of Departments">No. of Departments</option>					
				</select>
			</div>
			<input type="text" class="small add-border-radius-5px margin-left-10" id="search_company_input">
			<button class="btn-normal display-inline-mid margin-left-10" id="search_company_btn">Search</button>
		</div>
	
			
		<div class="text-right-line margin-top-10">
			<div class="line"></div>
			<div class="content-text" id="sort_company">
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="#" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-company-name">Company Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="#" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-company-dept-count">No. of Departments <i class="fa fa-chevron-down"></i></a> </p>					
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th></th>
						<th>Company Code</th>
						<th>Company Name</th>
						<th>No. of Departments</th>
						<th></th>
					</tr>
				</thead>
				<tbody data-container="company_list_container">
					<tr class="company template">
						<td class="padding-0px" data-label="company_image"><img src="<?php echo base_url(); ?>/assets/images/mountain.png" class="width-300px max-h200px height-200px"></td>
						<td data-label="company_code"><!-- 1010 --></td>
						<td data-label="company_name"><!-- Cr8v Web Solutions, Inc. --></td>
						<td data-label="company_department_count">0</td>
						<td><a href="javascript:void(0)" class="view_departments">View Company</a></td><!-- <?php echo base_url();?>departments/department_list -->
					</tr>
					<tr class="no_results_found template hidden">
						<td colspan="5" class="text-center">No Results found</td><!-- <?php echo base_url();?>departments/department_list -->
					</tr>
					<!-- <tr>
						<td class="padding-0px"><img src="<?php echo base_url(); ?>/assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td class="padding-0px"><img src="<?php echo base_url(); ?>/assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td class="padding-0px"><img src="<?php echo base_url(); ?>/assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td><img src="<?php echo base_url(); ?>/assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr>
					<tr>
						<td><img src="<?php echo base_url(); ?>/assets/images/mountain.png"></td>
						<td>1010</td>
						<td>Cr8v Web Solutions, Inc.</td>
						<td>10</td>
						<td><a href="COMPANY-LIST-view.php">View Company</a></td>
					</tr> -->
					<tr class="last-content">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-company">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD COMPANY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content  padding-left-0 padding-right-0">
			<div class="error add_company_error_message hidden margin-bottom-10">
			</div>
			<div class="success add_company_success_message hidden margin-bottom-10">
			</div>

			<div class="upload-photo upload-photo-modal position-rel width-150px height-150px" data-container="image_container">
				<div class="width-100percent">
					<i class="fa fa-camera fa-3x"></i>
					<p class="margin-top-10 ">Upload Photo</p>
				</div>
				<img class="width-100percent position-abs height-100percent top-0 left-0">
			</div>
			<label>
	            <input type="file" name="company_image" class="hidden" /> 
			</label>

			<form id="add_company_form">
				<table class="width-100percent margin-top-10">
					<tbody>
						<tr>
							<td class="text-right padding-right-10">Company Code: <span class="red-color">*</span></td>
							<td class="text-center"><input type="text" class="normal code-generated" value="" name="company_code" datavalid="required" labelinput="Company Code"/></td>
						</tr>
						<tr>
							<td class="text-right padding-right-10">Company Name: <span class="red-color">*</span></td>
							<td class="text-center">
								<input type="text" class="normal" name="company_name" datavalid="required" labelinput="Company Name"/>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal" id="add_company_btn">Add Company</button>
		</div>
		<div class="clear"></div>
	</div>
</div>