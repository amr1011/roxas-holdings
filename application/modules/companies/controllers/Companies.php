<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }

        $this->load->model('Companies/Companies_model');

        $allowed = $this->check_user_role();

        if(!$allowed)
        {
            // header('Location: '.base_url().'esop');
            header('Location: '.base_url().'auth/forbidden_403');
        }
    }

    public function index ()
    {
        header('Location: '.base_url().'companies/company_list');
    }
    
    public function company_list ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/companies.js' );
        $this->template->add_content( $this->load->view( 'companies', $data, TRUE ) );
        $this->template->draw();
    }

    public function get ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'name' => 'c.name',
                    'company_code' => 'c.company_code',
                    'department_count' => 'department_count',
                    'date_created' => 'c.date_created',
                    'id' => 'c.id',
                    'is_deleted' => 'c.is_deleted',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "c.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;
            $companies = $this->Companies_model->get_company_data($qualifiers);

            if(count($companies) > 0)
            {
                $response['total_rows'] = $companies['total_rows'];
                unset($companies['total_rows']);
                $response['data'] = $companies;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function add ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $company_information = $this->input->post();

        if(isset($company_information) && count($company_information) > 0)
        {
            /*validate company code*/
            if(isset($company_information['company_code']) AND strlen($company_information['company_code']) > 0)
            {
                if(strlen($company_information['company_code']) > 0 AND preg_match("/^[0-9]+$/", trim($company_information['company_code'])) )
                {
                    $company_information_input['company_code'] = $company_information['company_code'];
                }
                else
                {
                    $response['message'][] = "Invalid company code or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Company Code is required.";
                $error++; 
            }

            /*validate company name*/
            if(isset($company_information['name']) AND strlen($company_information['name']) > 0)
            {
                if(strlen($company_information['name']) > 1 AND preg_match("/[a-z||A-Z]/", trim($company_information['name'])) )
                {
                    if(preg_match("/^[a-zA-Z0-9\-\.\&\,\/ ]+$/", trim($company_information['name'])) )
                    {
                        $company_information_input['name'] = $company_information['name'];
                    }
                    else
                    {
                        $response['message'][] = "Invalid company name or is too short";
                        $error++;
                    }
                }
                else
                {
                    $response['message'][] = "Invalid company name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Company Name is required.";
                $error++; 
            }

            $company_information_input['img'] = isset($company_information['img']) ? $company_information['img'] : '' ;
            $company_information_input['is_deleted'] = 0;
            $company_information_input['date_created'] = date('Y-m-d H:i:s');
            
            if($error == 0)
            {
                /*validate company code if existing*/
                $check_existing_qualifiers = array(
                    'where' => array(
                        array(
                            'field' => 'company_code',
                            'operator' => '=',
                            'value' => "'".$company_information['company_code']."'",
                        )
                    )
                );
                $existing_company_code = $this->Companies_model->check_existing_data($check_existing_qualifiers, 'company');

                if($existing_company_code > 0)
                {
                    $response['message'][] = "Company Code is already existing.";
                    $error++;
                }

                /*insert if all inputs are okay*/
                if($existing_company_code == 0 && $error == 0)
                {
                    $added_company_information_id = $this->Companies_model->insert_company_information($company_information_input);

                    if($added_company_information_id > 0)
                    {
                        $response['message'][] = "Company added successfully.";
                        $response['status'] = TRUE;
                        $response['data'] = $company_information_input;
                        $response['data']['id'] = $added_company_information_id;

                        /*for logs*/
                        $_POST['params'] = $response['data'];
                        $_POST['status'] = TRUE;
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function edit ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $company_information = $this->input->post();

        if(isset($company_information) && count($company_information) > 0)
        {
            if(isset($company_information['data']) AND is_array($company_information['data']) AND count($company_information['data']) > 0 AND isset($company_information['id']) AND $company_information['id'] > 0)
            {
                foreach ( $company_information['data'][0] as $i => $update_data )
                {
                    if ( strlen( $i ) > 0 )
                    {
                        /*validate fields*/

                        /*company name validation*/
                        if ($i == 'name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1 AND preg_match("/[a-z||A-Z]/", trim($update_data)))
                                {
                                    if(preg_match("/^[a-zA-Z0-9\-\.\&\,\/ ]+$/", trim($update_data)))
                                    {
                                        $company_information_input['name'] = $update_data;
                                    }
                                    else
                                    {
                                        $response['message'][] = "Company name invalid or is too short";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Company name invalid or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Company name is required";
                                $error ++;
                            }
                        }

                        /*company code validation*/
                        if ($i == 'company_code')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    /*validate company code if existing*/
                                    $check_existing_qualifiers = array(
                                        'where' => array(
                                            array(
                                                'field' => 'company_code',
                                                'operator' => '=',
                                                'value' => "'".$update_data."'",
                                            ),
                                            array(
                                                'field' => 'id',
                                                'operator' => '!=',
                                                'value' => "'".$company_information['id']."'",
                                            ),
                                        )
                                    );
                                    $existing_company_code = $this->Companies_model->check_existing_data($check_existing_qualifiers, 'company');

                                    if($existing_company_code > 0)
                                    {
                                        $response['message'][] = "Company Code is already existing.";
                                        $error++;
                                    }
                                    else
                                    {
                                        $company_information_input['company_code'] = $update_data;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Company code invalid or is too short";
                                    $error ++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Company code is required";
                                $error ++;
                            }
                        }

                        /*image for return*/
                        if ($i == 'img')
                        {
                            $company_information_input['img'] = $update_data;
                        }
                    }
                }

                if($error == 0)
                {
                    /*update if all inputs are okay*/
                    if($error == 0)
                    {
                        $logs_from = $this->Companies_model->get_company_data(
                                array(
                                    'where' => array(
                                        array(
                                            'field' => 'c.id',
                                            'operator' => '=',
                                            'value' => $company_information['id'],
                                        )
                                    )
                                )
                            );
                        $updated_company_information_id = $this->Companies_model->update_company_information(
                            array(
                                array(
                                'field' => 'c.id',
                                'value' => $company_information['id'],
                                'operator' => '='
                                )
                            ), 
                            $company_information['data'][0]);

                        if($updated_company_information_id > 0)
                        {
                            $response['message'][] = "Company updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $company_information_input;
                            $response['data']['id'] = $company_information['id'];

                            /*for logs*/
                            $_POST['params']['id'] = $company_information['id'];
                            $_POST['params']['to'] = $response['data'];
                            $_POST['params']['from'] = $logs_from[0];
                            $_POST['params']['diff'] = array_diff_assoc($_POST['params']['to'], $_POST['params']['from']);
                            $_POST['status'] = TRUE;
                        }
                        else
                        {
                            // $response['message'][] = "Nothing is changed.";
                            $response['message'][] = "Company updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $company_information_input;
                            $response['data']['id'] = $company_information['id'];
                        }
                        $response['data']['affected_rows'] = $updated_company_information_id;
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function ajax_upload_image()
    {
        $return = array();

        $path = './assets/images/companies';

        if(!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path,0777,TRUE);
        }
        // else
        // {
        //     echo json_encode(array('status'=>FALSE, 'message'=>'Error updating image, please try again later.', 'data'=>array()));
        //     exit();
        // }

        echo json_encode($this->_upload_image($_FILES));
    }

    private function _upload_image($file)
    {
        $_FILES = $file;

        $file_name = uniqid( TRUE );

        if ( ! isset( $_FILES['client_img']['name'] ) )
        {
            return array( 'status' => FALSE );
        }

        $file       = pathinfo( $_FILES['client_img']['name'] );
        $extensions = array( 'jpg', 'jpeg', 'gif', 'png' );
        // kprint($file); exit;
        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ) )
        {

            //get width and height of selected image
            list( $width, $height ) = getimagesize( $_FILES['client_img']['tmp_name'] );

            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.' . $file['extension'];

            if(move_uploaded_file($_FILES['client_img']['tmp_name'], './assets/images/companies/' . $file_name))
            {
                $data['image_height'] = $height;
                $data['image_width'] = $width;
                $data['image_name'] = $file_name;
                $data['image_path'] = /*base_url() . */'/assets/images/companies/' . $file_name;

                return array('status'=>TRUE, 'message'=>'Image file successfully updated!', 'data'=>$data);
            }
            else
            {
                return array('status'=>FALSE, 'message'=>'Error adding image file, please try again later.', 'data'=>array());
            }

        }
        else
        {
            return array('status'=>FALSE, 'message'=>'Invalid image file, please try again.', 'data'=>array());
        }
    }


}

/* End of file companies.php */
/* Location: ./application/modules/companies/controllers/companies.php */