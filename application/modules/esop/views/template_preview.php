<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left hidden">ESOP View</h1>
			<div class="breadcrumbs margin-bottom-20 border-10px">
				<a href="<?php echo base_url(); ?>esop/esop_list">ESOP</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="<?php echo base_url(); ?>esop/view_esop">ESOP 1</a>
				<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
				<a href="esop-check.php">Share Distribution Template</a>
			</div>
			<div class="f-right">
				<a href="<?php echo base_url(); ?>esop/view_esop">
					<button class="btn-normal margin-right-10">Cancel Upload</button>
				</a>	
				<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Another Template</button>
				<a href="ESOP-view-esop-2.php">
					<button class="btn-normal">Confirm Upload</button>				
				</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section section-style="content-panel">
	
	<div class="content">
	
		<h2 class="f-left">Preview of ESOP_Template.xls</h2>
		<h2 class="f-right">Granted: July 29, 2015</h2>
		<div class="clear"></div>

		<div class="big-tbl-cont">
			<div class="tbl-rounded ">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>No.</th>
						<th>Company Code</th>
						<th>Department Code</th>						
						<th>Department Name</th>						
						<th>Department Code</th>						
						<th>Employee Name</th>						
						<th>Alloted Shares</th>	
						<th>Payment Type</th>
						<th>Payment Value</th>											
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>						
						<td>Cash</td>		
						<td>Php 6,000.00</td>		
					</tr>
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
						<td>Credit Card</td>		
						<td>Php 6,000.00</td>	
					</tr>			
					<tr>
						<td>1</td>
						<td>0001</td>
						<td>0010</td>
						<td>Office of the President</td>
						<td>123456</td>
						<td>Joselito Salazar</td>
						<td>45000</td>
						<td>Check</td>		
						<td>Php 6,000.00</td>	
					</tr>		
				</tbody>
			</table>
		</div>
		</div>

		<h2 class="margin-top-30">Error on the Document</h2>
		<div class="tbl-rounded margin-top-30">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Row Number</th>
						<th>Column Name</th>
						<th>Issue</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Employee Name, Employee Code</td>
						<td>Employee Name does not match code</td>						
					</tr>
					<tr>
						<td>2</td>
						<td>Department Name, Employee Code</td>
						<td>Department Name does not match code</td>
					</tr>					
				</tbody>
			</table>
		</div>

		

	<div>
</section>


<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">	
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>		
			<div class="margin-top-20">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>				
			<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
		</div>
		<div class="clear"></div>
	</div>
</div>