<section section-style="top-panel">
    <div class="content">
        <div>
            <h1 class="f-left hidden">ESOP View</h1>
            <div class="breadcrumbs margin-bottom-20 border-10px">
                <a href="<?php echo base_url() ?>esop/personal_stocks">Personal Stock</a>
                <span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
                <a href="<?php echo base_url() ?>esop/personal_stock_view" data-label="esop_name">ESOP 1</a>
                <span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
                <a>Stock Claim Form</a>
            </div>
            <div class="clear"></div>
        </div>
        <button class="btn-normal f-right print_claim">Download Claim Form</button>
        <div class="clear"></div>


    </div>
</section>

<section section-style="content-panel" class="claim-form">

    <div class="content mother-container">

        <div class="main-container">

            <div class="sub-container">
<!--                <div class="head-center">-->
<!--                    <i class="fa fa-angle-left"></i>-->
<!--                    <p>Page</p>-->
<!--                    <p>1</p>-->
<!--                    <p class="white-color">/</p>-->
<!--                    <p>2</p>-->
<!--                    <i class="fa fa-angle-right"></i>-->
<!--                </div>-->
<!--                <div class="head-right">-->
<!--                    <i class="fa fa-search-plus"></i>-->
<!--                    <i class="fa fa-search-minus"></i>-->
<!--                </div>-->
                <div class="clear"></div>
            </div>
            <div class="sub-container1" style="padding-top: 20px; padding-bottom : 20px;">
                <div class="inside-sub" >
                    <table class="line-tbl" id="divcontents">
                        <tbody>
                        <tr>
                            <td><p class="text-center" style="text-align: center">PART 1 - To be filled out by the employee</p></td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <p class="display-inline-mid">1. OPTION PLAN TYPE</p>
                                    <div class="checkbox-esop display-inline-mid">
                                        <label class="black-color"><input type="checkbox" name="option"><data data-label="esop_name">ESOP 1</data></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>2. NUMBER OF MATURE STOCKS: <span class="margin-left-30" data-label="mature_stocks">43,468.00</span></p>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <p>3. NUMBER OF STOCKS TO BE CLAIMED</p>
                                    <p class="margin-left-30">(Check the box of the desired claim)</p>

                                    <div class="checkbox-rate">
                                        <label class="black-color"><input type="checkbox" name="stocks"><span>Partial:</span> No. of Shares _________ Amount for Payment _________ </label>
                                        <br><label class="black-color"><input type="checkbox" name="stocks"><span>Full:</span> No of Shares <span class="txt-under"><data data-label="mature_stocks">43,468.00</data> </span> Amounth of Payment <span class="txt-under"  style="width:120px"> <data data-label="stocks_value">P 108,235.32</data> </span></label>
                                    </div>
                                    <div class="text-center margin-top-30 margin-bottom-30">
                                        <p>Furthermore, this is to request for the issuance of the above number of stock certificates under my name.</p>
                                    </div>
                                    <div class="profile-line-tbl">
                                        <div class="profile1" style="float:left; width: 50%">
                                            <p><data data-label="full_name">43,468.00</data> </p>
                                            <p>_________________________</p>
                                            <p>Employee's Name and Signature</p>
                                        </div>
                                        <div class="profile2" style="float:right; width: 50%">
                                            <p><data data-label="date">43,468.00</data> </p>
                                            <p>_________________________</p>
                                            <p>Date</p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                    <h2 class="black-color margin-top-30 padding-top-30">Payment Application</h2>
                    <data data-container="payments_claim_form">
                        <data class="template payments claim_form">
                            <div class="long-panel border-10px margin-top-10">
                                <p class="first-text font-bold" data-label="year_no">Year 2</p>
                                <p class="first-text margin-left-30" data-label="year_date">July 28, 2016</p>
                                <p class="second-text margin-right-50" data-label="vesting">With Vesting Rights</p>
                                <div class="clear"></div>
                            </div>
                            <div class="long-panel border-10px margin-top-10">
                                <p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10"><data data-label="claim_mature_stocks">43,468.00</data> | @ <data data-label="price_per_share">43,468.00</data></span></p>
                                <p class="second-text margin-right-50"><data data-label="claim_stocks_value">43,468.00</data></p>
                                <div class="clear"></div>
                            </div>

                            <div class="tbl-rounded">
                                <table class="table-roxas tbl-display margin-top-20">
                                    <thead>
                                    <tr>
                                        <th class="width-250px">No.</th>
                                        <th>Payment Details</th>
                                        <th>Date Paid</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody data-container="payment_container_claim_form">
                                    <tr class="payments_claim_form template">
                                        <td data-label="year_no">1</td>
                                        <td data-label="payment_type">Gratuity</td>
                                        <td data-label="date">October 2, 2013</td>
                                        <td data-label="payment_value">18,399.36 Php</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </data>
                    </data>


                </div>
            </div>
        </div>

    </div>

</section>
<iframe id="ifmcontentstoprint" style="height: 0px; width: 0px; position: absolute"></iframe>