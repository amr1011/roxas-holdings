<div data-container="esop_information">
	<section section-style="top-panel">
		<div class="content">
			<div>
				<h1 class="f-left hidden">ESOP View</h1>
				<div class="breadcrumbs margin-bottom-20 border-10px">
					<a href="<?php echo base_url(); ?>esop/esop_list">ESOP</a>
					<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
					<a href="<?php echo base_url(); ?>esop/view_esop" data-label="esop_name">ESOP 1</a>
					<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
					<a>Distribute Shares</a>
				</div>			
				<div class="clear"></div>
			</div>
			<div class="f-right">
				<a href="<?php echo base_url(); ?>esop/view_esop">
					<button class="btn-cancel margin-right-10 color-cancel">Cancel</button>
				</a>
				<a href="javascript:void(0)"><!-- <?php echo base_url(); ?>esop/finalize_distribution -->
					<button class="btn-normal" id="finalize_edit_distribute_shares_btn">Finalise Distribution</button>
				</a>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			
			<!-- <div class="error-out margin-top-30">Share's of CACI Employees have exceeded the Total Alloted Shares. 
				Please re-distribute the shares accordingly
			</div> -->
			<div class="error-user-add finalize_distribution_shares_error_message hidden margin-bottom-10 margin-top-10">
			</div>
			<div class="success-user-add finalize_distribution_shares_success_message hidden margin-bottom-10 margin-top-10">
			</div>
			
		</div>
	</section>

	<section section-style="content-panel">

		<div class="content print-statement" data-container="esop_companies_container">

			<div class="esop_companies template hidden">
				<div>
					<h2 class="f-left margin-top-0 margin-bottom-0" data-label="company_name">CACI</h2>			
					<div class="f-right">
						<input type="text" class="large add-border-radius-5px display-inline-mid" name="company_alloted_shares"/>
						<p class="display-inline-mid font-20 white-color margin-left-10">Allotted Shares</p>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="long-panel border-10px margin-top-30 bg-other-color">
					<div class="f-left">
						<p class="first-text margin-right-30">Offer Letter</p>
						<div class="select add-radius width-200px offer_letter_dropdown">
							<select>
								<!-- <option value="1">Offer Letter 1</option>
								<option value="2">Offer Letter 2</option>
								<option value="3">Offer Letter 3</option>
								<option value="4">Offer Letter 4</option>
								<option value="5">Offer Letter 5</option> -->
							</select>
						</div>
					</div>
					<div class="f-right">
						<p class="second-text f-left margin-right-50">Acceptance Letter:</p>
						<div class="select add-radius f-left drop-change-color width-200px acceptance_letter_dropdown">
							<select>
								<!-- <option value="acc1">Acceptance Letter 1</option>
								<option value="acc2">Acceptance Letter 2</option>
								<option value="acc3">Acceptance Letter 3</option>
								<option value="acc4">Acceptance Letter 4</option>
								<option value="acc5">Acceptance Letter 5</option> -->
							</select>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>


				<div class="width-100per display-block margin-top-30 bg-last-content border-10px">
					<p class="font-20 white-color padding-top-10 padding-left-30 padding-bottom-10">Employee</p>
				</div>

				<div class="tbl-rounded margin-top-30">
					<table class="table-roxas tbl-display">
						<thead>
							<tr class="sort_esop_employees">
								<th>Company Code</th>
								<th><a href="javascript:void(0)" class="sort_field white-color" data-sort-value="desc" data-sort-by="data-user-employee-code" >Employee Code <i class="fa fa-chevron-down"></i></a></th>
								<th><a href="javascript:void(0)" class="sort_field white-color" data-sort-value="desc" data-sort-by="data-user-department-name" >Department Name <i class="fa fa-chevron-down"></i></a></th>
								<th><a href="javascript:void(0)" class="sort_field white-color" data-sort-value="desc" data-sort-by="data-user-full-name" >Employee Name <i class="fa fa-chevron-down"></i></a></th>
								<th><a href="javascript:void(0)" class="sort_field white-color" data-sort-value="desc" data-sort-by="data-user-rank-name" >Rank / Level <i class="fa fa-chevron-down"></i></a></th>
								<th>Alloted Shares</th>
							</tr>
						</thead>
						<tbody data-container="esop_employees_container">
							<tr class="esop_employees template hidden">
								<td data-label="company_code">0001</td>
								<td data-label="employee_code">0001</td>
								<td data-label="department_name">Office of the President</td>
								<td data-label="full_name">Juan Dela Cruz</td>
								<td data-label="rank_name">Executive</td>
								<td><input type="text" class="small add-border-radius-5px " name="employee_alloted_shares" disabled="true"/></td>
							</tr>
							<!-- <tr>
								<td>0002</td>
								<td>01214</td>
								<td>Corporate Accounting</td>
								<td>Marcelino Cabingao</td>
								<td>Executive</td>
								<td><input type="text" class="small add-border-radius-5px " /></td>
							</tr> -->					
						</tbody>
					</table>
				</div>
				<div class="text-right-line margin-top-30 padding-bottom-30">
					<div class="line"></div>
				</div>
			</div>

			<!-- <div class="margin-top-70">
				<h2 class="margin-top-0 margin-bottom-0 f-left">CAPDI.HO</h2>
				
				<div class="f-right">
					<input type="text" class="large add-border-radius-5px display-inline-mid" />
					<p class="display-inline-mid font-20 white-color margin-left-10">Allotted Shares</p>
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="long-panel border-10px margin-top-30 bg-other-color">
				<div class="f-left">
					<p class="first-text margin-right-30">Offer Letter</p>
					<div class="select add-radius width-200px">
						<select>
							<option value="letter1">Offer Letter 1</option>
							<option value="letter2">Offer Letter 2</option>
							<option value="letter3">Offer Letter 3</option>
							<option value="letter4">Offer Letter 4</option>
							<option value="letter5">Offer Letter 5</option>
						</select>
					</div>
				</div>
				<div class="f-right">
					<p class="second-text f-left margin-right-50">Acceptance Letter:</p>
					<div class="select add-radius f-left drop-change-color width-200px">
						<select>
							<option value="acc1">Acceptance Letter 1</option>
							<option value="acc2">Acceptance Letter 2</option>
							<option value="acc3">Acceptance Letter 3</option>
							<option value="acc4">Acceptance Letter 4</option>
							<option value="acc5">Acceptance Letter 5</option>
						</select>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>


			<div class="width-100per display-block margin-top-30 bg-last-content border-10px">
				<p class="font-20 white-color padding-top-10 padding-left-30 padding-bottom-10">Employee</p>
			</div>

			<div class="tbl-rounded margin-top-30">
				<table class="table-roxas tbl-display">
					<thead>
						<tr>
							<th>Company Code</th>
							<th>Employee Code</th>
							<th>Department Name</th>
							<th>Employee Name</th>
							<th>Rank / Level</th>
							<th>Alloted Shares</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>0001</td>
							<td>0001</td>
							<td>Office of the President</td>
							<td>Juan Dela Cruz</td>
							<td>Executive</td>
							<td><input type="text" class="small add-border-radius-5px " /></td>
						</tr>
						<tr>
							<td>0002</td>
							<td>01214</td>
							<td>Corporate Accounting</td>
							<td>Marcelino Cabingao</td>
							<td>Executive</td>
							<td><input type="text" class="small add-border-radius-5px " /></td>
						</tr>					
					</tbody>
				</table>
			</div> -->

		<div>
	</section>
</div>