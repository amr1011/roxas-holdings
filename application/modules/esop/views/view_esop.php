<div data-container="esop_information">
	<section section-style="top-panel">
		<div class="content">
			<div>
				<h1 class="f-left hidden">ESOP View</h1>
				<div class="breadcrumbs margin-bottom-20 border-10px">
					<a href="<?php echo base_url(); ?>esop/esop_list">ESOP</a>
					<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
					<a data-label="esop_name">ESOP 1</a>
				</div>
				<div class="undistributed_esop hidden">
					<div class="f-right">
						<a href="<?php echo base_url(); ?>esop/add_distribute_shares">
							<button class="btn-normal margin-right-10">Distribute Shares</button>
						</a>
						<a href="<?php echo base_url(); ?>esop/download_share_distribution_template">
							<button class="btn-normal margin-top-20 margin-right-10">Download Share Distribution Template</button>
						</a>
						<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Share Distribution Template</button>
						<button class="btn-normal modal-trigger" modal-target="edit-esop">Edit ESOP</button>				
					</div>
				</div>
				<div class="distributed_esop hidden">
					<div class="f-right">
						<button class="btn-normal margin-right-10 modal-trigger" modal-target="offer-expiration">Set Offer Expiration</button>
						<a href="<?php echo base_url(); ?>esop/download_share_distribution_template">
							<button class="btn-normal margin-top-20 margin-right-10">Download Share Distribution Template</button>
						</a>
						<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Share Distribution Template</button>
						<a href="<?php echo base_url(); ?>esop/edit_distribute_shares">
							<button class="btn-normal margin-right-10 ">Edit Distribute Shares</button>				
						</a>
					</div>
				</div>
				<div class="expiration_set hidden">
					<div class="f-right">
						<button class="btn-normal modal-trigger" modal-target="send-letter">Send Offer Letter</button>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section section-style="content-panel">
		<div class="content">

			<div data-container="esop_total_shares_container">
				<h2 class="f-left" data-label="esop_name">ESOP</h2>
				<h2 class="f-right">Granted: <span data-label="grant_date"></span></h2>
				<div class="clear"></div>

				<div class="option-box trio">
					<p class="title">Total Alloted Shares</p>
					<p class="description"><span data-label="total_share_qty">0</span> Shares</p>
				</div>
				<div class="option-box trio">
					<p class="title">Price per Share</p>
					<p class="description" data-label="price_per_share">0.00</p>
				</div>
				<div class="option-box trio">
					<p class="title">Vesting Years</p>
					<p class="description"><span data-label="vesting_years">0</span> Years</p>
				</div>
				<div class="option-box trio">
					<p class="title">No. of Employee Accepted</p>
					<p class="description"><span data-label="esop_total_employees_accepted">0</span> Employees</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shared Availed</p>
					<p class="description"><span data-label="esop_total_shares_availed">0</span> Shares</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shared Unavailed</p>
					<p class="description"><span data-label="esop_total_shares_unavailed">0</span> Shares</p>
				</div>
			</div>

			<div data-container="esop_companies_container">
				<div class="esop_companies template hidden">
					<div class="text-right-line margin-top-25 margin-bottom-30">
						<div class="line"></div>
					</div>

					<h2 class="f-left margin-top-30" data-label="company_name">CAPDI.HO</h2>		
					<div class="clear"></div>

					<div class="option-box trio">
						<p class="title">Total Alloted Shares</p>
						<p class="description"><span data-label="company_alloted_shares">0</span> Shares</p>
					</div>
					<div class="option-box trio">
						<p class="title">Price per Share</p>
						<p class="description" data-label="price_per_share">Php 2.49</p>
					</div>
					<div class="option-box trio">
						<p class="title">Vesting Years</p>
						<p class="description"><span data-label="vesting_years">0</span> Years</p>
					</div>

					<div class="option-box trio">
						<p class="title">No. of Employee Accepted</p>
						<p class="description"><span data-label="company_total_employees_accepted">0</span> Employees</p>
					</div>
					<div class="option-box trio">
						<p class="title">Total Shared Availed</p>
						<p class="description"><span data-label="company_total_shares_availed">0</span> Shares</p>
					</div>
					<div class="option-box trio">
						<p class="title">Total Shared Unavailed</p>
						<p class="description"><span data-label="company_total_shares_unavailed">0</span> Shares</p>
					</div>
					<div class="panel-group text-left margin-top-10 padding-top-30">

						<div class="accordion_custom ">
							<div class="panel-heading border-10px">
								<a href="#">
									<h4 class="panel-title white-color active">							
										Employee
										<i class="change-font fa fa-caret-right font-left"></i>
										<i class="fa fa-caret-down font-right"></i>							
									</h4>
								</a>																	
								<div class="clear"></div>					
							</div>					
							<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
								<div class="panel-body">

									<table class="table-roxas">
										<thead>
											<tr>
												<th>Company Code</th>
												<th>Department Name</th>
												<th>Employee Name</th>
												<th>Employee Code</th>
												<th>Rank / Level</th>
												<th>Alloted Shares</th>
												<th>Total Shares Availed</th>
												<th>Total Amount Availed</th>
												<!-- <th>Action</th> -->
											</tr>
										</thead>
										<tbody data-container="esop_employees_container">
											<tr class="esop_employees template hidden">
												<td data-label="company_code">0001</td>
												<td data-label="department_name">Office of the President</td>
												<td data-label="full_name">Juan Dela Cruz</td>
												<td data-label="employee_code">0001</td>
												<td data-label="rank_name">Executive</td>
												<td data-label="employee_alloted_shares">10,000</td>
												<td data-label="employee_shares_availed">0.00</td>
												<td data-label="employee_total_amount_availed">0.00</td>
												<!-- <td><a href="javascript:void(0)">Edit</a></td> -->
											</tr>
											<!-- <tr>
												<td>0001</td>
												<td>0001</td>
												<td>Office of the President</td>
												<td>Juan Dela Cruz</td>
												<td>Executive</td>
												<td>10,000</td>
												<td>0.00</td>
												<td>Php 125,000.00</td>
												<td><a href="#">Edit</a></td>
											</tr>
											<tr>
												<td>0001</td>
												<td>0001</td>
												<td>Office of the President</td>
												<td>Juan Dela Cruz</td>
												<td>Executive</td>
												<td>10,000</td>
												<td>0.00</td>
												<td>Php 125,000.00</td>
												<td><a href="#">Edit</a></td>
											</tr>
											<tr>
												<td>0001</td>
												<td>0001</td>
												<td>Office of the President</td>
												<td>Juan Dela Cruz</td>
												<td>Executive</td>
												<td>10,000</td>
												<td>0.00</td>
												<td>Php 125,000.00</td>
												<td><a href="#">Edit</a></td>
											</tr>
											<tr>
												<td>0001</td>
												<td>0001</td>
												<td>Office of the President</td>
												<td>Juan Dela Cruz</td>
												<td>Executive</td>
												<td>10,000</td>
												<td>0.00</td>
												<td>Php 125,000.00</td>
												<td><a href="#">Edit</a></td>
											</tr> -->
										</tbody>
									</table>

								</div>			
							</div>
						</div>	
					</div>
				</div>
			</div>

			<!-- Primary Accordion -->
			<!-- <div class="panel-group text-left margin-top-10 padding-top-30">

				<div class="accordion_custom ">
					<div class="panel-heading border-10px">
						<a href="#">
							<h4 class="panel-title white-color active">							
								Employee
								<i class="change-font fa fa-caret-right font-left"></i>
								<i class="fa fa-caret-down font-right"></i>							
							</h4>
						</a>																	
						<div class="clear"></div>					
					</div>					
					<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
						<div class="panel-body">

							<table class="table-roxas">
								<thead>
									<tr>
										<th>Company Code</th>
										<th>Employee Code</th>
										<th>Department Name</th>
										<th>Employee Name</th>
										<th>Rank / Level</th>
										<th>Alloted Shares</th>
										<th>Total Shares Availed</th>
										<th>Total Amount Availed</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
								</tbody>
							</table>

						</div>			
					</div>
				</div>	
					
				<div class="text-right-line  margin-top-25 margin-bottom-30 ">				
					<div class="line"></div>								
				</div>
				
				<h2 class="f-left margin-top-30">CACI</h2>		
				<div class="clear"></div>

				<div class="option-box trio">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				<div class="option-box trio">
					<p class="title">Price per Share</p>
					<p class="description">Php 2.49</p>
				</div>
				<div class="option-box trio">
					<p class="title">Vesting Years</p>
					<p class="description">5 Years</p>
				</div>

				<div class="option-box trio">
					<p class="title">No. of Employee Accepted</p>
					<p class="description">22 Employees</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shared Availed</p>
					<p class="description">125,000 Shares</p>
				</div>
				<div class="option-box trio">
					<p class="title">Total Shared Unavailed</p>
					<p class="description">125,000 Shares</p>
				</div>

				
				<div class="accordion_custom margin-top-30">
					<div class="panel-heading border-10px">
						<a href="#">
							<h4 class="panel-title white-color active">							
								Employee
								<i class="change-font fa fa-caret-right font-left"></i>
								<i class="fa fa-caret-down font-right"></i>							
							</h4>
						</a>																	
						<div class="clear"></div>					
					</div>					
					<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
						<div class="panel-body">

							<table class="table-roxas">
								<thead>
									<tr>
										<th>Company Code</th>
										<th>Employee Code</th>
										<th>Department Name</th>
										<th>Employee Name</th>
										<th>Rank / Level</th>
										<th>Alloted Shares</th>
										<th>Total Shares Availed</th>
										<th>Total Amount Availed</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
									<tr>
										<td>0001</td>
										<td>0001</td>
										<td>Office of the President</td>
										<td>Juan Dela Cruz</td>
										<td>Executive</td>
										<td>10,000</td>
										<td>0.00</td>
										<td>Php 125,000.00</td>
										<td><a href="#">Edit</a></td>
									</tr>
								</tbody>
							</table>

						</div>			
					</div>
				</div>	


			</div> -->

			<div data-container="audit_logs" class="hidden">
				<div class="panel-group text-left margin-top-30">
					<div class="accordion_custom">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color">							
									Audit Logs
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
							<div class="panel-body ">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Date of Activity</th>
											<th>User</th>
											<th>Activity Description</th>
										</tr>
									</thead>
									<tbody data-container="logs">
										<tr class="logs template hidden">
											<td data-label="date_created">September 10, 2015</td>
											<td data-label="created_by">ROXAS, PEDRO OLGADO</td>
											<td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
										</tr>
										<!-- <tr>
											<td>September 10, 2015</td>
											<td>VALENCIA, RENATO CRUZ</td>
											<td>Created Department  <span class="font-bold">"HR Department"</span></td>
										</tr> -->
									</tbody>
								</table>
							</div>			
						</div>
					</div>
				</div>
			</div>
		<div>
	</section>
</div>

<!-- share distribution template -->
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<!-- <div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	 -->
			<div class="error upload_share_distribution_error_message hidden margin-bottom-15">
			</div>
			<div class="success upload_share_distribution_success_message hidden margin-bottom-15">
			</div>
			<div class="margin-top-5">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30" id="file_name"><i>No file uploaded yet</i></p>
				<a href="javascript:void(0)" class="display-inline-mid" id="trigger_upload_file">Upload File</a>
				<label><input type="file" name="upload_share_distribution" class="hidden"></label>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<!-- <a href="<?php echo base_url(); ?>esop/template_preview"> -->
				<button type="button" class="display-inline-mid btn-dark btn-normal" id="upload_shares" disabled="true">Upload Template</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="modal-container" modal-id="edit-esop">
	<div class="modal-body small width-600px">
		<div class="modal-head">
			<h4 class="text-left">EDIT ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">	
			<!-- <div class="error">Total Share Quantity is Invalid. <br>Please type numbers only in the textbox.</div>	 -->
			<div class="error edit_esop_error_message hidden margin-bottom-10">
			</div>
			<div class="success edit_esop_success_message hidden margin-bottom-10">
			</div>

			<form id="edit_esop_form">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="width-200px">ESOP Name:</td>
							<td><input type="text" class="small width-250px add-border-radius-5px" name="esop_name" datavalid="required" labelinput="Esop Name"/></td>
						</tr>
						<tr>
							<td class="padding-top-10">Grant Date:</td>
							<td>
								<div class="date-picker display-inline-mid add-radius ">
									<input type="text" data-date-format="MM/DD/YYYY" class="width-210px" name="grant_date" datavalid="required" labelinput="Grant Date">
									<span class="fa fa-calendar text-center"></span>
								</div>
							</td>
						</tr>
						<tr>
							<td class="padding-bottom-40">Total Share Quantity:</td>
							<td>
								<input type="text" class="normal add-border-radius-5px width-250px input_numeric" name="total_share_qty" datavalid="required" labelinput="Total Share Quantity"/>
								<p class="display-inline-mid margin-left-10">Shares</p>
							</td>
						</tr>
						<tr>
							<td class="padding-top-10">Price per Share:</td>
							<td>
								<input type="text" class="normal add-border-radius-5px width-250px input_numeric" name="price_per_share" datavalid="required" labelinput="Price Per Share"/>
								<div class="select xsmall display-inline-mid margin-left-10 add-radius" id="currency_dropdown">
									<select>
										<!-- <option value="PHP">PHP</option>
										<option value="USD">USD</option>
										<option value="CAD">CAD</option>
										<option value="HKD">HKD</option>
										<option value="INR">INR</option> -->
									</select>
								</div>
							</td>
						</tr>
						<tr>
							<td class="padding-top-10">Vesting Years:</td>
							<td>
								<input type="text" class="normal add-border-radius-5px width-250px input_numeric" name="vesting_years" datavalid="required" labelinput="Vesting Years"/>
								<p class="display-inline-mid margin-left-10">Years</p>
							</td>
						</tr>
					</tbody>
				</table>		
			</form>

			<!-- <div class="margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div> -->
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal" id="edit_esop_btn">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- offer expiration  -->
<div class="modal-container" modal-id="offer-expiration">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SET OFFER EXPIRATION</h4>
			<div class="modal-close close-me"></div>
		</div>
		<form id="set_expiration_form">
			<div class="modal-content padding-40px">
				<!-- <div class="error">Date below has already passed. Please choose another date.</div> -->
				<div class="error set_offer_expiration_error_message hidden margin-bottom-10">
				</div>
				<div class="success set_offer_expiration_success_message hidden margin-bottom-10">
				</div>
				<div class="margin-top-20">
					<p class="display-inline-mid margin-left-15">Expiry Date:</p>
					<div class="date-picker display-inline-mid add-radius margin-left-20 set-offer-dp">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-250px date-picker" name="offer_expiration">
						<span class="fa fa-calendar text-center"></span>
					</div>
				</div>
			</div>
		</form>

		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			<!-- <a href="ESOP-view-esop-3.php"> -->
				<button type="button" class="display-inline-mid btn-dark" id="set_expiration_button">Set Offer Expiration</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- send offer letter -->
<div class="modal-container" modal-id="send-letter">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SEND OFFER LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">	
			<div class="error send_letter_error_message hidden margin-bottom-10">
			</div>
			<div class="success send_letter_success_message hidden margin-bottom-10">
			</div>		
			<div class="">
				<p class="font-15 font-bold">Are you sure you want to send the offer letters of this ESOP to the employees?</p>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>	
			<!-- <a href="ESOP-view-esop-4.php"> -->
				<button type="button" class="display-inline-mid btn-dark" id="esop_send_letter_btn">Send Offer Letter</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>