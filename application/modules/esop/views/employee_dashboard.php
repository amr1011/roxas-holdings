<section section-style="top-panel">

	
</section>

<section section-style="content-panel" style="overflow-y:hidden;">	

	<div class="content ">			

		<div class="tab-panel" data-container="personal_stocks_container">
			<div class="margin-bottom-20 margin-top-20">
				<h2 class="font-400">Welcome, <span><?php echo $this->session->userdata('full_name'); ?></span></h2>
			</div>
			<div data-container="personal_stocks_tab_container">
				<input type="radio" name="tabs" id="toggle-tab123" checked="checked" data-attr="" class="personal_stocks_tab_input template hidden"/>
				<label for="toggle-tab123" data-attr="" data-label="personal_stocks_esop_name" class="personal_stocks_tab_label template hidden">ESOP 12</label>
			</div>

			<div class="personal_stocks template hidden">
				<div id="tab123" class="tab" data-attr="" data-container="details-info">
			        <div class="option-box">
			            <p class="title">Grant Started</p>
			            <p class="description" data-label="grant_date">August 31, 2013</p>
			        </div>

			        <div class="option-box">
			            <p class="title">Subscription Price</p>
			            <p class="description" ><!-- Php --> <data data-label="price_per_share">6.00 </data></p>
			        </div>

			        <div class="option-box">
			            <p class="title">No. of Shares Availed</p>
			            <p class="description" data-label="accepted">130,144</p>
			        </div>

			        <div class="option-box">
			            <p class="title">Value of Shares Availed</p>
			            <p class="description"><!-- Php --> <data data-label="value">324,058.56 </data></p>
			        </div>
			        <h2 class="margin-top-35">Vesting Rights</h2>

			        <div class="tbl-rounded">
			            <table class="table-roxas tbl-display">
			                <thead>
			                <tr>
			                    <th>No.</th>
			                    <th>Year</th>
			                    <th>Percentage</th>
			                    <th>No. of Shares</th>
			                    <th>Value of Shares</th>
			                </tr>
			                </thead>
			                <tbody data-container="vesting">
			                <tr class="template vesting-rights">
			                    <td data-label="year_no">2</td>
			                    <td data-label="year">Sept. 01, 2013</td>
			                    <td data-label="percentage">20%</td>
			                    <td data-label="no_of_shares">20%</td>
			                    <td data-label="value_of_shares">20%</td>
			                </tr>

			                <tr class="last-content template">
			                    <td colspan="2"></td>
			                    <td class="combine"><span class="total-text">Total:</span> 100%</td>
			                    <td data-label="total_shares">130,144</td>
			                    <td data-label="total_value">Php 324,058.56</td>
			                </tr>

			                </tbody>
			            </table>


			            <h2 class="margin-top-35">Payment Summary</h2>

			            <div class="long-panel border-10px">
			                <p class="first-text">Amount Shares Taken <data data-label="summary_amount">130,144</data> | @ <data data-label="summary_price_per_share">2.40</data></p>
			                <p class="second-text margin-right-80"><data data-label="summary_currency">Php</data> <data data-label="summary_total"> </data></p>
			                <div class="clear"></div>
			            </div>

			            <div class="tbl-runded">
			                <table class="table-roxas tbl-display margin-top-20">
			                    <thead>
			                    <tr>
			                        <th>No.</th>
			                        <th>Payment Details</th>
			                        <th>Shares</th>
			                        <th>Date Paid</th>
			                        <th>Amount</th>
			                    </tr>
			                    </thead>
			                    <tbody data-container="summary_vesting">
			                    </tbody>
			                </table>
			            </div>

			            <div class="hidden">
				            <h2 class="margin-top-35">Payment Application</h2>
				            <data data-container="vesting_payments">
				                <div data-template="vesting_payments" class="template margin-top-20">
				                    <div class="long-panel border-10px">
				                        <p class="first-text font-bold">Year <data data-label="year_no">2</data></p>
				                        <p class="first-text margin-left-30";><data data-label="year">July 28, 2015</data></p>
				                        <p class="second-text margin-right-65 vesting_status">With Vesting Rights</p>
				                        <div class="clear"></div>
				                    </div>

				                    <div class="long-panel border-10px margin-top-20">
				                        <p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10"><data data-label="no_of_shares">130,144</data> | @ <data data-label="share_price">2.40</data></span></p>
				                        <p class="second-text margin-right-80"><data data-label="value_of_shares"></data></p>
				                        <div class="clear"></div>
				                    </div>

				                    <div class="tbl-runded">
				                        <table class="table-roxas tbl-display margin-top-20">
				                            <thead>
				                            <tr>
				                                <th>No.</th>
				                                <th>Payment Details</th>
				                                <th>Shares</th>
				                                <th>Date Paid</th>
				                                <th>Amount</th>
				                            </tr>
				                            </thead>
				                            <tbody data-container="payments">

				                            <tr class="template payments">
				                                <td data-label="id">1</td>
				                                <td data-label="payment_name">Payment</td>
				                                <td data-label="stocks">06</td>
				                                <td data-label="date_of_or">October 2, 2013</td>
				                                <td data-label="amount_paid">18,399.36</td>
				                            </tr>

				                            </tbody>
				                        </table>
				                    </div>
				                </div>
				            </data>
			            </div>

					</div>
				</div>
			</div>
		</div>

		<div data-container="no_personal_stocks" class="text-center margin-top-30 hidden">
			<div class="content margin-top-40 text-center">			
				<h2 class="font-400 font-styling">Welcome to RHI Employee Stock Options Plan (ESOP) System</h2>

				<h2 class="font-400 margin-top-40 text-center font-styling">Currently, You have no ESOP offered to you.<br>Please wait until your HR department has offered you a plan.
				<h2 class="font-400 margin-top-40 text-center font-styling">Once you have an ESOP offered to you, you can:</h2>
			<div>
			<div class="content">
				<div class="width-300px margin-20px display-inline-top">
					<div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/handshake.png" alt=""  class="icons-dashboard"></div>
					<p class="font-18 white-color font-styling margin-top-20">Accept ESOP Offers</p>
				</div>
				<div class="width-300px margin-20px display-inline-top">
					<div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/Group 3.png" alt=""  class="icons-dashboard"></div>
					<p class="font-18 white-color font-styling margin-top-20">Claim Vesting Rights from the Stock Option Plan</p>
				</div>
				<div class="width-300px margin-20px display-inline-top">
					<div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="../assets/images/1464598092_12.png" alt=""  class="icons-dashboard"></div>
					<p class="font-18 white-color font-styling margin-top-20">View ESOP Statement of Accounts and other documents</p>
				</div>
			</div>
		</div>
	<div>
</section>
