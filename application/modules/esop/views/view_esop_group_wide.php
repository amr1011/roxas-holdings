



<section section-style="top-panel">
  <div class="content">
    <div>
      <h1 class="f-left">Groupwide Stock Offer</h1>
      <div class="clear"></div>
    </div>
  
    <div class="header-effect">

      <div class="display-inline-mid default">
       
        <div>
          
            <div class="select add-radius display-inline-mid">
                <p class="white-color margin-bottom-5">Search</p>
                <select>
                    <option value="ESOP Name">ESOP Name</option>
                    <option value="Vesting Years">Vesting Years</option>
                    <option value="Grant Date">Grant Date</option>
                    <option value="Share QTY">Price per Share</option>
                </select>
            </div>
            <div class="display-inline-mid search-me">
                <input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
                <button class="btn-normal display-inline-mid margin-left-10">Search</button>
            </div>
            <br />

            <div class="select add-radius display-inline-mid">
                <p class="white-color margin-bottom-5">Filter by company</p>
                <select class="filter-by-company">
                    <option value="All companies">All companies</option>
                    <?php foreach ($companies as $key => $value) : ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                   <?php endforeach;?>
                </select>
            </div>

            <!-- <div class="display-inline-mid search-me">
                <input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px"/>
                <button class="btn-filter-company btn-normal display-inline-mid margin-left-10">Filter</button>
                <button class="btn-normal btn-filter-company">test</button>
            </div> -->

          <div class="display-inline-mid vesting-years">
            <input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
            <button class="btn-normal display-inline-mid margin-left-10">Search</button>
          </div>
          <div class="display-inline-mid price-share" style="display: inline-block;">
            <label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
            <br>
            <!-- <div class="price xsmall display-inline-mid margin-left-20">
              <input type="text">
            </div> -->
            <input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px search_price_per_share_input">
            <button class="btn-normal display-inline-mid margin-left-10 search_esop_btn">Search</button>        
          </div>
          <div class="display-inline-mid grant-date" style="display: none;">
          <p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
          <div>
            <label class="display-inline-mid margin-left-20">From</label>
            <div class="date-picker add-radius display-inline-mid margin-left-10">
              <input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_from_input">
              <span class="fa fa-calendar text-center"></span>
            </div>
            <label class="display-inline-mid margin-left-10">To</label>
            <div class="date-picker add-radius display-inline-mid margin-left-10">
              <input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_to_input">
              <span class="fa fa-calendar text-center"></span>
            </div>
            <button class="btn-normal display-inline-mid margin-left-10 search_esop_btn">Search</button>
          </div>

        </div>
        </div>
      </div>

      <!-- <div class="text-right-line margin-top-25 margin-bottom-30"> -->
      </div>

  </div>
</section>
<script type="text/javascript">
  var obj = <?php echo json_encode($companies);?>
</script>
<section section-style="content-panel">

  
    
    <?php if(isset($esop) AND count($esop) > 0):?>

    <div class="content padding-top-30">

      <?php foreach ($esop as $key => $value) :
        $page_count = count($value)/10;
        $ii = 0;
        $values = isset($companies) ? array_flip($companies) : array();
       ?>
        <div data-company-id = '<?php echo isset($values[$key]) ? $values[$key] : ""; ?>'>
        <h2 class="f-left margin-top-30"><?php echo $key; ?></h2> 

        <div class="clear"></div>

        <div class="tbl-rounded margin-top-20 table-content display_on">

          <table class="table-roxas tbl-display">

            <thead>
              <tr>
                <th>Name</th>
                <th>Date Granted</th>
                <th>Total Share Quantity</th>
                <th>Price per Share</th>
                <th>Vesting Years</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <?php  foreach($value as $key => $esop_details):?>

                  <tr class='<?php echo $ii >10 ? "display_hide" : "" ; ?>'>
                    <td><?php echo $esop_details['name']; ?></td>
                    <td><?php echo $esop_details['grant_date']; ?></td>
                    <td><?php echo number_format($esop_details['total_share_qty']); ?> Shares</td>
                    <td>Php <?php echo $esop_details['price_per_share']; ?></td>
                    <td><?php echo $esop_details['vesting_years']; ?> Years</td>
                    <td><a href="<?php echo base_url() ?>esop/view_group_wide_stock_offers/<?php echo $esop_details['company_id']; ?>/<?php echo $esop_details['id']; ?>">View Offer</a></td>
                  </tr>

              <?php $ii++;
               endforeach;?>
                <tr class='no-results'>
                   <td colspan = "6">No results Found.</td>
                  </tr>

          </tbody>
          </table>

          </div>
          <?php if($page_count >= 1 ): ?>

          <div class="paganation-container">
            <div class="paganation-content">
              <div class="btn-page">Previous</div>

              <?php for ($i=1; $i <= 2 ; $i++): ?>

                <div class="btn-page" ><?php echo $i; ?></div>
              
              <?php endfor;?>
               <?php if($page_count >= 6): ?>
               <div class="btn-page">...</div>
               <div class="btn-page"><?php echo $page_count; ?></div>

              <?php endif;?>
              <div class="btn-page">Next</div>

            </div>
          </div>
         <?php endif;?>
        </div>
         
      <?php endforeach;?>
    </div>
    <?php endif;?>
   
</section>

<style type="text/css">
  /*paganation*/
.paganation-container{
  margin: 20px 0;
  padding: 10px 0;
  position: relative;
  display: block;
}

.paganation-content{
  width: 100%;
  font-size: 0;
  text-align: center;
}

.paganation-content .btn-page{
  display: inline-block;
  vertical-align: middle;
  padding: 10px 20px;
  margin: 0 5px;
  border-radius: 10px;
  background-color: #a7bed7;
  font-size: 15px;
  cursor: pointer;

  transition: all .5s;
}

.paganation-content .btn-page:hover{
  background-color: #fff;
}
table tr.display_hide
{
  display: none;
}

.search-me,.vesting-years
{
  margin-top: 25px;
}
table.table-roxas tbody tr.no-results
{
  display: none;
}
.table-roxas tbody tr.filtered.odd
{
    background-color: #adc4dd!important;
}
.table-roxas tbody tr.filtered.even
{
    background-color:  #ffffff!important;
}
.paganation-content .btn-page.active{
  background: #fff;
}
</style>
