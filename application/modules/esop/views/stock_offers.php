<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Stock Offer</h1>
			
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid" id="search_stock_offer_dropdown">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px search_esop_name_stock_offer_input"/>
						<button class="btn-normal display-inline-mid margin-left-10 search_stock_offer_btn">Search</button>
					</div>
					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px search_vesting_years_stock_offer_input"/>
						<button class="btn-normal display-inline-mid margin-left-10 search_stock_offer_btn">Search</button>
					</div>
				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_from_stock_offer_input">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_to_stock_offer_input">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10 search_stock_offer_btn">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<!-- <div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div> -->
				<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px search_price_per_share_stock_offer_input"/>
				<button class="btn-normal display-inline-mid margin-left-10 search_stock_offer_btn">Search</button>				
			</div>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text" id="sort_esop">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-stock-offer-esop-name">ESOP Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-stock-offer-esop-grant-date">Grant Date <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-stock-offer-esop-price-per-share">Price per Share <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-stock-offer-esop-vesting-year">Vesting Year <i class="fa fa-chevron-down"></i></a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">
		<div class="grid-content" data-container="esop_view_by_grid">

			<div class="data-box padding-20-30px divide-by-2 esop_grid template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3 data-label="esop_name">ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted: <span data-label="grant_date">Sept 10, 2016</span></td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td><span data-label="total_share_qty">500,000</span> Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right"><span data-label="vesting_years">5</span> Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td data-label="price_per_share">Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="javascript:void(0)" class="grid_view_stock_offer"><button class="btn-normal">View Offer</button></a><!-- <?php echo base_url(); ?>esop/view_esop -->
				</div>
			</div>
			<div class="data-box width-100per padding-20-30px no_results_found template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="text-center">No Results found</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- <div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 2</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View Offer</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 3</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View Offer</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>E-ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View Offer</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ROXAS ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View Offer</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View Offer</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>PESO ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View Offer</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 15</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View Offer</button></a>
				</div>
			</div> -->

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price per Share</th>
						<th>Vesting</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody data-container="esop_view_by_list">
					<tr class="esop_list template hidden">
						<td data-label="esop_name">ESOP 1</td>
						<td data-label="grant_date">Sept 10, 2015</td>
						<td><span data-label="total_share_qty">500,000</span> Shares</td>
						<td data-label="price_per_share">Php 6.00</td>
						<td><span data-label="vesting_years">5</span> Years</td>
						<td><a href="javascript:void(0)" class="list_view_stock_offer">View Offer</a></td><!-- <?php echo base_url(); ?>esop/view_esop -->
					</tr>
					<tr class="no_results_found template hidden">
						<td colspan="6">No Results found</td>
					</tr>
					<!-- <tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View Offer</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View Offer</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View Offer</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View Offer</a></td>
					</tr> -->
					<tr class="last-content">
						<td colspan="6"></td>
					</tr>
				</tbody>
			</table>



		</div>
	<div>
</section>