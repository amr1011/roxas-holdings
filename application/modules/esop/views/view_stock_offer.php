<div data-container="offer_letter" class="">
	<div data-container="esop_information">
		<section section-style="top-panel">
			<div class="content">
				<div>
					<h1 class="f-left hidden">ESOP View</h1>
					<div class="breadcrumbs margin-bottom-20 border-10px">
						<a href="<?php echo base_url(); ?>esop/stock_offers">Stock Offers</a>
						<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
						<a data-label="esop_name">ESOP 1</a>				
					</div>			
					<div class="clear"></div>
				</div>
				<div class="f-right margin-top-10 margin-bottom-10" data-container="unexpired_offer">
					<button class="btn-normal margin-right-5" id="print_offer_letter">Download Offer</button>
					<button class="btn-normal modal-trigger" modal-target="accept-offer">Accept Offer</button>
				</div>
				<div class="clear"></div>
				
				
			</div>
		</section>
	</div>

	<section section-style="content-panel">

		<div class="content mother-container margin-bottom-30">

			<div class="main-container">

				<div class="sub-container">			
					<div class="head-center">
						<p></p>
						<!-- <a href="javascript:void(0)">					
							<i class="fa fa-angle-left"></i>
						</a>
						<p>Page</p>
						<a href="javascript:void(0)">
							<p>1</p>
						</a>
						<p class="white-color">/</p>
						<p class="black-color">2</p>
						<i class="fa fa-angle-right"></i> -->
					</div>				
					<div class="head-right">
						<!-- <i class="fa fa-search-plus"></i>
						<i class="fa fa-search-minus"></i> -->
					</div>			
					<div class="clear"></div>
				</div>
				<div class="sub-container1">
					<div class="inside-sub big-paper" data-container="offer_letter_html_container" style="overflow:auto;">						
						
						<!-- <p class="margin-top-10 font-bold">6. Disposition of Dividends</p>
						<p class="margin-left-20 margin-top-10">a. Cash Dividends</p>
						<p class="margin-left-20">Any cash dividends declared and paid out by the Company will be 
						applied against your subscription.</p>
						<p class="margin-left-20 margin-top-10">b. Stock Dividends</p>
						<p class="margin-left-20">In case of stock dividend declaration, the corresponding stock 
						dividends payable on the shares shall be withheld until your full 
						payment of the shares subscribed</p>
						
						
						
						
						<p class="margin-top-10 font-bold">7. Withdrawals from the Plan.</p>
						<p class="margin-left-20 margin-top-10">In case of retirement, resignation or dismissal other 
						than for just cause, you may exercise any Option that you have accepted within fifteen (15) 
						trading days from the date of retirement, separation or dismissal and pay the Subscription Price 
						in full. Transfer of employment within the Group shall not be deemed separation from employment.</p>
						
						<p class="margin-left-20 margin-top-10">In case of dismissal for a just cause, you shall, on the date of dismissal, 
						lose the right to exercise any unexercised Option.</p>

						<p class="margin-left-20 margin-top-10">In case of disability which renders you unfit to perform your functions, 
						you may exercise any Option that you have accepted and pay the Subscription Price no later than six 
						(6) months therefrom (extendible for a maximum period of one (1) year at the determination of the 
						Administrator).</p>

						<p class="margin-top-10 font-bold">8. Company Option to Repurchase</p>
						<p class="margin-left-20 margin-top-10">The Company has the option to repurchase all or a portion of the shares 
						issued or transferred to you pursuant to this Plan upon the cessation of your employment from the Group. The repurchase
						 price shall be i) the cash that have paid and/or the amount equivalent to the stock purchase gratuity applied to 
						 your subscription price if the shares are not yet fully paid at the time of repurchase or ii) the market price of the 
						 shares if already fully paid.
						</p>

						<p class="margin-top-30">Should you accept this offer, kindly signify your acceptance by accomplishing the attached Letter of Acceptance and 
						submit the same to the Human Resource Department (HRD) within thirty (30) days from your receipt of this letter together 
						with the sum of Ten Pesos (Php10.00) as acceptance fee.</p>
						
						<div class="address-paper">
							<p>6/F Cacho-Gonzales Bldg., 101 Aguirre Street, Legaspi Village, Makati City 1229 Philippines</p>
							<p>Trunk Lines: (632)810 8901 to 06 Fax No.: (632) 8171875</p>
						</div>
						
						<div class="f-right margin-top-50 margin-right-10">
							<p>Very truly yours,</p>
							<p class="margin-top-10">_____________________________</p>
							<p>RAMON M. DE LEON</p>
							<p>SVP, Human Resources &amp ESOP Administrator</p>
						</div> -->
					</div>
				</div>
			</div>

		</div>

	</section>
</div>
<div data-container="acceptance_letter" class="hidden">
	<div data-container="esop_information">
		<section section-style="top-panel">
			<div class="content">
				<div>
					<h1 class="f-left hidden">ESOP View</h1>
					<div class="breadcrumbs margin-bottom-20 border-10px">
						<a href="<?php echo base_url(); ?>esop/stock_offers">Stock Offers</a>
						<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
						<a data-label="esop_name">ESOP 1</a>
					</div>			
					<div class="clear"></div>
				</div>
				<div class="margin-top-30 margin-bottom-15">
					<h2 class="f-left margin-top-0 margin-bottom-0">Accept Offer</h2>
					<div class="f-right">
						<button class="btn-cancel margin-right-5 color-cancel" id="back_to_offer_letter_btn">Cancel</button>
						<button class="btn-normal margin-right-5" id="print_acceptance_letter">Download Acceptance Letter</button>
						<button class="btn-normal " id="send_letter_btn">Send Letter</button>
					</div>
					<div class="clear"></div>				
				<div class="error-user-add accept_offer_error_message hidden margin-bottom-10">
				</div>
				<div class="success-user-add accept_offer_success_message hidden margin-bottom-10">
				</div>
				</div>
			</div>
		</section>
	</div>

	<section section-style="content-panel">

		<div class="content mother-container margin-bottom-30">

			<div class="main-container">

				<div class="sub-container">			
					<div class="head-center">
						<p></p>
						<!-- <a href="javascript:void(0)">					
							<i class="fa fa-angle-left"></i>
						</a>
						<p>Page</p>
						<a href="javascript:void(0)">
							<p>1</p>
						</a>
						<p class="white-color">/</p>
						<p class="black-color">2</p>
						<i class="fa fa-angle-right"></i> -->
					</div>				
					<div class="head-right">
						<!-- <i class="fa fa-search-plus"></i>
						<i class="fa fa-search-minus"></i> -->
					</div>			
					<div class="clear"></div>
				</div>
				<div class="sub-container1">
					<div class="inside-sub big-paper" data-container="acceptance_letter_html_container">						
						<!-- <p class="f-right">January 05, 2016</p>
						<div class="clear"></div>
						<div>
							<p>Mr. Roman M. De Leon</p>
							<p>SVP, Human Resources &amp; ESOP Administrator</p>
							<p>Roxas Holdings, Inc.</p>
							<p>7th Floor, Cacho-Gonzalez Building</p>
							<p>101 Aguirre St., Legazpi Village</p>
							<p>Makati City, Metro Manila</p>
						</div>
						
						<p class="margin-top-30 text-center">Subject: 
							<span class="font-bold">EMPLOYEE STOCK OPTION PLAN (ESOP)</span>
						</p>

						<p class="margin-top-20">Dear Sir:</p>
						<p class="margin-left-50 margin-top-30">This Refers to the Option offer whick was the subject of your letter 
							dated <span class="txt-under">January 05, 2016</span>
						</p>
						<p class="margin-left-50 margin-top-10">In this regard, Please be informed that I am accepting the Option offer
						 under the terms and conditions of plan. Further, I am exercising my option to: </p>
						<p class="margin-left-50 margin-top-10">(Please check appropriate box)</p>

						<div class="checkbox-rate margin-left-50 margin-top-20">
							<label class="black-color font-15"><input type="checkbox" name="stocks" class="margin-right-10">ALL of my allocated shares totalling <span class="txt-under"> 964,000 </span> shares. </label>
							<br />
							<label class="black-color font-15 margin-top-20"><input type="checkbox" name="stocks" class="margin-right-10">Only ___________ shares from my allocated shares totalling ______________ shares.</label>
						</div>
						
						<p class="margin-left-50 margin-top-10">I am authorizing the company to deduct from my salary tha amount of ten pesos (P 10.00) as acceptance fee. </p>

						<div class="f-right margin-right-50 margin-top-30">

							<p class="margin-bottom-30">Very truly yours,</p>
							<p>______________________________</p>
							<p class="text-center margin-bottom-30">(Printed Name &amp; Signature)
							<p>______________________________</p>
							<p class="text-center margin-bottom-30">(Position)</p>
							<p>______________________________</p>
							<p class="text-center">(Company)</p>
						</div>
						<div class="clear"></div>
						<p class="margin-top-50 margin-left-50">Note: Please submit thru HR Department</p> -->

						
					</div>
				</div>
			</div>

		</div>

	</section>
</div>
<!-- Accept Offer -->
<div class="modal-container" modal-id="accept-offer">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ACCEPT OFFER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			
			<div class="">

				<!-- <div class="error margin-bottom-10">Quantity in Invalid. <br />Please type number only in the textbox</div> -->
				<div class="error accept_offer_error_message hidden margin-bottom-10">
				</div>
				<div class="success accept_offer_success_message hidden margin-bottom-10">
				</div>

				<p class="font-18 margin-bottom-10"	>I will accept the offer and take:</p>

				<div class="margin-left-30">
					<input type="radio" name="offer_type" class="default-cursor" id="all" />
					<label for="all" class="black-color txt-normal font-15 margin-left-10"></label>
					<span>All of the Shares Offered</span>
				</div>

				<div class="margin-left-30 margin-top-5">
					<input type="radio" name="offer_type" class="default-cursor" id="only" />
					<label for="only" class="black-color txt-normal font-15 margin-left-10"></label>
					<span>
						Only <input type="text" class="small width-100px margin-right-5 margin-left-5 add-border-radius-5px" name="stock_amount"/>Shares from Shares Offered
					</span>
				</div>													
			</div>	
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			
			<button type="button" class="display-inline-mid btn-normal" id="view_acceptance_offer_btn">View Acceptance Letter</button>
		</div>
		<div class="clear"></div>
	</div>
</div>
