<div data-container="with_esop_dashboard" class="hidden">
	<section section-style="top-panel">
		<div class="content">
			<div>
				<h2 class="font-400">Welcome, <span><?php echo $this->session->userdata('full_name'); ?></span></h2>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	<section section-style="content-panel" style="overflow-y:hidden;">	
		
		<div class="content ">		
			<div class="margin-bottom-20 ">					
				<div class="f-left width-100per">
						<table class="width-100per" data-container="esop_total_shares_container">
						<tbody>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description"><span data-label="main_total_alloted_shares">0</span> Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shares Availed</p>
										<p class="description"><span data-label="main_total_shares_availed">0</span> Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

											
											<div class="data-container">
												<span data-value="35.5%" class="">Total Shares Availed</span>										
												<span data-value="64.5%" class="margin-top-30">Total Shares Unavailed</span>										
											</div>		
											<!-- <div class="graph-txt">
												<p class="higher">35.5 %</p>
												<p class="lower">64.5 %</p>
											</div>	 -->												
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Unavailed</p>
										<p class="description"><span data-label="main_total_shares_unavailed">0</span> Shares</p>
									</div>
								</td>						
							</tr>
						</tbody>
					</table>
					
				</div>
				
				<div class="clear"></div>
			</div>

			<div class="text-right-line">
				<div class="view-by">				
					<p>View By: 
						<i class="fa fa-th-large grid" ></i>
						<i class="fa fa-bars list" ></i>
					</p>
				</div>
				<div class="line"></div>					
			</div>
			


			<div class="dash-grid margin-top-70">
				<div class="tab-panel" data-container="hr_head_and_admin_esop_grid_container">
					<div data-container="hr_head_and_admin_esop_grid_tab_container">
						<input type="radio" name="tabs" id="toggle-tab1234" checked="checked" data-attr="" class="hr_head_and_admin_esop_grid_tab_input template hidden"/>
						<label for="toggle-tab1234" data-attr="" data-label="hr_head_and_admin_esop_name" class="hr_head_and_admin_esop_grid_tab_label template hidden">ESOP 12</label>
					</div>

					<div class="hr_head_and_admin_esop_grid template hidden">
						<div id="tab1234" class="tab" data-attr="">
							<p class="display-inline-mid margin-right-10">Filter</p>
							<div class="select add-radius margin-left-5 margin-bottom-10 margin-top-10 company_filter">
								<select>
									<option value="All">All</option>
									<!-- <option value="filter2">Per Subsidiaries</option> -->
								</select>
							</div>
							<div data-container="grand_total_container">
								<h2 class="margin-left-5 margin-top-35 black-color">Grand Total</h2>
								<table class="width-100per">
									<tbody>
										<tr>
											<td>
												<div class="option-box width-tbl">
													<p class="title">Price Per Share</p>
													<p class="description"><span data-label="price_per_share">0</span></p>
												</div>
											</td>
											<td>
												<div class="option-box width-tbl">
													<p class="title">Total Alloted Shares</p>
													<p class="description"><span data-label="total_share_qty">0</span> Shares</p>
												</div>
											</td>
											<td rowspan="2" class="width-400px">
												<div class="with-txt">
													<div class="svg-cont chart-container graph-redesign margin-left-5">
														<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
														
														<div class="data-container">
															<span data-value="70%" class="">Total Shares Availed</span>										
															<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

														</div>		
														<!-- <div class="graph-txt">
															<p class="lower">30 %</p>
															<p class="higher">70 %</p>
														</div>	 -->												
													</div>												
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div class="option-box width-tbl">
													<p class="title">Total Shared Availed</p>
													<p class="description"><span data-label="esop_total_shares_availed">0</span> Shares</p>
												</div>
											</td>		
											<td>
												<div class="option-box width-tbl">
													<p class="title">Total Shared Unavailed</p>
													<p class="description"><span data-label="esop_total_shares_unavailed">0</span> Shares</p>
												</div>
											</td>					
										</tr>
									</tbody>
								</table>
							</div>
							<div data-container="esop_companies_container">
								<div class="esop_companies template hidden">
									<div class="text-right-line margin-bottom-30 margin-top-30 padding-bottom-5">				
										<div class="line"></div>								
									</div>
									<h2 class="margin-left-5 margin-top-35 black-color" data-label="company_name">Grand Total</h2>
									<table class="width-100per">
										<tbody>
											<tr>
												<td>
													<div class="option-box width-tbl">
														<p class="title">Price Per Share</p>
														<p class="description"><span data-label="price_per_share">0</span></p>
													</div>
												</td>
												<td>
													<div class="option-box width-tbl">
														<p class="title">Total Alloted Shares</p>
														<p class="description"><span data-label="company_alloted_shares">0</span> Shares</p>
													</div>
												</td>
												<td rowspan="2" class="width-400px">
													<div class="with-txt">
														<div class="svg-cont chart-container graph-redesign margin-left-5">
															<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>
															
															<div class="data-container">
																<span data-value="70%" class="">Total Shares Availed</span>										
																<span data-value="30%" class="margin-top-30">Total Shares Unavailed</span>										

															</div>		
															<!-- <div class="graph-txt">
																<p class="lower">30 %</p>
																<p class="higher">70 %</p>
															</div>	 -->												
														</div>												
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="option-box width-tbl">
														<p class="title">Total Shared Availed</p>
														<p class="description"><span data-label="company_total_shares_availed">0</span> Shares</p>
													</div>
												</td>		
												<td>
													<div class="option-box width-tbl">
														<p class="title">Total Shared Unavailed</p>
														<p class="description"><span data-label="company_total_shares_unavailed">0</span> Shares</p>
													</div>
												</td>					
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="dash-table margin-top-70">
				<div class="tab-panel" data-container="hr_head_and_admin_esop_list_container">
					<div data-container="hr_head_and_admin_esop_list_tab_container">
						<input type="radio" name="tabs" id="toggle-tab123" checked="checked" data-attr="" class="hr_head_and_admin_esop_list_tab_input template hidden"/>
						<label for="toggle-tab123" data-attr="" data-label="hr_head_and_admin_esop_name" class="hr_head_and_admin_esop_list_tab_label template hidden">ESOP 12</label>
					</div>
					
					<div class="hr_head_and_admin_esop_list template hidden">
						<div id="tab123" class="tab" data-attr="">
							<p class="display-inline-mid margin-right-10">Filter</p>
							<div class="select add-radius margin-left-5 margin-bottom-10 margin-top-10 company_filter">
								<select>
									<option value="All">All</option>
									<!-- <option value="filter2">Per Subsidiaries</option> -->
								</select>
							</div>

							<h2 class="black-color margin-top-35">Total of <span data-label="hr_head_and_admin_esop_name"></span></h2>

							<div class="tbl-rounded margin-top-25">
								<table class="table-roxas tbl-display">
									<thead>
										<tr>
											<th>Price Per Share</th>
											<th>Total Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Shares Unavailed</th>									
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><span data-label="price_per_share">Php 2.49</span></td>
											<td><span data-label="total_share_qty">0</span> Shares</td>
											<td><span data-label="esop_total_shares_availed">0</span></td>
											<td><span data-label="esop_total_shares_unavailed">0</span></td>							
										</tr>								
									</tbody>
								</table>
							</div>

							<div class="tbl-rounded margin-top-30">
								<table class="table-roxas tbl-display">
									<thead>
										<tr>
											<th>Subsidiaries</th>
											<th>Department</th>
											<th>ESOP Name</th>
											<th>Price / Share</th>									
											<th>Total Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Unavailed</th>
										</tr>
									</thead>
									<tbody data-container="esop_employees_container">
										<tr class="esop_employees template hidden">
											<td data-label="company_code">0001</td>
											<td data-label="department_name">0001</td>
											<td data-label="esop_name">0001</td>
											<td data-label="price_per_share">0001</td>
											<!-- <td data-label="full_name">Juan Dela Cruz</td> -->
											<td data-label="employee_alloted_shares">10,000</td>
											<td data-label="employee_shares_availed">0.00</td>
											<td data-label="employee_total_amount_availed">0.00</td>							
										</tr>											
									</tbody>
								</table>
							</div>
							<div data-container="additionals_container" class="hidden">
								<div class="panel-group text-left margin-top-10 padding-top-30">
									<div class="accordion_custom ">
										<div class="panel-heading border-10px">
											<a href="#">
												<h4 class="panel-title white-color active">							
													Additionals
													<i class="change-font fa fa-caret-right font-left"></i>
													<i class="fa fa-caret-down font-right"></i>							
												</h4>
											</a>																	
											<div class="clear"></div>					
										</div>					
										<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
											<div class="panel-body">

												<table class="table-roxas">
													<thead>
														<tr>
															<!-- <th>Company Code</th> -->
															<th>Employee Code</th>
															<th>Department Name</th>
															<th>Employee Name</th>
															<th>Rank / Level</th>
															<th>Grant Date</th>
															<th>Price Per Share</th>
															<th>Vesting Years</th>
														</tr>
													</thead>
													<tbody data-container="esop_special_container">
														<tr class="esop_special template hidden">
															<!-- <td data-label="company_code">0001</td> -->
															<td data-label="employee_code">0001</td>
															<td data-label="department_name">Office of the President</td>
															<td data-label="full_name">Juan Dela Cruz</td>
															<td data-label="rank_name">Executive</td>
															<td data-label="grant_date"></td>
															<td data-label="price_per_share"></td>
															<td data-label="vesting_years"></td>
														</tr>
													</tbody>
												</table>

											</div>			
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<div>
	</section>
</div>
<div data-container="without_esop_dashboard" class="hidden">
	<section section-style="content-panel">  

	  <div class="content tect-center margin-top-40 text-center">      
	    <h2 class="font-400 font-styling">Welcome to RHI Employee Stock Options Plan (ESOP) System</h2>

	    <h2 class="font-400 margin-top-40 text-center font-styling">Currently, you have no ESOP in the system. <br/> To add an ESOP, please click the button below.</h2>
	    <!-- <button class="btn-normal display-inline-mid modal-trigger font-20 margin-top-25 padding-top-10 padding-bottom-10"  modal-target="add-esop">Add an ESOP</button>	 -->
		<a href="<?php echo base_url(); ?>esop/esop_list"><button type="button" class="btn-normal display-inline-mid font-20 margin-top-25 padding-top-10 padding-bottom-10">Add an ESOP</button></a>
	    <h2 class="font-400 margin-top-40 text-center font-styling">Once you have an ESOP ofered to you, you can:</h2>
	  <div>
	  <div class="content">
	    <div class="display-inline-mid width-300px margin-20px">
	      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="<?php echo base_url(); ?>/assets/images/handshake.png" alt=""  class="icons-dashboard"></div>
	      <p class="font-18 white-color margin-top-20">Distribute ESOP Shares</p>
	    </div>
	    <div class="display-inline-mid width-300px margin-20px">
	      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="<?php echo base_url(); ?>/assets/images/group-3.png" alt=""  class="icons-dashboard"></div>
	      <p class="font-18 white-color margin-top-20">Manage Employee ESOP Claims</p>
	    </div>
	    <div class="display-inline-mid width-300px margin-20px">
	      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="<?php echo base_url(); ?>/assets/images/money.png" alt=""  class="icons-dashboard"></div>
	      <p class="font-18 white-color margin-top-20">Manage Gratuities and <br/> ESOP Payments</p>
	    </div>
	  </div>
	</section>
</div>