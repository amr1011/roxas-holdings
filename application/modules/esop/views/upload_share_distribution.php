<div data-container="esop_information">
	<section section-style="top-panel">
		<div class="content">
			<div>
				<h1 class="f-left hidden">ESOP View</h1>
				<div class="breadcrumbs margin-bottom-20 border-10px">
					<a href="<?php echo base_url(); ?>esop/esop_list">ESOP</a>
					<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
					<a href="<?php echo base_url(); ?>esop/view_esop" data-label="esop_name">ESOP 1</a>
					<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
					<a>Share Distribution Template</a>
				</div>
				<div class="f-right">
					<!-- <a href="<?php echo base_url(); ?>users/share_distribution"> -->
						<button class="btn-normal margin-right-10" id="cancel_upload_btn">Cancel Upload</button>
					<!-- </a> -->
					<button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Another Template</button>
					<!-- <a href="ESOP-view-esop2.php"> -->
						<button class="btn-normal" id="confirm_upload_btn" disabled="disabled">Confirm Upload</button>				
					<!-- </a> -->
				</div>
				<div class="clear"></div>
			</div>

			<div class="error-user-add insert_share_distribution_error_message hidden margin-bottom-10 margin-top-10">
			</div>
			<div class="success-user-add insert_share_distribution_success_message hidden margin-bottom-10 margin-top-10">
			</div>
		</div>
	</section>

	<section section-style="content-panel">
		
		<div class="content">
		
			<h2 class="f-left">Preview of <span data-label="upload_share_distribution_file_name"></span></h2>
			<div class="clear"></div>

			<div class="big-tbl-cont-esop margin-top-10">
				<div class="tbl-rounded ">
				<table class="table-roxas tbl-display">
					<thead>
						<tr>
							<th>No.</th>
							<th>Company Code</th>
							<th>Employee Code</th>
							<th>Employee Name</th>
							<th>Alloted Shares</th>
							<th>Offer Letter ID</th>
							<th>Acceptance Letter ID</th>
						</tr>
					</thead>
					<tbody data-container="upload_share_distribution_container">
						<tr class="upload_share_distribution template hidden">
							<td data-label="row">1</td>
							<td data-error="Company Code" data-label="company_code"></td>
							<td data-error="Employee Code" data-label="employee_code"></td>
							<td data-error="Employee Name" data-label="employee_name"></td>
							<td data-error="Alloted Shares" data-label="alloted_shares"></td>
							<td data-error="Offer Letter ID" data-label="offer_letter_id"></td>
							<td data-error="Acceptance Letter ID" data-label="acceptance_letter_id"></td>		
						</tr>	
					</tbody>
				</table>
			</div>
			</div>

			<h2 class="margin-top-30">Error on the Document</h2>
			<div class="tbl-rounded margin-top-30">
				<table class="table-roxas tbl-display">
					<thead>
						<tr>
							<th>Row Number</th>
							<th>Column Name</th>
							<th>Issue</th>						
						</tr>
					</thead>
					<tbody data-container="upload_share_distribution_error_container">
						<tr class="upload_share_distribution_error template hidden">
							<td data-label="row_error">1</td>
							<td data-label="row_name_error"></td>
							<td data-label="row_message_error"></td>
						</tr>
						<tr class="no_upload_share_distribution_error template hidden">
							<td colspan="3">No Errors Found.</td>
						</tr>				
					</tbody>
				</table>
			</div>

		<div>
	</section>
</div>
<div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<!-- <div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	 -->
			<div class="error upload_share_distribution_error_message hidden margin-bottom-15">
			</div>
			<div class="success upload_share_distribution_success_message hidden margin-bottom-15">
			</div>
			<div class="margin-top-5">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30" id="file_name"><i>No file uploaded yet</i></p>
				<a href="javascript:void(0)" class="display-inline-mid" id="trigger_upload_file">Upload File</a>
				<label><input type="file" name="upload_share_distribution" class="hidden"></label>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<!-- <a href="<?php echo base_url(); ?>esop/template_preview"> -->
				<button type="button" class="display-inline-mid btn-dark btn-normal" id="upload_shares" disabled="true">Upload Template</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>