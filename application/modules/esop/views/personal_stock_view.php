<section section-style="top-panel" data-container="main-info">
    <div class="content">
        <div>
            <!-- <h1 class="f-left hidden">ESOP View</h1> -->
            <div class="breadcrumbs margin-bottom-20 border-10px">
                <a href="<?php echo base_url();?>esop/personal_stocks">Personal Stock</a>
                <span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
                <a href="#" data-label="esop_name">ESOP 1</a>
            </div>
            <div class="clear"></div>
        </div>
        <h1 class="f-left"><abbr title="Employee Stock Option Plan">ESOP</abbr> <data data-label="esop_name" > </data> Statement of Account</h1>
        <div class="f-right">
            <button class="btn-normal margin-right-10 modal-trigger" modal-target="claim-document">View Claim Documents</button>
            <button class="btn-normal modal-trigger" modal-target="vesting-rights">Claim Vesting Right</button>
        </div>
        <div class="clear"></div>

        <div class="user-profile margin-top-30">
            <div class="user-image">
                <img src="<?php echo base_url(); ?>assets/images/profile/default_user_image.jpg" alt="user-picture" />
            </div>
            <div class="user-name">
                <p class="name display-block" data-label="full_name">Joselito Salazar</p>
                <p class="position">Employee No: <span data-label="employee_code">132</span></p>
            </div>
            <div class="user-info">
                <table>
                    <tbody>
                    <tr>
                        <td>Company</td>
                        <td data-label="company">ROXOL</td>
                    </tr>
                    <tr>
                        <td>Department</td>
                        <td data-label="department">ISD</td>
                    </tr>
                    <tr>
                        <td>Rank:</td>
                        <td data-label="rank">Officer I</td>
                    </tr>
                    <tr>
                        <td>Separation Status: </td>
                        <td>N/A</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</section>

<section section-style="content-panel" data-container="details-info">

    <div class="content">

        <div class="option-box">
            <p class="title">Grant Started</p>
            <p class="description" data-label="grant_date">August 31, 2013</p>
        </div>

        <div class="option-box">
            <p class="title">Subscription Price</p>
            <p class="description" ><!-- Php --> <data data-label="price_per_share">6.00 </data></p>
        </div>

        <div class="option-box">
            <p class="title">No. of Shares Availed</p>
            <p class="description" data-label="accepted">130,144</p>
        </div>

        <div class="option-box">
            <p class="title">Value of Shares Availed</p>
            <p class="description"><!-- Php --> <data data-label="value">324,058.56 </data></p>
        </div>

        <h2 class="margin-top-35">Vesting Rights</h2>

        <div class="tbl-rounded">
            <table class="table-roxas tbl-display">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Year</th>
                    <th>Percentage</th>
                    <th>No. of Shares</th>
                    <th>Value of Shares</th>
                </tr>
                </thead>
                <tbody data-container="vesting">
                <!--                <tr>-->
                <!--                    <td>1</td>-->
                <!--                    <td>Sept. 01, 2013</td>-->
                <!--                    <td>20%</td>-->
                <!--                    <td>26,028</td>-->
                <!--                    <td>64,809.72 Php</td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td>2</td>-->
                <!--                    <td>Sept. 01, 2013</td>-->
                <!--                    <td>20%</td>-->
                <!--                    <td>26,028</td>-->
                <!--                    <td>64,809.72 Php</td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td>3</td>-->
                <!--                    <td>Sept. 01, 2013</td>-->
                <!--                    <td>20%</td>-->
                <!--                    <td>26,028</td>-->
                <!--                    <td>64,809.72 Php</td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td>4</td>-->
                <!--                    <td>Sept. 01, 2013</td>-->
                <!--                    <td>20%</td>-->
                <!--                    <td>26,028</td>-->
                <!--                    <td>64,809.72 Php</td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td>5</td>-->
                <!--                    <td>Sept. 01, 2013</td>-->
                <!--                    <td>20%</td>-->
                <!--                    <td>26,028</td>-->
                <!--                    <td>64,809.72 Php</td>-->
                <!--                </tr>-->

                <tr class="template vesting-rights">
                    <td data-label="year_no">2</td>
                    <td data-label="year">Sept. 01, 2013</td>
                    <td data-label="percentage">20%</td>
                    <td data-label="no_of_shares">20%</td>
                    <td data-label="value_of_shares">20%</td>
                </tr>

                <tr class="last-content template">
                    <td colspan="2"></td>
                    <td class="combine"><span class="total-text">Total:</span> 100%</td>
                    <td data-label="total_shares">130,144</td>
                    <td data-label="total_value">Php 324,058.56</td>
                </tr>

                </tbody>
            </table>


            <h2 class="margin-top-35">Payment Summary</h2>

            <div class="long-panel border-10px">
                <p class="first-text">Amount Shares Taken <data data-label="summary_amount">130,144</data> | @ <data data-label="summary_price_per_share"></data></p>
                <p class="second-text margin-right-80"><data data-label="summary_currency">Php</data> <data data-label="summary_total"> </data></p>
                <div class="clear"></div>
            </div>

            <div class="tbl-runded">
                <table class="table-roxas tbl-display margin-top-20">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Payment Details</th>
                        <!-- <th>Shares</th> -->
                        <th>Date Paid</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody data-container="summary_vesting">
<!--                    <tr>-->
<!--                        <td>1</td>-->
<!--                        <td>Gratuity</td>-->
<!--                        <td>06</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>18,399.36 Php</td>-->
<!--                    </tr>-->

<!--                    <tr class="last-content ">-->
<!--                        <td colspan="4" class="text-left">-->
<!--                            <p class="margin-left-30 padding-bottom-5">Total Payment: </p>-->
<!--                            <p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>-->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <p class="font-15 padding-bottom-5">39,001.35 Php</p>-->
<!--                            <p class="font-15">285,057.21 Php</p>-->
<!--                        </td>-->
<!--                    </tr>-->
                    </tbody>
                </table>
            </div>

            <h2 class="margin-top-35">Payment Application</h2>
            <data data-container="vesting_payments">
                <div data-template="vesting_payments" class="template margin-top-20">
                    <div class="long-panel border-10px">
                        <p class="first-text font-bold">Year <data data-label="year_no">2</data></p>
                        <p class="first-text margin-left-30";><data data-label="year">July 28, 2015</data></p>
                        <p class="second-text margin-right-10">
                            <span class="tool-tip uc_status_notes_tool_tip display-inline-mid" data-id = '' tt-html="<p>Claim Status:</p><p><small>26,208 Shares</small></p>">
                               <span class="fa fa-exclamation-circle"></span>
                            </span>  
                        </p>

                        <!-- <p class="second-text margin-right-10">With Vesting Rights</p> -->

                        <p class="second-text margin-right-10"><data data-label="vesting_status"></data></p>

                        <div class="clear"></div>
                    </div>

                    <div class="long-panel border-10px margin-top-20">
                        <p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10"><data data-label="no_of_shares">130,144</data> | @ <data data-label="share_price"></data></span></p>
                        <p class="second-text margin-right-80"><data data-label="value_of_shares"></data></p>
                        <div class="clear"></div>
                    </div>

                    <div class="tbl-runded">
                        <table class="table-roxas tbl-display margin-top-20">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Payment Details</th>
                                <!-- <th>Shares</th> -->
                                <th>Date Paid</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody data-container="payments">

                            <tr class="template payments">
                                <td data-label="number">1</td>
                                <td data-label="payment_name">Payment</td>
                                <!-- <td data-label="stocks">06</td> -->

                                <td data-label="date_of_or">October 2, 2013</td>
                                <td data-label="amount_paid">18,399.36</td>
                            </tr>

                            <tr class="no-payment">
                                <td colspan="4">No Records Founds.</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </data>



<!--            <div class="long-panel border-10px margin-top-20">-->
<!--                <p class="first-text font-bold">Year 3</p>-->
<!--                <p class="first-text margin-left-30">July 28, 2015</p>-->
<!--                <p class="second-text margin-right-65">With Vesting Rights</p>-->
<!--                <div class="clear"></div>-->
<!--            </div>-->
<!---->
<!--            <div class="long-panel border-10px margin-top-20">-->
<!--                <p class="first-text"><strong>Amount Shares Taken</strong>  <span class="margin-left-10">130,144 | @ 2.40</span></p>-->
<!--                <p class="second-text margin-right-80">Php 324,058.56</p>-->
<!--                <div class="clear"></div>-->
<!--            </div>-->
<!---->
<!--            <div class="tbl-runded">-->
<!--                <table class="table-roxas tbl-display margin-top-20">-->
<!--                    <thead>-->
<!--                    <tr>-->
<!--                        <th>No.</th>-->
<!--                        <th>Payment Details</th>-->
<!--                        <th>Shares</th>-->
<!--                        <th>Page</th>-->
<!--                        <th>Date Paid</th>-->
<!--                        <th>Amount</th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    <tr>-->
<!--                        <td>1</td>-->
<!--                        <td>Gratuity</td>-->
<!--                        <td>06</td>-->
<!--                        <td>3 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>18,399.36 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>2</td>-->
<!--                        <td>Profit Share</td>-->
<!--                        <td>12</td>-->
<!--                        <td>3 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>10,590.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>3</td>-->
<!--                        <td>Cash</td>-->
<!--                        <td>-</td>-->
<!--                        <td>3 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>4</td>-->
<!--                        <td>Credit Card</td>-->
<!--                        <td>12</td>-->
<!--                        <td>3 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>5</td>-->
<!--                        <td>Check</td>-->
<!--                        <td>12</td>-->
<!--                        <td>2 of 4</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr class="last-content ">-->
<!--                        <td colspan="5" class="text-left">-->
<!--                            <p class="margin-left-30 padding-bottom-5">Total Payment: </p>-->
<!--                            <p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>-->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <p class="font-15 padding-bottom-5">39,001.35 Php</p>-->
<!--                            <p class="font-15">285,057.21 Php</p>-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->
<!---->
<!--            <div class="long-panel border-10px margin-top-20">-->
<!--                <p class="first-text font-bold">Year 4</p>-->
<!--                <p class="first-text margin-left-30">July 28, 2015</p>-->
<!--                <p class="second-text margin-right-65">With Vesting Rights</p>-->
<!--                <div class="clear"></div>-->
<!--            </div>-->
<!---->
<!--            <div class="long-panel border-10px margin-top-20">-->
<!--                <p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10">130,144 | @ 2.40</span></p>-->
<!--                <p class="second-text margin-right-80">Php 324,058.56</p>-->
<!--                <div class="clear"></div>-->
<!--            </div>-->
<!---->
<!--            <div class="tbl-runded">-->
<!--                <table class="table-roxas tbl-display margin-top-20">-->
<!--                    <thead>-->
<!--                    <tr>-->
<!--                        <th>No.</th>-->
<!--                        <th>Payment Details</th>-->
<!--                        <th>Shares</th>-->
<!--                        <th>Page</th>-->
<!--                        <th>Date Paid</th>-->
<!--                        <th>Amount</th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    <tr>-->
<!--                        <td>1</td>-->
<!--                        <td>Gratuity</td>-->
<!--                        <td>06</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>18,399.36 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>2</td>-->
<!--                        <td>Profit Share</td>-->
<!--                        <td>12</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>10,590.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>3</td>-->
<!--                        <td>Cash</td>-->
<!--                        <td>-</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>4</td>-->
<!--                        <td>Credit Card</td>-->
<!--                        <td>12</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>5</td>-->
<!--                        <td>Check</td>-->
<!--                        <td>12</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr class="last-content ">-->
<!--                        <td colspan="5" class="text-left">-->
<!--                            <p class="margin-left-30 padding-bottom-5">Total Payment: </p>-->
<!--                            <p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>-->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <p class="font-15 padding-bottom-5">39,001.35 Php</p>-->
<!--                            <p class="font-15">285,057.21 Php</p>-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->
<!---->
<!--            <div class="long-panel border-10px margin-top-20">-->
<!--                <p class="first-text font-bold">Year 5</p>-->
<!--                <p class="first-text margin-left-30">July 28, 2015</p>-->
<!--                <p class="second-text margin-right-65">With Vesting Rights</p>-->
<!--                <div class="clear"></div>-->
<!--            </div>-->
<!---->
<!--            <div class="long-panel border-10px margin-top-20">-->
<!--                <p class="first-text"><strong>Amount Shares Taken</strong> <span class="margin-left-10">130,144 | @ 2.40</span></p>-->
<!--                <p class="second-text margin-right-80">Php 324,058.56</p>-->
<!--                <div class="clear"></div>-->
<!--            </div>-->
<!---->
<!--            <div class="tbl-runded">-->
<!--                <table class="table-roxas tbl-display margin-top-20">-->
<!--                    <thead>-->
<!--                    <tr>-->
<!--                        <th>No.</th>-->
<!--                        <th>Payment Details</th>-->
<!--                        <th>Shares</th>-->
<!--                        <th>Page</th>-->
<!--                        <th>Date Paid</th>-->
<!--                        <th>Amount</th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    <tr>-->
<!--                        <td>1</td>-->
<!--                        <td>Gratuity</td>-->
<!--                        <td>06</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>18,399.36 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>2</td>-->
<!--                        <td>Profit Share</td>-->
<!--                        <td>12</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>10,590.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>3</td>-->
<!--                        <td>Cash</td>-->
<!--                        <td>-</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>4</td>-->
<!--                        <td>Credit Card</td>-->
<!--                        <td>12</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>5</td>-->
<!--                        <td>Check</td>-->
<!--                        <td>12</td>-->
<!--                        <td>4 of 5</td>-->
<!--                        <td>October 2, 2013</td>-->
<!--                        <td>64,809.72 Php</td>-->
<!--                    </tr>-->
<!--                    <tr class="last-content ">-->
<!--                        <td colspan="5" class="text-left">-->
<!--                            <p class="margin-left-30 padding-bottom-5">Total Payment: </p>-->
<!--                            <p class="margin-left-30">Outstanding Balance (as of November 24, 2014)</p>-->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <p class="font-15 padding-bottom-5">39,001.35 Php</p>-->
<!--                            <p class="font-15">285,057.21 Php</p>-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->


            <!-- place accordion here  -->
            <!-- <div class="panel-group text-left margin-top-30">
                <div class="accordion_custom">
                    <div class="panel-heading border-10px">
                        <a href="#">
                            <h4 class="panel-title white-color active">
                                Audit Logs
                                <i class="change-font fa fa-caret-right font-left"></i>
                                <i class="fa fa-caret-down font-right"></i>
                            </h4>
                        </a>
                        <div class="clear"></div>
                    </div>
                    <div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">
                        <div class="panel-body ">

                            <table class="table-roxas">
                                <thead>
                                <tr>
                                    <th>Date of Activiy</th>
                                    <th>User</th>
                                    <th>Activity Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>September 10, 2015</td>
                                    <td>ROXAS, PEDRO OLGADO</td>
                                    <td>Claimed <span class="font-bold">Year 1</span>Vesting Rights</td>
                                </tr>
                                <tr>
                                    <td>September 10, 2015</td>
                                    <td>VALENCIA, RENATO CRUZ</td>
                                    <td>View Claim Form from <span class="font-bold">Year 1</span>Vesting Rights</td>
                                </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

                <div>
                </div>

            </div> -->
    <div data-container="audit_logs" class="hidden">
        <div class="panel-group text-left margin-top-30">
            <div class="accordion_custom">
                <div class="panel-heading border-10px">
                    <a href="#">
                        <h4 class="panel-title white-color">                            
                            Audit Logs
                            <i class="change-font fa fa-caret-right font-left"></i>
                            <i class="fa fa-caret-down font-right"></i>                         
                        </h4>
                    </a>                                                                    
                    <div class="clear"></div>                   
                </div>                  
                <div class="panel-collapse border-10px margin-top-20 margin-bottom-20">                             
                    <div class="panel-body ">

                        <table class="table-roxas">
                            <thead>
                                <tr>
                                    <th>Date of Activity</th>
                                    <th>User</th>
                                    <th>Activity Description</th>
                                </tr>
                            </thead>
                            <tbody data-container="logs">
                                <tr class="logs template hidden">
                                    <td data-label="date_created">September 10, 2015</td>
                                    <td data-label="created_by">ROXAS, PEDRO OLGADO</td>
                                    <td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
                                </tr>
                                <!-- <tr>
                                    <td>September 10, 2015</td>
                                    <td>VALENCIA, RENATO CRUZ</td>
                                    <td>Created Department  <span class="font-bold">"HR Department"</span></td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>          
                </div>
            </div>
        </div>
    </div>
</section>

<!-- claim documents  -->
<div class="modal-container" modal-id="claim-document">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">VIEW CLAIM DOCUMENTS</h4>
            <div class="modal-close close-me margin-top-5"></div>
        </div>

        <!-- content -->
        <div class="modal-content">

            <table class="width-100percent text-center margin-top-10">
                <thead>
                <tr>
                    <th></th>
                    <th class="text-center">Years Claimed</th>
                    <th class="text-center">Date of Vesting Rights</th>
                </tr>
                </thead>
                <tbody data-container="claim_documents">
                <tr class="template claim_documents">
                    <td><input type="radio" name="claim_id"></td>
                    <td><label class="black-color normal font-normal" data-label="year_text">Year 1 &amp; Year 2</label></td>
                    <td data-label="date_text">July 01, 2013</td>
                </tr>
<!--                <tr>-->
<!--                    <td><input type="radio" name="claim-document" id="year3"></td>-->
<!--                    <td><label for="year3" class="black-color normal font-normal">Year 3</label></td>-->
<!--                    <td>July 01, 2013</td>-->
<!--                </tr>-->
                </tbody>
            </table>
            <div class="margin-top-10 text-center">
                <p class="display-inline-mid margin-right-30 ">Document Type</p>
                <div class="select add-radius display-inline-mid width-250px personal-stock-dd">
                    <select name="document_view">
                        <option value="Statement of Account">Statement of Account</option>
                        <option value="Claim Form">Claim Form</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
            <button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
            <button type="button" class="display-inline-mid btn-normal personal-stock-dd-btn view_claim_document" disabled>Confirm</button>
        </div>
        <div class="clear"></div>
    </div>
</div>

<!-- vesting righhts  -->
<div class="modal-container " modal-id="vesting-rights">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">CLAIM VESTING RIGHTS</h4>
            <div class="modal-close close-me margin-top-5"></div>
        </div>

        <!-- content -->
        <div class="modal-content">
            <p>Please select allowed vesting year/s to claim:</p>
            <table class="width-100percent text-center margin-top-10">
                <thead>
                <tr>
                    <th></th>
                    <th class="text-center">Allowed Year</th>
                    <th class="text-center">Date of Vesting Rights</th>
                </tr>
                </thead>
                <tbody data-container="vesting_rights_claim">
                <tr class="template vesting_rights_claim">
                    <td class="width-50px"><input data-label="year_no" type="checkbox"></td>
                    <td>Year <data data-label="year_no">1</data></td>
                    <td><data data-label="year_date">1</data></td>
                </tr>

                </tbody>
            </table>

        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
            <button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
            <button type="button" class="display-inline-mid btn-normal submit_vesting_rights" disabled>Claim Vesting Rights</button>
        </div>
        <div class="clear"></div>
    </div>
</div>

