


<section section-style="top-panel">
  <div class="content">
    <div class="header-effect">
      <div class="breadcrumbs margin-bottom-20 border-10px">
        <a href="<?php echo base_url() ?>esop/group_wide_stocks">Groupwide Stock Offer</a>
        <span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
        <a href=""><?php echo $esop_name; ?></a>
      </div>
      <div class="display-inline-mid default display-block">
        <p class="white-color margin-bottom-5 ">Status:</p>
        <div>
          <div class="select add-radius display-inline-mid ">
            <select class="select display-inline-block" id='select-status'>
              <option value="All">All</option>
              <option value="Accepted">Accepted</option>
              <option value="Waiting">Waiting</option>
            </select>
          </div>
          <div class="display-inline-mid search-me hidden">
            <button class="btn-normal display-inline-mid margin-left-10">Filter List</button>
          </div>
          <div class="f-right">
            <button class="btn-normal display-inline-mid margin-left-10">Download Selected Acceptance Letter</button>
            <button class="btn-normal display-inline-mid margin-left-10">Download Selected Offer Letter</button>
            <button class="btn-normal display-inline-mid margin-left-10 modal-trigger" modal-target="modal_update_offer_list" disabled>Accept Offer</button>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>

</section>
<section section-style="content-panel">

<?php foreach($esop as $key => $offers): 
	$page_count = ceil(count($offers)/10);
?>
	<div class="content padding-top-30">
    <h2 class="f-left margin-top-30"><?php echo $key ;?></h2>    
    <div class="clear"></div>
    <div class="tbl-rounded margin-top-20 table-content display_on">
      <table class="table-roxas tbl-display" style='display:none'>
        <thead>
          <tr>
            <th><input type="checkbox" name="" id=""> </th>
            <th>Name</th>
            <th>Alloted Shares</th>
            <th>Accepted Shares</th>
            <th>Offer Letter</th>
            <th>Acceptance Letter</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
	    <?php  foreach($offers as $key => $offer):?>
          <tr class='tr-<?php echo $offer['status']; ?>' data-stock-offer= "<?php echo $offer['stock_offer_id']; ?>" data-id = "<?php echo $offer['id'];?>" data-user = "<?php echo $offer['user_id']; ?>" data-user-esop-id='<?php echo $offer['user_esop_id']; ?>' data-alloted-shares="<?php echo $offer['allotment']; ?>">
            <td><input type="checkbox" name="esop-checkbox[]" id="" value='<?php echo $offer['stock_offer_id'];?>' <?php echo $offer['status'] == 'Waiting' ? '': 'disabled=disabled'; ?>> </td>
            <td><?php echo $offer['fullname']; ?></td>
            <td><?php echo number_format($offer['allotment'],2); ?> Shares</td>
            <td>
              <input type="text" class="<?php echo $offer['status'] == 'Waiting' ? 'display-block ': 'display-none'; ?> shares" value="<?php echo $offer['accepted_shares'] > 0 ? number_format($offer['accepted_shares']) : number_format($offer['allotment']); ?>"/>
              <label class="shares black-color font-15 <?php echo $offer['status'] == 'Waiting' ? 'display-none': 'display-block '; ?>"><?php echo number_format($offer['accepted_shares'],2); ?> Shares</label> 
            </td>
            <td><a class='a-print' href="<?php echo base_url() ?>esop/download_offer_letter/<?php echo $offer['stock_offer_id']; ?>/<?php echo $offer['user_id']; ?>">Download</a></td>
            <td><a class='a-print' href="<?php echo base_url() ?>esop/download_acceptance_letter/<?php echo $offer['stock_offer_id']; ?>/<?php echo $offer['user_id']; ?>">Download</a></td>
            <td><label class="gray-color"><?php echo $offer['status']; ?></label></td>
            <td><a href="#" class='a-edit <?php echo $offer['status'] == 'Waiting' ? 'display-none' : ''; ?>'>Edit</a>
            	<div class='save-panel display-none'><a class="red-color cancel" href="#">Cancel</a> | <a class="green-color save" href="#">Save
            	</div></a>
            </td>
          </tr>
        <?php endforeach;?>

        <tr class='no-results'>
         <td colspan = "8">No results Found.</td>
        </tr>
        </tbody>
      </table>
    </div>
    <!-- paganation -->
    
         <div data-container="audit_logs" class="hidden">
                <div class="panel-group text-left margin-top-30">
                  <div class="accordion_custom">
                    <div class="panel-heading border-10px">
                      <a href="#">
                        <h4 class="panel-title white-color">              
                          Audit Logs
                          <i class="change-font fa fa-caret-right font-left"></i>
                          <i class="fa fa-caret-down font-right"></i>             
                        </h4>
                      </a>                                  
                      <div class="clear"></div>         
                    </div>          
                    <div class="panel-collapse border-10px margin-top-20 margin-bottom-20">               
                      <div class="panel-body ">

                        <table class="table-roxas-2">
                          <thead>
                            <tr>
                              <th>Date of Activity</th>
                              <th>User</th>
                              <th>Activity Description</th>
                            </tr>
                          </thead>
                          <tbody data-container="logs">
                            <tr class="logs template hidden">
                              <td data-label="date_created">September 10, 2015</td>
                              <td data-label="created_by">ROXAS, PEDRO OLGADO</td>
                              <td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
                            </tr>
                            <!-- <tr>
                              <td>September 10, 2015</td>
                              <td>VALENCIA, RENATO CRUZ</td>
                              <td>Created Department  <span class="font-bold">"HR Department"</span></td>
                            </tr> -->
                          </tbody>
                        </table>
                      </div>      
                    </div>
                  </div>
                </div>
</div>
</div>
<?php endforeach;?>

</section>

<!-- update offer list -->
<div class="modal-container" modal-id="modal_update_offer_list">
      <div class="modal-body width-600px">
        <div class="modal-head">
          <h4 class="text-left">Accept Offer</h4>
          <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">   
          <div class="success display-none">Offer has been accepted. Offer List has been updated</div>
          <br />
          <p class='p-alert'>Are you sure you want to update the offer acceptance list of <strong><?php echo $esop_name; ?> ?</strong></p>
          
        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
          <button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>      
          <button type="button" class="display-inline-mid btn-dark btn-accept-offer" >Accept Offer</button>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>


<style type="text/css">
	/*paganation*/
.paganation-container{
	margin: 0px 0;
	padding: 0px 0;
	position: relative;
	display: block;
	border-top: 0px;
  border-bottom: solid 2px #fff;
}

.paganation-content{
	width: 100%;
	font-size: 0;
	text-align: right;
}

.paganation-content .btn-page{
	display: inline-block;
	vertical-align: middle;
	padding: 10px 20px;
	margin: 0 5px;
	border-radius: 10px;
	background-color: #a7bed7;
	font-size: 15px;
	cursor: pointer;

	transition: all .5s;
}

.paganation-content .btn-page:hover{
	background-color: #fff;
}
table.table-roxas tbody tr.no-results
{
  display: none;
}
table.table-roxas-2
{
  width: 100%;
}
table.table-roxas-2 th{
    background-color: #2e4c6e;
    color: #fff;
    font-weight: normal;
    font-size: 15px;
    padding: 10px;

}
table.table-roxas-2 td{
  padding: 10px;
}
.table-roxas-2 tr:nth-child(even)
{
      background-color: #adc4dd;
}
.table-roxas-2 tr:nth-child(odd)
{
      background-color: #fff;
}
.btn-page-selector-holder
{
    display: inline-block;
    vertical-align: middle;
    padding: 10px 20px;
    padding-left:0px;
    padding-right: 0px;
    margin: 0 5px;
    border-radius: 10px;
    font-size: 15px;
    cursor: pointer;
    transition: all .5s;
    border-radius: 5px;

}
.btn-page-selector-holder .input-holder
{
    display: inline-block;
    position: relative;
    font-weight: normal;
  
}

.btn-page-selector-holder .input-holder input
{
    width: 100px;
    height: 36px;
    text-align: center;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    border: 0px;
    background: #d1deec;
}

.btn-page-selector-holder .input-caret
{
    display: block;
    position: absolute;
    width: 20px;
    height: 36px;
    right: -20px;
    top: 0px;
    padding: 0;
    border-left: 1px solid #ccc;
    background: #a7bed7 url(http://localhost/roxas/assets/images/ui/select-arrow.png) no-repeat;
    background-size: 15px 9px;
    background-position: center;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
}
.btn-page-selector-holder .options-holder
{
    width: 100%;
    position: absolute;
    background-color: #fff;
    z-index: 9999;
    max-height: 180px;
    top: -180px;
    box-shadow: 0 2px 10px rgba(0,0,0,.3);
    overflow-x: hidden;
    transition: all .5s;
    display: none;
    text-align: center;


}
.btn-page-selector-holder .options-holder .options
{
    display: inline-block;
    padding: 0px;
    width: 100%;
    height: 25px;
    transition: all .5s;
    text-align: right;
    margin-right: 20px;
    padding-right: 26px;
}
.btn-page-selector-holder .options-holder .options:hover
{
  background: #a7bed7;
}
.odd{
  background-color: #adc4dd!important;
}
.even{
  background-color: #fff!important;
}
/*.panel-collapse
{
  overflow: inherit!important;
}*/
</style>