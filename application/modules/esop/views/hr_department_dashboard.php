<section section-style="top-panel">
</section>

<section section-style="content-panel" style="overflow-y:hidden;">	
	

	<div class="content ">
			
		<div class="margin-bottom-20 margin-top-20">
			<div data-container="with_esop_dashboard" class="hidden">
				<h2 class="font-400">Welcome, <span><?php echo $this->session->userdata('full_name'); ?></span></h2>
				<p class="font-20 white-color"><!-- CAPDI. HO --><?php echo $this->session->userdata('company_name'); ?></p>
				<div class="f-left width-100per">
					<table class="width-100per" data-container="esop_total_shares_container">
						<tbody>
							<tr>
								<!-- <td>
									<div class="option-box width-tbl">
										<p class="title">Price Per Share</p>
										<p class="description" data-label="price_per_share">Php 2.49</p>
									</div>
								</td> -->
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Alloted Shares</p>
										<p class="description"><span data-label="main_total_alloted_shares">0</span> Shares</p>
									</div>
								</td>
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description"><span data-label="main_total_shares_availed">0</span> Shares</p>
									</div>
								</td>
								<td rowspan="2" class="width-400px">
									<div class="with-txt">
										<div class="svg-cont chart-container">
											<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

											
											<div class="data-container">
												<span data-value="35.5%" class="">Total Shares Availed</span>										
												<span data-value="64.5%" class="margin-top-30">Total Shares Unavailed</span>										
											</div>		
											<!-- <div class="graph-txt">
												<p class="higher">35.5 %</p>
												<p class="lower">64.5 %</p>
											</div>	 -->												
										</div>												
									</div>
								</td>
							</tr>
							<tr>
								<!-- <td>
									<div class="option-box width-tbl">
										<p class="title">Total Shared Availed</p>
										<p class="description"><span data-label="main_total_shares_availed">0</span> Shares</p>
									</div>
								</td> -->
								<td>
									<div class="option-box width-tbl">
										<p class="title">Total Shares Unavailed</p>
										<p class="description"><span data-label="main_total_shares_unavailed">0</span> Shares</p>
									</div>
								</td>
							</tr>
						</tbody>


					</table>
					
				</div>
				
				<div class="clear"></div>

				<div class="text-right-line margin-top-30 margin-bottom-70">
					<div class="line"></div>
				</div>

				<div class="tab-panel" data-container="hr_department_esop_container">
					<div data-container="hr_department_esop_tab_container">
						<input type="radio" name="tabs" id="toggle-tab123" checked="checked" data-attr="" class="hr_department_esop_tab_input template hidden"/>
						<label for="toggle-tab123" data-attr="" data-label="hr_department_esop_name" class="hr_department_esop_tab_label template hidden">ESOP 12</label>
					</div>

					<div class="hr_department_esop template hidden">
						<div id="tab123" class="tab" data-attr="">
							<h2 class="margin-top-35 black-color">Total of <span data-label="hr_department_esop_name"></span><!-- ESOP 1 --><span data-label="company_name"></span></h2>				
							<div class="tbl-rounded">
								<table class="table-roxas tbl-display" data-container="esop_companies_container">
									<thead>
										<tr>
											<th>Price per Share</th>
											<th>Total Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Unavailed</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><span data-label="price_per_share">Php 2.49</span></td>
											<td><span data-label="company_alloted_shares">0</span> Shares</td>
											<td><span data-label="company_total_shares_availed">0</span></td>
											<td><span data-label="company_total_shares_unavailed">0</span></td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="tbl-rounded margin-top-30">
								<table class="table-roxas tbl-display">
									<thead>
										<tr>
											<th>Employee Name</th>
											<th>Employee Code</th>
											<th>Rank / Level</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
										</tr>
									</thead>
									<tbody data-container="esop_employees_container">
										<tr class="esop_employees template hidden">
											<td data-label="full_name">Juan Dela Cruz</td>
											<td data-label="employee_code">0001</td>
											<td data-label="rank_name">Executive</td>
											<!-- <td data-label="employee_alloted_shares">10,000</td> -->
											<td data-label="employee_shares_availed">0.00</td>
											<td data-label="employee_total_amount_availed">0.00</td>
										</tr>
										<!-- <tr>
											<td>0002</td>
											<td>Aaron Paul Labing-Lima</td>
											<td>President</td>
											<td>25,000,000</td>
											<td>25,000,000</td>
										</tr>
										<tr>
											<td>0003</td>
											<td>Eric Nilo</td>
											<td>Secretary</td>
											<td>25,000,000</td>
											<td>5,000,000</td>
										</tr>
										<tr>
											<td>0004</td>
											<td>Adrian Cedrick Lim</td>
											<td>Secretary</td>
											<td>25,000,000</td>
											<td>25,000,000</td>
										</tr>
										<tr>
											<td>0005</td>
											<td>Adrian Cedrick Lim</td>
											<td>Secretary</td>
											<td>25,000,000</td>
											<td>25,000,000</td>
										</tr> -->
										
										
									</tbody>
								</table>
							</div>
							<div data-container="additionals_container" class="hidden">
								<div class="panel-group text-left margin-top-10 padding-top-30">
									<div class="accordion_custom ">
										<div class="panel-heading border-10px">
											<a href="#">
												<h4 class="panel-title white-color active">							
													Additionals
													<i class="change-font fa fa-caret-right font-left"></i>
													<i class="fa fa-caret-down font-right"></i>							
												</h4>
											</a>																	
											<div class="clear"></div>					
										</div>					
										<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
											<div class="panel-body">

												<table class="table-roxas">
													<thead>
														<tr>
															<!-- <th>Company Code</th> -->
															<th>Employee Code</th>
															<th>Department Name</th>
															<th>Employee Name</th>
															<th>Rank / Level</th>
															<th>Grant Date</th>
															<th>Price Per Share</th>
															<th>Vesting Years</th>
														</tr>
													</thead>
													<tbody data-container="esop_special_container">
														<tr class="esop_special template hidden">
															<!-- <td data-label="company_code">0001</td> -->
															<td data-label="employee_code">0001</td>
															<td data-label="department_name">Office of the President</td>
															<td data-label="full_name">Juan Dela Cruz</td>
															<td data-label="rank_name">Executive</td>
															<td data-label="grant_date"></td>
															<td data-label="price_per_share"></td>
															<td data-label="vesting_years"></td>
														</tr>
													</tbody>
												</table>

											</div>			
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>

					<!-- <div id="tab2" class="tab">
					
						<h2 class="margin-top-35 black-color">Total of ESOP 1</h2>				
						<div class="tbl-rounded">
							<table class="table-roxas tbl-display">
								<thead>
									<tr>
										<th>Price per Share</th>
										<th>Total Alloted Shares</th>
										<th>Total Shares Availed</th>
										<th>Total Amount Unavailed</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Php 2.49</td>
										<td>30,000,000 Shares</td>
										<td>25,000,000</td>
										<td>5,000,000</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="tbl-rounded margin-top-30">
							<table class="table-roxas tbl-display">
								<thead>
									<tr>
										<th>Employee Code</th>
										<th>Employee Name</th>
										<th>Rank / Level</th>
										<th>Total Shares Availed</th>
										<th>Total Amount Availed</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>0001</td>
										<td>Joselito Salazar</td>
										<td>Chairman</td>
										<td>25,000,000</td>
										<td>5,000,000</td>
									</tr>
									<tr>
										<td>0002</td>
										<td>Aaron Paul Labing-Lima</td>
										<td>President</td>
										<td>25,000,000</td>
										<td>25,000,000</td>
									</tr>
									<tr>
										<td>0003</td>
										<td>Eric Nilo</td>
										<td>Secretary</td>
										<td>25,000,000</td>
										<td>5,000,000</td>
									</tr>
									<tr>
										<td>0004</td>
										<td>Adrian Cedrick Lim</td>
										<td>Secretary</td>
										<td>25,000,000</td>
										<td>25,000,000</td>
									</tr>
									<tr>
										<td>0005</td>
										<td>Adrian Cedrick Lim</td>
										<td>Secretary</td>
										<td>25,000,000</td>
										<td>25,000,000</td>
									</tr>
									
									
								</tbody>
							</table>
						</div>

				
					</div>

					<div id="tab3" class="tab">

						<h2 class="margin-top-35 black-color">Total of ESOP 1</h2>				
						<div class="tbl-rounded">
							<table class="table-roxas tbl-display">
								<thead>
									<tr>
										<th>Price per Share</th>
										<th>Total Alloted Shares</th>
										<th>Total Shares Availed</th>
										<th>Total Amount Unavailed</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Php 2.49</td>
										<td>30,000,000 Shares</td>
										<td>25,000,000</td>
										<td>5,000,000</td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="tbl-rounded margin-top-30">
							<table class="table-roxas tbl-display">
								<thead>
									<tr>
										<th>Employee Code</th>
										<th>Employee Name</th>
										<th>Rank / Level</th>
										<th>Total Shares Availed</th>
										<th>Total Amount Availed</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>0001</td>
										<td>Joselito Salazar</td>
										<td>Chairman</td>
										<td>25,000,000</td>
										<td>5,000,000</td>
									</tr>
									<tr>
										<td>0002</td>
										<td>Aaron Paul Labing-Lima</td>
										<td>President</td>
										<td>25,000,000</td>
										<td>25,000,000</td>
									</tr>
									<tr>
										<td>0003</td>
										<td>Eric Nilo</td>
										<td>Secretary</td>
										<td>25,000,000</td>
										<td>5,000,000</td>
									</tr>
									<tr>
										<td>0004</td>
										<td>Adrian Cedrick Lim</td>
										<td>Secretary</td>
										<td>25,000,000</td>
										<td>25,000,000</td>
									</tr>
									<tr>
										<td>0005</td>
										<td>Adrian Cedrick Lim</td>
										<td>Secretary</td>
										<td>25,000,000</td>
										<td>25,000,000</td>
									</tr>
									
									
								</tbody>
							</table>
						</div>

					</div> -->

				</div>
			</div>
			<div data-container="without_esop_dashboard" class="text-center margin-top-30 hidden">
			  <div class="content tect-center margin-top-40 text-center">      
			    <h2 class="font-400 font-styling">Welcome to RHI Employee Stock Options Plan (ESOP) System</h2>
			    <h2 class="font-400 font-styling margin-top-40 text-center">Currently there are no ESOPs added to the system. <br/> Please wait until the ESOP Administrator add an ESOP to the system. </h2>
			    <h2 class="font-400 margin-top-40 text-center font-styling">Once you have an ESOP ofered to you, you can:</h2>
			  <div>
			  <div class="content">
			    <div class="display-inline-mid width-300px margin-20px">
			      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="<?php echo base_url(); ?>/assets/images/handshake.png" alt=""  class="icons-dashboard"></div>
			      <p class="font-18 white-color margin-top-20">Distribute ESOP Shares</p>
			    </div>
			    <div class="display-inline-mid width-300px margin-20px">
			      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="<?php echo base_url(); ?>/assets/images/group-3.png" alt=""  class="icons-dashboard"></div>
			      <p class="font-18 white-color margin-top-20">Manage Employee ESOP Claims</p>
			    </div>
			    <div class="display-inline-mid width-300px margin-20px">
			      <div class=" height-150px width-150px border-all-smallest border-white add-border-radius-full text-center margin-center display-inline-mid padding-10px prnt"><img src="<?php echo base_url(); ?>/assets/images/file.png" alt=""  class="icons-dashboard"></div>
			      <p class="font-18 white-color margin-top-20">View ESOP Statement of Accounts and other documents of Employees</p>
			    </div>
			  </div>
			</div>
		</div>

	<div>
</section>

