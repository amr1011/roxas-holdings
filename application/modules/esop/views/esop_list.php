<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">ESOP List</h1>
			<button class="btn-normal f-right margin-top-20 modal-trigger" modal-target="add-esop">Add ESOP</button>
			<div class="clear"></div>
		</div>

		<div class="header-effect">

			<div class="display-inline-mid default">
				<p class="white-color margin-bottom-5">Search</p>
				<div>
					<div class="select add-radius display-inline-mid" id="search_esop_dropdown">
						<select>
							<option value="ESOP Name">ESOP Name</option>
							<option value="Vesting Years">Vesting Years</option>
							<option value="Grant Date">Grant Date</option>
							<option value="Share QTY">Price per Share</option>
						</select>
					</div>
					<div class="display-inline-mid search-me">
						<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px search_esop_name_input"/>
						<button class="btn-normal display-inline-mid margin-left-10 search_esop_btn">Search</button>
					</div>
					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px search_vesting_years_input"/>
						<button class="btn-normal display-inline-mid margin-left-10 search_esop_btn">Search</button>
					</div>
				</div>
			</div>

			<div class="display-inline-mid grant-date">
				<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
				<div>
					<label class="display-inline-mid margin-left-20">From</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_from_input">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<label class="display-inline-mid margin-left-10">To</label>
					<div class="date-picker add-radius display-inline-mid margin-left-10">
						<input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_to_input">
						<span class="fa fa-calendar text-center"></span>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10 search_esop_btn">Search</button>
				</div>
			</div>

			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<!-- <div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div> -->
				<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px search_price_per_share_input"/>
				<button class="btn-normal display-inline-mid margin-left-10 search_esop_btn">Search</button>				
			</div>
			
		</div>
		
		<div class="text-right-line margin-top-30">
			<div class="view-by">
				<p>View By: 
				<i class="fa fa-th-large grid"></i>
				<i class="fa fa-bars list"></i>
				</p>

			</div>
			<div class="line"></div>
			
			<div class="content-text" id="sort_esop">				
				<p class="font-15 white-color display-inline-mid">Sort By: <a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-esop-name">ESOP Name <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-esop-grant-date">Grant Date <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-esop-price-per-share">Price per Share <i class="fa fa-chevron-down"></i></a></p>
				<span class="margin-left-10 margin-right-10 white-color">|</span>
				<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-esop-vesting-year">Vesting Year <i class="fa fa-chevron-down"></i></a></p>
			</div>
		</div>
	</div>
</section>

<section section-style="content-panel">

	<div class="content padding-top-30">

		<div class="grid-content" data-container="esop_view_by_grid">

			<div class="data-box padding-20-30px divide-by-2 esop_grid template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3 data-label="esop_name">ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted: <span data-label="grant_date">Sept 10, 2016</span></td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td><span data-label="total_share_qty">500,000</span> Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right"><span data-label="vesting_years">5</span> Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td data-label="price_per_share">Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="javascript:void(0)" class="grid_view_esop"><button class="btn-normal">View ESOP</button></a><!-- <?php echo base_url(); ?>esop/view_esop -->
				</div>
			</div>
			<div class="data-box width-100per padding-20-30px no_results_found template hidden">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="text-center">No Results found</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- <div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 2</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 3</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>E-ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ROXAS ESOP</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>PESO ESOP 1</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div>

			<div class="data-box padding-20-30px divide-by-2">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td colspan="2"><h3>ESOP 15</h3></td>
							<td colspan="2" class="text-right">Granted Sept 10, 2016</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Share Holding:</td>
							<td>500,000 Shares</td>
							<td class="text-right">Vesting Years:</td>
							<td class="text-right">5 Years</td>
						</tr>
						<tr>
							<td class="padding-top-10 padding-bottom-10">Price per Share</td>
							<td>Php 6.00</td>
						</tr>
					</tbody>
				</table>
				<div class="data-hover text-center">
					<a href="<?php echo base_url(); ?>esop/view_esop"><button class="btn-normal">View ESOP</button></a>
				</div>
			</div> -->

		</div>

		<div class="tbl-rounded margin-top-20 table-content">
			<table class="table-roxas tbl-display">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date Granted</th>
						<th>Total Share Quantity</th>
						<th>Price per Share</th>
						<th>Vesting</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody data-container="esop_view_by_list">
					<tr class="esop_list template hidden">
						<td data-label="esop_name">ESOP 1</td>
						<td data-label="grant_date">Sept 10, 2015</td>
						<td><span data-label="total_share_qty">500,000</span> Shares</td>
						<td data-label="price_per_share">Php 6.00</td>
						<td><span data-label="vesting_years">5</span> Years</td>
						<td><a href="javascript:void(0)" class="list_view_esop">View ESOP</a></td><!-- <?php echo base_url(); ?>esop/view_esop -->
					</tr>
					<tr class="no_results_found template hidden">
						<td colspan="6">No Results found</td>
					</tr>
					<!-- <tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View ESOP</a></td>
					</tr>
					<tr>
						<td>ESOP 1</td>
						<td>Sept 10, 2015</td>
						<td>500,000 Shares</td>
						<td>Php 6.00</td>
						<td>5 Years</td>
						<td><a href="<?php echo base_url(); ?>esop/view_esop">View ESOP</a></td>
					</tr> -->
					<tr class="last-content">
						<td colspan="6"></td>
					</tr>
				</tbody>
			</table>



		</div>


	<div>
</section>

<!-- add ESOP -->
<div class="modal-container" modal-id="add-esop">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">ADD ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
			<!-- <div class="error">Total Share Quantity is invalid. <br /> Please type numbers only in the textbox</div> -->
			<div class="error add_esop_error_message hidden margin-bottom-10">
			</div>
			<div class="success add_esop_success_message hidden margin-bottom-10">
			</div>

			<form id="add_esop_form">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="width-200px">ESOP Name:</td>
							<td><input type="text" class="small width-250px add-border-radius-5px" name="esop_name" datavalid="required" labelinput="Esop Name"/></td>
						</tr>
						<tr>
							<td class="padding-top-10">Grant Date:</td>
							<td>
								<div class="date-picker display-inline-mid add-radius ">
									<input type="text" data-date-format="MM/DD/YYYY" class="width-210px" name="grant_date" datavalid="required" labelinput="Grant Date">
									<span class="fa fa-calendar text-center"></span>
								</div>
							</td>
						</tr>
						<tr>
							<td class="padding-bottom-40">Total Share Quantity:</td>
							<td>
								<input type="text" class="normal add-border-radius-5px width-250px input_numeric" name="total_share_qty" datavalid="required" labelinput="Total Share Quantity"/>
								<p class="display-inline-mid margin-left-10">Shares</p>
								<label class="black-color font-15 txt-normal margin-top-10 share-lbl">
									<input type="checkbox" class="zoom1-2" id="share_allotment_checkbox"/>
										Share Allotment
								</label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								
								<div class="add-esop-dash">
									<table>
										<thead>
											<tr>
												<th>ESOP Name</th>
												<th>Remaining Share QTY</th>
												<th>Shares to be Taken</th>						
											</tr>
										</thead>
										<tbody data-container="share_allotment">
											<tr data-template="share_allotment" class="template">
												<td>
													<label class="black-color font-15 txt-normal">
														<input type="checkbox" name="esop_name" data-label="esop_id" class="margin-right-10 zoom1-2"><data data-label="esop_name">ESOP 1</data>
													</label>							
												</td>
												<td><data data-label="esop_unavailed">826,985.08</data></td>
												<td><input type="text" class="small add-border-radius-5px share_allotment_input" value="" /></td>
											</tr>
<!--											<tr>-->
<!--												<td>-->
<!--													<label class="black-color font-15 txt-normal">-->
<!--														<input type="checkbox" name="option" class="margin-right-10 zoom1-2">ESOP 2-->
<!--													</label>-->
<!--												</td>-->
<!--												<td>25,500,000.00</td>-->
<!--												<td><input type="text" class="small add-border-radius-5px" /></td>-->
<!--											</tr>					-->
										</tbody>
									</table>
								</div>

							</td>
						</tr>
						<tr>
							<td class="padding-top-10">Price per Share:</td>
							<td>
								<input type="text" class="normal add-border-radius-5px width-250px input_numeric" name="price_per_share" datavalid="required" labelinput="Price Per Share"/>
								<div class="select xsmall display-inline-mid margin-left-10 add-radius" id="currency_dropdown">
									<select>
										<!-- <option value="PHP">PHP</option>
										<option value="USD">USD</option>
										<option value="CAD">CAD</option>
										<option value="HKD">HKD</option>
										<option value="INR">INR</option> -->
									</select>
							</div>
							</td>
						</tr>
						<tr>
							<td class="padding-top-10">Vesting Years:</td>
							<td>
								<input type="text" class="normal add-border-radius-5px width-250px input_numeric" name="vesting_years" datavalid="required" labelinput="Vesting Years"/>
								<p class="display-inline-mid margin-left-10">Years</p>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
			<!-- <div class="">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div> -->
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			
			<button type="button" class="display-inline-mid btn-dark" id="add_esop_btn">Add ESOP</button>
		</div>
		<div class="clear"></div>
	</div>
</div>