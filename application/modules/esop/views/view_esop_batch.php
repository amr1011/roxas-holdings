<?php $user_role = $this->session->userdata('user_role'); ?>
<div data-container="esop_information">
	<section section-style="top-panel">
		<div class="content">
			<div>
				<h1 class="f-left hidden">ESOP View</h1>
				<div class="breadcrumbs margin-bottom-20 border-10px">
					<a href="<?php echo base_url(); ?>esop/esop_list">ESOP</a>
					<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
					<a data-label="esop_name">ESOP 1</a>
				</div>
				<div class="f-right">
					<button class="btn-normal btn-extend-offer-expiration modal-trigger margin-right-10" modal-target="extend-offer-expiration">Extend Expiration</button>	
					<!-- <button class="btn-normal margin-right-10">Print Analytics Report</button>				 -->
					<!-- <a href="<?php echo base_url(); ?>esop/download_payment_record_template">
						<button class="btn-normal margin-top-20 margin-right-10">Download Payment Template</button>
					</a>
					<button class="btn-normal modal-trigger margin-right-10" modal-target="upload-payment">Upload Payment Records</button>	 -->
					<button class="btn-normal margin-right-10 modal-trigger" modal-target="esop-batch">Create ESOP Batch</button>
					<button class="btn-normal modal-trigger" modal-target="add-new-employee">Add New Employee</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section section-style="content-panel">	
		

		<div class="content ">
			
			<div class="margin-bottom-20 margin-top-20">
				<div data-container="esop_total_shares_container">
					<div class="f-left width-100per">
						<table class="width-100per">
							<tbody>
								<tr>
									<td>
										<div class="option-box width-tbl">
											<p class="title">Price Per Share</p>
											<p class="description" data-label="price_per_share">2.49</p>
										</div>
									</td>
									<td>
										<div class="option-box width-tbl">
											<p class="title">Total Alloted Shares</p>
											<p class="description"><span data-label="total_share_qty">0</span> Shares</p>
										</div>
									</td>
									<td rowspan="2" class="width-400px">
										<div class="with-txt">
											<div class="svg-cont chart-container">
												<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 400 400"></svg>

												
												<div class="data-container">
													<span data-value="35.5%" class="">Total Shares Availed</span>										
													<span data-value="64.5%" class="margin-top-30">Total Shares Unavailed</span>										
												</div>		
												<!-- <div class="graph-txt">
													<p class="higher">35.5 %</p>
													<p class="lower">64.5 %</p>
												</div>	 -->												
											</div>												
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="option-box width-tbl">
											<p class="title">Total Shared Availed</p>
											<p class="description"><span data-label="esop_total_shares_availed">0</span> Shares</p>
										</div>
									</td>
									<td>
										<div class="option-box width-tbl">
											<p class="title">Total Shared Unavailed</p>
											<p class="description"><span data-label="esop_total_shares_unavailed">0</span> Shares</p>
										</div>
									</td>						
								</tr>
							</tbody>
						</table>
						
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="text-right-line margin-bottom-55">				
				<div class="line"></div>								
			</div>
				
			<h2 class="f-left  white-color" data-label="esop_name">ESOP 1</h2>
			<h2 class="f-right  white-color">Granted: <span data-label="grant_date">September 10, 2015</span></h2>
			<div class="clear"></div>

		<div class="tab-panel margin-top-20">
			
			<div data-container="esop_batch_container">
				<div data-container="esop_batch_tab_container">
					<input type="radio" name="tabs" id="toggle-tab123" checked="checked" data-attr="" class="esop_batch_tab_input template hidden"/>
					<label for="toggle-tab123" data-attr="" data-label="esop_batch_name" class="esop_batch_tab_label template hidden">ESOP 12</label>
						
					<!-- <input type="radio" name="tabs" id="toggle-tab234" data-attr="esop_batch_group_234"/>
					<label for="toggle-tab234" data-attr="esop_batch_group_234">ESOP 123</label> -->
				</div>

				<div class="esop_batch template hidden">
					<div id="tab123" class="tab" data-attr="">
						<div class="undistributed_esop hidden">
							<div class="f-right">
								<a href="javascript:void(0)"><!-- <?php echo base_url(); ?>esop/add_distribute_shares -->
									<button class="btn-normal margin-right-10 batch_add_distribute_shares">Distribute Shares</button>
								</a>
								<a href="<?php echo base_url(); ?>esop/download_share_distribution_template">
									<button class="btn-normal margin-top-20 margin-right-10">Download Share Distribution Template</button>
								</a>
								<button class="btn-normal margin-right-10 modal-trigger" modal-target="batch-share-distribution-template">Upload Share Distribution Template</button>
								<button class="btn-normal modal-trigger edit_esop_batch_btn" modal-target="edit-esop-batch">Edit ESOP</button>				
							</div>
						</div>
						<div class="distributed_esop hidden">
							<div class="f-right">
								<button class="btn-normal margin-right-10 modal-trigger" modal-target="offer-expiration-batch">Set Offer Expiration</button>
								<a href="<?php echo base_url(); ?>esop/download_share_distribution_template">
									<button class="btn-normal margin-top-20 margin-right-10">Download Share Distribution Template</button>
								</a>
								<button class="btn-normal margin-right-10 modal-trigger" modal-target="batch-share-distribution-template">Upload Share Distribution Template</button>
								<a href="javascript:void(0)"><!-- <?php echo base_url(); ?>esop/edit_distribute_shares -->
									<button class="btn-normal margin-right-10 batch_edit_distribute_shares">Edit Distribute Shares</button>				
								</a>
							</div>
						</div>
						<div class="expiration_set hidden">
							<div class="f-right">
								<button class="btn-normal modal-trigger" modal-target="send-letter-batch">Send Offer Letter</button>
							</div>
						</div>
						<div class="clear"></div>
						<div data-container="esop_batch_total_shares_container">

							<div class="option-box trio margin-top-10">
								<p class="title">Total Alloted Shares</p>
								<p class="description"><span data-label="total_share_qty">0</span> Shares</p>
							</div>
							
							<div class="option-box trio margin-top-10">
								<p class="title">Price Per Share</p>
								<p class="description" data-label="price_per_share">Php 2.49</p>
							</div>

							<div class="option-box trio margin-top-10">
								<p class="title">Vesting Years</p>
								<p class="description"><span data-label="vesting_years">0</span> Years</p>
							</div>

							<div class="option-box trio">
								<p class="title">No. of Employees Accepted</p>
								<p class="description"><span data-label="esop_total_employees_accepted">0</span> Employees</p>
							</div>

							<div class="option-box trio">
								<p class="title">Total Shares Availed</p>
								<p class="description"><span data-label="esop_total_shares_availed">0</span> Shares</p>
							</div>
							
							<div class="option-box trio">
								<div data-container="shares_taken" class="hidden">
									<div class="tool-tip shares_taken_by_other_esop_tool_tip" tt-html="<p>Shares Taken by:</p><p><small>26,208 Shares</small></p>">
										<p class="title">Total Shares Unavailed <i class="warn fa fa-exclamation-triangle" aria-hidden="true"></i></p>
										<p class="description"><span data-label="esop_total_shares_unavailed">0</span> Shares</p>
									</div>
								</div>
								<div data-container="shares_not_taken">
									<p class="title">Total Shares Unavailed</p>
									<p class="description"><span data-label="esop_total_shares_unavailed">0</span> Shares</p>
								</div>
							</div>
						</div>
						<div data-container="esop_companies_container">
							<div class="esop_companies template hidden">
								<div class="text-right-line margin-top-25 margin-bottom-30">
									<div class="line"></div>
								</div>

								<h2 class="f-left margin-top-30 black-color" data-label="company_name">CAPDI.HO</h2>		
								<div class="clear"></div>

								<div class="option-box trio">
									<p class="title">Total Alloted Shares</p>
									<p class="description"><span data-label="company_alloted_shares">0</span> Shares</p>
								</div>
								<div class="option-box trio">
									<p class="title">Price per Share</p>
									<p class="description" data-label="price_per_share">Php 2.49</p>
								</div>
								<div class="option-box trio">
									<p class="title">Vesting Years</p>
									<p class="description"><span data-label="vesting_years">0</span> Years</p>
								</div>

								<div class="option-box trio">
									<p class="title">No. of Employee Accepted</p>
									<p class="description"><span data-label="company_total_employees_accepted">0</span> Employees</p>
								</div>
								<div class="option-box trio">
									<p class="title">Total Shared Availed</p>
									<p class="description"><span data-label="company_total_shares_availed">0</span> Shares</p>
								</div>
								<div class="option-box trio">
									<p class="title">Total Shared Unavailed</p>
									<p class="description"><span data-label="company_total_shares_unavailed">0</span> Shares</p>
								</div>
								<div class="panel-group text-left margin-top-10 padding-top-30">

									<div class="accordion_custom ">
										<div class="panel-heading border-10px">
											<a href="#">
												<h4 class="panel-title white-color active">							
													Employee
													<i class="change-font fa fa-caret-right font-left"></i>
													<i class="fa fa-caret-down font-right"></i>							
												</h4>
											</a>																	
											<div class="clear"></div>					
										</div>					
										<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
											<div class="panel-body">

												<table class="table-roxas">
													<thead>
														<tr>
															<th>Company Code</th>
															<th>Employee Name</th>
															<th>Employee Code</th>
															<th>Department Name</th>
															<th>Rank / Level</th>
															<th>Alloted Shares</th>
															<th>Total Shares Availed</th>
															<th>Total Amount Availed</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody data-container="esop_employees_container">
														<tr class="esop_employees template hidden">
															<td data-label="company_code">0001</td>
															<td data-label="full_name">Juan Dela Cruz</td>
															<td data-label="employee_code">0001</td>
															<td data-label="department_name">Office of the President</td>
															<td data-label="rank_name">Executive</td>
															<td data-label="employee_alloted_shares">10,000</td>
															<td data-label="employee_shares_availed">0.00</td>
															<td data-label="employee_total_amount_availed">0.00</td>
															<td><a href="javascript:void(0)" class="go_to_view_employee_by_esop_batch">View</a></td>
														</tr>
													</tbody>
												</table>

											</div>			
										</div>
									</div>	
								</div>
								<div data-container="additionals_container" class="hidden">
									<div class="panel-group text-left margin-top-10 padding-top-30">
										<div class="accordion_custom ">
											<div class="panel-heading border-10px">
												<a href="#">
													<h4 class="panel-title white-color active">							
														Additionals
														<i class="change-font fa fa-caret-right font-left"></i>
														<i class="fa fa-caret-down font-right"></i>							
													</h4>
												</a>																	
												<div class="clear"></div>					
											</div>					
											<div class="panel-collapse in border-10px margin-top-20 margin-bottom-20">								
												<div class="panel-body">

													<table class="table-roxas">
														<thead>
															<tr>
																<!-- <th>Company Code</th> -->
																<th>Employee Code</th>
																<th>Department Name</th>
																<th>Employee Name</th>
																<th>Rank / Level</th>
																<th>Grant Date</th>
																<th>Price Per Share</th>
																<th>Vesting Years</th>
															</tr>
														</thead>
														<tbody data-container="esop_special_container">
															<tr class="esop_special template hidden">
																<!-- <td data-label="company_code">0001</td> -->
																<td data-label="employee_code">0001</td>
																<td data-label="department_name">Office of the President</td>
																<td data-label="full_name">Juan Dela Cruz</td>
																<td data-label="rank_name">Executive</td>
																<td data-label="grant_date"></td>
																<td data-label="price_per_share"></td>
																<td data-label="vesting_years"></td>
															</tr>
														</tbody>
													</table>

												</div>			
											</div>
										</div>	
									</div>
								</div>
							</div>
						</div>

<!-- 						<div class="text-right-line margin-bottom-50 margin-top-25">				
							<div class="line"></div>								
						</div>
						
						<div class=" margin-top-20">
							<h2 class="f-left black-color">CACI</h2>					
							<div class="clear"></div>
						</div>

						<div class="option-box trio margin-top-10">
							<p class="title">Total Alloted Shares</p>
							<p class="description">500,000 Shares</p>
						</div>
						
						<div class="option-box trio margin-top-10">
							<p class="title">Price Per Share</p>
							<p class="description">Php 2.49</p>
						</div>

						<div class="option-box trio margin-top-10">
							<p class="title">Vesting Years</p>
							<p class="description">2 Years</p>
						</div>

						<div class="option-box trio">
							<p class="title">No. of Employees Accepted</p>
							<p class="description">53 Employees</p>
						</div>

						<div class="option-box trio">
							<p class="title">Total Shares Availed</p>
							<p class="description">250,000 Shares</p>
						</div>
						
						<div class="option-box trio">
							<p class="title">Total Shares Unavailed</p>
							<p class="description">250,000 Shares</p>
						</div>

						
						<div class="panel-group text-left margin-top-50 ">
							
							<div class="accordion_custom ">
								<div class="panel-heading border-10px">
									<a href="#">
										<h4 class="panel-title white-color active">							
											Employee
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
									<div class="panel-body">

										<table class="table-roxas">
											<thead>
												<tr>
													<th>Company Code</th>
													<th>Employee Code</th>
													<th>Department Name</th>
													<th>Employee Name</th>
													<th>Rank / Level</th>
													<th>Alloted Shares</th>
													<th>Total Shares Availed</th>
													<th>Total Amount Availed</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="ESOP-view-employee.php">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						
							<div class="text-right-line  margin-bottom-55">				
								<div class="line"></div>								
							</div>
							
							<h2 class="black-color black-color">CAPDI.HO</h2>

							<div class="option-box trio margin-top-10">
								<p class="title">Total Alloted Shares</p>
								<p class="description">500,000 Shares</p>
							</div>
							
							<div class="option-box trio margin-top-10">
								<p class="title">Price Per Share</p>
								<p class="description">Php 2.49</p>
							</div>
							<div class="option-box trio margin-top-10">
								<p class="title">Vesting Years</p>
								<p class="description">2 Years</p>
							</div>
							<div class="option-box trio">
								<p class="title">No. of Employees Accepted</p>
								<p class="description">53 Employees</p>
							</div>
							<div class="option-box trio">
								<p class="title">Total Shares Availed</p>
								<p class="description">250,000 Shares</p>
							</div>
							
							<div class="option-box trio">
								<p class="title">Total Shares Unavailed</p>
								<p class="description">250,000 Shares</p>
							</div>
								
							<div class="accordion_custom margin-top-30">
								<div class="panel-heading border-10px">
									<a href="#">
										<h4 class="panel-title white-color active">							
											Employee
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
									<div class="panel-body">

										<table class="table-roxas">
											<thead>
												<tr>
													<th>Company Code</th>
													<th>Employee Code</th>
													<th>Department Name</th>
													<th>Employee Name</th>
													<th>Rank / Level</th>
													<th>Alloted Shares</th>
													<th>Total Shares Availed</th>
													<th>Total Amount Availed</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	 -->
					
							<div class="text-right-line margin-bottom-60 margin-top-25">
								<div class="line"></div>
							</div>

							<!-- <div class="margin-bottom-20">
								<h2 class=" black-color f-left margin-top-0 margin-bottom-0">Gratuity List</h2>
								<button type="button" class="btn-normal f-right modal-trigger" modal-target="add-gratuity"> Add Gratuity</button>
								<div class="clear"></div>
							</div>
							
							<div class="tbl-rounded">
								<table class="table-roxas tbl-display margin-top-20">
									<thead>
										<tr>
											<th>Date Added</th>
											<th>Dividend Date</th>
											<th>Price per Share</th>
											<th>Total Value of Gratuity Given</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody data-container="gratuity_list_container">
										<tr class="tmp_gratuity_list template hidden">
											<td data-label="grant_date"></td>
											<td data-label="dividend_date"></td>
											<td data-label="price_per_share"></td>
											<td data-label="total_value_of_gratuity_given"></td>
											<td data-label="status"></p></td>
											<td>
		
							
											</td>
										</tr>
										<tr class="no_results_found">
											<td colspan="8">No Results found</td>
										</tr>
							
										
									</tbody>
								</table>
							</div> -->

							<!-- <div class="text-right-line margin-top-25 margin-bottom-70" >
								<div class="line"></div>
							</div> -->

							<div data-container="audit_logs" class="hidden">
								<div class="panel-group text-left margin-top-30">
									<div class="accordion_custom">
										<div class="panel-heading border-10px">
											<a href="#">
												<h4 class="panel-title white-color">							
													Audit Logs
													<i class="change-font fa fa-caret-right font-left"></i>
													<i class="fa fa-caret-down font-right"></i>							
												</h4>
											</a>																	
											<div class="clear"></div>					
										</div>					
										<div class="panel-collapse border-10px margin-top-20 margin-bottom-20">								
											<div class="panel-body ">

												<table class="table-roxas">
													<thead>
														<tr>
															<th>Date of Activity</th>
															<th>User</th>
															<th>Activity Description</th>
														</tr>
													</thead>
													<tbody data-container="logs">
														<tr class="logs template hidden">
															<td data-label="date_created">September 10, 2015</td>
															<td data-label="created_by">ROXAS, PEDRO OLGADO</td>
															<td data-label="value">Created Company<span class="font-bold">"Cr8v Web Solutions Inc."</span></td>
														</tr>
														<!-- <tr>
															<td>September 10, 2015</td>
															<td>VALENCIA, RENATO CRUZ</td>
															<td>Created Department  <span class="font-bold">"HR Department"</span></td>
														</tr> -->
													</tbody>
												</table>
											</div>			
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- <div class="esop_batch">
					<div id="tab234" class="tab" data-attr="esop_batch_group_234">

						<div class="option-box trio margin-top-10">
							<p class="title">Total Alloted Shares</p>
							<p class="description">500,000 Shares</p>
						</div>
						
						<div class="option-box trio margin-top-10">
							<p class="title">Price Per Share</p>
							<p class="description">Php 2.49</p>
						</div>

						<div class="option-box trio margin-top-10">
							<p class="title">Vesting Years</p>
							<p class="description">2 Years</p>
						</div>

						<div class="option-box trio">
							<p class="title">No. of Employees Accepted</p>
							<p class="description">53 Employees</p>
						</div>

						<div class="option-box trio">
							<p class="title">Total Shares Availed</p>
							<p class="description">250,000 Shares</p>
						</div>
						
						<div class="option-box trio">
							<p class="title">Total Shares Unavailed</p>
							<p class="description">250,000 Shares</p>
						</div>
						

						<div class="text-right-line margin-bottom-50 margin-top-25">				
							<div class="line"></div>								
						</div>
						
						<div class=" margin-top-20">
							<h2 class="f-left black-color">CACI</h2>					
							<div class="clear"></div>
						</div>

						<div class="option-box trio margin-top-10">
							<p class="title">Total Alloted Shares</p>
							<p class="description">500,000 Shares</p>
						</div>
						
						<div class="option-box trio margin-top-10">
							<p class="title">Price Per Share</p>
							<p class="description">Php 2.49</p>
						</div>

						<div class="option-box trio margin-top-10">
							<p class="title">Vesting Years</p>
							<p class="description">2 Years</p>
						</div>

						<div class="option-box trio">
							<p class="title">No. of Employees Accepted</p>
							<p class="description">53 Employees</p>
						</div>

						<div class="option-box trio">
							<p class="title">Total Shares Availed</p>
							<p class="description">250,000 Shares</p>
						</div>
						
						<div class="option-box trio">
							<p class="title">Total Shares Unavailed</p>
							<p class="description">250,000 Shares</p>
						</div>

						
						<div class="panel-group text-left margin-top-50 ">
							
							<div class="accordion_custom ">
								<div class="panel-heading border-10px">
									<a href="#">
										<h4 class="panel-title white-color active">							
											Employee
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
									<div class="panel-body">

										<table class="table-roxas">
											<thead>
												<tr>
													<th>Company Code</th>
													<th>Employee Code</th>
													<th>Department Name</th>
													<th>Employee Name</th>
													<th>Rank / Level</th>
													<th>Alloted Shares</th>
													<th>Total Shares Availed</th>
													<th>Total Amount Availed</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="ESOP-view-employee.php">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
						
							<div class="text-right-line  margin-bottom-55">				
								<div class="line"></div>								
							</div>
							
							<h2 class="black-color black-color">CAPDI.HO</h2>

							<div class="option-box trio margin-top-10">
								<p class="title">Total Alloted Shares</p>
								<p class="description">500,000 Shares</p>
							</div>
							
							<div class="option-box trio margin-top-10">
								<p class="title">Price Per Share</p>
								<p class="description">Php 2.49</p>
							</div>
							<div class="option-box trio margin-top-10">
								<p class="title">Vesting Years</p>
								<p class="description">2 Years</p>
							</div>
							<div class="option-box trio">
								<p class="title">No. of Employees Accepted</p>
								<p class="description">53 Employees</p>
							</div>
							<div class="option-box trio">
								<p class="title">Total Shares Availed</p>
								<p class="description">250,000 Shares</p>
							</div>
							
							<div class="option-box trio">
								<p class="title">Total Shares Unavailed</p>
								<p class="description">250,000 Shares</p>
							</div>
								
							<div class="accordion_custom margin-top-30">
								<div class="panel-heading border-10px">
									<a href="#">
										<h4 class="panel-title white-color active">							
											Employee
											<i class="change-font fa fa-caret-right font-left"></i>
											<i class="fa fa-caret-down font-right"></i>							
										</h4>
									</a>																	
									<div class="clear"></div>					
								</div>					
								<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
									<div class="panel-body">

										<table class="table-roxas">
											<thead>
												<tr>
													<th>Company Code</th>
													<th>Employee Code</th>
													<th>Department Name</th>
													<th>Employee Name</th>
													<th>Rank / Level</th>
													<th>Alloted Shares</th>
													<th>Total Shares Availed</th>
													<th>Total Amount Availed</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
												<tr>
													<td>0001</td>
													<td>0001</td>
													<td>Office of the President</td>
													<td>Juan Dela Cruz</td>
													<td>Executive</td>
													<td>10,000</td>
													<td>0.00</td>
													<td>Php 125,000.00</td>
													<td><a href="#">View</a></td>
												</tr>
											</tbody>
										</table>

									</div>			
								</div>
							</div>	
					
							<div class="text-right-line margin-bottom-60 margin-top-25">
								<div class="line"></div>
							</div>

							<div class="margin-bottom-20">
								<h2 class=" black-color f-left margin-top-0 margin-bottom-0">Gratuity List</h2>
								<button type="button" class="btn-normal f-right" > Add Gratuity</button>
								<div class="clear"></div>
							</div>

							<div class="tbl-rounded">
								<table class="table-roxas tbl-display margin-top-20">
									<thead>
										<tr>
											<th>Date Added</th>
											<th>Price per Share</th>
											<th>Total Value of Gratuity Given</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>October 10, 2015</td>
											<td>Php 0.60 / Share</td>
											<td>Php 1,300,215.15</td>
											<td><p>Pending Approval</p></td>
											<td>
												<a class="modal-trigger default-cursor" modal-target="approved-grat">Approve</a>
												<span class="margin-left-10 margin-right-10"> | </span>
												<a class="red-color modal-trigger default-cursor" modal-target="reject-grant">Reject</a>
											</td>
										</tr>
										<tr>
											<td>September 30, 2015</td>
											<td>Php 0.60 / Share</td>
											<td>Php 1,300,215.15</td>
											<td><p class="green-color">Approved</p></td>
											<td>
												<a href="#">Approve</a>
												<span class="margin-left-10 margin-right-10"> | </span>
												<a href="#" class="red-color">Reject</a>
											</td>
										</tr>
										<tr>
											<td>September 30, 2015</td>
											<td>Php 0.60 / Share</td>
											<td>Php 1,300,215.15</td>
											<td><p class="red-color">Rejected</p></td>
											<td>
												<a class="modal-trigger" modal-target="reject-reason">View Rejected Reason</a>
											</td>
										</tr>
										<tr>
											<td>September 20, 2015</td>
											<td>Php 0.60 / Share</td>
											<td>Php 1,300,215.15</td>
											<td><p class="green-color">Gratuity Sent</p></td>
											<td>
											
											</td>
										</tr>
										
										
									</tbody>
								</table>
							</div>

							<div class="text-right-line margin-top-25 margin-bottom-70" >
								<div class="line"></div>
							</div>

							<div data-container="audit_logs" class="hidden">
								<div class="accordion_custom margin-top-30">
									<div class="panel-heading border-10px">
										<a href="#">
											<h4 class="panel-title white-color active">							
												Audit Logs
												<i class="change-font fa fa-caret-right font-left"></i>
												<i class="fa fa-caret-down font-right"></i>							
											</h4>
										</a>																	
										<div class="clear"></div>					
									</div>					
									<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
										<div class="panel-body ">

											<table class="table-roxas">
												<thead>
													<tr>
														<th>Date of Activiy</th>
														<th>User</th>
														<th>Shares Offered</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>September 10, 2015</td>
														<td>ROXAS, PEDRO OLGADO</td>
														<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
													</tr>
													<tr>
														<td>September 10, 2015</td>
														<td>VALENCIA, RENATO CRUZ</td>
														<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
													</tr>
												
												</tbody>
											</table>

										</div>			
									</div>
								</div>	
							</div>
						</div>
						
					</div>
				</div> -->
			</div>
			<!-- <input type="radio" name="tabs" id="toggle-tab1" checked="checked" />
			<label for="toggle-tab1">ESOP 1</label>

			<input type="radio" name="tabs" id="toggle-tab2" />
			<label for="toggle-tab2">ESOP 2</label>

			<input type="radio" name="tabs" id="toggle-tab3" />
			<label for="toggle-tab3">ESOP 3</label>


			<div id="tab1" class="tab">

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">Php 2.49</p>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>

				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>

				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>
				

				<div class="text-right-line margin-bottom-50 margin-top-25">				
					<div class="line"></div>								
				</div>
				
				<div class=" margin-top-20">
					<h2 class="f-left black-color">CACI</h2>					
					<div class="clear"></div>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">Php 2.49</p>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>

				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>

				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>

				
				<div class="panel-group text-left margin-top-50 ">
					
					<div class="accordion_custom ">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="ESOP-view-employee.php">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
				
					<div class="text-right-line  margin-bottom-55">				
						<div class="line"></div>								
					</div>
					
					<h2 class="black-color black-color">CAPDI.HO</h2>

					<div class="option-box trio margin-top-10">
						<p class="title">Total Alloted Shares</p>
						<p class="description">500,000 Shares</p>
					</div>
					
					<div class="option-box trio margin-top-10">
						<p class="title">Price Per Share</p>
						<p class="description">Php 2.49</p>
					</div>
					<div class="option-box trio margin-top-10">
						<p class="title">Vesting Years</p>
						<p class="description">2 Years</p>
					</div>
					<div class="option-box trio">
						<p class="title">No. of Employees Accepted</p>
						<p class="description">53 Employees</p>
					</div>
					<div class="option-box trio">
						<p class="title">Total Shares Availed</p>
						<p class="description">250,000 Shares</p>
					</div>
					
					<div class="option-box trio">
						<p class="title">Total Shares Unavailed</p>
						<p class="description">250,000 Shares</p>
					</div>
						
					<div class="accordion_custom margin-top-30">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
			
					<div class="text-right-line margin-bottom-60 margin-top-25">
						<div class="line"></div>
					</div>

					<div class="margin-bottom-20">
						<h2 class=" black-color f-left margin-top-0 margin-bottom-0">Gratuity List</h2>
						<button type="button" class="btn-normal f-right" > Add Gratuity</button>
						<div class="clear"></div>
					</div>

					<div class="tbl-rounded">
						<table class="table-roxas tbl-display margin-top-20">
							<thead>
								<tr>
									<th>Date Added</th>
									<th>Price per Share</th>
									<th>Total Value of Gratuity Given</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>October 10, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p>Pending Approval</p></td>
									<td>
										<a class="modal-trigger default-cursor" modal-target="approved-grat">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a class="red-color modal-trigger default-cursor" modal-target="reject-grant">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Approved</p></td>
									<td>
										<a href="#">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a href="#" class="red-color">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="red-color">Rejected</p></td>
									<td>
										<a class="modal-trigger" modal-target="reject-reason">View Rejected Reason</a>
									</td>
								</tr>
								<tr>
									<td>September 20, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Gratuity Sent</p></td>
									<td>
									
									</td>
								</tr>
								
								
							</tbody>
						</table>
					</div>

					<div class="text-right-line margin-top-25 margin-bottom-70" >
						<div class="line"></div>
					</div>

					<div data-container="audit_logs" class="hidden">
						<div class="accordion_custom margin-top-30">
							<div class="panel-heading border-10px">
								<a href="#">
									<h4 class="panel-title white-color active">							
										Audit Logs
										<i class="change-font fa fa-caret-right font-left"></i>
										<i class="fa fa-caret-down font-right"></i>							
									</h4>
								</a>																	
								<div class="clear"></div>					
							</div>					
							<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
								<div class="panel-body ">

									<table class="table-roxas">
										<thead>
											<tr>
												<th>Date of Activiy</th>
												<th>User</th>
												<th>Shares Offered</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>September 10, 2015</td>
												<td>ROXAS, PEDRO OLGADO</td>
												<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
											</tr>
											<tr>
												<td>September 10, 2015</td>
												<td>VALENCIA, RENATO CRUZ</td>
												<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
											</tr>
										
										</tbody>
									</table>

								</div>			
							</div>
						</div>	
					</div>
				</div>
				
			</div>

			<div id="tab2" class="tab">
			
				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">Php 2.49</p>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>

				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>

				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>
				

				<div class="text-right-line margin-bottom-50 margin-top-25">				
					<div class="line"></div>								
				</div>
				
				<div class=" margin-top-20">
					<h2 class="f-left black-color">CACI</h2>					
					<div class="clear"></div>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">Php 2.49</p>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>

				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>

				
				<div class="panel-group text-left margin-top-50 ">
					
					<div class="accordion_custom ">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="ESOP-view-employee.php">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
				
					<div class="text-right-line  margin-bottom-55">				
						<div class="line"></div>								
					</div>
					
					<h2 class="black-color black-color">CAPDI.HO</h2>

					<div class="option-box trio margin-top-10">
						<p class="title">Total Alloted Shares</p>
						<p class="description">500,000 Shares</p>
					</div>
					
					<div class="option-box trio margin-top-10">
						<p class="title">Price Per Share</p>
						<p class="description">Php 2.49</p>
					</div>
					<div class="option-box trio margin-top-10">
						<p class="title">Vesting Years</p>
						<p class="description">2 Years</p>
					</div>
					<div class="option-box trio">
						<p class="title">No. of Employees Accepted</p>
						<p class="description">53 Employees</p>
					</div>
					<div class="option-box trio">
						<p class="title">Total Shares Availed</p>
						<p class="description">250,000 Shares</p>
					</div>
					
					<div class="option-box trio">
						<p class="title">Total Shares Unavailed</p>
						<p class="description">250,000 Shares</p>
					</div>
						
					<div class="accordion_custom margin-top-30">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
			
					<div class="text-right-line margin-bottom-60 margin-top-25">
						<div class="line"></div>
					</div>

					<div class="margin-bottom-20">
						<h2 class=" black-color f-left margin-top-0 margin-bottom-0">Gratuity List</h2>
						<button type="button" class="btn-normal f-right" > Add Gratuity</button>
						<div class="clear"></div>
					</div>

					<div class="tbl-rounded">
						<table class="table-roxas tbl-display margin-top-20">
							<thead>
								<tr>
									<th>Date Added</th>
									<th>Price per Share</th>
									<th>Total Value of Gratuity Given</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>October 10, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p>Pending Approval</p></td>
									<td>
										<a class="modal-trigger default-cursor" modal-target="approved-grat">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a class="red-color modal-trigger default-cursor" modal-target="reject-grant">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Approved</p></td>
									<td>
										<a href="#">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a href="#" class="red-color">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="red-color">Rejected</p></td>
									<td>
										<a class="modal-trigger" modal-target="reject-reason">View Rejected Reason</a>
									</td>
								</tr>
								<tr>
									<td>September 20, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Gratuity Sent</p></td>
									<td>
									
									</td>
								</tr>
								
								
							</tbody>
						</table>
					</div>

					<div class="text-right-line margin-top-25 margin-bottom-70" >
						<div class="line"></div>
					</div>

					<div data-container="audit_logs" class="hidden">
						<div class="accordion_custom margin-top-30">
							<div class="panel-heading border-10px">
								<a href="#">
									<h4 class="panel-title white-color active">							
										Audit Logs
										<i class="change-font fa fa-caret-right font-left"></i>
										<i class="fa fa-caret-down font-right"></i>							
									</h4>
								</a>																	
								<div class="clear"></div>					
							</div>					
							<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
								<div class="panel-body ">

									<table class="table-roxas">
										<thead>
											<tr>
												<th>Date of Activiy</th>
												<th>User</th>
												<th>Shares Offered</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>September 10, 2015</td>
												<td>ROXAS, PEDRO OLGADO</td>
												<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
											</tr>
											<tr>
												<td>September 10, 2015</td>
												<td>VALENCIA, RENATO CRUZ</td>
												<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
											</tr>
										
										</tbody>
									</table>

								</div>			
							</div>
						</div>	
					</div>
				</div>
		
			</div>

			<div id="tab3" class="tab">

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">Php 2.49</p>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>

				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>

				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>
				

				<div class="text-right-line margin-bottom-50 margin-top-25">				
					<div class="line"></div>								
				</div>
				
				<div class=" margin-top-20">
					<h2 class="f-left black-color">CACI</h2>					
					<div class="clear"></div>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Total Alloted Shares</p>
					<p class="description">500,000 Shares</p>
				</div>
				
				<div class="option-box trio margin-top-10">
					<p class="title">Price Per Share</p>
					<p class="description">Php 2.49</p>
				</div>

				<div class="option-box trio margin-top-10">
					<p class="title">Vesting Years</p>
					<p class="description">2 Years</p>
				</div>

				<div class="option-box trio">
					<p class="title">No. of Employees Accepted</p>
					<p class="description">53 Employees</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Availed</p>
					<p class="description">250,000 Shares</p>
				</div>
				
				<div class="option-box trio">
					<p class="title">Total Shares Unavailed</p>
					<p class="description">250,000 Shares</p>
				</div>

				
				<div class="panel-group text-left margin-top-50 ">
					
					<div class="accordion_custom ">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="ESOP-view-employee.php">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
				
					<div class="text-right-line  margin-bottom-55">				
						<div class="line"></div>								
					</div>
					
					<h2 class="black-color black-color">CAPDI.HO</h2>

					<div class="option-box trio margin-top-10">
						<p class="title">Total Alloted Shares</p>
						<p class="description">500,000 Shares</p>
					</div>
					
					<div class="option-box trio margin-top-10">
						<p class="title">Price Per Share</p>
						<p class="description">Php 2.49</p>
					</div>
					<div class="option-box trio margin-top-10">
						<p class="title">Vesting Years</p>
						<p class="description">2 Years</p>
					</div>
					<div class="option-box trio">
						<p class="title">No. of Employees Accepted</p>
						<p class="description">53 Employees</p>
					</div>
					<div class="option-box trio">
						<p class="title">Total Shares Availed</p>
						<p class="description">250,000 Shares</p>
					</div>
					
					<div class="option-box trio">
						<p class="title">Total Shares Unavailed</p>
						<p class="description">250,000 Shares</p>
					</div>
						
					<div class="accordion_custom margin-top-30">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Employee
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in">								
							<div class="panel-body">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Company Code</th>
											<th>Employee Code</th>
											<th>Department Name</th>
											<th>Employee Name</th>
											<th>Rank / Level</th>
											<th>Alloted Shares</th>
											<th>Total Shares Availed</th>
											<th>Total Amount Availed</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
										<tr>
											<td>0001</td>
											<td>0001</td>
											<td>Office of the President</td>
											<td>Juan Dela Cruz</td>
											<td>Executive</td>
											<td>10,000</td>
											<td>0.00</td>
											<td>Php 125,000.00</td>
											<td><a href="#">View</a></td>
										</tr>
									</tbody>
								</table>

							</div>			
						</div>
					</div>	
			
					<div class="text-right-line margin-bottom-60 margin-top-25">
						<div class="line"></div>
					</div>

					<div class="margin-bottom-20">
						<h2 class=" black-color f-left margin-top-0 margin-bottom-0">Gratuity List</h2>
						<button type="button" class="btn-normal f-right" > Add Gratuity</button>
						<div class="clear"></div>
					</div>

					<div class="tbl-rounded">
						<table class="table-roxas tbl-display margin-top-20">
							<thead>
								<tr>
									<th>Date Added</th>
									<th>Price per Share</th>
									<th>Total Value of Gratuity Given</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>October 10, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p>Pending Approval</p></td>
									<td>
										<a class="modal-trigger default-cursor" modal-target="approved-grat">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a class="red-color modal-trigger default-cursor" modal-target="reject-grant">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Approved</p></td>
									<td>
										<a href="#">Approve</a>
										<span class="margin-left-10 margin-right-10"> | </span>
										<a href="#" class="red-color">Reject</a>
									</td>
								</tr>
								<tr>
									<td>September 30, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="red-color">Rejected</p></td>
									<td>
										<a class="modal-trigger" modal-target="reject-reason">View Rejected Reason</a>
									</td>
								</tr>
								<tr>
									<td>September 20, 2015</td>
									<td>Php 0.60 / Share</td>
									<td>Php 1,300,215.15</td>
									<td><p class="green-color">Gratuity Sent</p></td>
									<td>
									
									</td>
								</tr>
								
								
							</tbody>
						</table>
					</div>

					<div class="text-right-line margin-top-25 margin-bottom-70" >
						<div class="line"></div>
					</div>

					<div data-container="audit_logs" class="hidden">
						<div class="accordion_custom margin-top-30">
						<div class="panel-heading border-10px">
							<a href="#">
								<h4 class="panel-title white-color active">							
									Audit Logs
									<i class="change-font fa fa-caret-right font-left"></i>
									<i class="fa fa-caret-down font-right"></i>							
								</h4>
							</a>																	
							<div class="clear"></div>					
						</div>					
						<div class="panel-collapse border-10px margin-top-20 margin-bottom-20 in	">								
							<div class="panel-body ">

								<table class="table-roxas">
									<thead>
										<tr>
											<th>Date of Activiy</th>
											<th>User</th>
											<th>Shares Offered</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>September 10, 2015</td>
											<td>ROXAS, PEDRO OLGADO</td>
											<td>Create ESOP Plan name <span class="font-bold">"ESOP 1"</span></td>
										</tr>
										<tr>
											<td>September 10, 2015</td>
											<td>VALENCIA, RENATO CRUZ</td>
											<td>Edited ESOP 1 Price per Share from <span class="font-bold">"1.00"</span> to <span class="font-bold">"6.00"</span></td>
										</tr>
									
									</tbody>
								</table>

							</div>			
						</div>
						</div>	
					</div>
				</div>

			</div> -->

		</div>

		<div>
	</section>
</div>

<div class="modal-container" modal-id="upload-payment">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD PAYMENT RECORD TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div data-container="valid-upload-payment" class="hidden">
			<div class="modal-content padding-40px">		
				<!-- <div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	 -->
				<div class="error upload_payment_record_error_message hidden margin-bottom-15">
				</div>
				<div class="success upload_payment_record_success_message hidden margin-bottom-15">
				</div>
				<div class="margin-top-5">
					<p class="display-inline-mid margin-right-30">Upload Payment Record Template:</p>
					<p class="display-inline-mid margin-right-30" id="upload_payment_file_name"><i>No file uploaded yet</i></p>
					<a href="javascript:void(0)" class="display-inline-mid" id="trigger_payment_upload_file">Upload File</a>
					<label><input type="file" name="upload_payment_record" class="hidden"></label>
				</div>
				<div class="margin-top-15">
					<div class="display-inline-mid margin-right-20">ESOP name:</div>
					<div class="select add-radius display-inline-mid upload_esop_dropdown">
						<select >
							<!-- <option value="dept1">CACI</option>
							<option value="dept2">CAPDI</option>
							<option value="dept3">CAPDI.HT</option> -->
						</select>
					</div>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
				<!-- <a href="<?php echo base_url(); ?>esop/template_preview"> -->
					<button type="button" class="display-inline-mid btn-dark btn-normal" id="upload_payment" disabled="true">Upload Template</button>
				<!-- </a> -->
			</div>
			<div class="clear"></div>
		</div>

		<div data-container="invalid-upload-payment" class="hidden">
			<div class="modal-content">					
				<div class="">
					<p class="font-15 font-bold">
						Uploading payment record is not allowed when there are no expired ESOP. Please wait until the ESOP have been completely expired.
					</p>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-dark close-me margin-right-10">OK</button>	
			</div>
			<div class="clear"></div>
		</div>

	</div>
</div>

<!-- <div class="modal-container" modal-id="upload-payment">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">UPLOAD PAYMENT RECORD TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>
		<div class="modal-content padding-40px">		
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>			
			<div class=" margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid ">Upload File</a>
			</div>
		</div>
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>	
			<a href="ESOP-view-esop-4-prev-template.php">		
				<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
			</a>
		</div>
		<div class="clear"></div>
	</div>
</div> -->

<!-- reject gratuity  -->
<!-- <div class="modal-container" modal-id="reject-grant">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">REJECT GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		content
		<div class="modal-content">		
			<div class="success success_message margin-bottom-10 hidden"></div>
			<div class="error error_message margin-bottom-10 hidden"></div>
			<p>Please indicate reason for rejection:</p>
			<textarea class="margin-top-10 border-10px" name="reject_reason"></textarea>
			
		</div>
		button
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-reject-grant">Reject Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div> -->


<!-- view reject reason  -->
<!-- <div class="modal-container" modal-id="view-reject-reason">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">VIEW REJECTION REASON</h4>
			<div class="modal-close close-me"></div>
		</div>
		
		<div class="modal-content">
			<table>
				<tbody>
					<tr>
						<td class="width-200px">ESOP Name:</td>
						<td data-label="esop_name">ESOP 1</td>
					</tr>
					<tr>
						<td>Dividend Price per Share</td>
						<td data-label="price_per_share">Php 0.60 / Share</td>
					</tr>
					<tr>
						<td>Date Applied:</td>
						<td data-label="grant_date">September 30, 2015</td>
					</tr>
					<tr>
						<td>Reason:</td>
					</tr>
				</tbody>
			</table>
			<p data-label="reject_reason">Zombie ipsum reversus ab viral inferno, nam rick grimes malum 
			cerebro. De carne lumbering animata corpora quaeritis. Summus brains
			 sit​​, morbo vel maleficia? De apocalypsi gorger omero undead 
			 survivor dictum mauris. 
			 </p>
		</div>				
	</div>
</div> -->

<!-- create esop batch  -->
<div class="modal-container" modal-id="esop-batch">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">CREATE ESOP BATCH</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div data-container="valid-create-batch" class="hidden">
			<div class="modal-content">					
				<div class="error create_esop_batch_error_message hidden margin-bottom-10">
				</div>
				<div class="success create_esop_batch_success_message hidden margin-bottom-10">
				</div>
				<table>
					<tbody>
						<tr>
							<td class="width-200px"><p>ESOP Name:</p></td>
							<td><input type="text" class="small add-border-radius-5px" name="esop_batch_name"/></td>
						</tr>
						<tr>
							<td class="width-200px"><p>Total Share Quantity:</p></td>
							<td>
								<div class="tool-tip esop_batch_total_share_qty_tool_tip display-inline-mid" tt-html="<p>Total Unavailed Shares:</p><p><small>26,208 Shares</small></p>">
									<input type="text" class="small add-border-radius-5px" name="esop_batch_total_share_qty"/> 
								</div> Shares
							</td>
						</tr>
						<tr>
							<td><p>Divident Price per Share:</p></td>
							<td class="font-bold"><span data-label="price_per_share"></span> / Share</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>	
				<!-- <a href="ESOP-view-esop-new-batch.php">		 -->
					<button type="button" class="display-inline-mid btn-dark btn-normal" disabled="true" id="create_esop_batch_btn">Create Batch</button>
				<!-- </a> -->
			</div>
			<div class="clear"></div>
		</div>
		<div data-container="invalid-create-batch" class="hidden">
			<div class="modal-content">					
				<div class="">
					<p class="font-15 font-bold">
						<!-- Creating an ESOP Batch while there's an ongoing batch is not allowed. Please wait until the current batch expires. -->
						Creating a new ESOP Batch is not allowed when there is a pending process on one of the ESOP batch. Please wait until the batch have been completely posted.
					</p>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-dark close-me margin-right-10">OK</button>	
			</div>
			<div class="clear"></div>
		</div>
		<div data-container="invalid-create-batch-max" class="hidden">
			<div class="modal-content">					
				<div class="">
					<p class="font-15 font-bold">
						We're sorry but there are no more available shares in this ESOP to create another batch. Please try again with another ESOP.
					</p>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-dark close-me margin-right-10">OK</button>	
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>


<!-- approve gratuity  -->
<!-- <div class="modal-container" modal-id="approve-gratuity">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">APPROVE GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		content
		<div class="modal-content">		
			<div class="success success_message margin-bottom-10 hidden"></div>
			<div class="error error_message margin-bottom-10 hidden"></div>
			<p>Are you sure you want to approve this gratuity application?</p>
			<table>
				<tbody>
					<tr>
						<td class="width-200px"><p>ESOP Name:</p></td>
						<td><p class="font-bold" data-label="esop_name">ESOP 1</p></td>
					</tr>
					<tr>
						<td><p>Dividend Price per Share:</p></td>
						<td class="font-bold" data-label="price_per_share">Php 0.60 / Share</td>
					</tr>
				</tbody>
			</table>
		</div>
		button
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-approve-gratuity">Appoved Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div> -->

<!-- add employee -->
<div class="modal-container" modal-id="add-new-employee">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">ADD NEW EMPLOYEE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div data-container="valid-add-employee" class="hidden">
			<div class="modal-content">		
				<form id="add_new_employee_form">
					<!-- <div class="error">Price per Share is invalid. Please type number only in the textbox.</div>			 -->
					<div class="error add_new_employee_error_message hidden margin-bottom-10">
					</div>
					<div class="success add_new_employee_success_message hidden margin-bottom-10">
					</div>
					<table>
						<tbody>
							<tr>
								<td>
									<p class="margin-right-10">Company:</p>
								</td>
								<td>
									<div class="select add-radius width-250px company_dropdown">
										<select >
											<!-- <option value="dept1">CACI</option>
											<option value="dept2">CAPDI</option>
											<option value="dept3">CAPDI.HT</option> -->
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<p class="margin-right-10">Employee Name:</p>
								</td>
								<td>
									<div class="select add-radius width-250px employee_dropdown">
										<select>
											<!-- <option value="emp1">Joselito Salazar</option>
											<option value="emp1">Aaron Paul Labing-Isa</option> -->
										</select>							
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<p class="margin-right-10">ESOP Name:</p>
								</td>
								<td>
									<div class="select add-radius width-250px esop_dropdown">
										<select>
											<!-- <option value="eop1">ESOP 1</option>
											<option value="eop2">ESOP 1.2</option> -->
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<p class="margin-right-10">Total Share Unavailed:</p>
								</td>
								<td>
									<input type="text" class="small add-border-radius-5px width-250px display-inline-mid" name="total_share_unavailed" disabled="disabled"/>
									<p class="display-inline-mid margin-left-5">Shares</p>
								</td>
							</tr>
							<tr>
								<td>
									<p class="margin-right-10">Share you want to Avail</p>
								</td>
								<td>
									<input type="text" class="small add-border-radius-5px width-250px display-inline-mid" name="total_share_qty" datavalid="required" labelinput="Share you want to avail"/>
									<p class="display-inline-mid margin-left-5">Shares</p>
								</td>
							</tr>
							<tr>
								<td><p class="margin-right-10">Offer Expiration:</p></td>
								<td>
									<div class="date-picker add-radius display-inline-mid">
										<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" name="offer_expiration" datavalid="required" labelinput="Offer Expiration"/>
										<span class="fa fa-calendar text-center"></span>
									</div>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
									<label class="copy_initial">
										<input type="checkbox" name="copy_initial">
										<span class="black-color txt-normal font-15">Copy Initial Grant Date, Price per Share and Vesting Years</span>
									</label>
								</td>
							</tr>
							<tr class="copy_initial">
								<td><p class="margin-right-10">Grant Date:</p></td>
								<td>
									<div class="date-picker add-radius display-inline-mid">
										<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" name="grant_date">
										<span class="fa fa-calendar text-center"></span>
									</div>
								</td>
							</tr>
							<tr class="copy_initial">
								<td>
									<p class="margin-right-10">Price per Share</p>
								</td>
								<td>
									<input type="text" class="small add-border-radius-5px width-150px" name="price_per_share">
									<div class="select add-radius width-100px currency_dropdown">
										<select>
											<!-- <option value="price1">PHP</option>
											<option value="price2">USD</option>
											<option value="price3">CAD</option>
											<option value="price4">HKD</option>
											<option value="price5">INR</option> -->
										</select>
									</div>
								</td>
							</tr>
							<tr class="copy_initial">
								<td>
									<p class="margin-right-10">Vesting Years</p>
								</td>
								<td>
									<input type="text" class="small add-border-radius-5px display-inline-mid width-250px" name="vesting_years">
									<p class="display-inline-mid">Years</p>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
				<button type="button" class="display-inline-mid btn-normal" id="add_new_employee_btn">Add Employee</button>
			</div>
			<div class="clear"></div>
		</div>
		<div data-container="invalid-add-employee" class="hidden">
			<div class="modal-content">					
				<div class="">
					<p class="font-15 font-bold">
						<!-- Adding an employee while there's an ongoing batch is not allowed. Please wait until the current batch expires. -->
						Adding an employee is not allowed when there is a pending process on one of the ESOP batch. Please wait until the batch have been completely posted.
					</p>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-dark close-me margin-right-10">OK</button>	
			</div>
			<div class="clear"></div>
		</div>
		<div data-container="invalid-add-employee-max" class="hidden">
			<div class="modal-content">					
				<div class="">
					<p class="font-15 font-bold">
						We're sorry but there are no more available shares in this ESOP to offer for a new employee. Please try again with another ESOP.
					</p>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-dark close-me margin-right-10">OK</button>	
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>


<!-- add gratuity  -->
<div class="modal-container" modal-id="add-gratuity">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<div class="modal-content">
			<div class="success success_message margin-bottom-10 hidden"></div>
            <div class="error error_message margin-bottom-10 hidden"></div> 
			<table>
				<tbody>
					<tr>
						<td class="width-200px">ESOP Name</td>
						<td data-label="esop_name">ESOP 1</td>
					</tr>
					<tr>
						<td>Dividend Price per Share</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-100px display-inline-mid" name="price_per_share"/>
							<div class="select add-radius display-inline-mid width-100px currency_dropdown">
								<select>
									<option value="php">PHP</option>
									<option value="usd">USD</option>
									<option value="cad">CAD</option>
									<option value="hkd">HKD</option>
									<option value="inr">INR</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td><p class="margin-right-10">Dividend Date</p></td>
						<td>
							<div class="date-picker add-radius display-inline-mid">
								<input type="text" data-date-format="MM/DD/YYYY" class="width-200px dividend-date" name="dividend_date" datavalid="required" labelinput="Dividend Date"/>
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-add-gratuity">Add Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- share distribution template -->
<div class="modal-container" modal-id="batch-share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content padding-40px">		
			<!-- <div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	 -->
			<div class="error upload_batch_share_distribution_error_message hidden margin-bottom-15">
			</div>
			<div class="success upload_batch_share_distribution_success_message hidden margin-bottom-15">
			</div>
			<div class="margin-top-5">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30" id="file_name"><i>No file uploaded yet</i></p>
				<a href="javascript:void(0)" class="display-inline-mid" id="trigger_batch_upload_file">Upload File</a>
				<label><input type="file" name="upload_batch_share_distribution" class="hidden"></label>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<!-- <a href="<?php echo base_url(); ?>esop/template_preview"> -->
				<button type="button" class="display-inline-mid btn-dark btn-normal" id="upload_batch_shares" disabled="true">Upload Template</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="modal-container" modal-id="edit-esop-batch">
	<div class="modal-body small width-600px">
		<div class="modal-head">
			<h4 class="text-left">EDIT ESOP</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">	
			<!-- <div class="error">Total Share Quantity is Invalid. <br>Please type numbers only in the textbox.</div>	 -->
			<div class="error edit_esop_error_message hidden margin-bottom-10">
			</div>
			<div class="success edit_esop_success_message hidden margin-bottom-10">
			</div>

			<form id="edit_esop_batch_form">
				<table class="width-100percent">
					<tbody>
						<tr>
							<td class="width-200px">ESOP Name:</td>
							<td><input type="text" class="small width-250px add-border-radius-5px" name="esop_name" datavalid="required" labelinput="Esop Name"/></td>
						</tr>
					</tbody>
				</table>		
			</form>

			<!-- <div class="margin-top-30">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div> -->
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-normal" id="edit_esop_batch_btn">Save Changes</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- offer expiration  -->
<div class="modal-container" modal-id="offer-expiration-batch">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SET OFFER EXPIRATION</h4>
			<div class="modal-close close-me"></div>
		</div>
		<form id="set_expiration_batch_form">
			<div class="modal-content padding-40px">
				<!-- <div class="error">Date below has already passed. Please choose another date.</div> -->
				<div class="error set_offer_expiration_error_message hidden margin-bottom-10">
				</div>
				<div class="success set_offer_expiration_success_message hidden margin-bottom-10">
				</div>
				<div class="margin-top-20">
					<p class="display-inline-mid margin-left-15">Expiry Date:</p>
					<div class="date-picker display-inline-mid add-radius margin-left-20 set-offer-dp">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-250px date-picker" name="offer_expiration">
						<span class="fa fa-calendar text-center"></span>
					</div>
				</div>
			</div>
		</form>

		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			<!-- <a href="ESOP-view-esop-3.php"> -->
				<button type="button" class="display-inline-mid btn-dark" id="set_expiration_batch_button">Set Offer Expiration</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- ================================================== -->
<!-- ==========EXTEND EXPIRATION MODAL================= -->
<!-- ================================================== -->
<div class="modal-container modal-extend-expiration">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">EXTEND OFFER EXPIRATION</h4>
			<div class="modal-close close-me"></div>
		</div>
		<form id="set_expiration_batch_form">
			<div class="modal-content padding-40px">
				<!-- <div class="error">Date below has already passed. Please choose another date.</div> -->
				<div class="error set_offer_expiration_error_message hidden margin-bottom-10">
				</div>
				<div class="success set_offer_expiration_success_message hidden margin-bottom-10">
				</div>
				<div class="margin-top-20">
					<p class="extend-success"></p>
					<p class="display-inline-mid margin-left-15">Old Expiry Date: <span class="old-expiry-date"></span></p>
					<!-- <p class="old-expiry-date margin-left-15"></p> -->
					<div class="date-picker display-inline-mid add-radius margin-left-20 set-offer-dp">
						<input type="text" data-date-format="MM/DD/YYYY" class="width-250px date-picker" name="offer_expiration">
						<span class="fa fa-calendar text-center"></span>
					</div>
				</div>
			</div>
		</form>

		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			<!-- <a href="ESOP-view-esop-3.php"> -->
				<button type="button" class="display-inline-mid btn-dark" id="submit_extend_offer">Extend Offer Expiration</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- ================================================== -->
<!-- =========END EXTEND EXPIRATION MODAL============== -->
<!-- ================================================== -->


<!-- send offer letter -->
<div class="modal-container" modal-id="send-letter-batch">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SEND OFFER LETTER</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">	
			<div class="error send_letter_error_message hidden margin-bottom-10">
			</div>
			<div class="success send_letter_success_message hidden margin-bottom-10">
			</div>		
			<div class="">
				<p class="font-15 font-bold">Are you sure you want to send the offer letters of this ESOP to the employees?</p>
			</div>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>	
			<!-- <a href="ESOP-view-esop-4.php"> -->
				<button type="button" class="display-inline-mid btn-dark" id="esop_send_letter_batch_btn">Send Offer Letter</button>
			<!-- </a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- send gratuity to employees  -->
<div class="modal-container" modal-id="send-gratuity-to-employees">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">SEND GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<div class="modal-content">
			<div class="success success_message margin-bottom-10 hidden"></div>
            <div class="error error_message margin-bottom-10 hidden"></div> 
			<p>Are you really sure you want to send gratuity to employees?</p>
		</div>

		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-send-gratuity-to-employees">Send Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>
