<?php $user_role = $this->session->userdata('user_role'); ?>
<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Gratuity Lists</h1>
			<button class="btn-normal f-right margin-top-20 modal-trigger" modal-target="add-gratuity">Add Gratuity</button>
			<div class="clear"></div>
		</div>

		<div class="header-effect" id="gratuity_table">

			<div class="display-inline-mid default">
				<div>
					<div class="select add-radius display-inline-mid">
						<p class="white-color margin-bottom-5">ESOP Name:</p>
						<div class="select margin-right-10 add-radius upload_esop_dropdown" style="width:175px;">
							<select>
								<!-- <option value="op1">ESOP 1</option>
								<option value="op2">ESOP 2</option>
								<option value="op3">ESOP 3</option> -->
							</select>
						</div>
					</div>
					<div class="select add-radius display-inline-mid">
						<p class="white-color margin-bottom-5">Status:</p>
						<div class="select margin-right-10 add-radius gratuity_status_dropdown" style="width:175px;">
							<select>
								<option value="Approved">Approved</option>
								<option value="Pending Approval">Pending</option>
								<option value="Rejected">Rejected</option>
								<option value="Gratuity Sent">Gratuity Sent</option>
							</select>
						</div>
					</div>
					<div class="display-inline-bot search-me">
						<button class="btn-normal display-inline-mid margin-left-10" id ="btn_gratuity_table">Filter List</button>
					</div>

					<div class="display-inline-mid vesting-years">
						<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px"/>
						<button class="btn-normal display-inline-mid margin-left-10">Search</button>
					</div>
				</div>
			</div>


			<div class="display-inline-mid price-share">
				<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
				<br />
				<div class="price xsmall display-inline-mid margin-left-20">
					<input type="text">
				</div>
				<button class="btn-normal display-inline-mid margin-left-10">Search</button>				
			</div>
			
		</div>
		
		
	</div>
</section>

<section section-style="content-panel">
	<div class="content">
		<div class="tbl-rounded">
			<table class="table-roxas tbl-display margin-top-20">
				<thead>
					<tr>
						<th>Esop Name</th>
						<th>Date Added</th>
						<th>Dividend Date</th>
						<th>Price per Share</th>
						<th>Total Value of Gratuity Given</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody data-container="gratuity_list_container">
					<tr class="tmp_gratuity_list template hidden">
						<td data-label="esop_name"></td>
						<td data-label="grant_date"></td>
						<td data-label="dividend_date"></td>
						<td data-label="price_per_share"></td>
						<td data-label="total_value_of_gratuity_given"></td>
						<td data-label="status"></p></td>
						<td>
						<?php if($user_role == 4 || $user_role == 2 ){?>
							<span data-container="admin_gratuity_action_btns">
								<a class="modal-trigger default-cursor" modal-target="approve-gratuity">Approve</a>
								<span class="margin-left-10 margin-right-10"> | </span>
								<a class="red-color modal-trigger default-cursor" modal-target="reject-grant">Reject</a>
							</span>
							<span data-container="view_reject_reason" class="">
								<a class="modal-trigger default-cursor" modal-target="view-reject-reason">View Reject Reason</a>	
							</span>
							<?php }else if($user_role == 2 || $user_role == 3){?>
							<span data-container="send_gratuity_to_employees" class="">
								<a class="modal-trigger default-cursor" modal-target="send-gratuity-to-employees">Send Gratuity to Employees</a>	
							</span>
							<span data-container="view_reject_reason" class="">
								<a class="modal-trigger default-cursor" modal-target="view-reject-reason">View Reject Reason</a>	
							</span>
							<?php } ?>
						</td>
					</tr>
					<tr class="no_results_found">
						<td colspan="8">No Results found</td>
					</tr>

					
				</tbody>
			</table>
		</div>
	</div>
</section>

<!-- add Gratuity -->

<div class="modal-container" modal-id="add-gratuity">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">ADD GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<div class="modal-content">
			<div class="success success_message margin-bottom-10 hidden"></div>
            <div class="error error_message margin-bottom-10 hidden"></div> 
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
							
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20 margin-left-20 esop_dropdown">
								<select >
									<option value="dept1">ESOP 1</option>
									<option value="dept2">ESOP 2</option>
									<option value="dept3">E-ESOP 1</option>
									<option value="dept4">ESOP 3</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Dividend Price per Share</td>
						<td>
							<input type="text" class="small add-border-radius-5px width-100px display-inline-mid margin-left-20" name="price_per_share"/>
							<div class="select add-radius display-inline-mid width-100px currency_dropdown">
								<select>
									<!-- <option value="php">PHP</option>
									<option value="usd">USD</option>
									<option value="cad">CAD</option>
									<option value="hkd">HKD</option>
									<option value="inr">INR</option> -->
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td><p class="margin-right-10">Dividend Date</p></td>
						<td>
							<div class="date-picker add-radius display-inline-mid margin-left-20">
								<input type="text" data-date-format="MM/DD/YYYY" class="width-200px dividend-date" name="dividend_date" datavalid="required" labelinput="Dividend Date"/>
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-add-gratuity">Add Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>

<!-- Reject Graduity
<div class="modal-container" modal-id="reject-gratuity">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Reject Gratuity</h4>
			<div class="modal-close close-me"></div>
		</div>

		content
		<div class="modal-content">		
			
			<p><strong>Please indicate reason for rejection:</strong></p>
			<textarea rows="4" cols="30">
			</textarea>
		</div>
		button
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Reject Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>
 -->
<!-- Approve Gratuity -->
<!-- <div class="modal-container" modal-id="approve-gratuity">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Approve Gratuity</h4>
			<div class="modal-close close-me"></div>
		</div>

		content
		<div class="modal-content">		
			
			<p><strong>Are you sure you want to approve this gratuity application?</strong></p>
			<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>ESOP 1</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Dividend Price per Share:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>Php 0.60 / Share</strong></p>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		</div>
		button
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark">Approve Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div> -->

<!-- approve gratuity  -->
<div class="modal-container" modal-id="approve-gratuity">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">APPROVE GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="success success_message margin-bottom-10 hidden"></div>
			<div class="error error_message margin-bottom-10 hidden"></div>
			<p>Are you sure you want to approve this gratuity application?</p>
			<table>
				<tbody>
					<tr>
						<td class="width-200px"><p>ESOP Name:</p></td>
						<td><p class="font-bold" data-label="esop_name">ESOP 1</p></td>
					</tr>
					<tr>
						<td><p>Dividend Price per Share:</p></td>
						<td class="font-bold" data-label="price_per_share">Php 0.60 / Share</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-approve-gratuity">Appoved Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- reject gratuity  -->
<div class="modal-container" modal-id="reject-grant">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">REJECT GRATUITY</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">		
			<div class="success success_message margin-bottom-10 hidden"></div>
			<div class="error error_message margin-bottom-10 hidden"></div>
			<p>Please indicate reason for rejection:</p>
			<textarea class="margin-top-10 border-10px" name="reject_reason"></textarea>
			
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
			<button type="button" class="display-inline-mid btn-dark btn-reject-grant">Reject Gratuity</button>
		</div>
		<div class="clear"></div>
	</div>
</div>


<!-- view reject reason  -->
<div class="modal-container" modal-id="view-reject-reason">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">VIEW REJECTION REASON</h4>
			<div class="modal-close close-me"></div>
		</div>
		
		<div class="modal-content">
			<table>
				<tbody>
					<tr>
						<td class="width-200px">ESOP Name:</td>
						<td data-label="esop_name">ESOP 1</td>
					</tr>
					<tr>
						<td>Dividend Price per Share</td>
						<td data-label="price_per_share">Php 0.60 / Share</td>
					</tr>
					<tr>
						<td>Date Applied:</td>
						<td data-label="grant_date">September 30, 2015</td>
					</tr>
					<tr>
						<td>Reason:</td>
					</tr>
				</tbody>
			</table>
			<p data-label="reject_reason">Zombie ipsum reversus ab viral inferno, nam rick grimes malum 
			cerebro. De carne lumbering animata corpora quaeritis. Summus brains
			 sit​​, morbo vel maleficia? De apocalypsi gorger omero undead 
			 survivor dictum mauris. 
			 </p>
		</div>				
	</div>
</div>
<!-- View Rejection Reason
<div class="modal-container" modal-id="view-rejection-reason">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">View Rejection Reason</h4>
			<div class="modal-close close-me"></div>
		</div>
		content
		<div class="modal-content">		
			<div class="modal-content">		
			
			<table>
				<tbody>
					<tr>
						<td>
							<p>ESOP Name:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>ESOP 1</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Dividend Price per Share:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>Php 0.60 / Share</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Date Applied:</p>
						</td>
						<td>
							<div class="select add-radius width-250px margin-left-20">
								<p><strong>September 30, 2015</strong></p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<p>Reason:</p>
						</td>
					</tr>
				</tbody>
			</table>
			<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</strong></p>
			
		</div>
		</div>
		button
		<div class="f-right margin-right-20 margin-bottom-10">
		</div>
		<div class="clear"></div>
	</div>
</div> -->