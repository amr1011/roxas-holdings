<section section-style="top-panel">
	<div class="content">
		<div>
			<h1 class="f-left">Payment Records</h1>
			
		<div class="f-right">
			<!-- <button class="btn-normal margin-right-10">Download Payment Template</button> -->
			<a href="<?php echo base_url(); ?>esop/download_payment_record_template">
				<button class="btn-normal margin-top-20 margin-right-10">Download Payment Template</button>
			</a>
			<!-- <button class="btn-normal margin-right-10 modal-trigger" modal-target="share-distribution-template">Upload Payment Record</button> -->
			<button class="btn-normal margin-right-10 modal-trigger" modal-target="upload-payment">Upload Payment Records</button>
			<button class="btn-normal modal-trigger" modal-target="add-payment-record">Add Payment Record</button>				
		</div>
			<div class="clear"></div>
		</div>

		<div class="header-effect" id="payment_table">
			<div class="display-inline-mid default">
				<div class="inline-block">
					<div class="select add-radius display-inline-mid margin-right-10">
						<p class="white-color margin-bottom-5">ESOP Name</p>
						<div class="select margin-right-10 add-radius upload_esop_dropdown" style="width:175px;">
							<select>
								<!-- <option value="op1">ESOP 1</option>
								<option value="op2">ESOP 2</option>
								<option value="op3">ESOP 3</option> -->
							</select>
						</div>
					</div>

					<div class="select add-radius display-inline-mid margin-right-10">
						<p class="white-color margin-bottom-5">Payment Method</p>
						<div class="select margin-right-10 add-radius payment_method_dropdown" style="width:175px;">
							<select>
								<!-- <option value="op1">Cash</option>
								<option value="op2">Credit Card</option>
								<option value="op3">Check</option>
								<option value="op3">Profit Share</option> -->
							</select>
						</div>
					</div>
					<div class="select add-radius display-inline-mid margin-right-10">
						<button class="btn-normal display-inline-mid margin-left-10 margin-top-25" id="btn_filter_payment_table">Search</button>
					</div>
				</div>	
			</div>
		</div>

	</div>
</section>
<section section-style="content-panel">

  <div id="main-record-container" class="content padding-top-30">
	    <div class="clear"></div>
	    <div class="tbl-rounded margin-top-20 table-content display_on">
	      <table class="table-roxas tbl-display" >
	        <thead>
	          <tr>
	            <th>Dividend Added</th>
	            <th>Employee Name</th>
	            <th>ESOP Name</th>
	            <th>Payment Method</th>
	            <th>Amount</th>
	          </tr>
	        </thead>
	        <tbody data-container="payment_record_container" class=".payment_records">
	          <tr class="tmp_payment_list template">
						<td data-label="dividend_added"></td>
						<td data-label="employee_name"></td>
						<td data-label="esop_name"></td>
						<td data-label="payment_method"></td>
						<td data-label="amount"></td>
					</tr>
					<tr class="no_results_found">
						<td colspan="8">No Results Found</td>
					</tr>
	        </tbody>
	      </table>
	    </div>
	</div>
</section>

<!-- share distribution template -->
<!-- <div class="modal-container" modal-id="share-distribution-template">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">SHARE DISTRIBUTION TEMPLATE</h4>
			<div class="modal-close close-me"></div>
		</div>

		content
		<div class="modal-content padding-40px">		
			<div class="error">File Upload is Invalid. Please upload the correct template file.</div>			
			<div class=" margin-top-20">
				<p class="display-inline-mid margin-right-30">Share Distribution Template:</p>
				<p class="display-inline-mid margin-right-30"><i>No file uploaded yet</i></p>
				<a href="#" class="display-inline-mid">Upload File</a>
			</div>
		</div>
		button
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>
			<a href="payment-preview-of-template.php">			
				<button type="button" class="display-inline-mid btn-dark">Upload Template</button>
			</a>
		</div>
		<div class="clear"></div>
	</div>
</div> -->

<!-- upload payment Records -->
<div class="modal-container" modal-id="upload-payment">
	<div class="modal-body width-600px">
		<div class="modal-head">
			<h4 class="text-left">Upload Payment Record Template</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div data-container="valid-upload-payment">
			<div class="modal-content padding-40px">		
				<!-- <div class="error">File Uploaded is Invalid. <br />Please upload the correct template file.</div>	 -->
				<div class="error upload_payment_record_error_message hidden margin-bottom-15">
				</div>
				<div class="success upload_payment_record_success_message hidden margin-bottom-15">
				</div>
				<div class="margin-top-5">
					<p class="display-inline-mid margin-right-30">Upload Payment Record Template:</p>
					<p class="display-inline-mid margin-right-30" id="upload_payment_file_name"><i>No file uploaded yet</i></p>
					<a href="javascript:void(0)" class="display-inline-mid" id="trigger_payment_upload_file">Upload File</a>
					<label><input type="file" name="upload_payment_record" class="hidden"></label>
				</div>
				<div class="margin-top-15">
					<div class="display-inline-mid margin-right-20">ESOP name:</div>
					<div class="select add-radius display-inline-mid upload_esop_dropdown">
						<select class="this-dropdown">
							<!-- <option value="dept1">CACI</option>
							<option value="dept2">CAPDI</option>
							<option value="dept3">CAPDI.HT</option> -->
						</select>
					</div>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
				<!-- <a href="<?php echo base_url(); ?>esop/template_preview"> -->
					<button type="button" class="display-inline-mid btn-dark btn-normal" id="upload_payment" disabled="true">Upload Template</button>
				<!-- </a> -->
			</div>
			<div class="clear"></div>
		</div>

		<div data-container="invalid-upload-payment" class="hidden">
			<div class="modal-content">					
				<div class="">
					<p class="font-15 font-bold">
						Uploading payment record is not allowed when there are no expired ESOP. Please wait until the ESOP have been completely expired.
					</p>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-dark close-me margin-right-10">OK</button>	
			</div>
			<div class="clear"></div>
		</div>

	</div>
</div>


<!-- add payment record -->
<div class="modal-container" modal-id="add-payment-record">
	<div class="modal-body small">
		<div class="modal-head">
			<h4 class="text-left">Add Payment Record</h4>
			<div class="modal-close close-me"></div>
		</div>

		<!-- content -->
		<div class="modal-content">
		 	<div class="success success_message margin-bottom-10 hidden"></div>
            <div class="error error_message margin-bottom-10 hidden"></div>  
			<table class="width-100ppercent center-margin">
				<tbody>
					<tr>
						<td class="padding-top-10">Company :</td>
						<td>
							<div class="select margin-left-20 width-300px add-radius add_company_payment_record_dropdown">
								<select>
									<!-- <option value="op1">RHI</option>
									<option value="op2">CACI</option>
									<option value="op3">CAPDI</option>
									<option value="op3">Roxol</option> -->
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Employee Name :</td>
						<td>
							<div class="select margin-left-20 width-300px add-radius add_employee_name_payment_record_dropdown">
								<select>
									<option value="op1">ESOP 1</option>
									<option value="op2">ESOP 2</option>
									<option value="op3">ESOP 3</option>
									<option value="op3">E-ESOP 1</option>
									<option value="op3">E-ESOP 1</option>
								</select>
							</div>
							<!-- <div class="modal-content text-center">	
								<div class="display-inline-mid member-selection">
									<div class="">
										<div class="member-container display-inline-mid">
											<p class="">1 Member Selected</p>
											<div class="angle-down">
												<i class="fa fa-angle-down fa-2x" aria-hidden="true"></i>
											</div>
										</div>
									</div>
									<div class="popup_person_list">
										<div class="display-inline-mid width-100percent text-left font-0">
											<input type="text" class="width-90percent display-inline-mid">
											<div class="sSearch display-inline-mid width-10percent">
												<i class="fa fa-search font-18" aria-hidden="true"></i>
											</div>
										</div>
										<div class="popup_person_list_div">
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Dwayne Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Girl Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Hello Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Hi Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Kapoy Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Katulugon Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Diputa Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
											<div class="thumb_list_view small text-size-0">
												<div class="thumb_list_inner display-inline-mid width-90percent">
													<img class="img-responsive" src="assets/images/profile/aaron.png">
													<div class="profile display-inline-mid">
														<p class="profile_name font-15 padding-left-10">Hayup Garcia</p>														
													</div>
												</div>
												<div class="check width-10percent">
													<i class="fa fa-check font-18" aria-hidden="true"></i>
												</div>
							
											</div>
										</div>										
									</div>
								</div>
							</div> -->
						</div>
						</td>
					</tr>
					<tr>
						<td class="v-top padding-top-15">ESOP Name:</td>
						<td>
							<div class="select margin-left-20 width-300px add-radius upload_esop_dropdown">
								<select>
									<!-- <option value="op1">ESOP 1</option>
									<option value="op2">ESOP 2</option>
									<option value="op3">ESOP 3</option> -->
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="v-top padding-top-15">Payment Method:</td>
						<td>
							<div class="select margin-left-20 width-300px add-radius " id="payment_method_dropdown">
								<select>
									<!-- <option value="op1">Cash</option>
									<option value="op2">Credit Card</option>
									<option value="op3">Check</option>
									<option value="op3">Profit Share</option> -->
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="v-top padding-top-15">Dividend Date: </td>
						<td>
							<div class="date-picker add-radius display-inline-mid margin-left-20">
								<input type="text" style="width:265px;" data-date-format="MM/DD/YYYY" class="dividend-date" name="dividend_date" datavalid="required" labelinput="Dividend Date"/>
								<span class="fa fa-calendar text-center"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="padding-top-10">Payment Amount:</td>
						<td><input type="text" class="normal margin-left-20 width-150px add-border-radius-5px display-inline-mid" name="amount_paid"/>
						<div class="select add-radius width-150px display-inline-mid " id="currency_dropdown">
							<select>
								<!-- <option value="op1">Cash</option>
								<option value="op2">Credit Card</option>
								<option value="op3">Check</option>
								<option value="op3">Profit Share</option> -->
							</select>
						</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- button -->
		<div class="f-right margin-right-20 margin-bottom-10">
			<button type="button" class="display-inline-mid btn-cancel close-me margin-right-5">Cancel</button>			
			<!-- <a href="ESOP-view-employee.php"><button type="button" class="display-inline-mid btn-normal close-me">Add Payment</button></a> -->
			<button type="button" class="display-inline-mid btn-normal btn-add-payment-record">Add Payment</button>
		</div>
		<div class="clear"></div>
	</div>
 </div>