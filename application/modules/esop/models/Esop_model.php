<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Esop_model extends CI_Model {

    public function get_esop_data ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT e.*, c.name as currency_name, 
                CONCAT(u.first_name, \' \', u.middle_name, \' \', u.last_name) as created_by_name, 
                (SELECT COUNT(uso.id) FROM "user_stock_offer" uso WHERE uso.esop_id = e.id ) as employee_count 
                FROM "esop" e 
                LEFT JOIN "currency" c ON e.currency = c.id 
                LEFT JOIN "user" u ON e.created_by = u.id 
                LEFT JOIN "user_stock_offer" uso ON e.id = uso.esop_id 
                LEFT JOIN "user_esop" ue ON e.id = ue.esop_id 
                LEFT JOIN "department_distribution" dd ON e.id = dd.esop_id 
                LEFT JOIN "gratuity" g ON e.id = g.esop_id 
                WHERE TRUE ';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'e.name')
            {
                $sql .= " AND ".$qualifiers['search_field']." ILIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'e.price_per_share')
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
            else if ($qualifiers['search_field'] == 'e.grant_date')
            {
                $sql .= " AND DATE(".$qualifiers['search_field'].") BETWEEN ".$qualifiers['keyword']." ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "e.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY e.id,c.name,created_by_name";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." OFFSET ".$qualifiers['offset']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_user_stock_offer_data ($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT  uso.id as id, 
                uso.stock_amount as employee_alloted_shares, 
                uso.stock_amount as stock_amount, 
                uso.esop_id as esop_id, 
                uso.offer_letter_id as offer_letter_id, 
                uso.acceptance_letter_id as acceptance_letter_id, 
                uso.date_sent as date_sent, uso.status as status, 
                to_char(uso.date_sent, \'DD Month YYYY\') as date_sent_formatted, 
                c.id as company_id, 
                c.name as company_name, 
                d.name as department_name, 
                r.name as rank_name, c.company_code as company_code, 
                d.department_code as department_code, 
                r.number as rank_code, 
                ur.description as user_role_name, 
                CONCAT(u.first_name, \' \', u.middle_name, \' \', u.last_name) as full_name, 
                u.employee_code as employee_code, 
                u.id as user_id, 
                (SELECT allotment FROM "'.'offer_allotment" oa WHERE u.company_id = oa.company_id AND uso.esop_id = oa.esop_id) as company_allotment, 
                e.name as esop_name, 
                e.grant_date as esop_grant_date, 
                e.total_share_qty as esop_total_share_qty,
                e.currency as esop_currency, 
                e.vesting_years as esop_vesting_years, 
                e.price_per_share as esop_price_per_share, 
                e.offer_expiration as esop_offer_expiration, 
                e.created_by as esop_created_by, 
                e.parent_id as esop_parent_id, 
                (SELECT CONCAT(us.first_name, \' \', us.middle_name, \' \', us.last_name) FROM "'.'user" us WHERE e.created_by = us.id) as esop_created_by_name, 
                (SELECT rank_id FROM "'.'user" us WHERE e.created_by = us.id) as esop_created_by_rank_id, 
                (SELECT name FROM "'.'rank" ra WHERE ra.id = (SELECT rank_id FROM "'.'user" us WHERE e.created_by = us.id)) as esop_created_by_rank_name, 
                (SELECT department_id FROM "'.'user" us WHERE e.created_by = us.id) as esop_created_by_department_id, 
                (SELECT name FROM "'.'department" de WHERE de.id = (SELECT department_id FROM "'.'user" us WHERE e.created_by = us.id)) as esop_created_by_department_name, 
                (SELECT content FROM "'.'offer_letter" ol WHERE uso.offer_letter_id = ol.id) as offer_letter_content, 
                (SELECT content FROM "'.'offer_letter" ol WHERE uso.acceptance_letter_id = ol.id) as acceptance_letter_content, 
                (SELECT com.img FROM "'.'user" us LEFT JOIN company com ON com.id=us.company_id WHERE us.id=e.created_by) as esop_created_company_logo, 
                cu.name as currency_name, cu.name as esop_currency_name, 
                (SELECT stock FROM "'.'user_esop" WHERE uso.user_id = user_id AND uso.esop_id = esop_id LIMIT 1) as accepted 
                FROM "'.'user_stock_offer" uso 
                LEFT JOIN "'.'esop" e ON e.id = uso.esop_id 
                LEFT JOIN "'.'user" u ON u.id = uso.user_id 
                LEFT JOIN "'.'company" c ON u.company_id = c.id 
                LEFT JOIN "'.'department" d ON u.department_id = d.id 
                LEFT JOIN "'.'rank" r ON u.rank_id = r.id 
                LEFT JOIN "'.'user_role" ur ON u.user_role = ur.id 
                LEFT JOIN "'.'currency" cu ON e.currency = cu.id
                WHERE TRUE';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        if($where['operator'] == 'IN')
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                        }
                        else
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                        }
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'e.name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'e.price_per_share')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if ($qualifiers['search_field'] == 'e.grant_date')
            {
                $sql .= " AND DATE(".$qualifiers['search_field'].") BETWEEN ".$qualifiers['keyword']." ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "uso.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        /*$sql .= " GROUP BY uso.id,c.id,d.name,r.name,d.department_code,r.number,ur.description,full_name,u.employee_code,u.id,e.name,e.grant_date,e.total_share_qty,e.currency,e.vesting_years,e.price_per_share,e.offer_expiration,e.created_by,e.parent_id,cu.name";*/


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." OFFSET ".$qualifiers['offset']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function insert_esop_information($esop = array())
    {
        if(is_array($esop) AND count($esop) > 0)
        {
            $this->db->insert("esop", $esop);
            return $this->db->insert_id();
        }
    }

    public function update_esop_information($qualifier = array(), $data = array())
    {
        if(count($qualifier) > 0 AND count($data) > 0)
        {
            $sql = "UPDATE esop e SET ";
            foreach($data as $i => $update)
            {
                if(isset($i) AND strlen($i) > 0 AND isset($update) AND strlen($update) > 0 )
                {
                    $length = count($data) - 1;
                    $sql .= " ".$i." = '".$update."' ";

                    if($i != $length )
                    {
                        $sql .= ",";
                    }
                }
            }
            $sql = rtrim($sql, ",");
            $sql .= " WHERE TRUE ";

            foreach($qualifier as $i => $where)
            {
                if(isset($where['field']) AND strlen($where['value']) > 0 AND isset($where['field']) AND strlen($where['value']) > 0  AND isset($where['operator']))
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";

                }
            }
        }

        $query = $this->db->query($sql);

        return $this->db->affected_rows();
    }

    public function insert_distribution_share_information($distribution_share = array())
    {
        if(is_array($distribution_share) AND count($distribution_share) > 0)
        {
            $this->db->insert("user_stock_offer", $distribution_share);
            return $this->db->insert_id();
        }
    }

    public function update_distribution_share_information($qualifier = array(), $data = array())
    {
        if(count($qualifier) > 0 AND count($data) > 0)
        {
            $sql = "UPDATE user_stock_offer uso SET ";
            foreach($data as $i => $update)
            {
                if(isset($i) AND strlen($i) > 0 AND isset($update) AND strlen($update) > 0 )
                {
                    $length = count($data) - 1;
                    $sql .= " ".$i." = '".$update."' ";

                    if($i != $length )
                    {
                        $sql .= ",";
                    }
                }
            }
            $sql = rtrim($sql, ",");
            $sql .= " WHERE TRUE ";

            foreach($qualifier as $i => $where)
            {
                if(isset($where['field']) AND strlen($where['value']) > 0 AND isset($where['field']) AND strlen($where['value']) > 0  AND isset($where['operator']))
                {
                    if($where['operator'] == 'IN')
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                }
            }
        }

        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function insert_offer_allotment_information($offer_allotment = array())
    {
        if(is_array($offer_allotment) AND count($offer_allotment) > 0)
        {
            $this->db->insert("offer_allotment", $offer_allotment);
            return $this->db->insert_id();
        }
    }

    public function get_currency()
    {
        $sql = "SELECT * from currency";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function insert_user_esop_information($user_esop = array())
    {
        if(is_array($user_esop) AND count($user_esop) > 0)
        {
            $this->db->insert("user_esop", $user_esop);
            return $this->db->insert_id();
        }
    }

    public function insert_vesting_rights_information($vesting_rights = array())
    {
        if(is_array($vesting_rights) AND count($vesting_rights) > 0)
        {
            $this->db->insert("vesting_rights", $vesting_rights);
            return $this->db->insert_id();
        }
    }

    public function check_existing_data($qualifiers = array(), $table = '')
    {
        if(count($qualifiers) > 0 AND strlen($table) >0)
        {
            $sql = "SELECT
                    *
                    FROM {$table}
                    WHERE 1";

            if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
            {
                foreach($qualifiers['where'] as $key => $where)
                {
                    $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                }
            }

            $query = $this->db->query($sql);
            return $query->num_rows();
        }
        else
        {
            return 0;
        }
    }

    public function delete_data($id = 0, $table = '', $field = '', $custom = '')
    {
        if(strlen($id) > 0 AND strlen($table) > 0 && strlen($field) > 0)
        {
            if(strlen($custom) > 0)
            {
                $this->db->where($custom);
            }
            else
            {
                $this->db->where($field, $id);
            }

            $this->db->delete($table);
        }
    }

    public function get_vesting_rights($qualifiers = array())
    {
        $sql = "SELECT
                    v.*, uc.status as uc_status, uc.date_completed as uc_completed,
                    (SELECT count(claim_id) from claim_vesting_rights cvr WHERE vesting_rights_id = v.id) as no_of_claim
                    FROM vesting_rights v
                    LEFT JOIN claim_vesting_rights cvr ON v.id = cvr.vesting_rights_id
                    LEFT JOIN user_claim uc ON cvr.claim_id = uc.id";

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                $sql .= " WHERE ".$where['field']." ".$where['operator']." ".$where['value']." ";
            }
        }
        $sql .= " ORDER BY v.id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_payments($qualifiers = array())
    {
        $sql = "SELECT
                    up.*,
                    pm.name
                    FROM user_payment up
                    LEFT JOIN payment_methods pm ON pm.id = up.payment_type_id";

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
            }
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_personal_stocks($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT ue.id as user_esop_id, CONCAT(u.first_name,\' \', u.middle_name,\' \', u.last_name) as full_name, u.user_role, u.email, u.contact_number, u.contact_number_type, u.department_id, u.company_id, u.img, u.id as user_id, u.employee_code, u.date_created, e.name as name, e.grant_date, e.total_share_qty, e.id as id, e.currency, e.vesting_years, e.parent_id, ue.stock as accepted, e.price_per_share, cu.name as currency, c.name as company_name, r.name as rank_name, d.name as department_name, (SELECT stock_amount from user_stock_offer uso where ue.esop_id = uso.esop_id and ue.user_id = uso.user_id) as stock_offer_amount, 
            (SELECT id from user_stock_offer uso where uso.user_id = ue.user_id AND uso.esop_id = ue.esop_id) as stock_offer_id 
            from "user_esop" ue 
            LEFT JOIN "esop" e ON ue.esop_id = e.id 
            LEFT JOIN "currency" cu ON cu.id = e.currency 
            LEFT JOIN "user" u ON u.id = ue.user_id 
            LEFT JOIN "rank" r ON r.id = u.rank_id 
            LEFT JOIN "company" c ON c.id = u.company_id 
            LEFT JOIN "department" d ON d.id = u.department_id 
            WHERE TRUE';

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                }
            }
        }

        if(isset($qualifiers['group_by']) && strlen($qualifiers['group_by']) > 0)
        {
            $sql .= " GROUP BY ".$qualifiers['group_by'].",ue.id,full_name,u.user_role,u.email,u.contact_number,u.contact_number_type,u.department_id,u.company_id,u.img,u.id,u.employee_code,u.date_created, e.name,e.grant_date,e.total_share_qty,e.id, e.currency, e.vesting_years, e.parent_id,accepted, e.price_per_share,cu.name,company_name,rank_name,department_name ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    function insert_claims($data = array())
    {
        if(count($data) > 0)
        {
            $this->db->insert("user_claim", $data);
            return $this->db->insert_id();
        }
    }

    function insert_claim_map($data = array())
    {
        if(count($data) > 0)
        {
            $this->db->insert_batch("claim_vesting_rights", $data);
            //return $this->db->insert_id();
        }
    }

    public function insert_user_payment($data)
    {
        $this->db->insert('user_payment',$data);
        return $this->db->insert_id();
    }

    public function get_payment_details($vesting_rights_id) {
        $sql = "SELECT * 
        FROM user_payment
        WHERE vesting_rights_id = $vesting_rights_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_payment_methods_data($qualifiers = array())
    {
        $result = array();
        $sql = "SELECT 
                    pm.*
                FROM payment_methods pm";
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        $sql .= " WHERE ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                    }
                    else
                    {
                        $sql .= " WHERE ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "pm.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY pm.id";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function insert_gratuity($data)
    {
        $this->db->insert('gratuity',$data);
        return $this->db->insert_id();
    }

    public function get_gratuity($qualifiers = array())
    {
        $result = array();
        $sql = "SELECT g.*, c.name as currency_name, e.name as esop_name, e.vesting_years as vesting_years 
            FROM gratuity g 
            LEFT JOIN currency c ON c.id = g.currency_id 
            LEFT JOIN esop e ON e.id = g.esop_id ";
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'g.status')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'g.price_per_share')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if ($qualifiers['search_field'] == 'g.grant_date')
            {
                $sql .= " AND DATE(".$qualifiers['search_field'].") BETWEEN ".$qualifiers['keyword']." ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "g.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        if(isset($qualifiers['group_by']))
        {
            $sql .= " GROUP BY ".$qualifiers['group_by']." ";
        }
        else
        {
            $sql .= " GROUP BY g.id,c.name,esop_name,vesting_years ";
        }

        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_all_gratuity($esop_id)
    {
        $sql = 'SELECT 
                *, c.name as currency_name
                FROM gratuity g
                LEFT JOIN currency c ON c.id=g.currency_id
                WHERE g.esop_id=\''.$esop_id.'\' AND g.status = 1';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function update_gratuity($id,$data)
    {
        $query = $this->db
        ->where('gratuity.id',$id)
        ->update('gratuity',$data);
        //print_r($query);exit;
        return $query;
    }

    public function get_user_esop($esop_id)
    {
        $result = array();
        $sql = "SELECT 
                    ue.*

                FROM user_esop ue
                WHERE 1 ";

        $sql .= " AND ue.esop_id = ". $esop_id . " ";
        $sql .= " ORDER BY ue.id DESC ";

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_user_active_vesting_rights($esop_map_id)
    {
        $result = array();
        $sql = "SELECT 
                    vr.*

                FROM vesting_rights vr
                WHERE 1 ";

        $sql .= " AND vr.esop_map_id = ". $esop_map_id . " ";
        $sql .= " AND vr.status = 0 ";
        $sql .= " ORDER BY vr.id DESC ";

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    function get_offer_allotments() {
        $sql = 'SELECT e.id, e.name , e.total_share_qty, 
                (SELECT SUM(ue.stock) from "user_esop" ue WHERE ue.esop_id = e.id) as total_share_accepted, 
                (e.total_share_qty - ((SELECT SUM(ue.stock) from "user_esop" ue WHERE ue.esop_id = e.id) + 
                CASE WHEN (SELECT SUM(ue.stock) from "user_esop" ue WHERE ue.esop_id IN (SELECT bit_and(es.id) from "esop" es WHERE es.parent_id = e.id)) > 0 THEN (SELECT SUM(ue.stock) from "user_esop" ue WHERE ue.esop_id IN (SELECT bit_and(es.id) from "esop" es WHERE es.parent_id = e.id)) ELSE 0 END )) as total_unavailed_main_esop, 
                (SELECT SUM(ue.stock) from user_esop ue WHERE ue.esop_id IN (SELECT bit_and(es.id) from "esop" es WHERE es.parent_id = e.id)) as availed_in_other_esop_batch 
                FROM "esop" e 
                WHERE e.offer_expiration <= now() AND e.offer_expiration IS NOT NULL AND e.parent_id = 0
                GROUP BY e.id,e.name,e.total_share_qty';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_user_esop_data($qualifiers = array())
    {
        $result = array();
        $sql = "SELECT 
                    ue.*

                FROM user_esop ue
                WHERE TRUE ";

        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    if(isset($where['operator']) AND strlen($where['operator']) > 0)
                    {
                        if($where['operator'] == 'IN')
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." ".$where['value']." ";
                        }
                        else
                        {
                            $sql .= " AND ".$where['field']." ".$where['operator']." '".$where['value']."' ";
                        }
                    }
                    else
                    {
                        $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                    }
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "ue.id";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        $sql .= " GROUP BY ue.id";


        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['offset'].", ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    public function get_esop_data_by($qualifiers = array())
    {
        $return = array();
        $sql = "SELECT ";
   
        if(isset($qualifiers['fields']) AND $qualifiers['fields'] !='')
        {
            $sql.= " ".$qualifiers['fields'];
        }
        else{
            $sql.= "e.*, c.id as company_id,c.company_code,c.name as company_name";
        }
        if(isset($qualifiers['from']) AND $qualifiers['from'] !='')
        {
            $sql.= " ".$qualifiers['from'];
        }
        else{
             $sql.= ' FROM "company" c ';
        }
       

        if(isset($qualifiers['joins']) AND $qualifiers['joins'] != "")
        {
            $sql.= " ".$qualifiers['joins'];
        }
        else
        {
             $sql.= '
                CROSS JOIN "esop" e 
                WHERE e.is_deleted = 0 
             ';
        }

        if(count($qualifiers) > 0 )
        {
            if(isset($qualifiers['where']))
            {
                $sql.= " ".$qualifiers['where'];
            }
        }
       
        $query = $this->db->query($sql);

        if(count($query->result_array()) > 0)
        {
            $return = $query->result_array();
        }
       
        return $return;
    }

    public function get_esop_name ($esop_id = 0){
        $result = array();
        if($esop_id > 0)
        {
            $sql = "SELECT 
                        e.name
                    FROM esop e
                    WHERE TRUE ";

            $sql .= " AND e.id = ". $esop_id . " ";

            $query = $this->db->query($sql);
            if(count($query->result_array()) > 0)
            {
                $result = $query->result_array();
            }
        }

        return $result;
    }

    public function get_esop_details($id)
    {
        $result = $this->db->query('SELECT name FROM esop WHERE id ='.$id.'')->row_array();
        return $result['name'];
    }


    public function insert_outstanding_balance_as_payment($data) {
        $this->db->insert('user_payment', $data);
        return $this->db->insert_id();
    }

    public function update_user_esop($id,$data)
    {
        $this->db->update('user_esop', $data, array('id' => $id));
    }

    public function get_esop_search($qualifier)
    {
        $sql = "SELECT id, name
                FROM esop WHERE name LIKE '%$qualifier%'
                ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function get_company_no_result()
    {
        $sql = "SELECT id
                FROM company
                WHERE is_deleted = '0'
                ORDER BY id ASC LIMIT 1 
                ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_users_by_company($company_id)
    {
         $sql = 'SELECT id, CONCAT(first_name,\' \',last_name) as name
                FROM "'.'user"
                WHERE is_deleted = \'0\' AND company_id='.$company_id.'
                ORDER BY name ASC
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_esop_name_info($esop_id)
    {
        $sql = 'SELECT name as esop_name, price_per_share, to_char(grant_date, \'Month DD, YYYY\') as grant_date_formatted
                FROM esop
                WHERE id='.$esop_id.'
                ';  
         $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_user_esop_info($esop_id, $company_id)
    {
        $sql = 'SELECT ue.user_id, u.first_name, u.middle_name, u.last_name, ue.stock as num_shares, e.price_per_share, d.name as department_name, r.name as rank_name, u.employee_code
                FROM "esop" e 
                LEFT JOIN "user_esop" ue ON ue.esop_id=e.id
                LEFT JOIN "user" u ON u.id=ue.user_id
                LEFT JOIN "company" c ON c.id=u.company_id
                LEFT JOIN "department" d ON d.id=u.department_id
                LEFT JOIN "rank" r ON r.id=u.rank_id
                WHERE ue.esop_id=\''.$esop_id.'\' AND u.company_id=\''.$company_id.'\' ORDER BY r.name ASC
                ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function get_payment_records()
    {
        $sql = 'SELECT currency.name || \' \' || up.amount_paid as Amount,
                to_char(up.dividend_date,\'DD Month YYYY\') as Dividend_Date,
                u.first_name || \' \' || u.last_name as Name,
                esop.name as Esop,
                payment_methods.name as Payment_Method,
                esop.grant_date 
                FROM "user_payment" up 
                left join "user" u on up.user_id = u.id 
                left join "esop" on up.esop_id = esop.id 
                left join "payment_methods" on up.payment_type_id = payment_methods.id 
                left join "currency" on up.currency = currency.id';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_esop_info($esop_id) {
        $sql = "SELECT *
        FROM esop
        WHERE id = {$esop_id}";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function submit_offer_extension($esop_id, $update_data) {
        $this->db->where('id', $esop_id);
        $this->db->update('esop', $update_data);
        return $this->db->affected_rows();
        // print_r($update_date);
    }

    public function check_last_code($code) {
        // var_dump($code);
        $sql = "SELECT *
        FROM company
        WHERE company_code = {$code}";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function check_last_generated_code($table) {
        $sql = "SELECT *
        FROM {$table}
        ORDER BY id DESC
        LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_esop_info_claims($esop_id)
    {
         $sql = 'SELECT name, price_per_share, (SELECT date_part(\'year\',age(now(), grant_date)) FROM esop WHERE id=\''.$esop_id.'\') as e_vesting_date
        FROM esop
        WHERE id = \''.$esop_id.'\'';

        $query = $this->db->query($sql);
        return $query->result_array();

    }

    public function get_payment_per_user($esop_id, $user_id)
    {
        $sql = "SELECT up.user_id, up.esop_id, up.amount_paid, pm.name as payment_method_name, to_char(up.date_added, 'Month DD, YYY') as date_added_formatted, to_char(up.dividend_date, 'Month DD, YYY') as dividend_date_formatted, to_char(up.dividend_date, 'YYYY') as dividend_date_year 
                FROM user_payment up 
                LEFT JOIN payment_methods pm ON pm.id=up.payment_type_id 
                WHERE up.user_id='{$user_id}' AND up.esop_id='{$esop_id}' 
                ";

        $query = $this->db->query($sql);
        return $query->result_array();

    }

    public function get_all_esop() {
        $sql = "SELECT *
        FROM esop";

        $query = $this->db->query($sql);
        return $query->result_array();
    }
}