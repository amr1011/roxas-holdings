<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."third_party/dompdf/autoload.inc.php";

use Dompdf\Dompdf;
use Dompdf\Options;

class Esop extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }
       
       $this->load->model('Esop/Esop_model');

        $allowed = $this->check_user_role();

        if(!$allowed && $this->input->is_ajax_request() == false)
        {
            // header('Location: '.base_url().'esop');
            header('Location: '.base_url().'auth/forbidden_403');
        }
    }


    public function index ()
    {
        // $user_role = $this->session->userdata('user_role');
        // if($user_role == 1)
        // {
        //     /*employee*/

        //     header('Location: '.base_url().'esop/personal_stocks');
        // }
        // else if($user_role == 2)
        // {
        //     /*hr department*/

        //     header('Location: '.base_url().'esop/esop_list');
        // }
        // else if($user_role == 3)
        // {
        //     /*hr head*/

        //     header('Location: '.base_url().'esop/esop_list');
        // }
        // else if($user_role == 4)
        // {
        //     /*esop admin*/

        //     header('Location: '.base_url().'esop/esop_list');
        // }
        
        header('Location: '.base_url().'esop/dashboard');
    }

    public function dashboard(){
        
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );

        $user_role = $this->session->userdata('user_role');
        if($user_role == 1)
        {
            /*employee*/
            $this->template->add_content( $this->load->view( 'employee_dashboard', $data, TRUE ) );
        }
        else if($user_role == 2)
        {
            /*hr department*/
            $this->template->add_content( $this->load->view( 'hr_department_dashboard', $data, TRUE ) );
        }
        else if($user_role == 3 || $user_role == 4)
        {
            /*hr head*/
            $this->template->add_content( $this->load->view( 'esop_admin_hr_head_dashboard', $data, TRUE ) );
        }

        $this->template->draw();
    }

    public function esop_list(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'esop_list', $data, TRUE ) );
        $this->template->draw();
    }

    public function personal_stocks(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'personal_stocks', $data, TRUE ) );
        $this->template->draw();
    }

    public function personal_stock_view(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'personal_stock_view', $data, TRUE ) );
        $this->template->draw();
    }

    public function esop_gratuity_list(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'esop_gratuity_list', $data, TRUE ) );
        $this->template->draw();
    }

    public function payment_records(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'payment_records', $data, TRUE ) );
        $this->template->draw();
    }

    public function download_letters($stock_offer_id = '', $user_id = '')
    {
  

            $parsed_offer_letters = '';
           
            
            if(isset($_POST['data']))
            {
                 
                   $_POST['data'] = json_decode($_POST['data'],true);
                   foreach ($_POST['data'] as $key => $value) {
                   
                       if(strlen($value['stock_offer_id']) > 0 && strlen($value['user_id']) > 0)
                        {
                            $qualifiers = array(
                                'where' => array(
                                    array(
                                        'field' => 'uso.user_id',
                                        'operator' => '=',
                                        'value' => $value['user_id'],
                                    ),
                                    array(
                                        'field' => 'uso.id',
                                        'operator' => '=',
                                        'value' => $value['stock_offer_id'],
                                    )
                                )
                            );

                            
                            $user_stock_offers = $this->Esop_model->get_user_stock_offer_data($qualifiers);
                            $total_rows = $user_stock_offers['total_rows'];
                            unset($user_stock_offers['total_rows']);


                            $this->load->helper('parse_letter_helper');
                            
                            if(count($user_stock_offers) > 0)
                            {
                                foreach ($user_stock_offers as $i => $user_stock_offer) {
                                    $parsed_offer_letter = parse_letter($user_stock_offer[$_POST['type']], $user_stock_offer);
                                }

                              
                                $parsed_offer_letter = '<div style="page-break-after:always">'.$parsed_offer_letter.'</div>';
                                $parsed_offer_letters.= '<div style="page-break-after:always">'.$parsed_offer_letter.'</div>';
                           
                             
                            }
                         }
                    }
            }
  
            $dompdf = new DOMPDF();
            $dompdf->load_html($parsed_offer_letters);

            $dompdf->render();
            $output = $dompdf->output();
            
            $path = './assets/downloads/';

            if(!is_dir($path)) //create the folder if it's not already exists
            {
                mkdir($path,0777,TRUE);
            }

            $file_name = 'offerletters'.time().'.pdf';
            $dir_path = './assets/downloads/'.$file_name;
            $url_path = base_url() . "assets/downloads/" . $file_name;
            $url_path = str_replace("/", "\\/", $url_path);
            file_put_contents($dir_path, $output);
            $response['url'] = $url_path;
            $response['status'] = 'success';

    
        echo json_encode($response);

    }

    public function group_wide_stocks()
    {

        $qualifiers = '';

        if($this->session->userdata('user_role') == 2)
        {
            $qualifiers['fields'] = " e.*, d.id as company_id,d.name as company_name ";
            $qualifiers['from']   = ' FROM "department" d ';
    
            $qualifiers['joins']  = '
                        CROSS JOIN "esop" e 
                        WHERE e.is_deleted = 0  ANd d.company_id = \''.$this->session->userdata('company_id').'\'
            ';

        }

        $data['companies'] = array();

        $esop_data = $this->Esop_model->get_esop_data_by($qualifiers);

        $return_data = array();

        if(count($esop_data) > 0)
        {
            foreach ($esop_data as $key => $esop) {
                if(!isset($return_data[$esop['company_name']]))
                {
                    $return_data[$esop['company_name']] = array();

                }

                if(!isset($return_data[$esop['company_name']]))
                {
                    $return_data[$esop['company_name']][$esop_data['id']] = array();
                }
               
               $return_data[$esop['company_name']][$esop['id']] = $esop;
               $data['companies'][$esop['company_id']] = $esop['company_name'] ;
               
            }

        }

        $data['esop'] = $return_data;

        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'view_esop_group_wide', $data, TRUE ) );
        $this->template->draw();
    }
    public function save_upon_edit_stock_offers()
    {

       $return = array('status'=>'fail');

       if($_POST)
       {

            if($this->input->post('status') == 'Accepted')
            {
                $update = [];
              
                $update['stock'] = $this->input->post('shares');
                $update['date_accepted'] = date('Y-m-d H:i:s');
                $id  = $this->Esop_model->update_user_esop($this->input->post('user_esop_id'),$update);
                $return['status'] = 'success';
                $return['type'] = 'update';
                $return['id']   = $id;

                $_POST['params']['esop_name'] = $this->Esop_model->get_esop_details($this->input->post('esop_id'));
                $_POST['params']['action'] = ' Updated ';
                $_POST['params']['empname'] = $this->input->post('empname');
                $_POST['params']['esop_id'] = $this->input->post('esop_id');
                $_POST['params']['user_id'] = $this->input->post('user_id');
                $_POST['params']['stock'] = $this->input->post('shares');
                $_POST['status'] = TRUE; 
                
            }
            elseif($this->input->post('status') == 'Waiting')
            {
                $insert = [];
                $insert['esop_id'] = $this->input->post('esop_id');
                $insert['user_id'] = $this->input->post('user_id');
                $insert['stock'] = $this->input->post('shares');
                $insert['date_accepted'] = date('Y-m-d H:i:s');
                $insert['status'] = 0;

                $id = $this->Esop_model->insert_user_esop_information($insert);
                $return['status'] = 'success';
                $return['type'] = 'insert';
                $return['id']   = $id;


                $_POST['params']['esop_name'] = $this->Esop_model->get_esop_details($this->input->post('esop_id'));
                $_POST['params']['action'] = ' Accepted ';
                $_POST['params']['empname'] = $this->input->post('empname');
                $_POST['params']['esop_id'] = $this->input->post('esop_id');
                $_POST['params']['user_id'] = $this->input->post('user_id');
                $_POST['params']['stock'] = $this->input->post('shares');
                $_POST['status'] = TRUE; 


            }


       }
        echo json_encode($return);
    }
    public function view_group_wide_stock_offers($company_id,$esop_id)
    {

        $qualifiers = array();

        $qualifiers['fields'] =  " e.*,
                                   uso.id as stock_offer_id, c.name as company_name,
                                   CONCAT(u.first_name,' ',u.middle_name,' ',u.last_name) as fullname, 
                                   uso.offer_letter_id ,
                                   uso.acceptance_letter_id, 
                                   ue.date_accepted,u.id as user_id,
                                   ue.id as user_esop_id,
                                   uso.stock_amount as allotment,
                                   CASE WHEN uso.acceptance_letter_id > 0 THEN ue.stock ELSE uso.stock_amount END as accepted_shares 
                                  ";

        $qualifiers['from']   = " FROM \"user_stock_offer\" uso ";

        // /LEFT JOIN user_claim uc ON uc.user_id = u.id

        $qualifiers['joins']  = "
                        INNER JOIN \"esop\" e ON e.id = uso.esop_id
                        INNER JOIN \"user\" u ON u.id = uso.user_id
                        INNER JOIN \"company\" c ON c.id = u.company_id
                        INNER JOIN \"department\" d ON d.id = u.department_id
                        LEFT JOIN \"user_esop\" ue ON ue.esop_id = uso.esop_id AND ue.user_id = uso.user_id
        ";

        $qualifiers['where'] = " WHERE e.is_deleted = 0 AND c.id ='{$company_id}'  AND uso.esop_id = '{$esop_id}'";

        if($this->session->userdata('user_role') == 2)
        {
             $qualifiers['fields'] =  " e.*,
                                   uso.id as stock_offer_id, d.name as company_name,
                                   CONCAT(u.first_name,' ',u.middle_name,' ',u.last_name) as fullname, 
                                   uso.offer_letter_id ,
                                   uso.acceptance_letter_id, 
                                   ue.date_accepted,u.id as user_id,
                                   ue.id as user_esop_id,
                                   uso.stock_amount as allotment,
                                   CASE WHEN uso.acceptance_letter_id > 0 THEN ue.stock ELSE uso.stock_amount END as accepted_shares 
                                  ";

            $qualifiers['where'] = " WHERE e.is_deleted = 0 AND u.department_id ='{$company_id}'  AND uso.esop_id = '{$esop_id}' and uso.user_id IN (SELECT id FROM \"user\" WHERE department_id = ".$company_id." )";
        }

        $esop_data = $this->Esop_model->get_esop_data_by($qualifiers);
        $return_data = array();
    

        if(count($esop_data) > 0)
        {
            foreach ($esop_data as $key => $esop) {

                if(!isset($return_data[$esop['company_name']]))
                {
                    $return_data[$esop['company_name']] = array();
                }

                if(!isset($return_data[$esop['company_name']]))
                {
                    $return_data[$esop['company_name']][$esop['stock_offer_id']] = array();
                }

               if(isset($esop['date_claimed']) AND $esop['date_claimed'] != '0000-00-00 00:00:00' )
               {
                $esop['status'] = 'Claimed';
               }
               else if(isset($esop['date_accepted']) AND $esop['date_accepted'] != '0000-00-00 00:00:00' )
               {
                $esop['status'] = 'Accepted';
               }
               else
               {
                $esop['status'] = 'Waiting';
               }

               $return_data[$esop['company_name']][$esop['stock_offer_id']] = $esop;
            }
        }

        $data['esop_name'] = $this->Esop_model->get_esop_details($esop_id);
        $data['esop'] = $return_data;
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'view_group_esop_offer.php', $data, TRUE ) );
        $this->template->draw();
    }

    public function stock_offers(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'stock_offers', $data, TRUE ) );
        $this->template->draw();
    }

    public function view_esop(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'view_esop', $data, TRUE ) );
        $this->template->draw();
    }

    public function view_esop_batch(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'view_esop_batch', $data, TRUE ) );
        $this->template->draw();
    }

    public function add_distribute_shares(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'add_distribute_shares', $data, TRUE ) );
        $this->template->draw();
    }

    public function edit_distribute_shares(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'edit_distribute_shares', $data, TRUE ) );
        $this->template->draw();
    }

    public function batch_add_distribute_shares(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'batch_add_distribute_shares', $data, TRUE ) );
        $this->template->draw();
    }

    public function batch_edit_distribute_shares(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'batch_edit_distribute_shares', $data, TRUE ) );
        $this->template->draw();
    }

    public function view_stock_offer(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'view_stock_offer', $data, TRUE ) );
        $this->template->draw();
    }

    public function claim_form() {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'claim_form', $data, TRUE ) );
        $this->template->draw();
    }

    public function get ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'e.id',
                    'name' => 'e.name',
                    'grant_date' => 'e.grant_date',
                    'total_share_qty' => 'total_share_qty',
                    'price_per_share' => 'e.price_per_share',
                    'currency' => 'e.currency',
                    'vesting_years' => 'e.vesting_years',
                    'is_deleted' => 'e.is_deleted',
                    'created_by' => 'e.created_by',
                    'status' => 'e.status',
                    'parent_id' => 'e.parent_id',
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $total_rows = 0;

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "e.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;
            $esops = $this->Esop_model->get_esop_data($qualifiers);
            $total_rows = $esops['total_rows'];
            unset($esops['total_rows']);


            if(count($esops) > 0)
            {
                $esop_data = array();
                foreach ($esops as $i => $esop) {
                    $total_shares_availed_esop = 0;
                    $total_shares_unavailed_esop = 0;
             
                    $get_user_stock_offer_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "uso.esop_id", 
                                "value" => $esop['id']
                            )
                        ) 
                    );

                    $user_stock_offers = $this->Esop_model->get_user_stock_offer_data($get_user_stock_offer_qualifiers);
                    unset($user_stock_offers['total_rows']);

                    $esop_data[$i] = $esop;
                    $esop_data[$i]['distribution'] = array();

                    $total_shares_availed_company = 0;
                    $total_shares_unavailed_company = 0;
                    foreach ($user_stock_offers as $key => $user_stock_offer) {
                        // $company_allotment = $this->Esop_model->get_company_allotment();

                        $esop_data[$i]['letter_sent'] = $user_stock_offer['status']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['id'] = $user_stock_offer['company_id']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['company_id'] = $user_stock_offer['company_id']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['company_name'] = $user_stock_offer['company_name']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['employees'][] = $user_stock_offer; 

                        $total_shares_availed_company += $user_stock_offer['employee_alloted_shares'];
                        $total_shares_availed_esop += $user_stock_offer['employee_alloted_shares'];
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['total_shares_availed'] = $total_shares_availed_company;

                        $total_shares_unavailed_company = $user_stock_offer['company_allotment'] - $total_shares_availed_company;
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['total_shares_unavailed'] = $total_shares_unavailed_company; 

                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['total_alloted_shares'] = $user_stock_offer['company_allotment']; 
                        $esop_data[$i]['distribution'][$user_stock_offer['company_id']]['company_allotment'] = $user_stock_offer['company_allotment'];
                    }

                    $esop_data[$i]['total_shares_availed'] = $total_shares_availed_esop;
                    $esop_data[$i]['total_shares_unavailed'] = $esop['total_share_qty'] - $total_shares_availed_esop;

                    $get_esop_batch_qualifiers = array(
                        "limit" => 999999, 
                        "where" => array(
                            array(
                                "field" => "e.parent_id", 
                                "value" => $esop['id']
                            ),
                            array(
                                "field" => "e.is_special", 
                                "value" => 0
                            )
                        ) 
                    );

                    $esop_batches = $this->Esop_model->get_esop_data($get_esop_batch_qualifiers);
                    unset($esop_batches['total_rows']);

                    $esop_data[$i]['esop_batches'] = array();

                    if(isset($esop_batches) AND count($esop_batches) > 0)
                    {
                        $esop_data[$i]['esop_batches'] = $esop_batches;
                        foreach ($esop_batches as $x => $esop_batch) {
                            $total_shares_availed_esop = 0;
                            $total_shares_unavailed_esop = 0;

                            $get_user_stock_offer_qualifiers = array(
                                "limit" => 999999, 
                                "where" => array(
                                    array(
                                        "field" => "uso.esop_id", 
                                        "value" => $esop_batch['id']
                                    )
                                ) 
                            );

                            $user_stock_offers = $this->Esop_model->get_user_stock_offer_data($get_user_stock_offer_qualifiers);
                            unset($user_stock_offers['total_rows']);

                            $esop_data[$i]['esop_batches'][$x] = $esop_batch;
                            $esop_data[$i]['esop_batches'][$x]['distribution'] = array();

                            $total_shares_availed_company = 0;
                            $total_shares_unavailed_company = 0;
                            foreach ($user_stock_offers as $key => $user_stock_offer) {
                                $esop_data[$i]['esop_batches'][$x]['letter_sent'] = $user_stock_offer['status']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['id'] = $user_stock_offer['company_id']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['company_id'] = $user_stock_offer['company_id']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['company_name'] = $user_stock_offer['company_name']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['employees'][] = $user_stock_offer; 

                                $total_shares_availed_company += $user_stock_offer['employee_alloted_shares'];
                                $total_shares_availed_esop += $user_stock_offer['employee_alloted_shares'];
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['total_shares_availed'] = $total_shares_availed_company;

                                $total_shares_unavailed_company = $user_stock_offer['company_allotment'] - $total_shares_availed_company;
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['total_shares_unavailed'] = $total_shares_unavailed_company; 

                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['total_alloted_shares'] = $user_stock_offer['company_allotment']; 
                                $esop_data[$i]['esop_batches'][$x]['distribution'][$user_stock_offer['company_id']]['company_allotment'] = $user_stock_offer['company_allotment'];
                            }
                            $esop_data[$i]['esop_batches'][$x]['total_shares_availed'] = $total_shares_availed_esop;
                            $esop_data[$i]['esop_batches'][$x]['total_shares_unavailed'] = $esop_batch['total_share_qty'] - $total_shares_availed_esop;
                        }
                    }
                }

                $esops = $esop_data;
                $response['total_rows'] = $total_rows;
                $response['data'] = $esops;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function get_currency()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $currency = $this->Esop_model->get_currency();

        if(count($currency) > 0)
        {
            $response['status'] = TRUE;
            $response['data'] = $currency;
        }

        echo json_encode($response);
    }

    public function add ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $esop_information = $this->input->post();

        if(isset($esop_information) && count($esop_information) > 0)
        {
            /*validate esop name*/
            if(isset($esop_information['name']) AND strlen(trim($esop_information['name'])) > 0)
            {
                if(strlen(trim($esop_information['name'])) > 1)
                {
                    $esop_information_input['name'] = $esop_information['name'];
                }
                else
                {
                    $response['message'][] = "Invalid esop name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Esop name is required.";
                $error++; 
            }

            /*validate grant date*/
            if(isset($esop_information['grant_date']) AND strlen(trim($esop_information['grant_date'])) > 0)
            {
                if(strlen(trim($esop_information['grant_date'])) > 1)
                {
                    $esop_information_input['grant_date'] = date('Y-m-d' , strtotime($esop_information['grant_date']));
                }
                else
                {
                    $response['message'][] = "Invalid grant date or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Grant date is required.";
                $error++; 
            }

            /*validate total share quantity*/
            if(isset($esop_information['total_share_qty']) AND strlen(trim($esop_information['total_share_qty'])) > 0)
            {
                if(strlen(trim($esop_information['total_share_qty'])) > 1 AND preg_match("/^[0-9\,\.]+$/", $esop_information['total_share_qty']) )
                {
                    $esop_information_input['total_share_qty'] = str_replace(",", "", $esop_information['total_share_qty']);
                }
                else
                {
                    $response['message'][] = "Invalid total share quantity or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Total share quantity is required.";
                $error++; 
            }

            /*validate price per share*/
            if(isset($esop_information['price_per_share']) AND strlen(trim($esop_information['price_per_share'])) > 0)
            {
                if(strlen(trim($esop_information['price_per_share'])) > 1 AND preg_match("/^[0-9\,\.]+$/", $esop_information['price_per_share']) )
                {
                    $esop_information_input['price_per_share'] = str_replace(",", "", $esop_information['price_per_share']);
                }
                else
                {
                    $response['message'][] = "Invalid price per share or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Price per share is required.";
                $error++; 
            }

            /*validate currency*/
            if(isset($esop_information['currency']) AND strlen(trim($esop_information['currency'])) > 0)
            {
                if(preg_match("/^[0-9]+$/", $esop_information['currency']) )
                {
                    $esop_information_input['currency'] = $esop_information['currency'];
                }
                else
                {
                    $response['message'][] = "Invalid currency or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Currency is required.";
                $error++; 
            }

            /*validate vesting years*/
            if(isset($esop_information['vesting_years']) AND strlen(trim($esop_information['vesting_years'])) > 0)
            {
                if(preg_match("/^[0-9]+$/", $esop_information['vesting_years']) )
                {
                    $esop_information_input['vesting_years'] = $esop_information['vesting_years'];
                }
                else
                {
                    $response['message'][] = "Invalid vesting years or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Vesting years is required.";
                $error++; 
            }

            /*validate created by*/
            if(isset($esop_information['created_by']) AND strlen(trim($esop_information['created_by'])) > 0)
            {
                if(strlen(trim($esop_information['created_by'])) > 0 AND preg_match("/^[0-9]+$/", $esop_information['created_by']) )
                {
                    $esop_information_input['created_by'] = $esop_information['created_by'];
                }
                else
                {
                    $response['message'][] = "Invalid created by or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Created by is required.";
                $error++; 
            }

            $esop_information_input['status'] = isset($esop_information['status']) ? $esop_information['status'] : 1 ;
            $esop_information_input['is_deleted'] = isset($esop_information['is_deleted']) ? $esop_information['is_deleted'] : 0 ;
            $esop_information_input['parent_id'] = isset($esop_information['parent_id']) ? $esop_information['parent_id'] : 0 ;
            $esop_information_input['offer_expiration'] = isset($esop_information['offer_expiration']) ? date('Y-m-d' , strtotime($esop_information['offer_expiration'])) : null;
            $esop_information_input['is_special'] = isset($esop_information['is_special']) ? $esop_information['is_special'] : 0 ;
            
            if($error == 0)
            {
                /*insert if all inputs are okay*/
                $added_esop_information_id = $this->Esop_model->insert_esop_information($esop_information_input);

                if($added_esop_information_id > 0)
                {
                    $response['message'][] = "Esop added successfully.";
                    $response['status'] = TRUE;
                    $response['data'] = $esop_information_input;
                    $response['data']['id'] = $added_esop_information_id;

                    if(isset($esop_information['share_allotment']) && count($esop_information['share_allotment']) > 0 )
                    {
                        foreach($esop_information['share_allotment'] as $i => $esop)
                        {
                            $data = array(
                                'id' => $esop['id'],
                                'data' => array(
                                    array(
                                        'total_share_qty' => $esop['total_share_qty']
                                    )
                                )
                            );

                            $this->edit($data);

                            $esop_batches = $this->Esop_model->get_esop_data(array(
                                'where' => array(
                                    array(
                                        'field' => 'e.parent_id',
                                        'operator' => '=',
                                        'value' => $esop['id']
                                    )
                                )
                            ));

                            if(count($esop_batches) > 0)
                            {
                                unset($esop_batches['total_rows']);
                                foreach($esop_batches as $i => $esop_batch)
                                {

                                        $data = array(
                                            'id' => $esop_batch['id'],
                                            'data' => array(
                                                array(
                                                    'total_share_qty' => $esop_batch['total_share_qty'] - $esop['deduction']
                                                )
                                            )
                                        );

                                        $this->edit($data);


                                }
                            }
                        }
                    }

                    /*for logs*/
                    $_POST['params'] = $response['data'];
                    $_POST['status'] = TRUE;
                    
                    if(isset($esop_information['type']))
                    {
                        $_POST['params']['type'] = $esop_information['type'];
                    }
                    else
                    {
                        $_POST['params']['type'] = 'main_esop';
                    }
                }


            }
        }

        echo json_encode($response);
    }

    public function edit ($data = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        if(count($data) > 0)
        {
            $esop_information = $data;
        }
        else{
            $esop_information = $this->input->post();
        }


        if(isset($esop_information) && count($esop_information) > 0)
        {
            if(isset($esop_information['data']) AND is_array($esop_information['data']) AND count($esop_information['data']) > 0 AND isset($esop_information['id']) AND $esop_information['id'] > 0)
            {
                foreach ( $esop_information['data'][0] as $i => $update_data )
                {
                    if ( strlen( $i ) > 0 )
                    {
                        /*validate fields*/

                        /*validate esop name*/
                        if($i == 'name')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 1)
                                {
                                    $esop_information_input['name'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid esop name or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Esop name is required.";
                                $error++; 
                            }
                        }

                        /*validate grant date*/
                        if($i == 'grant_date')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 1)
                                {
                                    $esop_information_input['grant_date'] = date('Y-m-d' , strtotime($update_data));
                                    $esop_information['data'][0]['grant_date'] = date('Y-m-d' , strtotime($update_data));
                                }
                                else
                                {
                                    $response['message'][] = "Invalid grant date or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Grant date is required.";
                                $error++; 
                            }
                        }

                        /*validate total share quantity*/
                        if($i == 'total_share_qty')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 1 AND preg_match("/^[0-9\,\.]+$/", $update_data) )
                                {
                                    $esop_information_input['total_share_qty'] = str_replace(",", "", $update_data);
                                    $esop_information['data'][0]['total_share_qty'] = str_replace(",", "", $update_data);
                                }
                                else
                                {
                                    $response['message'][] = "Invalid total share quantity or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Total share quantity is required.";
                                $error++; 
                            }
                        }

                        /*validate price per share*/
                        if($i == 'price_per_share')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 1 AND preg_match("/^[0-9\,\.]+$/", $update_data) )
                                {
                                    $esop_information_input['price_per_share'] = str_replace(",", "", $update_data);
                                    $esop_information['data'][0]['price_per_share'] = str_replace(",", "", $update_data);
                                }
                                else
                                {
                                    $response['message'][] = "Invalid price per share or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Price per share is required.";
                                $error++; 
                            }
                        }

                        /*validate currency*/
                        if($i == 'currency')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(preg_match("/^[0-9]+$/", $update_data) )
                                {
                                    $esop_information_input['currency'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid currency or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Currency is required.";
                                $error++; 
                            }
                        }
                        /*validate currency name for logs*/
                        if ($i == 'currency_name')
                        {
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $esop_information_input['currency_name'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid currency ID or is too short";
                                    // $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Currency ID is required";
                                // $error ++;
                            }
                        }

                        /*validate vesting years*/
                        if($i == 'vesting_years')
                        {                        
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(preg_match("/^[0-9]+$/", $update_data) )
                                {
                                    $esop_information_input['vesting_years'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid vesting years or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Vesting years is required.";
                                $error++; 
                            }
                        }

                        /*validate created by*/
                        if($i == 'created_by')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data) )
                                {
                                    $esop_information_input['created_by'] = $update_data;
                                }
                                else
                                {
                                    $response['message'][] = "Invalid created by or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Created by is required.";
                                $error++; 
                            }
                        }

                        /*validate offer expiration*/
                        if($i == 'offer_expiration')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 0)
                                {
                                    $esop_information_input['offer_expiration'] = date('Y-m-d' , strtotime($update_data));
                                    $esop_information['data'][0]['offer_expiration'] = date('Y-m-d' , strtotime($update_data));
                                }
                                else
                                {
                                    $response['message'][] = "Invalid offer expiration or is too short";
                                    $error++;
                                }
                            }
                            else
                            {
                                $response['message'][] = "Offer expiration is required.";
                                $error++; 
                            }
                        }

                        /*validate status*/
                        if($i == 'status')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                                {
                                    $esop_information_input['status'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid status or is too short";
                                    // $error++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Status is required.";
                                // $error++; 
                            }
                        }

                        /*validate is deleted*/
                        if($i == 'is_deleted')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                                {
                                    $esop_information_input['is_deleted'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid is deleted or is too short";
                                    // $error++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Is deleted is required.";
                                // $error++; 
                            }
                        }

                        /*validate parent id*/
                        if($i == 'parent_id')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                                {
                                    $esop_information_input['parent_id'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid parent ID or is too short";
                                    // $error++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Parent ID is required.";
                                // $error++; 
                            }
                        }

                        /*validate is special*/
                        if($i == 'is_special')
                        {
                            if(isset($update_data) AND strlen(trim($update_data)) > 0)
                            {
                                if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                                {
                                    $esop_information_input['is_special'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid parent ID or is too short";
                                    // $error++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Parent ID is required.";
                                // $error++; 
                            }
                        }
                    }
                }

                if($error == 0)
                {
                    /*update if all inputs are okay*/
                    if($error == 0)
                    {
                        $logs_from = $this->Esop_model->get_esop_data(
                                array(
                                    'where' => array(
                                        array(
                                            'field' => 'e.id',
                                            'operator' => '=',
                                            'value' => $esop_information['id'],
                                        )
                                    )
                                )
                            );
                        if(isset($esop_information['data'][0]['currency_name'])) { unset($esop_information['data'][0]['currency_name']); }

                        $updated_esop_information_id = $this->Esop_model->update_esop_information(
                            array(
                                array(
                                'field' => 'e.id',
                                'value' => $esop_information['id'],
                                'operator' => '='
                                )
                            ), 
                            $esop_information['data'][0]);

                        if($updated_esop_information_id > 0)
                        {
                            $response['message'][] = "Esop updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $esop_information_input;
                            $response['data']['id'] = $esop_information['id'];

                            /*for logs*/
                            if(isset($esop_information['type']))
                            {
                                $_POST['params']['type'] = $esop_information['type'];
                                $esop_name = "";
                                if(array_key_exists("name", $esop_information)){
                                    $esop_name = $esop_information['name'];
                                }elseif(array_key_exists("esop_name", $esop_information)){
                                    $esop_name = $esop_information['esop_name'];
                                }
                                $_POST['params']['esop_name'] = $esop_name;
                            }
                            else
                            {
                                $_POST['params']['type'] = 'esop_normal_edit';
                            }

                            $_POST['params']['id'] = $esop_information['id'];
                            $_POST['params']['to'] = $response['data'];
                            $_POST['params']['from'] = $logs_from[0];
                            $_POST['params']['diff'] = array_diff_assoc($_POST['params']['to'], $_POST['params']['from']);
                            $_POST['status'] = TRUE;
                        }
                        else
                        {
                            // $response['message'][] = "Nothing is changed.";
                            $response['message'][] = "Esop updated successfully.";
                            $response['status'] = TRUE;
                            $response['data'] = $esop_information_input;
                            $response['data']['id'] = $esop_information['id'];
                        }
                        $response['data']['affected_rows'] = $updated_esop_information_id;
                    }
                }
            }
        }

        if(count($data) > 0)
        {
            //test
        }
        else{
            echo json_encode($response);
        }

    }

    public function add_distribute_share ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        $user_offer_data = array();

        if(isset($params['employee_data']) && count($params['employee_data']) > 0
            && isset($params['allotment_data']) && count($params['allotment_data']) > 0
            && isset($params['esop_id']))
        {
            foreach ($params['employee_data'] as $key => $distribution_share_info)
            {
                $error = 0;
                $distribution_share_information_input = array(
                        "esop_id" => '',
                        "user_id" => '',
                        "stock_amount" => '',
                        "offer_letter_id" => '',
                        "acceptance_letter_id" => '',
                        "date_sent" => ''
                    );

                foreach ($distribution_share_info as $i => $update_data)
                {
                    /*validate esop id*/
                    if($i == 'esop_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['esop_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid esop id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Esop id is required.";
                            $error++; 
                        }
                    }

                    /*validate user id*/
                    if($i == 'user_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['user_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid user id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "User id is required.";
                            $error++; 
                        }
                    }

                    /*validate stock amount*/
                    if($i == 'stock_amount')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9\,\.]+$/", $update_data))
                            {
                                $distribution_share_information_input['stock_amount'] = str_replace(",", "", $update_data);
                            }
                            else
                            {
                                $response['message'][] = "Invalid stock amount or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Stock amount is required.";
                            $error++; 
                        }
                    }

                    /*validate offer letter id*/
                    if($i == 'offer_letter_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['offer_letter_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid offer letter id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Offer letter id is required.";
                            $error++; 
                        }
                    }

                    /*validate acceptance letter id*/
                    if($i == 'acceptance_letter_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['acceptance_letter_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid acceptance letter id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Acceptance letter id is required.";
                            $error++; 
                        }
                    }

                    $distribution_share_information_input['date_sent'] = date('Y-m-d H:i:s');

                }
                if($error == 0)
                {
                    /*insert if all inputs are okay*/
                    $added_distribution_share_information_id = $this->Esop_model->insert_distribution_share_information($distribution_share_information_input);

                    if($added_distribution_share_information_id > 0)
                    {
                        $response['message'][] = "Distribution shares added successfully.";
                        $response['status'] = TRUE;
                        $response_input = $distribution_share_information_input;
                        $response_input['id'] = $added_distribution_share_information_id;
                        $response['data'][] = $response_input;

                        $user_offer_data[] = $response_input;
                    }
                }
            }

            foreach ($params['allotment_data'] as $key => $offer_allotment_info)
            {
                $error_allotment = 0;

                $offer_allotment_information_input = array(
                        "esop_id" => '',
                        "company_id" => '',
                        "department_id" => '',
                        "allotment" => ''
                    );

                foreach ($offer_allotment_info as $i => $update_data)
                {
                    /*validate esop id*/
                    if($i == 'esop_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $offer_allotment_information_input['esop_id'] = $update_data;
                            }
                            else
                            {
                                // $response['message'][] = "Invalid esop id or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Esop id is required.";
                            // $error++; 
                        }
                    }

                    /*validate company id*/
                    if($i == 'company_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $offer_allotment_information_input['company_id'] = $update_data;
                            }
                            else
                            {
                                // $response['message'][] = "Invalid company id or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Company id is required.";
                            // $error++; 
                        }
                    }

                    /*validate department id*/
                    if($i == 'department_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $offer_allotment_information_input['department_id'] = $update_data;
                            }
                            else
                            {
                                // $response['message'][] = "Invalid department id or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Department id is required.";
                            // $error++; 
                        }
                    }

                    /*validate allotment*/
                    if($i == 'allotment')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9\,\.]+$/", $update_data))
                            {
                                $offer_allotment_information_input['allotment'] = str_replace(",", "", $update_data);
                            }
                            else
                            {
                                // $response['message'][] = "Invalid allotment or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Allotment is required.";
                            // $error++; 
                        }
                    }
                }

                if($error_allotment == 0)
                {
                    $offer_allotment_information_id = $this->Esop_model->insert_offer_allotment_information($offer_allotment_information_input);
                   
                    if($offer_allotment_information_id > 0)
                    {
                        $response['message'][] = "Offer Allotment added successfully.";
                        $response['status'] = TRUE;
                        $response_input = $offer_allotment_information_input;
                        $response_input['id'] = $offer_allotment_information_id;
                        $response['allotment_data'][] = $response_input;
                    }
                }
            }

            if($error == 0 && $error_allotment ==  0)
            {
                /*for logs*/
                $_POST['params']['user_offer_data'] = $user_offer_data;
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['params']['esop_name'] = $params['esop_name'];
                $_POST['status'] = TRUE; 
            }
        }

        echo json_encode($response);
    }

    public function edit_distribute_share ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        $user_offer_data = array();

        if(isset($params['employee_data']) && count($params['employee_data']) > 0
            && isset($params['allotment_data']) && count($params['allotment_data']) > 0
            && isset($params['esop_id']) && strlen($params['esop_id']) > 0)
        {
            $delete_user_stock_offer_data = $this->Esop_model->delete_data($params['esop_id'], 'user_stock_offer', 'esop_id');
            $delete_offer_allotment_data = $this->Esop_model->delete_data($params['esop_id'], 'offer_allotment', 'esop_id');

            foreach ($params['employee_data'] as $key => $distribution_share_info)
            {
                $error = 0;
                $distribution_share_information_input = array(
                        "esop_id" => '',
                        "user_id" => '',
                        "stock_amount" => '',
                        "offer_letter_id" => '',
                        "date_sent" => ''
                    );

                foreach ($distribution_share_info as $i => $update_data)
                {
                    /*validate esop id*/
                    if($i == 'esop_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['esop_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid esop id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Esop id is required.";
                            $error++; 
                        }
                    }

                    /*validate user id*/
                    if($i == 'user_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['user_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid user id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "User id is required.";
                            $error++; 
                        }
                    }

                    /*validate stock amount*/
                    if($i == 'stock_amount')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9\,\.]+$/", $update_data))
                            {
                                $distribution_share_information_input['stock_amount'] = str_replace(",", "", $update_data);
                            }
                            else
                            {
                                $response['message'][] = "Invalid stock amount or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Stock amount is required.";
                            $error++; 
                        }
                    }

                    /*validate offer letter id*/
                    if($i == 'offer_letter_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['offer_letter_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid offer letter id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Offer letter id is required.";
                            $error++; 
                        }
                    }

                    /*validate acceptance letter id*/
                    if($i == 'acceptance_letter_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $distribution_share_information_input['acceptance_letter_id'] = $update_data;
                            }
                            else
                            {
                                $response['message'][] = "Invalid acceptance letter id or is too short";
                                $error++;
                            }
                        }
                        else
                        {
                            $response['message'][] = "Acceptance letter id is required.";
                            $error++; 
                        }
                    }

                    $distribution_share_information_input['date_sent'] = date('Y-m-d H:i:s');

                }

                if($error == 0)
                {
                    /*insert if all inputs are okay*/
                    $edit_distribution_share_information_id = $this->Esop_model->insert_distribution_share_information($distribution_share_information_input);

                    if($edit_distribution_share_information_id > 0)
                    {
                        $response['message'][] = "Distribution shares edited successfully.";
                        $response['status'] = TRUE;
                        $response_input = $distribution_share_information_input;
                        $response_input['id'] = $edit_distribution_share_information_id;
                        $response['data'][] = $response_input;

                        $user_offer_data[] = $response_input;
                    }
                }
            }

            foreach ($params['allotment_data'] as $key => $offer_allotment_info)
            {
                $error_allotment = 0;

                $offer_allotment_information_input = array(
                        "esop_id" => '',
                        "company_id" => '',
                        "department_id" => 0,
                        "allotment" => ''
                    );

                foreach ($offer_allotment_info as $i => $update_data)
                {

                    /*validate esop id*/
                    if($i == 'esop_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $offer_allotment_information_input['esop_id'] = $update_data;
                            }
                            else
                            {
                                // $response['message'][] = "Invalid esop id or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Esop id is required.";
                            // $error++; 
                        }
                    }

                    /*validate company id*/
                    if($i == 'company_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $offer_allotment_information_input['company_id'] = $update_data;
                            }
                            else
                            {
                                // $response['message'][] = "Invalid company id or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Offer company id is required.";
                            // $error++; 
                        }
                    }

                    /*validate department id*/
                    if($i == 'department_id')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                            {
                                $offer_allotment_information_input['department_id'] = $update_data;
                            }
                            else
                            {
                                // $response['message'][] = "Invalid department id or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Department id is required.";
                            // $error++; 
                        }
                    }

                    /*validate allotment*/
                    if($i == 'allotment')
                    {
                        if(isset($update_data) AND strlen(trim($update_data)) > 0)
                        {
                            if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9\,\.]+$/", $update_data))
                            {
                                $offer_allotment_information_input['allotment'] = str_replace(",", "", $update_data);
                            }
                            else
                            {
                                // $response['message'][] = "Invalid allotment or is too short";
                                // $error++;
                            }
                        }
                        else
                        {
                            // $response['message'][] = "Allotment is required.";
                            // $error++; 
                        }
                    }
                }

                if($error_allotment == 0)
                {
                    $offer_allotment_information_id = $this->Esop_model->insert_offer_allotment_information($offer_allotment_information_input);
                   
                    if($offer_allotment_information_id > 0)
                    {
                        $response['message'][] = "Offer Allotment edited successfully.";
                        $response['status'] = TRUE;
                        $response_input = $offer_allotment_information_input;
                        $response_input['id'] = $offer_allotment_information_id;
                        $response['allotment_data'][] = $response_input;
                    }
                }
            }

            if($error == 0 && $error_allotment ==  0)
            {
                /*for logs*/
                $_POST['params']['user_offer_data'] = $user_offer_data;
                $_POST['params']['esop_name'] = $params['esop_name'];
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['status'] = TRUE; 

            }
        }

        echo json_encode($response);
    }

    public function get_time () {
        echo json_encode(date('Y-m-d H:i:s'));
    }

    public function get_user_stock_offer ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'uso.id',
                    'esop_id' => 'uso.esop_id',
                    'user_id' => 'uso.user_id',
                    'stock_amount' => 'uso.stock_amount',
                    'date_sent' => 'uso.date_sent',
                    'status' => 'uso.status',
                    'offer_letter_id' => 'uso.offer_letter_id',
                    'acceptance_letter_id' => 'uso.acceptance_letter_id',
                    'esop_name' => 'e.name',
                    'esop_vesting_years' => 'e.vesting_years',
                    'esop_price_per_share' => 'e.price_per_share'
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $total_rows = 0;

            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "uso.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            



            $response['qualifiers'] = $qualifiers;
            $user_stock_offers = $this->Esop_model->get_user_stock_offer_data($qualifiers);
            $total_rows = $user_stock_offers['total_rows'];
            unset($user_stock_offers['total_rows']);



            $this->load->helper('parse_letter_helper');
            
            if(count($user_stock_offers) > 0)
            {
                foreach ($user_stock_offers as $i => $user_stock_offer) {
                    $get_url = "";
                    $get_url = $_SERVER['SCRIPT_NAME'];
                    $get_explode = explode("/",$get_url);

                    $get_url = "http://" . $_SERVER['HTTP_HOST'] .'/'. $get_explode[1];
            
                    if($user_stock_offer["esop_created_company_logo"] == "" ){
                        
                        $user_stock_offer["esop_created_company_logo"] = "/assets/images/roxas-holdings-logo.png";

                    }
                    // kprint($user_stock_offers); exit;
                           

                    $parsed_offer_letter = parse_letter($user_stock_offer['offer_letter_content'], $user_stock_offer, $get_url);
                    $user_stock_offers[$i]['offer_letter_content'] = $parsed_offer_letter;

                    $parsed_acceptance_letter = parse_letter($user_stock_offer['acceptance_letter_content'], $user_stock_offer, $get_url);
                    $user_stock_offers[$i]['acceptance_letter_content'] = $parsed_acceptance_letter;
                }

                $response['total_rows'] = $total_rows;
                $response['data'] = $user_stock_offers;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function accept_offer ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $error = 0;

        $accept_offer_information = $this->input->post();

        if(isset($accept_offer_information) && count($accept_offer_information) > 0
            && isset($accept_offer_information['user_name'])
            && isset($accept_offer_information['esop_log_id']))
        {
            /*validate esop id*/
            if(isset($accept_offer_information['esop_id']) AND strlen(trim($accept_offer_information['esop_id'])) > 0)
            {
                if(strlen(trim($accept_offer_information['esop_id'])) > 0 AND preg_match("/^[0-9]+$/", $accept_offer_information['esop_id']))
                {
                    $accept_offer_information_input['esop_id'] = $accept_offer_information['esop_id'];
                }
                else
                {
                    $response['message'][] = "Invalid esop id or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Esop id is required.";
                $error++; 
            }

            /*validate user id*/
            if(isset($accept_offer_information['user_id']) AND strlen(trim($accept_offer_information['user_id'])) > 0)
            {
                if(strlen(trim($accept_offer_information['user_id'])) > 0 AND preg_match("/^[0-9]+$/", $accept_offer_information['user_id']))
                {
                    $accept_offer_information_input['user_id'] = $accept_offer_information['user_id'];
                }
                else
                {
                    $response['message'][] = "Invalid user id or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "User id is required.";
                $error++; 
            }

            /*validate stock*/
            if(isset($accept_offer_information['stock']) AND strlen(trim($accept_offer_information['stock'])) > 0)
            {
                if(strlen(trim($accept_offer_information['stock'])) > 0 AND preg_match("/^[0-9\,\.]+$/", $accept_offer_information['stock']))
                {
                    $accept_offer_information_input['stock'] = str_replace(",", "", $accept_offer_information['stock']);
                }
                else
                {
                    $response['message'][] = "Invalid stock or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Stock is required.";
                $error++; 
            }

            $accept_offer_information_input['date_accepted'] = date('Y-m-d H:i:s');
            $accept_offer_information_input['status'] = 0;

            if($error == 0)
            {
                /*insert if all inputs are okay*/
                $accept_offer_information_id = $this->Esop_model->insert_user_esop_information($accept_offer_information_input);


                if($accept_offer_information_id > 0)
                {
                    $update_stock_offer_status_qualifiers = array(
                        array(
                            "field" => "uso.user_id", 
                            "operator" => "=", 
                            "value" => $accept_offer_information_input['user_id']
                        ),
                        array(
                            "field" => "uso.esop_id", 
                            "operator" => "=", 
                            "value" => $accept_offer_information_input['esop_id']
                        )
                    );
                    $update_stock_offer_status_data = array(
                        "status" => 2
                    );
                    $update_stock_offer_status = $this->Esop_model->update_distribution_share_information($update_stock_offer_status_qualifiers, $update_stock_offer_status_data);

                    $response['message'][] = "User esop added successfully.";
                    $response['status'] = TRUE;
                    $response['data'] = $accept_offer_information_input;
                    $response['data']['id'] = $accept_offer_information_id;

                    /*for logs*/
                    $_POST['params']['id'] = $accept_offer_information['esop_log_id'];

                    $esop_name = $this->Esop_model->get_esop_name($accept_offer_information['esop_log_id']);
                    
                    if(count($esop_name) > 0)
                    {
                        $_POST['params']['esop_name'] = $esop_name[0]['name'];
                    }

                    $_POST['params']['stocks_accepted'] = $accept_offer_information['stock'];
                    $_POST['params']['name'] = $accept_offer_information['user_name'];
                    $_POST['status'] = TRUE; 

                    $add_vesting_rights_data = $response['data'];
                    $this->add_vesting_rights($add_vesting_rights_data);
                }
            }
        }

        echo json_encode($response);
    }
    public function accept_multiple_offer()
    {

         $response = array(
            "status" => 'fail',
            "message" => array(),
            "data" => array(),
            "data2" => array()
        );
       
        $error = 0;
        $array_data = array();
        if($_POST)
        {
            $array_data = [];

            $_POST['data']  = json_decode($_POST['data'] ,true);
            
            foreach ($_POST['data'] as $key => $value) {
               
                $accept_offer_information = $value;
                $accept_offer_information['user_name']  = $this->session->userdata('user_name');
                /*validate esop id*/
                if(isset($accept_offer_information['esop_id']) AND strlen(trim($accept_offer_information['esop_id'])) > 0)
                {
                    if(strlen(trim($accept_offer_information['esop_id'])) > 0 AND preg_match("/^[0-9]+$/", $accept_offer_information['esop_id']))
                    {
                        $accept_offer_information_input['esop_id'] = $accept_offer_information['esop_id'];
                    }
                    else
                    {
                        $response['message'][] = "Invalid esop id or is too short";
                        $error++;
                    }
                }
                else
                {
                    $response['message'][] = "Esop id is required.";
                    $error++; 
                }
                $accept_offer_information_input['user_id'] = $accept_offer_information['user_id'];
                $accept_offer_information_input['date_accepted'] = date('Y-m-d H:i:s');
                $accept_offer_information_input['status'] = 0;
                $accept_offer_information_input['stock'] = $accept_offer_information['stock'];

                if($error == 0)
                {
                    /*insert if all inputs are okay*/
                    $accept_offer_information_id = $this->Esop_model->insert_user_esop_information($accept_offer_information_input);

                    $response['data2'][] = array('stock_offer_id'=>$accept_offer_information['stock_offer'],'uso_id'=>$accept_offer_information_id,'stock'=>$accept_offer_information['stock']);

                    if($accept_offer_information_id > 0)
                    {
                        $update_stock_offer_status_qualifiers = array(
                            array(
                                "field" => "uso.user_id", 
                                "operator" => "=", 
                                "value" => $accept_offer_information_input['user_id']
                            ),
                            array(
                                "field" => "uso.esop_id", 
                                "operator" => "=", 
                                "value" => $accept_offer_information_input['esop_id']
                            )
                        );

                        $update_stock_offer_status_data = array(
                            "status" => 2
                        );

                        $update_stock_offer_status = $this->Esop_model->update_distribution_share_information($update_stock_offer_status_qualifiers, $update_stock_offer_status_data);

                        $response['message'][] = "User esop added successfully.";
                        $response['status'] = 'success';
                        $response['data'] = $accept_offer_information_input;
                        $response['data']['id'] = $accept_offer_information_id;

                        /*for logs*/
                        $_POST['params']['id'] = $accept_offer_information['esop_log_id'];

                        $esop_name = $this->Esop_model->get_esop_name($accept_offer_information['esop_log_id']);
                        
                        if(count($esop_name) > 0)
                        {
                            $_POST['params']['esop_name'] = $esop_name[0]['name'];
                        }

                        $array_data[] = array(
                                'esop_id'=> $accept_offer_information['esop_log_id'],
                                'esop_name'=> $esop_name,
                                'user_id' => $accept_offer_information['user_id'],
                                'stocks'  => $accept_offer_information_input['stock'],
                                'empname' => $value['empname'],
                                'datetime'=> date('Y-m-d H:i:s')
                        );

                        $add_vesting_rights_data = $response['data'];

                        $this->add_vesting_rights($add_vesting_rights_data);

                    }
                }
            }

            $_POST['params']['info'] = $array_data;
            $_POST['status'] = TRUE; 

        }

        echo json_encode($response);
    }
    public function add_vesting_rights($data = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        if(count($data) == 0){
            $vesting_rights_information = $this->input->post();
        }
        else
        {
            $vesting_rights_information = $data;
        }

        if(isset($vesting_rights_information) && count($vesting_rights_information) > 0)
        {
            if(isset($vesting_rights_information['esop_id']) && strlen($vesting_rights_information['esop_id']) > 0
                && isset($vesting_rights_information['stock']) && strlen($vesting_rights_information['stock']) > 0
                && isset($vesting_rights_information['id']) && strlen($vesting_rights_information['id']) > 0)
            {
                $qualifiers = array(
                    "where" => array(
                        array(
                            "field" => "e.id", 
                            "value" => $vesting_rights_information['esop_id']
                        )
                    )
                );

                $esops = $this->Esop_model->get_esop_data($qualifiers);
                unset($esops['total_rows']);

                if(count($esops) > 0)
                {
                    $other_years = 0;
                    $final_year = 0;
                    $decimal = 0;

                    foreach ($esops as $key => $esop) {

                        for($i = 1; $i <= 5; $i++)
                        {
                            $vesting_rights_information_input = array();

                            $vesting_rights_information_input['esop_map_id'] = $vesting_rights_information['id'];
                            $vesting_rights_information_input['year'] = date('Y-m-d', strtotime('+'.$i.' year', strtotime($esop['grant_date'])));
                            $vesting_rights_information_input['year_no'] = $i;
                            $vesting_rights_information_input['percentage'] = number_format((100 / 5), 2, '.', '');
                            $vesting_rights_information_input['no_of_shares'] = number_format($vesting_rights_information['stock'] / $esop['vesting_years'], 2, '.', '');
                            $vesting_rights_information_input['value_of_shares'] = number_format((($vesting_rights_information['stock'] / $esop['vesting_years']) * $esop['price_per_share']), 2, '.', '');
                            $vesting_rights_information_input['status'] = 0;

                            $other_years = number_format($vesting_rights_information['stock'] / 5, 2, '.', '');
                            $final_year = number_format($other_years * 4, 2, '.', '');
                            $final_year = number_format($vesting_rights_information['stock'] - $final_year, 2, '.', '');

                            $decimal = explode(".", $other_years);
                            $decimal = '0.' . $decimal[1];
                            $decimal = number_format($decimal * 4, 2, '.', '');

                            $final_year = number_format($final_year + $decimal, 2, '.', '');

                            $decimal = explode('.', $other_years);
                            $other_years = $decimal[0] . '.00';

                            if($i != 5) {
                                $vesting_rights_information_input['no_of_shares'] = $other_years;
                            } else {
                                $vesting_rights_information_input['no_of_shares'] = $final_year;
                            }

                            $vesting_rights_information_id = $this->Esop_model->insert_vesting_rights_information($vesting_rights_information_input);
                            
                            if($vesting_rights_information_id > 0)
                            {
                                $response['message'][] = "Vesting Rights added successfully.";
                                $response['status'] = TRUE;
                                $vesting_rights_information_input['id'] = $vesting_rights_information_id;
                                $response['data'][] = $vesting_rights_information_input;
                            }
                        }
                    }
                }
            }
        }

        if(count($data) == 0){
            echo json_encode($response);
        }
    }

    public function check_offer_expiration()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $check_offer_expiration = $this->input->post();

        if(isset($check_offer_expiration) && count($check_offer_expiration) > 0)
        {
            if(isset($check_offer_expiration['esop_id']) && strlen($check_offer_expiration['esop_id']) > 0)
            {
                $qualifiers = array(
                    "where" => array(
                        array(
                            "field" => "e.id", 
                            "value" => $check_offer_expiration['esop_id']
                        ),
                        array(
                            "field" => "e.offer_expiration",
                            "operator" => '>=',
                            "value" => date('Y-m-d')
                        )
                    ) 
                );

                $esop = $this->Esop_model->get_esop_data($qualifiers);
                $total_rows = $esop['total_rows'];
                unset($esop['total_rows']);

                if(count($esop) > 0)
                {
                    $response['status'] = TRUE;
                    $response['data'] = $esop;
                }
            }
            else
            {
                $response['message'][] = 'Esop ID is required';
            }
        }

        echo json_encode($response);
    }

    public function get_personal_stocks()
    {

        $post = $this->input->post();
        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array()
        );
        if (count($post) > 0) {
            $qualifiers = array();
            $alias = array(
                'user_id' => 'ue.user_id',
                'esop_id' => 'ue.esop_id'
            );

            if (isset($post['where']) && count($post['where']) > 0) {
                foreach ($post['where'] as $i => $where) {
                    $qualifiers['where'][] = array(
                        'field' => $alias[$where['field']],
                        'value' => trim($where['value'])
                    );
                }
                $qualifiers['group_by'] = isset($post['group_by']) ? $alias[$post['group_by']] : '';
                $stocks = $this->Esop_model->get_personal_stocks($qualifiers);

                if (count($stocks) > 0) {
                    foreach ($stocks as $i => $stock) {
                        $vesting_qualifiers = array(
                            'where' => array(
                                array(
                                    'field' => 'esop_map_id',
                                    'operator' => '=',
                                    'value' => $stock['user_esop_id']
                                )
                            )
                        );

                        $vesting_rights = $this->Esop_model->get_vesting_rights($vesting_qualifiers);

                        if (count($vesting_rights) > 0) {
                            $total_value = 0;
                            $total_shares = 0;

                            foreach ($vesting_rights as $i_v => $vesting) {
                                $total_shares += $vesting_rights[$i_v]['no_of_shares'];
                                $total_value += $vesting_rights[$i_v]['value_of_shares'];
                                $vesting_rights[$i_v]['year'] = date('F d, Y', strtotime($vesting_rights[$i_v]['year']));
                                $vesting_rights[$i_v]['percentage'] = abs($vesting_rights[$i_v]['percentage']) . '%';
                                $vesting_rights[$i_v]['no_of_shares'] = number_format($vesting_rights[$i_v]['no_of_shares'], 2);
                                $vesting_rights[$i_v]['value_of_shares'] = $stock['currency'] . ' ' . number_format($vesting_rights[$i_v]['value_of_shares'], 2);

                                $vesting_qualifiers = array(
                                    'where' => array(
                                        array(
                                            'field' => 'vesting_rights_id',
                                            'operator' => '=',
                                            'value' => $vesting['id']
                                        ),
                                        array(
                                            'field' => 'user_id',
                                            'operator' => '=',
                                            'value' => $stock['user_id']
                                        )
                                    )
                                );
                                $payments = $this->Esop_model->get_payments($vesting_qualifiers);
                                $payments = $this->manipulate_payments($payments, $vesting['no_of_shares'], $stock['price_per_share'] , $vesting['id']);
                                $vesting_rights[$i_v]['payments'] = isset($payments['data']) ? $payments['data'] : array();
                                $vesting_rights[$i_v]['payments_totals'] = isset($payments['totals']) ? $payments['totals'] : array();


                                $cur_date = date('Y-m-d');
                                $vesting_rights[$i_v]['vesting_status'] = (strtotime($cur_date) >= strtotime($vesting['year'])) ? "With Vesting Rights" : "Without Vesting Rights";


                                if(isset($vesting['uc_status']) AND $vesting['uc_status'] < 2)
                                {
                                    $vesting_rights[$i_v]['uc_status_notes'] = "Claim in process";
                                }
                                elseif(isset($vesting['uc_status']) AND $vesting['uc_status'] == 2 AND $vesting['uc_completed'] != '0000-00-00')
                                {
                                    $vesting_rights[$i_v]['uc_status_notes'] = "Claim as of ".$vesting['uc_completed'];
                                }
                                
                            }
                                $vesting_rights[] = array(
                                    'total' => true,
                                    'total_value' => $stock['currency'] . ' ' . number_format($total_value, 2),
                                    'total_shares' => number_format($total_shares, 2)
                                );

                            }

                            $stocks[$i]['vesting_rights'] = $vesting_rights;
                        }
                    }


                    $response['data'] = $stocks;
                    $response['status'] = (count($stocks) > 0) ? TRUE : FALSE;
                    echo json_encode($response);
                }
            } else {
                echo json_encode($response);
            }

    }

    public function manipulate_payments($data = array() , $no_of_shares = 0, $price_per_share = 0 , $vesting_id)
    {
        $total_outstanding = 0;
        $total_paid = 0;
        $manipulated = array();
        if(count($data) > 0)
        {

            foreach($data as $i => $d)
            {
                $data[$i]['stocks'] = ($data[$i]['amount_paid'] / $price_per_share);
                $total_paid += $data[$i]['amount_paid'];
                $data[$i]['amount_paid'] =  number_format($data[$i]['amount_paid'], 2);
                $data[$i]['date_of_or'] =  date('F d, Y', strtotime($data[$i]['date_of_or']));
                $data[$i]['date_added'] =  date('F d, Y', strtotime($data[$i]['date_added']));
                // $data[$i]['payment_method'] = $data[$i]['payment_type_id'];
                $no_of_shares -= $data[$i]['stocks'];
                //$data[$i]['stocks'] = 100; /*temp*/


            }

            $total_outstanding = $no_of_shares * $price_per_share;
        }

        $manipulated['data'] = $data;

        $manipulated['totals'] = array(
            'vesting_id' => $vesting_id,
            'total' => true,
            'total_outstanding' => $total_outstanding,
            'total_paid' => $total_paid
        );

        return $manipulated;
    }

    public function download_offer_letter($stock_offer_id = '', $user_id = '')
    {
        if(strlen($stock_offer_id) > 0 && strlen($user_id) > 0)
        {
            $qualifiers = array(
                'where' => array(
                    array(
                        'field' => 'uso.user_id',
                        'operator' => '=',
                        'value' => $user_id,
                    ),
                    array(
                        'field' => 'uso.id',
                        'operator' => '=',
                        'value' => $stock_offer_id,
                    )
                )
            );

            $response['qualifiers'] = $qualifiers;
            $user_stock_offers = $this->Esop_model->get_user_stock_offer_data($qualifiers);
            $total_rows = $user_stock_offers['total_rows'];
            unset($user_stock_offers['total_rows']);


            $this->load->helper('parse_letter_helper');
            
            if(count($user_stock_offers) > 0)
            {
                foreach ($user_stock_offers as $i => $user_stock_offer) {


                    $base_url_this = str_replace('/system/', '', BASEPATH);

                    if($user_stock_offer['esop_created_company_logo'] == "" || $user_stock_offer['esop_created_company_logo'] === null )
                    {
                        $user_stock_offer["esop_created_company_logo"] = "/assets/images/roxas-holdings-logo.png";
                    }                   

                    $parsed_offer_letter = parse_letter($user_stock_offer['offer_letter_content'], $user_stock_offer, $base_url_this);
                }

               
                $this_line_footer = "__________________________________________________________________________________________________________________________________";
                $this_is_footer =  "6/F Cacho-Gonzales Bldg., 101 Aguirre Street, Legaspi Village, Makati City 1229 Philippines";
                $this_is_footer2 =  "Trunk Lines: (632)810 8901 to 06 Fax No.: (632) 8171875";

                include_once APPPATH . 'third_party/dompdf-0.5.1/dompdf_config.inc.php' ;
                // header('Content-Type: text/html; charset=utf-8');

                $dompdf = new DOMPDF();
                $dompdf->load_html($parsed_offer_letter);
                $dompdf->render();

                $canvas = $dompdf->get_canvas();
                $fonts_obj = $dompdf->getFontMetrics();

                $footer = $canvas->open_object();

                // get height and width of page
                $w = $canvas->get_width();
                $h = $canvas->get_height();

                // get font
                $font = $fonts_obj->get_font("helvetica", "normal");
                $txtHeight = $fonts_obj->get_font_height($font, 11);

                // draw a line along the bottom
                $y = $h - 2 * $txtHeight - 16;
                $y0 = $h - 2 * $txtHeight - 18;
                $y1 = $h - 2 * $txtHeight - 5;
                $y2 = $h - 2 * $txtHeight - -5;
                $color = array(0, 0, 0);
                // $canvas->line(16, $y, $w - 16, $y, $color, 1);
                
                // set page number on the left side
                $canvas->page_text(16, $y0, $this_line_footer, $font, 8, $color);
                $canvas->page_text(140, $y1, $this_is_footer, $font, 8, $color);
                $canvas->page_text(190, $y2, $this_is_footer2, $font, 8, $color);
                // set additional text
                // $text = "canvas is awesome";
                // $width = $fonts_obj->get_text_width($text, $font, 8);
                // $canvas->text($w - $width - 16, $y, $text, $font, 8);

                // close the object (stop capture)
                $canvas->close_object();

                // add the object to every page (can also specify
                // "odd" or "even")
                $canvas->add_object($footer, "all");


                $dompdf->stream("offer_letter");
            }

             
        }
    }



    public function download_acceptance_letter($stock_offer_id = '', $user_id = '', $temp_shares='')
    {
        if(strlen($stock_offer_id) > 0 && strlen($user_id) > 0)
        {
            $qualifiers = array(
                'where' => array(
                    array(
                        'field' => 'uso.user_id',
                        'operator' => '=',
                        'value' => $user_id,
                    ),
                    array(
                        'field' => 'uso.id',
                        'operator' => '=',
                        'value' => $stock_offer_id,
                    )
                )
            );

            $ishares="0";
            if($temp_shares == "null" || $temp_shares == "0" || $temp_shares == "")
            {
                $ishares="0";
            }else{
                $ishares = $temp_shares;
            }

            $response['qualifiers'] = $qualifiers;
            $user_stock_offers = $this->Esop_model->get_user_stock_offer_data($qualifiers);
            $total_rows = $user_stock_offers['total_rows'];
            unset($user_stock_offers['total_rows']);

            $this->load->helper('parse_letter_helper');
            
            if(count($user_stock_offers) > 0)
            {
                foreach ($user_stock_offers as $i => $user_stock_offer) {
                    $base_url_this = str_replace('/system/', '', BASEPATH);

                    if($user_stock_offer['esop_created_company_logo'] == "" || $user_stock_offer['esop_created_company_logo'] === null )
                    {
                        $user_stock_offer["esop_created_company_logo"] = "/assets/images/roxas-holdings-logo.png";
                    } 

                    
                    
                    if($user_stock_offer['accepted'] == "" || $user_stock_offer['accepted'] === null)
                    {
                        $user_stock_offer['accepted'] = $ishares;
                    }

                    // kprint($user_stock_offers); exit;

                    $parsed_offer_letter = parse_letter($user_stock_offer['acceptance_letter_content'], $user_stock_offer, $base_url_this);
                }

                 // $parsed_offer_letter.="<div class='footer' style='width: 100%; text-align: center; position: fixed; bottom: 0px; font-size: 12px; border-top: 1px solid #000; padding-top: 5px;'>
                 //                            <div style='text-align: center;'>6/F Cacho-Gonzales Bldg., 101 Aguirre Street, Legaspi Village, Makati City 1229 Philippines</div>
                 //                            <div style='text-align: center;'>Trunk Lines: (632)810 8901 to 06 Fax No.: (632) 8171875</div>
                 //                        </div>";

                $this_line_footer = "__________________________________________________________________________________________________________________________________";
                $this_is_footer =  "6/F Cacho-Gonzales Bldg., 101 Aguirre Street, Legaspi Village, Makati City 1229 Philippines";
                $this_is_footer2 =  "Trunk Lines: (632)810 8901 to 06 Fax No.: (632) 8171875";

                include_once APPPATH . 'third_party/dompdf-0.5.1/dompdf_config.inc.php' ;
                // header('Content-Type: text/html; charset=utf-8');

                $dompdf = new DOMPDF();
                $dompdf->load_html($parsed_offer_letter);
                $dompdf->render();

                $canvas = $dompdf->get_canvas();
                $fonts_obj = $dompdf->getFontMetrics();

                $footer = $canvas->open_object();

                // get height and width of page
                $w = $canvas->get_width();
                $h = $canvas->get_height();

                // get font
                $font = $fonts_obj->get_font("helvetica", "normal");
                $txtHeight = $fonts_obj->get_font_height($font, 11);

                // draw a line along the bottom
                $y = $h - 2 * $txtHeight - 16;
                $y0 = $h - 2 * $txtHeight - 18;
                $y1 = $h - 2 * $txtHeight - 5;
                $y2 = $h - 2 * $txtHeight - -5;
                $color = array(0, 0, 0);
                // $canvas->line(16, $y, $w - 16, $y, $color, 1);
                
                // set page number on the left side
                $canvas->page_text(16, $y0, $this_line_footer, $font, 8, $color);
                $canvas->page_text(140, $y1, $this_is_footer, $font, 8, $color);
                $canvas->page_text(190, $y2, $this_is_footer2, $font, 8, $color);
                // set additional text
                // $text = "canvas is awesome";
                // $width = $fonts_obj->get_text_width($text, $font, 8);
                // $canvas->text($w - $width - 16, $y, $text, $font, 8);

                // close the object (stop capture)
                $canvas->close_object();

                // add the object to every page (can also specify
                // "odd" or "even")
                $canvas->add_object($footer, "all");


                $dompdf->stream("acceptance_letter");
            }
        }
    }

    public function add_vesting_claims() {
        if($this->input->is_ajax_request())
        {
            $response = array(
                'status' => FALSE,
                'data' => array(),
                'message' => array()
            );

            $data = $this->input->post();

            if(isset($data['user_id']) && is_array($data['vesting_rights_id']))
            {
                $claim_data = array(
                    'esop_id' =>$data['esop_id'],
                    'user_id' => $data['user_id'],
                    'date_claimed' => date('Y-m-d H:i:s'),
                    'status' => 0
                );

                $claims_map = array();

                $claim_id = $this->Esop_model->insert_claims($claim_data);

                if($claim_id > 0)
                {
                    foreach($data['vesting_rights_id'] as $i => $vesting_id)
                    {
                        $claims_map[] = array(
                            'claim_id' => $claim_id,
                            'vesting_rights_id' => $vesting_id
                        );
                    }
                }

                $this->Esop_model->insert_claim_map($claims_map);

                if($claim_id > 0)
                {
                    $response['status'] = TRUE;

                    /*for logs*/
                    $_POST['params']['id'] = $data['esop_id'];
                    $_POST['params']['user_id'] = $data['user_id'];
                    $_POST['params']['name'] = $data['user_name'];
                    $_POST['params']['esop_name'] = $data['esop_name'];
                    $_POST['params']['vesting_rights_name'] = $data['vesting_rights_name'];
                    $_POST['status'] = TRUE; 
                }
            }

            echo json_encode($response);
            //$test = 0;
        }
    }

    public function get_submitted_claims() {
        if($this->input->is_ajax_request()) {

            $post = $this->input->post();
            $response = array(
                'status' => FALSE,
                'data' => array(),
                'message' => array()
            );

            $claims_module = $this->load->module('claims/Claims');

            $alias = array(
                'esop_id' => 'ue.esop_id',
                'user_id' => 'ue.user_id'
            );

            if (isset($post['where']) && count($post['where']) > 0) {
                foreach ($post['where'] as $i => $where) {
                    $qualifiers['where'][] = array(
                        'field' => $alias[$where['field']],
                        'value' => trim($where['value'])
                    );
                }

                $qualifiers['group_by'] = 'e.esop_id';
            }

            $claims = $claims_module->get($qualifiers);

            if(count($claims) > 0)
            {
                foreach($claims as $i => $claim)
                {
                    $reassembled_claims = array();

                    if(count($claim['data_vesting_rights']) > 0)
                    {
                        $claim_ids = array_values(array_unique(array_column($claim['data_claim_vesting_rights'] , 'claim_id')));
                        foreach($claim_ids as $i_c => $claim_id)
                        {
                            $s_year_claim = '';
                            $s_year_date = '';

                            $vestings_in_this_claim = array_values(array_filter($claim['data_claim_vesting_rights'] , function($k) use ($claim_id) {
                               return $k['claim_id'] == $claim_id;
                            }));

                            if(count($vestings_in_this_claim) > 0)
                            {
                                $vestings_in_this_claim = array_reverse($vestings_in_this_claim);
                                foreach($vestings_in_this_claim as $i_v => $vesting)
                                {
                                    if($i_v == 0)
                                    {
                                        $s_year_claim = 'Year ' . $vesting['year_no'];
                                        $s_year_date = date('F d, Y' , strtotime($vesting['year']));
                                    }
                                    else{
                                        $s_year_claim .= ' & Year ' . $vesting['year_no'];
                                        $s_year_date = date('F d, Y' , strtotime($vesting['year']));
                                    }
                                }

                                $reassembled_claims[] = array(
                                    'year_no' => $s_year_claim,
                                    'claim_id' => $claim_id,
                                    'date' => $s_year_date
                                 );
                            }
                        }
                    }

                    $claims[$i]['claim_group'] = $reassembled_claims;
                }
            }

            echo json_encode($claims);
        }
    }

    public function upload_share_distribution ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'upload_share_distribution', $data, TRUE ) );
        $this->template->draw();
    }

    public function upload_share_distribution_csv ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $path = './assets/uploads/share_distribution/csv';

        if(!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path,0777,TRUE);
        }
        // else
        // {
        //     $response['message'][] = 'Error uploading file, please try again later.';
        //     echo json_encode($response);
        //     exit();
        // }

        $file_name = uniqid( TRUE );

        if ( ! isset( $_FILES['csv_file']['name'] ) )
        {
            echo json_encode($response);
            exit();
        }

        $file       = pathinfo( $_FILES['csv_file']['name'] );
        $extensions = array( 'csv'/*, 'xls', 'xlsx'*/ );

        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ) )
        {
            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.csv' /*. $file['extension']*/;

            if(move_uploaded_file($_FILES['csv_file']['tmp_name'], './assets/uploads/share_distribution/csv/' . $file_name))
            {
                $data['original_file_name'] = $_FILES['csv_file']['name'];
                $data['file_name'] = $file_name;
                $data['file_path'] = base_url() . '/assets/uploads/share_distribution/csv/' . $file_name;
             
                $csv_file = fopen($data['file_path'], 'r');
                while (!feof($csv_file) ) {
                    $share_distribution_info[] = fgetcsv($csv_file, 1024, ',');
                }
                fclose($csv_file);

                $share_distribution_info = array_filter($share_distribution_info);

                /*check if array has more than 2 columns. the first column / $share_distribution_info[0] is the header, the second column / $share_distribution_info[1] is false if it has no other data*/
                if(count($share_distribution_info) > 1 AND isset($share_distribution_info[1]) AND $share_distribution_info[1] != FALSE)
                {
                    $response['status'] = TRUE;
                    $response['data'] = $data;
                    $response['message'][] = 'File successfully uploaded!';
                }
                else
                {
                    unlink('./assets/uploads/share_distribution/csv/' . $file_name);
                    $response['message'][] = 'File uploaded has no content. Please upload file with content.';
                }
            }
            else
            {
                $response['message'][] = 'Error adding file, please try again later.';
            }
        }
        else
        {
            $response['message'][] = 'Invalid file, please try again.';
        }
        
        echo json_encode($response);
    }

    public function open_csv()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $share_distribution_info = array();

        $file = $this->input->post();

        if(isset($file['file_path']) && strlen($file['file_path']) > 0 && isset($file['esop_id']) && strlen($file['esop_id']) > 0 && $file['esop_id'] > 0)
        {
            $file_path = str_replace(base_url(), '.', $file['file_path']);

            if(file_exists($file_path))
            {
                $csv_file = fopen($file['file_path'], 'r');
                while (!feof($csv_file) ) {
                    $share_distribution_info[] = fgetcsv($csv_file, 1024, ',');
                }
                fclose($csv_file);
        
                $share_distribution_info = array_filter($share_distribution_info);

                if(count($share_distribution_info) > 1 AND isset($share_distribution_info[1]) AND $share_distribution_info[1] != FALSE)
                {
                        $validate = $this->validate_upload_share_distribution($share_distribution_info, $file['esop_id']);

                        if(isset($validate['data']) AND count($validate['data']) > 0)
                        {
                            $response['status'] = TRUE;
                            $response['data'] = $validate['data'];
                        }
                }
                else
                {
                    $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
                }
            }
            else
            {
                $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
            }
        }

        echo json_encode($response);
    }

    public function validate_upload_share_distribution($share_distribution_info = array(), $esop_id = 0)
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $all_inputs_valid = 0;
        $total_alloted_shares = 0;
        $share_distribution_upload_info = array();

        $employee_codes = array();

        $companies_controller = $this->load->module('companies/Companies');
        $companies_get = $companies_controller->Companies_model->get_company_data(array("limit" => 999999));
        unset($companies_get['total_rows']);
        $companies = $companies_get;

        $users_controller = $this->load->module('users/Users');
        $users_get = $users_controller->Users_model->get_user_data(array("limit" => 999999));
        unset($users_get['total_rows']);
        $users = $users_get;

        $esop_get = $this->Esop_model->get_esop_data(array("limit" => 999999, "where" => array(array("field" => 'e.id', "value" => $esop_id))));
        unset($esop_get['total_rows']);
        $esops = $esop_get;

        if(isset($esops) AND count($esops) > 0)
        {
            $esop_total_share_qty = $esops[0]['total_share_qty'];
        }

        $settings_controller = $this->load->module('settings/Settings');
        $letters_get = $settings_controller->Settings_model->get_offer_letter_data(array("limit" => 999999));
        unset($letters_get['total_rows']);
        $letters = $letters_get;

        // $stock_offer_get = $this->Esop_model->get_user_stock_offer_data(array("limit" => 999999, "where" => array(array("field" => 'uso.esop_id', "value" => $esop_id))));
        // unset($stock_offer_get['total_rows']);
        // $stock_offers = $stock_offer_get;

        // echo json_encode($stock_offers);
        // exit;

        $share_distribution_info = array_filter($share_distribution_info);

        if(isset($share_distribution_info) AND count($share_distribution_info) > 0 AND isset($esops) AND count($esops) > 0)
        {
            foreach ($share_distribution_info as $key => $share_info) {

                if($key > 0)
                {
                    $error = 0;
                    $share_information_input = array(
                            "company_code" => '',
                            "company_id" => '',
                            "employee_code" => '',
                            "employee_name" => '',
                            "employee_id" => '',
                            "alloted_shares" => '',
                            "offer_letter_id" => '',
                            "acceptance_letter_id" => '',
                            "valid" => 0,
                            "error_message" => array()
                        );

                    foreach ($share_info as $i => $update_data) {
                        $filtered = '';
                        $match = array();
                        /*validate company id*/
                        if ($i == 0)
                        {
                            $share_information_input['company_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($companies, function($filter_key) use ($update_data) {
                                        return $filter_key['company_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $company_id = $match['id'];
                                        $share_information_input['company_id'] = $company_id;
                                        $share_information_input['company_code'] = $update_data;
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Company code is not existing";
                                        $share_information_input['error_message']['Company Code'] = "Company code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Company code";
                                    $share_information_input['error_message']['Company Code'] = "Invalid Company code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Company code is required";
                                $share_information_input['error_message']['Company Code'] = "Company code is required";
                                $error ++;
                            }
                        }

                        /*validate epmployee code*/
                        if ($i == 1)
                        {
                            $share_information_input['employee_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $filtered = array_filter($users, function($filter_key) use ($update_data) {
                                        return $filter_key['employee_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        if(in_array($update_data, $employee_codes))
                                        {
                                            array_walk($share_distribution_upload_info, function(&$value, $key) use ($update_data) { 
                                                if($value['employee_code'] == $update_data)
                                                {
                                                    $value['error_message']['Employee Code'] = "Employee Code is already existing in the template"; 
                                                }
                                            });

                                            // $response['message'][] = "Employee Code is already existing.";
                                            $share_information_input['error_message']['Employee Code'] = "Employee Code is already existing in the template";
                                            $error++;
                                        }
                                        else
                                        {
                                            foreach ($filtered as $filtered_key => $filtered_value) {
                                                $match = $filtered_value;
                                            }

                                            // $match_id = $match['id'];
                                            // $filtered_stocks = array_filter($stock_offers, function($filter_key) use ($match_id) {
                                            //     return $filter_key['user_id'] === $match_id;
                                            // });

                                            // if(count($filtered_stocks) > 0)
                                            // {
                                            //     // $response['message'][] = "Employee Code already has shares for this ESOP";
                                            //     $share_information_input['error_message']['Employee Code'] = "Employee Code already has shares for this ESOP";
                                            //     $error ++;
                                            // }
                                            // else
                                            // {                                            
                                                if($share_information_input['company_id'] == $match['company_id'])
                                                {
                                                    $user_id = $match['id'];
                                                    $share_information_input['employee_id'] = $user_id;
                                                    array_push($employee_codes, $update_data);
                                                    $share_information_input['employee_code'] = $update_data;
                                                }
                                                else
                                                {
                                                    // $response['message'][] = "Employee Code is does not match company code";
                                                    $share_information_input['error_message']['Employee Code'] = "Employee Code is does not match company code";
                                                    $error ++;
                                                }
                                            // }
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Employee Code is not existing";
                                        $share_information_input['error_message']['Employee Code'] = "Employee Code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Employee code";
                                    $share_information_input['error_message']['Employee Code'] = "Invalid Employee code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Employee code is required";
                                $share_information_input['error_message']['Employee Code'] = "Employee code is required";
                                $error ++;
                            }
                        }

                        /*validate employee name*/
                        if ($i == 2)
                        {
                            $share_information_input['employee_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $employee_code = $share_information_input['employee_code'];
                                    $filtered = array_filter($users, function($filter_key) use ($employee_code) {
                                        return $filter_key['employee_code'] === $employee_code;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $code_first_name = (isset($match['first_name']) AND strlen($match['first_name']) > 0) ? ' '.$match['first_name'] : '' ;
                                        $code_middle_name = (isset($match['middle_name']) AND strlen($match['middle_name']) > 0) ? ' '.$match['middle_name'] : '' ;
                                        $code_last_name = (isset($match['last_name']) AND strlen($match['last_name']) > 0) ? $match['last_name'].',' : '' ;

                                        $code_full_name = $code_last_name.''.$code_first_name.''.$code_middle_name;

                                        if(strtolower($code_full_name) == strtolower($update_data))
                                        {
                                            $share_information_input['employee_name'] = $update_data;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Employee name does not match the employee code";
                                            $share_information_input['error_message']['Employee Name'] = "Employee name does not match the employee code";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Employee name does not match the employee code";
                                        $share_information_input['error_message']['Employee Name'] = "Employee name does not match the employee code";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid employee name";
                                    $share_information_input['error_message']['Employee Name'] = "Invalid employee name";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Employee name is required";
                                $share_information_input['error_message']['Employee Name'] = "Employee name is required";
                                $error ++;
                            }
                        }
                        
                        /*validate alloted shares*/
                        if ($i == 3)
                        {
                            $share_information_input['alloted_shares'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9\.\,]+$/", trim($update_data)))
                                {
                                    $total_alloted_shares = $total_alloted_shares + $update_data;
                                    $share_information_input['alloted_shares'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid alloted shares";
                                    $share_information_input['error_message']['Alloted Shares'] = "Invalid alloted shares";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Alloted shares is required";
                                $share_information_input['error_message']['Alloted Shares'] = "Alloted shares is required";
                                $error ++;
                            }
                        }

                        /*validate offer letter id*/
                        if ($i == 4)
                        {
                            $share_information_input['offer_letter_id'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($letters, function($filter_key) use ($update_data) {
                                        return $filter_key['id'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        if($match['status'] == 1)
                                        {
                                            if($match['type'] == 1)
                                            {
                                                $share_information_input['offer_letter_id'] = $update_data;
                                            }
                                            else
                                            {
                                                // $response['message'][] = "Offer letter ID provided is not an offer letter";
                                                $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID provided is not an offer letter";
                                                $error ++;
                                            }
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Offer letter ID is archived";
                                            $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID is archived";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Offer letter ID is not existing";
                                        $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid offer letter id";
                                    $share_information_input['error_message']['Offer Letter ID'] = "Invalid offer letter id";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Offer letter ID is required";
                                $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID is required";
                                $error ++;
                            }
                        }

                        /*validate acceptance letter id*/
                        if ($i == 5)
                        {
                            $share_information_input['acceptance_letter_id'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($letters, function($filter_key) use ($update_data) {
                                        return $filter_key['id'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        if($match['status'] == 1)
                                        {
                                            if($match['type'] == 2)
                                            {
                                                $share_information_input['acceptance_letter_id'] = $update_data;
                                            }
                                            else
                                            {
                                                // $response['message'][] = "Acceptance letter ID provided is not an acceptance letter";
                                                $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID provided is not an acceptance letter";
                                                $error ++;
                                            }
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Acceptance letter ID is archived";
                                            $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID is archived";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Acceptance letter ID is not existing";
                                        $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid acceptance letter id";
                                    $share_information_input['error_message']['Acceptance Letter ID'] = "Invalid acceptance letter id";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Acceptance letter ID is required";
                                $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID is required";
                                $error ++;
                            }
                        }

                    }
                    
                    if($error == 0)
                    {
                        $share_information_input['valid'] = 1;
                    }
                    else
                    {
                        $all_inputs_valid++;
                    }
                    
                    $share_distribution_upload_info[] = $share_information_input;
                }
            }
            
            if($total_alloted_shares > $esop_total_share_qty)
            {
                array_walk($share_distribution_upload_info, function(&$value, $key) use ($all_inputs_valid){ 
                    $all_inputs_valid++;
                    $value['valid'] = 0;
                    $value['error_message']['Alloted Shares'] = "Shares have exceeded the Total Alloted Shares. Please re-distribute the shares accordingly"; 
                });
            }

            if($all_inputs_valid == 0)
            {
                $response['status'] = TRUE;
            }

            $response['data'] = $share_distribution_upload_info;
        }
        return $response;
    }

    public function insert_upload_share_distribution()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        $user_offer_data = array();

        if(isset($params['employee_data']) && count($params['employee_data']) > 0
            && isset($params['allotment_data']) && count($params['allotment_data']) > 0
            && isset($params['esop_id']) && strlen($params['esop_id']) > 0)
        {
            $delete_user_stock_offer_data = $this->Esop_model->delete_data($params['esop_id'], 'user_stock_offer', 'esop_id');
            $delete_offer_allotment_data = $this->Esop_model->delete_data($params['esop_id'], 'offer_allotment', 'esop_id');

            $error = 0;
            foreach ($params['employee_data'] as $key => $distribution_share_info)
            {
                $distribution_share_information_input = array();
                $distribution_share_information_input['esop_id'] = isset($distribution_share_info['esop_id']) ? $distribution_share_info['esop_id'] : '' ;
                $distribution_share_information_input['user_id'] = isset($distribution_share_info['user_id']) ? $distribution_share_info['user_id'] : '' ;
                $distribution_share_information_input['stock_amount'] = isset($distribution_share_info['stock_amount']) ? $distribution_share_info['stock_amount'] : '' ;
                $distribution_share_information_input['offer_letter_id'] = isset($distribution_share_info['offer_letter_id']) ? $distribution_share_info['offer_letter_id'] : '' ;
                $distribution_share_information_input['acceptance_letter_id'] = isset($distribution_share_info['acceptance_letter_id']) ? $distribution_share_info['acceptance_letter_id'] : '' ;
                $distribution_share_information_input['date_sent'] = date('Y-m-d H:i:s');

                /*insert if all inputs are okay*/
                $upload_distribution_share_information_id = $this->Esop_model->insert_distribution_share_information($distribution_share_information_input);

                if($upload_distribution_share_information_id > 0)
                {
                    $response['message'][] = "Distribution shares added successfully.";
                    $response['status'] = TRUE;
                    $response_input = $distribution_share_information_input;
                    $response_input['id'] = $upload_distribution_share_information_id;
                    $response['data'][] = $response_input;
                    $user_offer_data[] = $response_input;
                }
            }

            $error_allotment = 0;
            foreach ($params['allotment_data'] as $key => $offer_allotment_info)
            {
                $offer_allotment_information_input = array();
                $offer_allotment_information_input['esop_id'] = isset($offer_allotment_info['esop_id']) ? $offer_allotment_info['esop_id'] : '' ;
                $offer_allotment_information_input['company_id'] = isset($offer_allotment_info['company_id']) ? $offer_allotment_info['company_id'] : '' ;
                $offer_allotment_information_input['department_id'] = 0;
                $offer_allotment_information_input['allotment'] = isset($offer_allotment_info['allotment']) ? $offer_allotment_info['allotment'] : '' ;

                $offer_allotment_information_id = $this->Esop_model->insert_offer_allotment_information($offer_allotment_information_input);
               
                if($offer_allotment_information_id > 0)
                {
                    $response['message'][] = "Offer Allotment added successfully.";
                    $response['status'] = TRUE;
                    $response_input = $offer_allotment_information_input;
                    $response_input['id'] = $offer_allotment_information_id;
                    $response['allotment_data'][] = $response_input;
                }
            }
            if($error == 0 && $error_allotment ==  0)
            {
                /*for logs*/
                $_POST['params']['user_offer_data'] = $user_offer_data; 
                $_POST['params']['esop_name'] = $params['esop_name'];
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['status'] = TRUE; 
                
            }
        }

        echo json_encode($response);
    }

    public function download_share_distribution_template()
    {
        $file = './assets/templates/share_distribution_template.csv';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function update_stock_offer_status()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $params = $this->input->post();

        if(isset($params['esop_id']) AND strlen($params['esop_id']) AND $params['esop_id'] > 0)
        {
            $update_stock_offer_status_qualifiers = array(
                array(
                    "field" => "uso.esop_id", 
                    "operator" => "=", 
                    "value" => $params['esop_id']
                )
            );
            $update_stock_offer_status_data = array(
                "status" => 1,
                "date_sent" => date('Y-m-d H:i:s')
            );
            $update_stock_offer_status = $this->Esop_model->update_distribution_share_information($update_stock_offer_status_qualifiers, $update_stock_offer_status_data);

            if($update_stock_offer_status > 0)
            {
                $response['message'] = "Stock offer status updated successfully.";
                $response['status'] = TRUE;
                $response['data']['id'] = $params['esop_id'];
                $response['data']['status'] = $update_stock_offer_status;

                /*for logs*/
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['params']['esop_name'] = $params['esop_name'];
                $_POST['status'] = TRUE; 
            }
        }

        echo json_encode($response);
    }

    public function view_employee_by_esop_batch(){
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'view_employee_by_esop_batch', $data, TRUE ) );
        $this->template->draw();
    }

    public function add_payment_record ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();
           
            if(isset($params['payment_type_id']) AND strlen(trim($params['payment_type_id'])) > 0)
            {
                if(isset($params['currency']) AND strlen(trim($params['currency'])) > 0)
                {
                    if(isset($params['amount_paid']) AND strlen(trim($params['amount_paid'])) > 1)
                    {

                    }
                    else
                    {
                        $response['message'][] = "Amount paid is required.";
                        $error++;
                    }
                }
                else
                {
                    $response['message'][] = "Currency is required.";
                    $error++;
                }
            }

            else
            {
                $response['message'][] = "Payment type is required.";
                $error++; 
            }
            if(isset($params['dividend_date']) AND strlen(trim($params['dividend_date'])) == 0 )
            {
                $response['message'][] = "Dividend date is required.";
                $error++; 
            }
            if(strtotime($params['date_granted']) > strtotime($params['dividend_date']))
            {
                $response['message'][] = "Dividend date must be greater than ESOP grant date.";
                $error++; 
            }

            if($error == 0)
            {
                /*insert if all inputs are okay*/
                $dividend = strtotime($params['dividend_date']);
                $converted_dividend = date('Y-m-d',$dividend);
                $params['date_of_or'] = date('Y-m-d H:i:s');
                $params['date_added'] = date('Y-m-d H:i:s');
                $esop_id = $params['esop_id'];
                $user_name = $params['user_name'];
                $esop_name = $params['esop_name'];
                $currency_name = $params['currency_name'];
                $payment_type = $params['payment_type'];
                $vesting_rights = $params['vesting_rights'];
                $dividend_date = $converted_dividend;
                $params['dividend_date'] = $converted_dividend;
                // $payments = $vesting_rights[0]['payments'];
                $amount_paid = $params['amount_paid'];
                $vesting_rights_count = 0;
                // if(isset($params['esop_id'])) { unset($params['esop_id']); }
                if(isset($params['user_name'])) { unset($params['user_name']); }
                if(isset($params['esop_name'])) { unset($params['esop_name']); }
                if(isset($params['currency_name'])) { unset($params['currency_name']); }
                if(isset($params['payment_type'])) { unset($params['payment_type']); }
                if(isset($params['date_granted'])){unset($params['date_granted']);}

                unset($params['vesting_rights']);
                $vesting_rights_count = count($vesting_rights);
                $vesting_rights_limit = count($vesting_rights) - 2;

                $inserted_count = 0;
                $inserted_id = 0;

/*                $vesting_rights_temp = $vesting_rights;
                foreach ($vesting_rights_temp as $key => $value) {
                    if(!array_key_exists("value_of_shares", $value)){
                        unset($vesting_rights_temp[$key]);
                    }
                }

                foreach ($vesting_rights_temp as $key => $row) {
                    $value_of_shares = $row['value_of_shares'];
                    $value_of_shares = str_replace(',', '', $value_of_shares);
                    $value_of_shares = str_replace('PHP', '', $value_of_shares);
                    $value_of_shares = floatval($value_of_shares);
                    $payment_total_per_vesting = 0;

                    if($amount_paid > 0) {

                        $payment_details = $this->get_payment_details($row['id']);

                        if($payment_details !== false){
                            foreach ($payment_details as $pd) {
                                $payment_total_per_vesting = $payment_total_per_vesting + floatval($pd["amount_paid"]);
                            }
                        }
                        

                        if($payment_total_per_vesting >= $value_of_shares){
                            //move to next vesting years
                            continue;
                        }else{

                            $left = $value_of_shares - $payment_total_per_vesting;
                            if($left > 0){
                                if($left <= $amount_paid){
                                    $deducted_from_amount_paid = $amount_paid - $left;
                                    $amount_to_save = $left;

                                    $params['vesting_rights_id'] = $row['id'];
                                    $params['amount_paid'] = $amount_to_save;
                                    
                                    $insert_id = $this->Esop_model->insert_user_payment($params);

                                    $amount_paid = $deducted_from_amount_paid;
                                    continue;
                                }else{
                                    $params['vesting_rights_id'] = $row['id'];
                                    $params['amount_paid'] = $amount_paid;
                                    
                                    $insert_id = $this->Esop_model->insert_user_payment($params);
                                    break;
                                }
                            }

                        }
                    }

                }*/
                $insert_id = $this->Esop_model->insert_user_payment($params);
     
                if($insert_id > 0)
                {

                    $response['message'][] = "Payment Successfully Added.";
                    $response['status'] = TRUE;
                    $response['data'] = $params;
                    $response['data']['id'] = $insert_id;

                    /*for logs*/
                    $_POST['params']['id'] = $esop_id;
                    $_POST['params']['user_id'] = $params['user_id'];
                    $_POST['params']['name'] = $user_name;
                    $_POST['params']['esop_name'] = $esop_name;
                    $_POST['params']['payment_type'] = $payment_type;
                    $_POST['params']['currency_name'] = $currency_name;
                    $_POST['params']['amount_paid'] = $params['amount_paid'];

                    $_POST['status'] = TRUE;
                }
                
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function get_payment_details($vesting_rights_id) {
        $payment = $this->Esop_model->get_payment_details($vesting_rights_id);

        if(count($payment) > 0) {
            return $payment;
        } else {
            return false;
        }
    }

    public function get_payment_method()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $qualifiers = array(
            'where' => isset($params['where']) ? $params['where'] : array() ,
            'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
            'search_field' => isset($search_field) ? $search_field : '' ,
            'offset' => isset($params['offset']) ? $params['offset'] : 0,
            'limit' => isset($params['limit']) ? $params['limit'] : 30,
            'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "pm.id",
            'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
        );

        $qualifiers['where'] = array(
                    array(
                    'field' => 'pm.status',
                    'value' => '1'
                )
            );

        $payment_method = $this->Esop_model->get_payment_methods_data($qualifiers);
        unset($payment_method['total_rows']);
        if(count($payment_method) > 0)
        {
            $response['status'] = TRUE;
            $response['data'] = $payment_method;
        }

        echo json_encode($response);
    }

    public function insert_gratuity ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['price_per_share']) AND strlen(trim($params['price_per_share'])) > 0)
            {

            }
            else
            {
                $response['message'][] = "Price Per Share required.";
                $error++; 
            }

            if($error == 0)
            {
                /*insert if all inputs are okay*/
                $grant_date = date('Y-m-d');
                $dividenddate = date('Y-m-d' , strtotime($params['dividend_date']));
                $insert_data = array(
                    'esop_id' => $params['esop_id'],
                    'grant_date' => $grant_date,
                    'dividend_date' => $dividenddate,
                    'currency_id' => $params['currency_id'],
                    'price_per_share' => $params['price_per_share'],
                    'total_value_of_gratuity_given' => $params['total_value_of_gratuity_given'],
                    'status' => $params['status'],
                    'reject_reason' => '',
                    'price' => 0
                );

                $this->Esop_model->insert_gratuity($insert_data);
  
                $response['message'][] = "Gratuity Successfully Added.";
                $response['status'] = TRUE;
                $response['data'] = $params;

                /*for logs*/
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['params']['price_per_share'] = $params['price_per_share'];
                $_POST['params']['currency_name'] = $params['currency_name'];
                $_POST['params']['esop_name'] = $params['esop_name'];
                $_POST['status'] = TRUE;
            }

            //$response['data']= $params;

            echo json_encode($response);
        }
    }

    public function get_all_gratuity()
    {
        $esop_id = $this->input->post("esop_id");

        $gratuity =  $this->Esop_model->get_all_gratuity($esop_id);

        foreach ($gratuity as $key => $value) {
            $this_date = strtotime($value["grant_date"]);
            $divident_date = strtotime($value["dividend_date"]);
            $gratuity[$key]["grant_date"] = date("F d, Y", $this_date);
            $gratuity[$key]["dividend_date_formatted"] = date("F d, Y", $divident_date);
            
        }

         echo json_encode($gratuity);

    }

    public function get_gratuity ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'g.id',
                    'grant_date' => 'g.name',
                    'esop_id' => 'g.esop_id'
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $total_rows = 0;

            $claims= array();
            //$claims['data_vesting_rights'] = array();
            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "g.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;


            $gratuities = $this->Esop_model->get_gratuity($qualifiers);
            $total_rows = $gratuities['total_rows'];
            unset($gratuities['total_rows']);

            if(count($gratuities) > 0)
            {
                $response['total_rows'] = $total_rows;
                $response['data'] = $gratuities;
                $response['status'] = TRUE;
            }
        }


        if (count($get_param) == 0)
        {
            echo json_encode($response);
        } else {
            return($response['data']);
        }




    }

    public function approve_gratuity ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['id']) AND strlen(trim($params['id'])) > 0)
            {

            }
            else
            {
                $response['message'][] = "ID required.";
                $error++; 
            }

            if($error == 0)
            {
                /*update if all inputs are okay*/
                $grant_date = date('Y-m-d');
                $id = $params['id'];
                $data = array(
                    'grant_date' => $grant_date,
                    'status' => $params['status']
                );

                $this->Esop_model->update_gratuity($id,$data);
  
                $response['message'][] = "Gratuity Successfully Approved.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                
                /*for logs*/
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['params']['price_per_share'] = $params['price_per_share'];
                $_POST['params']['currency_name'] = $params['currency_name'];

                // for email notification in logs
                $_POST['params']['esop_name'] = $params['esop_name'];
                // end for email notification in logs

                $_POST['status'] = TRUE;
            }

            //$response['data']= $params;

            echo json_encode($response);
        }
    }

    public function reject_gratuity ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['id']) AND strlen(trim($params['id'])) > 0)
            {
                if(isset($params['reject_reason']) AND strlen(trim($params['reject_reason'])) > 0)
                {

                }
                else
                {
                    $response['message'][] = "Reject Reason required.";
                    $error++; 
                }
            }
            else
            {
                $response['message'][] = "ID required.";
                $error++; 
            }

            if($error == 0)
            {
                /*update if all inputs are okay*/
                $grant_date = date('Y-m-d');
                $id = $params['id'];
                $reject_reason = $params['reject_reason'];
                $data = array(
                    'reject_reason' => $reject_reason,
                    'grant_date' => $grant_date,
                    'status' => $params['status']
                );

                $this->Esop_model->update_gratuity($id,$data);
  
                $response['message'][] = "Gratuity Successfully Rejected.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                
                /*for logs*/
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['params']['price_per_share'] = $params['price_per_share'];
                $_POST['params']['currency_name'] = $params['currency_name'];
                $_POST['status'] = TRUE;
            }

            //$response['data']= $params;

            echo json_encode($response);
        }
    }
  
    public function send_gratuity_to_employees ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();
            $user_esop_data = array();

            if(isset($params['id']) AND strlen(trim($params['id'])) > 0)
            {
                if(isset($params['esop_id']) AND strlen(trim($params['esop_id'])) > 0)
                {
                    if(isset($params['price_per_share']) AND strlen(trim($params['price_per_share'])) > 0)
                    {
                        if(isset($params['vesting_years']) AND strlen(trim($params['vesting_years'])) > 0)
                        {

                        }
                        else
                        {
                            $response['message'][] = "Vesting years required.";
                            $error++; 
                        }
                    }
                    else
                    {
                        $response['message'][] = "Price per share required.";
                        $error++; 
                    }
                }
                else
                {
                    $response['message'][] = "Esop ID required.";
                    $error++; 
                }
            }
            else
            {
                $response['message'][] = "ID required.";
                $error++; 
            }

            if($error == 0)
            {
                
                $esop_id = $params['esop_id'];
                $price_per_share = $params['price_per_share'];
                $vesting_years = $params['vesting_years'];

                $data_user_esop = $this->Esop_model->get_user_esop($esop_id);
                if(count($data_user_esop) > 0)
                {
                    foreach ($data_user_esop as $key => $val) {
                        $vesting_rights_id = $val['id'];
                        $user_id = $val['user_id'];
                        $data_user_active_vesting_rights = $this->Esop_model->get_user_active_vesting_rights($vesting_rights_id);
                        //$data_user_esop[$key]['data_user_active_vesting_rights'] = $data_user_active_vesting_rights;

                        //get only active years of vesting rights
                        $vesting_years = count($data_user_active_vesting_rights);
                        $stock = $val['stock'];
                        //computation for user payment
                        $amount_paid = ($stock * $price_per_share) / $vesting_years;

                        $payment_type_id = 2;
                        $placed_by = $this->session->userdata('user_id');
                        $currency = $params['currency'];
                        $date_of_or = date('Y-m-d');
                        $date_added = date('Y-m-d H:i:s');

                        $user_payment_ids = '';
                        foreach ($data_user_active_vesting_rights as $key2 => $val2) {
                            $insert_user_payment_data = array(
                                'user_id' => $user_id,
                                'payment_type_id' => $payment_type_id,
                                'amount_paid' => $amount_paid,
                                'currency' => $currency,
                                'placed_by' => $placed_by,
                                'date_of_or' => $date_of_or,
                                'date_added' => $date_added,
                                'vesting_rights_id' => $val2['id'],
                                'gratuity_id' => $params['id']
                                );
                            
                            $user_payment_id = $this->Esop_model->insert_user_payment($insert_user_payment_data);
                            $user_payment_ids .= $user_payment_id.', ';
                        }

                        //$data_user_esop[$key]['data_user_payment_ids'] = $user_payment_ids;
                        //$data_user_esop[$key]['data_amount_paid'] = $amount_paid;
                        $user_data = array();
                        $user_data = $val;
                        $user_data['gratuity_per_share'] = $price_per_share;
                        $user_esop_data[] = $user_data;
                    }
                }

                $grant_date = date('Y-m-d');
                $id = $params['id'];
                $data = array(
                    'grant_date' => $grant_date,
                    'status' => $params['status']
                );
                //update gratuity status
                $this->Esop_model->update_gratuity($id,$data);


                $response['message'][] = "Gratuity Successfully Sent to Employees.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                //$response['data_test'] = $data_user_esop;
                
                /*for logs*/
                $_POST['params']['id'] = $params['esop_id'];
                $_POST['params']['price_per_share'] = $params['price_per_share'];
                $_POST['params']['currency_name'] = $params['currency_name'];
                $_POST['params']['user_esop_data'] = $user_esop_data;
                $_POST['params']['esop_name'] = $params['esop_name'];
                $_POST['status'] = TRUE;
            }

            //$response['data']= $params;

            echo json_encode($response);
        }
    }

    public function batch_upload_share_distribution ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'batch_upload_share_distribution', $data, TRUE ) );
        $this->template->draw();
    }

    public function open_batch_csv()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $share_distribution_info = array();

        $file = $this->input->post();

        if(isset($file['file_path']) && strlen($file['file_path']) > 0 && isset($file['esop_id']) && strlen($file['esop_id']) > 0 && $file['esop_id'] > 0
            && isset($file['esop_batch_parent_id']) && strlen($file['esop_batch_parent_id']) > 0)
        {
            $file_path = str_replace(base_url(), '.', $file['file_path']);

            if(file_exists($file_path))
            {
                $csv_file = fopen($file['file_path'], 'r');
                while (!feof($csv_file) ) {
                    $share_distribution_info[] = fgetcsv($csv_file, 1024, ',');
                }
                fclose($csv_file);
        
                $share_distribution_info = array_filter($share_distribution_info);

                if(count($share_distribution_info) > 1 AND isset($share_distribution_info[1]) AND $share_distribution_info[1] != FALSE)
                {
                        $validate = $this->validate_batch_upload_share_distribution($share_distribution_info, $file['esop_id'], $file['esop_batch_parent_id']);

                        if(isset($validate['data']) AND count($validate['data']) > 0)
                        {
                            $response['status'] = TRUE;
                            $response['data'] = $validate['data'];
                        }
                }
                else
                {
                    $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
                }
            }
            else
            {
                $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
            }
        }

        echo json_encode($response);
    }

    public function validate_batch_upload_share_distribution($share_distribution_info = array(), $esop_id = 0, $esop_batch_parent_id = '')
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $all_inputs_valid = 0;
        $total_alloted_shares = 0;
        $share_distribution_upload_info = array();

        $employee_codes = array();

        $companies_controller = $this->load->module('companies/Companies');
        $companies_get = $companies_controller->Companies_model->get_company_data(array("limit" => 999999));
        unset($companies_get['total_rows']);
        $companies = $companies_get;

        $users_controller = $this->load->module('users/Users');
        $users_get = $users_controller->Users_model->get_user_data(array("limit" => 999999));
        unset($users_get['total_rows']);
        $users = $users_get;

        $esop_get = $this->Esop_model->get_esop_data(array("limit" => 999999, "where" => array(array("field" => 'e.id', "value" => $esop_id))));
        unset($esop_get['total_rows']);
        $esops = $esop_get;

        if(isset($esops) AND count($esops) > 0)
        {
            $esop_total_share_qty = $esops[0]['total_share_qty'];
        }

        $settings_controller = $this->load->module('settings/Settings');
        $letters_get = $settings_controller->Settings_model->get_offer_letter_data(array("limit" => 999999));
        unset($letters_get['total_rows']);
        $letters = $letters_get;

        $esop_ids = explode(',', $esop_batch_parent_id);

        $filtered_esop_parent_ids = array_filter($esop_ids, function($filter_key) use ($esop_id) {
            return $filter_key !== $esop_id;
        });

        $filtered_esop_parent_ids = join(',', $filtered_esop_parent_ids);

        // $stock_offer_get = $this->Esop_model->get_user_stock_offer_data(array("limit" => 999999));
        $stock_offer_get = $this->Esop_model->get_user_stock_offer_data(array("limit" => 999999, "where" => array(array("field" => 'uso.esop_id', "operator" => 'IN', "value" => '('.$filtered_esop_parent_ids.')' ))));
        unset($stock_offer_get['total_rows']);
        $stock_offers = $stock_offer_get;

        // echo json_encode($stock_offers);
        // exit;

        $share_distribution_info = array_filter($share_distribution_info);

        if(isset($share_distribution_info) AND count($share_distribution_info) > 0 AND isset($esops) AND count($esops) > 0)
        {
            foreach ($share_distribution_info as $key => $share_info) {

                if($key > 0)
                {
                    $error = 0;
                    $share_information_input = array(
                            "company_code" => '',
                            "company_id" => '',
                            "employee_code" => '',
                            "employee_name" => '',
                            "employee_id" => '',
                            "alloted_shares" => '',
                            "offer_letter_id" => '',
                            "acceptance_letter_id" => '',
                            "valid" => 0,
                            "error_message" => array()
                        );

                    foreach ($share_info as $i => $update_data) {
                        $filtered = '';
                        $match = array();
                        /*validate company id*/
                        if ($i == 0)
                        {
                            $share_information_input['company_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($companies, function($filter_key) use ($update_data) {
                                        return $filter_key['company_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $company_id = $match['id'];
                                        $share_information_input['company_id'] = $company_id;
                                        $share_information_input['company_code'] = $update_data;
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Company code is not existing";
                                        $share_information_input['error_message']['Company Code'] = "Company code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Company code";
                                    $share_information_input['error_message']['Company Code'] = "Invalid Company code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Company code is required";
                                $share_information_input['error_message']['Company Code'] = "Company code is required";
                                $error ++;
                            }
                        }

                        /*validate epmployee code*/
                        if ($i == 1)
                        {
                            $share_information_input['employee_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $filtered = array_filter($users, function($filter_key) use ($update_data) {
                                        return $filter_key['employee_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        if(in_array($update_data, $employee_codes))
                                        {
                                            array_walk($share_distribution_upload_info, function(&$value, $key) use ($update_data) { 
                                                if($value['employee_code'] == $update_data)
                                                {
                                                    $value['error_message']['Employee Code'] = "Employee Code is already existing in the template"; 
                                                }
                                            });

                                            // $response['message'][] = "Employee Code is already existing.";
                                            $share_information_input['error_message']['Employee Code'] = "Employee Code is already existing in the template";
                                            $error++;
                                        }
                                        else
                                        {
                                            foreach ($filtered as $filtered_key => $filtered_value) {
                                                $match = $filtered_value;
                                            }

                                            $match_id = $match['id'];
                                            $filtered_stocks = array_filter($stock_offers, function($filter_key) use ($match_id) {
                                                return $filter_key['user_id'] === $match_id && $filter_key['accepted'] !== null;
                                            });

                                            if(count($filtered_stocks) > 0)
                                            {
                                                // $response['message'][] = "Employee Code already has shares from the ESOP batches";
                                                $share_information_input['error_message']['Employee Code'] = "Employee Code already has shares from the ESOP batches";
                                                $error ++;
                                            }
                                            else
                                            {
                                                if($share_information_input['company_id'] == $match['company_id'])
                                                {
                                                    $user_id = $match['id'];
                                                    $share_information_input['employee_id'] = $user_id;
                                                    array_push($employee_codes, $update_data);
                                                    $share_information_input['employee_code'] = $update_data;
                                                }
                                                else
                                                {
                                                    // $response['message'][] = "Employee Code is does not match company code";
                                                    $share_information_input['error_message']['Employee Code'] = "Employee Code is does not match company code";
                                                    $error ++;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Employee Code is not existing";
                                        $share_information_input['error_message']['Employee Code'] = "Employee Code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Employee code";
                                    $share_information_input['error_message']['Employee Code'] = "Invalid Employee code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Employee code is required";
                                $share_information_input['error_message']['Employee Code'] = "Employee code is required";
                                $error ++;
                            }
                        }

                        /*validate employee name*/
                        if ($i == 2)
                        {
                            $share_information_input['employee_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $employee_code = $share_information_input['employee_code'];
                                    $filtered = array_filter($users, function($filter_key) use ($employee_code) {
                                        return $filter_key['employee_code'] === $employee_code;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $code_first_name = (isset($match['first_name']) AND strlen($match['first_name']) > 0) ? ' '.$match['first_name'] : '' ;
                                        $code_middle_name = (isset($match['middle_name']) AND strlen($match['middle_name']) > 0) ? ' '.$match['middle_name'] : '' ;
                                        $code_last_name = (isset($match['last_name']) AND strlen($match['last_name']) > 0) ? $match['last_name'].',' : '' ;

                                        $code_full_name = $code_last_name.''.$code_first_name.''.$code_middle_name;

                                        if(strtolower($code_full_name) == strtolower($update_data))
                                        {
                                            $share_information_input['employee_name'] = $update_data;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Employee name does not match the employee code";
                                            $share_information_input['error_message']['Employee Name'] = "Employee name does not match the employee code";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Employee name does not match the employee code";
                                        $share_information_input['error_message']['Employee Name'] = "Employee name does not match the employee code";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid employee name";
                                    $share_information_input['error_message']['Employee Name'] = "Invalid employee name";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Employee name is required";
                                $share_information_input['error_message']['Employee Name'] = "Employee name is required";
                                $error ++;
                            }
                        }
                        
                        /*validate alloted shares*/
                        if ($i == 3)
                        {
                            $share_information_input['alloted_shares'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9\.\,]+$/", trim($update_data)))
                                {
                                    $total_alloted_shares = $total_alloted_shares + $update_data;
                                    $share_information_input['alloted_shares'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid alloted shares";
                                    $share_information_input['error_message']['Alloted Shares'] = "Invalid alloted shares";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Alloted shares is required";
                                $share_information_input['error_message']['Alloted Shares'] = "Alloted shares is required";
                                $error ++;
                            }
                        }

                        /*validate offer letter id*/
                        if ($i == 4)
                        {
                            $share_information_input['offer_letter_id'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($letters, function($filter_key) use ($update_data) {
                                        return $filter_key['id'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        if($match['status'] == 1)
                                        {
                                            if($match['type'] == 1)
                                            {
                                                $share_information_input['offer_letter_id'] = $update_data;
                                            }
                                            else
                                            {
                                                // $response['message'][] = "Offer letter ID provided is not an offer letter";
                                                $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID provided is not an offer letter";
                                                $error ++;
                                            }
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Offer letter ID is archived";
                                            $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID is archived";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Offer letter ID is not existing";
                                        $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid offer letter id";
                                    $share_information_input['error_message']['Offer Letter ID'] = "Invalid offer letter id";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Offer letter ID is required";
                                $share_information_input['error_message']['Offer Letter ID'] = "Offer letter ID is required";
                                $error ++;
                            }
                        }

                        /*validate acceptance letter id*/
                        if ($i == 5)
                        {
                            $share_information_input['acceptance_letter_id'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($letters, function($filter_key) use ($update_data) {
                                        return $filter_key['id'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        if($match['status'] == 1)
                                        {
                                            if($match['type'] == 2)
                                            {
                                                $share_information_input['acceptance_letter_id'] = $update_data;
                                            }
                                            else
                                            {
                                                // $response['message'][] = "Acceptance letter ID provided is not an acceptance letter";
                                                $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID provided is not an acceptance letter";
                                                $error ++;
                                            }
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Acceptance letter ID is archived";
                                            $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID is archived";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Acceptance letter ID is not existing";
                                        $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid acceptance letter id";
                                    $share_information_input['error_message']['Acceptance Letter ID'] = "Invalid acceptance letter id";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Acceptance letter ID is required";
                                $share_information_input['error_message']['Acceptance Letter ID'] = "Acceptance letter ID is required";
                                $error ++;
                            }
                        }

                    }
                    
                    if($error == 0)
                    {
                        $share_information_input['valid'] = 1;
                    }
                    else
                    {
                        $all_inputs_valid++;
                    }
                    
                    $share_distribution_upload_info[] = $share_information_input;
                }
            }
            
            if($total_alloted_shares > $esop_total_share_qty)
            {
                array_walk($share_distribution_upload_info, function(&$value, $key) use ($all_inputs_valid){ 
                    $all_inputs_valid++;
                    $value['valid'] = 0;
                    $value['error_message']['Alloted Shares'] = "Shares have exceeded the Total Alloted Shares. Please re-distribute the shares accordingly"; 
                });
            }

            if($all_inputs_valid == 0)
            {
                $response['status'] = TRUE;
            }

            $response['data'] = $share_distribution_upload_info;
        }
        return $response;
    }

    public function add_new_employee ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $error = 0;

        $params = $this->input->post();
        $esop_information = $params['esop_information'];
        $stock_offer_information = $params['stock_offer_information'];

        if(isset($esop_information) && count($esop_information) > 0 AND isset($stock_offer_information) && count($stock_offer_information) > 0
            && isset($stock_offer_information['esop_id']) && isset($stock_offer_information['user_id']) && isset($stock_offer_information['user_name']))
        {
            /*validate esop name*/
            if(isset($esop_information['name']) AND strlen(trim($esop_information['name'])) > 0)
            {
                if(strlen(trim($esop_information['name'])) > 1)
                {
                    $esop_information_input['name'] = $esop_information['name'];
                }
                else
                {
                    $response['message'][] = "Invalid esop name or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Esop name is required.";
                $error++; 
            }

            /*validate grant date*/
            if(isset($esop_information['grant_date']) AND strlen(trim($esop_information['grant_date'])) > 0)
            {
                if(strlen(trim($esop_information['grant_date'])) > 1)
                {
                    $esop_information_input['grant_date'] = date('Y-m-d' , strtotime($esop_information['grant_date']));
                }
                else
                {
                    $response['message'][] = "Invalid grant date or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Grant date is required.";
                $error++; 
            }

            /*validate total share quantity*/
            if(isset($esop_information['total_share_qty']) AND strlen(trim($esop_information['total_share_qty'])) > 0)
            {
                if(strlen(trim($esop_information['total_share_qty'])) > 1 AND preg_match("/^[0-9\,\.]+$/", $esop_information['total_share_qty']) )
                {
                    $esop_information_input['total_share_qty'] = str_replace(",", "", $esop_information['total_share_qty']);
                }
                else
                {
                    $response['message'][] = "Invalid total share quantity or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Total share quantity is required.";
                $error++; 
            }

            /*validate price per share*/
            if(isset($esop_information['price_per_share']) AND strlen(trim($esop_information['price_per_share'])) > 0)
            {
                if(strlen(trim($esop_information['price_per_share'])) > 1 AND preg_match("/^[0-9\,\.]+$/", $esop_information['price_per_share']) )
                {
                    $esop_information_input['price_per_share'] = str_replace(",", "", $esop_information['price_per_share']);
                }
                else
                {
                    $response['message'][] = "Invalid price per share or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Price per share is required.";
                $error++; 
            }

            /*validate currency*/
            if(isset($esop_information['currency']) AND strlen(trim($esop_information['currency'])) > 0)
            {
                if(preg_match("/^[0-9]+$/", $esop_information['currency']) )
                {
                    $esop_information_input['currency'] = $esop_information['currency'];
                }
                else
                {
                    $response['message'][] = "Invalid currency or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Currency is required.";
                $error++; 
            }

            /*validate vesting years*/
            if(isset($esop_information['vesting_years']) AND strlen(trim($esop_information['vesting_years'])) > 0)
            {
                if(preg_match("/^[0-9]+$/", $esop_information['vesting_years']) )
                {
                    $esop_information_input['vesting_years'] = $esop_information['vesting_years'];
                }
                else
                {
                    $response['message'][] = "Invalid vesting years or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Vesting years is required.";
                $error++; 
            }

            /*validate created by*/
            if(isset($esop_information['created_by']) AND strlen(trim($esop_information['created_by'])) > 0)
            {
                if(strlen(trim($esop_information['created_by'])) > 0 AND preg_match("/^[0-9]+$/", $esop_information['created_by']) )
                {
                    $esop_information_input['created_by'] = $esop_information['created_by'];
                }
                else
                {
                    $response['message'][] = "Invalid created by or is too short";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Created by is required.";
                $error++; 
            }

            $esop_information_input['status'] = isset($esop_information['status']) ? $esop_information['status'] : 1 ;
            $esop_information_input['is_deleted'] = isset($esop_information['is_deleted']) ? $esop_information['is_deleted'] : 0 ;
            $esop_information_input['parent_id'] = isset($esop_information['parent_id']) ? $esop_information['parent_id'] : 0 ;
            $esop_information_input['offer_expiration'] = isset($esop_information['offer_expiration']) ? date('Y-m-d' , strtotime($esop_information['offer_expiration'])) : '0000-00-00';
            $esop_information_input['is_special'] = isset($esop_information['is_special']) ? $esop_information['is_special'] : 0 ;
            
            if($error == 0)
            {
                /*insert if all inputs are okay*/
                $added_esop_information_id = $this->Esop_model->insert_esop_information($esop_information_input);

                if($added_esop_information_id > 0)
                {
                    // $response['message'][] = "Esop added successfully.";
                    // $response['status'] = TRUE;
                    // $response['data'] = $esop_information_input;
                    // $response['data']['id'] = $added_esop_information_id;

                    // $custom = "user_id = '".$stock_offer_information["user_id"]."' AND esop_id = '".$stock_offer_information["esop_id"]."' ";
                    // $delete_user_stock_offer_data = $this->Esop_model->delete_data($stock_offer_information['user_id'], 'user_stock_offer', 'user_id', $custom);
                    $update_stock_offer_status_qualifiers = array(
                        array(
                            "field" => "uso.user_id", 
                            "operator" => "=", 
                            "value" => $stock_offer_information["user_id"]
                        ),
                        array(
                            "field" => "uso.esop_id", 
                            "operator" => "IN", 
                            "value" => "(".$stock_offer_information["esop_id"].")"
                        )
                    );
                    $update_stock_offer_status_data = array(
                        "status" => 3
                    );
                    $update_stock_offer_status = $this->Esop_model->update_distribution_share_information($update_stock_offer_status_qualifiers, $update_stock_offer_status_data);

                    foreach ($stock_offer_information['employee_data'] as $key => $distribution_share_info)
                    {
                        $error_stock_offer = 0;
                        $distribution_share_information_input = array(
                                "id" => NULL,
                                "esop_id" => $added_esop_information_id,
                                "user_id" => '',
                                "stock_amount" => '',
                                "offer_letter_id" => '',
                                "date_sent" => ''
                            );

                        foreach ($distribution_share_info as $i => $update_data)
                        {
                            /*validate user id*/
                            if($i == 'user_id')
                            {
                                if(isset($update_data) AND strlen(trim($update_data)) > 0)
                                {
                                    if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                                    {
                                        $distribution_share_information_input['user_id'] = $update_data;
                                    }
                                    else
                                    {
                                        $response['message'][] = "Invalid user id or is too short";
                                        $error_stock_offer++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "User id is required.";
                                    $error_stock_offer++; 
                                }
                            }

                            /*validate stock amount*/
                            if($i == 'stock_amount')
                            {
                                if(isset($update_data) AND strlen(trim($update_data)) > 0)
                                {
                                    if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9\,\.]+$/", $update_data))
                                    {
                                        $distribution_share_information_input['stock_amount'] = str_replace(",", "", $update_data);
                                    }
                                    else
                                    {
                                        $response['message'][] = "Invalid stock amount or is too short";
                                        $error_stock_offer++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Stock amount is required.";
                                    $error_stock_offer++; 
                                }
                            }

                            /*validate offer letter id*/
                            if($i == 'offer_letter_id')
                            {
                                if(isset($update_data) AND strlen(trim($update_data)) > 0)
                                {
                                    if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                                    {
                                        $distribution_share_information_input['offer_letter_id'] = $update_data;
                                    }
                                    else
                                    {
                                        $response['message'][] = "Invalid offer letter id or is too short";
                                        $error_stock_offer++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Offer letter id is required.";
                                    $error_stock_offer++; 
                                }
                            }

                            /*validate acceptance letter id*/
                            if($i == 'acceptance_letter_id')
                            {
                                if(isset($update_data) AND strlen(trim($update_data)) > 0)
                                {
                                    if(strlen(trim($update_data)) > 0 AND preg_match("/^[0-9]+$/", $update_data))
                                    {
                                        $distribution_share_information_input['acceptance_letter_id'] = $update_data;
                                    }
                                    else
                                    {
                                        $response['message'][] = "Invalid acceptance letter id or is too short";
                                        $error_stock_offer++;
                                    }
                                }
                                else
                                {
                                    $response['message'][] = "Acceptance letter id is required.";
                                    $error_stock_offer++; 
                                }
                            }

                            $distribution_share_information_input['date_sent'] = date('Y-m-d H:i:s');
                            $distribution_share_information_input['status'] = 1;
                        }

                        if($error_stock_offer == 0)
                        {
                            /*insert if all inputs are okay*/
                            $add_employee_distribution_share_information_id = $this->Esop_model->insert_distribution_share_information($distribution_share_information_input);

                            if($add_employee_distribution_share_information_id > 0)
                            {
                                $response['message'][] = "Added employee successfully.";
                                $response['status'] = TRUE;

                                /*for logs*/
                                $_POST['params']['id'] = $esop_information['parent_id'];
                                $_POST['params']['name'] = $stock_offer_information['user_name'];
                                $_POST['status'] = TRUE;
                                
                                // if(isset($esop_information['type']))
                                // {
                                //     $_POST['params']['type'] = $esop_information['type'];
                                // }
                                // else
                                // {
                                //     $_POST['params']['type'] = 'main_esop';
                                // }
                            }
                        }
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function get_special ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params['parent_ids']) AND strlen($params['parent_ids']) > 0)
        {
            $parent_esop_ids = explode(',', $params['parent_ids']);

            $special_data = array();
            foreach ($parent_esop_ids as $key => $parent_esop_id) {

                $qualifiers = array(
                        'where' => array(
                                array(
                                    "field" => 'e.parent_id',
                                    "value" => $parent_esop_id
                                ),
                                array(
                                    "field" => 'e.is_special',
                                    "value" => 1
                                )
                            )
                    );

                $specials = $this->Esop_model->get_esop_data($qualifiers);
                unset($specials['total_rows']);

                if(count($specials) > 0)
                {
                    foreach ($specials as $i => $special) {
                        $total_shares_availed_esop = 0;
                        $total_shares_unavailed_esop = 0;
                 
                        $get_user_stock_offer_qualifiers = array(
                            "limit" => 999999, 
                            "where" => array(
                                array(
                                    "field" => "uso.esop_id", 
                                    "value" => $special['id']
                                ),
                                array(
                                    "field" => "uso.status", 
                                    "operator" => "!=", 
                                    "value" => 3
                                )
                            ) 
                        );

                        $user_stock_offers = $this->Esop_model->get_user_stock_offer_data($get_user_stock_offer_qualifiers);
                        unset($user_stock_offers['total_rows']);

                        $special_data[$special['id']] = $special;
                        $special_data[$special['id']]['stock_offers'] = array();

                        if(count($user_stock_offers) > 0)
                        {
                            $special_data[$special['id']]['stock_offers'] = $user_stock_offers;
                        }
                    }
                }
            }

            if(count($special_data) > 0)
            {
                $response['data'] = $special_data;
                $response['status'] = TRUE;
            }
        }

        echo json_encode($response);
    }

    public function offer_allotments () {
        $response['data'] = $this->Esop_model->get_offer_allotments();
        echo json_encode($response);
    }

    public function upload_payment_record ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/esop.js' );
        $this->template->add_content( $this->load->view( 'upload_payment_record', $data, TRUE ) );
        $this->template->draw();
    }

    public function upload_payment_record_csv ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $path = './assets/uploads/payment_record/csv';

        if(!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path,0777,TRUE);
        }
        // else
        // {
        //     $response['message'][] = 'Error uploading file, please try again later.';
        //     echo json_encode($response);
        //     exit();
        // }

        $file_name = uniqid( TRUE );

        if ( ! isset( $_FILES['csv_file']['name'] ) )
        {
            echo json_encode($response);
            exit();
        }

        $file       = pathinfo( $_FILES['csv_file']['name'] );
        $extensions = array( 'csv'/*, 'xls', 'xlsx'*/ );

        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ) )
        {
            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.csv' /*. $file['extension']*/;

            if(move_uploaded_file($_FILES['csv_file']['tmp_name'], './assets/uploads/payment_record/csv/' . $file_name))
            {
                $data['original_file_name'] = $_FILES['csv_file']['name'];
                $data['file_name'] = $file_name;
                $data['file_path'] = base_url() . '/assets/uploads/payment_record/csv/' . $file_name;
             
                $csv_file = fopen($data['file_path'], 'r');
                while (!feof($csv_file) ) {
                    $payment_record_info[] = fgetcsv($csv_file, 1024, ',');
                }
                fclose($csv_file);

                $payment_record_info = array_filter($payment_record_info);

                /*check if array has more than 2 columns. the first column / $payment_record_info[0] is the header, the second column / $payment_record_info[1] is false if it has no other data*/
                if(count($payment_record_info) > 1 AND isset($payment_record_info[1]) AND $payment_record_info[1] != FALSE)
                {
                    $response['status'] = TRUE;
                    $response['data'] = $data;
                    $response['message'][] = 'File successfully uploaded!';
                }
                else
                {
                    unlink('./assets/uploads/payment_record/csv/' . $file_name);
                    $response['message'][] = 'File uploaded has no content. Please upload file with content.';
                }
            }
            else
            {
                $response['message'][] = 'Error adding file, please try again later.';
            }
        }
        else
        {
            $response['message'][] = 'Invalid file, please try again.';
        }
        
        echo json_encode($response);
    }

    public function open_payment_csv()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $payment_record_info = array();

        $file = $this->input->post();

        if(isset($file['file_path']) && strlen($file['file_path']) > 0 && isset($file['esop_id']) && strlen($file['esop_id']) > 0 && $file['esop_id'] > 0)
        {
            $file_path = str_replace(base_url(), '.', $file['file_path']);

            if(file_exists($file_path))
            {
                $csv_file = fopen($file['file_path'], 'r');
                while (!feof($csv_file) ) {
                    $payment_record_info[] = fgetcsv($csv_file, 1024, ',');
                }
                fclose($csv_file);
        
                $payment_record_info = array_filter($payment_record_info);

                if(count($payment_record_info) > 1 AND isset($payment_record_info[1]) AND $payment_record_info[1] != FALSE)
                {
                    $validate = $this->validate_upload_payment_data($payment_record_info, $file['esop_id']);

                    if(isset($validate['data']) AND count($validate['data']) > 0)
                    {
                        $response['status'] = TRUE;
                        $response['data'] = $validate['data'];
                    }
                }
                else
                {
                    $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
                }
            }
            else
            {
                $response['message'][] = 'File uploaded has experienced some error. Please upload another file.';
            }
        }

        echo json_encode($response);
    }

    public function validate_upload_payment_data($payment_record_info = array(), $esop_id = 0)
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $all_inputs_valid = 0;
        $payment_record_upload_info = array();

        $employee_codes = array();

        $esop_ids = array($esop_id);
        $user_esop = array();

        $payment_methods_get = $this->Esop_model->get_payment_methods_data(array("limit" => 999999, "where" => array(array("field" => 'pm.status', "value" => '1'))));
        unset($payment_methods_get['total_rows']);
        $payment_methods = $payment_methods_get;
        
        $companies_controller = $this->load->module('companies/Companies');
        $companies_get = $companies_controller->Companies_model->get_company_data(array("limit" => 999999));
        unset($companies_get['total_rows']);
        $companies = $companies_get;

        $users_controller = $this->load->module('users/Users');
        $users_get = $users_controller->Users_model->get_user_data(array("limit" => 999999));
        unset($users_get['total_rows']);
        $users = $users_get;

        $currencies = $this->Esop_model->get_currency();

        $esop_special_get = $this->Esop_model->get_esop_data(
            array(
                "limit" => 999999, 
                "where" => array(
                    array("field" => 'e.parent_id', "value" => $esop_id),
                    array("field" => 'e.is_special', "value" => '1')
                )
            )
        );
        unset($esop_special_get['total_rows']);
        $esop_specials = $esop_special_get;

        if(count($esop_specials) > 0)
        {
            foreach ($esop_specials as $key => $value) {
                $esop_ids[] = $value['id'];
            }
        }

        $user_esop_get = $this->Esop_model->get_user_esop_data(array("limit" => 999999, "where" => array(array("field" => 'ue.id', "operator" => "IN", "value" => "(". join(',', $esop_ids) . ")"))));
        unset($user_esop_get['total_rows']);
        $user_esops = $user_esop_get;

        // echo json_encode($payment_methods);
        // echo json_encode($user_esops);
        // // echo json_encode($esops);
        // echo json_encode($esop_specials);
        // echo json_encode($esop_ids);
        // echo json_encode($currencies);
        // exit;

        $payment_record_info = array_filter($payment_record_info);

        if(isset($payment_record_info) AND count($payment_record_info) > 0)
        {
            foreach ($payment_record_info as $key => $payment_info) {

                if($key > 0)
                {
                    $error = 0;
                    $payment_record_input = array(
                            "company_code" => '',
                            "company_id" => '',
                            "employee_code" => '',
                            "employee_name" => '',
                            "employee_id" => '',
                            "payment_method_id" => '',
                            "payment_value" => '',
                            "currency" => '',
                            "currency_id" => '',
                            "vesting_rights_id" => '',
                            "valid" => 0,
                            "dividend_date" => '',
                            "error_message" => array()
                        );

                    foreach ($payment_info as $i => $update_data) {
                        $filtered = '';
                        $match = array();
                        $update_data = utf8_encode($update_data);
                        
                        /*validate company id*/
                        if ($i == 0)
                        {
                            $payment_record_input['company_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($companies, function($filter_key) use ($update_data) {
                                        return $filter_key['company_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $company_id = $match['id'];
                                        $payment_record_input['company_id'] = $company_id;
                                        $payment_record_input['company_code'] = $update_data;
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Company code is not existing";
                                        $payment_record_input['error_message']['Company Code'] = "Company code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Company code";
                                    $payment_record_input['error_message']['Company Code'] = "Invalid Company code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Company code is required";
                                $payment_record_input['error_message']['Company Code'] = "Company code is required";
                                $error ++;
                            }
                        }

                        /*validate epmployee code*/
                        if ($i == 1)
                        {
                            $payment_record_input['employee_code'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $filtered = array_filter($users, function($filter_key) use ($update_data) {
                                        return $filter_key['employee_code'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        if(in_array($update_data, $employee_codes))
                                        {
                                            array_walk($payment_record_upload_info, function(&$value, $key) use ($update_data) { 
                                                if($value['employee_code'] == $update_data)
                                                {
                                                    $value['error_message']['Employee Code'] = "Employee Code is already existing in the template"; 
                                                }
                                            });

                                            // $response['message'][] = "Employee Code is already existing.";
                                            $payment_record_input['error_message']['Employee Code'] = "Employee Code is already existing in the template";
                                            $error++;
                                        }
                                        else
                                        {
                                            foreach ($filtered as $filtered_key => $filtered_value) {
                                                $match = $filtered_value;
                                            }

                                            $match_id = $match['id'];
                                            $filtered_stocks = array_filter($user_esops, function($filter_key) use ($match_id) {
                                                return $filter_key['user_id'] === $match_id;
                                            });

                                            if(count($filtered_stocks) < 0)
                                            {
                                                // $response['message'][] = "Employee Code has no shares for this ESOP";
                                                $payment_record_input['error_message']['Employee Code'] = "Employee Code has no shares for this ESOP ";
                                                $error ++;
                                            }
                                            else
                                            {                                            
                                                if($payment_record_input['company_id'] == $match['company_id'])
                                                {
                                                    $user_id = $match['id'];
                                                    $payment_record_input['employee_id'] = $user_id;
                                                    array_push($employee_codes, $update_data);
                                                    $payment_record_input['employee_code'] = $update_data;

                                                    foreach ($filtered_stocks as $filtered_stocks_key => $filtered_stocks_value) {
                                                        $match_stocks = $filtered_stocks_value;
                                                    }

                                                    if(count($match_stocks) > 0)
                                                    {
                                                        $vesting_qualifiers = array(
                                                            'where' => array(
                                                                array(
                                                                    'field' => 'esop_map_id',
                                                                    'operator' => '=',
                                                                    'value' => $match_stocks['id']
                                                                )
                                                            )
                                                        );
                                                        $vesting_rights = $this->Esop_model->get_vesting_rights($vesting_qualifiers);

                                                        if(count($vesting_rights) > 0)
                                                        {
                                                            foreach ($vesting_rights as $key => $value) {
                                                                if($value['status'] == 0)
                                                                {
                                                                    $payment_record_input['vesting_rights_id'] = $value['id'];
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    // $response['message'][] = "Employee Code is does not match company code";
                                                    $payment_record_input['error_message']['Employee Code'] = "Employee Code is does not match company code";
                                                    $error ++;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Employee Code is not existing";
                                        $payment_record_input['error_message']['Employee Code'] = "Employee Code is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid Employee code";
                                    $payment_record_input['error_message']['Employee Code'] = "Invalid Employee code";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Employee code is required";
                                $payment_record_input['error_message']['Employee Code'] = "Employee code is required";
                                $error ++;
                            }
                        }

                        /*validate employee name*/
                        if ($i == 2)
                        {
                            $payment_record_input['employee_name'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 1)
                                {
                                    $employee_code = $payment_record_input['employee_code'];
                                    $filtered = array_filter($users, function($filter_key) use ($employee_code) {
                                        return $filter_key['employee_code'] == $employee_code;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $code_first_name = (isset($match['first_name']) AND strlen($match['first_name']) > 0) ? ' '.$match['first_name'] : '' ;
                                        $code_middle_name = (isset($match['middle_name']) AND strlen($match['middle_name']) > 0) ? ' '.$match['middle_name'] : '' ;
                                        $code_last_name = (isset($match['last_name']) AND strlen($match['last_name']) > 0) ? $match['last_name'] : '' ;

                                        $code_full_name = $code_last_name.''.$code_first_name.''.$code_middle_name;

                                        if(/*strtolower($code_full_name) == strtolower($update_data)*/strcmp(strtolower($code_full_name),strtolower($update_data)) == 0)
                                        {
                                            $payment_record_input['employee_name'] = $update_data;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Employee name does not match the employee code";
                                            $payment_record_input['error_message']['Employee Name'] = "Employee name does not match the employee code ";
                                            $error ++;
                                        }
                                    }
                                    /*else
                                    {
                                        // $response['message'][] = "Employee name does not match the employee code";
                                        $payment_record_input['error_message']['Employee Name'] = "Employee name does not match the employee code";
                                        $error ++;
                                    }*/
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid employee name";
                                    $payment_record_input['error_message']['Employee Name'] = "Invalid employee name";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Employee name is required";
                                $payment_record_input['error_message']['Employee Name'] = "Employee name is required";
                                $error ++;
                            }
                        }
                        
                        /*validate payment method id*/
                        if ($i == 3)
                        {
                            $payment_record_input['payment_method_id'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9]+$/", trim($update_data)))
                                {
                                    $filtered = array_filter($payment_methods, function($filter_key) use ($update_data) {
                                        return $filter_key['id'] === $update_data;
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        if($match['status'] == 1)
                                        {
                                            $payment_record_input['payment_method_id'] = $update_data;
                                        }
                                        else
                                        {
                                            // $response['message'][] = "Payment method ID is archived";
                                            $payment_record_input['error_message']['Payment Method ID'] = "Payment method ID is archived";
                                            $error ++;
                                        }
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Payment method ID is not existing";
                                        $payment_record_input['error_message']['Payment Method ID'] = "Payment method ID is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid payment method ID";
                                    $payment_record_input['error_message']['Payment Method ID'] = "Invalid payment method ID";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Payment method ID is required";
                                $payment_record_input['error_message']['Payment Method ID'] = "Payment method ID is required";
                                $error ++;
                            }
                        }

                        /*validate payment value*/
                        if ($i == 4)
                        {
                            $payment_record_input['payment_value'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0 AND preg_match("/^[0-9\.\,]+$/", trim($update_data)))
                                {
                                    $payment_record_input['payment_value'] = $update_data;
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid payment value";
                                    $payment_record_input['error_message']['Payment Value'] = "Invalid payment value";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Payment value is required";
                                $payment_record_input['error_message']['Payment Value'] = "Payment value is required";
                                $error ++;
                            }
                        }

                        /*validate currency*/
                        if ($i == 5)
                        {
                            $payment_record_input['currency'] = $update_data;
                            if(strlen($update_data) > 0)
                            {
                                if(strlen($update_data) > 0)
                                {
                                    $filtered = array_filter($currencies, function($filter_key) use ($update_data) {
                                        return strtolower($filter_key['name']) === strtolower($update_data);
                                    });

                                    if(count($filtered) > 0)
                                    {
                                        foreach ($filtered as $filtered_key => $filtered_value) {
                                            $match = $filtered_value;
                                        }

                                        $currency_id = $match['id'];
                                        $payment_record_input['currency_id'] = $currency_id;
                                        $payment_record_input['currency'] = $update_data;
                                    }
                                    else
                                    {
                                        // $response['message'][] = "Currency is not existing";
                                        $payment_record_input['error_message']['Currency'] = "Currency is not existing";
                                        $error ++;
                                    }
                                }
                                else
                                {
                                    // $response['message'][] = "Invalid currency";
                                    $payment_record_input['error_message']['Currency'] = "Invalid currency";
                                    $error ++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Currency is required";
                                $payment_record_input['error_message']['Currency'] = "Currency is required";
                                $error ++;
                            }
                        }

                        if($i == 6){
                            $payment_record_input['dividend_date'] = $update_data;
                        }
                    }
                    
                    if($error == 0)
                    {
                        $payment_record_input['valid'] = 1;
                    }
                    else
                    {
                        $all_inputs_valid++;
                    }
                    
                    $payment_record_upload_info[] = $payment_record_input;
                }
            }

            if($all_inputs_valid == 0)
            {
                $response['status'] = TRUE;
            }

            $response['data'] = $payment_record_upload_info;
        }
        return $response;
    }

    public function download_payment_record_template()
    {
        $file = './assets/templates/payment_record_template.csv';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function insert_upload_payment_record ()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );
        $params = $this->input->post();
        $payment_record_list = $params;

        if(isset($payment_record_list['data']) && count($payment_record_list['data']) > 0)
        {
            foreach ($payment_record_list['data'] as $i => $payment_record) {
                $payment_record_input = array();
                $payment_record_input['payment_type_id'] = isset($payment_record['payment_type_id']) ? $payment_record['payment_type_id'] : '' ;
                $payment_record_input['currency'] = isset($payment_record['currency']) ? $payment_record['currency'] : '' ;
                $payment_record_input['amount_paid'] = isset($payment_record['amount_paid']) ? $payment_record['amount_paid'] : '' ;
                $payment_record_input['vesting_rights_id'] = isset($payment_record['vesting_rights_id']) ? $payment_record['vesting_rights_id'] : '' ;
                $payment_record_input['placed_by'] = isset($payment_record['placed_by']) ? $payment_record['placed_by'] : '' ;
                $payment_record_input['user_id'] = isset($payment_record['user_id']) ? $payment_record['user_id'] : '' ;
                $payment_record_input['dividend_date'] = isset($payment_record['dividend_date']) ? date('Y-m-d', strtotime($payment_record['dividend_date'])) : '' ;
                $payment_record_input['date_of_or'] = date('Y-m-d H:i:s');
                $payment_record_input['date_added'] = date('Y-m-d H:i:s');
                $payment_record_input['esop_id'] = isset($payment_record['esop_id']) ? $payment_record['esop_id'] : '' ;

                /*insert if all inputs are okay*/
                $esop_id = $payment_record['esop_id'];
                $user_name = $payment_record['user_name'];
                if(isset($payment_record['esop_id'])) { unset($payment_record['esop_id']); }
                if(isset($payment_record['user_name'])) { unset($payment_record['user_name']); }

                $added_payment_id = $this->Esop_model->insert_user_payment($payment_record_input);

                if($added_payment_id > 0)
                {
                    $response['message'][] = "Payment Successfully Added.";
                    $response['status'] = TRUE;
                    $payment_record_input['id'] = $added_payment_id;
                    $response['data'][] = $payment_record_input;

                    /*for logs*/
                    $added['params']['id'] = $esop_id;
                    $added['params']['name'] = $user_name;
                    $_POST['status'] = TRUE;
                    $_POST['params']['added'][] = $added;
                }
            }
        }

        echo json_encode($response);
    }

    public function insert_outstanding_balance_as_payment() {
        $response = array(
            "status" => null,
            "message" => '',
            "data" => array()
        );

        $data = array(
            'user_id' => $this->input->post('user_id'),
            'amount_paid' => $this->input->post('current_balance'),
            'vesting_rights_id' => $this->input->post('vesting_rights_id'),
            'date_added' => date("Y-m-d H:i:s"),
            'date_of_or' => date('Y-m-d'),
            'gratuity_id' => $this->input->post('gratuity_id'),
            'currency' => $this->input->post('currency'),
            'placed_by' => $this->session->userdata('user_id')
        );

        $current_vesting_right_id = $this->input->post('current_vesting_rights');
        $outstanding_balance = $this->input->post('current_balance');
        $vesting_rights = $this->input->post('vesting_rights');

        // foreach ($vesting_rights as $key => $vesting_right) {
        //     if($vesting_right['id'] == $current_vesting_rights_id) {
                
        //     }
        // }

        $insert_result = $this->Esop_model->insert_outstanding_balance_as_payment($data);  
        if($insert_result > 0) {
            $response['status'] = true;
            $response['data'] = $data;
        } else {
            $response['status'] = false;
        }

        echo json_encode($response);
    }

    public function download_claim_form()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        if(strlen($params['html']) > 0)
        {
            $dompdf = new DOMPDF();
            $dompdf->load_html($params['html']);
            $dompdf->render();
            $output = $dompdf->output();

            $path = './assets/downloads/';

            if(!is_dir($path)) //create the folder if it's not already exists
            {
                mkdir($path,0777,TRUE);
            }

            $file_name = 'claim_form'.time().'.pdf';
            $dir_path = './assets/downloads/'.$file_name;
            $url_path = base_url() . "assets/downloads/" . $file_name;

            file_put_contents($dir_path, $output);

            $response['url'] = $url_path;
            $response['file_name'] = $file_name;
            $response['status'] = TRUE;
        }

        echo json_encode($response);
    }

    public function download_soa()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $params = $this->input->post();
        if(strlen($params['html']) > 0)
        {
            $dompdf = new DOMPDF();
            $dompdf->load_html($params['html']);
            $dompdf->render();
            $output = $dompdf->output();

            $path = './assets/downloads/';

            if(!is_dir($path)) //create the folder if it's not already exists
            {
                mkdir($path,0777,TRUE);
            }

            $file_name = 'soa'.time().'.pdf';
            $dir_path = './assets/downloads/'.$file_name;
            $url_path = base_url() . "assets/downloads/" . $file_name;

            file_put_contents($dir_path, $output);

            $response['url'] = $url_path;
            $response['file_name'] = $file_name;
            $response['status'] = TRUE;
        }

        echo json_encode($response);
    }

    public function get_esop_search()
    {
        $qualifier = $this->input->post("s_value");

        $esop_search = $this->Esop_model->get_esop_search($qualifier);

        echo json_encode($esop_search);
    }

     public function get_users_by_company()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );


        $company_id = $this->input->get('company_id');


        if($company_id == 0)
        {
            $user_by_company = $this->Esop_model->get_company_no_result();
            $company_id = $user_by_company[0]["id"];
        }

        $user_by_company = $this->Esop_model->get_users_by_company($company_id);

        if(count($user_by_company) > 0)
        {

        }else{
            $user_by_company = array(array("id"=>"0", "name"=>"No Users Found"));
        }


        $response['status'] = TRUE;
        $response["data"] = $user_by_company;
        
        echo json_encode($response);

    }

    public function get_payment_records()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $response['status'] = TRUE;
        $response["data"] = $this->Esop_model->get_payment_records();
        
        echo json_encode($response);

    }

    public function get_esop_info() {
        $esop_id = $this->input->get('id');
        $esop_info = $this->Esop_model->get_esop_info($esop_id);
        
        $response = array(
            "status" => TRUE,
            "message" => '',
            "data" => $esop_info
        );
        echo json_encode($response);
    }

    public function submit_offer_extension() {
        $esop_id = $this->input->post('esop_id');
        $update_data = array(
            'offer_expiration' => $this->input->post('offer_expiration')
        );

        $update_result = $this->Esop_model->submit_offer_extension($esop_id, $update_data);
        
        if($update_result > 0) {
            $response = array(
                "status" => TRUE,
                "message" => 'Expiration Offer Updated!',
                'data' => array()
            );
        } else {
            $response = array(
                "status" => FALSE,
                "message" => 'Expiration Offer Not Updated!',
                'data' => array()
            );
        }  
        // $response = array(
        //     "status" => TRUE,
        //     "message" => $esop_id,
        //     'data' => $update_result
        // ); 
        echo json_encode($response);
    }

    public function generate_new_code() {
        $new_code = 0;
        $response = array(
            "status" => TRUE,
            "message" => '',
            'data' => array()
        );
        
        if($this->input->post('table') != 'undefined' 
        && $this->input->post('table') != '') {

            $table = $this->input->post('table');
            $column = $this->input->post('column');
            $last_generated_code = $this->Esop_model->check_last_generated_code($table, $column);

            if(count($last_generated_code) != 0) {

                // $last_generated_code[0]['company_code'] = $last_generated_code[0]['company_code'] + 1;
                // $new_code = $last_generated_code[0]['company_code'];
                $last_generated_code[0][$column] = $last_generated_code[0][$column] + 1;
                $new_code = $last_generated_code[0][$column];
                $length = 5 - strlen(intval($new_code));
                
                for ($i = 1; $i <= $length ; $i++) { 
                    $new_code = '0' . $new_code;
                }

                $response['data'] = $new_code;

            } else {

                $new_code = '00001';
                $response['data'] = $new_code;

            }

        } else {

            $response['message'] = 'Code generation failed.';

        }
        

        echo json_encode($response);
    }


    public function check_existing_company_code($code, $generated_code) {
        // $code = '00001';
        $result = $this->Esop_model->check_existing_company_code($generated_code);
        if($result > 0) {
            return $code;
        } else {
            return $result;
        }
    }

    public function get_payment_per_user()
    {
        $response = array(
            "status" => TRUE,
            "message" => '',
            'data' => array()
        );

        $esop_id = $this->input->post("esop_id");
        $user_id = $this->input->post("user_id");

        $payment_per_user = $this->Esop_model->get_payment_per_user($esop_id, $user_id);

        $response["data"] = $payment_per_user;

         echo json_encode($response);

    }

    public function get_all_esop()
    {
        $response = array(
            "status" => TRUE,
            "message" => '',
            'data' => array()
        );

        $about_esop = $this->Esop_model->get_all_esop();

        $response["data"] = $about_esop;

         echo json_encode($response);

    }

}

/* End of file esop.php */
/* Location: ./application/modules/esop/controllers/esop.php */