<?php $user_role = $this->session->userdata('user_role'); ?>
<section class="claim_view">
	<section section-style="top-panel">
		<div class="content">
			<div>
				<!-- <h1 class="f-left hidden">ESOP View</h1> -->
				<div class="breadcrumbs margin-bottom-20 border-10px">
					<a href="#" class="back_to_index">ESOP Claim</a>
					<span class="fa fa-chevron-right margin-left-10 margin-right-10"></span>
					<a data-label="employee_name" data-label="employee_name">Jose Protacio</a>				
				</div>			
				<div class="clear"></div>
			</div>
			<h1 class="f-left">View Employee Claim</h1>
			
			<?php if($user_role == 2 || $user_role == 3){?>	

				<div class="f-right" data-container="send_claim_esop_container">		
					<button class="btn-normal display-inline-mid modal-trigger send_claim" modal-target="send-claim">Send Claim to ESOP Admin</button>
				</div>

				<div class="f-right hidden" data-container="mark_claim_as_completed">		
					<button class="btn-normal display-inline-mid modal-trigger" modal-target="mark-claim-as-completed">Mark Claim as Completed</button>
				</div>

			<?php } elseif ($user_role == 4) { ?>

				<div class="f-right" data-container="esop_admin_buttons_conter">			
					<button class="btn-normal display-inline-mid margin-right-10 print_claim">Print Claim</button>
					<button class="btn-normal display-inline-mid margin-right-10ain modal-trigger" modal-target="reject-claim">Reject Claim</button>
					<button class="btn-normal display-inline-mid modal-trigger" modal-target="approve-claim">Approve Claim</button>
				</div>

			<?php } ?>

				<div class="f-right hidden" data-container="soa_buttons_container">			
					<button class="btn-normal display-inline-mid margin-right-10 print_soa">Download Statement of Account</button>
				</div>

			<div class="clear"></div>
		</div>
	</section>

	<section section-style="content-panel">	
		

		<div class="content print-statement">

			<div class="hidden" data-container="claim_attachment_container">
				<div class="big-box border-10px">
					<table>
						<tbody><tr>
							<td class="text-left width-200px">ESOP Name</td>
							<td class="font-bold text-left" data-label="esop_name">ESOP 1</td>
							<td class="text-left width-200px">Vested Right:</td>
							<td class="font-bold text-left" data-label="vesting_years"></td>
							<td class="width-200px"></td>
							<td></td>
						</tr>
						<tr>
							<td class="text-left">OR Number:</td>
							<td class="font-bold text-left" data-label="document_name">0000</td>
							<td class="text-left">Date of OR:</td>
							<td class="font-bold text-left" data-label="date_of_or">October 1, 2015</td>
							<td class="text-left">Amount Paid:</td>
							<td class="font-bold text-left" data-label="amount_paid">0.00</td>
						</tr>
					</tbody></table>
				</div>

				<div class="big-box margin-top-30 padding-20px border-10px">
					<p class="font-20 margin-left-30 margin-bottom-30">Attachment</p>			
					<table class="border-all-ssmall" id="display-or-attachment">
						<tbody>
							<tr>
								<td class="text-left width-300px padding-left-50">
									<i class="fa fa-file-o fa-3x display-inline-mid margin-right-30"></i>
									<p class="display-inline-mid" data-label="document_name_type">ESOP Payment OR.pdf</p>
								</td>
								<td>
									<p class="display-inline-mid margin-right-10">Date Attached: </p>
									<p class="display-inline-mid font-bold" data-label="date_created">October 1, 2015</p>
								</td>
								<td>
									<a href="#" class="font-15" data-label="download_attachment">Download File</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="user-profile margin-bottom-30">
				<div class="margin-bottom-20 margin-top-50">
					<h2><abbr title="" data-label="esop_name">ESOP</abbr> Statement of Account</h2>
				</div>
				<div class="user-name">
					<p class="employee_name" data-label="employee_name">Joselito Salazar</p>
					<p class="position">Employee No: <span data-label="employee_code">132</span></p>
				</div>
				<div class="user-info">
					<table>
						<tbody>
							<tr>
								<td>Company</td>
								<td data-label="company_name"> ROXOL</td>
							</tr>
							<tr>
								<td>Department</td>
								<td data-label="department_name">ISD</td>
							</tr>
							<tr>
								<td>Rank:</td>
								<td data-label="rank_name">Officer 1</td>
							</tr>
							<tr>
								<td>Separation Status: </td>
								<td>N/A</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="option-box">
				<p class="title">Grant Started</p>
				<p class="description"  data-label="grant_date">August 31, 2013</p>
			</div>
			<div class="option-box">
				<p class="title">Subscription Price</p>
				<p class="description" data-label="price_per_share">Php 6.00 </p>
			</div>
			<div class="option-box">
				<p class="title">No. of Shares Availed</p>
				<p class="description" data-label="total_no_of_shares">130,144</p>
			</div>
			<div class="option-box">
				<p class="title">Value of Shares Availed</p>
				<p class="description" data-label="value_of_shares">Php 324,058.56 </p>
			</div>

			<h2 class="margin-top-50">Vesting Rights</h2>
			
			<div class="tbl-rounded">
				<table class="table-roxas tbl-display">
					<thead>
						<tr>
							<th>No.</th>
							<th>Year</th>
							<th>Percentage</th>
							<th>No. of Shares</th>
							<th>Value of Shares</th>
						</tr>
					</thead>
					<tbody data-container="vr_list_view">
						<tr class="tmp_vr_list template hidden">
							<td data-label="year_no">1</td>
							<td data-label="year">Sept. 01, 2013</td>
							<td data-label="percentage">20%</td>
							<td data-label="no_of_shares">26,028</td>
							<td data-label="value_of_shares">64,809.72 Php</td>
						</tr>

						<tr class="last-content">					
							<td colspan="2"></td>
							<td class="combine"><span class="total-text">Total:</span> 100%</td>
							<td data-label="total_no_of_shares">0.00</td>
							<td data-label="value_of_shares">0.00</td>
						</tr>
				</table>
			</tbody>

			<h2 class="margin-top-50">Payment Summary</h2>

			<div class="long-panel border-10px">
				<p class="first-text">Amount Shares Taken <span data-label="no_of_shares">130,144</span> | @ <span data-label="price_per_share">2.40</span></p>
				<p class="second-text margin-right-100" data-label="value_of_shares">Php 324, 058.56 </p>
				<div class="clear"></div>
			</div>

			<div class="tbl-runded">
				<table class="table-roxas tbl-display margin-top-20">
					<thead>
						<tr>
							<th>No.</th>
							<th>Payment Details</th>
							<th>Shares</th>
							<th>Date Paid</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody data-container="up_list_view">
						<tr class="tmp_up_list template hidden">
							<td data-label="id">1</td>
							<td data-label="payment_name">Gratuity</td>
							<td data-label="shares">-</td>
							<td data-label="date_of_or">October 2, 2013</td>
							<td data-label="amount_paid">18,399.36 Php</td>
						</tr>
						<tr class="no_record_found hidden">
							<td colspan="5">No Records Found.</td>
						</tr>
						<tr class="last-content">
							<td colspan="4" class="text-left">
								<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
								<p class="margin-left-30">Outstanding Balance (as of <span data-label="ob_as_of"><?php echo date("F j, Y")?></span>)</p>							
							</td>
							<td>
								<p class="font-15 padding-bottom-5" data-label="total_payment">0.00</p>
								<p class="font-15" data-label="ob_as_of_total">0.00</p>
							</td>					
						</tr>
					</tbody>
				</table>
			</div>

			<h2 class="margin-top-50 payment_application_header hidden">Payment Application</h2>
			<div data-container="payment_application_content">
				<div class="tmp_payment_application_container template hidden">
					<div class="long-panel border-10px margin-top-50">
						<p class="first-text">Year <span data-label="year_no">2</span></p>
						<p class="first-text margin-left-30" data-label="year">July 28, 2015</p>
						<p class="second-text margin-right-80" data-label="with_vesting_rights">With Vesting Rights</p>
						<div class="clear"></div>
					</div>

					<div class="long-panel border-10px margin-top-20">
						<p class="first-text">Amount Shares Taken <span data-label="no_of_shares">130,144</span> | @ <span data-label="price_per_share">2.40</span></p>
						<p class="second-text margin-right-80" data-label="value_of_shares">0.00</p>
						<div class="clear"></div>
					</div>

					<div class="tbl-runded">
						<table class="table-roxas tbl-display margin-top-20 payment_application_payments_container">
							<thead>
								<tr>
									<th>No.</th>
									<th>Payment Details</th>
									<th>Shares</th>
									<th>Date Paid</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody data-container="pa_list_view">
								<tr class="tmp_pa_list template hidden">
									<td data-label="id">1</td>
									<td data-label="payment_name">Gratuity</td>
									<td data-label="shares">-</td>
									<td data-label="date_of_or">October 2, 2013</td>
									<td data-label="amount_paid">18,399.36 Php</td>
								</tr>
								<tr class="no_record_found hidden">
									<td colspan="5">No Records Found.</td>
								</tr>
								<tr class="last-content">
									<td colspan="4" class="text-left">
										<p class="margin-left-30 padding-bottom-5">Total Payment: </p>
										<!-- <p class="margin-left-30">Outstanding Balance (as of <span data-label="ob_as_of"><?php echo date("F j, Y")?></span>)</p> -->							
									</td>
									<td>
										<p class="font-15 padding-bottom-5" data-label="total_payment">0.00</p>
										<!-- <p class="font-15" data-label="ob_as_of_total">0.00</p> -->
									</td>					
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<?php if($user_role == 3 || $user_role == 4){?>
			<div class="emp-claim-cont margin-top-40">
				<div class="emp-form" id="divcontents">
					<p class="title-form">Employee Claim Form</p>
					<table class="emp-table">
						<tbody>
							<tr>
								<td>ESOP Name:</td>
								<td data-label="claim_esop_name">ESOP 1</td>
								<td>Number of Matured Stocks:</td>
								<td><strong data-label="claim_no_of_shares">26,500 Stocks</strong></td>
							</tr>
						</tbody>
					</table>
					<div class="numbers">
						<p class="f-left">Number of Stocks to be claimed:</p>
						<p class="f-right" data-label="claim_no_of_shares">26,500 Shares</p>
						<div class="clear"></div>
					</div>
					<p>Furthermore, this is to request for the issuance of the aboce number of stock certificates under my name.</p>
					<div class="complex-info">
						<div>
							<table>
								<tbody>
									<tr>
										<td>
											<p class="" data-label="claim_employee_name">xx</p>
											<div class="line"></div>
											<p>Employee's Name and Signature</p>
										</td>
										<td>
											<p data-label="claim_date_claimed">September 21, 2014</p>
											<div class="line"></div>
											<p>Date</p>
										</td>
									</tr>
								</tbody>
							</table>					
						</div>
					</div>

					<div class="divider"></div>
					<p class="authorized">Authorization of Claim</p>
					<p class="long-text">This is to accept and validate the Mr. / Ms. <strong data-label="claim_employee_name">Joselito Salazar</strong> has execised his /
						her stock option vested rights, over <strong data-label="claim_no_of_shares">26,500</strong> number of shares under <strong><abbr title="
						Employee Stock Option Plan" data-label="claim_esop_name">ESOP</abbr></strong>.
						In this regard, the Stock Transfer Agent shall issue the Stocks Certificate as declared on this portion.</p>

					<p class="approved">Approved by:</p>
					<div class="incharge">
						<p><abbr title="Employee Stock Option Plan">ESOP</abbr> Administrator</p>
					</div>
				</div>
			</div>
			<?php } ?>

		<div>
	</section>

	<!-- send claim  -->
	<?php if($user_role == 2 || $user_role == 3){?>
	<div class="modal-container moda" modal-id="send-claim" data-container="send_claim_esop_container">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">SEND CLAIM TO ESOP ADMIN</h4>
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">	
				<div class="success add_success_message margin-bottom-10 hidden"></div>
				<div class="error add_error_message margin-bottom-10 hidden"></div>
				<table class="send-claim margin-top-10 ">
					<tbody>
						<tr>
							<td class="width-200px">
								<p>ESOP NAME</p>
							</td>
							<td>
								<p data-label="esop_name">ESOP 1</p>
							</td>
						</tr>
						<tr>
							<td>
								<p>Vesting Rights</p>
							</td>
							<td>
								<p class="">Year 1</p>
							</td>

						</tr>

						<!-- <tr>
							<td>
								<p>Outstanding Balance</p>
							</td>
							<td>
								<p data-label="outstanding_balance">Balance</p>
							</td>
							
						</tr> -->

						<tr>
							<td>Amount Paid:</td>
							<td>
								<input type="text" class="small add-border-radius-5px amountpaid input_numeric" name="amount_paid" />
								<div class="select width-100px add-radius" id="currency_dropdown">
									<select>
										<option value="PHP">PHP</option>
										<option value="USD">USD</option>
									</select>
								</div> 
							</td>
						</tr>	
						<tr>
							<td>
								<p>OR Number:</p>
							</td>
							<td>
								<input type="text" class="small add-border-radius-5px width-300px" name="document_name"/>
							</td>
						</tr>
						<tr>
							<td>
								<p>Date of OR:</p>
							</td>
							<td>
								<div class="date-picker display-inline-mid add-radius ">
									<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" name="date_of_or">
									<span class="fa fa-calendar text-center"></span>
								</div>
							</td>
						</tr>	
		
					
					</tbody>
				</table>		

				<p class="margin-top-20 font-bold">PLEASE ATTACH O.R. FILE:<span><a href="#" class="margin-left-30 lbl_upload_file">Upload File</a></span></p>
				<p class="margin-bottom-5 margin-top-15 display-inline-mid"><input type="file" name="claim_attachment" style="display: none;" class=""/><!-- File Name: <span><em class="file_browsed">No file uploaded yet </em></span> --></p>
				
				<!-- <p class="display-inline-mid"></p> -->
				<div class="bg-modal padding-20px margin-top-5 hidden" data-container="attachement_holder">
					<i class="fa fa-file-o fa-3x display-inline-mid"></i>
					<p class="display-inline-mid margin-left-10" data-container="attachement_name">File name here</p>
					<a href="#" class="red-color f-right margin-top-10 remove_attachment">Remove File</a>
					<div class="clear"></div>
				</div>

				<div class=" padding-20px margin-top-20 invalid_file error hidden">
					Invalid File Upload.
					<div class="clear"></div>
				</div>

				<!-- <p class="margin-bottom-20 margin-top-15 display-inline-mid">File Name: <span><em>No file uploaded yet</em></span></p>
				<p class="display-inline-mid"><a href="#" class=" margin-left-30">Upload File</a></p>
				<div class="bg-modal padding-20px margin-top-20">
					<i class="fa fa-file-pdf-o fa-3x display-inline-mid"></i>
					<p class="display-inline-mid margin-left-10">ESOP Payment OR.pdf</p>
					<a href="#" class="red-color f-right margin-top-10">Remove File</a>
					<div class="clear"></div>
				</div> -->
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
				<button type="button" class="display-inline-mid btn-normal btn-add-claim">Add Claim</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<div class="modal-container" modal-id="mark-claim-as-completed">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">MARK CLAIM AS COMPLETED</h4>
				<div class="modal-close close-me"></div>
			</div>

			<div class="modal-content padding-40px">
				<div class="success success_message margin-bottom-10 hidden"></div>
				<div class="error error_message margin-bottom-10 hidden"></div>
				<p>Are you sure you want to mark this claim as Completed?</p>
				<div class="date-picker display-inline-mid add-radius margin-top-10 margin-left-60">
					<input type="text" data-date-format="MM/DD/YYYY" class="width-200px" name="date_completed">
					<span class="fa fa-calendar text-center"></span>
				</div>
			</div>

			<div class="f-right margin-right-20 margin-bottom-10">
				<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
				<button type="button" class="display-inline-mid btn-normal btn-mark-claim-as-completed">Mark as Completed</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>


	<?php } elseif ($user_role == 4){ ?>
		<div class="modal-container" modal-id="reject-claim">
			<div class="modal-body small">
				<div class="modal-head">
					<h4 class="text-left">REJECT CLAIM</h4>
					<div class="modal-close close-me"></div>
				</div>

				<div class="modal-content">
					<div class="success success_message margin-bottom-10 hidden"></div>
					<div class="error error_message margin-bottom-10 hidden"></div>
					<p>Please state reason for rejection:</p>
					<textarea class="margin-top-10 border-10px" name="reject_reason"></textarea>

				</div>

				<div class="f-right margin-right-20 margin-bottom-10">
					<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
					<button type="button" class="display-inline-mid btn-normal btn-reject-claim">Reject Claim</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="modal-container" modal-id="approve-claim">
			<div class="modal-body small">
				<div class="modal-head">
					<h4 class="text-left">APPROVE CLAIM</h4>
					<div class="modal-close close-me"></div>
				</div>

				<div class="modal-content padding-40px">
					<div class="success success_message margin-bottom-10 hidden"></div>
					<div class="error error_message margin-bottom-10 hidden"></div>
					<p>Are you sure you want to approve this ESOP Claim?</p>

				</div>

				<div class="f-right margin-right-20 margin-bottom-10">
					<button type="button" class="display-inline-mid btn-cancel close-me margin-right-10">Cancel</button>			
					<button type="button" class="display-inline-mid btn-normal btn-approve-claim">Approve Claim</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	<?php } ?>

</section>

<iframe id="ifmcontentstoprint" style="height: 0px; width: 0px; position: absolute"></iframe>