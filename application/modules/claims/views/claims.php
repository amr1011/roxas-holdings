<section class="claim_index">
	<section section-style="top-panel">
		<div class="content">
			<div>
				<h1 class="f-left">ESOP Claims</h1>
				
				<div class="clear"></div>
			</div>

			<div class="header-effect">

				<div class="display-inline-mid default">
					<p class="white-color margin-bottom-5">Search</p>
					<div>
						<div class="select add-radius display-inline-mid" id="search_dropdown">
							<select>
								<option value="ESOP Name">ESOP Name</option>
								<option value="Vesting Years">Vesting Years</option>
								<!-- <option value="Grant Date">Grant Date</option> -->
								<option value="Share QTY">Price per Share</option>
								<option value="Status">Status</option>
							</select>
						</div>
						<div class="display-inline-mid search-me">
							<input type="text" class="search normal display-inline-mid margin-left-10 add-border-radius-5px search_name_input"/>
							<button class="btn-normal display-inline-mid margin-left-10 search_btn">Search</button>
						</div>
						<div class="display-inline-mid vesting-years">
							<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px search_vesting_years_input"/>
							<button class="btn-normal display-inline-mid margin-left-10 search_btn">Search</button>
						</div>
					</div>
				</div>

				<div class="display-inline-mid grant-date">
					<p class="white-color margin-bottom-5 margin-left-20">Grant Date</p>
					<div>
						<label class="display-inline-mid margin-left-20">From</label>
						<div class="date-picker add-radius display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_from_input">
							<span class="fa fa-calendar text-center"></span>
						</div>
						<label class="display-inline-mid margin-left-10">To</label>
						<div class="date-picker add-radius display-inline-mid margin-left-10">
							<input type="text" data-date-format="MM/DD/YYYY" class="search_grant_date_to_input">
							<span class="fa fa-calendar text-center"></span>
						</div>
						<button class="btn-normal display-inline-mid margin-left-10 search_btn">Search</button>
					</div>
				</div>

				<div class="display-inline-mid price-share">
					<label class="padding-left-20 margin-bottom-5 white-color">Price per Share</label>
					<br />
					<!-- <div class="price xsmall display-inline-mid margin-left-20">
						<input type="text">
					</div> -->
					<input type="text" class="search width-150px display-inline-mid margin-left-10 add-border-radius-5px search_price_per_share_input"/>
					<button class="btn-normal display-inline-mid margin-left-10 search_btn">Search</button>				
				</div>

				<div class="display-inline-mid margin-left-10 status" style="display: inline-block;">
					<label class="margin-bottom-5 white-color">Status</label>
					<br>
					<div class="select add-radius">
						<div class="frm-custom-dropdown">
							<div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="claim_status"></div><div class="frm-custom-icon"></div>
							<div class="frm-custom-dropdown-option">
								<div class="option" data-value="">All</div>
								<div class="option" data-value="0">Pending</div>
								<div class="option" data-value="2">Completed</div>
								<div class="option" data-value="3">Rejected</div>
								<div class="option" data-value="1">For Finalization</div>
							</div>
						</div>
						<select class="frm-custom-dropdown-origin" style="display: none;">
							<option value="">All</option>
							<option value="0">Pending</option>
							<option value="2">Completed</option>
							<option value="3">Rejected</option>
							<option value="1">For Finalization</option>						
						</select>
					</div>
					<button class="btn-normal display-inline-mid margin-left-10 search_btn">Search</button>
				</div>
				
			</div>
			
			<div class="text-right-line margin-top-30">
				<div class="view-by">
					<p>View By: 
					<i class="fa fa-th-large grid"></i>
					<i class="fa fa-bars list"></i>
					</p>

				</div>
				<div class="line"></div>
				
				<div class="content-text" id="sort">				
					<p class="font-15 white-color display-inline-mid">Sort By: <a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-name">ESOP Name <i class="fa fa-chevron-down"></i></a></p>
					<span class="margin-left-10 margin-right-10 white-color">|</span>
					<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-price-per-share">Price per Share <i class="fa fa-chevron-down"></i></a></p>
					<span class="margin-left-10 margin-right-10 white-color">|</span>
					<p class="font-15 white-color display-inline-mid"><a href="javascript:void(0)" class="white-color sort_field" data-sort-value="desc" data-sort-by="data-year-no">Vesting Year <i class="fa fa-chevron-down"></i></a></p>
				</div>
			</div>
		</div>
	</section>

	<section section-style="content-panel">

		<div class="content padding-top-30">

			<div class="grid-content" data-container="view_by_grid">

				<div class="data-box data-two divide-by-2 tmp_grid template hidden">
					<table class="width-100per">
						<tbody>
							<tr>
								<td colspan="2"><h3 class="font-bold black-color"><span data-label="employee_code">1219</span> - <span data-label="employee_name">Jose Protacio</span></h3></td>
								<td colspan="2" class="text-right font-15 font-bold padding-right-20" data-label="claim_status">PENDING</td>
							</tr>
							<tr>
								<td class="padding-top-10 padding-bottom-10">ESOP NAME:</td>
								<td data-label="esop_name">ESOP 1</td>
								<td class="text-right">Vesting Year:</td>
								<td class="text-right" data-label="vesting_years"></td>
							</tr>
							<tr>
								<td class="padding-top-10 padding-bottom-10">Number of Shares:</td>
								<td data-label="value_of_shares">25,000 Shares</td>
								<td class="text-right">Price per Share:</td>
								<td class="text-right" data-label="price_per_share">Php 6.00 </td>
							</tr>
						</tbody>
					</table>
					
					<div class="data-hover text-center">
						<a href="#">
							<button class="btn-normal grid_view_data">View Claim</button>
						</a>
					</div>
				</div>

				<div class="data-box width-100per padding-20-30px no_results_found template hidden">
					<table class="width-100percent">
						<tbody>
							<tr>
								<td class="text-center">No Results found</td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>

			<div class="tbl-rounded margin-top-20 table-content">
				<table class="table-roxas tbl-display">
					<thead>
						<tr>
							<th>Employee No.</th>
							<th>Employee Name</th>
							<th>ESOP Name</th>
							<th>Vesting Year</th>
							<th>No. of Shares</th>
							<th>Price per Share</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody data-container="view_by_list">
						<tr class="tmp_list template hidden">
							<td data-label="employee_code">12345</td>
							<td data-label="employee_name">Jose Protacio</td>
							<td data-label="esop_name">ESOP 1</td>
							<td data-label="vesting_years">Year 1</td>
							<td data-label="value_of_shares">25,000 Shares</td>
							<td data-label="price_per_share">Php 6.00</td>
							<td data-label="claim_status"><p class="color-pending">Pending</p></td>
							<td><a href="#" class="list_view_data">View claim</a></td>
						</tr>
						<tr class="no_results_found template hidden">
							<td colspan="8">No Results found</td>
						</tr>
					</tbody>
				</table>



			</div>


		<div>
	</section>
</section>