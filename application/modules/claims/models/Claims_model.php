<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Claims_model extends CI_Model {

    public function get($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT uc.id,
                         uc.date_claimed, 
                         uc.status as claim_status, 
                         uc.date_completed, 
                         u.id as user_id, 
                         u.employee_code, 
                         u.first_name, 
                         u.middle_name, 
                         u.last_name, 
                         c.name as company_name, 
                         d.name as department_name, 
                         r.name as rank_name, 
                         vr.id as vesting_rights_id, 
                         vr.year, 
                         vr.year_no, 
                         vr.percentage, 
                         vr.no_of_shares, 
                         vr.value_of_shares, 
                         ue.id as user_esop_id, 
                         e.id as esop_id, 
                         e.name as esop_name, 
                         e.grant_date, 
                         e.total_share_qty, 
                         e.price_per_share, 
                         e.currency, 
                         e.vesting_years, 
                         e.offer_expiration 
                FROM "user_claim" uc 
                LEFT JOIN "user" u ON uc.user_id = u.id 
                LEFT JOIN "company" c ON c.id = u.company_id 
                LEFT JOIN "department" d ON d.id = u.department_id 
                LEFT JOIN "rank" r ON r.id = u.rank_id 
                LEFT JOIN "claim_vesting_rights" cvr ON uc.id = cvr.claim_id 
                LEFT JOIN "vesting_rights" vr ON vr.id = cvr.vesting_rights_id 
                LEFT JOIN "user_esop" ue ON vr.esop_map_id = ue.id 
                LEFT JOIN "esop" e ON ue.esop_id = e.id 
                WHERE TRUE';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'e.name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'e.price_per_share')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if ($qualifiers['search_field'] == 'e.grant_date')
            {
                $sql .= " AND DATE(".$qualifiers['search_field'].") BETWEEN ".$qualifiers['keyword']." ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "uc.date_claimed";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        if(isset($qualifiers['group_by']))
        {
            $sql .= " GROUP BY ".$qualifiers['group_by'].", uc.date_claimed, uc.status, uc.date_completed, u.id, u.employee_code, u.first_name, u.middle_name, u.last_name, c.name, d.name, r.name, vr.id, vr.year, vr.year_no, vr.percentage, vr.no_of_shares, vr.value_of_shares, ue.id, e.id, e.name, e.grant_date, e.total_share_qty, e.price_per_share, e.currency, e.vesting_years, e.offer_expiration  ";
        }
        else
        {
            $sql .= " GROUP BY uc.id, uc.date_claimed, uc.status, uc.date_completed, u.id, u.employee_code, u.first_name, u.middle_name, u.last_name, c.name, d.name, r.name, vr.id, vr.year, vr.year_no, vr.percentage, vr.no_of_shares, vr.value_of_shares, ue.id, e.id, e.name, e.grant_date, e.total_share_qty, e.price_per_share, e.currency, e.vesting_years, e.offer_expiration ";
        }

        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

   public function employee_claims($qualifiers = array())
    {
        $result = array();
        $sql = 'SELECT uc.id, uc.date_claimed, uc.status as claim_status, u.id as user_id, u.employee_code, u.first_name, u.middle_name, u.last_name, c.name as company_name, d.name as department_name, r.name as rank_name, vr.id as vesting_rights_id, vr.year, vr.year_no, vr.percentage, vr.no_of_shares, vr.value_of_shares, ue.id as user_esop_id, e.id as esop_id, e.name as esop_name, e.grant_date, e.total_share_qty, e.price_per_share, e.currency, e.vesting_years, e.offer_expiration 
            FROM "user_claim" uc 
            LEFT JOIN "user" u ON uc.user_id = u.id 
            LEFT JOIN "company" c ON c.id = u.company_id 
            LEFT JOIN "department" d ON d.id = u.department_id 
            LEFT JOIN "rank" r ON r.id = u.rank_id 
            LEFT JOIN "claim_vesting_rights" cvr ON uc.id = cvr.claim_id 
            LEFT JOIN "vesting_rights" vr ON vr.id = cvr.vesting_rights_id 
            LEFT JOIN "user_esop" ue ON vr.esop_map_id = ue.id 
            LEFT JOIN "esop" e ON ue.esop_id = e.id 
            WHERE TRUE';
            
        if(isset($qualifiers['where']) AND count($qualifiers['where']) > 0) //if where clause has been passed
        {
            foreach($qualifiers['where'] as $key => $where)
            {
                if(isset($where['field']) AND isset($where['value']) AND strlen($where['field']) > 0 AND strlen($where['value']) > 0)
                {
                    $sql .= " AND ".$where['field']." = '".$where['value']."' ";
                }
            }
        }

        if(isset($qualifiers['keyword']) AND strlen($qualifiers['keyword']) > 0 AND isset($qualifiers['search_field']) AND strlen($qualifiers['search_field']) > 0)
        {
            if($qualifiers['search_field'] == 'e.name')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if($qualifiers['search_field'] == 'e.price_per_share')
            {
                $sql .= " AND ".$qualifiers['search_field']." LIKE '%".$qualifiers['keyword']."%' ";
            }
            else if ($qualifiers['search_field'] == 'e.grant_date')
            {
                $sql .= " AND DATE(".$qualifiers['search_field'].") BETWEEN ".$qualifiers['keyword']." ";
            }
            else
            {
                $sql .= " AND ".$qualifiers['search_field']." = '".$qualifiers['keyword']."' ";
            }
        }

        if(!isset($qualifiers['order_by']))
        {
            $order_by = "uc.date_claimed";
        }
        else
        {
            $order_by = $qualifiers['order_by'];
        }

        if(isset($qualifiers['group_by']))
        {
            $sql .= " GROUP BY ".$qualifiers['group_by'].",uc.date_claimed, uc.status, u.id, u.employee_code, u.first_name, u.middle_name, u.last_name, c.name, d.name, r.name, vr.id, vr.year, vr.year_no, vr.percentage, vr.no_of_shares, vr.value_of_shares, ue.id, e.id, e.name, e.grant_date, e.total_share_qty, e.price_per_share, e.currency, e.vesting_years, e.offer_expiration  ";
        }
        else
        {
            $sql .= " GROUP BY uc.id,uc.date_claimed, uc.status, u.id, u.employee_code, u.first_name, u.middle_name, u.last_name, c.name, d.name, r.name, vr.id, vr.year, vr.year_no, vr.percentage, vr.no_of_shares, vr.value_of_shares, ue.id, e.id, e.name, e.grant_date, e.total_share_qty, e.price_per_share, e.currency, e.vesting_years, e.offer_expiration ";
        }

        if(isset($qualifiers['sorting']))
        {
            $sql .= " ORDER BY ".$order_by." ".$qualifiers['sorting']." ";
        }

        if(isset($qualifiers['offset']) AND isset($qualifiers['limit']))
        {
            $sql .= " LIMIT ".$qualifiers['limit']." ";
        }

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        $sql2 = "SELECT count(*) OVER() as total";

        $total_rows = $this->db->query($sql2)->row()->total;
        $result["total_rows"] = $total_rows;

        return $result;
    }

    
    public function get_claim_vesting_rights($claim_id)
    {
        $result = array();
        $sql = 'SELECT 
                    cvr.*,
                    vr.id as vesting_rights_id,
                    vr.year,
                    vr.year_no,
                    vr.percentage,
                    vr.no_of_shares,
                    vr.value_of_shares,
                    ue.id as user_esop_id,
                    e.id as esop_id,
                    e.name as esop_name,
                    e.grant_date,
                    e.total_share_qty,
                    e.price_per_share,
                    e.currency,
                    e.vesting_years,
                    e.offer_expiration,
                    c.name as currency_name

                FROM "claim_vesting_rights" cvr
                LEFT JOIN "vesting_rights" vr ON vr.id = cvr.vesting_rights_id
                LEFT JOIN "user_esop" ue ON vr.esop_map_id = ue.id
                LEFT JOIN "esop" e ON ue.esop_id = e.id
                LEFT JOIN "currency" c ON e.currency = c.id

                WHERE TRUE ';

        $sql .= ' AND cvr.claim_id = '. $claim_id . ' ';
        //$sql .= " AND vr.year_no != 1 ";
        $sql .= ' ORDER BY cvr.vesting_rights_id DESC ';

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_vesting_rights($user_esop_id)
    {
        $result = array();
        $sql = 'SELECT 
                    vr.*

                FROM vesting_rights vr

                WHERE TRUE ';

        $sql .= ' AND vr.esop_map_id = '. $user_esop_id . ' ';
        $sql .= ' ORDER BY vr.year_no DESC ';

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_user_payments_vesting_rights_id($user_id,$vesting_rights_id)
    {
        $result = array();
        $sql = 'SELECT 
                    up.*,
                    c.name as currency_name,
                    p.name as payment_name

                FROM "user_payment" up
                LEFT JOIN "currency" c ON up.currency = c.id
                LEFT JOIN "payment_methods" p ON p.id = up.payment_type_id
                WHERE TRUE ';

        $sql .= ' AND up.user_id = '. $user_id . ' ';
        $sql .= ' AND up.vesting_rights_id = '. $vesting_rights_id . ' ';
        $sql .= ' ORDER BY up.id DESC ';

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_user_payments_by_esop_id($user_id,$esop_id)
    {
        $result = array();
        $sql = 'SELECT 
                    up.*,
                    c.name as currency_name,
                    p.name as payment_name,
                    vr.id as vesting_rights_id,
                    vr.year,
                    vr.year_no,
                    vr.percentage,
                    vr.no_of_shares,
                    vr.value_of_shares,
                    ue.id as user_esop_id,
                    e.id as esop_id,
                    e.name as esop_name,
                    e.grant_date,
                    e.total_share_qty,
                    e.price_per_share,
                    e.currency,
                    e.vesting_years,
                    e.offer_expiration

                FROM "user_payment" up
                LEFT JOIN "currency" c ON up.currency = c.id
                LEFT JOIN "payment_methods" p ON p.id = up.payment_type_id
                LEFT JOIN "vesting_rights" vr ON vr.id = up.vesting_rights_id
                LEFT JOIN "user_esop" ue ON vr.esop_map_id = ue.id
                LEFT JOIN "esop" e ON ue.esop_id = e.id
                WHERE TRUE ';

        $sql .= " AND up.user_id = ". $user_id . " ";
        $sql .= " AND e.id = ". $esop_id . " ";
        $sql .= " ORDER BY up.id DESC ";

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    public function insert_claim_attachment($data)
    {
        $this->db->insert('claim_attachment',$data);
        return $this->db->insert_id();
    }

    public function update_user_claim($id,$data)
    {
        $query = $this->db
        ->where('user_claim.id',$id)
        ->update('user_claim',$data);
        //print_r($query);exit;
        return $query;
    }

    public function get_claim_attachments_by_claim($claim_id,$user_id)
    {
        $result = array();
        $sql = "SELECT 
                    ca.*

                FROM claim_attachment ca
                WHERE TRUE ";

        $sql .= " AND ca.claim_id = ". $claim_id . " ";
        $sql .= " AND ca.user_id = ". $user_id . " ";
        $sql .= " AND ca.status = 1 ";
        $sql .= " ORDER BY ca.id DESC ";

        $query = $this->db->query($sql);
        if(count($query->result_array()) > 0)
        {
            $result = $query->result_array();
        }

        return $result;
    }

    
    public function update_claim_attachment($id,$data)
    {
        $query = $this->db
        ->where('claim_attachment.claim_id',$id)
        ->update('claim_attachment',$data);
        //print_r($query);exit;
        return $query;
    }

    public function update_vesting_rights($id,$data)
    {
        $query = $this->db
        ->where('vesting_rights.id',$id)
        ->update('vesting_rights',$data);
        //print_r($query);exit;
        return $query;
    }

    
}