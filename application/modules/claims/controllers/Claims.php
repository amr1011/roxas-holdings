<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Claims extends Authenticated_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        if( $this->session->userdata('user_id') == '' ) //if no session is found load login page
        {
           header('Location: '.base_url().'auth');
           exit;
        }

        $this->load->model('Claims/Claims_model');

        $allowed = $this->check_user_role();

        /*if(!$allowed && $this->input->is_ajax_request() == false)
        {
            header('Location: '.base_url().'auth/forbidden_403');
        }   */
    }


    public function index ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/claims.js' );
        $this->template->add_content( $this->load->view( 'claims', $data, TRUE ) );
        $this->template->draw();
    }

    

    public function view ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/claims.js' );
        $this->template->add_content( $this->load->view( 'view', $data, TRUE ) );
        $this->template->draw();
    }

    public function get ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'uc.id',
                    'name' => 'e.name',
                    'grant_date' => 'e.grant_date',
                    'total_share_qty' => 'total_share_qty',
                    'price_per_share' => 'e.price_per_share',
                    'currency' => 'e.currency',
                    'vesting_years' => 'e.vesting_years',
                    'is_deleted' => 'e.is_deleted',
                    'created_by' => 'e.created_by',
                    'status' => 'e.status',
                    'parent_id' => 'e.parent_id',
                    'claim_status' => 'uc.status',
                    'user_id' => 'ue.user_id'
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $total_rows = 0;

            $claims= array();

            $keyword = "";

            if(isset($params['keyword']))
            {
                if($params['keyword'] != '')
                {
                    $keyword = $params['keyword'];
                }else{
                    $keyword = $params['keyword'];
                }
            }
            //$claims['data_vesting_rights'] = array();
            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' =>$keyword,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "uc.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;


            $claims = $this->Claims_model->get($qualifiers);
            $total_rows = $claims['total_rows'];
            unset($claims['total_rows']);
            foreach ($claims as $key => $val) {
                $data_vesting_rights = $this->Claims_model->get_vesting_rights($val['user_esop_id']);
                $claims[$key]['data_vesting_rights'] = $data_vesting_rights;

                $data_claim_vesting_rights = $this->Claims_model->get_claim_vesting_rights($val['id']);
                $claims[$key]['data_claim_vesting_rights'] = $data_claim_vesting_rights;
                foreach ($data_claim_vesting_rights as $key2 => $val2) {

                    $data_user_payments_by_vesting_rights = $this->Claims_model->get_user_payments_vesting_rights_id($val['user_id'],$val2['vesting_rights_id']);
                    $claims[$key]['data_claim_vesting_rights'][$key2]['data_user_payments'] = $data_user_payments_by_vesting_rights;
                    //$claims[$key]['data_claim_vesting_rights'][$key2]['data_user_payments'] = $data_user_payments_by_vesting_rights;
                }

                $data_user_payments_summary = $this->Claims_model->get_user_payments_by_esop_id($val['user_id'],$val['esop_id']);
                $claims[$key]['data_user_payments_summary'] = $data_user_payments_summary;

                $data_claim_attachments = $this->Claims_model->get_claim_attachments_by_claim($val['id'],$val['user_id']);
                $claims[$key]['data_claim_attachments'] = $data_claim_attachments;

            }

            if($this->session->userdata('user_role') == 4)
            {

            }

            if(count($claims) > 0)
            {
                $response['total_rows'] = $total_rows;
                $response['data'] = $claims;
                $response['status'] = TRUE;
            }
        }


        if (count($get_param) == 0)
        {
            echo json_encode($response);
        } else {
            return($response['data']);
        }




    }

    public function employee_claims ()
    {
        $data = array();
        $this->template->add_script( assets_url() . '/js/libraries/claims.js' );
        $this->template->add_content( $this->load->view( 'claims-employee', $data, TRUE ) );
        $this->template->draw();
    }
public function get_payment_method()
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        $qualifiers = array(
            'where' => isset($params['where']) ? $params['where'] : array() ,
            'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
            'search_field' => isset($search_field) ? $search_field : '' ,
            'offset' => isset($params['offset']) ? $params['offset'] : 0,
            'limit' => isset($params['limit']) ? $params['limit'] : 30,
            'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "pm.id",
            'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
        );

        $qualifiers['where'] = array(
                    array(
                    'field' => 'pm.status',
                    'value' => '1'
                )
            );

        $payment_method = $this->Esop_model->get_payment_methods_data($qualifiers);
        unset($payment_method['total_rows']);
        if(count($payment_method) > 0)
        {
            $response['status'] = TRUE;
            $response['data'] = $payment_method;
        }

        echo json_encode($response);
    }

    public function get_employee_claims ($get_param = array())
    {
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if (count($get_param) == 0)
        {
            $params = $this->input->get();
        } else {
            $params = $get_param;
        }

        if(isset($params))
        {
            $alias = array(
                    'id' => 'uc.id',
                    'name' => 'e.name',
                    'grant_date' => 'e.grant_date',
                    'total_share_qty' => 'total_share_qty',
                    'price_per_share' => 'e.price_per_share',
                    'currency' => 'e.currency',
                    'vesting_years' => 'e.vesting_years',
                    'is_deleted' => 'e.is_deleted',
                    'created_by' => 'e.created_by',
                    'status' => 'e.status',
                    'parent_id' => 'e.parent_id',
                    'claim_status' => 'uc.status',
                    'user_id' => 'ue.user_id'
                );

            if(isset($params['search_field']) AND array_key_exists($params['search_field'], $alias))
            {
                $search_field = $alias[$params['search_field']];
            }

            $total_rows = 0;

            $claims= array();
            //$claims['data_vesting_rights'] = array();
            $qualifiers = array(
                    'where' => isset($params['where']) ? $params['where'] : array() ,
                    'keyword' => isset($params['keyword']) ? $params['keyword'] : '' ,
                    'search_field' => isset($search_field) ? $search_field : '' ,
                    'offset' => isset($params['offset']) ? $params['offset'] : 0,
                    'limit' => isset($params['limit']) ? $params['limit'] : 30,
                    'order_by' => isset($params['sort_field']) ? $params['sort_field'] : "uc.id",
                    'sorting' => isset($params['sorting']) ? $params['sorting'] : "asc",
                );

            $response['qualifiers'] = $qualifiers;


            $claims = $this->Claims_model->employee_claims($qualifiers);
            $total_rows = $claims['total_rows'];
            unset($claims['total_rows']);
            foreach ($claims as $key => $val) {
                $data_vesting_rights = $this->Claims_model->get_vesting_rights($val['user_esop_id']);
                $claims[$key]['data_vesting_rights'] = $data_vesting_rights;

                $data_claim_vesting_rights = $this->Claims_model->get_claim_vesting_rights($val['id']);
                $claims[$key]['data_claim_vesting_rights'] = $data_claim_vesting_rights;
                foreach ($data_claim_vesting_rights as $key2 => $val2) {

                    $data_user_payments_by_vesting_rights = $this->Claims_model->get_user_payments_vesting_rights_id($val['user_id'],$val2['vesting_rights_id']);
                    $claims[$key]['data_claim_vesting_rights'][$key2]['data_user_payments'] = $data_user_payments_by_vesting_rights;
                    //$claims[$key]['data_claim_vesting_rights'][$key2]['data_user_payments'] = $data_user_payments_by_vesting_rights;
                }

                $data_user_payments_summary = $this->Claims_model->get_user_payments_by_esop_id($val['user_id'],$val['esop_id']);
                $claims[$key]['data_user_payments_summary'] = $data_user_payments_summary;

                $data_claim_attachments = $this->Claims_model->get_claim_attachments_by_claim($val['id'],$val['user_id']);
                $claims[$key]['data_claim_attachments'] = $data_claim_attachments;

            }

            if($this->session->userdata('user_role') == 4)
            {

            }

            if(count($claims) > 0)
            {
                $response['total_rows'] = $total_rows;
                $response['data'] = $claims;
                $response['status'] = TRUE;
            }
        }


        if (count($get_param) == 0)
        {
            echo json_encode($response);
        } else {
            return($response['data']);
        }




    }

    public function send_claim_esop_to_admin ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['document_name']) AND strlen(trim($params['document_name'])) > 0)
            {
                if(isset($params['amount_paid']) AND strlen(trim($params['amount_paid'])) > 0)
                {
                    if(isset($params['currency']) AND strlen(trim($params['currency'])) > 0)
                    {
                        if(isset($params['user_id']) AND strlen(trim($params['user_id'])) > 0)
                        {

                            if(isset($params['path']) AND strlen(trim($params['path'])) > 0)
                            {
                                if(isset($params['document_type']) AND strlen(trim($params['document_type'])) > 0)
                                {

                                }
                                else
                                {
                                    // $response['message'][] = "Document Type is required.";
                                    // $error++;
                                }
                            }
                            else
                            {
                                // $response['message'][] = "Claim Attachment is required.";
                                // $error++;
                            }
                        }
                        else
                        {
                                $response['message'][] = "User ID is required.";
                                $error++;
                        }

                    }
                    else
                    {
                        $response['message'][] = "Currency is required.";
                        $error++;
                    }

                }
                else
                {
                    $response['message'][] = "Amount Paid is required.";
                    $error++;
                }
            }
            else
            {
                // $response['message'][] = "OR Number is required.";
                // $error++; 
            }

            if($error == 0)
            {
                $esop_name = $params['esop_name'];
                $user_name = $params['user_name'];
                if(isset($params['esop_name'])) { unset($params['esop_name']); }
                if(isset($params['user_name'])) { unset($params['user_name']); }

                /*insert if all inputs are okay*/
                $params['date_created'] = date('Y-m-d H:i:s');
                $insert_id = $this->Claims_model->insert_claim_attachment($params);

                if($insert_id > 0)
                {
                    $claim_id = $params['claim_id'];
                    $update_data = array(
                        'status' => 0
                        );
                    /*need to update user claim to for finalization*/
                    $this->Claims_model->update_user_claim($claim_id,$update_data);

                    $response['message'][] = "Claim to ESOP Admin Successfully Sent.";
                    $response['status'] = TRUE;
                    $response['data'] = $params;
                    $response['data']['id'] = $insert_id;

                    if($insert_id > 0)
                    {
                        /*for logs or notif*/
                        $_POST['params']['user_id'] = $params['user_id'];
                        $_POST['params']['user_name'] = $user_name;
                        $_POST['params']['esop_name'] = $esop_name;
                        $_POST['status'] = TRUE; 
                    }
                }
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function ajax_upload_attachment()
    {
        $return = array();

        $path = './assets/uploads/claim_attachments';

        if(!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path,0777,TRUE);
        }
        // else
        // {
        //     echo json_encode(array('status'=>FALSE, 'message'=>'Error updating image, please try again later.', 'data'=>array()));
        //     exit();
        // }

        echo json_encode($this->_upload_attachment($_FILES));
    }

    private function _upload_attachment($file)
    {
        $_FILES = $file;

        $file_name = uniqid( TRUE );


        if ( ! isset( $_FILES['claim_attachment']['name'] ) )
        {
            return array( 'status' => FALSE );
        }

        $file       = pathinfo( $_FILES['claim_attachment']['name'] );
        $extensions = array( 'jpg', 'jpeg', 'gif', 'png', 'pdf' );
        // kprint($file); exit;
        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ))
        {

            //get width and height of selected image
            list( $width, $height ) = getimagesize( $_FILES['claim_attachment']['tmp_name'] );

            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.' . $file['extension'];

            if(move_uploaded_file($_FILES['claim_attachment']['tmp_name'], './assets/uploads/claim_attachments/' . $file_name))
            {
                $data['file_path'] = /*base_url() . */'/assets/uploads/claim_attachments/' . $file_name;
                $data['document_type'] = $file['extension'];

                return array('status'=>TRUE, 'message'=>'File successfully uploaded!', 'data'=>$data);
            }
            else
            {
                return array('status'=>FALSE, 'message'=>'Error adding file, please try again later.', 'data'=>array());
            }

        }
        else
        {
            // return array('status'=>FALSE, 'message'=>'Invalid file, please try again.', 'data'=>array());
        }
    }

    public function reject_esop_claim ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['claim_id']) AND strlen(trim($params['claim_id'])) > 0)
            {
                if(isset($params['status']) AND strlen(trim($params['status'])) > 0)
                {
                    if(isset($params['reject_reason']) AND strlen(trim($params['reject_reason'])) > 0)
                    {
                        
                    }
                    else
                    {
                        $response['message'][] = "Reject reason is required.";
                        $error++;
                    }

                }
                else
                {
                    $response['message'][] = "Status is required.";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Claim ID is required.";
                $error++; 
            }

            if($error == 0)
            {
                /*update if all inputs are okay*/
                $claim_id = $params['claim_id'];

                $update_user_claim_data = array(
                            'status' => $params['status']
                            );

                $this->Claims_model->update_user_claim($claim_id, $update_user_claim_data);

                $update_claim_attachment_data = array(
                    'status' => $params['status']
                    );
                /*need to update claim_attachment*/
                $this->Claims_model->update_claim_attachment($claim_id,$update_claim_attachment_data);

                $response['message'][] = "Claim to ESOP Successfully Rejected.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                $response['data']['id'] = $claim_id;
                
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function approve_esop_claim ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['claim_id']) AND strlen(trim($params['claim_id'])) > 0)
            {
                if(isset($params['status']) AND strlen(trim($params['status'])) > 0)
                {

                }
                else
                {
                    $response['message'][] = "Status is required.";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Claim ID is required.";
                $error++; 
            }

            if($error == 0)
            {
                $user_id = $params['user_id'];
                $esop_name = $params['esop_name'];
                $user_name = $params['user_name'];
                if(isset($params['user_id'])) { unset($params['user_id']); }
                if(isset($params['esop_name'])) { unset($params['esop_name']); }
                if(isset($params['user_name'])) { unset($params['user_name']); }

                /*update if all inputs are okay*/
                $claim_id = $params['claim_id'];

                $update_user_claim_data = array(
                            'status' => $params['status']
                            );

                $this->Claims_model->update_user_claim($claim_id, $update_user_claim_data);


                $response['message'][] = "Claim to ESOP Successfully Approved.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                $response['data']['id'] = $claim_id;
            
                /*for logs or notif*/
                $_POST['params']['user_id'] = $user_id;
                $_POST['params']['user_name'] = $user_name;
                $_POST['params']['esop_name'] = $esop_name;
                $_POST['status'] = TRUE; 
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    public function mark_claim_as_completed ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['claim_id']) AND strlen(trim($params['claim_id'])) > 0)
            {
                if(isset($params['status']) AND strlen(trim($params['status'])) > 0)
                {

                }
                else
                {
                    $response['message'][] = "Status is required.";
                    $error++;
                }
            }
            else
            {
                $response['message'][] = "Claim ID is required.";
                $error++; 
            }

            if($error == 0)
            {
                $user_id = $params['user_id'];
                $esop_name = $params['esop_name'];
                $user_name = $params['user_name'];
                if(isset($params['user_id'])) { unset($params['user_id']); }
                if(isset($params['esop_name'])) { unset($params['esop_name']); }
                if(isset($params['user_name'])) { unset($params['user_name']); }
                
                /*update if all inputs are okay*/
                $claim_id = $params['claim_id'];
                $date_completed = date('Y-m-d', strtotime($params['date_completed']));
                $update_user_claim_data = array(
                            'status' => $params['status'],
                            'date_completed' => $date_completed
                            );

                $this->Claims_model->update_user_claim($claim_id, $update_user_claim_data);

                $response['message'][] = "Claim to ESOP Successfully Completed.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                $response['data']['id'] = $claim_id;
                
                /*for logs or notif*/
                $_POST['params']['user_id'] = $user_id;
                $_POST['params']['user_name'] = $user_name;
                $_POST['params']['esop_name'] = $esop_name;
                $_POST['status'] = TRUE; 
            }

            $response['data']= $params;

            echo json_encode($response);
        }
    }

    
    public function update_vesting_rights ()
    {
        if($this->input->is_ajax_request())/*validate if ajax request*/
        {
            $response = array(
                "status" => FALSE,
                "message" => array(),
                "data" => array()
            );
            $error = 0;

            $params = $this->input->post();

            if(isset($params['vesting_rights_ids']) AND strlen(trim($params['vesting_rights_ids'])) > 0)
            {

            }
            else
            {
                $response['message'][] = "Vesting Rights IDS required.";
                $error++; 
            }

            if($error == 0)
            {
                /*update if all inputs are okay*/

                $vesting_rights_ids = $params['vesting_rights_ids'];
                if($vesting_rights_ids !=""){
                    $vesting_rights_ids = substr($vesting_rights_ids, 0, -1);
                    $vesting_rights_ids  = explode(",", $vesting_rights_ids);

                    foreach($vesting_rights_ids as $vesting_rights_id){
                        $update_data = array(
                            'status' => 1
                        );
                        /*update vesting rights to completed*/
                        $this->Claims_model->update_vesting_rights($vesting_rights_id, $update_data);
                    }
                }

                $response['message'][] = "Vesting Rights Successfully Updated.";
                $response['status'] = TRUE;
                $response['data'] = $params;
                
            }

            //$response['data']= $params;

            echo json_encode($response);
        }
    }


}

/* End of file claims.php */
/* Location: ./application/modules/claims/controllers/claims.php */